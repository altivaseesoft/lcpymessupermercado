Imports System.Data.SqlClient
Imports System.Data
Imports system.Windows.Forms

Public Class FrmConfiguracion
    Inherits System.Windows.Forms.Form
    Private cConexion As Conexion
    Private sqlConexion As SqlConnection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        'pictureEdit1.DataBindings.Add(New Binding("EditValue", DsConfiguracion, "configuraciones.Logo"))
        Me.PictureEdit1.DataBindings.Add(New binding("EditValue", DsConfiguracion, "configuraciones.Logo"))
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents VTTel2 As System.Windows.Forms.TextBox
    Friend WithEvents VTFax2 As System.Windows.Forms.TextBox
    Friend WithEvents VTFax1 As System.Windows.Forms.TextBox
    Friend WithEvents VTTel1 As System.Windows.Forms.TextBox
    Friend WithEvents TxtFrase As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtCedula As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TxtInteres As System.Windows.Forms.TextBox
    Friend WithEvents TxtCajeros As System.Windows.Forms.TextBox
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    'Friend WithEvents DsConfiguracion As Inventario.DsConfiguracion
    Friend WithEvents DaConfiguracion As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents TxtImpuesto As System.Windows.Forms.TextBox
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents DsConfiguracion As DsConfiguracion
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents CheckBoxFacturaPersonalizada As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxUnico As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCredito As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxContadoPVE As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxContado As System.Windows.Forms.CheckBox
    Friend WithEvents TextBoxNumUnico As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxNumCRE As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxNumPVE As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxNumCON As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxCambiaPrecio As System.Windows.Forms.CheckBox
    Friend WithEvents txtNombreComercial As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CBMostrar_Cod_Barras As System.Windows.Forms.CheckBox
    Friend WithEvents txtLeyendaFactura As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmConfiguracion))
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtNombreComercial = New System.Windows.Forms.TextBox
        Me.DsConfiguracion = New LcPymes_5._2.DsConfiguracion
        Me.Label10 = New System.Windows.Forms.Label
        Me.TxtDireccion = New System.Windows.Forms.TextBox
        Me.VTTel2 = New System.Windows.Forms.TextBox
        Me.VTFax2 = New System.Windows.Forms.TextBox
        Me.VTFax1 = New System.Windows.Forms.TextBox
        Me.VTTel1 = New System.Windows.Forms.TextBox
        Me.TxtFrase = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TxtCedula = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TxtEmpresa = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtLeyendaFactura = New System.Windows.Forms.TextBox
        Me.CBMostrar_Cod_Barras = New System.Windows.Forms.CheckBox
        Me.CheckBoxCambiaPrecio = New System.Windows.Forms.CheckBox
        Me.TextBoxNumUnico = New System.Windows.Forms.TextBox
        Me.CheckBoxFacturaPersonalizada = New System.Windows.Forms.CheckBox
        Me.CheckBoxUnico = New System.Windows.Forms.CheckBox
        Me.TextBoxNumCRE = New System.Windows.Forms.TextBox
        Me.TextBoxNumPVE = New System.Windows.Forms.TextBox
        Me.CheckBoxCredito = New System.Windows.Forms.CheckBox
        Me.CheckBoxContadoPVE = New System.Windows.Forms.CheckBox
        Me.CheckBoxContado = New System.Windows.Forms.CheckBox
        Me.TextBoxNumCON = New System.Windows.Forms.TextBox
        Me.TxtCajeros = New System.Windows.Forms.TextBox
        Me.TxtInteres = New System.Windows.Forms.TextBox
        Me.TxtImpuesto = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.DaConfiguracion = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DsConfiguracion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(408, 224)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(400, 198)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Empresas"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtNombreComercial)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.TxtDireccion)
        Me.GroupBox2.Controls.Add(Me.VTTel2)
        Me.GroupBox2.Controls.Add(Me.VTFax2)
        Me.GroupBox2.Controls.Add(Me.VTFax1)
        Me.GroupBox2.Controls.Add(Me.VTTel1)
        Me.GroupBox2.Controls.Add(Me.TxtFrase)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.TxtCedula)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.TxtEmpresa)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(400, 176)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'txtNombreComercial
        '
        Me.txtNombreComercial.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreComercial.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NombreComercial"))
        Me.txtNombreComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreComercial.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreComercial.Location = New System.Drawing.Point(216, 32)
        Me.txtNombreComercial.Name = "txtNombreComercial"
        Me.txtNombreComercial.Size = New System.Drawing.Size(176, 13)
        Me.txtNombreComercial.TabIndex = 1
        Me.txtNombreComercial.Text = ""
        '
        'DsConfiguracion
        '
        Me.DsConfiguracion.DataSetName = "DsConfiguracion"
        Me.DsConfiguracion.Locale = New System.Globalization.CultureInfo("")
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Blue
        Me.Label10.Location = New System.Drawing.Point(216, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(176, 16)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "Nombre Comercial"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtDireccion
        '
        Me.TxtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtDireccion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Direccion"))
        Me.TxtDireccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDireccion.ForeColor = System.Drawing.Color.Blue
        Me.TxtDireccion.Location = New System.Drawing.Point(12, 150)
        Me.TxtDireccion.Name = "TxtDireccion"
        Me.TxtDireccion.Size = New System.Drawing.Size(376, 13)
        Me.TxtDireccion.TabIndex = 8
        Me.TxtDireccion.Text = ""
        '
        'VTTel2
        '
        Me.VTTel2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.VTTel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Tel_02"))
        Me.VTTel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VTTel2.ForeColor = System.Drawing.Color.Blue
        Me.VTTel2.Location = New System.Drawing.Point(316, 70)
        Me.VTTel2.Name = "VTTel2"
        Me.VTTel2.Size = New System.Drawing.Size(72, 13)
        Me.VTTel2.TabIndex = 4
        Me.VTTel2.Text = ""
        '
        'VTFax2
        '
        Me.VTFax2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.VTFax2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Fax_02"))
        Me.VTFax2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VTFax2.ForeColor = System.Drawing.Color.Blue
        Me.VTFax2.Location = New System.Drawing.Point(316, 110)
        Me.VTFax2.Name = "VTFax2"
        Me.VTFax2.Size = New System.Drawing.Size(72, 13)
        Me.VTFax2.TabIndex = 7
        Me.VTFax2.Text = ""
        '
        'VTFax1
        '
        Me.VTFax1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.VTFax1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Fax_01"))
        Me.VTFax1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VTFax1.ForeColor = System.Drawing.Color.Blue
        Me.VTFax1.Location = New System.Drawing.Point(228, 110)
        Me.VTFax1.Name = "VTFax1"
        Me.VTFax1.Size = New System.Drawing.Size(76, 13)
        Me.VTFax1.TabIndex = 6
        Me.VTFax1.Text = ""
        '
        'VTTel1
        '
        Me.VTTel1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.VTTel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Tel_01"))
        Me.VTTel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VTTel1.ForeColor = System.Drawing.Color.Blue
        Me.VTTel1.Location = New System.Drawing.Point(228, 70)
        Me.VTTel1.Name = "VTTel1"
        Me.VTTel1.Size = New System.Drawing.Size(72, 13)
        Me.VTTel1.TabIndex = 3
        Me.VTTel1.Text = ""
        '
        'TxtFrase
        '
        Me.TxtFrase.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtFrase.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Frase"))
        Me.TxtFrase.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtFrase.ForeColor = System.Drawing.Color.Blue
        Me.TxtFrase.Location = New System.Drawing.Point(12, 110)
        Me.TxtFrase.Name = "TxtFrase"
        Me.TxtFrase.Size = New System.Drawing.Size(176, 13)
        Me.TxtFrase.TabIndex = 5
        Me.TxtFrase.Text = ""
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Blue
        Me.Label6.Location = New System.Drawing.Point(12, 134)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(376, 16)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Direcci�n"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Blue
        Me.Label5.Location = New System.Drawing.Point(228, 94)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(160, 16)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Fax (es)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Blue
        Me.Label4.Location = New System.Drawing.Point(12, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(176, 16)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Frase Publicitaria"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtCedula
        '
        Me.TxtCedula.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtCedula.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Cedula"))
        Me.TxtCedula.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCedula.ForeColor = System.Drawing.Color.Blue
        Me.TxtCedula.Location = New System.Drawing.Point(12, 70)
        Me.TxtCedula.Name = "TxtCedula"
        Me.TxtCedula.Size = New System.Drawing.Size(176, 13)
        Me.TxtCedula.TabIndex = 2
        Me.TxtCedula.Text = ""
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Blue
        Me.Label3.Location = New System.Drawing.Point(228, 54)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(160, 16)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Tel�fono (s)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(12, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(176, 16)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "C�dula Jur�dica"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtEmpresa
        '
        Me.TxtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtEmpresa.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Empresa"))
        Me.TxtEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtEmpresa.ForeColor = System.Drawing.Color.Blue
        Me.TxtEmpresa.Location = New System.Drawing.Point(12, 30)
        Me.TxtEmpresa.Name = "TxtEmpresa"
        Me.TxtEmpresa.Size = New System.Drawing.Size(180, 13)
        Me.TxtEmpresa.TabIndex = 0
        Me.TxtEmpresa.Text = ""
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(12, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(180, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Empresa"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(400, 198)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Valores"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtLeyendaFactura)
        Me.GroupBox1.Controls.Add(Me.CBMostrar_Cod_Barras)
        Me.GroupBox1.Controls.Add(Me.CheckBoxCambiaPrecio)
        Me.GroupBox1.Controls.Add(Me.TextBoxNumUnico)
        Me.GroupBox1.Controls.Add(Me.CheckBoxFacturaPersonalizada)
        Me.GroupBox1.Controls.Add(Me.CheckBoxUnico)
        Me.GroupBox1.Controls.Add(Me.TextBoxNumCRE)
        Me.GroupBox1.Controls.Add(Me.TextBoxNumPVE)
        Me.GroupBox1.Controls.Add(Me.CheckBoxCredito)
        Me.GroupBox1.Controls.Add(Me.CheckBoxContadoPVE)
        Me.GroupBox1.Controls.Add(Me.CheckBoxContado)
        Me.GroupBox1.Controls.Add(Me.TextBoxNumCON)
        Me.GroupBox1.Controls.Add(Me.TxtCajeros)
        Me.GroupBox1.Controls.Add(Me.TxtInteres)
        Me.GroupBox1.Controls.Add(Me.TxtImpuesto)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(400, 192)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Blue
        Me.Label11.Location = New System.Drawing.Point(16, 129)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(128, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Leyenda Factura"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLeyendaFactura
        '
        Me.txtLeyendaFactura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Leyenda_Fact"))
        Me.txtLeyendaFactura.Location = New System.Drawing.Point(16, 145)
        Me.txtLeyendaFactura.Multiline = True
        Me.txtLeyendaFactura.Name = "txtLeyendaFactura"
        Me.txtLeyendaFactura.Size = New System.Drawing.Size(368, 40)
        Me.txtLeyendaFactura.TabIndex = 22
        Me.txtLeyendaFactura.Text = ""
        '
        'CBMostrar_Cod_Barras
        '
        Me.CBMostrar_Cod_Barras.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CBMostrar_Cod_Barras.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.Mostrar_CodBarras"))
        Me.CBMostrar_Cod_Barras.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBMostrar_Cod_Barras.ForeColor = System.Drawing.Color.Blue
        Me.CBMostrar_Cod_Barras.Location = New System.Drawing.Point(168, 110)
        Me.CBMostrar_Cod_Barras.Name = "CBMostrar_Cod_Barras"
        Me.CBMostrar_Cod_Barras.Size = New System.Drawing.Size(216, 16)
        Me.CBMostrar_Cod_Barras.TabIndex = 21
        Me.CBMostrar_Cod_Barras.Text = "Mostrar Cod. Barras"
        '
        'CheckBoxCambiaPrecio
        '
        Me.CheckBoxCambiaPrecio.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CheckBoxCambiaPrecio.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.CambiaPrecioCredito"))
        Me.CheckBoxCambiaPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxCambiaPrecio.ForeColor = System.Drawing.Color.Blue
        Me.CheckBoxCambiaPrecio.Location = New System.Drawing.Point(168, 78)
        Me.CheckBoxCambiaPrecio.Name = "CheckBoxCambiaPrecio"
        Me.CheckBoxCambiaPrecio.Size = New System.Drawing.Size(216, 16)
        Me.CheckBoxCambiaPrecio.TabIndex = 20
        Me.CheckBoxCambiaPrecio.Text = "Cambia Precio Personalizado"
        '
        'TextBoxNumUnico
        '
        Me.TextBoxNumUnico.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxNumUnico.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NumeroConsecutivo"))
        Me.TextBoxNumUnico.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNumUnico.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxNumUnico.Location = New System.Drawing.Point(312, 15)
        Me.TextBoxNumUnico.Name = "TextBoxNumUnico"
        Me.TextBoxNumUnico.Size = New System.Drawing.Size(72, 13)
        Me.TextBoxNumUnico.TabIndex = 18
        Me.TextBoxNumUnico.Text = "0.00"
        Me.TextBoxNumUnico.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CheckBoxFacturaPersonalizada
        '
        Me.CheckBoxFacturaPersonalizada.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CheckBoxFacturaPersonalizada.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.Imprimir_en_Factura_Personalizada"))
        Me.CheckBoxFacturaPersonalizada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxFacturaPersonalizada.ForeColor = System.Drawing.Color.Blue
        Me.CheckBoxFacturaPersonalizada.Location = New System.Drawing.Point(168, 94)
        Me.CheckBoxFacturaPersonalizada.Name = "CheckBoxFacturaPersonalizada"
        Me.CheckBoxFacturaPersonalizada.Size = New System.Drawing.Size(216, 16)
        Me.CheckBoxFacturaPersonalizada.TabIndex = 19
        Me.CheckBoxFacturaPersonalizada.Text = "Utilizar Factura Personalizada"
        '
        'CheckBoxUnico
        '
        Me.CheckBoxUnico.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CheckBoxUnico.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.UnicoConsecutivo"))
        Me.CheckBoxUnico.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxUnico.ForeColor = System.Drawing.Color.Blue
        Me.CheckBoxUnico.Location = New System.Drawing.Point(168, 15)
        Me.CheckBoxUnico.Name = "CheckBoxUnico"
        Me.CheckBoxUnico.Size = New System.Drawing.Size(144, 16)
        Me.CheckBoxUnico.TabIndex = 17
        Me.CheckBoxUnico.Text = "Consecutivo Unico"
        '
        'TextBoxNumCRE
        '
        Me.TextBoxNumCRE.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxNumCRE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NumeroCredito"))
        Me.TextBoxNumCRE.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNumCRE.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxNumCRE.Location = New System.Drawing.Point(312, 31)
        Me.TextBoxNumCRE.Name = "TextBoxNumCRE"
        Me.TextBoxNumCRE.Size = New System.Drawing.Size(72, 13)
        Me.TextBoxNumCRE.TabIndex = 16
        Me.TextBoxNumCRE.Text = "0.00"
        Me.TextBoxNumCRE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBoxNumPVE
        '
        Me.TextBoxNumPVE.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxNumPVE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NumeroPuntoVenta"))
        Me.TextBoxNumPVE.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNumPVE.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxNumPVE.Location = New System.Drawing.Point(312, 63)
        Me.TextBoxNumPVE.Name = "TextBoxNumPVE"
        Me.TextBoxNumPVE.Size = New System.Drawing.Size(72, 13)
        Me.TextBoxNumPVE.TabIndex = 15
        Me.TextBoxNumPVE.Text = "0.00"
        Me.TextBoxNumPVE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CheckBoxCredito
        '
        Me.CheckBoxCredito.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CheckBoxCredito.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.ConsCredito"))
        Me.CheckBoxCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxCredito.ForeColor = System.Drawing.Color.Blue
        Me.CheckBoxCredito.Location = New System.Drawing.Point(168, 31)
        Me.CheckBoxCredito.Name = "CheckBoxCredito"
        Me.CheckBoxCredito.Size = New System.Drawing.Size(144, 16)
        Me.CheckBoxCredito.TabIndex = 14
        Me.CheckBoxCredito.Text = "Consecutivo Cr�dito"
        '
        'CheckBoxContadoPVE
        '
        Me.CheckBoxContadoPVE.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CheckBoxContadoPVE.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.ConsPuntoVenta"))
        Me.CheckBoxContadoPVE.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxContadoPVE.ForeColor = System.Drawing.Color.Blue
        Me.CheckBoxContadoPVE.Location = New System.Drawing.Point(168, 63)
        Me.CheckBoxContadoPVE.Name = "CheckBoxContadoPVE"
        Me.CheckBoxContadoPVE.Size = New System.Drawing.Size(144, 16)
        Me.CheckBoxContadoPVE.TabIndex = 13
        Me.CheckBoxContadoPVE.Text = "Consecutivo PVE"
        '
        'CheckBoxContado
        '
        Me.CheckBoxContado.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CheckBoxContado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.ConsContado"))
        Me.CheckBoxContado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxContado.ForeColor = System.Drawing.Color.Blue
        Me.CheckBoxContado.Location = New System.Drawing.Point(168, 47)
        Me.CheckBoxContado.Name = "CheckBoxContado"
        Me.CheckBoxContado.Size = New System.Drawing.Size(144, 16)
        Me.CheckBoxContado.TabIndex = 12
        Me.CheckBoxContado.Text = "Consecutivo Contado"
        '
        'TextBoxNumCON
        '
        Me.TextBoxNumCON.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxNumCON.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NumeroContado"))
        Me.TextBoxNumCON.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNumCON.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxNumCON.Location = New System.Drawing.Point(312, 47)
        Me.TextBoxNumCON.Name = "TextBoxNumCON"
        Me.TextBoxNumCON.Size = New System.Drawing.Size(72, 13)
        Me.TextBoxNumCON.TabIndex = 11
        Me.TextBoxNumCON.Text = "0.00"
        Me.TextBoxNumCON.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtCajeros
        '
        Me.TxtCajeros.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtCajeros.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Cajeros"))
        Me.TxtCajeros.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCajeros.ForeColor = System.Drawing.Color.Blue
        Me.TxtCajeros.Location = New System.Drawing.Point(17, 95)
        Me.TxtCajeros.Name = "TxtCajeros"
        Me.TxtCajeros.Size = New System.Drawing.Size(128, 13)
        Me.TxtCajeros.TabIndex = 10
        Me.TxtCajeros.Text = "0.00"
        Me.TxtCajeros.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtInteres
        '
        Me.TxtInteres.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtInteres.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Intereses"))
        Me.TxtInteres.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtInteres.ForeColor = System.Drawing.Color.Blue
        Me.TxtInteres.Location = New System.Drawing.Point(17, 63)
        Me.TxtInteres.Name = "TxtInteres"
        Me.TxtInteres.Size = New System.Drawing.Size(128, 13)
        Me.TxtInteres.TabIndex = 9
        Me.TxtInteres.Text = "0.00"
        Me.TxtInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtImpuesto
        '
        Me.TxtImpuesto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtImpuesto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Imp_Venta"))
        Me.TxtImpuesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtImpuesto.ForeColor = System.Drawing.Color.Blue
        Me.TxtImpuesto.Location = New System.Drawing.Point(17, 31)
        Me.TxtImpuesto.Name = "TxtImpuesto"
        Me.TxtImpuesto.Size = New System.Drawing.Size(128, 13)
        Me.TxtImpuesto.TabIndex = 8
        Me.TxtImpuesto.Text = "0.00"
        Me.TxtImpuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Blue
        Me.Label9.Location = New System.Drawing.Point(17, 79)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(128, 16)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "N�mero de Cajeros"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Blue
        Me.Label8.Location = New System.Drawing.Point(17, 47)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(128, 16)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Intereses"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Blue
        Me.Label7.Location = New System.Drawing.Point(17, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(128, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Impuesto Venta"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.PictureEdit1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 4)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(400, 198)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Logo"
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Location = New System.Drawing.Point(103, 3)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Size = New System.Drawing.Size(192, 176)
        Me.PictureEdit1.TabIndex = 21
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.AutoSize = False
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarRegistrar, Me.ToolBarCerrar})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 225)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(408, 52)
        Me.ToolBar1.TabIndex = 61
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Text = "Nuevo"
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Text = "Registrar"
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'DaConfiguracion
        '
        Me.DaConfiguracion.DeleteCommand = Me.SqlDeleteCommand1
        Me.DaConfiguracion.InsertCommand = Me.SqlInsertCommand1
        Me.DaConfiguracion.SelectCommand = Me.SqlSelectCommand1
        Me.DaConfiguracion.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "configuraciones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Empresa", "Empresa"), New System.Data.Common.DataColumnMapping("NombreComercial", "NombreComercial"), New System.Data.Common.DataColumnMapping("Tel_01", "Tel_01"), New System.Data.Common.DataColumnMapping("Tel_02", "Tel_02"), New System.Data.Common.DataColumnMapping("Fax_01", "Fax_01"), New System.Data.Common.DataColumnMapping("Fax_02", "Fax_02"), New System.Data.Common.DataColumnMapping("Direccion", "Direccion"), New System.Data.Common.DataColumnMapping("Imp_Venta", "Imp_Venta"), New System.Data.Common.DataColumnMapping("Frase", "Frase"), New System.Data.Common.DataColumnMapping("Cajeros", "Cajeros"), New System.Data.Common.DataColumnMapping("Logo", "Logo"), New System.Data.Common.DataColumnMapping("Intereses", "Intereses"), New System.Data.Common.DataColumnMapping("UnicoConsecutivo", "UnicoConsecutivo"), New System.Data.Common.DataColumnMapping("NumeroConsecutivo", "NumeroConsecutivo"), New System.Data.Common.DataColumnMapping("ConsContado", "ConsContado"), New System.Data.Common.DataColumnMapping("NumeroContado", "NumeroContado"), New System.Data.Common.DataColumnMapping("ConsCredito", "ConsCredito"), New System.Data.Common.DataColumnMapping("NumeroCredito", "NumeroCredito"), New System.Data.Common.DataColumnMapping("ConsPuntoVenta", "ConsPuntoVenta"), New System.Data.Common.DataColumnMapping("NumeroPuntoVenta", "NumeroPuntoVenta"), New System.Data.Common.DataColumnMapping("Imprimir_en_Factura_Personalizada", "Imprimir_en_Factura_Personalizada"), New System.Data.Common.DataColumnMapping("FormatoCheck", "FormatoCheck"), New System.Data.Common.DataColumnMapping("Contabilidad", "Contabilidad"), New System.Data.Common.DataColumnMapping("CambiaPrecioCredito", "CambiaPrecioCredito"), New System.Data.Common.DataColumnMapping("Mostrar_CodBarras", "Mostrar_CodBarras"), New System.Data.Common.DataColumnMapping("Leyenda_Fact", "Leyenda_Fact")})})
        Me.DaConfiguracion.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM configuraciones WHERE (Cedula = @Original_Cedula) AND (Cajeros = @Ori" & _
        "ginal_Cajeros) AND (CambiaPrecioCredito = @Original_CambiaPrecioCredito) AND (Co" & _
        "nsContado = @Original_ConsContado) AND (ConsCredito = @Original_ConsCredito) AND" & _
        " (ConsPuntoVenta = @Original_ConsPuntoVenta) AND (Contabilidad = @Original_Conta" & _
        "bilidad) AND (Direccion = @Original_Direccion) AND (Empresa = @Original_Empresa)" & _
        " AND (Fax_01 = @Original_Fax_01) AND (Fax_02 = @Original_Fax_02) AND (FormatoChe" & _
        "ck = @Original_FormatoCheck) AND (Frase = @Original_Frase) AND (Imp_Venta = @Ori" & _
        "ginal_Imp_Venta) AND (Imprimir_en_Factura_Personalizada = @Original_Imprimir_en_" & _
        "Factura_Personalizada) AND (Intereses = @Original_Intereses) AND (Leyenda_Fact =" & _
        " @Original_Leyenda_Fact) AND (Mostrar_CodBarras = @Original_Mostrar_CodBarras) A" & _
        "ND (NombreComercial = @Original_NombreComercial) AND (NumeroConsecutivo = @Origi" & _
        "nal_NumeroConsecutivo) AND (NumeroContado = @Original_NumeroContado) AND (Numero" & _
        "Credito = @Original_NumeroCredito) AND (NumeroPuntoVenta = @Original_NumeroPunto" & _
        "Venta) AND (Tel_01 = @Original_Tel_01) AND (Tel_02 = @Original_Tel_02) AND (Unic" & _
        "oConsecutivo = @Original_UnicoConsecutivo)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cajeros", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajeros", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CambiaPrecioCredito", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CambiaPrecioCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ConsContado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ConsContado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ConsCredito", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ConsCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ConsPuntoVenta", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ConsPuntoVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contabilidad", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Empresa", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Empresa", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax_01", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax_01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax_02", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax_02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FormatoCheck", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormatoCheck", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Frase", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Frase", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Imp_Venta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Venta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Imprimir_en_Factura_Personalizada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imprimir_en_Factura_Personalizada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Intereses", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Intereses", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Leyenda_Fact", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Leyenda_Fact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Mostrar_CodBarras", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mostrar_CodBarras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreComercial", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreComercial", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroConsecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroConsecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroContado", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroContado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroCredito", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroPuntoVenta", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroPuntoVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tel_01", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tel_01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tel_02", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tel_02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnicoConsecutivo", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnicoConsecutivo", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO configuraciones(Cedula, Empresa, NombreComercial, Tel_01, Tel_02, Fax" & _
        "_01, Fax_02, Direccion, Imp_Venta, Frase, Cajeros, Logo, Intereses, UnicoConsecu" & _
        "tivo, NumeroConsecutivo, ConsContado, NumeroContado, ConsCredito, NumeroCredito," & _
        " ConsPuntoVenta, NumeroPuntoVenta, Imprimir_en_Factura_Personalizada, FormatoChe" & _
        "ck, Contabilidad, CambiaPrecioCredito, Mostrar_CodBarras, Leyenda_Fact) VALUES (" & _
        "@Cedula, @Empresa, @NombreComercial, @Tel_01, @Tel_02, @Fax_01, @Fax_02, @Direcc" & _
        "ion, @Imp_Venta, @Frase, @Cajeros, @Logo, @Intereses, @UnicoConsecutivo, @Numero" & _
        "Consecutivo, @ConsContado, @NumeroContado, @ConsCredito, @NumeroCredito, @ConsPu" & _
        "ntoVenta, @NumeroPuntoVenta, @Imprimir_en_Factura_Personalizada, @FormatoCheck, " & _
        "@Contabilidad, @CambiaPrecioCredito, @Mostrar_CodBarras, @Leyenda_Fact); SELECT " & _
        "Cedula, Empresa, NombreComercial, Tel_01, Tel_02, Fax_01, Fax_02, Direccion, Imp" & _
        "_Venta, Frase, Cajeros, Logo, Intereses, UnicoConsecutivo, NumeroConsecutivo, Co" & _
        "nsContado, NumeroContado, ConsCredito, NumeroCredito, ConsPuntoVenta, NumeroPunt" & _
        "oVenta, Imprimir_en_Factura_Personalizada, FormatoCheck, Contabilidad, CambiaPre" & _
        "cioCredito, Mostrar_CodBarras, Leyenda_Fact FROM configuraciones WHERE (Cedula =" & _
        " @Cedula)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 255, "Cedula"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 255, "Empresa"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreComercial", System.Data.SqlDbType.VarChar, 255, "NombreComercial"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tel_01", System.Data.SqlDbType.VarChar, 255, "Tel_01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tel_02", System.Data.SqlDbType.VarChar, 255, "Tel_02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax_01", System.Data.SqlDbType.VarChar, 255, "Fax_01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax_02", System.Data.SqlDbType.VarChar, 255, "Fax_02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 255, "Direccion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 8, "Imp_Venta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Frase", System.Data.SqlDbType.VarChar, 255, "Frase"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cajeros", System.Data.SqlDbType.Int, 4, "Cajeros"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Logo", System.Data.SqlDbType.VarBinary, 2147483647, "Logo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Int, 4, "Intereses"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnicoConsecutivo", System.Data.SqlDbType.Bit, 1, "UnicoConsecutivo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroConsecutivo", System.Data.SqlDbType.BigInt, 8, "NumeroConsecutivo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ConsContado", System.Data.SqlDbType.Bit, 1, "ConsContado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroContado", System.Data.SqlDbType.BigInt, 8, "NumeroContado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ConsCredito", System.Data.SqlDbType.Bit, 1, "ConsCredito"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroCredito", System.Data.SqlDbType.BigInt, 8, "NumeroCredito"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ConsPuntoVenta", System.Data.SqlDbType.Bit, 1, "ConsPuntoVenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroPuntoVenta", System.Data.SqlDbType.BigInt, 8, "NumeroPuntoVenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Imprimir_en_Factura_Personalizada", System.Data.SqlDbType.Bit, 1, "Imprimir_en_Factura_Personalizada"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FormatoCheck", System.Data.SqlDbType.Bit, 1, "FormatoCheck"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contabilidad", System.Data.SqlDbType.Bit, 1, "Contabilidad"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CambiaPrecioCredito", System.Data.SqlDbType.Bit, 1, "CambiaPrecioCredito"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Mostrar_CodBarras", System.Data.SqlDbType.Bit, 1, "Mostrar_CodBarras"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Leyenda_Fact", System.Data.SqlDbType.VarChar, 200, "Leyenda_Fact"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Cedula, Empresa, NombreComercial, Tel_01, Tel_02, Fax_01, Fax_02, Direccio" & _
        "n, Imp_Venta, Frase, Cajeros, Logo, Intereses, UnicoConsecutivo, NumeroConsecuti" & _
        "vo, ConsContado, NumeroContado, ConsCredito, NumeroCredito, ConsPuntoVenta, Nume" & _
        "roPuntoVenta, Imprimir_en_Factura_Personalizada, FormatoCheck, Contabilidad, Cam" & _
        "biaPrecioCredito, Mostrar_CodBarras, Leyenda_Fact FROM configuraciones"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE configuraciones SET Cedula = @Cedula, Empresa = @Empresa, NombreComercial " & _
        "= @NombreComercial, Tel_01 = @Tel_01, Tel_02 = @Tel_02, Fax_01 = @Fax_01, Fax_02" & _
        " = @Fax_02, Direccion = @Direccion, Imp_Venta = @Imp_Venta, Frase = @Frase, Caje" & _
        "ros = @Cajeros, Logo = @Logo, Intereses = @Intereses, UnicoConsecutivo = @UnicoC" & _
        "onsecutivo, NumeroConsecutivo = @NumeroConsecutivo, ConsContado = @ConsContado, " & _
        "NumeroContado = @NumeroContado, ConsCredito = @ConsCredito, NumeroCredito = @Num" & _
        "eroCredito, ConsPuntoVenta = @ConsPuntoVenta, NumeroPuntoVenta = @NumeroPuntoVen" & _
        "ta, Imprimir_en_Factura_Personalizada = @Imprimir_en_Factura_Personalizada, Form" & _
        "atoCheck = @FormatoCheck, Contabilidad = @Contabilidad, CambiaPrecioCredito = @C" & _
        "ambiaPrecioCredito, Mostrar_CodBarras = @Mostrar_CodBarras, Leyenda_Fact = @Leye" & _
        "nda_Fact WHERE (Cedula = @Original_Cedula) AND (Cajeros = @Original_Cajeros) AND" & _
        " (CambiaPrecioCredito = @Original_CambiaPrecioCredito) AND (ConsContado = @Origi" & _
        "nal_ConsContado) AND (ConsCredito = @Original_ConsCredito) AND (ConsPuntoVenta =" & _
        " @Original_ConsPuntoVenta) AND (Contabilidad = @Original_Contabilidad) AND (Dire" & _
        "ccion = @Original_Direccion) AND (Empresa = @Original_Empresa) AND (Fax_01 = @Or" & _
        "iginal_Fax_01) AND (Fax_02 = @Original_Fax_02) AND (FormatoCheck = @Original_For" & _
        "matoCheck) AND (Frase = @Original_Frase) AND (Imp_Venta = @Original_Imp_Venta) A" & _
        "ND (Imprimir_en_Factura_Personalizada = @Original_Imprimir_en_Factura_Personaliz" & _
        "ada) AND (Intereses = @Original_Intereses) AND (Leyenda_Fact = @Original_Leyenda" & _
        "_Fact) AND (Mostrar_CodBarras = @Original_Mostrar_CodBarras) AND (NombreComercia" & _
        "l = @Original_NombreComercial) AND (NumeroConsecutivo = @Original_NumeroConsecut" & _
        "ivo) AND (NumeroContado = @Original_NumeroContado) AND (NumeroCredito = @Origina" & _
        "l_NumeroCredito) AND (NumeroPuntoVenta = @Original_NumeroPuntoVenta) AND (Tel_01" & _
        " = @Original_Tel_01) AND (Tel_02 = @Original_Tel_02) AND (UnicoConsecutivo = @Or" & _
        "iginal_UnicoConsecutivo); SELECT Cedula, Empresa, NombreComercial, Tel_01, Tel_0" & _
        "2, Fax_01, Fax_02, Direccion, Imp_Venta, Frase, Cajeros, Logo, Intereses, UnicoC" & _
        "onsecutivo, NumeroConsecutivo, ConsContado, NumeroContado, ConsCredito, NumeroCr" & _
        "edito, ConsPuntoVenta, NumeroPuntoVenta, Imprimir_en_Factura_Personalizada, Form" & _
        "atoCheck, Contabilidad, CambiaPrecioCredito, Mostrar_CodBarras, Leyenda_Fact FRO" & _
        "M configuraciones WHERE (Cedula = @Cedula)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 255, "Cedula"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 255, "Empresa"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreComercial", System.Data.SqlDbType.VarChar, 255, "NombreComercial"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tel_01", System.Data.SqlDbType.VarChar, 255, "Tel_01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tel_02", System.Data.SqlDbType.VarChar, 255, "Tel_02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax_01", System.Data.SqlDbType.VarChar, 255, "Fax_01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax_02", System.Data.SqlDbType.VarChar, 255, "Fax_02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 255, "Direccion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 8, "Imp_Venta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Frase", System.Data.SqlDbType.VarChar, 255, "Frase"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cajeros", System.Data.SqlDbType.Int, 4, "Cajeros"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Logo", System.Data.SqlDbType.VarBinary, 2147483647, "Logo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Int, 4, "Intereses"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnicoConsecutivo", System.Data.SqlDbType.Bit, 1, "UnicoConsecutivo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroConsecutivo", System.Data.SqlDbType.BigInt, 8, "NumeroConsecutivo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ConsContado", System.Data.SqlDbType.Bit, 1, "ConsContado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroContado", System.Data.SqlDbType.BigInt, 8, "NumeroContado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ConsCredito", System.Data.SqlDbType.Bit, 1, "ConsCredito"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroCredito", System.Data.SqlDbType.BigInt, 8, "NumeroCredito"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ConsPuntoVenta", System.Data.SqlDbType.Bit, 1, "ConsPuntoVenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroPuntoVenta", System.Data.SqlDbType.BigInt, 8, "NumeroPuntoVenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Imprimir_en_Factura_Personalizada", System.Data.SqlDbType.Bit, 1, "Imprimir_en_Factura_Personalizada"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FormatoCheck", System.Data.SqlDbType.Bit, 1, "FormatoCheck"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contabilidad", System.Data.SqlDbType.Bit, 1, "Contabilidad"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CambiaPrecioCredito", System.Data.SqlDbType.Bit, 1, "CambiaPrecioCredito"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Mostrar_CodBarras", System.Data.SqlDbType.Bit, 1, "Mostrar_CodBarras"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Leyenda_Fact", System.Data.SqlDbType.VarChar, 200, "Leyenda_Fact"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cajeros", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajeros", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CambiaPrecioCredito", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CambiaPrecioCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ConsContado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ConsContado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ConsCredito", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ConsCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ConsPuntoVenta", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ConsPuntoVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contabilidad", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Empresa", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Empresa", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax_01", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax_01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax_02", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax_02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FormatoCheck", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormatoCheck", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Frase", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Frase", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Imp_Venta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Venta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Imprimir_en_Factura_Personalizada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imprimir_en_Factura_Personalizada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Intereses", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Intereses", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Leyenda_Fact", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Leyenda_Fact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Mostrar_CodBarras", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mostrar_CodBarras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreComercial", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreComercial", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroConsecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroConsecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroContado", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroContado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroCredito", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroPuntoVenta", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroPuntoVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tel_01", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tel_01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tel_02", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tel_02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnicoConsecutivo", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnicoConsecutivo", System.Data.DataRowVersion.Original, Nothing))
        '
        'FrmConfiguracion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(408, 277)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Name = "FrmConfiguracion"
        Me.Text = "Configuraci�n"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DsConfiguracion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Variable"

#End Region

    Private Sub FrmConfiguraci�n_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SqlConnection.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")

            Me.DaConfiguracion.Fill(Me.DsConfiguracion.configuraciones)
            Me.DsConfiguracion.configuraciones.InteresesColumn.DefaultValue = 0
            Me.DsConfiguracion.configuraciones.Imp_VentaColumn.DefaultValue = 0
            Me.DsConfiguracion.configuraciones.CajerosColumn.DefaultValue = 0
            Me.DsConfiguracion.configuraciones.NumeroConsecutivoColumn.DefaultValue = 0
            Me.DsConfiguracion.configuraciones.NumeroCreditoColumn.DefaultValue = 0
            Me.DsConfiguracion.configuraciones.NumeroContadoColumn.DefaultValue = 0
            DsConfiguracion.configuraciones.NumeroPuntoVentaColumn.DefaultValue = 0
            DsConfiguracion.configuraciones.NombreComercialColumn.DefaultValue = ""
            DsConfiguracion.configuraciones.Mostrar_CodBarrasColumn.DefaultValue = 0

            DsConfiguracion.configuraciones.Leyenda_FactColumn.DefaultValue = ""

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub

    Private Sub binding()

        'Me.TxtCajeros.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Cajeros"))
        Me.TxtInteres.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Intereses"))
        Me.TxtImpuesto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Imp_Venta"))
        Me.CheckBoxContado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.ConsContado"))
        Me.TextBoxNumCON.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NumeroContado"))
        Me.TextBoxNumPVE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NumeroPuntoVenta"))
        Me.CheckBoxCredito.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.ConsCredito"))
        Me.CheckBoxContadoPVE.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.ConsPuntoVenta"))
        Me.CheckBoxFacturaPersonalizada.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.Imprimir_en_Factura_Personalizada"))
        Me.CheckBoxCambiaPrecio.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuracion.CambiaPrecioCredito"))
        Me.CheckBoxUnico.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.UnicoConsecutivo"))
        Me.TextBoxNumCRE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NumeroCredito"))
        Me.TxtCedula.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Cedula"))
        Me.TxtEmpresa.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Empresa"))
        Me.TextBoxNumUnico.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.NumeroConsecutivo"))
        Me.VTFax1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Fax_01"))
        Me.VTFax2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Fax_02"))
        Me.VTTel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Tel_02"))
        Me.TxtDireccion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Direccion"))
        Me.TxtFrase.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Frase"))
        Me.VTTel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Tel_01"))
        Me.CBMostrar_Cod_Barras.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsConfiguracion, "configuraciones.Mostrar_CodBarras"))
        Me.txtLeyendaFactura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsConfiguracion, "configuraciones.Leyenda_Fact"))

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : nuevo()

            Case 2 : registrar()

            Case 3 : Me.Close()

        End Select
    End Sub
    Private Sub nuevo()
        Try
            Me.BindingContext(Me.DsConfiguracion, "Configuraciones").EndCurrentEdit()
            Me.BindingContext(Me.DsConfiguracion, "Configuraciones").AddNew()
            'TxtInteres.Text = "0.00"
            ' TxtImpuesto.Text = "0.00"
            'TxtCajeros.Text = "0.00"
            'TxtDolar.Text = "0.00"
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub registrar()

        If Me.SqlConnection.State <> Me.SqlConnection.State.Open Then Me.SqlConnection.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection.BeginTransaction
        Try
            Me.BindingContext(Me.DsConfiguracion, "Configuraciones").EndCurrentEdit()

            Me.SqlInsertCommand1.Transaction = Trans
            Me.SqlUpdateCommand1.Transaction = Trans

            Me.DaConfiguracion.Update(Me.DsConfiguracion.configuraciones)
            Trans.Commit()

            MsgBox("Los datos fueron ingresados correctamente")

        Catch ex As Exception
            'SqlConnection.BeginTransaction.Rollback()
            Trans.Rollback()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub TxtEmpresa_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtEmpresa.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub TxtCedula_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCedula.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub TxtCajeros_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCajeros.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub TxtDireccion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtDireccion.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub TxtDolar_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxNumCON.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub TxtFrase_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtFrase.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub TxtImpuesto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtImpuesto.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub TxtInteres_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtInteres.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub VTFax1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles VTFax1.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub VTFax2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles VTFax2.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub VTTel1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles VTTel1.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub VTTel2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles VTTel2.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub CheckBoxUnico_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxUnico.CheckedChanged
        If CheckBoxUnico.Checked = True Then
            Me.CheckBoxCredito.Enabled = False
            Me.CheckBoxContado.Enabled = False


            Me.CheckBoxCredito.Checked = False
            Me.CheckBoxContado.Checked = False
            Me.TextBoxNumUnico.Enabled = True
            Me.TextBoxNumCON.Enabled = False
            Me.TextBoxNumCRE.Enabled = False


        Else
            Me.CheckBoxCredito.Enabled = True
            Me.CheckBoxContado.Enabled = True
            Me.TextBoxNumUnico.Enabled = False
            Me.TextBoxNumCON.Enabled = True
            Me.TextBoxNumCRE.Enabled = True

            Me.CheckBoxCredito.Checked = True
            Me.CheckBoxContado.Checked = True

        End If
    End Sub

    Private Sub TabPage3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage3.Click

    End Sub
End Class
