﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfiguracionAvanzada
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbPersonalizar = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.nuEnviar = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.nuPasar = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rbEnviarTodas = New System.Windows.Forms.RadioButton()
        Me.rbEnviarNinguna = New System.Windows.Forms.RadioButton()
        Me.Btn_ActualizaContador = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nuEnviar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nuPasar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbPersonalizar)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.nuEnviar)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.nuPasar)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.rbEnviarTodas)
        Me.GroupBox1.Controls.Add(Me.rbEnviarNinguna)
        Me.GroupBox1.Controls.Add(Me.Btn_ActualizaContador)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(14, 22)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(386, 120)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Inicializar"
        Me.GroupBox1.Visible = False
        '
        'rbPersonalizar
        '
        Me.rbPersonalizar.AutoSize = True
        Me.rbPersonalizar.Location = New System.Drawing.Point(287, 20)
        Me.rbPersonalizar.Name = "rbPersonalizar"
        Me.rbPersonalizar.Size = New System.Drawing.Size(98, 17)
        Me.rbPersonalizar.TabIndex = 10
        Me.rbPersonalizar.TabStop = True
        Me.rbPersonalizar.Text = "Personalizar."
        Me.rbPersonalizar.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(325, 55)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(11, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "."
        '
        'nuEnviar
        '
        Me.nuEnviar.Enabled = False
        Me.nuEnviar.Location = New System.Drawing.Point(279, 53)
        Me.nuEnviar.Name = "nuEnviar"
        Me.nuEnviar.Size = New System.Drawing.Size(42, 20)
        Me.nuEnviar.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(176, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "facturas, enviar"
        '
        'nuPasar
        '
        Me.nuPasar.Enabled = False
        Me.nuPasar.Location = New System.Drawing.Point(128, 52)
        Me.nuPasar.Name = "nuPasar"
        Me.nuPasar.Size = New System.Drawing.Size(42, 20)
        Me.nuPasar.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(63, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Con cada"
        '
        'rbEnviarTodas
        '
        Me.rbEnviarTodas.AutoSize = True
        Me.rbEnviarTodas.Location = New System.Drawing.Point(164, 20)
        Me.rbEnviarTodas.Name = "rbEnviarTodas"
        Me.rbEnviarTodas.Size = New System.Drawing.Size(100, 17)
        Me.rbEnviarTodas.TabIndex = 4
        Me.rbEnviarTodas.TabStop = True
        Me.rbEnviarTodas.Text = "Enviar todas."
        Me.rbEnviarTodas.UseVisualStyleBackColor = True
        '
        'rbEnviarNinguna
        '
        Me.rbEnviarNinguna.AutoSize = True
        Me.rbEnviarNinguna.Location = New System.Drawing.Point(6, 20)
        Me.rbEnviarNinguna.Name = "rbEnviarNinguna"
        Me.rbEnviarNinguna.Size = New System.Drawing.Size(133, 17)
        Me.rbEnviarNinguna.TabIndex = 3
        Me.rbEnviarNinguna.TabStop = True
        Me.rbEnviarNinguna.Text = "No enviar ninguna."
        Me.rbEnviarNinguna.UseVisualStyleBackColor = True
        '
        'Btn_ActualizaContador
        '
        Me.Btn_ActualizaContador.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.Btn_ActualizaContador.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Btn_ActualizaContador.FlatAppearance.BorderSize = 2
        Me.Btn_ActualizaContador.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_ActualizaContador.ForeColor = System.Drawing.Color.White
        Me.Btn_ActualizaContador.Location = New System.Drawing.Point(144, 83)
        Me.Btn_ActualizaContador.Name = "Btn_ActualizaContador"
        Me.Btn_ActualizaContador.Size = New System.Drawing.Size(85, 31)
        Me.Btn_ActualizaContador.TabIndex = 2
        Me.Btn_ActualizaContador.Text = "Actualizar"
        Me.Btn_ActualizaContador.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(415, 15)
        Me.Panel1.TabIndex = 6
        '
        'FrmConfiguracionAvanzada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(415, 155)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.Name = "FrmConfiguracionAvanzada"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configuración Avanzada"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nuEnviar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nuPasar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_ActualizaContador As System.Windows.Forms.Button
    Friend WithEvents Label6 As Windows.Forms.Label
    Friend WithEvents nuEnviar As Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As Windows.Forms.Label
    Friend WithEvents nuPasar As Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents rbEnviarTodas As Windows.Forms.RadioButton
    Friend WithEvents rbEnviarNinguna As Windows.Forms.RadioButton
    Friend WithEvents Panel1 As Windows.Forms.Panel
    Friend WithEvents rbPersonalizar As Windows.Forms.RadioButton
End Class
