﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class FrmConfiguracionAvanzada


	Dim Enviar As Integer = 0
	Dim Pasar As Integer = 0
	Dim _IdUsuario As String = "0"
	Public Sub New(IdUsuario As String)
		MyBase.New()
		InitializeComponent() 'This call is required by the Windows Form Designer.
		_IdUsuario = IdUsuario
	End Sub
	Private Sub FrmConfiguracionAvanzada_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Dim Dt As DataTable
		Dim Cn As New SqlConnection(GetSetting("SeeSOFT", "Seepos", "conexion"))
		Dim Da As New SqlDataAdapter
		Dim Cmd As New SqlCommand

		ObtenerPermisos()
		ObtenerConfiguracion()

	End Sub
	Function ObtenerPermisos() As Boolean
		Try
			Dim dtPermisos As New DataTable
			Dim UsaAnalisis As Boolean = False
			Dim TipoUsuario As Boolean
			cFunciones.Llenar_Tabla_Generico("Select top 1 UsaAnalisisVenta from Configuraciones", dtPermisos)

			If dtPermisos.Rows.Count > 0 Then
				UsaAnalisis = dtPermisos.Rows(0).ItemArray(0)
			End If

			dtPermisos.Clear()
			dtPermisos.Columns.Clear()
			dtPermisos.Rows.Clear()
			cFunciones.Llenar_Tabla_Generico("Select Gerencial from Usuarios where Id_Usuario='" & _IdUsuario & "'", dtPermisos, GetSetting("SeeSOFT", "Seguridad", "CONEXION"))
			If dtPermisos.Rows.Count > 0 Then
				TipoUsuario = dtPermisos.Rows(0).ItemArray(0)
			End If

			If UsaAnalisis = True And TipoUsuario = True Then
				GroupBox1.Visible = True
			End If
		Catch ex As Exception

		End Try
	End Function
	Sub ObtenerConfiguracion()  'OBTIENE LA CONFIGURACION DISPONIBLE DE LA TABLA TB_CONTADOR, 0 PARA RB NO ENVIAR NINGUNA, 1 PARA RB ENVIAR TODAS, 2 PARA HABILITAR CONFIGURACION PERSONALIZADA
		Try
			Dim dtConfiguracion As New DataTable
			Dim Opcion As Integer = 1
			cFunciones.Llenar_Tabla_Generico("SELECT Contador as Pasar, ContadorTemporal, Opcion, Enviar FROM [dbo].[Tb_Contador]", dtConfiguracion)

			If dtConfiguracion.Rows.Count > 0 Then
				Enviar = dtConfiguracion.Rows(0).Item("Enviar")
				Pasar = dtConfiguracion.Rows(0).Item("Pasar")
				Opcion = dtConfiguracion.Rows(0).Item("Opcion")

				Select Case Opcion
					Case 0
						rbEnviarNinguna.Checked = True
						nuPasar.Value = 0
						nuEnviar.Value = 0
					Case 1
						rbEnviarTodas.Checked = True
						nuPasar.Value = 0
						nuEnviar.Value = 0
					Case -1
						rbPersonalizar.Checked = True
						nuPasar.Enabled = True
						nuEnviar.Enabled = True
						nuPasar.Value = Pasar
						nuEnviar.Value = Enviar
					Case Else
						rbEnviarTodas.Checked = True
						nuPasar.Value = 0
						nuEnviar.Value = 0
				End Select
			End If
		Catch ex As Exception
			MsgBox("Error obtiendo información de la tabla contador. " & ex.Message)
		End Try
	End Sub

	Sub ActualizarContador()
		Try
			Dim Cn As New SqlConnection(GetSetting("SeeSOFT", "Seepos", "conexion"))
			Dim dtExiste As New DataTable
			Dim cmd As SqlCommand = Cn.CreateCommand()
			Dim Opcion As Integer = 0

			cFunciones.Llenar_Tabla_Generico("Select count(*) from tb_Contador", dtExiste)
			If rbEnviarNinguna.Checked Then 'NO ENVIA NINGUNA, PONE TRIBUTABLE EN FALSE EN VENTAS
				Opcion = 0
			ElseIf rbEnviarTodas.Checked Then ' ENVIA TODAS, PONE TRIBUTABLE EN TRUE EN VENTAS
				Opcion = 1
			ElseIf rbPersonalizar.Checked Then ' EN BASE A LA CONFIGURACION, ENVIARA SOLO ALGUNAS FACTURAS PONIENDO TRIBUTABLE FALSE O TRUE
				Opcion = -1
			End If

			If dtExiste.Rows.Count > 0 Then
				cmd.CommandText = "Update [dbo].[Tb_Contador] set [Contador]= " & nuPasar.Value & ", [ContadorTemporal]=1, Opcion=" & Opcion & ", Enviar=" & nuEnviar.Value & " "
			Else
				cmd.CommandText = "insert into [dbo].[Tb_Contador] (Contador,ContadorTemporal, Opcion, Enviar) values  (" & nuPasar.Value & ", 1," & Opcion & "," & nuEnviar.Value & " )"
			End If

			Cn.Open()
			Dim value As Object = cmd.ExecuteScalar()

		Catch ex As Exception
			MsgBox("Error actualizando la información. " & ex.Message)
		End Try

	End Sub

	Private Sub Btn_ActualizaContador_Click(sender As Object, e As EventArgs) Handles Btn_ActualizaContador.Click
		ActualizarContador()
		MsgBox("Se actualizo el envio de facturas.", MsgBoxStyle.Information, "Alerta")
	End Sub


	Private Sub rbEnviarNinguna_CheckedChanged(sender As Object, e As EventArgs) Handles rbEnviarNinguna.CheckedChanged
		If rbEnviarNinguna.Checked Then
			nuPasar.Enabled = False
			nuEnviar.Enabled = False
			nuPasar.Value = 0
			nuEnviar.Value = 0
		End If
	End Sub

	Private Sub rbEnviarTodas_CheckedChanged(sender As Object, e As EventArgs) Handles rbEnviarTodas.CheckedChanged
		If rbEnviarTodas.Checked Then
			nuPasar.Enabled = False
			nuEnviar.Enabled = False
			nuPasar.Value = 0
			nuEnviar.Value = 0
		End If
	End Sub

	Private Sub rbPersonalizar_CheckedChanged(sender As Object, e As EventArgs) Handles rbPersonalizar.CheckedChanged
		If rbPersonalizar.Checked Then
			nuPasar.Enabled = True
			nuEnviar.Enabled = True
			nuPasar.Value = Pasar
			nuEnviar.Value = Enviar
		End If
	End Sub


End Class