Imports System.Data
Public Class FrmMoneda
    Inherits FrmPlantilla
    Dim NuevaConexion As String
    Dim usua As Object

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Usuario_Parametro As Object, Optional ByVal Conexion As String = "")
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro

        NuevaConexion = Conexion
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub



    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents DaMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtCompra As System.Windows.Forms.TextBox
    Friend WithEvents TxtSimbolo As System.Windows.Forms.TextBox
    Friend WithEvents TxtVenta As System.Windows.Forms.TextBox
    Friend WithEvents TxtMoneda As System.Windows.Forms.TextBox
    Friend WithEvents TxtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TextUsuario As System.Windows.Forms.TextBox
    Friend WithEvents LabelUsuario As System.Windows.Forms.Label
    Friend WithEvents BHistorico As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents DsMoneda1 As LcPymes_5._2.DataSetMoneda
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmMoneda))
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.DaMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dtFecha = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.BHistorico = New System.Windows.Forms.Button
        Me.TxtCompra = New System.Windows.Forms.TextBox
        Me.TxtSimbolo = New System.Windows.Forms.TextBox
        Me.TxtVenta = New System.Windows.Forms.TextBox
        Me.TxtMoneda = New System.Windows.Forms.TextBox
        Me.TxtCodigo = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.TextUsuario = New System.Windows.Forms.TextBox
        Me.LabelUsuario = New System.Windows.Forms.Label
        Me.DsMoneda1 = New LcPymes_5._2.DataSetMoneda
        Me.GroupBox1.SuspendLayout()
        CType(Me.DsMoneda1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        Me.ToolBarBuscar.ImageIndex = 9
        Me.ToolBarBuscar.Text = "Editar"
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 148)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(442, 52)
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(442, 32)
        Me.TituloModulo.Text = "                             Registro de Monedas"
        '
        'DataNavigator
        '
        Me.DataNavigator.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.DataMember = "Moneda"
        Me.DataNavigator.DataSource = Me.DsMoneda1
        Me.DataNavigator.Location = New System.Drawing.Point(248, 176)
        Me.DataNavigator.Name = "DataNavigator"
        Me.DataNavigator.Size = New System.Drawing.Size(207, 21)
        Me.DataNavigator.Text = ""
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=SEESERVER;packet size=4096;integrated security=SSPI;data source=""." & _
        """;persist security info=False;initial catalog=Seguridad"
        '
        'DaMoneda
        '
        Me.DaMoneda.DeleteCommand = Me.SqlDeleteCommand1
        Me.DaMoneda.InsertCommand = Me.SqlInsertCommand1
        Me.DaMoneda.SelectCommand = Me.SqlSelectCommand1
        Me.DaMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        Me.DaMoneda.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Moneda WHERE (CodMoneda = @Original_CodMoneda) AND (MonedaNombre = @O" & _
        "riginal_MonedaNombre) AND (Simbolo = @Original_Simbolo) AND (ValorCompra = @Orig" & _
        "inal_ValorCompra) AND (ValorVenta = @Original_ValorVenta)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Moneda(CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo) VAL" & _
        "UES (@CodMoneda, @MonedaNombre, @ValorCompra, @ValorVenta, @Simbolo); SELECT Cod" & _
        "Moneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda WHERE (CodMon" & _
        "eda = @CodMoneda)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Moneda SET CodMoneda = @CodMoneda, MonedaNombre = @MonedaNombre, ValorComp" & _
        "ra = @ValorCompra, ValorVenta = @ValorVenta, Simbolo = @Simbolo WHERE (CodMoneda" & _
        " = @Original_CodMoneda) AND (MonedaNombre = @Original_MonedaNombre) AND (Simbolo" & _
        " = @Original_Simbolo) AND (ValorCompra = @Original_ValorCompra) AND (ValorVenta " & _
        "= @Original_ValorVenta); SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta" & _
        ", Simbolo FROM Moneda WHERE (CodMoneda = @CodMoneda)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtFecha)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.BHistorico)
        Me.GroupBox1.Controls.Add(Me.TxtCompra)
        Me.GroupBox1.Controls.Add(Me.TxtSimbolo)
        Me.GroupBox1.Controls.Add(Me.TxtVenta)
        Me.GroupBox1.Controls.Add(Me.TxtMoneda)
        Me.GroupBox1.Controls.Add(Me.TxtCodigo)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 33)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(440, 104)
        Me.GroupBox1.TabIndex = 64
        Me.GroupBox1.TabStop = False
        '
        'dtFecha
        '
        Me.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.dtFecha.Location = New System.Drawing.Point(224, 80)
        Me.dtFecha.MaxDate = New Date(2090, 1, 1, 0, 0, 0, 0)
        Me.dtFecha.MinDate = New Date(1999, 1, 1, 0, 0, 0, 0)
        Me.dtFecha.Name = "dtFecha"
        Me.dtFecha.Size = New System.Drawing.Size(96, 20)
        Me.dtFecha.TabIndex = 74
        Me.dtFecha.Value = New Date(2007, 11, 29, 0, 0, 0, 0)
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Location = New System.Drawing.Point(224, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Fecha"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BHistorico
        '
        Me.BHistorico.Location = New System.Drawing.Point(328, 72)
        Me.BHistorico.Name = "BHistorico"
        Me.BHistorico.Size = New System.Drawing.Size(40, 24)
        Me.BHistorico.TabIndex = 5
        Me.BHistorico.Text = "Ver"
        '
        'TxtCompra
        '
        Me.TxtCompra.Location = New System.Drawing.Point(8, 80)
        Me.TxtCompra.Name = "TxtCompra"
        Me.TxtCompra.TabIndex = 2
        Me.TxtCompra.Text = ""
        '
        'TxtSimbolo
        '
        Me.TxtSimbolo.Location = New System.Drawing.Point(376, 80)
        Me.TxtSimbolo.Name = "TxtSimbolo"
        Me.TxtSimbolo.Size = New System.Drawing.Size(56, 20)
        Me.TxtSimbolo.TabIndex = 4
        Me.TxtSimbolo.Text = ""
        '
        'TxtVenta
        '
        Me.TxtVenta.Location = New System.Drawing.Point(120, 80)
        Me.TxtVenta.Name = "TxtVenta"
        Me.TxtVenta.TabIndex = 3
        Me.TxtVenta.Text = ""
        '
        'TxtMoneda
        '
        Me.TxtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtMoneda.Location = New System.Drawing.Point(120, 32)
        Me.TxtMoneda.Name = "TxtMoneda"
        Me.TxtMoneda.Size = New System.Drawing.Size(312, 20)
        Me.TxtMoneda.TabIndex = 1
        Me.TxtMoneda.Text = ""
        '
        'TxtCodigo
        '
        Me.TxtCodigo.Location = New System.Drawing.Point(8, 32)
        Me.TxtCodigo.Name = "TxtCodigo"
        Me.TxtCodigo.TabIndex = 0
        Me.TxtCodigo.Text = ""
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Location = New System.Drawing.Point(376, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Simbolo"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Location = New System.Drawing.Point(120, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Valor de Venta"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Location = New System.Drawing.Point(8, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "C�digo"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Location = New System.Drawing.Point(8, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Valor de Compra"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Location = New System.Drawing.Point(120, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(312, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre de Moneda"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(304, 160)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(56, 13)
        Me.Label36.TabIndex = 174
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextUsuario
        '
        Me.TextUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextUsuario.Location = New System.Drawing.Point(360, 160)
        Me.TextUsuario.Name = "TextUsuario"
        Me.TextUsuario.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.TextUsuario.Size = New System.Drawing.Size(80, 13)
        Me.TextUsuario.TabIndex = 0
        Me.TextUsuario.Text = ""
        '
        'LabelUsuario
        '
        Me.LabelUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.LabelUsuario.Location = New System.Drawing.Point(304, 144)
        Me.LabelUsuario.Name = "LabelUsuario"
        Me.LabelUsuario.Size = New System.Drawing.Size(137, 16)
        Me.LabelUsuario.TabIndex = 173
        '
        'DsMoneda1
        '
        Me.DsMoneda1.DataSetName = "DataSetMoneda"
        Me.DsMoneda1.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'FrmMoneda
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(442, 200)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.TextUsuario)
        Me.Controls.Add(Me.LabelUsuario)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmMoneda"
        Me.Text = "Moneda"
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.LabelUsuario, 0)
        Me.Controls.SetChildIndex(Me.TextUsuario, 0)
        Me.Controls.SetChildIndex(Me.Label36, 0)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DsMoneda1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub FrmMoneda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "CONEXION"), NuevaConexion)
        Me.DaMoneda.Fill(Me.DsMoneda1, "Moneda")
        Me.ToolBar1.Buttons(4).Visible = False
        desactivaCampos()
        Binding()
        DataNavigator.Enabled = True
        dtFecha.Value = Now.Date
        TextUsuario.Focus()
    End Sub

    'Private Sub TxtCodigo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCodigo.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        SendKeys.Send("{TAB}")
    '    End If
    'End Sub

    'Private Sub TxtCompra_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCompra.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        SendKeys.Send("{TAB}")
    '    End If
    'End Sub

    'Private Sub TxtMoneda_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtMoneda.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        SendKeys.Send("{Tab}")
    '    End If
    'End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
        PMU = VSM(usua.Cedula, Me.Name) 'Carga los privilegios del usuario con el modulo

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0 : Nuevo()
            Case 1 : Editar()
            Case 2 : If PMU.Update Then Registra() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Delete Then Me.EliminarDatos(Me.DaMoneda, Me.DsMoneda1, Me.DsMoneda1.Moneda.ToString) Else MsgBox("No tiene permiso para eliminar o anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Update Then Me.RegistrarDatos(Me.DaMoneda, Me.DsMoneda1, Me.DsMoneda1.Moneda.ToString) Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : Me.Cerrar()
        End Select
    End Sub

    'Define dataset y tablas para crear  un nuevo campo
    Function Nuevo()
        Try
            If Me.ToolBarNuevo.Text = "Nuevo" Then
                Me.ToolBarNuevo.Text = "Cancel"
                Me.ToolBarNuevo.ImageIndex = 4
                Me.ToolBarBuscar.Enabled = False
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarRegistrar.Enabled = True

                Me.BindingContext(Me.DsMoneda1, "Moneda").EndCurrentEdit()
                Me.BindingContext(Me.DsMoneda1, "Moneda").AddNew()
                activaCampos()
                DataNavigator.Enabled = False

            Else
                Me.ToolBarNuevo.Text = "Nuevo"
                Me.ToolBarNuevo.ImageIndex = 0
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Me.ToolBarRegistrar.Enabled = False
                desactivaCampos()
                Me.BindingContext(Me.DsMoneda1, "Moneda").CancelCurrentEdit()
                DataNavigator.Enabled = True
            End If

        Catch ex As Exception
            Dim mesg As String = "Problemas al hacer un Usuario Nuevo." & vbCrLf & _
                                 "Intente Iniciar el formulario Otra vez, " & vbCrLf & _
                                 "si el problema persiste comuniquelo al 'Administrador del Sistema'" & vbCrLf & _
                                 "'" & ex.Message & "'"
            MsgBox(mesg, MsgBoxStyle.Critical, "Atenci�n")
        End Try
    End Function


    Function Editar()
        Try
            If Me.ToolBarBuscar.Text = "Editar" Then
                Me.ToolBarBuscar.Text = "Cancel"
                Me.ToolBarBuscar.ImageIndex = 4
                Me.ToolBarNuevo.Enabled = False
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarRegistrar.Enabled = True
                activaCampos()
                DataNavigator.Enabled = False
                TxtCompra.Focus()
            Else
                Me.BindingContext(Me.DsMoneda1, "Moneda").CancelCurrentEdit()
                Me.ToolBarBuscar.Text = "Editar"
                Me.ToolBarBuscar.ImageIndex = 9
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Me.ToolBarRegistrar.Enabled = False
                DataNavigator.Enabled = True
            End If

        Catch ex As Exception
            Dim mesg As String = "Problemas al Editar un Usuario." & vbCrLf & _
                                 "Intente Iniciar el formulario Otra vez, " & vbCrLf & _
                                 "si el problema persiste comuniquelo al 'Administrador del Sistema'" & vbCrLf & _
                                 "'" & ex.Message & "'"
            MsgBox(mesg, MsgBoxStyle.Critical, "Atenci�n")
        End Try
    End Function


    Function Registra()
        Try
            Me.BindingContext(Me.DsMoneda1, "Moneda").EndCurrentEdit()
            Me.DaMoneda.Update(Me.DsMoneda1.Moneda)
            Me.DsMoneda1.GetChanges()

            Historico()

            desactivaCampos()
            DataNavigator.Enabled = True

            'toolBar
            Me.ToolBarNuevo.Text = "Nuevo"
            Me.ToolBarNuevo.ImageIndex = 0
            Me.ToolBarBuscar.Text = "Editar"
            Me.ToolBarBuscar.ImageIndex = 9
            Me.ToolBarBuscar.Enabled = True
            Me.ToolBarNuevo.Enabled = True
            Me.ToolBarRegistrar.Enabled = False
            Me.ToolBarEliminar.Enabled = True

        Catch ex As SystemException
            Dim mesg As String = "Problemas al Registrar un Usuario." & vbCrLf & _
                                 "Intente Iniciar el formulario Otra vez, " & vbCrLf & _
                                 "si el problema persiste comuniquelo al 'Administrador del Sistema'" & vbCrLf & _
                                 "'" & ex.Message & "'"
            MsgBox(mesg, MsgBoxStyle.Critical, "Atenci�n")
        End Try
    End Function


    Private Sub desactivaCampos()
        TxtCodigo.Enabled = False
        TxtCompra.Enabled = False
        TxtMoneda.Enabled = False
        TxtSimbolo.Enabled = False
        TxtVenta.Enabled = False
        dtFecha.Enabled = False
    End Sub


    Private Sub activaCampos()
        TxtCodigo.Enabled = True
        TxtCompra.Enabled = True
        TxtMoneda.Enabled = True
        TxtSimbolo.Enabled = True
        TxtVenta.Enabled = True
        dtFecha.Enabled = True
    End Sub


    Function Binding()
        Me.TxtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsMoneda1, "Moneda.CodMoneda"))
        Me.TxtMoneda.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsMoneda1, "Moneda.MonedaNombre"))
        Me.TxtCompra.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsMoneda1, "Moneda.ValorCompra"))
        Me.TxtVenta.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsMoneda1, "Moneda.ValorVenta"))
        Me.TxtSimbolo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsMoneda1, "Moneda.Simbolo"))
    End Function


    Private Sub Historico()
        Dim Conecta As New Conexion
        Dim sqlconexion As New SqlClient.SqlConnection
        Dim Resultado As Double
        Try
            sqlconexion = Conecta.Conectar("Seguridad")
            Resultado = CDbl(Conecta.SlqExecuteScalar(sqlconexion, "Select IDs from HistoricoMoneda where Id_Moneda = " & Me.BindingContext(Me.DsMoneda1, "Moneda").Current("CodMoneda") & " AND CAST(fecha AS DATE) = '" & Format(dtFecha.Value.Date, "dd/MM/yyyy") & "'"))
            Conecta.DesConectar(sqlconexion)
            If Resultado = 0 Then
                Conecta.AddNewRecord("HistoricoMoneda", "Id_Moneda,ValorCompra,ValorVenta,Usuario,Fecha", Me.BindingContext(Me.DsMoneda1, "Moneda").Current("CodMoneda") & "," & Me.BindingContext(Me.DsMoneda1, "Moneda").Current("ValorCompra") & "," & Me.BindingContext(Me.DsMoneda1, "Moneda").Current("ValorVenta") & ",'" & LabelUsuario.Text & "','" & dtFecha.Value.Date & "'", "Seguridad")
            Else
                If MsgBox("Ya se registro un tipo de cambio para esta fecha, �Desea actualizar con este monto?", MsgBoxStyle.YesNo, "Atenci�n...") = MsgBoxResult.Yes Then
                    Conecta.UpdateRecords("HistoricoMoneda", "ValorCompra = " & Me.BindingContext(Me.DsMoneda1, "Moneda").Current("ValorCompra") & ",ValorVenta = " & Me.BindingContext(Me.DsMoneda1, "Moneda").Current("ValorVenta") & ", Usuario = 'up" & LabelUsuario.Text & "' ", "Id_Moneda =  " & Me.BindingContext(Me.DsMoneda1, "Moneda").Current("CodMoneda") & " AND CAST(Fecha AS DATE) = '" & Format(dtFecha.Value.Date, "dd/MM/yyyy") & "'", "Seguridad")
                End If

                dtFecha.Focus()
            End If

        Catch ex As Exception
            MsgBox("Favor Comunicar el siguiente Error a su Empresa Proveedora de Software.:" & vbCrLf & ex.Message, MsgBoxStyle.Critical, "Alerta...")
        Finally

        End Try
    End Sub


#Region "Validaci�n Usuario"
    Private Sub TextUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextUsuario.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            Try
                Dim sql As String
                Dim clsConexion As New Conexion
                Dim cnnConexion As New SqlClient.SqlConnection
                Dim rstReader As SqlClient.SqlDataReader

                cnnConexion = clsConexion.Conectar("Seguridad")
                sql = " SELECT Nombre FROM Usuarios WHERE Clave_Interna ='" & TextUsuario.Text & "'"
                rstReader = clsConexion.GetRecorset(cnnConexion, sql)

                If rstReader.Read() = False Then
                    MsgBox("Usuario Incorrecto", MsgBoxStyle.Critical, "Contabilidad")
                    LabelUsuario.Text = Nothing
                    ToolBarNuevo.Enabled = False
                    ToolBarBuscar.Enabled = False
                    ToolBarRegistrar.Enabled = False
                    ToolBarEliminar.Enabled = False
                    TextUsuario.Focus()
                Else
                    LabelUsuario.Text = rstReader.Item("Nombre")
                    ToolBarNuevo.Enabled = True
                    ToolBarBuscar.Enabled = True
                    ToolBarRegistrar.Enabled = True
                    ToolBarEliminar.Enabled = True
                    TxtCompra.Focus()
                End If
                clsConexion.DesConectar(cnnConexion)

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Seguridad - Entrada _Usuario")
                Console.WriteLine(ex.StackTrace)
            End Try
        End If
    End Sub
#End Region

    Private Sub BHistorico_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BHistorico.Click
        Dim Historico As New frmhistorico
        Historico.Moneda = CInt(TxtCodigo.Text)
        Historico.NombreMoneda = TxtMoneda.Text
        Historico.Label15.Text = "HISTORICO MONEDA " & TxtMoneda.Text
        Historico.Show()
    End Sub

    Private Sub TxtVenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtVenta.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            dtFecha.Focus()
        End If
    End Sub
End Class
