
Public Class frmhistorico
    Inherits System.Windows.Forms.Form

    Public Moneda As Integer
    Public NombreMoneda As String

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Grid As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Fecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Usuario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdapterHistorico As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents ValorCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ValorVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DsHistoricoMoneda1 As DsHistoricoMoneda
    Friend WithEvents BImprimir As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label15 = New System.Windows.Forms.Label
        Me.Grid = New DevExpress.XtraGrid.GridControl
        Me.DsHistoricoMoneda1 = New DsHistoricoMoneda
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.Fecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ValorCompra = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ValorVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Usuario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.AdapterHistorico = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.BImprimir = New DevExpress.XtraEditors.SimpleButton
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsHistoricoMoneda1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(0, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(752, 32)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Historico Moneda"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Grid
        '
        Me.Grid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Grid.DataMember = "HistoricoMoneda"
        Me.Grid.DataSource = Me.DsHistoricoMoneda1
        '
        'Grid.EmbeddedNavigator
        '
        Me.Grid.EmbeddedNavigator.Name = ""
        Me.Grid.Location = New System.Drawing.Point(0, 40)
        Me.Grid.MainView = Me.GridView1
        Me.Grid.Name = "Grid"
        Me.Grid.Size = New System.Drawing.Size(752, 344)
        Me.Grid.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.Grid.TabIndex = 5
        Me.Grid.Text = "GridControl1"
        '
        'DsHistoricoMoneda1
        '
        Me.DsHistoricoMoneda1.DataSetName = "DsHistoricoMoneda"
        Me.DsHistoricoMoneda1.Locale = New System.Globalization.CultureInfo("es-ES")
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Fecha, Me.ValorCompra, Me.ValorVenta, Me.Usuario})
        Me.GridView1.GroupPanelText = "Arrastre la categor�a aqu� para agrupar de acuerdo a ella"
        Me.GridView1.Name = "GridView1"
        '
        'Fecha
        '
        Me.Fecha.Caption = "Fecha"
        Me.Fecha.DisplayFormat.FormatString = "dddd, dd' de 'MMMM' de 'yyyy"
        Me.Fecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.Fecha.FieldName = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Fecha.SortIndex = 0
        Me.Fecha.SortOrder = DevExpress.Data.ColumnSortOrder.Descending
        Me.Fecha.VisibleIndex = 0
        Me.Fecha.Width = 200
        '
        'ValorCompra
        '
        Me.ValorCompra.Caption = "Valor Compra"
        Me.ValorCompra.DisplayFormat.FormatString = "#,#0.00"
        Me.ValorCompra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.ValorCompra.FieldName = "ValorCompra"
        Me.ValorCompra.Name = "ValorCompra"
        Me.ValorCompra.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.ValorCompra.VisibleIndex = 1
        Me.ValorCompra.Width = 185
        '
        'ValorVenta
        '
        Me.ValorVenta.Caption = "Valor Venta"
        Me.ValorVenta.DisplayFormat.FormatString = "#,#0.00"
        Me.ValorVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.ValorVenta.FieldName = "ValorVenta"
        Me.ValorVenta.Name = "ValorVenta"
        Me.ValorVenta.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.ValorVenta.VisibleIndex = 2
        Me.ValorVenta.Width = 185
        '
        'Usuario
        '
        Me.Usuario.Caption = "Usuario"
        Me.Usuario.FieldName = "Usuario"
        Me.Usuario.Name = "Usuario"
        Me.Usuario.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Usuario.VisibleIndex = 3
        Me.Usuario.Width = 192
        '
        'AdapterHistorico
        '
        Me.AdapterHistorico.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterHistorico.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterHistorico.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterHistorico.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "HistoricoMoneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("IDs", "IDs"), New System.Data.Common.DataColumnMapping("Id_Moneda", "Id_Moneda"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha")})})
        Me.AdapterHistorico.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM HistoricoMoneda WHERE (IDs = @Original_IDs) AND (Fecha = @Original_Fe" & _
        "cha) AND (Id_Moneda = @Original_Id_Moneda) AND (Usuario = @Original_Usuario) AND" & _
        " (ValorCompra = @Original_ValorCompra) AND (ValorVenta = @Original_ValorVenta)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IDs", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IDs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Moneda", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=Seguridad"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO HistoricoMoneda(Id_Moneda, ValorCompra, ValorVenta, Usuario, Fecha) V" & _
        "ALUES (@Id_Moneda, @ValorCompra, @ValorVenta, @Usuario, @Fecha); SELECT IDs, Id_" & _
        "Moneda, ValorCompra, ValorVenta, Usuario, Fecha FROM HistoricoMoneda WHERE (IDs " & _
        "= @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Moneda", System.Data.SqlDbType.BigInt, 8, "Id_Moneda"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT IDs, Id_Moneda, ValorCompra, ValorVenta, Usuario, Fecha FROM HistoricoMone" & _
        "da"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE HistoricoMoneda SET Id_Moneda = @Id_Moneda, ValorCompra = @ValorCompra, Va" & _
        "lorVenta = @ValorVenta, Usuario = @Usuario, Fecha = @Fecha WHERE (IDs = @Origina" & _
        "l_IDs) AND (Fecha = @Original_Fecha) AND (Id_Moneda = @Original_Id_Moneda) AND (" & _
        "Usuario = @Original_Usuario) AND (ValorCompra = @Original_ValorCompra) AND (Valo" & _
        "rVenta = @Original_ValorVenta); SELECT IDs, Id_Moneda, ValorCompra, ValorVenta, " & _
        "Usuario, Fecha FROM HistoricoMoneda WHERE (IDs = @IDs)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Moneda", System.Data.SqlDbType.BigInt, 8, "Id_Moneda"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IDs", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IDs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Moneda", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IDs", System.Data.SqlDbType.BigInt, 8, "IDs"))
        '
        'BImprimir
        '
        Me.BImprimir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BImprimir.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
        Me.BImprimir.ImageIndex = 1
        Me.BImprimir.Location = New System.Drawing.Point(8, 392)
        Me.BImprimir.Name = "BImprimir"
        Me.BImprimir.Size = New System.Drawing.Size(112, 24)
        Me.BImprimir.TabIndex = 76
        Me.BImprimir.Text = "Imprimir"
        '
        'frmhistorico
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(752, 422)
        Me.Controls.Add(Me.BImprimir)
        Me.Controls.Add(Me.Grid)
        Me.Controls.Add(Me.Label15)
        Me.Name = "frmhistorico"
        Me.Text = "Historico Moneda"
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsHistoricoMoneda1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmhistorico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Seguridad", "CONEXION")
        Dim cfunciones As New cfunciones
        Me.DsHistoricoMoneda1.HistoricoMoneda.Clear()
        cfunciones.Llenar_Tabla_Generico("SELECT * FROM HistoricoMoneda WHERE Id_Moneda = '" & Moneda & "'", Me.DsHistoricoMoneda1.HistoricoMoneda, SqlConnection1.ConnectionString)
    End Sub

    Private Sub BImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BImprimir.Click
        Dim ReporteHistorico As New rptHistoricoMoneda
        ReporteHistorico.SetParameterValue(0, Moneda)
        ReporteHistorico.SetParameterValue(1, NombreMoneda)
        CrystalReportsConexion.LoadShow(ReporteHistorico, MdiParent, Me.SqlConnection1.ConnectionString)
    End Sub
End Class
