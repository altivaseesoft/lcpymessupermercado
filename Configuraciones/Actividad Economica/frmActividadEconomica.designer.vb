﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmActividadEconomica
	Inherits _2.FrmPlantilla

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmActividadEconomica))
        Me.dgwVista = New System.Windows.Forms.DataGridView()
        Me.CodigoActividadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InactivoDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bsActividadEconomica = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtsActividadEconomica1 = New LcPymes_5._2.dtsActividadEconomica()
        Me.gbPrincipal = New System.Windows.Forms.GroupBox()
        Me.chbInactivo = New System.Windows.Forms.CheckBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TB_CE_ActividadEconomicaTableAdapter = New LcPymes_5._2.dtsActividadEconomicaTableAdapters.TB_CE_ActividadEconomicaTableAdapter()
        CType(Me.dgwVista, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsActividadEconomica, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsActividadEconomica1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbPrincipal.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Visible = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        Me.ToolBarRegistrar.ImageIndex = 1
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.Text = "Inactivar"
        Me.ToolBarEliminar.Visible = False
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Visible = False
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 2
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.Images.SetKeyName(0, "Nuevo.png")
        Me.ImageList.Images.SetKeyName(1, "Guardar.png")
        Me.ImageList.Images.SetKeyName(2, "Cerrar.png")
        Me.ImageList.Images.SetKeyName(3, "Cancelar.png")
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 291)
        Me.ToolBar1.Size = New System.Drawing.Size(609, 52)
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(473, 321)
        '
        'TituloModulo
        '
        Me.TituloModulo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TituloModulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.TituloModulo.Dock = System.Windows.Forms.DockStyle.None
        Me.TituloModulo.Image = Nothing
        Me.TituloModulo.Location = New System.Drawing.Point(0, 1)
        Me.TituloModulo.Size = New System.Drawing.Size(609, 10)
        Me.TituloModulo.Text = ""
        '
        'dgwVista
        '
        Me.dgwVista.AllowUserToAddRows = False
        Me.dgwVista.AllowUserToDeleteRows = False
        Me.dgwVista.AutoGenerateColumns = False
        Me.dgwVista.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgwVista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwVista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CodigoActividadDataGridViewTextBoxColumn, Me.DescripcionDataGridViewTextBoxColumn, Me.InactivoDataGridViewCheckBoxColumn})
        Me.dgwVista.DataSource = Me.bsActividadEconomica
        Me.dgwVista.Location = New System.Drawing.Point(4, 123)
        Me.dgwVista.Name = "dgwVista"
        Me.dgwVista.ReadOnly = True
        Me.dgwVista.Size = New System.Drawing.Size(593, 162)
        Me.dgwVista.TabIndex = 71
        '
        'CodigoActividadDataGridViewTextBoxColumn
        '
        Me.CodigoActividadDataGridViewTextBoxColumn.DataPropertyName = "CodigoActividad"
        Me.CodigoActividadDataGridViewTextBoxColumn.HeaderText = "Código"
        Me.CodigoActividadDataGridViewTextBoxColumn.Name = "CodigoActividadDataGridViewTextBoxColumn"
        Me.CodigoActividadDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Descripción"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        Me.DescripcionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'InactivoDataGridViewCheckBoxColumn
        '
        Me.InactivoDataGridViewCheckBoxColumn.DataPropertyName = "Inactivo"
        Me.InactivoDataGridViewCheckBoxColumn.HeaderText = "Inactivo"
        Me.InactivoDataGridViewCheckBoxColumn.Name = "InactivoDataGridViewCheckBoxColumn"
        Me.InactivoDataGridViewCheckBoxColumn.ReadOnly = True
        '
        'bsActividadEconomica
        '
        Me.bsActividadEconomica.DataMember = "TB_CE_ActividadEconomica"
        Me.bsActividadEconomica.DataSource = Me.DtsActividadEconomica1
        '
        'DtsActividadEconomica1
        '
        Me.DtsActividadEconomica1.DataSetName = "dtsActividadEconomica"
        Me.DtsActividadEconomica1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'gbPrincipal
        '
        Me.gbPrincipal.Controls.Add(Me.chbInactivo)
        Me.gbPrincipal.Controls.Add(Me.txtDescripcion)
        Me.gbPrincipal.Controls.Add(Me.Label2)
        Me.gbPrincipal.Controls.Add(Me.txtCodigo)
        Me.gbPrincipal.Controls.Add(Me.Label1)
        Me.gbPrincipal.Enabled = False
        Me.gbPrincipal.Location = New System.Drawing.Point(4, 28)
        Me.gbPrincipal.Name = "gbPrincipal"
        Me.gbPrincipal.Size = New System.Drawing.Size(593, 89)
        Me.gbPrincipal.TabIndex = 72
        Me.gbPrincipal.TabStop = False
        '
        'chbInactivo
        '
        Me.chbInactivo.AutoSize = True
        Me.chbInactivo.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.bsActividadEconomica, "Inactivo", True))
        Me.chbInactivo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.chbInactivo.Location = New System.Drawing.Point(495, 19)
        Me.chbInactivo.Name = "chbInactivo"
        Me.chbInactivo.Size = New System.Drawing.Size(64, 17)
        Me.chbInactivo.TabIndex = 4
        Me.chbInactivo.Text = "Inactivo"
        Me.chbInactivo.UseVisualStyleBackColor = True
        '
        'txtDescripcion
        '
        Me.txtDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsActividadEconomica, "Descripcion", True))
        Me.txtDescripcion.Location = New System.Drawing.Point(94, 42)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(482, 41)
        Me.txtDescripcion.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(6, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Descripción :"
        '
        'txtCodigo
        '
        Me.txtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsActividadEconomica, "CodigoActividad", True))
        Me.txtCodigo.Location = New System.Drawing.Point(94, 10)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(227, 20)
        Me.txtCodigo.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(30, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Código : "
        '
        'TB_CE_ActividadEconomicaTableAdapter
        '
        Me.TB_CE_ActividadEconomicaTableAdapter.ClearBeforeFill = True
        '
        'frmActividadEconomica
        '
        Me.AutoValidate = System.Windows.Forms.AutoValidate.Disable
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(609, 343)
        Me.Controls.Add(Me.gbPrincipal)
        Me.Controls.Add(Me.dgwVista)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(625, 382)
        Me.MinimumSize = New System.Drawing.Size(625, 382)
        Me.Name = "frmActividadEconomica"
        Me.Text = "Configuración/Actividad Económica"
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.dgwVista, 0)
        Me.Controls.SetChildIndex(Me.gbPrincipal, 0)
        CType(Me.dgwVista, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsActividadEconomica, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsActividadEconomica1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbPrincipal.ResumeLayout(False)
        Me.gbPrincipal.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents DtsActividadEconomica1 As dtsActividadEconomica
	Friend WithEvents dgwVista As Windows.Forms.DataGridView
	Friend WithEvents bsActividadEconomica As Windows.Forms.BindingSource
	Friend WithEvents TB_CE_ActividadEconomicaTableAdapter As dtsActividadEconomicaTableAdapters.TB_CE_ActividadEconomicaTableAdapter
	Friend WithEvents CodigoActividadDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
	Friend WithEvents DescripcionDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
	Friend WithEvents InactivoDataGridViewCheckBoxColumn As Windows.Forms.DataGridViewCheckBoxColumn
	Friend WithEvents gbPrincipal As Windows.Forms.GroupBox
	Friend WithEvents chbInactivo As Windows.Forms.CheckBox
	Friend WithEvents txtDescripcion As Windows.Forms.TextBox
	Friend WithEvents Label2 As Windows.Forms.Label
	Friend WithEvents txtCodigo As Windows.Forms.TextBox
	Friend WithEvents Label1 As Windows.Forms.Label
End Class
