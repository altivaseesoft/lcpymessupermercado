﻿Imports System.Windows.Forms

Public Class frmActividadEconomica

	Dim usuario As Usuario_Logeado

	Dim Nuevo As Boolean

	Public Sub New(ByVal Usuario_Parametro As Object, Optional ByVal Conexion As String = "")
		MyBase.New()

		'El Diseñador de Windows Forms requiere esta llamada.
		InitializeComponent()


		usuario = Usuario_Parametro

	End Sub

	Private Sub frmActividadEconomica_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Try
			Me.Text = "Configuración/Actividad Económica"
			TB_CE_ActividadEconomicaTableAdapter.Connection.ConnectionString = GetSetting("SeeSoft", "Seepos", "Conexion")
			TB_CE_ActividadEconomicaTableAdapter.Fill(DtsActividadEconomica1.TB_CE_ActividadEconomica)
		Catch ex As Exception

		End Try

	End Sub

	Private Sub ToolBar1_ButtonClick(sender As Object, e As Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
		Try

			Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
			PMU = VSM(usuario.Cedula, Me.Name) 'Carga los privilegios del usuario con el modu

			Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
				Case 1
					Me.spNuevo()

				Case 3
					If PMU.Update Then spRegistrar() Else MsgBox("No tiene permiso para agregar o actualizar datos.", MsgBoxStyle.Information, "Atención.") : Exit Sub
				Case 4
					If PMU.Delete Then spCambiarEstado() Else MsgBox("No tiene permiso para eliminar o anular datos.", MsgBoxStyle.Information, "Atención.") : Exit Sub
				Case 7
					If MessageBox.Show("¿Desea cerrar el módulo de 'Actividad Económica' ?", "Atención.", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
						Me.Close()
					End If
			End Select
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub spNuevo()
		Try
			If Me.ToolBarNuevo.Text = "Nuevo" Then
				Nuevo = True
				bsActividadEconomica.AddNew()
				Me.ToolBarNuevo.Text = "Cancelar"
				ToolBarRegistrar.Enabled = True
				ToolBarNuevo.ImageIndex = 3
				gbPrincipal.Enabled = True
				txtCodigo.Focus()
			Else
				Nuevo = False
				Me.ToolBarNuevo.Text = "Nuevo"
				ToolBarRegistrar.Enabled = False
				ToolBarNuevo.ImageIndex = 0
				gbPrincipal.Enabled = False
				bsActividadEconomica.CancelEdit()
			End If

		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Function fnValidarCampos() As Boolean
		Try
			If txtCodigo.Text.Replace(" ", "").Length = 0 Then
				MessageBox.Show("Por favor, complete los campos obligatorios.", "Atención.", MessageBoxButtons.OK, MessageBoxIcon.Warning)
				Return False
			End If
			If txtDescripcion.Text.Replace(" ", "").Length = 0 Then
				MessageBox.Show("Por favor, complete los campos obligatorios.", "Atención.", MessageBoxButtons.OK, MessageBoxIcon.Warning)
				Return False
			End If
			Return True
		Catch ex As Exception
			MsgBox(ex.Message)
			Return False
		End Try

	End Function

	Private Sub spRegistrar()
		Try
			If Not fnValidarCampos() Then Exit Sub

			bsActividadEconomica.EndEdit()

			TB_CE_ActividadEconomicaTableAdapter.Update(DtsActividadEconomica1.TB_CE_ActividadEconomica)

			MessageBox.Show("La información se guardó correctamente.", "Atención.", MessageBoxButtons.OK, MessageBoxIcon.Information)

			spNuevo()
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub spCambiarEstado()
		Try

		Catch ex As Exception

		End Try
	End Sub



	Private Sub dgwVista_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgwVista.CellDoubleClick

		If bsActividadEconomica.Count > 0 Then

			bsActividadEconomica.CancelEdit()
				Nuevo = False
				Me.ToolBarNuevo.Text = "Cancelar"
				ToolBarRegistrar.Enabled = True
			ToolBarNuevo.ImageIndex = 3
			gbPrincipal.Enabled = True



			End If

	End Sub

	Private Sub txtCodigo_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCodigo.KeyDown
		If e.KeyCode = Keys.Enter Then
			txtDescripcion.Focus()
		End If
	End Sub

	Private Sub txtDescripcion_KeyDown(sender As Object, e As KeyEventArgs) Handles txtDescripcion.KeyDown
		If e.KeyCode = Keys.Enter Then
			txtDescripcion.Focus()
		End If
	End Sub

End Class
