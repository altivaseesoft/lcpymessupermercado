Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports AutoActualiza
Imports Importar_XML
Imports ReportesXML

Public Class MainForm
    Inherits System.Windows.Forms.Form

#Region "Variables"
    Dim Contador As Integer
    Dim TITULO As String
    Dim usua As Usuario_Logeado
    Friend WithEvents StatusBarPanel5 As StatusBarPanel
    Friend WithEvents MenuItem8 As MenuItem
    Friend WithEvents MenuItem9 As MenuItem
    Friend WithEvents MenuItemConfirmar As MenuItem
    Friend WithEvents MenuItem11 As MenuItem
    Friend WithEvents MenuItem12 As MenuItem
    Friend WithEvents MenuItemReporteHacienda As MenuItem
    Friend WithEvents MenuItem10 As MenuItem
    Friend WithEvents btImpuestoActividadEconomica As MenuItem
    Friend WithEvents btnActividades As MenuItem
    Friend WithEvents btnAnalisisVenta As MenuItem
    Public Texto As String
#End Region

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    Public Sub New(ByVal Usuario_Parametro)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        usua = Usuario_Parametro
        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel3 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel4 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents HelpProvider As System.Windows.Forms.HelpProvider
    Friend WithEvents MenuItemInventario_Catalogo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Familias As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Ubicaciones As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Presentaciones As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Marcas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Ajustes As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Etiquetas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Aumentos_X_Categoria As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Bodegas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemInventario_Ajuste_Bodegas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemRedondeo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteVentas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteCompras As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteABON_CRE_NDEB As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporte_Inventario As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporte_Kardex As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteABC As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteClientes As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteProveedores As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAdministracion_Clientes As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAdministracion_Moneda As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAdministracion_Proveedores As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAdministracion_ConfiguracionSistema As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_Facturacion As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_Proformas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_DevolucionesVentas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_AbonosCXC As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_AjustesCXC As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_EstadoCuentaCXC As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_ComprasProveedor As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_OrdenCompraAutomatica As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_OrdenCompraManual As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_DevolucionesProveedor As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_AbonosCuentaProveedor As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_AjusteCuentaProveedor As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_EstadoCuentaProveedor As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_Caja_Apertura As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_Caja_Cierre As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_Caja_OpcionesPago As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_Caja_Movimientos As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuAdministracion As System.Windows.Forms.MenuItem
    Friend WithEvents MenuOperaciones As System.Windows.Forms.MenuItem
    Friend WithEvents MenuInventarios As System.Windows.Forms.MenuItem
    Friend WithEvents MenuReportes As System.Windows.Forms.MenuItem
    Friend WithEvents MenuInterfaz As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem19 As System.Windows.Forms.MenuItem
    Friend WithEvents menuExtender As MenuExtender.MenuExtender
    Friend WithEvents imageList As System.Windows.Forms.ImageList
    Friend WithEvents MenuItemCuentasXCobrar As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemCompras As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemCuentasXPagar As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemControlCaja As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemVentas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea01 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea02 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea03 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea04 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea05 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemControlBodega As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea06 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea07 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea09 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAyuda As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemMaximizar As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemNormal As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemMinimizar As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemSeguridad As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea12 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemCalculadora As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemLinea13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemCerrar As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemPerfilUsuarios As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemCambioUsuarios As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemRegistroUsuarios As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemActivarPOS As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemRespaldos As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemFullScreenPVE As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteCajas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAdministracion_ConfiguracionMoneda As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacion_Caja_Arqueo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAdministracion_Tarjetas As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReportesCXP As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemTomaFisica As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemComisionista As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOperacionesComisiones As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteComisiones As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemTraslados As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemMovArt As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemModificaFactura As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteApartados As System.Windows.Forms.MenuItem
    Friend WithEvents Menu_Importar_Excel As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteAjusteInventarioSalida As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemReporteRecarga As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemPaquetes As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemPromociones As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemHistoricoCierreCaja As System.Windows.Forms.MenuItem
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButtonInventario As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButtonProveedores As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButtonClientes As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButtonCompras As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButtonEstadocuenta As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton5 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButtonFactura As System.Windows.Forms.ToolBarButton
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemUtilidadXProveedor As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemCambioutilidad As System.Windows.Forms.MenuItem

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuAdministracion = New System.Windows.Forms.MenuItem()
        Me.MenuItemAdministracion_Clientes = New System.Windows.Forms.MenuItem()
        Me.MenuItemComisionista = New System.Windows.Forms.MenuItem()
        Me.MenuItemAdministracion_Proveedores = New System.Windows.Forms.MenuItem()
        Me.MenuItemAdministracion_Moneda = New System.Windows.Forms.MenuItem()
        Me.MenuItemAdministracion_Tarjetas = New System.Windows.Forms.MenuItem()
        Me.MenuItemAdministracion_ConfiguracionMoneda = New System.Windows.Forms.MenuItem()
        Me.MenuItemAdministracion_ConfiguracionSistema = New System.Windows.Forms.MenuItem()
        Me.MenuItemPaquetes = New System.Windows.Forms.MenuItem()
        Me.MenuItemPromociones = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.btnActividades = New System.Windows.Forms.MenuItem()
        Me.MenuOperaciones = New System.Windows.Forms.MenuItem()
        Me.MenuItemVentas = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_Facturacion = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_Proformas = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_DevolucionesVentas = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacionesComisiones = New System.Windows.Forms.MenuItem()
        Me.MenuItemModificaFactura = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea01 = New System.Windows.Forms.MenuItem()
        Me.MenuItemCuentasXCobrar = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_AbonosCXC = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_AjustesCXC = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_EstadoCuentaCXC = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea02 = New System.Windows.Forms.MenuItem()
        Me.MenuItemCompras = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_ComprasProveedor = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_OrdenCompraManual = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_OrdenCompraAutomatica = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_DevolucionesProveedor = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea03 = New System.Windows.Forms.MenuItem()
        Me.MenuItemCuentasXPagar = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_AbonosCuentaProveedor = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_AjusteCuentaProveedor = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_EstadoCuentaProveedor = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea04 = New System.Windows.Forms.MenuItem()
        Me.MenuItemControlCaja = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_Caja_Apertura = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_Caja_Arqueo = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_Caja_Cierre = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_Caja_OpcionesPago = New System.Windows.Forms.MenuItem()
        Me.MenuItemOperacion_Caja_Movimientos = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItemHistoricoCierreCaja = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.MenuItemConfirmar = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.MenuInventarios = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Catalogo = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Familias = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Ubicaciones = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Presentaciones = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Marcas = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Ajustes = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Etiquetas = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Aumentos_X_Categoria = New System.Windows.Forms.MenuItem()
        Me.MenuItemRedondeo = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Bodegas = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea05 = New System.Windows.Forms.MenuItem()
        Me.MenuItemControlBodega = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea06 = New System.Windows.Forms.MenuItem()
        Me.MenuItemInventario_Ajuste_Bodegas = New System.Windows.Forms.MenuItem()
        Me.MenuItemTraslados = New System.Windows.Forms.MenuItem()
        Me.MenuItemTomaFisica = New System.Windows.Forms.MenuItem()
        Me.Menu_Importar_Excel = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItemUtilidadXProveedor = New System.Windows.Forms.MenuItem()
        Me.MenuReportes = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteVentas = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteCompras = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteHacienda = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteComisiones = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteApartados = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteABON_CRE_NDEB = New System.Windows.Forms.MenuItem()
        Me.MenuItemReportesCXP = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea07 = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporte_Inventario = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteAjusteInventarioSalida = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporte_Kardex = New System.Windows.Forms.MenuItem()
        Me.MenuItemMovArt = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteABC = New System.Windows.Forms.MenuItem()
        Me.btImpuestoActividadEconomica = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea09 = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteClientes = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteProveedores = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteCajas = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItemReporteRecarga = New System.Windows.Forms.MenuItem()
        Me.MenuItemCambioutilidad = New System.Windows.Forms.MenuItem()
        Me.MenuInterfaz = New System.Windows.Forms.MenuItem()
        Me.MenuItemAyuda = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea10 = New System.Windows.Forms.MenuItem()
        Me.MenuItemMaximizar = New System.Windows.Forms.MenuItem()
        Me.MenuItemNormal = New System.Windows.Forms.MenuItem()
        Me.MenuItemMinimizar = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea11 = New System.Windows.Forms.MenuItem()
        Me.MenuItemSeguridad = New System.Windows.Forms.MenuItem()
        Me.MenuItemPerfilUsuarios = New System.Windows.Forms.MenuItem()
        Me.MenuItemCambioUsuarios = New System.Windows.Forms.MenuItem()
        Me.MenuItemRegistroUsuarios = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItemRespaldos = New System.Windows.Forms.MenuItem()
        Me.MenuItemActivarPOS = New System.Windows.Forms.MenuItem()
        Me.MenuItem = New System.Windows.Forms.MenuItem()
        Me.MenuItem19 = New System.Windows.Forms.MenuItem()
        Me.MenuItemFullScreenPVE = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea12 = New System.Windows.Forms.MenuItem()
        Me.MenuItemCalculadora = New System.Windows.Forms.MenuItem()
        Me.MenuItemLinea13 = New System.Windows.Forms.MenuItem()
        Me.MenuItemCerrar = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusBar1 = New System.Windows.Forms.StatusBar()
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel()
        Me.StatusBarPanel4 = New System.Windows.Forms.StatusBarPanel()
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel()
        Me.StatusBarPanel3 = New System.Windows.Forms.StatusBarPanel()
        Me.StatusBarPanel5 = New System.Windows.Forms.StatusBarPanel()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.HelpProvider = New System.Windows.Forms.HelpProvider()
        Me.imageList = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar()
        Me.ToolBarButtonInventario = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButtonProveedores = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButtonClientes = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButtonCompras = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButtonEstadocuenta = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButton5 = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarButtonFactura = New System.Windows.Forms.ToolBarButton()
        Me.menuExtender = New MenuExtender.MenuExtender(Me.components)
        Me.btnAnalisisVenta = New System.Windows.Forms.MenuItem()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuAdministracion, Me.MenuOperaciones, Me.MenuInventarios, Me.MenuReportes, Me.MenuInterfaz, Me.MenuItem4})
        '
        'MenuAdministracion
        '
        Me.MenuAdministracion.Index = 0
        Me.MenuAdministracion.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemAdministracion_Clientes, Me.MenuItemComisionista, Me.MenuItemAdministracion_Proveedores, Me.MenuItemAdministracion_Moneda, Me.MenuItemAdministracion_Tarjetas, Me.MenuItemAdministracion_ConfiguracionMoneda, Me.MenuItemAdministracion_ConfiguracionSistema, Me.MenuItemPaquetes, Me.MenuItemPromociones, Me.MenuItem10, Me.btnActividades, Me.btnAnalisisVenta})
        Me.MenuAdministracion.Text = "Administración"
        '
        'MenuItemAdministracion_Clientes
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemAdministracion_Clientes, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemAdministracion_Clientes, -1)
        Me.MenuItemAdministracion_Clientes.Index = 0
        Me.MenuItemAdministracion_Clientes.OwnerDraw = True
        Me.MenuItemAdministracion_Clientes.Text = "Registro de Clientes"
        '
        'MenuItemComisionista
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemComisionista, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemComisionista, -1)
        Me.MenuItemComisionista.Index = 1
        Me.MenuItemComisionista.OwnerDraw = True
        Me.MenuItemComisionista.Text = "Registro de Comisionistas"
        '
        'MenuItemAdministracion_Proveedores
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemAdministracion_Proveedores, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemAdministracion_Proveedores, -1)
        Me.MenuItemAdministracion_Proveedores.Index = 2
        Me.MenuItemAdministracion_Proveedores.OwnerDraw = True
        Me.MenuItemAdministracion_Proveedores.Text = "Registro de Proveedores"
        '
        'MenuItemAdministracion_Moneda
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemAdministracion_Moneda, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemAdministracion_Moneda, -1)
        Me.MenuItemAdministracion_Moneda.Index = 3
        Me.MenuItemAdministracion_Moneda.OwnerDraw = True
        Me.MenuItemAdministracion_Moneda.Text = "Registro de Moneda"
        '
        'MenuItemAdministracion_Tarjetas
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemAdministracion_Tarjetas, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemAdministracion_Tarjetas, -1)
        Me.MenuItemAdministracion_Tarjetas.Index = 4
        Me.MenuItemAdministracion_Tarjetas.OwnerDraw = True
        Me.MenuItemAdministracion_Tarjetas.Text = "Operadores de Tarjeta"
        '
        'MenuItemAdministracion_ConfiguracionMoneda
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemAdministracion_ConfiguracionMoneda, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemAdministracion_ConfiguracionMoneda, -1)
        Me.MenuItemAdministracion_ConfiguracionMoneda.Index = 5
        Me.MenuItemAdministracion_ConfiguracionMoneda.OwnerDraw = True
        Me.MenuItemAdministracion_ConfiguracionMoneda.Text = "Configuración Moneda"
        '
        'MenuItemAdministracion_ConfiguracionSistema
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemAdministracion_ConfiguracionSistema, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemAdministracion_ConfiguracionSistema, -1)
        Me.MenuItemAdministracion_ConfiguracionSistema.Index = 6
        Me.MenuItemAdministracion_ConfiguracionSistema.OwnerDraw = True
        Me.MenuItemAdministracion_ConfiguracionSistema.Text = "Configuración Sistema"
        '
        'MenuItemPaquetes
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemPaquetes, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemPaquetes, -1)
        Me.MenuItemPaquetes.Index = 7
        Me.MenuItemPaquetes.OwnerDraw = True
        Me.MenuItemPaquetes.Text = "Paquetes"
        '
        'MenuItemPromociones
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemPromociones, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemPromociones, -1)
        Me.MenuItemPromociones.Index = 8
        Me.MenuItemPromociones.OwnerDraw = True
        Me.MenuItemPromociones.Text = "Promociones"
        '
        'MenuItem10
        '
        Me.menuExtender.SetExtEnable(Me.MenuItem10, True)
        Me.menuExtender.SetImageIndex(Me.MenuItem10, -1)
        Me.MenuItem10.Index = 9
        Me.MenuItem10.OwnerDraw = True
        Me.MenuItem10.Text = "Tarifa Impuesto Valor Agregado"
        '
        'btnActividades
        '
        Me.menuExtender.SetExtEnable(Me.btnActividades, True)
        Me.menuExtender.SetImageIndex(Me.btnActividades, -1)
        Me.btnActividades.Index = 10
        Me.btnActividades.OwnerDraw = True
        Me.btnActividades.Tag = "frmActividadEconomica"
        Me.btnActividades.Text = "Actividad Económica"
        '
        'MenuOperaciones
        '
        Me.MenuOperaciones.Index = 1
        Me.MenuOperaciones.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemVentas, Me.MenuItemLinea01, Me.MenuItemCuentasXCobrar, Me.MenuItemLinea02, Me.MenuItemCompras, Me.MenuItemLinea03, Me.MenuItemCuentasXPagar, Me.MenuItemLinea04, Me.MenuItemControlCaja, Me.MenuItem9, Me.MenuItemConfirmar})
        Me.MenuOperaciones.Text = "Operaciones"
        '
        'MenuItemVentas
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemVentas, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemVentas, -1)
        Me.MenuItemVentas.Index = 0
        Me.MenuItemVentas.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemOperacion_Facturacion, Me.MenuItemOperacion_Proformas, Me.MenuItemOperacion_DevolucionesVentas, Me.MenuItemOperacionesComisiones, Me.MenuItemModificaFactura})
        Me.MenuItemVentas.OwnerDraw = True
        Me.MenuItemVentas.Text = "Ventas"
        '
        'MenuItemOperacion_Facturacion
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemOperacion_Facturacion, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_Facturacion, -1)
        Me.MenuItemOperacion_Facturacion.Index = 0
        Me.MenuItemOperacion_Facturacion.OwnerDraw = True
        Me.MenuItemOperacion_Facturacion.Shortcut = System.Windows.Forms.Shortcut.CtrlF
        Me.MenuItemOperacion_Facturacion.Text = "Facturación"
        '
        'MenuItemOperacion_Proformas
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemOperacion_Proformas, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_Proformas, -1)
        Me.MenuItemOperacion_Proformas.Index = 1
        Me.MenuItemOperacion_Proformas.OwnerDraw = True
        Me.MenuItemOperacion_Proformas.Shortcut = System.Windows.Forms.Shortcut.CtrlP
        Me.MenuItemOperacion_Proformas.Text = "Proformas o Cotización"
        '
        'MenuItemOperacion_DevolucionesVentas
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemOperacion_DevolucionesVentas, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_DevolucionesVentas, -1)
        Me.MenuItemOperacion_DevolucionesVentas.Index = 2
        Me.MenuItemOperacion_DevolucionesVentas.OwnerDraw = True
        Me.MenuItemOperacion_DevolucionesVentas.Text = "Devoluciones"
        '
        'MenuItemOperacionesComisiones
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemOperacionesComisiones, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacionesComisiones, -1)
        Me.MenuItemOperacionesComisiones.Index = 3
        Me.MenuItemOperacionesComisiones.OwnerDraw = True
        Me.MenuItemOperacionesComisiones.Text = "Pago Comisiones"
        '
        'MenuItemModificaFactura
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemModificaFactura, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemModificaFactura, -1)
        Me.MenuItemModificaFactura.Index = 4
        Me.MenuItemModificaFactura.OwnerDraw = True
        Me.MenuItemModificaFactura.Text = "Modifica Factura"
        Me.MenuItemModificaFactura.Visible = False
        '
        'MenuItemLinea01
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea01, -1)
        Me.MenuItemLinea01.Index = 1
        Me.MenuItemLinea01.Text = "-"
        '
        'MenuItemCuentasXCobrar
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemCuentasXCobrar, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemCuentasXCobrar, -1)
        Me.MenuItemCuentasXCobrar.Index = 2
        Me.MenuItemCuentasXCobrar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemOperacion_AbonosCXC, Me.MenuItemOperacion_AjustesCXC, Me.MenuItemOperacion_EstadoCuentaCXC})
        Me.MenuItemCuentasXCobrar.OwnerDraw = True
        Me.MenuItemCuentasXCobrar.Text = "Cuentas x Cobrar"
        '
        'MenuItemOperacion_AbonosCXC
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_AbonosCXC, -1)
        Me.MenuItemOperacion_AbonosCXC.Index = 0
        Me.MenuItemOperacion_AbonosCXC.Text = "Abonos a Facturas"
        '
        'MenuItemOperacion_AjustesCXC
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_AjustesCXC, -1)
        Me.MenuItemOperacion_AjustesCXC.Index = 1
        Me.MenuItemOperacion_AjustesCXC.Text = "Ajustes de Cuenta"
        '
        'MenuItemOperacion_EstadoCuentaCXC
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_EstadoCuentaCXC, -1)
        Me.MenuItemOperacion_EstadoCuentaCXC.Index = 2
        Me.MenuItemOperacion_EstadoCuentaCXC.Text = "Estados de  Cuenta"
        '
        'MenuItemLinea02
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea02, -1)
        Me.MenuItemLinea02.Index = 3
        Me.MenuItemLinea02.Text = "-"
        '
        'MenuItemCompras
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemCompras, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemCompras, -1)
        Me.MenuItemCompras.Index = 4
        Me.MenuItemCompras.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemOperacion_ComprasProveedor, Me.MenuItemOperacion_OrdenCompraManual, Me.MenuItemOperacion_OrdenCompraAutomatica, Me.MenuItemOperacion_DevolucionesProveedor})
        Me.MenuItemCompras.OwnerDraw = True
        Me.MenuItemCompras.Text = "Compras"
        '
        'MenuItemOperacion_ComprasProveedor
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_ComprasProveedor, -1)
        Me.MenuItemOperacion_ComprasProveedor.Index = 0
        Me.MenuItemOperacion_ComprasProveedor.Shortcut = System.Windows.Forms.Shortcut.CtrlM
        Me.MenuItemOperacion_ComprasProveedor.Text = "Factura de Compras"
        '
        'MenuItemOperacion_OrdenCompraManual
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_OrdenCompraManual, -1)
        Me.MenuItemOperacion_OrdenCompraManual.Index = 1
        Me.MenuItemOperacion_OrdenCompraManual.Text = "Ordenes de Compra Manual"
        '
        'MenuItemOperacion_OrdenCompraAutomatica
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_OrdenCompraAutomatica, -1)
        Me.MenuItemOperacion_OrdenCompraAutomatica.Index = 2
        Me.MenuItemOperacion_OrdenCompraAutomatica.Text = "Ordenes de Compra Automática"
        '
        'MenuItemOperacion_DevolucionesProveedor
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_DevolucionesProveedor, -1)
        Me.MenuItemOperacion_DevolucionesProveedor.Index = 3
        Me.MenuItemOperacion_DevolucionesProveedor.Text = "Devoluciones de Compra"
        '
        'MenuItemLinea03
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea03, -1)
        Me.MenuItemLinea03.Index = 5
        Me.MenuItemLinea03.Text = "-"
        '
        'MenuItemCuentasXPagar
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemCuentasXPagar, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemCuentasXPagar, -1)
        Me.MenuItemCuentasXPagar.Index = 6
        Me.MenuItemCuentasXPagar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemOperacion_AbonosCuentaProveedor, Me.MenuItemOperacion_AjusteCuentaProveedor, Me.MenuItemOperacion_EstadoCuentaProveedor, Me.MenuItem1})
        Me.MenuItemCuentasXPagar.OwnerDraw = True
        Me.MenuItemCuentasXPagar.Text = "Cuentas  x Pagar"
        '
        'MenuItemOperacion_AbonosCuentaProveedor
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_AbonosCuentaProveedor, -1)
        Me.MenuItemOperacion_AbonosCuentaProveedor.Index = 0
        Me.MenuItemOperacion_AbonosCuentaProveedor.Text = "Abonos a Cuenta"
        '
        'MenuItemOperacion_AjusteCuentaProveedor
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_AjusteCuentaProveedor, -1)
        Me.MenuItemOperacion_AjusteCuentaProveedor.Index = 1
        Me.MenuItemOperacion_AjusteCuentaProveedor.Text = "Ajustes a Cuenta"
        '
        'MenuItemOperacion_EstadoCuentaProveedor
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_EstadoCuentaProveedor, -1)
        Me.MenuItemOperacion_EstadoCuentaProveedor.Index = 2
        Me.MenuItemOperacion_EstadoCuentaProveedor.Text = "Estado de Cuenta"
        '
        'MenuItem1
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem1, -1)
        Me.MenuItem1.Index = 3
        Me.MenuItem1.Text = "Gastos (Otras CXP)"
        '
        'MenuItemLinea04
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea04, -1)
        Me.MenuItemLinea04.Index = 7
        Me.MenuItemLinea04.Text = "-"
        '
        'MenuItemControlCaja
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemControlCaja, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemControlCaja, -1)
        Me.MenuItemControlCaja.Index = 8
        Me.MenuItemControlCaja.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemOperacion_Caja_Apertura, Me.MenuItemOperacion_Caja_Arqueo, Me.MenuItemOperacion_Caja_Cierre, Me.MenuItemOperacion_Caja_OpcionesPago, Me.MenuItemOperacion_Caja_Movimientos, Me.MenuItem7, Me.MenuItemHistoricoCierreCaja})
        Me.MenuItemControlCaja.OwnerDraw = True
        Me.MenuItemControlCaja.Text = "Control de Caja"
        '
        'MenuItemOperacion_Caja_Apertura
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_Caja_Apertura, -1)
        Me.MenuItemOperacion_Caja_Apertura.Index = 0
        Me.MenuItemOperacion_Caja_Apertura.Text = "Apertura de Caja"
        '
        'MenuItemOperacion_Caja_Arqueo
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_Caja_Arqueo, -1)
        Me.MenuItemOperacion_Caja_Arqueo.Index = 1
        Me.MenuItemOperacion_Caja_Arqueo.Text = "Arqueo de Caja"
        '
        'MenuItemOperacion_Caja_Cierre
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_Caja_Cierre, -1)
        Me.MenuItemOperacion_Caja_Cierre.Index = 2
        Me.MenuItemOperacion_Caja_Cierre.Text = "Cierre de Caja"
        '
        'MenuItemOperacion_Caja_OpcionesPago
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_Caja_OpcionesPago, -1)
        Me.MenuItemOperacion_Caja_OpcionesPago.Index = 3
        Me.MenuItemOperacion_Caja_OpcionesPago.Text = "Opciones de Pago"
        '
        'MenuItemOperacion_Caja_Movimientos
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemOperacion_Caja_Movimientos, -1)
        Me.MenuItemOperacion_Caja_Movimientos.Index = 4
        Me.MenuItemOperacion_Caja_Movimientos.Text = "Movimiento de Caja"
        '
        'MenuItem7
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem7, -1)
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "-"
        '
        'MenuItemHistoricoCierreCaja
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemHistoricoCierreCaja, -1)
        Me.MenuItemHistoricoCierreCaja.Index = 6
        Me.MenuItemHistoricoCierreCaja.Text = "Histórico de Cierres de Caja"
        '
        'MenuItem9
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem9, -1)
        Me.MenuItem9.Index = 9
        Me.MenuItem9.Text = "-"
        '
        'MenuItemConfirmar
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemConfirmar, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemConfirmar, -1)
        Me.MenuItemConfirmar.Index = 10
        Me.MenuItemConfirmar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem11, Me.MenuItem12})
        Me.MenuItemConfirmar.OwnerDraw = True
        Me.MenuItemConfirmar.Text = "Confirmar"
        '
        'MenuItem11
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem11, -1)
        Me.MenuItem11.Index = 0
        Me.MenuItem11.Text = "Individual"
        '
        'MenuItem12
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem12, -1)
        Me.MenuItem12.Index = 1
        Me.MenuItem12.Text = "Varios"
        '
        'MenuInventarios
        '
        Me.MenuInventarios.Index = 2
        Me.MenuInventarios.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemInventario_Catalogo, Me.MenuItemInventario_Familias, Me.MenuItemInventario_Ubicaciones, Me.MenuItemInventario_Presentaciones, Me.MenuItemInventario_Marcas, Me.MenuItemInventario_Ajustes, Me.MenuItemInventario_Etiquetas, Me.MenuItemInventario_Aumentos_X_Categoria, Me.MenuItemRedondeo, Me.MenuItemInventario_Bodegas, Me.MenuItemLinea05, Me.MenuItemControlBodega, Me.MenuItemLinea06, Me.MenuItemInventario_Ajuste_Bodegas, Me.MenuItemTraslados, Me.MenuItemTomaFisica, Me.Menu_Importar_Excel, Me.MenuItem6, Me.MenuItemUtilidadXProveedor})
        Me.MenuInventarios.Text = "Inventario"
        '
        'MenuItemInventario_Catalogo
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Catalogo, -1)
        Me.MenuItemInventario_Catalogo.Index = 0
        Me.MenuItemInventario_Catalogo.Shortcut = System.Windows.Forms.Shortcut.CtrlI
        Me.MenuItemInventario_Catalogo.Text = "Registro de Catálogo Inventario"
        '
        'MenuItemInventario_Familias
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Familias, -1)
        Me.MenuItemInventario_Familias.Index = 1
        Me.MenuItemInventario_Familias.Text = "Registro de Familias"
        '
        'MenuItemInventario_Ubicaciones
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Ubicaciones, -1)
        Me.MenuItemInventario_Ubicaciones.Index = 2
        Me.MenuItemInventario_Ubicaciones.Text = "Registro de Ubicaciones"
        '
        'MenuItemInventario_Presentaciones
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Presentaciones, -1)
        Me.MenuItemInventario_Presentaciones.Index = 3
        Me.MenuItemInventario_Presentaciones.Text = "Registro de Presentaciones"
        '
        'MenuItemInventario_Marcas
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Marcas, -1)
        Me.MenuItemInventario_Marcas.Index = 4
        Me.MenuItemInventario_Marcas.Text = "Registro de Marcas"
        '
        'MenuItemInventario_Ajustes
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Ajustes, -1)
        Me.MenuItemInventario_Ajustes.Index = 5
        Me.MenuItemInventario_Ajustes.Text = "Registro de Ajuste Inventario"
        '
        'MenuItemInventario_Etiquetas
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Etiquetas, -1)
        Me.MenuItemInventario_Etiquetas.Index = 6
        Me.MenuItemInventario_Etiquetas.Text = "Etiquetas de  Artículos"
        Me.MenuItemInventario_Etiquetas.Visible = False
        '
        'MenuItemInventario_Aumentos_X_Categoria
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Aumentos_X_Categoria, -1)
        Me.MenuItemInventario_Aumentos_X_Categoria.Index = 7
        Me.MenuItemInventario_Aumentos_X_Categoria.Text = "Aumento a Categorías"
        '
        'MenuItemRedondeo
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemRedondeo, -1)
        Me.MenuItemRedondeo.Index = 8
        Me.MenuItemRedondeo.Text = "Redondeo de Inventarios"
        '
        'MenuItemInventario_Bodegas
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Bodegas, -1)
        Me.MenuItemInventario_Bodegas.Index = 9
        Me.MenuItemInventario_Bodegas.Text = "Registro de Bodegas"
        '
        'MenuItemLinea05
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea05, -1)
        Me.MenuItemLinea05.Index = 10
        Me.MenuItemLinea05.Text = "-"
        Me.MenuItemLinea05.Visible = False
        '
        'MenuItemControlBodega
        '
        Me.MenuItemControlBodega.Enabled = False
        Me.menuExtender.SetImageIndex(Me.MenuItemControlBodega, -1)
        Me.MenuItemControlBodega.Index = 11
        Me.MenuItemControlBodega.Text = ">> Control de Bodegas <<"
        Me.MenuItemControlBodega.Visible = False
        '
        'MenuItemLinea06
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea06, -1)
        Me.MenuItemLinea06.Index = 12
        Me.MenuItemLinea06.Text = "-"
        '
        'MenuItemInventario_Ajuste_Bodegas
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemInventario_Ajuste_Bodegas, -1)
        Me.MenuItemInventario_Ajuste_Bodegas.Index = 13
        Me.MenuItemInventario_Ajuste_Bodegas.Text = "Ajuste de Bodegas"
        '
        'MenuItemTraslados
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemTraslados, -1)
        Me.MenuItemTraslados.Index = 14
        Me.MenuItemTraslados.Text = "Traslado de Bodegas"
        '
        'MenuItemTomaFisica
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemTomaFisica, -1)
        Me.MenuItemTomaFisica.Index = 15
        Me.MenuItemTomaFisica.Text = "Toma Fisica"
        '
        'Menu_Importar_Excel
        '
        Me.menuExtender.SetImageIndex(Me.Menu_Importar_Excel, -1)
        Me.Menu_Importar_Excel.Index = 16
        Me.Menu_Importar_Excel.Text = "Importar de Excel"
        '
        'MenuItem6
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem6, -1)
        Me.MenuItem6.Index = 17
        Me.MenuItem6.Text = "-"
        '
        'MenuItemUtilidadXProveedor
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemUtilidadXProveedor, -1)
        Me.MenuItemUtilidadXProveedor.Index = 18
        Me.MenuItemUtilidadXProveedor.Text = "Utilidad por Proveedor"
        '
        'MenuReportes
        '
        Me.MenuReportes.Index = 3
        Me.MenuReportes.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemReporteVentas, Me.MenuItemReporteCompras, Me.MenuItemReporteHacienda, Me.MenuItem2, Me.MenuItemReporteComisiones, Me.MenuItemReporteApartados, Me.MenuItemReporteABON_CRE_NDEB, Me.MenuItemReportesCXP, Me.MenuItemLinea07, Me.MenuItemReporte_Inventario, Me.MenuItemReporteAjusteInventarioSalida, Me.MenuItemReporte_Kardex, Me.MenuItemMovArt, Me.MenuItemReporteABC, Me.btImpuestoActividadEconomica, Me.MenuItemLinea09, Me.MenuItemReporteClientes, Me.MenuItemReporteProveedores, Me.MenuItemReporteCajas, Me.MenuItem3, Me.MenuItemReporteRecarga, Me.MenuItemCambioutilidad})
        Me.MenuReportes.Text = "Reportes"
        '
        'MenuItemReporteVentas
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteVentas, -1)
        Me.MenuItemReporteVentas.Index = 0
        Me.MenuItemReporteVentas.Text = "Reportes de Ventas"
        '
        'MenuItemReporteCompras
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteCompras, -1)
        Me.MenuItemReporteCompras.Index = 1
        Me.MenuItemReporteCompras.Text = "Reportes de Compras"
        '
        'MenuItemReporteHacienda
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteHacienda, -1)
        Me.MenuItemReporteHacienda.Index = 2
        Me.MenuItemReporteHacienda.Text = "Reportes Facturas Hacienda"
        '
        'MenuItem2
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem2, -1)
        Me.MenuItem2.Index = 3
        Me.MenuItem2.Text = "Reportes de Gastos"
        '
        'MenuItemReporteComisiones
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteComisiones, -1)
        Me.MenuItemReporteComisiones.Index = 4
        Me.MenuItemReporteComisiones.Text = "Reportes de Comisiones"
        '
        'MenuItemReporteApartados
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteApartados, -1)
        Me.MenuItemReporteApartados.Index = 5
        Me.MenuItemReporteApartados.Text = "Reportes de Apartados"
        Me.MenuItemReporteApartados.Visible = False
        '
        'MenuItemReporteABON_CRE_NDEB
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteABON_CRE_NDEB, -1)
        Me.MenuItemReporteABON_CRE_NDEB.Index = 6
        Me.MenuItemReporteABON_CRE_NDEB.Text = "Reportes CXC (ABO,NCRE,NDEB)"
        '
        'MenuItemReportesCXP
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReportesCXP, -1)
        Me.MenuItemReportesCXP.Index = 7
        Me.MenuItemReportesCXP.Text = "Reportes CXP (ABO,NCRE,NDEB)"
        '
        'MenuItemLinea07
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea07, -1)
        Me.MenuItemLinea07.Index = 8
        Me.MenuItemLinea07.Text = "-"
        '
        'MenuItemReporte_Inventario
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporte_Inventario, -1)
        Me.MenuItemReporte_Inventario.Index = 9
        Me.MenuItemReporte_Inventario.Text = "Reportes de Inventario"
        '
        'MenuItemReporteAjusteInventarioSalida
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteAjusteInventarioSalida, -1)
        Me.MenuItemReporteAjusteInventarioSalida.Index = 10
        Me.MenuItemReporteAjusteInventarioSalida.Text = "Reporte Ajuste de Inventario"
        Me.MenuItemReporteAjusteInventarioSalida.Visible = False
        '
        'MenuItemReporte_Kardex
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporte_Kardex, -1)
        Me.MenuItemReporte_Kardex.Index = 11
        Me.MenuItemReporte_Kardex.Text = "Reportes de Kardex"
        '
        'MenuItemMovArt
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemMovArt, -1)
        Me.MenuItemMovArt.Index = 12
        Me.MenuItemMovArt.Text = "Movimiento de Articulos"
        '
        'MenuItemReporteABC
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteABC, -1)
        Me.MenuItemReporteABC.Index = 13
        Me.MenuItemReporteABC.Text = "Reporte ABC"
        '
        'btImpuestoActividadEconomica
        '
        Me.menuExtender.SetImageIndex(Me.btImpuestoActividadEconomica, -1)
        Me.btImpuestoActividadEconomica.Index = 14
        Me.btImpuestoActividadEconomica.Tag = "frmInformeActividadEconomica"
        Me.btImpuestoActividadEconomica.Text = "Reporte IVA por Actividad Económica"
        '
        'MenuItemLinea09
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea09, -1)
        Me.MenuItemLinea09.Index = 15
        Me.MenuItemLinea09.Text = "-"
        '
        'MenuItemReporteClientes
        '
        Me.MenuItemReporteClientes.Checked = True
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteClientes, -1)
        Me.MenuItemReporteClientes.Index = 16
        Me.MenuItemReporteClientes.RadioCheck = True
        Me.MenuItemReporteClientes.Text = "Reporte de Clientes"
        '
        'MenuItemReporteProveedores
        '
        Me.MenuItemReporteProveedores.Checked = True
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteProveedores, -1)
        Me.MenuItemReporteProveedores.Index = 17
        Me.MenuItemReporteProveedores.RadioCheck = True
        Me.MenuItemReporteProveedores.Text = "Reporte de Proveedores"
        '
        'MenuItemReporteCajas
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteCajas, -1)
        Me.MenuItemReporteCajas.Index = 18
        Me.MenuItemReporteCajas.Text = "Reporte Cajas x Forma de Pago"
        '
        'MenuItem3
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem3, -1)
        Me.MenuItem3.Index = 19
        Me.MenuItem3.Text = "-"
        '
        'MenuItemReporteRecarga
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemReporteRecarga, -1)
        Me.MenuItemReporteRecarga.Index = 20
        Me.MenuItemReporteRecarga.Text = "Reporte de Recargas"
        '
        'MenuItemCambioutilidad
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemCambioutilidad, -1)
        Me.MenuItemCambioutilidad.Index = 21
        Me.MenuItemCambioutilidad.Text = "Reporte Cambio de Utilidad"
        '
        'MenuInterfaz
        '
        Me.MenuInterfaz.Index = 4
        Me.MenuInterfaz.MdiList = True
        Me.MenuInterfaz.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemAyuda, Me.MenuItem8, Me.MenuItemLinea10, Me.MenuItemMaximizar, Me.MenuItemNormal, Me.MenuItemMinimizar, Me.MenuItemLinea11, Me.MenuItemSeguridad, Me.MenuItemLinea12, Me.MenuItemCalculadora, Me.MenuItemLinea13, Me.MenuItemCerrar})
        Me.MenuInterfaz.Text = "Interfaz"
        '
        'MenuItemAyuda
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemAyuda, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemAyuda, 10)
        Me.MenuItemAyuda.Index = 0
        Me.MenuItemAyuda.OwnerDraw = True
        Me.MenuItemAyuda.Text = "Ayuda"
        Me.MenuItemAyuda.Visible = False
        '
        'MenuItem8
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem8, -1)
        Me.MenuItem8.Index = 1
        Me.MenuItem8.Text = "Actualización"
        '
        'MenuItemLinea10
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea10, -1)
        Me.MenuItemLinea10.Index = 2
        Me.MenuItemLinea10.Text = "-"
        Me.MenuItemLinea10.Visible = False
        '
        'MenuItemMaximizar
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemMaximizar, -1)
        Me.MenuItemMaximizar.Index = 3
        Me.MenuItemMaximizar.Text = "Maximizar"
        '
        'MenuItemNormal
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemNormal, -1)
        Me.MenuItemNormal.Index = 4
        Me.MenuItemNormal.Text = "Normal"
        '
        'MenuItemMinimizar
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemMinimizar, -1)
        Me.MenuItemMinimizar.Index = 5
        Me.MenuItemMinimizar.Text = "Minimizar"
        '
        'MenuItemLinea11
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea11, -1)
        Me.MenuItemLinea11.Index = 6
        Me.MenuItemLinea11.Text = "-"
        '
        'MenuItemSeguridad
        '
        Me.menuExtender.SetExtEnable(Me.MenuItemSeguridad, True)
        Me.menuExtender.SetImageIndex(Me.MenuItemSeguridad, 11)
        Me.MenuItemSeguridad.Index = 7
        Me.MenuItemSeguridad.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemPerfilUsuarios, Me.MenuItemCambioUsuarios, Me.MenuItemRegistroUsuarios, Me.MenuItem5, Me.MenuItemRespaldos, Me.MenuItemActivarPOS, Me.MenuItem, Me.MenuItem19, Me.MenuItemFullScreenPVE})
        Me.MenuItemSeguridad.OwnerDraw = True
        Me.MenuItemSeguridad.Text = "Seguridad"
        '
        'MenuItemPerfilUsuarios
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemPerfilUsuarios, -1)
        Me.MenuItemPerfilUsuarios.Index = 0
        Me.MenuItemPerfilUsuarios.Text = "Perfil Usuarios"
        '
        'MenuItemCambioUsuarios
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemCambioUsuarios, -1)
        Me.MenuItemCambioUsuarios.Index = 1
        Me.MenuItemCambioUsuarios.Shortcut = System.Windows.Forms.Shortcut.CtrlU
        Me.MenuItemCambioUsuarios.Text = "Cambio Usuario"
        '
        'MenuItemRegistroUsuarios
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemRegistroUsuarios, -1)
        Me.MenuItemRegistroUsuarios.Index = 2
        Me.MenuItemRegistroUsuarios.Text = "Usuarios"
        '
        'MenuItem5
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem5, -1)
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'MenuItemRespaldos
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemRespaldos, -1)
        Me.MenuItemRespaldos.Index = 4
        Me.MenuItemRespaldos.Text = "Respaldo B.D."
        '
        'MenuItemActivarPOS
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemActivarPOS, -1)
        Me.MenuItemActivarPOS.Index = 5
        Me.MenuItemActivarPOS.Text = "Sistema POS"
        '
        'MenuItem
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem, -1)
        Me.MenuItem.Index = 6
        Me.MenuItem.Shortcut = System.Windows.Forms.Shortcut.F12
        Me.MenuItem.Text = "Capturar Pantalla"
        '
        'MenuItem19
        '
        Me.menuExtender.SetImageIndex(Me.MenuItem19, -1)
        Me.MenuItem19.Index = 7
        Me.MenuItem19.Text = "-"
        '
        'MenuItemFullScreenPVE
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemFullScreenPVE, -1)
        Me.MenuItemFullScreenPVE.Index = 8
        Me.MenuItemFullScreenPVE.Text = "Full Screen P.VE"
        '
        'MenuItemLinea12
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea12, -1)
        Me.MenuItemLinea12.Index = 8
        Me.MenuItemLinea12.Text = "-"
        '
        'MenuItemCalculadora
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemCalculadora, -1)
        Me.MenuItemCalculadora.Index = 9
        Me.MenuItemCalculadora.Text = "Calculadora"
        '
        'MenuItemLinea13
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemLinea13, -1)
        Me.MenuItemLinea13.Index = 10
        Me.MenuItemLinea13.Text = "-"
        '
        'MenuItemCerrar
        '
        Me.menuExtender.SetImageIndex(Me.MenuItemCerrar, -1)
        Me.MenuItemCerrar.Index = 11
        Me.MenuItemCerrar.MdiList = True
        Me.MenuItemCerrar.Text = "Cerrar"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 5
        Me.MenuItem4.Text = ""
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 50
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.StatusBar1.Location = New System.Drawing.Point(0, 461)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel4, Me.StatusBarPanel2, Me.StatusBarPanel3, Me.StatusBarPanel5})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(784, 16)
        Me.StatusBar1.TabIndex = 9
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.StatusBarPanel1.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.StatusBarPanel1.Icon = CType(resources.GetObject("StatusBarPanel1.Icon"), System.Drawing.Icon)
        Me.StatusBarPanel1.Name = "StatusBarPanel1"
        Me.StatusBarPanel1.Width = 31
        '
        'StatusBarPanel4
        '
        Me.StatusBarPanel4.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.StatusBarPanel4.Name = "StatusBarPanel4"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.StatusBarPanel2.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.StatusBarPanel2.Icon = CType(resources.GetObject("StatusBarPanel2.Icon"), System.Drawing.Icon)
        Me.StatusBarPanel2.Name = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 47
        '
        'StatusBarPanel3
        '
        Me.StatusBarPanel3.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.StatusBarPanel3.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.StatusBarPanel3.Name = "StatusBarPanel3"
        Me.StatusBarPanel3.Text = "XX"
        Me.StatusBarPanel3.Width = 29
        '
        'StatusBarPanel5
        '
        Me.StatusBarPanel5.Name = "StatusBarPanel5"
        Me.StatusBarPanel5.Text = "v. "
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        Me.ImageList1.Images.SetKeyName(7, "")
        Me.ImageList1.Images.SetKeyName(8, "")
        Me.ImageList1.Images.SetKeyName(9, "")
        '
        'HelpProvider
        '
        Me.HelpProvider.HelpNamespace = "Ayuda\Ayuda Lcpymes.chm"
        '
        'imageList
        '
        Me.imageList.ImageStream = CType(resources.GetObject("imageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imageList.TransparentColor = System.Drawing.Color.Transparent
        Me.imageList.Images.SetKeyName(0, "")
        Me.imageList.Images.SetKeyName(1, "")
        Me.imageList.Images.SetKeyName(2, "")
        Me.imageList.Images.SetKeyName(3, "")
        Me.imageList.Images.SetKeyName(4, "")
        Me.imageList.Images.SetKeyName(5, "")
        Me.imageList.Images.SetKeyName(6, "")
        Me.imageList.Images.SetKeyName(7, "")
        Me.imageList.Images.SetKeyName(8, "")
        Me.imageList.Images.SetKeyName(9, "")
        Me.imageList.Images.SetKeyName(10, "")
        Me.imageList.Images.SetKeyName(11, "")
        Me.imageList.Images.SetKeyName(12, "")
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "")
        Me.ImageList2.Images.SetKeyName(1, "")
        Me.ImageList2.Images.SetKeyName(2, "")
        Me.ImageList2.Images.SetKeyName(3, "")
        Me.ImageList2.Images.SetKeyName(4, "")
        Me.ImageList2.Images.SetKeyName(5, "")
        Me.ImageList2.Images.SetKeyName(6, "")
        Me.ImageList2.Images.SetKeyName(7, "")
        Me.ImageList2.Images.SetKeyName(8, "")
        Me.ImageList2.Images.SetKeyName(9, "")
        Me.ImageList2.Images.SetKeyName(10, "")
        Me.ImageList2.Images.SetKeyName(11, "")
        Me.ImageList2.Images.SetKeyName(12, "")
        Me.ImageList2.Images.SetKeyName(13, "")
        Me.ImageList2.Images.SetKeyName(14, "")
        Me.ImageList2.Images.SetKeyName(15, "")
        Me.ImageList2.Images.SetKeyName(16, "")
        Me.ImageList2.Images.SetKeyName(17, "")
        Me.ImageList2.Images.SetKeyName(18, "")
        Me.ImageList2.Images.SetKeyName(19, "")
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButtonInventario, Me.ToolBarButton1, Me.ToolBarButtonProveedores, Me.ToolBarButton2, Me.ToolBarButtonClientes, Me.ToolBarButton3, Me.ToolBarButtonCompras, Me.ToolBarButton4, Me.ToolBarButtonEstadocuenta, Me.ToolBarButton5, Me.ToolBarButtonFactura})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(115, 55)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolBar1.ImageList = Me.ImageList2
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(784, 58)
        Me.ToolBar1.TabIndex = 25
        '
        'ToolBarButtonInventario
        '
        Me.ToolBarButtonInventario.ImageIndex = 13
        Me.ToolBarButtonInventario.Name = "ToolBarButtonInventario"
        Me.ToolBarButtonInventario.Text = "Inventario"
        Me.ToolBarButtonInventario.ToolTipText = "Registro de Inventario"
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.Name = "ToolBarButton1"
        Me.ToolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'ToolBarButtonProveedores
        '
        Me.ToolBarButtonProveedores.ImageIndex = 14
        Me.ToolBarButtonProveedores.Name = "ToolBarButtonProveedores"
        Me.ToolBarButtonProveedores.Text = "Proveedores"
        Me.ToolBarButtonProveedores.ToolTipText = "Registro de Proveedores"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.Name = "ToolBarButton2"
        Me.ToolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'ToolBarButtonClientes
        '
        Me.ToolBarButtonClientes.ImageIndex = 19
        Me.ToolBarButtonClientes.Name = "ToolBarButtonClientes"
        Me.ToolBarButtonClientes.Text = "Clientes"
        Me.ToolBarButtonClientes.ToolTipText = "Clientes"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.Name = "ToolBarButton3"
        Me.ToolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'ToolBarButtonCompras
        '
        Me.ToolBarButtonCompras.ImageIndex = 17
        Me.ToolBarButtonCompras.Name = "ToolBarButtonCompras"
        Me.ToolBarButtonCompras.Text = "Compras"
        Me.ToolBarButtonCompras.ToolTipText = "Registro de compras"
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.Name = "ToolBarButton4"
        Me.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'ToolBarButtonEstadocuenta
        '
        Me.ToolBarButtonEstadocuenta.ImageIndex = 18
        Me.ToolBarButtonEstadocuenta.Name = "ToolBarButtonEstadocuenta"
        Me.ToolBarButtonEstadocuenta.Text = "Estado de Cuenta"
        Me.ToolBarButtonEstadocuenta.ToolTipText = "Estados de cuenta"
        '
        'ToolBarButton5
        '
        Me.ToolBarButton5.Name = "ToolBarButton5"
        Me.ToolBarButton5.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
        '
        'ToolBarButtonFactura
        '
        Me.ToolBarButtonFactura.ImageIndex = 16
        Me.ToolBarButtonFactura.Name = "ToolBarButtonFactura"
        Me.ToolBarButtonFactura.Text = "Facturación"
        Me.ToolBarButtonFactura.ToolTipText = "Facturacion"
        '
        'menuExtender
        '
        Me.menuExtender.Font = Nothing
        Me.menuExtender.ImageList = Me.imageList
        Me.menuExtender.SystemFont = CType(configurationAppSettings.GetValue("menuExtender.SystemFont", GetType(Boolean)), Boolean)
        '
        'btnAnalisisVenta
        '
        Me.menuExtender.SetExtEnable(Me.btnAnalisisVenta, True)
        Me.menuExtender.SetImageIndex(Me.btnAnalisisVenta, -1)
        Me.btnAnalisisVenta.Index = 11
        Me.btnAnalisisVenta.OwnerDraw = True
        Me.btnAnalisisVenta.Text = "Análisis de venta"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(232, 96)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(240, 192)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 23
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(784, 477)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.ToolBar1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Menu = Me.MainMenu1
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Public Function GetVersionPublicacion() As String

        Dim ver As String = ""
        If Deployment.Application.ApplicationDeployment.IsNetworkDeployed Then
            Dim ad As Deployment.Application.ApplicationDeployment
            ad = Deployment.Application.ApplicationDeployment.CurrentDeployment
            ver = ad.CurrentVersion.ToString()
        End If

        Return ver
    End Function
#Region "Controles del Menu Principal"
    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.DoubleBuffered = True
        Inicialza_reporteFactura()
        Dim fun As New Conexion
        Dim Nom_Empresa As String

        StatusBarPanel5.Text = "     V. " & GetVersionPublicacion()

        Dim ImportarXML As String = GetSetting("Seesoft", "Seepos", "ImportarXML")
        If ImportarXML = "" Or ImportarXML = "0" Then
            SaveSetting("Seesoft", "Seepos", "ImportarXML", "0")
            MenuItemConfirmar.Visible = False
            MenuItemReporteHacienda.Visible = False
        ElseIf ImportarXML = "1" Then
            MenuItemConfirmar.Visible = True
            MenuItemReporteHacienda.Visible = True
        End If

        Nom_Empresa = fun.SlqExecuteScalar(fun.Conectar, "SELECT Empresa FROM configuraciones")
        Me.Text = "LcPymes SeePOS (" & Nom_Empresa & ")   " & fun.sQlconexion.WorkstationId & "  -->  " & fun.sQlconexion.DataSource & "(" & fun.sQlconexion.Database & ")         "
        fun.DesConectar(fun.sQlconexion)
        Contador = 1
        TITULO = Me.Text
        If Not GetSetting("SeeSoft", "Seguridad", "Segura").Equals("1") Then
            Perfil_Usuario()
        End If
        spMostrarMenu()
        MenuItemActivarPOS.Checked = IIf(GetSetting("SeeSOFT", "SeePOS", "PVE", "0") = 0, False, True)
        MenuItemFullScreenPVE.Checked = IIf(GetSetting("SeeSOFT", "SeePOS", "PVEFullScreen", "0") = 0, False, True)
        Dim imagen As New Drawing.Bitmap(Me.PictureBox2.Image, Me.Width, Me.Height)
        Me.BackgroundImage = imagen
        cargarSucursal()
    End Sub
    Private Sub spMostrarMenu()
        Dim PremiosLealtad As Boolean = False
        Dim Recarga As Boolean = False
        Dim MostrarRazonAjuste As Boolean = False
        Dim MostrarReporteHistoricoCierre As Boolean = False
        Dim MostrarUtilidadXProveedor As Boolean = False

        If GetSetting("Seesoft", "SeePos", "PremiosLealtad") = "1" Then PremiosLealtad = True Else SaveSetting("Seesoft", "SeePos", "PremiosLealtad", "0")
        If GetSetting("Seesoft", "SeePos", "Recarga") = "1" Then Recarga = True Else SaveSetting("Seesoft", "SeePos", "Recarga", "0")
        If GetSetting("Seesoft", "SeePos", "MostrarRazonAjuste") = "1" Then MostrarRazonAjuste = True Else SaveSetting("Seesoft", "SeePos", "MostrarRazonAjuste", "0")
        If GetSetting("Seesoft", "SeePos", "MostrarHistoricoCierre") = "1" Then MostrarReporteHistoricoCierre = True Else SaveSetting("Seesoft", "SeePos", "MostrarHistoricoCierre", "0")
        If GetSetting("Seesoft", "SeePos", "MostrarUtilidadXProveedor") = "1" Then MostrarUtilidadXProveedor = True Else SaveSetting("Seesoft", "SeePos", "MostrarUtilidadXProveedor", "0")

        Me.MenuItemPromociones.Visible = PremiosLealtad
        Me.MenuItemPaquetes.Visible = PremiosLealtad
        Me.MenuItemReporteRecarga.Visible = Recarga
        MenuItemReporteAjusteInventarioSalida.Visible = MostrarRazonAjuste
        MenuItemHistoricoCierreCaja.Visible = MostrarReporteHistoricoCierre
        MenuItemUtilidadXProveedor.Visible = MostrarUtilidadXProveedor
        Me.MenuItemCambioutilidad.Visible = MostrarUtilidadXProveedor
    End Sub

    Private Sub Perfil_Usuario()
        Dim EnEspera As New DevExpress.Utils.WaitDialogForm
        EnEspera.Caption = "Habilitando accesos a módulos autorizados..."
        EnEspera.Text = "Validando Acceso a LcPymes..."
        EnEspera.Show()
        EnEspera.Caption = "Validando Acceso a LcPymes..."
        Me.Hide()

        '********************************************ADMINISTRACION*******************************************************************
        EnEspera.Caption = "Módulos de Administración..."
        Me.MenuAdministracion.Visible = True
        Application.DoEvents()

        Me.MenuItemAdministracion_Clientes.Enabled = VerificandoAcceso_a_Modulos("Frmcliente", "Administrador de Clientes", usua.Cedula, "Administración")
        Me.MenuItemComisionista.Enabled = VerificandoAcceso_a_Modulos("Comisionista", "Administrador de Comisionistas", usua.Cedula, "Administración")
        Me.MenuItemAdministracion_Proveedores.Enabled = VerificandoAcceso_a_Modulos("frmProveedores", "Administrador de Proveedores", usua.Cedula, "Administración")
        Me.MenuItemAdministracion_Moneda.Enabled = VerificandoAcceso_a_Modulos("FrmMoneda", "Moneda", usua.Cedula, "Administración")
        Me.MenuItemAdministracion_Tarjetas.Enabled = VerificandoAcceso_a_Modulos("TipoTarjeta", "Operadores de Tarjetas", usua.Cedula, "Administración")
        Me.MenuItemAdministracion_ConfiguracionMoneda.Enabled = VerificandoAcceso_a_Modulos("ConfiguracionMoneda", "Configuración Moneda", usua.Cedula, "Administración")
        Me.MenuItemAdministracion_ConfiguracionSistema.Enabled = VerificandoAcceso_a_Modulos("FrmConfiguracion", "Configuración", usua.Cedula, "Administración")
        btnAnalisisVenta.Enabled = VerificandoAcceso_a_Modulos("FrmConfiguracionAvanzada", "Analisis de venta", usua.Cedula, "Administracion")
        '*******************************************************************************************************

        '********************************************MENU OPERACIONES********************************************
        EnEspera.Caption = "Módulos de Operaciones..."
        Me.MenuOperaciones.Visible = True
        Application.DoEvents()
        '*********************************************************OPERACIONES->VENTAS****************************
        MenuItemOperacion_Facturacion.Enabled = VerificandoAcceso_a_Modulos("Facturacion", "Facturación", usua.Cedula, "Operaciones Ventas")
        MenuItemOperacion_Proformas.Enabled = VerificandoAcceso_a_Modulos("Frm_Cotizacion", "Cotizaciones", usua.Cedula, "Operaciones Ventas")
        MenuItemOperacion_DevolucionesVentas.Enabled = VerificandoAcceso_a_Modulos("FrmDevolucionesVentas", "Devoluciones", usua.Cedula, "Operaciones Ventas")
        MenuItemOperacionesComisiones.Enabled = VerificandoAcceso_a_Modulos("PagoComisiones", "Pago de Comisiones", usua.Cedula, "Operaciones Ventas")
        btnAnalisisVenta.Enabled = VerificandoAcceso_a_Modulos("FrmConfiguracionAvanzada", "Analisis de venta", usua.Cedula, "Administracion")
        MenuItemModificaFactura.Visible = VerificandoAcceso_a_Modulos("Modifica_Facturacion", "Modificar Factura de Venta", usua.Cedula, "Operaciones Ventas")
        'NO DEBE APARECER PARA TODAS LAS DE FELIPE!
        '*******************************************************************************************************
        '**********************OPERACIONES->CUENTAS POR COBRAR**************************************************
        MenuItemOperacion_AbonosCXC.Enabled = VerificandoAcceso_a_Modulos("frmReciboDinero", "CxC-Recibo de Dinero", usua.Cedula, "Operaciones Cuentas por Cobrar")
        MenuItemOperacion_AjustesCXC.Enabled = VerificandoAcceso_a_Modulos("frmAjustecCobrar", "CxC-Ajuste a Cuentas", usua.Cedula, "Operaciones Cuentas por Cobrar")
        MenuItemOperacion_EstadoCuentaCXC.Enabled = VerificandoAcceso_a_Modulos("frmEstado_CXC", "CxC-Estado de Cuentas", usua.Cedula, "Operaciones Cuentas por Cobrar")
        '********************************************************************************************************
        '**********************OPERACIONES->COMPRAS*************************************************************
        MenuItemOperacion_ComprasProveedor.Enabled = VerificandoAcceso_a_Modulos("frmCompra", "Registro de Compras", usua.Cedula, "Operaciones Compras")
        MenuItemOperacion_OrdenCompraManual.Enabled = VerificandoAcceso_a_Modulos("frmOrdenCompra", "Orden de Compra Manual", usua.Cedula, "Operaciones Compras")
        MenuItemOperacion_OrdenCompraAutomatica.Enabled = VerificandoAcceso_a_Modulos("frmOrdenCompraAutomatica", "Orden de Compra Automática", usua.Cedula, "Operaciones Compras")
        MenuItemOperacion_DevolucionesProveedor.Enabled = VerificandoAcceso_a_Modulos("Devoluciones_Compras", "Devoluciones de Compras", usua.Cedula, "Operaciones Compras")
        '*******************************************************************************************************
        '***********************OPERACIONES->CUENTAS POR PAGAR***************************************************
        MenuItemOperacion_AbonosCuentaProveedor.Enabled = VerificandoAcceso_a_Modulos("Abonos_Proveedor", "Abono a Proveedor", usua.Cedula, "Operaciones Cuentas por Pagar")
        MenuItemOperacion_AjusteCuentaProveedor.Enabled = VerificandoAcceso_a_Modulos("frmAjustePagar", "Ajustes de Cuentas por Pagar", usua.Cedula, "Operaciones Cuentas por Pagar")
        MenuItemOperacion_EstadoCuentaProveedor.Enabled = VerificandoAcceso_a_Modulos("frmEstado_CXP", "Estado de Cuentas por Pagar", usua.Cedula, "Operaciones Cuentas por Pagar")
        MenuItem1.Enabled = VerificandoAcceso_a_Modulos("frmGasto", "Registro de Gastos (Otras CXP)", usua.Cedula, "Cuentas por Pagar")
        '*******************************************************************************************************

        '*************************OPERACIONES->CONTROL DE CAJA***************************************************************
        MenuItemOperacion_Caja_Apertura.Enabled = VerificandoAcceso_a_Modulos("AperturaCaja", "Apertura de Caja", usua.Cedula, "Operaciones Control de Caja")
        MenuItemOperacion_Caja_Arqueo.Enabled = VerificandoAcceso_a_Modulos("ArqueoCaja", "Arqueo de Caja", usua.Cedula, "Operaciones Control de Caja")
        MenuItemOperacion_Caja_Cierre.Enabled = VerificandoAcceso_a_Modulos("CierreCaja", "Cierre Caja", usua.Cedula, "Operaciones Control de Caja")
        MenuItemOperacion_Caja_OpcionesPago.Enabled = VerificandoAcceso_a_Modulos("frmMovimientoCajaPago", "Movimiento Caja de Pago", usua.Cedula, "Operaciones Control de Caja")
        MenuItemOperacion_Caja_Movimientos.Enabled = VerificandoAcceso_a_Modulos("MovimientoCaja", "Movimiento de Caja", usua.Cedula, "Operaciones Control de Caja")
        '*******************************************************************************************************


        '********************************************************MENU INVENTARIOS********************************
        EnEspera.Caption = "Módulos de Inventarios ..."
        Me.MenuInventarios.Visible = True
        Application.DoEvents()

        MenuItemInventario_Ajuste_Bodegas.Enabled = VerificandoAcceso_a_Modulos("AjusteInventario", "Ajuste de Inventario", usua.Cedula, "Inventarios")
        Me.MenuItemInventario_Ajustes.Enabled = VerificandoAcceso_a_Modulos("AjusteInventario", "Ajuste de Inventario", usua.Cedula, "Inventarios")
        MenuItemInventario_Aumentos_X_Categoria.Enabled = VerificandoAcceso_a_Modulos("frmAumento", "Aumentos", usua.Cedula, "Inventarios")
        MenuItemInventario_Bodegas.Enabled = VerificandoAcceso_a_Modulos("FrmBodegas", "Bodegas", usua.Cedula, "Inventarios")
        MenuItemInventario_Catalogo.Enabled = VerificandoAcceso_a_Modulos("FrmInventario", "Inventarios", usua.Cedula, "Inventarios")
        ValidaEtiquetador()
        MenuItemInventario_Familias.Enabled = VerificandoAcceso_a_Modulos("Familia", "Familia", usua.Cedula, "Inventarios")
        MenuItemInventario_Marcas.Enabled = VerificandoAcceso_a_Modulos("FrmMarca", "Marca", usua.Cedula, "Inventarios")
        MenuItemInventario_Presentaciones.Enabled = VerificandoAcceso_a_Modulos("FrmPresentaciones", "Presentaciones", usua.Cedula, "Inventarios")
        MenuItemInventario_Ubicaciones.Enabled = VerificandoAcceso_a_Modulos("frmUbicacion", "Ubicaciones", usua.Cedula, "Inventarios")
        MenuItemTomaFisica.Enabled = VerificandoAcceso_a_Modulos("Toma_Fisica", "Toma Fisica", usua.Cedula, "Inventarios")
        MenuItemTomaFisica.Enabled = VerificandoAcceso_a_Modulos("Toma_Fisica2", "Toma Fisica 2", usua.Cedula, "Inventarios")
        MenuItemTraslados.Enabled = VerificandoAcceso_a_Modulos("Traslados", "Traslados de Bodega", usua.Cedula, "Inventarios")
        Menu_Importar_Excel.Enabled = VerificandoAcceso_a_Modulos("Importar_Excel", "Importar de Excel", usua.Cedula, "Inventarios")

        '***************************************************************************************************************

        '*************************************************** MENU REPORTES******************************************
        EnEspera.Caption = "Acceso a Reportes..."
        Me.MenuReportes.Visible = True
        Application.DoEvents()

        MenuItemRedondeo.Enabled = VerificandoAcceso_a_Modulos("FrmRedondeoInventario", "Redondeo Inventario", usua.Cedula, "Reportes")
        MenuItemReporte_Inventario.Enabled = VerificandoAcceso_a_Modulos("frmOpcionesVisualizacion", "Reportes de Inventario", usua.Cedula, "Reportes")
        MenuItemReporte_Kardex.Enabled = VerificandoAcceso_a_Modulos("FrmKardex", "Visualiazdor de Kardex", usua.Cedula, "Reportes")
        MenuItemMovArt.Enabled = VerificandoAcceso_a_Modulos("MovimientoArticulos", "Movimiento de Articulos", usua.Cedula, "Reportes")
        MenuItemReporteABON_CRE_NDEB.Enabled = VerificandoAcceso_a_Modulos("frmReciboAbonos", "Recibo Abonos", usua.Cedula, "Reportes")
        MenuItemReportesCXP.Enabled = VerificandoAcceso_a_Modulos("frmReportesCXP", "Reportes CXP (ABO,NCRE,NDEB)", usua.Cedula, "Reportes")
        MenuItemReporteClientes.Enabled = VerificandoAcceso_a_Reportes("Clientes.rpt", "Reporte de Clientes", usua.Cedula, "Reportes")
        MenuItemReporteCompras.Enabled = VerificandoAcceso_a_Modulos("frmReporteCompras", "Reporte Compras", usua.Cedula, "Reportes")
        MenuItemReporteProveedores.Enabled = VerificandoAcceso_a_Reportes("Proveedores.rpt", "Reporte de Proveedores", usua.Cedula, "Reportes")
        MenuItemReporteVentas.Enabled = VerificandoAcceso_a_Modulos("frmReporteVentas", "Reporte Ventas", usua.Cedula, "Reportes")
        MenuItemReporteCajas.Enabled = VerificandoAcceso_a_Modulos("FrmReportesCajas", "Reportes Forma de Pago en Caja", usua.Cedula, "Reportes")
        MenuItemReporteComisiones.Enabled = VerificandoAcceso_a_Modulos("frmReporteComisiones", "Reportes de Comisiones", usua.Cedula, "Reportes")
        MenuItemReporteABC.Enabled = VerificandoAcceso_a_Modulos("frmReporte_ABC", "Reportes ABC", usua.Cedula, "Reportes")
        MenuItemReporteApartados.Enabled = VerificandoAcceso_a_Modulos("frmReporteApartados", "Reportes de Apartados", usua.Cedula, "Reportes")
        '***************************************************************************************************************

        '*************************INTERFAZ->SEGURIDAD************************************************************************
        MenuItemRespaldos.Enabled = VerificandoAcceso_a_Modulos("FrmRespaldo", "Respaldo Base Datos", usua.Cedula, "Seguridad")
        MenuItemPerfilUsuarios.Enabled = VerificandoAcceso_a_Modulos("AgregarPerfil", "Crear Perfiles de Usuario", usua.Cedula, "Seguridad")
        MenuItemRegistroUsuarios.Enabled = VerificandoAcceso_a_Modulos("Frmusuario", "Administrador de Usuario", usua.Cedula, "Seguridad")
        '********************************************************************************************************************

        '************************* TOOLBAR ***************************************************************
        ToolBarButtonInventario.Enabled = MenuItemInventario_Catalogo.Enabled
        ToolBarButtonProveedores.Enabled = MenuItemAdministracion_Proveedores.Enabled
        ToolBarButtonClientes.Enabled = MenuItemAdministracion_Clientes.Enabled
        ToolBarButtonCompras.Enabled = MenuItemOperacion_ComprasProveedor.Enabled
        ToolBarButtonEstadocuenta.Enabled = MenuItemOperacion_EstadoCuentaProveedor.Enabled
        ToolBarButtonFactura.Enabled = MenuItemOperacion_Facturacion.Enabled
        '*************************************************************************************************

        'AgregarPerfil
        If Verificar_Acceso_minimo_modulo("AgregarPerfil") Then
            MsgBox("NO SE ENCUENTRA EN LOS MODULOS DE SEGURIDAD NINGUN USUARIO QUE TENGA ACCESO A PERFILES", MsgBoxStyle.Information, "Alerta..")
            Me.MenuItemPerfilUsuarios.Enabled = True
            Me.MenuItemRegistroUsuarios.Enabled = True
        End If

        Me.MenuInterfaz.Visible = True
        EnEspera.Caption = "Validación Finalizada..."
        Application.DoEvents()
        EnEspera.Caption = ""
        EnEspera.Close()
        EnEspera.Dispose()
        Me.Show()
    End Sub

    Public Function Verificar_Acceso_minimo_modulo(ByVal Modulo As String) As Boolean
        Try
            Dim Reader As System.Data.SqlClient.SqlDataReader
            Dim Cx As New Conexion
            Reader = Cx.GetRecorset(Cx.Conectar("Seguridad"), "SELECT dbo.Usuarios.Id_Usuario, dbo.Modulos.Modulo_Nombre_Interno, dbo.Modulos.Modulo_Nombre, dbo.Perfil_x_Modulo.Accion_Ejecucion,  dbo.Perfil_x_Modulo.Accion_Actualizacion, dbo.Perfil_x_Modulo.Accion_Eliminacion, dbo.Perfil_x_Modulo.Accion_Busqueda,  dbo.Perfil_x_Modulo.Accion_Impresion, dbo.Perfil_x_Modulo.Accion_Opcion FROM dbo.Modulos INNER JOIN  dbo.Perfil_x_Modulo ON dbo.Modulos.Id_modulo = dbo.Perfil_x_Modulo.Id_Modulo INNER JOIN  dbo.Perfil ON dbo.Perfil_x_Modulo.Id_Perfil = dbo.Perfil.Id_Perfil INNER JOIN  dbo.Perfil_x_Usuario ON dbo.Perfil.Id_Perfil = dbo.Perfil_x_Usuario.Id_Perfil INNER JOIN  dbo.Usuarios ON dbo.Perfil_x_Usuario.Id_Usuario = dbo.Usuarios.Id_Usuario WHERE dbo.Modulos.Modulo_Nombre_Interno = '" & Modulo & "'")
            If Reader.Read() Then Return False Else Return True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Function

    Private Sub ValidaEtiquetador()
        'VALIDA EL ACCESO AL ETIQUETADOR DESDE EL MODULO PRINCIPAL 30042007 JCGA
        Dim f As Boolean = False
        Dim s(50) As Integer
        Dim w(50) As Integer
        Dim z(50) As Integer
        MenuItemInventario_Etiquetas.Enabled = VerificandoAcceso_a_Modulos("frmEtiquetasProductos", "Etiquetas de Artículos", usua.Cedula, "Inventarios")
    End Sub

    Public Function ScrollText(ByVal strText As String) As String
        strText = (Microsoft.VisualBasic.Left(strText, Len(strText) + 1)) & Microsoft.VisualBasic.Right(strText, 1)
        ScrollText = strText
    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Text = Microsoft.VisualBasic.Mid(TITULO, 1, Contador)
        Contador = Contador + 1
        If Contador = Microsoft.VisualBasic.Len(TITULO) Then Contador = 1
        Me.StatusBarPanel1.Text = Date.Now
        Me.StatusBarPanel2.Text = usua.Nombre
        Me.StatusBarPanel3.Text = usua.Perfil
    End Sub

    'Private Sub MainForm_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
    '	Me.Width = 800
    '	Me.Height = 600
    '	Dim imagen As New Drawing.Bitmap(Me.PictureBox2.Image, Me.Width, Me.Height)
    '	Me.BackgroundImage = imagen
    'End Sub
#End Region

#Region "Menu ADMINISTRACIÓN"
    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAdministracion_Clientes.Click
        Me.CargarForm(New Frmcliente(usua))
    End Sub

    Private Sub MenuItem1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemComisionista.Click
        CargarForm(New Comisionista)
    End Sub

    Private Sub MenuItem12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAdministracion_Proveedores.Click
        CargarForm(New frmProveedores(usua))
    End Sub

    Private Sub MenuItemAdministracion_Moneda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAdministracion_Moneda.Click
        CargarForm(New FrmMoneda(usua))
    End Sub

    Private Sub MenuItemAdministracion_Tarjetas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAdministracion_Tarjetas.Click
        CargarForm(New TipoTarjeta)
    End Sub

    Private Sub MenuItemAdministracion_ConfiguracionMoneda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAdministracion_ConfiguracionMoneda.Click
        CargarForm(New ConfiguracionMoneda)
    End Sub

    Private Sub MenuItemAdministracion_ConfiguracionSistema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAdministracion_ConfiguracionSistema.Click
        CargarForm(New FrmConfiguracion)
    End Sub
    Private Sub btnAnalisisVenta_Click(sender As Object, e As EventArgs) Handles btnAnalisisVenta.Click
        '  CargarForm(New FrmConfiguracionAvanzada(usua.Cedula))
        Dim frm As New FrmConfiguracionAvanzada(usua.Cedula)
        frm.ShowDialog()
    End Sub
#End Region

#Region "Menu OPERACIONES"
    '---------------------------------------------------------------------------------- 
    '       VENTAS Y CUENTAS POR COBRAR
    Private Sub MenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_Facturacion.Click
        Facturacion()
    End Sub
    Private Sub MenuItem15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_Proformas.Click
        CargarForm(New Frm_Cotizacion(usua))
    End Sub
    Private Sub MenuItem6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_DevolucionesVentas.Click
        CargarForm(New FrmDevolucionesVentas)
    End Sub

    Private Sub MenuItemOperacionesComisiones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacionesComisiones.Click
        CargarForm(New PagoComisiones(usua))
    End Sub

    Private Sub MenuItemModificaFactura_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemModificaFactura.Click
        CargarForm(New Modifica_Facturacion(usua))
    End Sub

    '---------------------------------------------------------------------------------- 
    Private Sub MenuItem34_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_AbonosCXC.Click
        CargarForm(New frmReciboDinero(usua))
    End Sub

    Private Sub MenuItem51_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_AjustesCXC.Click
        CargarForm(New frmAjustecCobrar)
    End Sub

    Private Sub MenuItem48_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_EstadoCuentaCXC.Click
        CargarForm(New frmEstado_CXC)
    End Sub

    '---------------------------------------------------------------------------------- 
    '       COMPRAS
    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_ComprasProveedor.Click
        CargarForm(New frmCompra(usua))
    End Sub

    Private Sub MenuItem76_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_OrdenCompraAutomatica.Click
        CargarForm(New frmOrdenCompraAutomatica(usua))
    End Sub

    Private Sub MenuItem75_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_OrdenCompraManual.Click
        CargarForm(New frmOrdenCompra)
    End Sub

    Private Sub MenuItem39_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_DevolucionesProveedor.Click
        CargarForm(New Devoluciones_Compras(usua))
    End Sub

    '---------------------------------------------------------------------------------- 
    '       CUENTAS POR PAGAR
    Private Sub MenuItem55_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_AbonosCuentaProveedor.Click
        CargarForm(New Abonos_Proveedor(usua))
    End Sub

    Private Sub MenuItem57_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_AjusteCuentaProveedor.Click
        CargarForm(New frmAjustePagar(usua))
    End Sub

    Private Sub MenuItem60_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_EstadoCuentaProveedor.Click
        CargarForm(New frmEstado_CXP)
    End Sub
    '---------------------------------------------------------------------------------- 
    '       CONTROL DE CAJAS
    Private Sub MenuItem32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_Caja_Apertura.Click
        CargarForm(New AperturaCaja(usua))
    End Sub

    Private Sub MenuItemOperacion_Caja_Arqueo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_Caja_Arqueo.Click
        If GetSetting("SeeSoft", "SeePOS", "Arqueo").Equals("SIMPLE") Then
            CargarForm(New frmArqueo)
        Else
            CargarForm(New ArqueoCaja)
            SaveSetting("SeeSoft", "SeePOS", "Arqueo", "")

        End If

    End Sub

    Private Sub MenuItem33_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_Caja_Cierre.Click
        CargarForm(New CierreCaja)
    End Sub

    Private Sub MenuItem36_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_Caja_OpcionesPago.Click
        CargarForm(New frmMovimientoCajaPago(usua))
    End Sub

    Private Sub MenuItem70_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOperacion_Caja_Movimientos.Click
        CargarForm(New MovimientoCaja(usua))
    End Sub
#End Region

#Region "Menu INVENTARIOS"
    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Catalogo.Click
        CargarForm(New FrmInventario(usua))
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Familias.Click
        CargarForm(New Familia(usua))
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Ubicaciones.Click
        CargarForm(New frmUbicacion(usua))
    End Sub

    Private Sub MenuItem37_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Presentaciones.Click
        CargarForm(New FrmPresentaciones(usua))
    End Sub

    Private Sub MenuItem40_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Marcas.Click
        CargarForm(New FrmMarca(usua))
    End Sub

    Private Sub MenuItem43_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Ajustes.Click
        CargarForm(New AjusteInventario(usua))
    End Sub

    Private Sub MenuItem46_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Etiquetas.Click
        Try
            If System.IO.File.Exists(Application.StartupPath & "\Etiquetas\Etiquetador.exe") = True Then
                Dim proc As New System.Diagnostics.Process
                proc.StartInfo.FileName = Application.StartupPath & "\Etiquetas\Etiquetador.exe"
                proc.Start()
            Else
                MsgBox("El sistema de Etiquetación EXTERNO no se encuentra Instalado." & vbCrLf & "Verifique su Instalación y su ruta...." & vbCrLf & Application.StartupPath & "\Etiquetas\Etiquetador.exe" & vbCrLf & "Se procede a ejecutar internamente", MsgBoxStyle.Critical, "Atención...")
                Dim f As Boolean = False
                Dim s(50) As String
                Dim w(50) As Integer
                Dim z(50) As Integer
                CargarForm(New frmEtiquetasProductos(f, s, w, z))
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub

    Private Sub MenuItem23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Aumentos_X_Categoria.Click
        CargarForm(New frmAumento)
    End Sub

    Private Sub MenuItem61_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemRedondeo.Click
        CargarForm(New FrmRedondeoInventario(usua))
    End Sub

    Private Sub MenuItem72_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Bodegas.Click
        CargarForm(New FrmBodegas(usua))
    End Sub

    Private Sub MenuItem73_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemInventario_Ajuste_Bodegas.Click
        CargarForm(New AjusteInventario(usua))
    End Sub

    Private Sub MenuItemTomaFisica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemTomaFisica.Click
        Dim FormToma_Fisica As Integer = 1
        Try
            FormToma_Fisica = GetSetting("SeeSOFT", "SeePos", "FormToma_Fisica")

        Catch ex As Exception
            SaveSetting("SeeSOFT", "SeePos", "FormToma_Fisica", "1")
            FormToma_Fisica = 1
        End Try
        If FormToma_Fisica = 1 Then
            CargarForm(New Toma_Fisica(usua))
        ElseIf FormToma_Fisica = 2 Then
            CargarForm(New Toma_Fisica2(usua))
        End If
    End Sub

    Private Sub MenuItem1_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemTraslados.Click
        CargarForm(New Traslados(usua, GetSetting("SeeSOFT", "SeePOS", "Conexion")))
    End Sub
#End Region

#Region "Menu REPORTES"
    Private Sub MenuItem25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteVentas.Click
        CargarForm(New frmReporteVentas(usua.Cedula))
    End Sub

    Private Sub MenuItem27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteCompras.Click
        CargarForm(New frmReporteCompras)
    End Sub

    Private Sub MenuItemReporteComisiones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteComisiones.Click
        CargarForm(New frmReporteComisiones)
    End Sub

    Private Sub MenuItemReporteApartados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteApartados.Click
        CargarForm(New frmReporteApartados)
    End Sub

    Private Sub MenuItem52_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteABON_CRE_NDEB.Click
        CargarForm(New frmReciboAbonos)
    End Sub

    Private Sub MenuItemReportesCXP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReportesCXP.Click
        CargarForm(New frmReportesCXP)
    End Sub

    Private Sub MenuItem30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporte_Inventario.Click
        CargarForm(New frmOpcionesVisualizacion)
    End Sub

    Private Sub MenuItem42_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporte_Kardex.Click
        CargarForm(New FrmKardex)
    End Sub

    Private Sub MenuItemMovArt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemMovArt.Click
        CargarForm(New MovimientoArticulos)
    End Sub

    Private Sub MenuItem77_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteABC.Click
        CargarForm(New frmReporte_ABC)
    End Sub

    Private Sub MenuItem28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteClientes.Click
        Try
            Dim RptClientes As New Clientes
            CrystalReportsConexion.LoadShow(RptClientes, Me)

        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub

    Private Sub MenuItem29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteProveedores.Click
        Try
            Dim RptProveedores As New Proveedores
            CrystalReportsConexion.LoadShow(RptProveedores, Me)

        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub

    Private Sub MenuItemReporteCajas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteCajas.Click
        CargarForm(New FrmReportesCajas)
    End Sub
#End Region

#Region "Menu INTERFAZ"
    Private Sub MenuItem79_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAyuda.Click
        Help.ShowHelp(Me, HelpProvider.HelpNamespace)
    End Sub

    Private Sub MenuItemMaximizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemMaximizar.Click
        Me.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemNormal.Click
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemMinimizar.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub MenuItemPerfilUsuarios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemPerfilUsuarios.Click
        CargarForm(New AgregarPerfil(0, usua))
    End Sub

    Private Sub MenuItem47_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemCambioUsuarios.Click
        Dim Form As New Frm_login
        Form.ShowDialog()
        usua = Form.Usuario
        If Form.conectado = True Then
            Perfil_Usuario()
        End If
    End Sub

    Private Sub MenuItemRegistroUsuarios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemRegistroUsuarios.Click
        CargarForm(New Frmusuario(usua))
    End Sub

    Private Sub MenuItem18_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemRespaldos.Click
        CargarForm(New FrmRespaldo)
    End Sub

    Private Sub MenuItem50_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemActivarPOS.Click
        If MenuItemActivarPOS.Checked = True Then
            MenuItemActivarPOS.Checked = False
            SaveSetting("SeeSOFT", "SeePOS", "PVE", "0")
        Else
            MenuItemActivarPOS.Checked = True
            SaveSetting("SeeSOFT", "SeePOS", "PVE", "1")
        End If
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem.Click
        CapturaPantallaVisual()
    End Sub

    Private Sub MenuItem20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemFullScreenPVE.Click
        Try
            If MenuItemFullScreenPVE.Checked = True Then
                MenuItemFullScreenPVE.Checked = False
                SaveSetting("SeeSOFT", "SeePOS", "PVEFullScreen", "0")
            Else
                MenuItemFullScreenPVE.Checked = True
                SaveSetting("SeeSOFT", "SeePOS", "PVEFullScreen", "1")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Mensaje")
        End Try
    End Sub

    Private Sub MenuItem78_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemCalculadora.Click
        Dim hWndNotepad
        Dim proc As New System.Diagnostics.Process
        proc.StartInfo.FileName = "CALC.EXE"
        proc.Start()

        hWndNotepad = WinApi.FindWindow(Nothing, proc.MainWindowTitle)

        If hWndNotepad.ToInt32 > 0 Then
            WinApi.SetParent(hWndNotepad, Me.Handle)
        Else
            proc.Close()
            hWndNotepad = IntPtr.Zero
        End If
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemCerrar.Click
        Me.Dispose(True)
        Me.Close()
    End Sub
#End Region

#Region "Funciones"
    Public Sub Facturacion()
        Try
            ToolBarButtonFactura.Enabled = False
            If GetSetting("SeeSOFT", "SeePOS", "PVE") = 0 Then
                CargarForm(New Facturacion(usua))
            Else
                If System.IO.File.Exists(Application.StartupPath & "\PVE\SeePOS-PVE 5.3.exe") = True Then
                    Dim proc As New System.Diagnostics.Process
                    Dim hWndNotepad
                    proc.StartInfo.FileName = Application.StartupPath & "\PVE\SeePOS-PVE 5.3.exe"
                    proc.StartInfo.Arguments = usua.Cedula
                    proc.Start()

                    ' INICIA EL EJECUTABLE DENTRO DE LA APLICACION
                    hWndNotepad = WinApi.FindWindow(Nothing, proc.MainWindowTitle)
                    If hWndNotepad.ToInt32 > 0 Then
                        WinApi.SetParent(hWndNotepad, Me.Handle)
                    Else
                        proc.Close()
                        hWndNotepad = IntPtr.Zero
                    End If
                Else
                    MsgBox("El sistema de Facturación PVE no se encuentra Instalado." & vbCrLf & "Verifique su Instalación y su ruta...." & vbCrLf & Application.StartupPath & "\PVE\SeePOS-PVE.exe", MsgBoxStyle.Critical, "Atención...")
                End If
            End If
            ToolBarButtonFactura.Enabled = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Problemas al cargar el formulario Punto de Venta")
        End Try
    End Sub

    Private Sub CargarForm(ByRef Form As Form)
        Try
            Form.MdiParent = Me
            Form.Left = (Screen.PrimaryScreen.WorkingArea.Width - Form.Width) \ 2
            Form.Top = (Screen.PrimaryScreen.WorkingArea.Height - Form.Height) \ 2
            Form.StartPosition = FormStartPosition.CenterScreen
            Form.Show()
        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub

    Public Sub HabilitarMenu(ByVal tControl As Control)
        tControl.Enabled = True
        Texto = Texto & tControl.Name & vbCrLf
        If tControl.Controls.Count > 0 Then
            Dim tControl2 As Control
            For Each tControl2 In tControl.Controls
                HabilitarMenu(tControl2)
            Next
        End If
    End Sub
#End Region

#Region "TOOLBAR"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Try
            Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
                Case 1
                    CargarForm(New FrmInventario(usua))
                Case 3
                    CargarForm(New frmProveedores(usua))
                Case 5
                    Me.CargarForm(New Frmcliente(usua))
                Case 7
                    CargarForm(New frmCompra(usua))
                Case 9
                    CargarForm(New frmEstado_CXP)
                Case 11
                    Facturacion()
            End Select

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region



    Private Sub MenuItem1_Click_3(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Importar_Excel.Click
        Shell("ImportarExcel.exe")
    End Sub

    Private Sub MenuInventarios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuInventarios.Click

    End Sub

    Private Sub MenuItem1_Click_4(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        CargarForm(New frmGasto(usua))
    End Sub


    Private Sub MenuItem2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        CargarForm(New frmReporteGastos("", 1))
    End Sub

    Private Sub MenuItem3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteAjusteInventarioSalida.Click
        CargarForm(New frmReporteAjusteInventarioSalidas(usua))
    End Sub

    Private Sub MenuItemReporteRecarga_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemReporteRecarga.Click
        CargarForm(New frmReporteRecarga(usua))
    End Sub

    Private Sub MenuItemPaquetes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemPaquetes.Click
        CargarForm(New frmBuscarPaquete(usua))
    End Sub

    Private Sub MenuItemPromociones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemPromociones.Click
        CargarForm(New frmBuscarPromocion(usua))
    End Sub

    Private Sub MenuItemHistoricoCierreCaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemHistoricoCierreCaja.Click
        CargarForm(New rptReporteHistoricoCierreCaja(usua))
    End Sub

    Private Sub MenuItemUtilidadXProveedor_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemUtilidadXProveedor.Click
        CargarForm(New frmUtilidadXProveedor)
    End Sub

    Private Sub MenuItemCambioutilidad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemCambioutilidad.Click
        CargarForm(New frmReporteCambioUtilidad(usua))
    End Sub

    Private Sub MenuItem8_Click_1(sender As Object, e As EventArgs) Handles MenuItem8.Click
        Dim ver As New clsVersion(My.Application.Deployment)
        ver.EsPosible = My.Application.IsNetworkDeployed
        Dim fr As New frmActualiza(ver)
        If fr.ShowDialog = Windows.Forms.DialogResult.OK Then
            Close()
        End If
    End Sub

    Private Sub MenuItem11_Click_1(sender As Object, e As EventArgs) Handles MenuItem11.Click
        Dim clsImportarXml As New clsImportarXml
        clsImportarXml.spMostrarConfirmarIndividualXML()
    End Sub

    Private Sub MenuItem12_Click_1(sender As Object, e As EventArgs) Handles MenuItem12.Click
        Dim clsImportarXml As New clsImportarXml
        clsImportarXml.spMostrarConfirmarVariosXML()
    End Sub

    Private Sub MenuItem13_Click(sender As Object, e As EventArgs) Handles MenuItemReporteHacienda.Click
        Dim frm As New frmReportes
        frm.ShowDialog()
    End Sub

    Private Sub MenuItem10_Click_1(sender As Object, e As EventArgs) Handles MenuItem10.Click
        Dim frm As New UtilidadesXML.frmTarifaImpuestoIVA
        frm.ShowDialog()
    End Sub

    Private Sub btImpuestoActividadEconomica_Click(sender As Object, e As EventArgs) Handles btImpuestoActividadEconomica.Click
        CargarForm(New frmInformeActividadEconomica(usua.Nombre, usua.Cedula))
    End Sub

    Private Sub btnActividades_Click(sender As Object, e As EventArgs) Handles btnActividades.Click
        CargarForm(New frmActividadEconomica(usua))
    End Sub
    Private Sub cargarSucursal()
        Try
            LcPymes_5._2.Principal.Sucursal = Convert.ToInt32("1")
        Catch ex As Exception
            LcPymes_5._2.Principal.Sucursal = 1
        End Try
    End Sub


End Class

#Region "LLamados de archivos externos"



Public Class WinApi
    ' Hace que una ventana sea hija (o está contenida) en otra
    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Public Shared Function SetParent(ByVal hWndChild As IntPtr,
    ByVal hWndNewParent As IntPtr) As IntPtr
    End Function
    ' Devuelve el Handle (hWnd) de una ventana de la que sabemos el título
    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Public Shared Function FindWindow(ByVal lpClassName As String,
    ByVal lpWindowName As String) As IntPtr
    End Function
    ' Cambia el tamaño y la posición de una ventana
    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Public Shared Function MoveWindow(ByVal hWnd As IntPtr,
    ByVal x As Integer, ByVal y As Integer,
                                ByVal nWidth As Integer, ByVal nHeight As Integer,
                                ByVal bRepaint As Integer) As Integer
    End Function
End Class
#End Region