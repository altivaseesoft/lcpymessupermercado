Imports System.Data.SqlClient
Imports System.Data
Imports System.Windows.Forms
Imports UtilidadesXML


Public Class frmGasto
	Inherits System.Windows.Forms.Form

	Dim Usua As New Object

	Dim impuesto As Double
	Dim IdDetalle As Integer
	Dim Buscando As Integer = 0
	Friend WithEvents lblMoneda6 As Label
	Friend WithEvents lblMoneda5 As Label
	Friend WithEvents txtTotalImpuestoAplicable As TextBox
	Friend WithEvents Label26 As Label
	Friend WithEvents txtTotalMontoGasto As TextBox
	Friend WithEvents Label24 As Label
	Friend WithEvents txtImpuestoAplicable As TextBox
	Friend WithEvents Label18 As Label
	Friend WithEvents Label19 As Label
	Friend WithEvents txtMontoGasto As TextBox
	Friend WithEvents ToolBarRegistrarFEC As ToolBarButton
	Friend WithEvents ToolBarImportarXML As ToolBarButton
	Friend WithEvents PanelImportarXML As Panel
	Friend WithEvents btnContinuar As Button
	Friend WithEvents txtDetalleXML As TextBox
	Friend WithEvents Label54 As Label
	Friend WithEvents chConfirmada As CheckBox
	Friend WithEvents bsProveedor As BindingSource
	Friend WithEvents ProveedoresTableAdapter As DatasetGastoTableAdapters.ProveedoresTableAdapter
	Friend WithEvents Label39 As Label
	Friend WithEvents Label3 As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents Label1 As Label
	Friend WithEvents txtNumeroFactura As TextBox
	Friend WithEvents dtpFecha As DateTimePicker
	Friend WithEvents cmbTipo As ComboBox
	Friend WithEvents cmbProveedor As ComboBox
	Friend WithEvents Label15 As Label
	Friend WithEvents cmbMoneda As ComboBox
	Friend WithEvents chSimplificado As CheckBox
	Friend WithEvents GroupBox1 As GroupBox
	Friend WithEvents bsMoneda As BindingSource
	Friend WithEvents MonedaTableAdapter As DatasetGastoTableAdapters.MonedaTableAdapter
	Friend WithEvents txtCodigoCabys As TextBox
	Friend WithEvents lblCabys As Label
	Friend WithEvents txtTipoCambio As TextBox
	Dim IdGasto As Integer



#Region " C�digo generado por el Dise�ador de Windows Forms "

	Public Sub New(ByVal Usuario_Parametro As Object)
		MyBase.New()
		InitializeComponent() 'This call is required by the Windows Form Designer.
		Usua = Usuario_Parametro
	End Sub

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Dise�ador de Windows Forms. 
	'No lo modifique con el editor de c�digo.
	Protected Friend WithEvents Label46 As System.Windows.Forms.Label
	Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
	Friend WithEvents ToolBarExcel As System.Windows.Forms.ToolBarButton
	Friend WithEvents ToolBarButtonSeparador1 As System.Windows.Forms.ToolBarButton
	Friend WithEvents ToolBarImportar As System.Windows.Forms.ToolBarButton
	Friend WithEvents ToolBarButtonSeparador2 As System.Windows.Forms.ToolBarButton
	Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
	Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents Label5 As System.Windows.Forms.Label
	Friend WithEvents Label6 As System.Windows.Forms.Label
	Friend WithEvents Label7 As System.Windows.Forms.Label
	Friend WithEvents Label8 As System.Windows.Forms.Label
	Friend WithEvents Label9 As System.Windows.Forms.Label
	Friend WithEvents Label10 As System.Windows.Forms.Label
	Friend WithEvents Label11 As System.Windows.Forms.Label
	Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents Label12 As System.Windows.Forms.Label
	Friend WithEvents Label13 As System.Windows.Forms.Label
	Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents Label17 As System.Windows.Forms.Label
	Friend WithEvents txtClave As System.Windows.Forms.TextBox
	Friend WithEvents txtCuentaContableDescripcion As System.Windows.Forms.TextBox
	Friend WithEvents txtDetalleSubTotal As System.Windows.Forms.TextBox
	Friend WithEvents txtDetalleCantidad As System.Windows.Forms.TextBox
	Friend WithEvents txtCuentaContable As System.Windows.Forms.TextBox
	Friend WithEvents txtTotal As System.Windows.Forms.TextBox
	Friend WithEvents txtTotalImpuesto As System.Windows.Forms.TextBox
	Friend WithEvents txtTotalDescuento As System.Windows.Forms.TextBox
	Friend WithEvents txtDetalleDescuento As System.Windows.Forms.TextBox
	Friend WithEvents txtDetallePrecioUnidad As System.Windows.Forms.TextBox
	Friend WithEvents gridDetalle As DevExpress.XtraGrid.GridControl
	Friend WithEvents tlbNuevo As System.Windows.Forms.ToolBarButton
	Friend WithEvents tlbBuscar As System.Windows.Forms.ToolBarButton
	Friend WithEvents tlbRegistrar As System.Windows.Forms.ToolBarButton
	Friend WithEvents tlbEliminar As System.Windows.Forms.ToolBarButton
	Friend WithEvents tlbImprimir As System.Windows.Forms.ToolBarButton
	Friend WithEvents tlbRecalcular As System.Windows.Forms.ToolBarButton
	Friend WithEvents tlbCerrar As System.Windows.Forms.ToolBarButton
	Friend WithEvents dtsGasto As DatasetGasto
	Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents txtDetalleArticuloDescripcion As System.Windows.Forms.TextBox
	Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
	Friend WithEvents Label14 As System.Windows.Forms.Label
	Friend WithEvents txtImpuesto As System.Windows.Forms.TextBox
	Friend WithEvents lblMoneda4 As System.Windows.Forms.Label
	Friend WithEvents lblMoneda3 As System.Windows.Forms.Label
	Friend WithEvents lblMoneda2 As System.Windows.Forms.Label
	Friend WithEvents lblMoneda1 As System.Windows.Forms.Label
	Friend WithEvents TxtNombreUsuario As System.Windows.Forms.Label
	Friend WithEvents lblAsiento As System.Windows.Forms.Label
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGasto))
		Dim ColumnFilterInfo9 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo10 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo11 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo12 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo13 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo14 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo15 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo16 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Me.Label46 = New System.Windows.Forms.Label()
		Me.ToolBar1 = New System.Windows.Forms.ToolBar()
		Me.tlbNuevo = New System.Windows.Forms.ToolBarButton()
		Me.tlbBuscar = New System.Windows.Forms.ToolBarButton()
		Me.tlbRegistrar = New System.Windows.Forms.ToolBarButton()
		Me.tlbEliminar = New System.Windows.Forms.ToolBarButton()
		Me.tlbImprimir = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarRegistrarFEC = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarExcel = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarButtonSeparador1 = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarImportar = New System.Windows.Forms.ToolBarButton()
		Me.tlbRecalcular = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarButtonSeparador2 = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarImportarXML = New System.Windows.Forms.ToolBarButton()
		Me.tlbCerrar = New System.Windows.Forms.ToolBarButton()
		Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.txtCodigoCabys = New System.Windows.Forms.TextBox()
		Me.lblCabys = New System.Windows.Forms.Label()
		Me.txtImpuestoAplicable = New System.Windows.Forms.TextBox()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.txtMontoGasto = New System.Windows.Forms.TextBox()
		Me.lblMoneda6 = New System.Windows.Forms.Label()
		Me.lblMoneda5 = New System.Windows.Forms.Label()
		Me.txtTotalImpuestoAplicable = New System.Windows.Forms.TextBox()
		Me.Label26 = New System.Windows.Forms.Label()
		Me.txtTotalMontoGasto = New System.Windows.Forms.TextBox()
		Me.Label24 = New System.Windows.Forms.Label()
		Me.lblMoneda1 = New System.Windows.Forms.Label()
		Me.lblMoneda2 = New System.Windows.Forms.Label()
		Me.lblMoneda3 = New System.Windows.Forms.Label()
		Me.lblMoneda4 = New System.Windows.Forms.Label()
		Me.txtImpuesto = New System.Windows.Forms.TextBox()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.txtCuentaContableDescripcion = New System.Windows.Forms.TextBox()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.txtCuentaContable = New System.Windows.Forms.TextBox()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.txtTotal = New System.Windows.Forms.TextBox()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.txtTotalImpuesto = New System.Windows.Forms.TextBox()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.txtTotalDescuento = New System.Windows.Forms.TextBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.txtDetalleSubTotal = New System.Windows.Forms.TextBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.txtDetalleDescuento = New System.Windows.Forms.TextBox()
		Me.txtDetallePrecioUnidad = New System.Windows.Forms.TextBox()
		Me.txtDetalleArticuloDescripcion = New System.Windows.Forms.TextBox()
		Me.txtDetalleCantidad = New System.Windows.Forms.TextBox()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.gridDetalle = New DevExpress.XtraGrid.GridControl()
		Me.dtsGasto = New LcPymes_5._2.DatasetGasto()
		Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
		Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.Label17 = New System.Windows.Forms.Label()
		Me.txtClave = New System.Windows.Forms.TextBox()
		Me.TxtNombreUsuario = New System.Windows.Forms.Label()
		Me.lblAsiento = New System.Windows.Forms.Label()
		Me.PanelImportarXML = New System.Windows.Forms.Panel()
		Me.btnContinuar = New System.Windows.Forms.Button()
		Me.txtDetalleXML = New System.Windows.Forms.TextBox()
		Me.Label54 = New System.Windows.Forms.Label()
		Me.chConfirmada = New System.Windows.Forms.CheckBox()
		Me.bsProveedor = New System.Windows.Forms.BindingSource(Me.components)
		Me.ProveedoresTableAdapter = New LcPymes_5._2.DatasetGastoTableAdapters.ProveedoresTableAdapter()
		Me.Label39 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.txtNumeroFactura = New System.Windows.Forms.TextBox()
		Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
		Me.cmbTipo = New System.Windows.Forms.ComboBox()
		Me.cmbProveedor = New System.Windows.Forms.ComboBox()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.cmbMoneda = New System.Windows.Forms.ComboBox()
		Me.bsMoneda = New System.Windows.Forms.BindingSource(Me.components)
		Me.chSimplificado = New System.Windows.Forms.CheckBox()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.MonedaTableAdapter = New LcPymes_5._2.DatasetGastoTableAdapters.MonedaTableAdapter()
		Me.txtTipoCambio = New System.Windows.Forms.TextBox()
		Me.GroupBox2.SuspendLayout()
		CType(Me.gridDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.dtsGasto, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.PanelImportarXML.SuspendLayout()
		CType(Me.bsProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.bsMoneda, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Label46
		'
		Me.Label46.BackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(91, Byte), Integer), CType(CType(165, Byte), Integer))
		Me.Label46.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
		Me.Label46.ForeColor = System.Drawing.Color.White
		Me.Label46.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label46.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label46.Location = New System.Drawing.Point(0, 0)
		Me.Label46.Name = "Label46"
		Me.Label46.Size = New System.Drawing.Size(962, 32)
		Me.Label46.TabIndex = 109
		Me.Label46.Text = "Registro de Gastos"
		Me.Label46.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'ToolBar1
		'
		Me.ToolBar1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tlbNuevo, Me.tlbBuscar, Me.tlbRegistrar, Me.tlbEliminar, Me.tlbImprimir, Me.ToolBarRegistrarFEC, Me.ToolBarExcel, Me.ToolBarButtonSeparador1, Me.ToolBarImportar, Me.tlbRecalcular, Me.ToolBarButtonSeparador2, Me.ToolBarImportarXML, Me.tlbCerrar})
		Me.ToolBar1.ButtonSize = New System.Drawing.Size(60, 55)
		Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.None
		Me.ToolBar1.DropDownArrows = True
		Me.ToolBar1.ImageList = Me.ImageList1
		Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.ToolBar1.Location = New System.Drawing.Point(0, 426)
		Me.ToolBar1.Name = "ToolBar1"
		Me.ToolBar1.ShowToolTips = True
		Me.ToolBar1.Size = New System.Drawing.Size(1128, 58)
		Me.ToolBar1.TabIndex = 110
		'
		'tlbNuevo
		'
		Me.tlbNuevo.Enabled = False
		Me.tlbNuevo.ImageIndex = 0
		Me.tlbNuevo.Name = "tlbNuevo"
		Me.tlbNuevo.Text = "Nuevo"
		'
		'tlbBuscar
		'
		Me.tlbBuscar.Enabled = False
		Me.tlbBuscar.ImageIndex = 1
		Me.tlbBuscar.Name = "tlbBuscar"
		Me.tlbBuscar.Text = "Buscar"
		'
		'tlbRegistrar
		'
		Me.tlbRegistrar.Enabled = False
		Me.tlbRegistrar.ImageIndex = 2
		Me.tlbRegistrar.Name = "tlbRegistrar"
		Me.tlbRegistrar.Text = "Registrar"
		'
		'tlbEliminar
		'
		Me.tlbEliminar.Enabled = False
		Me.tlbEliminar.ImageIndex = 3
		Me.tlbEliminar.Name = "tlbEliminar"
		Me.tlbEliminar.Text = "Eliminar"
		'
		'tlbImprimir
		'
		Me.tlbImprimir.Enabled = False
		Me.tlbImprimir.ImageIndex = 7
		Me.tlbImprimir.Name = "tlbImprimir"
		Me.tlbImprimir.Text = "Imprimir"
		'
		'ToolBarRegistrarFEC
		'
		Me.ToolBarRegistrarFEC.Enabled = False
		Me.ToolBarRegistrarFEC.ImageIndex = 2
		Me.ToolBarRegistrarFEC.Name = "ToolBarRegistrarFEC"
		Me.ToolBarRegistrarFEC.Text = "Fac. Elec"
		'
		'ToolBarExcel
		'
		Me.ToolBarExcel.ImageIndex = 5
		Me.ToolBarExcel.Name = "ToolBarExcel"
		Me.ToolBarExcel.Text = "Exportar"
		Me.ToolBarExcel.Visible = False
		'
		'ToolBarButtonSeparador1
		'
		Me.ToolBarButtonSeparador1.Name = "ToolBarButtonSeparador1"
		Me.ToolBarButtonSeparador1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
		'
		'ToolBarImportar
		'
		Me.ToolBarImportar.ImageIndex = 9
		Me.ToolBarImportar.Name = "ToolBarImportar"
		Me.ToolBarImportar.Text = "Importar"
		Me.ToolBarImportar.Visible = False
		'
		'tlbRecalcular
		'
		Me.tlbRecalcular.Enabled = False
		Me.tlbRecalcular.ImageIndex = 10
		Me.tlbRecalcular.Name = "tlbRecalcular"
		Me.tlbRecalcular.Text = "ReCalcular"
		Me.tlbRecalcular.Visible = False
		'
		'ToolBarButtonSeparador2
		'
		Me.ToolBarButtonSeparador2.Name = "ToolBarButtonSeparador2"
		Me.ToolBarButtonSeparador2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator
		'
		'ToolBarImportarXML
		'
		Me.ToolBarImportarXML.Enabled = False
		Me.ToolBarImportarXML.ImageIndex = 9
		Me.ToolBarImportarXML.Name = "ToolBarImportarXML"
		Me.ToolBarImportarXML.Text = "Imp XML"
		'
		'tlbCerrar
		'
		Me.tlbCerrar.ImageIndex = 6
		Me.tlbCerrar.Name = "tlbCerrar"
		Me.tlbCerrar.Text = "Cerrar"
		'
		'ImageList1
		'
		Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
		Me.ImageList1.Images.SetKeyName(0, "")
		Me.ImageList1.Images.SetKeyName(1, "")
		Me.ImageList1.Images.SetKeyName(2, "")
		Me.ImageList1.Images.SetKeyName(3, "")
		Me.ImageList1.Images.SetKeyName(4, "")
		Me.ImageList1.Images.SetKeyName(5, "")
		Me.ImageList1.Images.SetKeyName(6, "")
		Me.ImageList1.Images.SetKeyName(7, "")
		Me.ImageList1.Images.SetKeyName(8, "")
		Me.ImageList1.Images.SetKeyName(9, "")
		Me.ImageList1.Images.SetKeyName(10, "")
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.txtCodigoCabys)
		Me.GroupBox2.Controls.Add(Me.lblCabys)
		Me.GroupBox2.Controls.Add(Me.txtImpuestoAplicable)
		Me.GroupBox2.Controls.Add(Me.Label18)
		Me.GroupBox2.Controls.Add(Me.Label19)
		Me.GroupBox2.Controls.Add(Me.txtMontoGasto)
		Me.GroupBox2.Controls.Add(Me.lblMoneda6)
		Me.GroupBox2.Controls.Add(Me.lblMoneda5)
		Me.GroupBox2.Controls.Add(Me.txtTotalImpuestoAplicable)
		Me.GroupBox2.Controls.Add(Me.Label26)
		Me.GroupBox2.Controls.Add(Me.txtTotalMontoGasto)
		Me.GroupBox2.Controls.Add(Me.Label24)
		Me.GroupBox2.Controls.Add(Me.lblMoneda1)
		Me.GroupBox2.Controls.Add(Me.lblMoneda2)
		Me.GroupBox2.Controls.Add(Me.lblMoneda3)
		Me.GroupBox2.Controls.Add(Me.lblMoneda4)
		Me.GroupBox2.Controls.Add(Me.txtImpuesto)
		Me.GroupBox2.Controls.Add(Me.Label14)
		Me.GroupBox2.Controls.Add(Me.txtCuentaContableDescripcion)
		Me.GroupBox2.Controls.Add(Me.Label13)
		Me.GroupBox2.Controls.Add(Me.txtCuentaContable)
		Me.GroupBox2.Controls.Add(Me.Label12)
		Me.GroupBox2.Controls.Add(Me.txtTotal)
		Me.GroupBox2.Controls.Add(Me.Label11)
		Me.GroupBox2.Controls.Add(Me.txtTotalImpuesto)
		Me.GroupBox2.Controls.Add(Me.Label10)
		Me.GroupBox2.Controls.Add(Me.txtTotalDescuento)
		Me.GroupBox2.Controls.Add(Me.Label9)
		Me.GroupBox2.Controls.Add(Me.txtDetalleSubTotal)
		Me.GroupBox2.Controls.Add(Me.Label8)
		Me.GroupBox2.Controls.Add(Me.txtDetalleDescuento)
		Me.GroupBox2.Controls.Add(Me.txtDetallePrecioUnidad)
		Me.GroupBox2.Controls.Add(Me.txtDetalleArticuloDescripcion)
		Me.GroupBox2.Controls.Add(Me.txtDetalleCantidad)
		Me.GroupBox2.Controls.Add(Me.Label7)
		Me.GroupBox2.Controls.Add(Me.Label6)
		Me.GroupBox2.Controls.Add(Me.Label5)
		Me.GroupBox2.Controls.Add(Me.Label4)
		Me.GroupBox2.Controls.Add(Me.gridDetalle)
		Me.GroupBox2.Location = New System.Drawing.Point(16, 124)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(883, 265)
		Me.GroupBox2.TabIndex = 121
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "Detalle"
		'
		'txtCodigoCabys
		'
		Me.txtCodigoCabys.BackColor = System.Drawing.Color.White
		Me.txtCodigoCabys.Location = New System.Drawing.Point(640, 71)
		Me.txtCodigoCabys.Name = "txtCodigoCabys"
		Me.txtCodigoCabys.Size = New System.Drawing.Size(237, 20)
		Me.txtCodigoCabys.TabIndex = 223
		Me.txtCodigoCabys.Visible = False
		'
		'lblCabys
		'
		Me.lblCabys.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblCabys.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblCabys.ForeColor = System.Drawing.Color.RoyalBlue
		Me.lblCabys.Location = New System.Drawing.Point(640, 55)
		Me.lblCabys.Name = "lblCabys"
		Me.lblCabys.Size = New System.Drawing.Size(237, 16)
		Me.lblCabys.TabIndex = 224
		Me.lblCabys.Text = "Cabys"
		Me.lblCabys.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.lblCabys.Visible = False
		'
		'txtImpuestoAplicable
		'
		Me.txtImpuestoAplicable.Location = New System.Drawing.Point(512, 71)
		Me.txtImpuestoAplicable.Name = "txtImpuestoAplicable"
		Me.txtImpuestoAplicable.ReadOnly = True
		Me.txtImpuestoAplicable.Size = New System.Drawing.Size(120, 20)
		Me.txtImpuestoAplicable.TabIndex = 220
		Me.txtImpuestoAplicable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label18
		'
		Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label18.Location = New System.Drawing.Point(512, 55)
		Me.Label18.Name = "Label18"
		Me.Label18.Size = New System.Drawing.Size(120, 16)
		Me.Label18.TabIndex = 222
		Me.Label18.Text = "Impuesto Aplicable"
		Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label19
		'
		Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label19.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label19.Location = New System.Drawing.Point(376, 55)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(118, 16)
		Me.Label19.TabIndex = 221
		Me.Label19.Text = "Monto Gasto"
		Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtMontoGasto
		'
		Me.txtMontoGasto.Location = New System.Drawing.Point(376, 71)
		Me.txtMontoGasto.Name = "txtMontoGasto"
		Me.txtMontoGasto.ReadOnly = True
		Me.txtMontoGasto.Size = New System.Drawing.Size(118, 20)
		Me.txtMontoGasto.TabIndex = 219
		Me.txtMontoGasto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'lblMoneda6
		'
		Me.lblMoneda6.Location = New System.Drawing.Point(743, 231)
		Me.lblMoneda6.Name = "lblMoneda6"
		Me.lblMoneda6.Size = New System.Drawing.Size(17, 20)
		Me.lblMoneda6.TabIndex = 218
		Me.lblMoneda6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblMoneda5
		'
		Me.lblMoneda5.Location = New System.Drawing.Point(597, 230)
		Me.lblMoneda5.Name = "lblMoneda5"
		Me.lblMoneda5.Size = New System.Drawing.Size(17, 20)
		Me.lblMoneda5.TabIndex = 217
		Me.lblMoneda5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'txtTotalImpuestoAplicable
		'
		Me.txtTotalImpuestoAplicable.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtTotalImpuestoAplicable.Enabled = False
		Me.txtTotalImpuestoAplicable.Location = New System.Drawing.Point(185, 231)
		Me.txtTotalImpuestoAplicable.Name = "txtTotalImpuestoAplicable"
		Me.txtTotalImpuestoAplicable.Size = New System.Drawing.Size(111, 20)
		Me.txtTotalImpuestoAplicable.TabIndex = 216
		Me.txtTotalImpuestoAplicable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label26
		'
		Me.Label26.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label26.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label26.Location = New System.Drawing.Point(185, 215)
		Me.Label26.Name = "Label26"
		Me.Label26.Size = New System.Drawing.Size(111, 16)
		Me.Label26.TabIndex = 215
		Me.Label26.Text = "Imp Aplicable:"
		Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtTotalMontoGasto
		'
		Me.txtTotalMontoGasto.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtTotalMontoGasto.Enabled = False
		Me.txtTotalMontoGasto.Location = New System.Drawing.Point(31, 231)
		Me.txtTotalMontoGasto.Name = "txtTotalMontoGasto"
		Me.txtTotalMontoGasto.Size = New System.Drawing.Size(116, 20)
		Me.txtTotalMontoGasto.TabIndex = 213
		Me.txtTotalMontoGasto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label24
		'
		Me.Label24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label24.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label24.Location = New System.Drawing.Point(31, 215)
		Me.Label24.Name = "Label24"
		Me.Label24.Size = New System.Drawing.Size(116, 16)
		Me.Label24.TabIndex = 212
		Me.Label24.Text = "Monto Gasto:"
		Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lblMoneda1
		'
		Me.lblMoneda1.Location = New System.Drawing.Point(13, 230)
		Me.lblMoneda1.Name = "lblMoneda1"
		Me.lblMoneda1.Size = New System.Drawing.Size(16, 20)
		Me.lblMoneda1.TabIndex = 138
		Me.lblMoneda1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblMoneda2
		'
		Me.lblMoneda2.Location = New System.Drawing.Point(163, 230)
		Me.lblMoneda2.Name = "lblMoneda2"
		Me.lblMoneda2.Size = New System.Drawing.Size(19, 23)
		Me.lblMoneda2.TabIndex = 137
		Me.lblMoneda2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblMoneda3
		'
		Me.lblMoneda3.Location = New System.Drawing.Point(311, 231)
		Me.lblMoneda3.Name = "lblMoneda3"
		Me.lblMoneda3.Size = New System.Drawing.Size(20, 20)
		Me.lblMoneda3.TabIndex = 136
		Me.lblMoneda3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblMoneda4
		'
		Me.lblMoneda4.Location = New System.Drawing.Point(458, 231)
		Me.lblMoneda4.Name = "lblMoneda4"
		Me.lblMoneda4.Size = New System.Drawing.Size(17, 20)
		Me.lblMoneda4.TabIndex = 135
		Me.lblMoneda4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'txtImpuesto
		'
		Me.txtImpuesto.Location = New System.Drawing.Point(360, 32)
		Me.txtImpuesto.Name = "txtImpuesto"
		Me.txtImpuesto.Size = New System.Drawing.Size(64, 20)
		Me.txtImpuesto.TabIndex = 8
		Me.txtImpuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label14
		'
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label14.Location = New System.Drawing.Point(360, 16)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(64, 16)
		Me.Label14.TabIndex = 134
		Me.Label14.Text = "Impuesto:"
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtCuentaContableDescripcion
		'
		Me.txtCuentaContableDescripcion.BackColor = System.Drawing.Color.White
		Me.txtCuentaContableDescripcion.Enabled = False
		Me.txtCuentaContableDescripcion.Location = New System.Drawing.Point(640, 32)
		Me.txtCuentaContableDescripcion.Name = "txtCuentaContableDescripcion"
		Me.txtCuentaContableDescripcion.Size = New System.Drawing.Size(237, 20)
		Me.txtCuentaContableDescripcion.TabIndex = 11
		'
		'Label13
		'
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label13.Location = New System.Drawing.Point(640, 16)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(237, 16)
		Me.Label13.TabIndex = 132
		Me.Label13.Text = "Descripci�n:"
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtCuentaContable
		'
		Me.txtCuentaContable.Location = New System.Drawing.Point(512, 32)
		Me.txtCuentaContable.Name = "txtCuentaContable"
		Me.txtCuentaContable.Size = New System.Drawing.Size(120, 20)
		Me.txtCuentaContable.TabIndex = 10
		'
		'Label12
		'
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label12.Location = New System.Drawing.Point(512, 16)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(120, 16)
		Me.Label12.TabIndex = 130
		Me.Label12.Text = "Cuenta contable:"
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtTotal
		'
		Me.txtTotal.BackColor = System.Drawing.Color.White
		Me.txtTotal.Enabled = False
		Me.txtTotal.Location = New System.Drawing.Point(763, 231)
		Me.txtTotal.Name = "txtTotal"
		Me.txtTotal.Size = New System.Drawing.Size(114, 20)
		Me.txtTotal.TabIndex = 129
		Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label11
		'
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label11.Location = New System.Drawing.Point(763, 215)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(114, 16)
		Me.Label11.TabIndex = 128
		Me.Label11.Text = "Total:"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtTotalImpuesto
		'
		Me.txtTotalImpuesto.BackColor = System.Drawing.Color.White
		Me.txtTotalImpuesto.Enabled = False
		Me.txtTotalImpuesto.Location = New System.Drawing.Point(617, 231)
		Me.txtTotalImpuesto.Name = "txtTotalImpuesto"
		Me.txtTotalImpuesto.Size = New System.Drawing.Size(109, 20)
		Me.txtTotalImpuesto.TabIndex = 127
		Me.txtTotalImpuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label10
		'
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label10.Location = New System.Drawing.Point(617, 215)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(109, 16)
		Me.Label10.TabIndex = 126
		Me.Label10.Text = "Impuesto:"
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtTotalDescuento
		'
		Me.txtTotalDescuento.BackColor = System.Drawing.Color.White
		Me.txtTotalDescuento.Enabled = False
		Me.txtTotalDescuento.Location = New System.Drawing.Point(478, 231)
		Me.txtTotalDescuento.Name = "txtTotalDescuento"
		Me.txtTotalDescuento.Size = New System.Drawing.Size(102, 20)
		Me.txtTotalDescuento.TabIndex = 125
		Me.txtTotalDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label9
		'
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label9.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label9.Location = New System.Drawing.Point(478, 215)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(102, 16)
		Me.Label9.TabIndex = 124
		Me.Label9.Text = "Descuento:"
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtDetalleSubTotal
		'
		Me.txtDetalleSubTotal.Enabled = False
		Me.txtDetalleSubTotal.Location = New System.Drawing.Point(334, 231)
		Me.txtDetalleSubTotal.Name = "txtDetalleSubTotal"
		Me.txtDetalleSubTotal.Size = New System.Drawing.Size(107, 20)
		Me.txtDetalleSubTotal.TabIndex = 123
		Me.txtDetalleSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label8
		'
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label8.Location = New System.Drawing.Point(334, 215)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(107, 16)
		Me.Label8.TabIndex = 122
		Me.Label8.Text = "Sub total:"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtDetalleDescuento
		'
		Me.txtDetalleDescuento.Location = New System.Drawing.Point(432, 32)
		Me.txtDetalleDescuento.Name = "txtDetalleDescuento"
		Me.txtDetalleDescuento.Size = New System.Drawing.Size(72, 20)
		Me.txtDetalleDescuento.TabIndex = 9
		Me.txtDetalleDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'txtDetallePrecioUnidad
		'
		Me.txtDetallePrecioUnidad.Location = New System.Drawing.Point(256, 32)
		Me.txtDetallePrecioUnidad.Name = "txtDetallePrecioUnidad"
		Me.txtDetallePrecioUnidad.Size = New System.Drawing.Size(96, 20)
		Me.txtDetallePrecioUnidad.TabIndex = 7
		Me.txtDetallePrecioUnidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'txtDetalleArticuloDescripcion
		'
		Me.txtDetalleArticuloDescripcion.Enabled = False
		Me.txtDetalleArticuloDescripcion.Location = New System.Drawing.Point(72, 32)
		Me.txtDetalleArticuloDescripcion.Name = "txtDetalleArticuloDescripcion"
		Me.txtDetalleArticuloDescripcion.Size = New System.Drawing.Size(176, 20)
		Me.txtDetalleArticuloDescripcion.TabIndex = 6
		'
		'txtDetalleCantidad
		'
		Me.txtDetalleCantidad.Location = New System.Drawing.Point(8, 32)
		Me.txtDetalleCantidad.Name = "txtDetalleCantidad"
		Me.txtDetalleCantidad.Size = New System.Drawing.Size(56, 20)
		Me.txtDetalleCantidad.TabIndex = 5
		Me.txtDetalleCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label7
		'
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label7.Location = New System.Drawing.Point(432, 16)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(72, 16)
		Me.Label7.TabIndex = 117
		Me.Label7.Text = "Descuento:"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label6
		'
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label6.Location = New System.Drawing.Point(256, 16)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(96, 16)
		Me.Label6.TabIndex = 116
		Me.Label6.Text = "Precio unidad:"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label5
		'
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label5.Location = New System.Drawing.Point(72, 16)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(176, 16)
		Me.Label5.TabIndex = 115
		Me.Label5.Text = "Descripci�n:"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label4
		'
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label4.Location = New System.Drawing.Point(8, 16)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(56, 16)
		Me.Label4.TabIndex = 114
		Me.Label4.Text = "Cantidad:"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'gridDetalle
		'
		Me.gridDetalle.DataSource = Me.dtsGasto.GastoDetalle
		'
		'
		'
		Me.gridDetalle.EmbeddedNavigator.Name = ""
		Me.gridDetalle.Location = New System.Drawing.Point(8, 97)
		Me.gridDetalle.MainView = Me.GridView1
		Me.gridDetalle.Name = "gridDetalle"
		Me.gridDetalle.Size = New System.Drawing.Size(869, 104)
		Me.gridDetalle.TabIndex = 0
		Me.gridDetalle.Text = "GridControl1"
		'
		'dtsGasto
		'
		Me.dtsGasto.DataSetName = "DatasetGasto"
		Me.dtsGasto.Locale = New System.Globalization.CultureInfo("en-US")
		Me.dtsGasto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'GridView1
		'
		Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn8, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7})
		Me.GridView1.Name = "GridView1"
		Me.GridView1.OptionsView.ShowGroupPanel = False
		'
		'GridColumn1
		'
		Me.GridColumn1.Caption = "Cantidad"
		Me.GridColumn1.FieldName = "Cantidad"
		Me.GridColumn1.FilterInfo = ColumnFilterInfo9
		Me.GridColumn1.Name = "GridColumn1"
		Me.GridColumn1.VisibleIndex = 0
		Me.GridColumn1.Width = 112
		'
		'GridColumn2
		'
		Me.GridColumn2.Caption = "Descripcion"
		Me.GridColumn2.FieldName = "Descripcion"
		Me.GridColumn2.FilterInfo = ColumnFilterInfo10
		Me.GridColumn2.Name = "GridColumn2"
		Me.GridColumn2.VisibleIndex = 1
		Me.GridColumn2.Width = 112
		'
		'GridColumn3
		'
		Me.GridColumn3.Caption = "Precio unidad"
		Me.GridColumn3.DisplayFormat.FormatString = "#,#0.00"
		Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.GridColumn3.FieldName = "Costo"
		Me.GridColumn3.FilterInfo = ColumnFilterInfo11
		Me.GridColumn3.Name = "GridColumn3"
		Me.GridColumn3.VisibleIndex = 2
		Me.GridColumn3.Width = 112
		'
		'GridColumn4
		'
		Me.GridColumn4.Caption = "% Des"
		Me.GridColumn4.DisplayFormat.FormatString = "#,#0.00"
		Me.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.GridColumn4.FieldName = "Descuento_P"
		Me.GridColumn4.FilterInfo = ColumnFilterInfo12
		Me.GridColumn4.Name = "GridColumn4"
		Me.GridColumn4.VisibleIndex = 3
		Me.GridColumn4.Width = 108
		'
		'GridColumn8
		'
		Me.GridColumn8.Caption = "% Imp"
		Me.GridColumn8.DisplayFormat.FormatString = "#,#0.00"
		Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.GridColumn8.FieldName = "Impuesto_p"
		Me.GridColumn8.FilterInfo = ColumnFilterInfo13
		Me.GridColumn8.Name = "GridColumn8"
		Me.GridColumn8.VisibleIndex = 4
		'
		'GridColumn5
		'
		Me.GridColumn5.Caption = "Sub total"
		Me.GridColumn5.DisplayFormat.FormatString = "#,#0.00"
		Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.GridColumn5.FieldName = "Total"
		Me.GridColumn5.FilterInfo = ColumnFilterInfo14
		Me.GridColumn5.Name = "GridColumn5"
		Me.GridColumn5.VisibleIndex = 5
		Me.GridColumn5.Width = 111
		'
		'GridColumn6
		'
		Me.GridColumn6.Caption = "Cuenta contable"
		Me.GridColumn6.FieldName = "CuentaContable"
		Me.GridColumn6.FilterInfo = ColumnFilterInfo15
		Me.GridColumn6.Name = "GridColumn6"
		Me.GridColumn6.VisibleIndex = 6
		Me.GridColumn6.Width = 111
		'
		'GridColumn7
		'
		Me.GridColumn7.Caption = "Descripci�n"
		Me.GridColumn7.FieldName = "CuentaContableDescripcion"
		Me.GridColumn7.FilterInfo = ColumnFilterInfo16
		Me.GridColumn7.Name = "GridColumn7"
		Me.GridColumn7.VisibleIndex = 7
		Me.GridColumn7.Width = 120
		'
		'Label17
		'
		Me.Label17.BackColor = System.Drawing.Color.RoyalBlue
		Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label17.ForeColor = System.Drawing.Color.White
		Me.Label17.Location = New System.Drawing.Point(480, 395)
		Me.Label17.Name = "Label17"
		Me.Label17.Size = New System.Drawing.Size(76, 13)
		Me.Label17.TabIndex = 122
		Me.Label17.Text = "Usuario->"
		Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtClave
		'
		Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtClave.ForeColor = System.Drawing.Color.Blue
		Me.txtClave.Location = New System.Drawing.Point(560, 395)
		Me.txtClave.Name = "txtClave"
		Me.txtClave.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
		Me.txtClave.Size = New System.Drawing.Size(72, 13)
		Me.txtClave.TabIndex = 0
		'
		'TxtNombreUsuario
		'
		Me.TxtNombreUsuario.BackColor = System.Drawing.SystemColors.Control
		Me.TxtNombreUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TxtNombreUsuario.ForeColor = System.Drawing.Color.Blue
		Me.TxtNombreUsuario.Location = New System.Drawing.Point(480, 411)
		Me.TxtNombreUsuario.Name = "TxtNombreUsuario"
		Me.TxtNombreUsuario.Size = New System.Drawing.Size(152, 13)
		Me.TxtNombreUsuario.TabIndex = 126
		'
		'lblAsiento
		'
		Me.lblAsiento.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.lblAsiento.BackColor = System.Drawing.Color.Transparent
		Me.lblAsiento.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblAsiento.Location = New System.Drawing.Point(794, 434)
		Me.lblAsiento.Name = "lblAsiento"
		Me.lblAsiento.Size = New System.Drawing.Size(160, 48)
		Me.lblAsiento.TabIndex = 127
		Me.lblAsiento.Text = "CXP-0000-0000"
		Me.lblAsiento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.lblAsiento.Visible = False
		'
		'PanelImportarXML
		'
		Me.PanelImportarXML.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.PanelImportarXML.Controls.Add(Me.btnContinuar)
		Me.PanelImportarXML.Controls.Add(Me.txtDetalleXML)
		Me.PanelImportarXML.Controls.Add(Me.Label54)
		Me.PanelImportarXML.Location = New System.Drawing.Point(612, 434)
		Me.PanelImportarXML.Name = "PanelImportarXML"
		Me.PanelImportarXML.Size = New System.Drawing.Size(187, 48)
		Me.PanelImportarXML.TabIndex = 128
		Me.PanelImportarXML.Visible = False
		'
		'btnContinuar
		'
		Me.btnContinuar.Location = New System.Drawing.Point(110, 5)
		Me.btnContinuar.Name = "btnContinuar"
		Me.btnContinuar.Size = New System.Drawing.Size(71, 32)
		Me.btnContinuar.TabIndex = 86
		Me.btnContinuar.Text = "Continuar"
		Me.btnContinuar.UseVisualStyleBackColor = True
		Me.btnContinuar.Visible = False
		'
		'txtDetalleXML
		'
		Me.txtDetalleXML.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtDetalleXML.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDetalleXML.ForeColor = System.Drawing.Color.Blue
		Me.txtDetalleXML.Location = New System.Drawing.Point(53, 16)
		Me.txtDetalleXML.Name = "txtDetalleXML"
		Me.txtDetalleXML.Size = New System.Drawing.Size(59, 13)
		Me.txtDetalleXML.TabIndex = 82
		Me.txtDetalleXML.Text = "0 de 10"
		'
		'Label54
		'
		Me.Label54.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label54.BackColor = System.Drawing.SystemColors.ControlLight
		Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label54.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label54.Location = New System.Drawing.Point(3, 16)
		Me.Label54.Name = "Label54"
		Me.Label54.Size = New System.Drawing.Size(52, 14)
		Me.Label54.TabIndex = 6
		Me.Label54.Text = "Detalle : "
		'
		'chConfirmada
		'
		Me.chConfirmada.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.chConfirmada.AutoSize = True
		Me.chConfirmada.Enabled = False
		Me.chConfirmada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.chConfirmada.ForeColor = System.Drawing.Color.RoyalBlue
		Me.chConfirmada.Location = New System.Drawing.Point(520, 450)
		Me.chConfirmada.Name = "chConfirmada"
		Me.chConfirmada.Size = New System.Drawing.Size(89, 17)
		Me.chConfirmada.TabIndex = 129
		Me.chConfirmada.Text = "Confirmada"
		Me.chConfirmada.UseVisualStyleBackColor = True
		Me.chConfirmada.Visible = False
		'
		'bsProveedor
		'
		Me.bsProveedor.DataMember = "Proveedores"
		Me.bsProveedor.DataSource = Me.dtsGasto
		'
		'ProveedoresTableAdapter
		'
		Me.ProveedoresTableAdapter.ClearBeforeFill = True
		'
		'Label39
		'
		Me.Label39.BackColor = System.Drawing.SystemColors.ControlLight
		Me.Label39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label39.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label39.Location = New System.Drawing.Point(576, 16)
		Me.Label39.Name = "Label39"
		Me.Label39.Size = New System.Drawing.Size(58, 15)
		Me.Label39.TabIndex = 114
		Me.Label39.Text = "Tipo:"
		'
		'Label3
		'
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label3.Location = New System.Drawing.Point(32, 16)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(120, 16)
		Me.Label3.TabIndex = 113
		Me.Label3.Text = "Proveedor:"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label2
		'
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label2.Location = New System.Drawing.Point(32, 48)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(120, 16)
		Me.Label2.TabIndex = 111
		Me.Label2.Text = "N�mero factura:"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label1
		'
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label1.Location = New System.Drawing.Point(393, 48)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(120, 16)
		Me.Label1.TabIndex = 112
		Me.Label1.Text = "Fecha:"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtNumeroFactura
		'
		Me.txtNumeroFactura.Location = New System.Drawing.Point(168, 48)
		Me.txtNumeroFactura.Name = "txtNumeroFactura"
		Me.txtNumeroFactura.Size = New System.Drawing.Size(210, 20)
		Me.txtNumeroFactura.TabIndex = 1
		'
		'dtpFecha
		'
		Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.dtpFecha.Location = New System.Drawing.Point(522, 48)
		Me.dtpFecha.Name = "dtpFecha"
		Me.dtpFecha.Size = New System.Drawing.Size(104, 20)
		Me.dtpFecha.TabIndex = 2
		'
		'cmbTipo
		'
		Me.cmbTipo.DisplayMember = "CON"
		Me.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cmbTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmbTipo.ForeColor = System.Drawing.Color.Blue
		Me.cmbTipo.Items.AddRange(New Object() {"CON", "CRE"})
		Me.cmbTipo.Location = New System.Drawing.Point(640, 16)
		Me.cmbTipo.Name = "cmbTipo"
		Me.cmbTipo.Size = New System.Drawing.Size(61, 21)
		Me.cmbTipo.TabIndex = 3
		Me.cmbTipo.ValueMember = "CON"
		'
		'cmbProveedor
		'
		Me.cmbProveedor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmbProveedor.DataSource = Me.bsProveedor
		Me.cmbProveedor.DisplayMember = "Nombre"
		Me.cmbProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cmbProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmbProveedor.ForeColor = System.Drawing.Color.Blue
		Me.cmbProveedor.ItemHeight = 13
		Me.cmbProveedor.Location = New System.Drawing.Point(168, 16)
		Me.cmbProveedor.Name = "cmbProveedor"
		Me.cmbProveedor.Size = New System.Drawing.Size(459, 21)
		Me.cmbProveedor.TabIndex = 0
		Me.cmbProveedor.ValueMember = "CodigoProv"
		'
		'Label15
		'
		Me.Label15.BackColor = System.Drawing.SystemColors.ControlLight
		Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label15.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label15.Location = New System.Drawing.Point(640, 48)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(58, 15)
		Me.Label15.TabIndex = 116
		Me.Label15.Text = "Moneda:"
		'
		'cmbMoneda
		'
		Me.cmbMoneda.DataSource = Me.bsMoneda
		Me.cmbMoneda.DisplayMember = "MonedaNombre"
		Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cmbMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmbMoneda.ForeColor = System.Drawing.Color.Blue
		Me.cmbMoneda.Location = New System.Drawing.Point(704, 46)
		Me.cmbMoneda.Name = "cmbMoneda"
		Me.cmbMoneda.Size = New System.Drawing.Size(94, 21)
		Me.cmbMoneda.TabIndex = 4
		Me.cmbMoneda.ValueMember = "CodMoneda"
		'
		'bsMoneda
		'
		Me.bsMoneda.DataMember = "Moneda"
		Me.bsMoneda.DataSource = Me.dtsGasto
		'
		'chSimplificado
		'
		Me.chSimplificado.AutoSize = True
		Me.chSimplificado.Enabled = False
		Me.chSimplificado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.chSimplificado.ForeColor = System.Drawing.Color.RoyalBlue
		Me.chSimplificado.Location = New System.Drawing.Point(763, 20)
		Me.chSimplificado.Name = "chSimplificado"
		Me.chSimplificado.Size = New System.Drawing.Size(94, 17)
		Me.chSimplificado.TabIndex = 223
		Me.chSimplificado.Text = "Simplificado"
		Me.chSimplificado.UseVisualStyleBackColor = True
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.txtTipoCambio)
		Me.GroupBox1.Controls.Add(Me.chSimplificado)
		Me.GroupBox1.Controls.Add(Me.cmbMoneda)
		Me.GroupBox1.Controls.Add(Me.Label15)
		Me.GroupBox1.Controls.Add(Me.cmbProveedor)
		Me.GroupBox1.Controls.Add(Me.cmbTipo)
		Me.GroupBox1.Controls.Add(Me.dtpFecha)
		Me.GroupBox1.Controls.Add(Me.txtNumeroFactura)
		Me.GroupBox1.Controls.Add(Me.Label1)
		Me.GroupBox1.Controls.Add(Me.Label2)
		Me.GroupBox1.Controls.Add(Me.Label3)
		Me.GroupBox1.Controls.Add(Me.Label39)
		Me.GroupBox1.Location = New System.Drawing.Point(16, 40)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(883, 78)
		Me.GroupBox1.TabIndex = 120
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Encabezado"
		'
		'MonedaTableAdapter
		'
		Me.MonedaTableAdapter.ClearBeforeFill = True
		'
		'txtTipoCambio
		'
		Me.txtTipoCambio.BackColor = System.Drawing.Color.White
		Me.txtTipoCambio.Enabled = False
		Me.txtTipoCambio.Location = New System.Drawing.Point(804, 46)
		Me.txtTipoCambio.Name = "txtTipoCambio"
		Me.txtTipoCambio.Size = New System.Drawing.Size(73, 20)
		Me.txtTipoCambio.TabIndex = 130
		Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'frmGasto
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(962, 488)
		Me.Controls.Add(Me.chConfirmada)
		Me.Controls.Add(Me.PanelImportarXML)
		Me.Controls.Add(Me.lblAsiento)
		Me.Controls.Add(Me.TxtNombreUsuario)
		Me.Controls.Add(Me.Label17)
		Me.Controls.Add(Me.txtClave)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.ToolBar1)
		Me.Controls.Add(Me.Label46)
		Me.Name = "frmGasto"
		Me.Text = "Registro de gastos"
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox2.PerformLayout()
		CType(Me.gridDetalle, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.dtsGasto, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.PanelImportarXML.ResumeLayout(False)
		Me.PanelImportarXML.PerformLayout()
		CType(Me.bsProveedor, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.bsMoneda, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region "Funciones GUI"

	Private Sub frmGasto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		MonedaTableAdapter.Connection.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
		Me.MonedaTableAdapter.Fill(Me.dtsGasto.Moneda)

		ProveedoresTableAdapter.Connection.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
		Me.ProveedoresTableAdapter.Fill(Me.dtsGasto.Proveedores)
		Cargar()
	End Sub

	Private Sub gridDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gridDetalle.KeyDown
		If e.KeyCode = Keys.Delete Then
			EliminarDetalle()
		End If
	End Sub

	Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
		Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
		PMU = VSM(Usua.Cedula, Me.Name) 'Carga los privilegios del usuario con el modu

		Select Case ToolBar1.Buttons.IndexOf(e.Button)
			Case 0 : Nuevo()
			Case 1 : If PMU.Find Then Buscar() Else MsgBox("No tiene permiso para Buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

			Case 2 : If PMU.Update Then AgregarEncabezadoBD(1) Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
			Case 3 : If PMU.Delete Then EliminarBD() Else MsgBox("No tiene permiso para eliminar o anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
			Case 4 : If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir los datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
			Case 5 : If PMU.Update Then AgregarEncabezadoBD(2) Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
			Case 8 : Me.CalcularTotales()
			Case 11 : spImportarXML()
			Case 12 : Me.Close()

		End Select


	End Sub
	Dim clsImportarXml As New Importar_XML.clsImportarXml(GetSetting("seesoft", "Seepos", "conexion"))

	Private Sub spImportarXML()
		Try
			Dim cx As New Conexion
			clsImportarXml.spBuscarArchivo()
			If clsImportarXml.esFactura Then
				If fnCargarDatosFactura() Then
					clsImportarXml.esXML = True
					PanelImportarXML.Visible = True
					chConfirmada.Visible = True
					chSimplificado.Checked = False
					ToolBarRegistrarFEC.Enabled = False
					txtDetalleXML.Text = "0 de " & clsImportarXml.dtsDatosFactura.FacturaDetalle.Count

					ToolBarImportarXML.Enabled = False
					chSimplificado.Checked = False
					chSimplificado.Visible = False
					spProcesarDetalleXML()
				End If
			End If

		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub


	Private Function fnCargarDatosFactura() As Boolean
		Try
RECARGARPROVEEDOR:
			Dim existeProveedor As Boolean
			For i As Integer = 0 To dtsGasto.Proveedores.Count - 1
				If dtsGasto.Proveedores(i).Cedula = clsImportarXml.dtsDatosFactura.Proveedor(0).Cedula Then
					bsProveedor.Position = i
					existeProveedor = True
				End If
			Next
			If Not existeProveedor Then
				If MessageBox.Show("No se encuentra la informaci�n del proveedor. Desea Registrar el proveedor ?", "Atenci�n", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
					Dim frm As New frmProveedores(Usua)
					frm.ShowDialog()
					ProveedoresTableAdapter.Fill(dtsGasto.Proveedores)
					GoTo RECARGARPROVEEDOR
				Else
					Return False
				End If
			Else
				txtNumeroFactura.Text = clsImportarXml.dtsDatosFactura.Factura(0).NumeroConsecutivo
				dtpFecha.Value = clsImportarXml.dtsDatosFactura.Factura(0).FechaEmision.Substring(0, 10)
				cmbTipo.SelectedIndex = 1
				If clsImportarXml.dtsDatosFactura.Factura(0).CodigoMoneda = "CRC" Then
					cmbMoneda.SelectedValue = 1
					txtTipoCambio.Text = BuscarTipoCambio(1)
				Else
					cmbMoneda.SelectedValue = 2
					txtTipoCambio.Text = BuscarTipoCambio(2)
				End If

				Return fnValidarNumeroFactura()
			End If
			Return True
		Catch ex As Exception
			MsgBox(ex.Message)
			Return False
		End Try
	End Function
	Private Sub spProcesarDetalleXML()
		Try
			For i As Integer = 0 To clsImportarXml.dtsDatosFactura.FacturaDetalle.Rows.Count - 1

				If Not clsImportarXml.dtsDatosFactura.FacturaDetalle(i).Procesada Then
					Dim dt As New DataTable



					txtDetalleArticuloDescripcion.Text = clsImportarXml.dtsDatosFactura.FacturaDetalle(i).Detalle

					txtDetallePrecioUnidad.Text = clsImportarXml.dtsDatosFactura.FacturaDetalle(i).PrecioUnitario
					txtDetalleCantidad.Text = clsImportarXml.dtsDatosFactura.FacturaDetalle(i).Cantidad
					txtDetalleDescuento.Text = (clsImportarXml.dtsDatosFactura.FacturaDetalle(i).MontoDescuento * 100) / (clsImportarXml.dtsDatosFactura.FacturaDetalle(i).Cantidad * clsImportarXml.dtsDatosFactura.FacturaDetalle(i).PrecioUnitario)

					txtImpuesto.Text = clsImportarXml.dtsDatosFactura.FacturaDetalle(i).Tarifa

					spCalcularProrrata()

					GroupBox1.Enabled = False

					clsImportarXml.NumeroLineaXML = i
					ActivarDetalle()
					txtCuentaContable.Focus()
					Exit Sub

				End If
			Next
			If clsImportarXml.NumeroLineaProcesadasXML < clsImportarXml.dtsDatosFactura.FacturaDetalle.Rows.Count Then btnContinuar.Visible = True
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub


	Private Function fnValidarNumeroFactura() As Boolean
		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim rstReader As System.Data.SqlClient.SqlDataReader

		cnnConexion = clsConexion.Conectar("SeePos")

		sql = " SELECT COUNT(*) FROM Compras where factura = '" & txtNumeroFactura.Text & "' AND CodigoProv=" & cmbProveedor.SelectedValue

		rstReader = clsConexion.GetRecorset(cnnConexion, sql)
		If rstReader.Read() = False Then Return False
		If rstReader(0) = 0 Then
		Else
			MsgBox("El n�mero de factura ya esta registrada, favor revisar...", MsgBoxStyle.Information, "Atenci�n ...")
			clsConexion.DesConectar(cnnConexion)

			Return False
		End If
		clsConexion.DesConectar(cnnConexion)

		Return True
	End Function

	Private Sub cmbMoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMoneda.SelectedIndexChanged
		If Buscando = 0 Then
			If Me.cmbMoneda.SelectedIndex = -1 Then
				Exit Sub
			ElseIf Me.cmbMoneda.SelectedIndex = 0 Then
				txtTipoCambio.Text = BuscarTipoCambio(1)
			Else
				txtTipoCambio.Text = BuscarTipoCambio(2)

			End If

			ObtenerFormatoMoneda(Me.cmbMoneda.SelectedValue)
		End If
	End Sub

#Region "Funciones KeyDown"

	Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
		If e.KeyCode = Keys.Enter Then
			If ValidarUsuario() = False Then
				Me.DesactivarCabezera()
				DesactivarToolBar()
				MsgBox("Contrase�a incorrecta", MsgBoxStyle.Information)
				Me.txtClave.Text = ""
			Else
				Me.tlbNuevo.Enabled = True
				Me.tlbBuscar.Enabled = True
				Nuevo()
			End If

		End If
	End Sub

	Private Sub txtNumeroFactura_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroFactura.KeyDown
		If Me.cmbProveedor.SelectedIndex = -1 Then
			MsgBox("Elija primero el proveedor", MsgBoxStyle.Information)
			Me.txtNumeroFactura.Text = ""
			cmbProveedor.Focus()
			Exit Sub
		End If
		If e.KeyCode = Keys.Enter Then
			If txtNumeroFactura.Text = "" Then Exit Sub
			If ValidarFactura() = False Then
				MsgBox("El n�mero de factura ya existe", MsgBoxStyle.Information)
				txtNumeroFactura.Text = ""
				txtNumeroFactura.Focus()
				Exit Sub
			End If
			SendKeys.Send("{TAB}")
		End If

	End Sub

	Private Sub dtpFecha_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtpFecha.KeyDown
		If e.KeyCode = Keys.Enter Then
			SendKeys.Send("{TAB}")
		End If
	End Sub

	Private Sub cmbTipo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbTipo.KeyDown
		If e.KeyCode = Keys.Enter Then
			SendKeys.Send("{TAB}")
		End If
	End Sub

	Private Sub cmbProveedor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbProveedor.KeyDown
		If e.KeyCode = Keys.Enter Then
			If e.KeyCode = Keys.Enter Then
				SendKeys.Send("{TAB}")
			End If
		End If
		If e.KeyCode = Keys.F1 Then
			Dim Fx As New cFunciones
			Dim valor As String
			valor = Fx.BuscarDatos("Select CodigoProv,Nombre from Proveedores", "Nombre", "Buscar Proveedor...", GetSetting("SeeSoft", "Seepos", "CONEXION"))

			If valor = "" Then
				Me.cmbProveedor.SelectedIndex = -1
			Else
				cmbProveedor.SelectedValue = valor

			End If
		End If
	End Sub

	Private Sub cmbMoneda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbMoneda.KeyDown
		If e.KeyCode = Keys.Enter Then
			If ValidarCabezera() = True Then
				ActivarDetalle()
				Me.txtDetalleCantidad.Focus()
			End If

		End If
	End Sub
	Private Sub txtCantidad_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDetalleCantidad.KeyDown
		If e.KeyCode = Keys.Enter Then
			SendKeys.Send("{TAB}")
		End If
	End Sub

	Private Sub txtDescripcion_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDetalleArticuloDescripcion.KeyDown
		If e.KeyCode = Keys.Enter Then
			SendKeys.Send("{TAB}")
		End If
	End Sub

	Private Sub txtPrecioUnidad_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDetallePrecioUnidad.KeyDown
		If e.KeyCode = Keys.Enter Then
			SendKeys.Send("{TAB}")
		End If
	End Sub

	Private Sub txtDescuento_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDetalleDescuento.KeyDown
		If e.KeyCode = Keys.Enter Then
			SendKeys.Send("{TAB}")
		End If
	End Sub

	Private Sub txtImpuesto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImpuesto.KeyDown
		If e.KeyCode = Keys.Enter Then
			spCalcularProrrata()
			SendKeys.Send("{TAB}")
		End If
	End Sub

	Private Sub txtCuentaContable_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCuentaContable.KeyDown
		If e.KeyCode = Keys.Enter Then
			If Not chSimplificado.Checked Then
				spAgregarLinea()
			Else
				txtCodigoCabys.Focus()
			End If
		End If
		If e.KeyCode = 112 Then
			LlamarFmrBuscarAsientoVenta()
		End If
	End Sub

	Private Sub spAgregarLinea()
		BuscarCuentaContable()
		If ValidarDetalle() = True Then
			spCalcularProrrata()
			AgregarDetalle()
			CalcularTotales()
			LimpiarDetalle()
			txtDetalleCantidad.Focus()
			IdDetalle = -1
			If clsImportarXml.esXML Then
				clsImportarXml.dtsDatosFactura.FacturaDetalle(clsImportarXml.NumeroLineaXML).Procesada = True
				clsImportarXml.NumeroLineaProcesadasXML += 1
				txtDetalleXML.Text = (clsImportarXml.NumeroLineaProcesadasXML).ToString & " de " & clsImportarXml.dtsDatosFactura.FacturaDetalle.Count
				spProcesarDetalleXML()
			End If
			If chSimplificado.Checked Then
				chSimplificado.Enabled = True
			End If
		End If
	End Sub

#End Region

#End Region

#Region "Funciones Basicas"

	Private Sub AgregarDetalle()

		Dim Descuento, PrecioUnidad, TotalImpuesto As Double
		Dim Cantidad As Integer
		Descuento = Me.txtDetalleDescuento.Text
		Cantidad = Me.txtDetalleCantidad.Text
		PrecioUnidad = Me.txtDetallePrecioUnidad.Text
		impuesto = Me.txtImpuesto.Text

		Dim NuevaFila As DatasetGasto.GastoDetalleRow
		NuevaFila = Me.dtsGasto.GastoDetalle.NewGastoDetalleRow
		NuevaFila.Cantidad = Me.txtDetalleCantidad.Text
		NuevaFila.IdCompra = -1
		NuevaFila.Descuento = (Descuento / 100) * (PrecioUnidad * Cantidad)
		NuevaFila.Impuesto = (impuesto / 100) * ((PrecioUnidad * Cantidad) - NuevaFila.Descuento)
		NuevaFila.Impuesto_p = impuesto
		NuevaFila.Total = (PrecioUnidad * Cantidad) - NuevaFila.Descuento
		NuevaFila.CuentaContable = Me.txtCuentaContable.Text
		NuevaFila.Descripcion = Me.txtDetalleArticuloDescripcion.Text
		NuevaFila.CuentaContableDescripcion = Me.txtCuentaContableDescripcion.Text
		NuevaFila.Descuento_P = Descuento

		If impuesto = 0 Then
			NuevaFila.Gravado = 0
			NuevaFila.Exento = NuevaFila.Total
		Else
			NuevaFila.Gravado = NuevaFila.Total
			NuevaFila.Exento = 0
		End If

		NuevaFila.Costo = Me.txtDetallePrecioUnidad.Text
		NuevaFila.NuevoCostoBase = NuevaFila.Total / Cantidad
		NuevaFila.MontoGasto = CDbl(txtMontoGasto.Text)
		NuevaFila.ImpuestoAplicable = CDbl(txtImpuestoAplicable.Text)
		NuevaFila.CodigoCabys = txtCodigoCabys.Text
		dtsGasto.GastoDetalle.AddGastoDetalleRow(NuevaFila)


	End Sub

	Private Sub EliminarDetalle()

		If MsgBox("Desea Eliminar este item del detalle..", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
			Exit Sub
		End If

		Dim posicion As Integer
		If Me.dtsGasto.GastoDetalle.Count = 0 Then Exit Sub
		posicion = Me.BindingContext(dtsGasto.GastoDetalle).Position()
		dtsGasto.GastoDetalle.Rows.RemoveAt(posicion)
		CalcularTotales()
	End Sub

	Private Sub BuscarEncabezado(ByVal pIdGasto As Double)

		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim rsReader As SqlClient.SqlDataReader


		sql = "SELECT Factura,TipoCambio,CodigoProv,Fecha,TipoCompra,Cod_MonedaCompra,XML,Confirmada,Simplificado FROM dbo.Compras  WHERE ID_Compra = " & pIdGasto

		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seepos", "CONEXION")
		cnnConexion.Open()
		rsReader = clsConexion.GetRecorset(cnnConexion, sql)
		If rsReader.Read() = False Then Exit Sub

		Me.txtNumeroFactura.Text = rsReader("Factura")
		Me.dtpFecha.Value = rsReader("Fecha")
		If rsReader("TipoCompra") = "CON" Then
			Me.cmbTipo.SelectedIndex = 0
		Else
			Me.cmbTipo.SelectedIndex = 1
		End If

		txtTipoCambio.Text = rsReader("TipoCambio")
		cmbProveedor.SelectedValue = rsReader("CodigoProv")
		cmbMoneda.SelectedValue = rsReader("Cod_MonedaCompra")
		ObtenerFormatoMoneda(rsReader("Cod_MonedaCompra"))
		clsImportarXml.esXML = rsReader("XML")
		chConfirmada.Checked = rsReader("Confirmada")
		chSimplificado.Checked = rsReader("Simplificado")
		chConfirmada.Visible = clsImportarXml.esXML
		chSimplificado.Visible = Not clsImportarXml.esXML
		chSimplificado.Enabled = False
		cnnConexion.Close()

		CargarGridDetalle(pIdGasto)
		CalcularTotales()

	End Sub





	Private Function BuscarTipoCambio(ByVal pIdMoneda As Double) As Double

		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim rsReader As SqlClient.SqlDataReader

		BuscarTipoCambio = 1
		sql = "SELECT ValorCompra FROM SeePos.dbo.Moneda  WHERE CodMoneda = " & pIdMoneda

		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seepos", "CONEXION")
		cnnConexion.Open()
		rsReader = clsConexion.GetRecorset(cnnConexion, sql)
		If rsReader.Read() = False Then Exit Function

		BuscarTipoCambio = rsReader("ValorCompra")

		cnnConexion.Close()


	End Function
	Private Function GetNumasiento(ByVal _id As Long) As String
		Try
			Dim dts As New DataTable
			cFunciones.Llenar_Tabla_Generico("select asiento from compras where gasto = 1 and id_compra = " & _id, dts, GetSetting("seesoft", "Seepos", "conexion"))
			If dts.Rows.Count > 0 Then
				Return dts.Rows(0).Item(0)
			Else
				Return "0"
			End If
		Catch ex As Exception
			Return "0"
		End Try
	End Function
	Private Function EstaMayorizado(ByVal _asiento As String) As Boolean
		Try
			Dim dts As New DataTable
			cFunciones.Llenar_Tabla_Generico("select mayorizado from asientoscontables where numasiento = '" & _asiento & "'", dts, GetSetting("seesoft", "contabilidad", "conexion"))
			If dts.Rows.Count > 0 Then
				If dts.Rows(0).Item(0) = True Then
					Return True
				Else
					Return False
				End If
			End If
		Catch ex As Exception
			Return False
		End Try
	End Function
	Private Function IsCerrado(ByVal _fecha As Date) As Boolean
		Try
			Dim dts As New DataTable
			cFunciones.Llenar_Tabla_Generico("select Cerrado from Periodo where Mes = " & _fecha.Month & " and Anno = " & _fecha.Year & "", dts, GetSetting("SeeSOFT", "Contabilidad", "Conexion"))
			If dts.Rows.Count > 0 Then
				If dts.Rows(0).Item(0) = 1 Or dts.Rows(0).Item(0) = True Then
					Return True
				Else
					Return False
				End If
			Else
				Return False
			End If
		Catch ex As Exception
			MsgBox(ex.Message, MsgBoxStyle.Exclamation, Text)
			Return False
		End Try
	End Function

	Private Function ValidarTipoCambio() As Boolean
		Try
			If CDbl(txtTipoCambio.Text) <= 0 Then
				MsgBox("El tipo de cambio no puede ser 0.", MsgBoxStyle.Information, "Validar tipo cambio.")
				txtTipoCambio.Focus()
				Return False
			End If
			Return True
		Catch ex As Exception
			MsgBox("El tipo de cambio debe ser un valor num�rico." & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "Validar tipo cambio.")
			txtTipoCambio.Focus()
			Return False
		End Try
	End Function
	Private Sub AgregarEncabezadoBD(TipoRegistro As Int16)
		Buscando = 0

		If Me.IsCerrado(Me.dtpFecha.Value.ToShortDateString) = True Then
			MsgBox("Esta operaci�n no se puede realizar!!!!" & vbCrLf _
			& "" & vbCrLf _
			& "Por motivo de que el periodo de trabajo contable se encuentra cerrado." & vbCrLf _
			& "Consulte con el departamento de contabilidad", MsgBoxStyle.Exclamation, Text)
			Exit Sub
		End If

		If Me.dtsGasto.GastoDetalle.Count = 0 Then
			MsgBox("No se llenaron los item del gasto, no se pude registrar el gasto", MsgBoxStyle.Information)
			Exit Sub
		End If

		If Not ValidarTipoCambio() Then
			Exit Sub
		End If
		' If ValidarModificarElimar() = False Then
		' MsgBox("No se puede modificar la factura")
		' Exit Sub
		'End If
		Dim cf As New cFunciones
		Dim Periodo As String
		Periodo = cf.BuscaPeriodo(Me.dtpFecha.Value)
		If Periodo = Nothing Then
			MsgBox("El periodo contable no existe o esta cerrado!!!", MsgBoxStyle.Exclamation, Text)
			Exit Sub
		End If

		Dim asi As String = GetNumasiento(Me.IdGasto)
		If EstaMayorizado(asi) = True Then
			MsgBox("La operaci�n no se puede realizar, El Asiento esta Mayorizado", MsgBoxStyle.Exclamation, )
			Exit Sub
		End If

		If fnValidar_Abono_Ajuste(IdGasto) Then
			MsgBox("La factura no se puede modificar porque tiene pagos asociados.")
			Exit Sub
		End If

		If clsImportarXml.esXML And IdGasto <= 0 Then
			If clsImportarXml.NumeroLineaXML < clsImportarXml.dtsDatosFactura.FacturaDetalle.Count - 1 Then
				MessageBox.Show("Debe ingresar todos los art�culos al detalle.", "Informaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
				btnContinuar.Visible = True
				Exit Sub
			End If
			'	If MessageBox.Show("Desea proceder a confirmar la factura de Compra...", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
			clsImportarXml.reintenta = False
REINTENTARCONFIRMAR:
			If clsImportarXml.fnConfimarFactura(False) Then
				MessageBox.Show("Factura confirmada correctamente.", "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Information)

			Else
				Dim dr As New DialogResult
				dr = clsImportarXml.fnMostrarMensaje()
				If dr = DialogResult.No Then
					clsImportarXml.reintenta = True
					GoTo REINTENTARCONFIRMAR
				End If
				If dr = DialogResult.Cancel Then
					Exit Sub
				End If

			End If

		End If

		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim rstReader As System.Data.SqlClient.SqlDataReader
		Dim Grabado, exento, TotalImpuesto, TotalDescuento, Total As Double
		Dim FechaVence As Date
		Dim Plazo As Double
		Dim Cx As New Conexion
		Dim n As Integer

		TotalImpuesto = Me.txtTotalImpuesto.Text
		Total = Me.txtTotal.Text
		TotalDescuento = Me.txtTotalDescuento.Text

		For n = 0 To Me.dtsGasto.GastoDetalle.Count - 1
			With dtsGasto.GastoDetalle(n)
				Grabado = Grabado + .Gravado
				exento = exento + .Exento

				If chSimplificado.Checked Then
					If .CodigoCabys = "" Then
						MessageBox.Show("Por favor, ingrese el c�digo cabys de las lineas.", "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Warning)
						Exit Sub
					End If
				End If
			End With
		Next

		Dim TipoFactura(2) As String
		TipoFactura(0) = "CON"
		TipoFactura(1) = "CRE"


		Dim Simplificado As Boolean
		If TipoRegistro = 1 Then
			Simplificado = False
		ElseIf TipoRegistro = 2 Then
			Simplificado = True
		End If

		Dim _TipoCambio As Double = 0
		If txtTipoCambio.Text = "" Then
			_TipoCambio = 0
		Else
			_TipoCambio = txtTipoCambio.Text
		End If

		Plazo = Cx.SlqExecuteScalar(Cx.Conectar("Seepos"), "SELECT Plazo FROM Proveedores WHERE CodigoProv = " & cmbProveedor.SelectedValue)
		Cx.DesConectar(Cx.sQlconexion)
		FechaVence = Me.dtpFecha.Value.Date.AddDays(Plazo)

		If IdGasto = -1 Then
			sql = " INSERT INTO Compras (Factura,CodigoProv,SubTotalGravado,SubTotalExento,Descuento,Impuesto" &
				" ,TotalFactura, Fecha,Vence,FechaIngreso,Gasto,TipoCompra,Cod_MonedaCompra,TipoCambio,XML,Confirmada, Simplificado, MontoGasto, ImpuestoAplicable) " &
				" VALUES ('" & txtNumeroFactura.Text & "'," & cmbProveedor.SelectedValue & "," &
				Grabado & "," & exento & "," & TotalDescuento & "," &
				TotalImpuesto & "," & Total & ",'" & Me.dtpFecha.Value.Date & "','" &
				FechaVence & "','" & Date.Now.Date & "',1,'" & TipoFactura(Me.cmbTipo.SelectedIndex) & "'," & Me.cmbMoneda.SelectedValue & "," &
				_TipoCambio & ",'" & clsImportarXml.esXML & "', '" & clsImportarXml.Confirmada & "', '" & Simplificado & "'," & CDbl(txtTotalMontoGasto.Text) & "," & CDbl(txtTotalImpuestoAplicable.Text) & ")"
		Else
			sql = "UPDATE Compras SET Factura ='" & txtNumeroFactura.Text & "',CodigoProv=" & cmbProveedor.SelectedValue & "," &
					"SubTotalGravado=" & Grabado & ",SubTotalExento=" & exento & ",Descuento=" & TotalDescuento & "," &
					"Impuesto =" & TotalImpuesto & ", TotalFactura=" & Total & ",Fecha ='" & dtpFecha.Value.Date & "'," &
					"Vence = '" & FechaVence & "', FechaIngreso='" & dtpFecha.Value.Date & "'" &
					",TipoCompra='" & TipoFactura(Me.cmbTipo.SelectedIndex) & "',Cod_MonedaCompra=" & Me.cmbMoneda.SelectedValue &
					", TipoCambio = " & _TipoCambio &
					", MontoGasto = " & CDbl(txtTotalMontoGasto.Text) & ", ImpuestoAplicable = " & CDbl(txtTotalImpuestoAplicable.Text) &
					"  WHERE ID_Compra =" & IdGasto
		End If

		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seepos", "CONEXION")
		cnnConexion.Open()
		clsConexion.SlqExecute(cnnConexion, sql)

		If IdGasto = -1 Then
			sql = "Select max(id_compra) from compras where Factura = '" & Me.txtNumeroFactura.Text & "' and codigoprov = " & cmbProveedor.SelectedValue & " and gasto = 1"
			rstReader = clsConexion.GetRecorset(cnnConexion, sql)
			rstReader.Read()
			IdGasto = rstReader(0)
		End If
		cnnConexion.Close()

		AgregarDetalleBD(IdGasto)

		regenera_asiento(IdGasto)

		Me.lblAsiento.Text = ""

		MsgBox("La factura de gasto ha sido registrado correctamente", MsgBoxStyle.Information)

		Try
			If clsImportarXml.esXML Then
				clsImportarXml.spActualizarIdFactura(IdGasto)
			End If
		Catch ex As Exception

		End Try

		Me.tlbNuevo.Enabled = True
		Me.tlbBuscar.Enabled = True
		Me.tlbRegistrar.Enabled = True
		Me.ToolBarRegistrarFEC.Enabled = True
		Me.tlbEliminar.Enabled = True
		Me.tlbImprimir.Enabled = True

		Me.LimpiarCabezera()
		Me.ActivarCabezera()
		Me.DesactivarDetalle()
		Me.dtsGasto.GastoDetalle.Clear()
	End Sub

	Private Sub regenera_asiento(ByVal _idcompra As String)
		Dim dt As New DataTable
		Dim NumAsiento, Periodo, CImpuesto, DImpuesto, CProveedor, DProveedor As String
		Dim TC As Decimal
		Dim db As New GestioDatos
		Dim cf As New cFunciones
		Dim Impuesto As Decimal
		NumAsiento = "0"

		'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		'Obtenemos datos del asiento.
		cFunciones.Llenar_Tabla_Generico("Select Asiento,Impuesto From SeePos.dbo.Compras As c Where c.Id_Compra = " & _idcompra, dt, GetSetting("SeeSoft", "SeePOS", "Conexion"))
		If dt.Rows.Count > 0 Then
			NumAsiento = dt.Rows(0).Item("Asiento")
			Impuesto = dt.Rows(0).Item("Impuesto")
			If NumAsiento = "0" Or NumAsiento = "" Then Exit Sub
			Periodo = cf.BuscaPeriodo(Me.dtpFecha.Value)
			If Periodo = Nothing Then
				MsgBox("El periodo contable no existe o esta cerrado!!!", MsgBoxStyle.Exclamation, Text)
				Exit Sub
			End If
		Else
			Exit Sub
		End If
		'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		'Obtenemos la cuenta de impuestos.
		cFunciones.Llenar_Tabla_Generico("Select CuentaContable, Descripcion from Contabilidad.dbo.CuentaContable As c Inner Join Contabilidad.dbo.SettingCuentaContable as s on s.IdImpuestoVenta = c.Id", dt, GetSetting("SeeSOFT", "SeePOS", "Conexion"))
		If dt.Rows.Count > 0 Then
			CImpuesto = dt.Rows(0).Item("CuentaContable")
			DImpuesto = dt.Rows(0).Item("Descripcion")
		Else
			Exit Sub
		End If
		'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		'Obtenemos la cuenta del proveedor.
		cFunciones.Llenar_Tabla_Generico("Select p.CuentaContable, p.DescripcionCuentaContable From SeePos.dbo.Compras As c Inner Join Seepos.dbo.Proveedores as p on c.CodigoProv = p.CodigoProv Where c.Id_Compra = " & _idcompra, dt, GetSetting("SeeSOFT", "SeePOS", "Conexion"))
		If dt.Rows.Count > 0 Then
			CProveedor = dt.Rows(0).Item("CuentaContable")
			DProveedor = dt.Rows(0).Item("DescripcionCuentaContable")
		Else
			Exit Sub
		End If


		'Eliminamos el asiento.
		db.Ejecuta("Delete from Contabilidad.dbo.AsientosContables where NumAsiento = '" & NumAsiento & "'")
		db.Ejecuta("Delete From Contabilidad.dbo.DetallesAsientosContable WHERE NumAsiento = '" & NumAsiento & "'")
		'Creamos el nuevo asiento.
		If txtTipoCambio.Text = "" Or txtTipoCambio.Text = "." Then
			TC = 0
		Else
			TC = txtTipoCambio.Text
		End If
		db.Ejecuta("INSERT INTO [Contabilidad].[dbo].[AsientosContables] ([NumAsiento],[Fecha],[IdNumDoc],[NumDoc],[Beneficiario],[TipoDoc],[Accion],[Anulado],[FechaEntrada],[Mayorizado],[Periodo],[NumMayorizado],[Modulo],[Observaciones],[NombreUsuario],[TotalDebe],[TotalHaber],[CodMoneda],[TipoCambio]) VALUES ('" & NumAsiento & "','" & Me.dtpFecha.Value.ToShortDateString & "'," & _idcompra & ",'" & Me.txtNumeroFactura.Text & "',' No. Fac : " & Me.txtNumeroFactura.Text & "  Proveedor:  " & Me.cmbProveedor.Text & "',0,'AUT',0,'" & Date.Now.ToShortDateString & "',1,'" & Periodo & "',0,'FACTURA GASTOS','FACTURA GASTOS. Regenerada.  Proveedor:  " & Me.cmbProveedor.Text & "','" & Me.TxtNombreUsuario.Text & "'," & CDec(Me.txtTotal.Text) & "," & CDec(Me.txtTotal.Text) & "," & Me.cmbMoneda.SelectedValue & "," & CDec(TC) & ")")
		'Agrega Cuenta Proveedor.
		db.Ejecuta("INSERT INTO [Contabilidad].[dbo].[DetallesAsientosContable]([NumAsiento],[Cuenta],[NombreCuenta],[Monto],[Debe],[Haber],[DescripcionAsiento],[Tipocambio]) VALUES ('" & NumAsiento & "','" & CProveedor & "','" & DProveedor & "'," & CDec(Me.txtTotal.Text) & ",0,1,'FACTURA GASTOS.   Proveedor:  " & Me.cmbProveedor.Text & "'," & CDec(TC) & ")")
		'Agrega Cuenta Impuesto.
		If Impuesto > 0 Then 'solo si hay impuestos
			db.Ejecuta("INSERT INTO [Contabilidad].[dbo].[DetallesAsientosContable]([NumAsiento],[Cuenta],[NombreCuenta],[Monto],[Debe],[Haber],[DescripcionAsiento],[Tipocambio]) VALUES ('" & NumAsiento & "','" & CImpuesto & "','" & DImpuesto & "'," & CDec(Impuesto) & ",1,0,'FACTURA GASTOS.   Proveedor:  " & Me.cmbProveedor.Text & "'," & CDec(TC) & ")")
		End If

		Dim _dt As New DataTable
		Dim cmd As String
		cmd = "SELECT     a.Descripcion, a.Base, a.Monto_Flete, a.OtrosCargos, a.Costo, a.Cantidad, a.Gravado, a.Exento, a.Descuento_P, a.Descuento, a.Impuesto_P, a.Impuesto, a.Total, a.Devoluciones, a.CuentaContable, ccm.Descripcion AS DesCC FROM  compras AS c INNER JOIN  Proveedores AS p ON c.CodigoProv = p.CodigoProv INNER JOIN  Articulos_Gastos AS a ON c.Id_Compra = a.IdCompra INNER JOIN  CuentasContableMovimimiento AS ccm ON a.CuentaContable = ccm.CuentaContable COLLATE Modern_Spanish_CI_AS WHERE    (c.Id_Compra = " & _idcompra & ") AND  (c.Gasto = 1) AND (c.TotalFactura > 0)"
		cFunciones.Llenar_Tabla_Generico(cmd, _dt, GetSetting("SeeSoft", "SeePOS", "Conexion"))
		If _dt.Rows.Count = 0 Then
			'  MsgBox("Linea no valida en Factura # " & dt.Rows(ic).Item("Factura"), MsgBoxStyle.OKOnly)
			Exit Sub
		End If
		For id As Integer = 0 To _dt.Rows.Count - 1
			If _dt.Rows(id).Item("Gravado") + _dt.Rows(id).Item("Exento") = 0 Then
				db.Ejecuta("INSERT INTO [Contabilidad].[dbo].[DetallesAsientosContable]([NumAsiento],[Cuenta],[NombreCuenta],[Monto],[Debe],[Haber],[DescripcionAsiento],[Tipocambio]) VALUES ('" & NumAsiento & "','" & _dt.Rows(id).Item("CuentaContable") & "','" & _dt.Rows(id).Item("DesCC") & "'," & CDec(_dt.Rows(id).Item("Total")) & ",1,0,'FACTURA GASTOS.   Proveedor:  " & Me.cmbProveedor.Text & "'," & CDec(TC) & ")")

			Else
				db.Ejecuta("INSERT INTO [Contabilidad].[dbo].[DetallesAsientosContable]([NumAsiento],[Cuenta],[NombreCuenta],[Monto],[Debe],[Haber],[DescripcionAsiento],[Tipocambio]) VALUES ('" & NumAsiento & "','" & _dt.Rows(id).Item("CuentaContable") & "','" & _dt.Rows(id).Item("DesCC") & "'," & CDec(_dt.Rows(id).Item("Gravado") + _dt.Rows(id).Item("Exento")) & ",1,0,'FACTURA GASTOS.   Proveedor:  " & Me.cmbProveedor.Text & "'," & CDec(TC) & ")")

			End If

		Next


	End Sub


	Private Sub AgregarDetalleBD(ByVal pIdGasto As Double)
		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim n As Integer


		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seepos", "CONEXION")
		cnnConexion.Open()

		sql = "DELETE FROM Articulos_Gastos WHERE IDCOMPRA = " & pIdGasto
		clsConexion.SlqExecute(cnnConexion, sql)


		For n = 0 To Me.dtsGasto.GastoDetalle.Count - 1
			With dtsGasto.GastoDetalle(n)
				sql = "INSERT INTO Articulos_Gastos(IdCompra,Descripcion,Base,Costo,Cantidad,Gravado,Exento," &
						"Descuento_p,Descuento,Impuesto_p,Impuesto,Total,NuevoCostoBase,CuentaContable,MontoGasto,ImpuestoAplicable,CodigoCabys)" &
						" VALUES(" & pIdGasto & ",'" & .Descripcion.Replace("'", "") &
						"'," & .Costo & "," & .Costo & "," & .Cantidad & "," & .Gravado & "," & .Exento &
						"," & .Descuento_P & "," & .Descuento & "," & .Impuesto_p & "," & .Impuesto &
						"," & .Total & "," & .NuevoCostoBase & ",'" & .CuentaContable & "'," & .MontoGasto & "," & .ImpuestoAplicable & ",'" & .CodigoCabys & "')"
				clsConexion.SlqExecute(cnnConexion, sql)
			End With
		Next
		cnnConexion.Close()

	End Sub

	Private Sub EliminarBD()
		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim n As Integer

		If IdGasto = -1 Then Exit Sub

		Dim asi As String = GetNumasiento(Me.IdGasto)
		If EstaMayorizado(asi) = True Then
			MsgBox("La operaci�n no se puede realizar, El Asiento esta Mayorizado", MsgBoxStyle.Exclamation, )
			Exit Sub
		End If

		If fnValidar_Abono_Ajuste(IdGasto) Then
			MsgBox("La factura no se puede modificar porque tiene pagos asociados.")
			Exit Sub
		End If

		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seepos", "CONEXION")
		cnnConexion.Open()


		If MessageBox.Show("�Desea eliminar esta Factura de gasto ?", "LcPymes", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = 6 Then


			'Elimnar los detalles
			sql = "Delete from Articulos_Gastos where IdCompra=" & IdGasto
			clsConexion.SlqExecute(cnnConexion, sql)

			'Eliminar la cabezara
			sql = "DELETE FROM  Compras where id_compra =" & IdGasto
			clsConexion.SlqExecute(cnnConexion, sql)

			sql = "update Contabilidad.dbo.AsientosContables set Anulado = 1 where NumAsiento = '" & asi & "'"
			clsConexion.SlqExecute(cnnConexion, sql)

			cnnConexion.Close()

			Me.tlbNuevo.Enabled = True
			Me.tlbBuscar.Enabled = True
			Me.tlbRegistrar.Enabled = False
			Me.tlbEliminar.Enabled = False
			Me.tlbImprimir.Enabled = False
			Me.ToolBarRegistrarFEC.Enabled = False

			Me.LimpiarCabezera()
			Me.DesactivarCabezera()
		End If
	End Sub

	Private Sub Nuevo()
		Try
			'Datos ImportarXML------------------
			If clsImportarXml.esXML Then
				PanelImportarXML.Visible = False
				chConfirmada.Visible = False
				btnContinuar.Visible = False
				clsImportarXml.spLimpiar()
			End If
			chSimplificado.Checked = False
			'-----------------------------------

			Me.lblAsiento.Text = ""
			Me.dtsGasto.GastoDetalle.Clear()
			If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then
				Me.ToolBar1.Buttons(0).Text = "Cancelar"
				Me.ToolBar1.Buttons(0).ImageIndex = 8
				Me.LimpiarCabezera()
				Me.ActivarCabezera()
				Me.DesactivarDetalle()

				Me.tlbNuevo.Enabled = True
				Me.tlbBuscar.Enabled = True
				Me.tlbRegistrar.Enabled = True
				Me.tlbEliminar.Enabled = False
				Me.tlbImprimir.Enabled = False
				Me.ToolBarRegistrarFEC.Enabled = True
				ToolBarImportarXML.Enabled = True
				GroupBox1.Enabled = True
				chSimplificado.Enabled = True
				chSimplificado.Visible = True
				ToolBarRegistrarFEC.Enabled = False
				Me.cmbProveedor.Focus()
			Else
				Me.ToolBar1.Buttons(0).Text = "Nuevo"
				Me.ToolBar1.Buttons(0).ImageIndex = 0
				Me.LimpiarCabezera()
				Me.DesactivarCabezera()

				Me.tlbNuevo.Enabled = True
				Me.tlbBuscar.Enabled = True
				Me.tlbRegistrar.Enabled = False
				Me.tlbEliminar.Enabled = False
				Me.tlbImprimir.Enabled = False
				Me.ToolBarRegistrarFEC.Enabled = False
				ToolBarImportarXML.Enabled = False
				GroupBox1.Enabled = False
				chSimplificado.Enabled = False
				chSimplificado.Visible = False
			End If



		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub Buscar()
		Me.LimpiarCabezera()
		Me.dtsGasto.GastoDetalle.Clear()
		Me.lblAsiento.Text = ""
		Buscando = 1
		Try

			Dim identificador As Double
			Dim Fx As New cFunciones
			identificador = CDbl(Fx.Buscar_X_Descripcion_Fecha("Select Id_Compra, (cast(Factura as varchar) + '-' + TipoCompra) as Factura,Proveedores.nombre,Fecha from compras inner join Proveedores on compras.CodigoProv = Proveedores.CodigoProv WHERE Compras.Gasto = 1 Order by Fecha DESC", "nombre", "Fecha", "Buscar Factura de Compra", GetSetting("SeeSoft", "seepos", "CONEXION")))


			If identificador = 0.0 Then ' si se dio en el boton de cancelar
				IdGasto = -1
				Exit Sub
			End If
			IdGasto = identificador

			'llenar las compras
			BuscarEncabezado(IdGasto)
			CargarlblAsiento(IdGasto)
			Me.ToolBar1.Buttons(2).Enabled = True
			Me.ToolBar1.Buttons(3).Enabled = True
			Me.ToolBar1.Buttons(4).Enabled = True


		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try


		Me.tlbNuevo.Enabled = True
		Me.tlbBuscar.Enabled = True
		Me.tlbRegistrar.Enabled = True
		Me.tlbEliminar.Enabled = True
		Me.tlbImprimir.Enabled = True
		Me.ToolBarRegistrarFEC.Enabled = True

		Me.ActivarCabezera()
		Me.ActivarDetalle()

	End Sub

	Private Sub Imprimir()
		If IdGasto = -1 Then Exit Sub

		Try
			Dim rptReporte As New rptGasto2
			rptReporte.SetParameterValue(0, IdGasto)
			CrystalReportsConexion.LoadShow(rptReporte, MdiParent, GetSetting("SeeSoft", "Seepos", "CONEXION"))

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub EditarDetalle()
		If IdDetalle = -1 Then Exit Sub
		impuesto = Me.txtImpuesto.Text
		With Me.dtsGasto.GastoDetalle(IdDetalle)
			Dim Descuento, PrecioUnidad, TotalImpuesto As Double
			Dim Cantidad As Integer
			.Cantidad = Me.txtDetalleCantidad.Text
			.IdCompra = -1
			.Descuento = (Descuento / 100) * (PrecioUnidad * Cantidad)
			.Impuesto = (impuesto / 100) * ((PrecioUnidad * Cantidad) - .Descuento)
			.Impuesto_p = impuesto
			.Total = (PrecioUnidad * Cantidad) - .Descuento + .Impuesto
			.CuentaContable = Me.txtCuentaContable.Text
			.Descripcion = Me.txtDetalleArticuloDescripcion.Text
			.CuentaContableDescripcion = Me.txtCuentaContableDescripcion.Text
			.Descuento_P = Me.txtDetalleDescuento.Text

			If impuesto = 0 Then
				.Gravado = 0
				.Exento = .Total
			Else
				.Gravado = .Total
				.Exento = 0
			End If

			.Costo = Me.txtDetallePrecioUnidad.Text
			.NuevoCostoBase = .Total / Cantidad
		End With
		CalcularTotales()
	End Sub

	Private Sub BuscarCuentaContable()
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim rstReader As System.Data.SqlClient.SqlDataReader
		Dim sql As String = "SELECT descripcion  FROM CuentaContable where CuentaContable = '" & Me.txtCuentaContable.Text & "' "

		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Contabilidad", "CONEXION")
		cnnConexion.Open()

		rstReader = clsConexion.GetRecorset(cnnConexion, sql)

		If rstReader.Read() = False Then
			Me.txtCuentaContable.Text = ""
			Me.txtCuentaContableDescripcion.Text = ""
			Exit Sub
		End If

		Me.txtCuentaContableDescripcion.Text = rstReader(0)

		cnnConexion.Close()
	End Sub

	Private Sub LlamarFmrBuscarAsientoVenta()

		Dim busca As New fmrBuscarMayorizacionAsiento
		busca.NuevaConexion = GetSetting("SeeSoft", "Contabilidad", "CONEXION")
		busca.sqlstring = " select CC.cuentacontable as [Cuenta contable],CC.descripcion as Descripcion,(SELECT descripcion from cuentacontable where id = cc.parentid) as [Cuenta madre] from cuentacontable CC " &
" where Movimiento=1 "
		busca.campo = "descripcion"
		busca.sqlStringAdicional = " ORDER BY CuentaContable  "
		busca.ShowDialog()

		If busca.codigo Is Nothing Then Exit Sub

		Me.txtCuentaContable.Text = busca.codigo
		Me.txtCuentaContableDescripcion.Text = busca.descrip

	End Sub

	Private Sub ObtenerFormatoMoneda(ByVal pIdMoneda)

		Dim cnnConexion As New SqlClient.SqlConnection
		Dim clsConexion As New Conexion
		Dim rstReader As System.Data.SqlClient.SqlDataReader
		Dim sql As String

		sql = "SELECT Simbolo FROM Moneda where CodMoneda =" & pIdMoneda
		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seepos", "CONEXION")
		cnnConexion.Open()

		rstReader = clsConexion.GetRecorset(cnnConexion, sql)

		If rstReader.Read() = False Then Exit Sub

		Me.lblMoneda1.Text = rstReader("Simbolo")
		Me.lblMoneda2.Text = rstReader("Simbolo")
		Me.lblMoneda3.Text = rstReader("Simbolo")
		Me.lblMoneda4.Text = rstReader("Simbolo")
		Me.lblMoneda5.Text = rstReader("Simbolo")
		Me.lblMoneda6.Text = rstReader("Simbolo")


		cnnConexion.Close()

	End Sub
#End Region

#Region "Funciones Validar"

	Private Function ValidarFactura() As Boolean
		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim rstReader As System.Data.SqlClient.SqlDataReader

		cnnConexion = clsConexion.Conectar("Seepos")

		sql = " SELECT COUNT(*) FROM Compras where factura = '" & Me.txtNumeroFactura.Text & "' AND CodigoProv=" & cmbProveedor.SelectedValue

		rstReader = clsConexion.GetRecorset(cnnConexion, sql)
		If rstReader.Read() = False Then Exit Function
		If rstReader(0) = 0 Then
			ValidarFactura = True
		Else
			ValidarFactura = False
		End If
		clsConexion.DesConectar(cnnConexion)
	End Function

	Private Function ValidarCabezera() As Boolean

		If Me.txtNumeroFactura.Text = "" Then
			MensajeError(txtNumeroFactura, "No se han completado los datos de la cabezera")
			Exit Function
		End If

		If Me.cmbTipo.SelectedIndex = -1 Then
			MensajeError(cmbTipo, "No se han completado los datos de la cabezera")
			Exit Function
		End If

		If Me.cmbProveedor.SelectedIndex = -1 Then
			MensajeError(cmbProveedor, "No se han completado los datos de la cabezera")
			Exit Function
		End If

		If Me.cmbMoneda.SelectedIndex = -1 Then
			MensajeError(cmbMoneda, "No se han completado los datos de la cabezera")
			Exit Function
		End If
		ValidarCabezera = True

	End Function

	Private Function ValidarDetalle() As Boolean

		If Me.txtDetalleCantidad.Text = "" Then
			MensajeError(txtDetalleCantidad, "No se han completado los datos del detalle")
			Exit Function
		End If


		If Me.txtDetalleArticuloDescripcion.Text = "" Then
			MensajeError(txtDetalleArticuloDescripcion, "No se han completado los datos del detalle")
			Exit Function
		End If

		If Me.txtDetallePrecioUnidad.Text = "" Then
			MensajeError(txtDetallePrecioUnidad, "No se han completado los datos del detalle")
			Exit Function
		End If

		If Me.txtDetalleDescuento.Text = "" Then
			MensajeError(txtDetalleDescuento, "No se han completado los datos del detalle")
			Exit Function
		End If

		If Me.txtImpuesto.Text = "" Then
			MensajeError(txtImpuesto, "No se han completado los datos del detalle")
			Exit Function
		End If

		If Me.txtCuentaContable.Text = "" Then
			MensajeError(txtCuentaContable, "No se han completado los datos del detalle")
			Exit Function
		End If

		ValidarDetalle = True
	End Function

	Private Sub MensajeError(ByVal pObjeto As Object, ByVal pMensaje As String)
		MsgBox(pMensaje, MsgBoxStyle.Information)
		pObjeto.focus()
	End Sub

	Private Function ValidarUsuario() As Boolean
		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim rsReader As SqlClient.SqlDataReader


		sql = "SELECT Nombre FROM USUARIOS WHERE Clave_interna= '" & txtClave.Text & "' AND ID_USUARIO ='" & Usua.Cedula & "'"

		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seguridad", "CONEXION")
		cnnConexion.Open()
		rsReader = clsConexion.GetRecorset(cnnConexion, sql)
		If rsReader.Read() = False Then Exit Function

		Me.TxtNombreUsuario.Text = rsReader(0)

		cnnConexion.Close()

		ValidarUsuario = True
	End Function

	Private Function ValidarModificarElimar() As Boolean
		Dim sql As String
		Dim clsConexion As New Conexion
		Dim cnnConexion As New System.Data.SqlClient.SqlConnection
		Dim rstReader As System.Data.SqlClient.SqlDataReader

		If IdGasto = -1 Then
			ValidarModificarElimar = True
			Exit Function
		End If
		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seepos", "CONEXION")
		cnnConexion.Open()
		sql = " select count(*) from compras where (Contabilizado = 1 or facturacancelado = 1  " &
		" ) AND ID_compra = " & IdGasto

		rstReader = clsConexion.GetRecorset(cnnConexion, sql)
		If rstReader.Read() = False Then Exit Function

		If rstReader(0) = 0 Then
			ValidarModificarElimar = True
		Else
			ValidarModificarElimar = False
		End If

	End Function


#End Region

#Region "Funciones Calculos"

	Private Sub CalcularTotales()
		Dim n As Integer = 0
		Dim subtotal, descuento, impuesto, MontoGasto, ImpuestoAplicable As Double

		For n = 0 To Me.dtsGasto.GastoDetalle.Count - 1
			With dtsGasto.GastoDetalle(n)
				impuesto = .Impuesto + impuesto
				descuento = .Descuento + descuento
				subtotal = (.Cantidad * .Costo) + subtotal
				MontoGasto = .MontoGasto + MontoGasto
				ImpuestoAplicable = .ImpuestoAplicable + ImpuestoAplicable

			End With
		Next
		Me.txtTotalMontoGasto.Text = Format(MontoGasto, "###,##0.00")
		Me.txtTotalImpuestoAplicable.Text = Format(ImpuestoAplicable, "###,##0.00")
		Me.txtTotalDescuento.Text = Format(descuento, "###,##0.00")
		Me.txtTotalImpuesto.Text = Format(impuesto, "###,##0.00")
		Me.txtDetalleSubTotal.Text = Format(subtotal, "###,##0.00")
		Me.txtTotal.Text = Format(subtotal - descuento + impuesto, "###,##0.00")
	End Sub

#End Region

#Region "Funciones Cargar"

	Private Sub ActivarCabezera()
		Me.txtNumeroFactura.Enabled = True
		Me.dtpFecha.Enabled = True
		Me.cmbTipo.Enabled = True
		Me.cmbProveedor.Enabled = True
		Me.cmbMoneda.Enabled = True
		txtTipoCambio.Enabled = True
	End Sub

	Private Sub DesactivarCabezera()
		Me.txtNumeroFactura.Enabled = False
		Me.dtpFecha.Enabled = False
		Me.cmbTipo.Enabled = False
		Me.cmbProveedor.Enabled = False
		Me.cmbMoneda.Enabled = False
		txtTipoCambio.Enabled = False
		DesactivarDetalle()
	End Sub

	Private Sub ActivarDetalle()
		Me.txtDetalleCantidad.Enabled = True
		Me.txtDetalleArticuloDescripcion.Enabled = True
		Me.txtDetallePrecioUnidad.Enabled = True
		Me.txtDetalleDescuento.Enabled = True
		Me.txtCuentaContable.Enabled = True
		Me.txtImpuesto.Enabled = True
	End Sub

	Private Sub DesactivarDetalle()
		Me.txtDetalleCantidad.Enabled = False
		Me.txtDetalleArticuloDescripcion.Enabled = False
		Me.txtDetallePrecioUnidad.Enabled = False
		Me.txtDetalleDescuento.Enabled = False
		Me.txtCuentaContable.Enabled = False
		Me.txtImpuesto.Enabled = False
	End Sub

	Private Sub LimpiarCabezera()
		IdGasto = -1
		Me.cmbProveedor.SelectedIndex = -1
		Me.cmbTipo.SelectedIndex = -1
		Me.cmbMoneda.SelectedIndex = -1
		Me.txtNumeroFactura.Text = ""
		Me.dtpFecha.Value = Date.Now
		Me.txtDetalleSubTotal.Text = "0"
		Me.txtTotal.Text = Format(0, "###,##0.00")
		Me.txtTotalDescuento.Text = Format(0, "###,##0.00")
		Me.txtTotalImpuesto.Text = Format(0, "###,##0.00")
		txtTotalImpuestoAplicable.Text = Format(0, "###,##0.00")
		txtTotalMontoGasto.Text = Format(0, "###,##0.00")
		Me.lblMoneda1.Text = ""
		Me.lblMoneda2.Text = ""
		Me.lblMoneda3.Text = ""
		Me.lblMoneda4.Text = ""
		Me.lblMoneda5.Text = ""
		Me.lblMoneda6.Text = ""
		LimpiarDetalle()
		'Datos ImportarXML------------------
		If clsImportarXml.esXML Then
			PanelImportarXML.Visible = False
			chConfirmada.Visible = False
			clsImportarXml.spLimpiar()
		End If
		chSimplificado.Checked = False
		'-----------------------------------
	End Sub

	Private Sub LimpiarDetalle()
		Me.txtDetalleCantidad.Text = "1"
		Me.txtDetalleArticuloDescripcion.Text = ""
		Me.txtDetallePrecioUnidad.Text = "0"
		Me.txtDetalleDescuento.Text = "0"
		Me.txtCuentaContable.Text = ""
		Me.txtDetalleArticuloDescripcion.Text = ""
		Me.txtCuentaContableDescripcion.Text = ""
		Me.txtImpuesto.Text = "13"
		Me.txtMontoGasto.Text = "0"
		Me.txtImpuestoAplicable.Text = "0"
		txtCodigoCabys.Text = ""
	End Sub

	Private Sub DesactivarToolBar()
		Me.tlbNuevo.Enabled = False
		Me.tlbImprimir.Enabled = False
		Me.tlbEliminar.Enabled = False
		Me.tlbBuscar.Enabled = False
	End Sub

	Private Sub CargarTxtDetalle()

		If Me.dtsGasto.GastoDetalle.Count = 0 Then Exit Sub
		IdDetalle = Me.BindingContext(dtsGasto.GastoDetalle).Position()

		With dtsGasto.GastoDetalle(IdDetalle)
			Me.txtCuentaContable.Text = .CuentaContable
			Me.txtCuentaContableDescripcion.Text = .CuentaContableDescripcion
			Me.txtDetalleCantidad.Text = .Descuento_P
			impuesto = .Impuesto_p
			Me.txtImpuesto.Text = .Impuesto_p
			Me.txtDetalleArticuloDescripcion.Text = .Descripcion

		End With
	End Sub

	Private Sub CargarGridDetalle(ByVal pIdGasto)
		Dim cnnConexion As New SqlClient.SqlConnection
		Dim adpAdapter As New SqlClient.SqlDataAdapter
		Dim sqlCommand As New System.Data.SqlClient.SqlCommand
		Dim sql As String
		Dim n As Integer


		sql = " SELECT A.Cantidad,A.IdCompra,A.Gravado, " &
" A.Exento,A.Descuento,A.Impuesto,A.Total,A.CuentaContable, " &
" A.Impuesto_p,A.NuevoCostoBase,A.Costo,A.Descuento_P ,A.Descripcion, " &
" c.Descripcion as CuentaContableDescripcion, a.ImpuestoAplicable, A.MontoGasto " &
" from Articulos_Gastos A, Contabilidad.dbo.cuentacontable C " &
" WHERE  " &
" C.cuentacontable  COLLATE Traditional_Spanish_CI_AS  = a.cuentacontable " &
" and idcompra = " & pIdGasto

		cnnConexion.ConnectionString = GetSetting("SeeSoft", "Seepos", "CONEXION")
		cnnConexion.Open()

		Me.dtsGasto.GastoDetalle.Clear()
		sqlCommand.Connection = cnnConexion
		sqlCommand.CommandText = sql
		adpAdapter.SelectCommand = sqlCommand
		adpAdapter.Fill(dtsGasto, "GastoDetalle")
	End Sub

#Region "FuncionesLLenar"

	Private Sub Cargar()
		Me.DesactivarCabezera()
		LimpiarCabezera()
	End Sub





#End Region

#End Region

	Private Sub CargarlblAsiento(ByVal _id As String)
		'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		'Esta funcion obtiene informacion del asiento relacionado a la compra
		'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Dim dt As New DataTable
		'selecciona el asiento relacionado a la compra y si esta mayorizado o no
		cFunciones.Llenar_Tabla_Generico("Select c.Asiento, isnull(a.Mayorizado,0) As Mayorizado From Seepos.dbo.Compras As c Left Join Contabilidad.dbo.Asientoscontables as a on c.Asiento collate Traditional_Spanish_CI_AS = a.Numasiento Where Id_Compra = " & _id, dt, GetSetting("SeeSOFT", "SeePOS", "Conexion"))
		If dt.Rows.Count > 0 Then
			'si exiten datos
			lblAsiento.Visible = True 'muestra la etiqueta.
			If dt.Rows(0).Item("Asiento") <> "0" Then
				lblAsiento.Text = dt.Rows(0).Item("Asiento") ' asigna el valor del asiento.
			Else
				lblAsiento.Text = ""
			End If
			If CBool(dt.Rows(0).Item("Mayorizado")) = True Then
				'si esta mayorizado cambia el color de la etiqueta del asinto a un rojo oscuro.
				Me.lblAsiento.ForeColor = System.Drawing.Color.DarkRed
			Else
				'si no esta mayorizado pone el color negro.
				Me.lblAsiento.ForeColor = System.Drawing.Color.Black
			End If
		Else
			'si no existen datos o no pudo obtenerlos pone valors por defecto.
			lblAsiento.Visible = False
			lblAsiento.Text = ""
		End If
		'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	End Sub

	Private Function fnValidar_Abono_Ajuste(ByVal _IdCompra As String) As Boolean
		Try
			Dim dt As New DataTable
			Dim sql As String

			sql = " select (select COUNT(*) from abonocpagar as a inner join detalle_abonocpagar as d " &
				  " on a.Id_Abonocpagar = d.Id_Abonocpagar where a.Anulado = 0 and a.Cod_Proveedor = '" & cmbProveedor.SelectedValue & "' and d.Id_Compra = '" & _IdCompra & "' ) + " &
				  " (select COUNT(*) from Ajustescpagar  as a inner join Detalle_AjustescPagar  as d " &
				  " on a.ID_Ajuste  = d.Id_AjustecPagar  where a.Anula = 0 and a.Cod_Proveedor = '" & cmbProveedor.SelectedValue & "' and d.Id_Compra = '" & _IdCompra & "') as Pago "

			cFunciones.Llenar_Tabla_Generico(sql, dt)
			If dt.Rows(0).Item(0) > 0 Then
				fnValidar_Abono_Ajuste = True
			Else
				fnValidar_Abono_Ajuste = False
			End If

		Catch ex As Exception
			MsgBox(ex.ToString)
			fnValidar_Abono_Ajuste = True
		End Try

	End Function

	Private Sub spCalcularProrrata()
		Try
			Dim Monto As Decimal = ((CDbl(txtDetallePrecioUnidad.Text) * CDbl(txtDetalleCantidad.Text)))
			Monto = Monto - (Monto * (CDbl(txtDetalleDescuento.Text) / 100))
			Dim Impuesto As Decimal = Monto * (CDbl(txtImpuesto.Text) / 100)
			If CDbl(txtImpuesto.Text) > 0 Then
				Dim clsutilidades As New clsUtilidades
				clsutilidades.spObtenerProrrata(Monto, Impuesto, dtpFecha.Value.Year.ToString())
				txtMontoGasto.Text = clsutilidades.MontoGasto
				txtImpuestoAplicable.Text = clsutilidades.ImpuestoAplicable

			Else
				txtMontoGasto.Text = Monto
				txtImpuestoAplicable.Text = 0
			End If

		Catch ex As Exception
			MessageBox.Show(ex.Message, "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Warning)
		End Try
	End Sub

	Private Sub btnContinuar_Click(sender As Object, e As EventArgs) Handles btnContinuar.Click

	End Sub

	Private Sub chSimplificado_CheckedChanged(sender As Object, e As EventArgs) Handles chSimplificado.CheckedChanged

		If chSimplificado.Checked Then
			ToolBarImportarXML.Enabled = False
			tlbRegistrar.Enabled = False
			ToolBarRegistrarFEC.Enabled = True
			lblCabys.Visible = True
			txtCodigoCabys.Visible = True
		Else
			ToolBarImportarXML.Enabled = True
			tlbRegistrar.Enabled = True
			ToolBarRegistrarFEC.Enabled = False
			lblCabys.Visible = False
			txtCodigoCabys.Visible = False
		End If

		cmbMoneda.Focus()

	End Sub

	Private Sub txtCodigoCabys_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCodigoCabys.KeyDown
		If e.KeyCode = Keys.F1 Then
			buscarCabys()
		End If

		If e.KeyCode = Keys.Enter Then
			If fnValidarCabys() Then
				spAgregarLinea()
			End If
		End If
	End Sub
	Private Function fnValidarCabys() As Boolean
		Try
			Dim sql As String = ""
			Dim dt As New DataTable

			sql = "Select count(*) from cabys where codigo = '" & txtCodigoCabys.Text & "'"
			cFunciones.Llenar_Tabla_Generico(sql, dt)

			If CDbl(dt.Rows(0).Item(0)) > 0 Then
				Return True
			Else
				MessageBox.Show("Por favor, ingrese un c�digo cabys valido.", "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Warning)
				Return False
			End If

		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Function

	Private Sub buscarCabys()
		Dim idCabys As Integer
		Dim buscarCabys As New FrmBuscarCabys

		buscarCabys.ShowDialog()
		idCabys = buscarCabys.idCabys
		Me.cargarCabys(idCabys)
	End Sub

	Private Sub cargarCabys(ByVal id As Integer)
		Dim dt As New DataTable
		cFunciones.Llenar_Tabla_Generico("select id,Descripcion,codigo from Cabys where id=" & id & "", dt, GetSetting("seSoft", "Seepos", "Conexion"))
		If dt.Rows.Count > 0 Then
			Me.txtCodigoCabys.Text = dt.Rows(0).Item("codigo")
		End If
	End Sub

	Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
		e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not e.KeyChar = "."
	End Sub
End Class
