Imports System.Windows.Forms
Imports System.Data


Public Class frmNavegacionCompras


    Dim dtFacturasCompras As New DataTable()
    Dim dtDetalleCompras As New DataTable()
    Dim Cargando As Boolean = True
    Dim dtFiltro As New DataTable()
    Dim Proceso As Boolean = True

    Private Sub frmNavegacionCompras_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.WindowState = FormWindowState.Maximized
        DTPDesde.Value = Now
        DTPHasta.Value = Now
        Cargando = False
        LbCargando.Visible = False
    End Sub


    Private Sub TraerDetallesCompras(ByVal IdCompra As Integer, ByVal Tipo As String)
        Try
            Dim Consulta As String = ""
            dtDetalleCompras.Columns.Clear()
            dtDetalleCompras.Clear()
            Select Case Tipo
                Case "COM"
                    Consulta = "select Fecha,barras as Codigo,Descripcion as Nombre,Cantidad,(Base + Monto_Flete + OtrosCargos) * (compras.TipoCambio / 1) as Valor,articulos_comprados.Descuento  * (compras.TipoCambio / 1) as Descuento,Total * (compras.TipoCambio / 1) as Costo,((Precio_A/Costo)-1)*100 as Utilidad,Precio_A * (compras.TipoCambio / 1) as Precio,Impuesto_P as Impuesto,((Precio_A*Impuesto_P)/100)+Precio_A * (compras.TipoCambio / 1) as PrecioFinal,Seguridad.dbo.Usuarios.Nombre as Usuario from articulos_comprados inner join compras on compras.Id_Compra= articulos_comprados.IdCompra ,Seguridad.dbo.Usuarios where Seguridad.dbo.Usuarios.Id_Usuario=CedulaUsuario and articulos_comprados.IdCompra =" & IdCompra & ""

                    cFunciones.TraerDatosDB(Consulta, dtDetalleCompras, GetSetting("SeeSoft", "SeePOS", "Conexion"))

                    dgvDetalleCompras.DataSource = dtDetalleCompras
                    dgvDetalleCompras.CellBorderStyle = DataGridViewCellBorderStyle.None
                    dgvDetalleCompras.Columns(0).DefaultCellStyle.Format = "dd/MM/yyyy"
                    dgvDetalleCompras.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(5).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(6).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(7).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(8).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(9).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(10).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(11).DefaultCellStyle.Format = "#,##0.00"

                    dgvDetalleCompras.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                    dgvDetalleCompras.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                    dgvDetalleCompras.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells


                Case "GASTOS"
                    Consulta = "select Fecha,0 as Codigo,Descripcion as Nombre,Cantidad,(Base + Monto_Flete + OtrosCargos) * (compras.TipoCambio / 1) as Valor,Articulos_Gastos.Descuento * (compras.TipoCambio / 1) as Descuento,Total + Articulos_Gastos.Impuesto * (compras.TipoCambio / 1) as Costo,0 as Utilidad,0 as Precio,Impuesto_P as Impuesto,0 as PrecioFinal,Seguridad.dbo.Usuarios.Nombre as Usuario from Articulos_Gastos inner join compras on compras.Id_Compra= Articulos_Gastos.IdCompra ,Seguridad.dbo.Usuarios where Seguridad.dbo.Usuarios.Id_Usuario=CedulaUsuario and Articulos_Gastos.IdCompra  =" & IdCompra & ""

                    cFunciones.TraerDatosDB(Consulta, dtDetalleCompras, GetSetting("SeeSoft", "SeePOS", "Conexion"))

                    dgvDetalleCompras.DataSource = dtDetalleCompras
                    dgvDetalleCompras.CellBorderStyle = DataGridViewCellBorderStyle.None
                    dgvDetalleCompras.Columns(0).DefaultCellStyle.Format = "dd/MM/yyyy"
                    dgvDetalleCompras.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(5).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(6).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(7).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(8).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(9).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(10).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(11).DefaultCellStyle.Format = "#,##0.00"

                    dgvDetalleCompras.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                    dgvDetalleCompras.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells

                Case "NC"
                    GoTo nada
                Case "ND"
                    GoTo nada
                Case "DEV"
                    Consulta = "select Codigo as Codigo,Descripcion as Nombre,Cantidad,SubTotal * (c.TipoCambio / 1) as SubTotal,d.Monto_Descuento * (c.TipoCambio / 1) as Descuento ,Monto_Impuesto * (c.TipoCambio / 1) as Impuesto,(SubTotal+Monto_Impuesto-d.Monto_Descuento) * (c.TipoCambio / 1) as Total from articulos_Compras_devueltos d inner join devoluciones_Compras as c on c.Devolucion = d.Devolucion   where c.Devolucion =" & IdCompra & ""

                    cFunciones.TraerDatosDB(Consulta, dtDetalleCompras, GetSetting("SeeSoft", "SeePOS", "Conexion"))

                    dgvDetalleCompras.DataSource = dtDetalleCompras
                    dgvDetalleCompras.CellBorderStyle = DataGridViewCellBorderStyle.None
                    dgvDetalleCompras.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(3).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(4).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(5).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(6).DefaultCellStyle.Format = "#,##0.00"

                    dgvDetalleCompras.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

                Case "ABO"
                    Consulta = "Select Fecha,Documento,Tipo,Moneda,Abono,Total,Saldo from vs_DetallesAbono where Id=" & IdCompra & ""

                    cFunciones.TraerDatosDB(Consulta, dtDetalleCompras, GetSetting("SeeSoft", "SeePOS", "Conexion"))

                    dgvDetalleCompras.DataSource = dtDetalleCompras
                    dgvDetalleCompras.CellBorderStyle = DataGridViewCellBorderStyle.None
                    dgvDetalleCompras.Columns(0).DefaultCellStyle.Format = "dd/MM/yyyy"
                    dgvDetalleCompras.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalleCompras.Columns(4).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(5).DefaultCellStyle.Format = "#,##0.00"
                    dgvDetalleCompras.Columns(6).DefaultCellStyle.Format = "#,##0.00"

                    dgvDetalleCompras.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            End Select


nada:
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtCodProveedor_KeyUp(sender As Object, e As KeyEventArgs) Handles txtCodProveedor.KeyUp
        LlamarHilo()
    End Sub

    Private Sub txtNumFactura_KeyUp(sender As Object, e As KeyEventArgs) Handles txtNumFactura.KeyUp
        LlamarHilo()
    End Sub

    Private Sub txtNombreProveedor_KeyDown(sender As Object, e As KeyEventArgs) Handles txtNombreProveedor.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.F1
                    Dim cFunciones As New cFunciones
                    Dim c As String
                    c = cFunciones.BuscarDatos("select CodigoProv as Codigo,Nombre as Proveedor from Proveedores ", "Nombre", "Nombre")

                    If c <> "" Then
                        Dim dtNombreProveedor As New DataTable
                        cFunciones.Llenar_Tabla_Generico("Select Proveedores.Nombre from compras inner join Proveedores on compras.CodigoProv = Proveedores.CodigoProv where Sucursal = " & Sucursal & " and Compras.CodigoProv =" & c & " Order by Fecha DESC", dtNombreProveedor, GetSetting("SeeSoft", "SeePOS", "Conexion"))

                        Me.txtNombreProveedor.Text = dtNombreProveedor.Rows(0).Item("Nombre")
                        Me.txtCodProveedor.Text = c
                        LlamarHilo() 'FiltroBusqueda(txtCodProveedor.Text, txtNombreProveedor.Text, txtNumFactura.Text)
                    Else
                        Exit Sub
                    End If
            End Select
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Sub

    Private Sub DTPDesde_ValueChanged(sender As Object, e As EventArgs) Handles DTPDesde.ValueChanged
        LlamarHilo()
    End Sub

    Private Sub DTPHasta_ValueChanged(sender As Object, e As EventArgs) Handles DTPHasta.ValueChanged
        LlamarHilo()
    End Sub

    Sub LlamarHilo()
        If Cargando = False Then
            If Proceso = True Then
                LbCargando.Visible = True
                FiltroBusqueda(txtCodProveedor.Text, txtNombreProveedor.Text, txtNumFactura.Text)
            Else
                LbCargando.Visible = True
            End If

        End If
    End Sub

    Public Sub FiltroBusqueda(CodProveedor As String, NombreProveedor As String, NumFactura As String)

        Try

            If Cargando = False Then
                Proceso = False
                Dim Consulta As String = "Select Id_Compra,  Documento, Proveedor, Cod, Tipo, Subtotal,Descuento,Impuesto,Total,Saldo, Fecha, Moneda from vs_NavegadorCxP where  dbo.dateonly(Fecha) between dbo.dateonly('" & Convert.ToDateTime(DTPDesde.Text) & "') and dbo.dateonly('" & Convert.ToDateTime(DTPHasta.Text) & "') #Adjunto"
            Dim Adjunto As String = ""


                If ckFacturas.Checked Then
                    Adjunto += " Tipo='COM' or Tipo='Gastos' or"
                End If

                If ckDevoluciones.Checked Then
                    Adjunto += " Tipo='DEV' or"
                End If

                If ckAbonos.Checked Then
                    Adjunto += " Tipo='ABO' or"
                End If

                If ckNC.Checked Then
                    Adjunto += " Tipo='NC' or"
                End If

                If ckND.Checked Then
                    Adjunto += " Tipo='ND' or"
                End If

                If ckFacturas.Checked Or ckDevoluciones.Checked Or ckAbonos.Checked Or ckNC.Checked Or ckND.Checked Then
                    Adjunto = Adjunto.Remove(Adjunto.Length - 2)
                    Adjunto = " and ( " & Adjunto & ")"
                End If

                If CodProveedor.Length >= 1 Then
                    Adjunto += " and Cod=" & Convert.ToInt32(txtCodProveedor.Text) & ""
                End If

                If NombreProveedor.Trim() <> "" Then
                    Adjunto += " and Proveedor like '%" & txtNombreProveedor.Text & "%'"
                End If

                If NumFactura.Trim() <> "" Then
					Adjunto += " and Documento  like '%" & txtNumFactura.Text & "%'"
				End If

                If ckAZ.Checked = True Or ckZA.Checked = True Then
                    If ckAZ.Checked = True Then
                        Adjunto += " order by Proveedor asc"
                    End If

                    If ckZA.Checked = True Then
                        Adjunto += " order by Proveedor desc"
                    End If
                Else
                    Adjunto += " order by Fecha desc"
                End If

                Consulta = Consulta.Replace("#Adjunto", Adjunto)
                dtFiltro.Clear()
                cFunciones.TraerDatosDB(Consulta, dtFiltro, GetSetting("SeeSoft", "SeePOS", "Conexion"))

                Me.dgvFacturasCompras.DataSource = dtFiltro
                    Me.dgvFacturasCompras.CellBorderStyle = DataGridViewCellBorderStyle.None
                Me.dgvFacturasCompras.Columns(0).Visible = False
                Me.dgvFacturasCompras.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Me.dgvFacturasCompras.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Me.dgvFacturasCompras.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Me.dgvFacturasCompras.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Me.dgvFacturasCompras.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Me.dgvFacturasCompras.Columns(5).DefaultCellStyle.Format = "#,##0.00"
                    Me.dgvFacturasCompras.Columns(6).DefaultCellStyle.Format = "#,##0.00"
                    Me.dgvFacturasCompras.Columns(7).DefaultCellStyle.Format = "#,##0.00"
                    Me.dgvFacturasCompras.Columns(8).DefaultCellStyle.Format = "#,##0.00"
                    Me.dgvFacturasCompras.Columns(9).DefaultCellStyle.Format = "#,##0.00"
                    Me.dgvFacturasCompras.Columns(10).DefaultCellStyle.Format = "dd/MM/yyyy"
                    Me.dgvFacturasCompras.Columns("Proveedor").SortMode = DataGridViewColumnSortMode.Automatic
                Me.dgvFacturasCompras.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                Me.dgvFacturasCompras.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                Me.dgvFacturasCompras.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                Me.dgvFacturasCompras.Columns(10).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                Proceso = True
                LbCargando.Visible = False
            End If
        Catch ex As Exception
            MsgBox("Error al aplicar filtros. " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub dgvFacturasCompras_SelectionChanged(sender As Object, e As EventArgs) Handles dgvFacturasCompras.SelectionChanged
        Try
            If dgvFacturasCompras.SelectedCells.Count <> 0 Then
                Dim CodArt As Integer = dgvFacturasCompras.SelectedCells(0).Value
                Dim Tipo As String = dgvFacturasCompras.SelectedCells(4).Value
                TraerDetallesCompras(CodArt, Tipo)
            Else
                dtDetalleCompras.Columns.Clear()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ckZA_CheckedChanged(sender As Object, e As EventArgs) Handles ckZA.CheckedChanged
        If ckZA.Checked = True Then
            ckAZ.Checked = False
            LlamarHilo()
        End If
    End Sub

    Private Sub ckAZ_CheckedChanged(sender As Object, e As EventArgs) Handles ckAZ.CheckedChanged
        If ckAZ.Checked = True Then
            ckZA.Checked = False
            LlamarHilo()
        End If
    End Sub

    Private Sub txtNombreProveedor_KeyUp(sender As Object, e As KeyEventArgs) Handles txtNombreProveedor.KeyUp
        LlamarHilo()
    End Sub

    Private Sub DTPDesde_KeyDown(sender As Object, e As KeyEventArgs) Handles DTPDesde.KeyDown
        If e.KeyCode = Keys.Enter Then
            DTPHasta.Focus()
        End If
    End Sub

    Private Sub ckFacturas_CheckedChanged(sender As Object, e As EventArgs) Handles ckFacturas.CheckedChanged
        LlamarHilo()
    End Sub

    Private Sub ckDevoluciones_CheckedChanged(sender As Object, e As EventArgs) Handles ckDevoluciones.CheckedChanged
        LlamarHilo()
    End Sub

    Private Sub ckAbonos_CheckedChanged(sender As Object, e As EventArgs) Handles ckAbonos.CheckedChanged
        LlamarHilo()
    End Sub

    Private Sub ckNC_CheckedChanged(sender As Object, e As EventArgs) Handles ckNC.CheckedChanged
        LlamarHilo()
    End Sub

    Private Sub ckND_CheckedChanged(sender As Object, e As EventArgs) Handles ckND.CheckedChanged
        LlamarHilo()
    End Sub


    Private Sub dgvFacturasCompras_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvFacturasCompras.DataError

    End Sub

    Private Sub dgvDetalleCompras_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvDetalleCompras.DataError

    End Sub
End Class