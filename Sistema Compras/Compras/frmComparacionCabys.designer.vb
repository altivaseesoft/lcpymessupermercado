﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComparacionCabys
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
		Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
		Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
		Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.txtDescripcionCabysXml = New System.Windows.Forms.TextBox()
		Me.txtCodigoCabysXml = New System.Windows.Forms.TextBox()
		Me.txtCodigoComercialXml = New System.Windows.Forms.TextBox()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.txtDescripcionSubFamilia = New System.Windows.Forms.TextBox()
		Me.txtDescripcionCabysInv = New System.Windows.Forms.TextBox()
		Me.txtCodigoCabysInv = New System.Windows.Forms.TextBox()
		Me.txtCodigoComercialInv = New System.Windows.Forms.TextBox()
		Me.GroupBox3 = New System.Windows.Forms.GroupBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.dgvSubFamilias = New System.Windows.Forms.DataGridView()
		Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.CodigoSubFamilia = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.btnNuevaSubfamilia = New System.Windows.Forms.Button()
		Me.btnCambiarSubFamilia = New System.Windows.Forms.Button()
		Me.btnOmitir = New System.Windows.Forms.Button()
		Me.btnCambiarCabysASubFamilia = New System.Windows.Forms.Button()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.txtPorcentajeIVA = New System.Windows.Forms.TextBox()
		Me.GroupBox1.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.GroupBox3.SuspendLayout()
		CType(Me.dgvSubFamilias, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.Label3)
		Me.GroupBox1.Controls.Add(Me.Label2)
		Me.GroupBox1.Controls.Add(Me.Label19)
		Me.GroupBox1.Controls.Add(Me.Label14)
		Me.GroupBox1.Controls.Add(Me.txtDescripcionCabysXml)
		Me.GroupBox1.Controls.Add(Me.txtCodigoCabysXml)
		Me.GroupBox1.Controls.Add(Me.txtCodigoComercialXml)
		Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.GroupBox1.Location = New System.Drawing.Point(13, 13)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(330, 232)
		Me.GroupBox1.TabIndex = 0
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Información línea en XML del Proveedor"
		'
		'Label3
		'
		Me.Label3.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.ForeColor = System.Drawing.Color.White
		Me.Label3.Location = New System.Drawing.Point(6, 144)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(314, 14)
		Me.Label3.TabIndex = 9
		Me.Label3.Text = "Descripción Cabys"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label2
		'
		Me.Label2.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.ForeColor = System.Drawing.Color.White
		Me.Label2.Location = New System.Drawing.Point(6, 85)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(252, 15)
		Me.Label2.TabIndex = 8
		Me.Label2.Text = "Código Cabys"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label19
		'
		Me.Label19.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label19.ForeColor = System.Drawing.Color.White
		Me.Label19.Location = New System.Drawing.Point(6, 33)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(252, 15)
		Me.Label19.TabIndex = 7
		Me.Label19.Text = "Código Comercial"
		Me.Label19.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label14
		'
		Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
		Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label14.ForeColor = System.Drawing.Color.White
		Me.Label14.Location = New System.Drawing.Point(-1, 0)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(331, 20)
		Me.Label14.TabIndex = 6
		Me.Label14.Text = "Información línea en XML del Proveedor"
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtDescripcionCabysXml
		'
		Me.txtDescripcionCabysXml.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtDescripcionCabysXml.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDescripcionCabysXml.Location = New System.Drawing.Point(6, 161)
		Me.txtDescripcionCabysXml.Name = "txtDescripcionCabysXml"
		Me.txtDescripcionCabysXml.ReadOnly = True
		Me.txtDescripcionCabysXml.Size = New System.Drawing.Size(314, 21)
		Me.txtDescripcionCabysXml.TabIndex = 6
		'
		'txtCodigoCabysXml
		'
		Me.txtCodigoCabysXml.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtCodigoCabysXml.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCodigoCabysXml.Location = New System.Drawing.Point(6, 102)
		Me.txtCodigoCabysXml.Name = "txtCodigoCabysXml"
		Me.txtCodigoCabysXml.ReadOnly = True
		Me.txtCodigoCabysXml.Size = New System.Drawing.Size(252, 21)
		Me.txtCodigoCabysXml.TabIndex = 4
		'
		'txtCodigoComercialXml
		'
		Me.txtCodigoComercialXml.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtCodigoComercialXml.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCodigoComercialXml.Location = New System.Drawing.Point(6, 50)
		Me.txtCodigoComercialXml.Name = "txtCodigoComercialXml"
		Me.txtCodigoComercialXml.ReadOnly = True
		Me.txtCodigoComercialXml.Size = New System.Drawing.Size(252, 21)
		Me.txtCodigoComercialXml.TabIndex = 2
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.Label7)
		Me.GroupBox2.Controls.Add(Me.Label6)
		Me.GroupBox2.Controls.Add(Me.Label1)
		Me.GroupBox2.Controls.Add(Me.txtPorcentajeIVA)
		Me.GroupBox2.Controls.Add(Me.Label5)
		Me.GroupBox2.Controls.Add(Me.Label4)
		Me.GroupBox2.Controls.Add(Me.Label8)
		Me.GroupBox2.Controls.Add(Me.txtDescripcionSubFamilia)
		Me.GroupBox2.Controls.Add(Me.btnCambiarCabysASubFamilia)
		Me.GroupBox2.Controls.Add(Me.txtDescripcionCabysInv)
		Me.GroupBox2.Controls.Add(Me.txtCodigoCabysInv)
		Me.GroupBox2.Controls.Add(Me.txtCodigoComercialInv)
		Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.GroupBox2.Location = New System.Drawing.Point(349, 13)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(332, 325)
		Me.GroupBox2.TabIndex = 1
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "Información línea en Inventario"
		'
		'Label6
		'
		Me.Label6.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label6.ForeColor = System.Drawing.Color.White
		Me.Label6.Location = New System.Drawing.Point(6, 193)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(314, 15)
		Me.Label6.TabIndex = 13
		Me.Label6.Text = "SubFamilias"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label1
		'
		Me.Label1.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.White
		Me.Label1.Location = New System.Drawing.Point(6, 33)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(252, 15)
		Me.Label1.TabIndex = 12
		Me.Label1.Text = "Código Interno"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label5
		'
		Me.Label5.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label5.ForeColor = System.Drawing.Color.White
		Me.Label5.Location = New System.Drawing.Point(6, 144)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(314, 14)
		Me.Label5.TabIndex = 11
		Me.Label5.Text = "Descripción Cabys"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label4
		'
		Me.Label4.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.ForeColor = System.Drawing.Color.White
		Me.Label4.Location = New System.Drawing.Point(6, 85)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(252, 15)
		Me.Label4.TabIndex = 10
		Me.Label4.Text = "Código Cabys"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label8
		'
		Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
		Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label8.ForeColor = System.Drawing.Color.White
		Me.Label8.Location = New System.Drawing.Point(1, 0)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(331, 20)
		Me.Label8.TabIndex = 7
		Me.Label8.Text = "Información línea en Inventario"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtDescripcionSubFamilia
		'
		Me.txtDescripcionSubFamilia.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtDescripcionSubFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDescripcionSubFamilia.Location = New System.Drawing.Point(6, 211)
		Me.txtDescripcionSubFamilia.Name = "txtDescripcionSubFamilia"
		Me.txtDescripcionSubFamilia.ReadOnly = True
		Me.txtDescripcionSubFamilia.Size = New System.Drawing.Size(314, 21)
		Me.txtDescripcionSubFamilia.TabIndex = 9
		'
		'txtDescripcionCabysInv
		'
		Me.txtDescripcionCabysInv.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtDescripcionCabysInv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDescripcionCabysInv.Location = New System.Drawing.Point(6, 161)
		Me.txtDescripcionCabysInv.Name = "txtDescripcionCabysInv"
		Me.txtDescripcionCabysInv.ReadOnly = True
		Me.txtDescripcionCabysInv.Size = New System.Drawing.Size(314, 21)
		Me.txtDescripcionCabysInv.TabIndex = 7
		'
		'txtCodigoCabysInv
		'
		Me.txtCodigoCabysInv.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtCodigoCabysInv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCodigoCabysInv.Location = New System.Drawing.Point(6, 102)
		Me.txtCodigoCabysInv.Name = "txtCodigoCabysInv"
		Me.txtCodigoCabysInv.ReadOnly = True
		Me.txtCodigoCabysInv.Size = New System.Drawing.Size(252, 21)
		Me.txtCodigoCabysInv.TabIndex = 3
		'
		'txtCodigoComercialInv
		'
		Me.txtCodigoComercialInv.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtCodigoComercialInv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCodigoComercialInv.Location = New System.Drawing.Point(6, 50)
		Me.txtCodigoComercialInv.Name = "txtCodigoComercialInv"
		Me.txtCodigoComercialInv.ReadOnly = True
		Me.txtCodigoComercialInv.Size = New System.Drawing.Size(252, 21)
		Me.txtCodigoComercialInv.TabIndex = 1
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.btnNuevaSubfamilia)
		Me.GroupBox3.Controls.Add(Me.Label9)
		Me.GroupBox3.Controls.Add(Me.dgvSubFamilias)
		Me.GroupBox3.Controls.Add(Me.btnCambiarSubFamilia)
		Me.GroupBox3.Location = New System.Drawing.Point(687, 13)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Size = New System.Drawing.Size(319, 325)
		Me.GroupBox3.TabIndex = 5
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "SubFamilias"
		'
		'Label9
		'
		Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
		Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label9.ForeColor = System.Drawing.Color.White
		Me.Label9.Location = New System.Drawing.Point(0, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(319, 20)
		Me.Label9.TabIndex = 7
		Me.Label9.Text = "SubFamilias Disponibles con Cabys del Proveedor "
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'dgvSubFamilias
		'
		Me.dgvSubFamilias.AllowUserToAddRows = False
		Me.dgvSubFamilias.AllowUserToDeleteRows = False
		Me.dgvSubFamilias.AllowUserToResizeColumns = False
		Me.dgvSubFamilias.AllowUserToResizeRows = False
		Me.dgvSubFamilias.BackgroundColor = System.Drawing.Color.White
		DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
		DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.AppWorkspace
		DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
		DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
		DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
		DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
		Me.dgvSubFamilias.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
		Me.dgvSubFamilias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.dgvSubFamilias.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.CodigoSubFamilia})
		DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
		DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
		DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
		DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
		DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
		DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
		Me.dgvSubFamilias.DefaultCellStyle = DataGridViewCellStyle3
		Me.dgvSubFamilias.Location = New System.Drawing.Point(6, 23)
		Me.dgvSubFamilias.MultiSelect = False
		Me.dgvSubFamilias.Name = "dgvSubFamilias"
		Me.dgvSubFamilias.ReadOnly = True
		DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
		DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
		DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
		DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
		DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
		DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
		Me.dgvSubFamilias.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
		Me.dgvSubFamilias.RowHeadersVisible = False
		Me.dgvSubFamilias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
		Me.dgvSubFamilias.Size = New System.Drawing.Size(307, 209)
		Me.dgvSubFamilias.TabIndex = 0
		'
		'Descripcion
		'
		Me.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
		Me.Descripcion.DataPropertyName = "Descripcion"
		DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
		Me.Descripcion.DefaultCellStyle = DataGridViewCellStyle2
		Me.Descripcion.HeaderText = "Descripción"
		Me.Descripcion.Name = "Descripcion"
		Me.Descripcion.ReadOnly = True
		Me.Descripcion.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
		Me.Descripcion.Width = 303
		'
		'CodigoSubFamilia
		'
		Me.CodigoSubFamilia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
		Me.CodigoSubFamilia.DataPropertyName = "CodigoSubFamilia"
		Me.CodigoSubFamilia.HeaderText = "CodigoSubFamilia"
		Me.CodigoSubFamilia.Name = "CodigoSubFamilia"
		Me.CodigoSubFamilia.ReadOnly = True
		Me.CodigoSubFamilia.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
		'
		'Label10
		'
		Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
		Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label10.ForeColor = System.Drawing.Color.White
		Me.Label10.Location = New System.Drawing.Point(-1, -10)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(1022, 20)
		Me.Label10.TabIndex = 7
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'btnNuevaSubfamilia
		'
		Me.btnNuevaSubfamilia.BackColor = System.Drawing.SystemColors.ControlDark
		Me.btnNuevaSubfamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnNuevaSubfamilia.ForeColor = System.Drawing.SystemColors.ButtonHighlight
		Me.btnNuevaSubfamilia.Image = Global.LcPymes_5._2.My.Resources.Resources.Guardar_sincolor
		Me.btnNuevaSubfamilia.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.btnNuevaSubfamilia.Location = New System.Drawing.Point(6, 251)
		Me.btnNuevaSubfamilia.Name = "btnNuevaSubfamilia"
		Me.btnNuevaSubfamilia.Size = New System.Drawing.Size(146, 55)
		Me.btnNuevaSubfamilia.TabIndex = 8
		Me.btnNuevaSubfamilia.Text = "Nueva    SubFamilia"
		Me.btnNuevaSubfamilia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.btnNuevaSubfamilia.UseVisualStyleBackColor = False
		'
		'btnCambiarSubFamilia
		'
		Me.btnCambiarSubFamilia.BackColor = System.Drawing.SystemColors.ControlDark
		Me.btnCambiarSubFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnCambiarSubFamilia.ForeColor = System.Drawing.SystemColors.ButtonHighlight
		Me.btnCambiarSubFamilia.Image = Global.LcPymes_5._2.My.Resources.Resources.Guardar_sincolor
		Me.btnCambiarSubFamilia.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.btnCambiarSubFamilia.Location = New System.Drawing.Point(167, 251)
		Me.btnCambiarSubFamilia.Name = "btnCambiarSubFamilia"
		Me.btnCambiarSubFamilia.Size = New System.Drawing.Size(146, 55)
		Me.btnCambiarSubFamilia.TabIndex = 2
		Me.btnCambiarSubFamilia.Text = "Cambiar SubFamilia"
		Me.btnCambiarSubFamilia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.btnCambiarSubFamilia.UseVisualStyleBackColor = False
		'
		'btnOmitir
		'
		Me.btnOmitir.BackColor = System.Drawing.Color.Coral
		Me.btnOmitir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
		Me.btnOmitir.Cursor = System.Windows.Forms.Cursors.Default
		Me.btnOmitir.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnOmitir.ForeColor = System.Drawing.SystemColors.ButtonHighlight
		Me.btnOmitir.Image = Global.LcPymes_5._2.My.Resources.Resources.Cerrar_sincolor
		Me.btnOmitir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.btnOmitir.Location = New System.Drawing.Point(92, 264)
		Me.btnOmitir.Name = "btnOmitir"
		Me.btnOmitir.Size = New System.Drawing.Size(156, 55)
		Me.btnOmitir.TabIndex = 4
		Me.btnOmitir.Text = "Omitir / Cerrar"
		Me.btnOmitir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.btnOmitir.UseVisualStyleBackColor = False
		'
		'btnCambiarCabysASubFamilia
		'
		Me.btnCambiarCabysASubFamilia.BackColor = System.Drawing.SystemColors.ControlDark
		Me.btnCambiarCabysASubFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnCambiarCabysASubFamilia.ForeColor = System.Drawing.SystemColors.ButtonHighlight
		Me.btnCambiarCabysASubFamilia.Image = Global.LcPymes_5._2.My.Resources.Resources.Guardar_sincolor
		Me.btnCambiarCabysASubFamilia.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.btnCambiarCabysASubFamilia.Location = New System.Drawing.Point(71, 251)
		Me.btnCambiarCabysASubFamilia.Name = "btnCambiarCabysASubFamilia"
		Me.btnCambiarCabysASubFamilia.Size = New System.Drawing.Size(176, 55)
		Me.btnCambiarCabysASubFamilia.TabIndex = 3
		Me.btnCambiarCabysASubFamilia.Text = "Cambiar Cabys a            SubFamilia"
		Me.btnCambiarCabysASubFamilia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.btnCambiarCabysASubFamilia.UseVisualStyleBackColor = False
		'
		'Label7
		'
		Me.Label7.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label7.ForeColor = System.Drawing.Color.White
		Me.Label7.Location = New System.Drawing.Point(264, 85)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(56, 15)
		Me.Label7.TabIndex = 15
		Me.Label7.Text = "%"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'txtPorcentajeIVA
		'
		Me.txtPorcentajeIVA.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtPorcentajeIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPorcentajeIVA.Location = New System.Drawing.Point(264, 102)
		Me.txtPorcentajeIVA.Name = "txtPorcentajeIVA"
		Me.txtPorcentajeIVA.ReadOnly = True
		Me.txtPorcentajeIVA.Size = New System.Drawing.Size(56, 21)
		Me.txtPorcentajeIVA.TabIndex = 14
		Me.txtPorcentajeIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		'
		'frmComparacionCabys
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1018, 349)
		Me.ControlBox = False
		Me.Controls.Add(Me.Label10)
		Me.Controls.Add(Me.GroupBox3)
		Me.Controls.Add(Me.btnOmitir)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.GroupBox1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Name = "frmComparacionCabys"
		Me.ShowIcon = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Comparar Cabys"
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox2.PerformLayout()
		Me.GroupBox3.ResumeLayout(False)
		CType(Me.dgvSubFamilias, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents txtDescripcionCabysXml As Windows.Forms.TextBox
    Friend WithEvents txtCodigoCabysXml As Windows.Forms.TextBox
    Friend WithEvents txtCodigoComercialXml As Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As Windows.Forms.GroupBox
    Friend WithEvents txtDescripcionSubFamilia As Windows.Forms.TextBox
    Friend WithEvents btnCambiarCabysASubFamilia As Windows.Forms.Button
    Friend WithEvents btnCambiarSubFamilia As Windows.Forms.Button
    Friend WithEvents txtDescripcionCabysInv As Windows.Forms.TextBox
    Friend WithEvents txtCodigoCabysInv As Windows.Forms.TextBox
    Friend WithEvents txtCodigoComercialInv As Windows.Forms.TextBox
    Friend WithEvents btnOmitir As Windows.Forms.Button
    Friend WithEvents GroupBox3 As Windows.Forms.GroupBox
    Friend WithEvents dgvSubFamilias As Windows.Forms.DataGridView
    Friend WithEvents Label14 As Windows.Forms.Label
    Friend WithEvents Label8 As Windows.Forms.Label
    Friend WithEvents Label9 As Windows.Forms.Label
    Friend WithEvents Label10 As Windows.Forms.Label
    Friend WithEvents Label3 As Windows.Forms.Label
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents Label19 As Windows.Forms.Label
    Friend WithEvents Label6 As Windows.Forms.Label
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents Label5 As Windows.Forms.Label
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents Descripcion As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodigoSubFamilia As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnNuevaSubfamilia As Windows.Forms.Button
    Friend WithEvents Label7 As Windows.Forms.Label
    Friend WithEvents txtPorcentajeIVA As Windows.Forms.TextBox
End Class
