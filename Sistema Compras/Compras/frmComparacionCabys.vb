﻿Imports System.Data

Public Class frmComparacionCabys
    Public codigoComercial As String
    Public codigoCabys As String
    Public codigoBarras As String
    Dim usuario As Usuario_Logeado

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()

        Usuario = Usuario_Parametro

    End Sub
    Private Sub frmComparacionCabys_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        spIniciarForm()
    End Sub

    Private Sub spIniciarForm()
        llenarInformacionXml()
        llenarInformacionInv()
        llenarDataGridSubFamilias()
    End Sub

    Private Sub llenarInformacionXml()
        Dim dt As New DataTable
        Me.txtCodigoComercialXml.Text = codigoComercial
        Me.txtCodigoCabysXml.Text = codigoCabys
        cFunciones.Llenar_Tabla_Generico("SELECT Descripcion FROM Cabys WHERE codigo= '" & codigoCabys & "'", dt)
        If dt.Rows.Count > 0 Then
            Me.txtDescripcionCabysXml.Text = dt.Rows(0).Item("Descripcion")
        Else
            Me.Close()
        End If
    End Sub

    Private Sub llenarInformacionInv()
        Dim dt As New DataTable
        Me.txtCodigoComercialInv.Text = codigoBarras
        cFunciones.Llenar_Tabla_Generico("select I.Barras, I.Descripcion,C.codigo as CabysCodigo, C.Descripcion as CabysDescripcion, (F.Descripcion+' / '+SF.Descripcion) as SFDescripcion, c.porcentajeIVA
            from Inventario I join SubFamilias SF
            on I.SubFamilia=SF.Codigo
            join Familia F on SF.CodigoFamilia=F.Codigo
            join Cabys C on SF.idCabys=C.id where I.Barras = '" & codigoBarras & "'", dt)
        If dt.Rows.Count > 0 Then
            Me.txtCodigoCabysInv.Text = dt.Rows(0).Item("CabysCodigo")
            Me.txtDescripcionCabysInv.Text = dt.Rows(0).Item("CabysDescripcion")
            Me.txtDescripcionSubFamilia.Text = dt.Rows(0).Item("SFDescripcion")
            Me.Text = dt.Rows(0).Item("Descripcion")
            txtPorcentajeIVA.Text = dt.Rows(0).Item("porcentajeIVA")
        End If
    End Sub

    Private Sub llenarDataGridSubFamilias()
        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("select (F.Descripcion+'/'+SF.Descripcion) as Descripcion, 
            SF.Codigo as CodigoSubFamilia from SubFamilias SF
            join Familia F on SF.CodigoFamilia=F.Codigo
            join Cabys C on SF.idCabys= C.id where C.codigo='" & codigoCabys & "'", dt)
        dgvSubFamilias.DataSource = dt.DefaultView
        dgvSubFamilias.Columns(1).Visible = False
        btnCambiarSubFamilia.Enabled = False
    End Sub

    Private Sub cambioCabysASubFamilia()
        Dim cabysId As New Integer
        Dim dtCodCabys As New DataTable
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        cFunciones.Llenar_Tabla_Generico("select id from Cabys where codigo='" & codigoCabys & "'", dtCodCabys)
        If dtCodCabys.Rows.Count > 0 Then
            cabysId = dtCodCabys.Rows(0).Item("id")
        End If
        cFunciones.Llenar_Tabla_Generico("update SF set idCabys=" & cabysId & "
            from SubFamilias SF 
            join Cabys C on SF.idCabys=C.id
            join Inventario I on SF.Codigo=I.SubFamilia
            where I.Barras = '" & codigoBarras & "'", dt)
        cFunciones.Llenar_Tabla_Generico("select (F.Descripcion+'/'+SF.Descripcion) as SFDescripcion,
            C.Descripcion as DescripcionC, C.codigo as CodigoC
            from Inventario I join SubFamilias SF
            on I.SubFamilia=SF.Codigo
            join Familia F on SF.CodigoFamilia=F.Codigo
            join Cabys C on SF.idCabys=C.id where I.Barras = '" & codigoBarras & "'", dt2)
        If dt2.Rows.Count > 0 Then
                Me.txtDescripcionSubFamilia.Text = dt2.Rows(0).Item("SFDescripcion")
                Me.txtDescripcionCabysInv.Text = dt2.Rows(0).Item("DescripcionC")
                Me.txtCodigoCabysInv.Text = dt2.Rows(0).Item("CodigoC")
                'End If
            End If
    End Sub

    Private Sub CambiarSubFamilia()
        Dim subFamiliaCodigo As String
        Dim dtSubFamilia As New DataTable
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        cFunciones.Llenar_Tabla_Generico("select Codigo as CodigoSubFamilia,Descripcion from SubFamilias where Codigo='" & dgvSubFamilias.CurrentRow.Cells(1).Value.ToString & "'", dtSubFamilia)
        If dtSubFamilia.Rows.Count > 0 Then
            subFamiliaCodigo = dtSubFamilia.Rows(0).Item("CodigoSubFamilia")
        End If
        cFunciones.Llenar_Tabla_Generico("update I set I.SubFamilia='" & subFamiliaCodigo & "'
            from Inventario I
            where I.Barras = '" & codigoBarras & "'", dt)
        cFunciones.Llenar_Tabla_Generico("select (F.Descripcion+'/'+SF.Descripcion) as SFDescripcion,
            C.Descripcion as DescripcionC, C.codigo as CodigoC
            from Inventario I join SubFamilias SF
            on I.SubFamilia=SF.Codigo
            join Familia F on SF.CodigoFamilia=F.Codigo
            join Cabys C on SF.idCabys=C.id where I.Barras = '" & codigoBarras & "'", dt2)
        If dt2.Rows.Count > 0 Then
            Me.txtDescripcionSubFamilia.Text = dt2.Rows(0).Item("SFDescripcion")
            Me.txtDescripcionCabysInv.Text = dt2.Rows(0).Item("DescripcionC")
            Me.txtCodigoCabysInv.Text = dt2.Rows(0).Item("CodigoC")
            'End If
        End If
    End Sub

    Private Sub btnOmitir_Click(sender As Object, e As EventArgs) Handles btnOmitir.Click
        Me.Close()
    End Sub

    Private Sub btnCambiarCabysASubFamilia_Click(sender As Object, e As EventArgs) Handles btnCambiarCabysASubFamilia.Click
        cambioCabysASubFamilia()
    End Sub

    Private Sub btnCambiarSubFamilia_Click(sender As Object, e As EventArgs) Handles btnCambiarSubFamilia.Click
        CambiarSubFamilia()
    End Sub

    Private Sub dgvSubFamilias_CellContentClick(sender As Object, e As Windows.Forms.DataGridViewCellEventArgs) Handles dgvSubFamilias.CellContentClick
        If dgvSubFamilias.SelectedRows.Count > 0 Then
            btnCambiarSubFamilia.Enabled = True
        Else
            btnCambiarSubFamilia.Enabled = False
        End If
    End Sub

    Private Sub btnNuevaSubfamilia_Click(sender As Object, e As EventArgs) Handles btnNuevaSubfamilia.Click
        Try
            Dim frm As New Familia(usuario)
            frm.ShowDialog()
            spIniciarForm()
        Catch ex As Exception

        End Try

    End Sub
End Class