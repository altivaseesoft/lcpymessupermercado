﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmNavegacionCompras
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNavegacionCompras))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvDetalleCompras = New System.Windows.Forms.DataGridView()
        Me.DTPHasta = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DTPDesde = New System.Windows.Forms.DateTimePicker()
        Me.dgvFacturasCompras = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCodProveedor = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNombreProveedor = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNumFactura = New System.Windows.Forms.TextBox()
        Me.ckZA = New System.Windows.Forms.CheckBox()
        Me.ckAZ = New System.Windows.Forms.CheckBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ckND = New System.Windows.Forms.CheckBox()
        Me.ckNC = New System.Windows.Forms.CheckBox()
        Me.ckAbonos = New System.Windows.Forms.CheckBox()
        Me.ckFacturas = New System.Windows.Forms.CheckBox()
        Me.ckDevoluciones = New System.Windows.Forms.CheckBox()
        Me.LbCargando = New System.Windows.Forms.Label()
        CType(Me.dgvDetalleCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvFacturasCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(129, 9)
        Me.Label1.MinimumSize = New System.Drawing.Size(97, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Desde"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgvDetalleCompras
        '
        Me.dgvDetalleCompras.AllowUserToAddRows = False
        Me.dgvDetalleCompras.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDetalleCompras.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDetalleCompras.BackgroundColor = System.Drawing.SystemColors.ControlLightLight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleCompras.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalleCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleCompras.Location = New System.Drawing.Point(13, 379)
        Me.dgvDetalleCompras.Name = "dgvDetalleCompras"
        Me.dgvDetalleCompras.ReadOnly = True
        Me.dgvDetalleCompras.Size = New System.Drawing.Size(1119, 169)
        Me.dgvDetalleCompras.TabIndex = 7
        '
        'DTPHasta
        '
        Me.DTPHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPHasta.Location = New System.Drawing.Point(231, 32)
        Me.DTPHasta.Name = "DTPHasta"
        Me.DTPHasta.Size = New System.Drawing.Size(97, 20)
        Me.DTPHasta.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(232, 9)
        Me.Label2.MinimumSize = New System.Drawing.Size(97, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Hasta"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DTPDesde
        '
        Me.DTPDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPDesde.Location = New System.Drawing.Point(129, 33)
        Me.DTPDesde.Name = "DTPDesde"
        Me.DTPDesde.Size = New System.Drawing.Size(97, 20)
        Me.DTPDesde.TabIndex = 4
        '
        'dgvFacturasCompras
        '
        Me.dgvFacturasCompras.AllowUserToAddRows = False
        Me.dgvFacturasCompras.AllowUserToDeleteRows = False
        Me.dgvFacturasCompras.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvFacturasCompras.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvFacturasCompras.BackgroundColor = System.Drawing.SystemColors.ControlLightLight
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFacturasCompras.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvFacturasCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFacturasCompras.Location = New System.Drawing.Point(13, 58)
        Me.dgvFacturasCompras.MultiSelect = False
        Me.dgvFacturasCompras.Name = "dgvFacturasCompras"
        Me.dgvFacturasCompras.ReadOnly = True
        Me.dgvFacturasCompras.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFacturasCompras.Size = New System.Drawing.Size(1116, 283)
        Me.dgvFacturasCompras.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(333, 9)
        Me.Label4.MinimumSize = New System.Drawing.Size(106, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 20)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Cod. Proveedor"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCodProveedor
        '
        Me.txtCodProveedor.Location = New System.Drawing.Point(333, 32)
        Me.txtCodProveedor.Name = "txtCodProveedor"
        Me.txtCodProveedor.Size = New System.Drawing.Size(106, 20)
        Me.txtCodProveedor.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(445, 9)
        Me.Label5.MinimumSize = New System.Drawing.Size(180, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(180, 20)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = " Proveedor"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombreProveedor
        '
        Me.txtNombreProveedor.Location = New System.Drawing.Point(445, 32)
        Me.txtNombreProveedor.Name = "txtNombreProveedor"
        Me.txtNombreProveedor.Size = New System.Drawing.Size(180, 20)
        Me.txtNombreProveedor.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(629, 9)
        Me.Label6.MinimumSize = New System.Drawing.Size(116, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(116, 20)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Número de Factura"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNumFactura
        '
        Me.txtNumFactura.Location = New System.Drawing.Point(629, 32)
        Me.txtNumFactura.Name = "txtNumFactura"
        Me.txtNumFactura.Size = New System.Drawing.Size(116, 20)
        Me.txtNumFactura.TabIndex = 15
        '
        'ckZA
        '
        Me.ckZA.AutoSize = True
        Me.ckZA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckZA.Location = New System.Drawing.Point(16, 33)
        Me.ckZA.Name = "ckZA"
        Me.ckZA.Size = New System.Drawing.Size(109, 17)
        Me.ckZA.TabIndex = 16
        Me.ckZA.Text = "De la Z a la A."
        Me.ckZA.UseVisualStyleBackColor = True
        '
        'ckAZ
        '
        Me.ckAZ.AutoSize = True
        Me.ckAZ.Checked = True
        Me.ckAZ.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckAZ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckAZ.Location = New System.Drawing.Point(16, 12)
        Me.ckAZ.Name = "ckAZ"
        Me.ckAZ.Size = New System.Drawing.Size(109, 17)
        Me.ckAZ.TabIndex = 17
        Me.ckAZ.Text = "De la A a la Z."
        Me.ckAZ.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.Button1.Enabled = False
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(12, 347)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(1119, 32)
        Me.Button1.TabIndex = 18
        Me.Button1.Text = "Movimiento del Inventario"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ckND)
        Me.GroupBox1.Controls.Add(Me.ckNC)
        Me.GroupBox1.Controls.Add(Me.ckAbonos)
        Me.GroupBox1.Controls.Add(Me.ckFacturas)
        Me.GroupBox1.Controls.Add(Me.ckDevoluciones)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(751, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(378, 43)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtros"
        '
        'ckND
        '
        Me.ckND.AutoSize = True
        Me.ckND.Location = New System.Drawing.Point(322, 20)
        Me.ckND.Name = "ckND"
        Me.ckND.Size = New System.Drawing.Size(48, 17)
        Me.ckND.TabIndex = 4
        Me.ckND.Text = "ND."
        Me.ckND.UseVisualStyleBackColor = True
        '
        'ckNC
        '
        Me.ckNC.AutoSize = True
        Me.ckNC.Location = New System.Drawing.Point(270, 19)
        Me.ckNC.Name = "ckNC"
        Me.ckNC.Size = New System.Drawing.Size(47, 17)
        Me.ckNC.TabIndex = 3
        Me.ckNC.Text = "NC."
        Me.ckNC.UseVisualStyleBackColor = True
        '
        'ckAbonos
        '
        Me.ckAbonos.AutoSize = True
        Me.ckAbonos.Location = New System.Drawing.Point(195, 19)
        Me.ckAbonos.Name = "ckAbonos"
        Me.ckAbonos.Size = New System.Drawing.Size(72, 17)
        Me.ckAbonos.TabIndex = 2
        Me.ckAbonos.Text = "Abonos."
        Me.ckAbonos.UseVisualStyleBackColor = True
        '
        'ckFacturas
        '
        Me.ckFacturas.AutoSize = True
        Me.ckFacturas.Location = New System.Drawing.Point(6, 19)
        Me.ckFacturas.Name = "ckFacturas"
        Me.ckFacturas.Size = New System.Drawing.Size(79, 17)
        Me.ckFacturas.TabIndex = 1
        Me.ckFacturas.Text = "Facturas."
        Me.ckFacturas.UseVisualStyleBackColor = True
        '
        'ckDevoluciones
        '
        Me.ckDevoluciones.AutoSize = True
        Me.ckDevoluciones.Location = New System.Drawing.Point(87, 19)
        Me.ckDevoluciones.Name = "ckDevoluciones"
        Me.ckDevoluciones.Size = New System.Drawing.Size(107, 17)
        Me.ckDevoluciones.TabIndex = 0
        Me.ckDevoluciones.Text = "Devoluciones."
        Me.ckDevoluciones.UseVisualStyleBackColor = True
        '
        'LbCargando
        '
        Me.LbCargando.AutoSize = True
        Me.LbCargando.BackColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.LbCargando.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbCargando.Location = New System.Drawing.Point(231, 353)
        Me.LbCargando.Name = "LbCargando"
        Me.LbCargando.Size = New System.Drawing.Size(79, 20)
        Me.LbCargando.TabIndex = 20
        Me.LbCargando.Text = "Cargando"
        '
        'frmNavegacionCompras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(1148, 561)
        Me.Controls.Add(Me.LbCargando)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ckAZ)
        Me.Controls.Add(Me.ckZA)
        Me.Controls.Add(Me.txtNumFactura)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtNombreProveedor)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtCodProveedor)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DTPHasta)
        Me.Controls.Add(Me.dgvDetalleCompras)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgvFacturasCompras)
        Me.Controls.Add(Me.DTPDesde)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmNavegacionCompras"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compras y Ctas por Pagar/Navegador de Compras"
        CType(Me.dgvDetalleCompras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvFacturasCompras, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents dgvDetalleCompras As Windows.Forms.DataGridView
	Friend WithEvents Label2 As Windows.Forms.Label
	Friend WithEvents dgvFacturasCompras As Windows.Forms.DataGridView
	Friend WithEvents Label4 As Windows.Forms.Label
	Friend WithEvents Label5 As Windows.Forms.Label
	Friend WithEvents Label6 As Windows.Forms.Label
	Friend WithEvents ckZA As Windows.Forms.CheckBox
	Friend WithEvents ckAZ As Windows.Forms.CheckBox
	Friend WithEvents Button1 As Windows.Forms.Button
	Public WithEvents DTPDesde As Windows.Forms.DateTimePicker
	Public WithEvents txtCodProveedor As Windows.Forms.TextBox
	Public WithEvents txtNombreProveedor As Windows.Forms.TextBox
	Public WithEvents txtNumFactura As Windows.Forms.TextBox
	Public WithEvents DTPHasta As Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents ckND As Windows.Forms.CheckBox
    Friend WithEvents ckNC As Windows.Forms.CheckBox
    Friend WithEvents ckAbonos As Windows.Forms.CheckBox
    Friend WithEvents ckFacturas As Windows.Forms.CheckBox
    Friend WithEvents ckDevoluciones As Windows.Forms.CheckBox
    Friend WithEvents LbCargando As Windows.Forms.Label
End Class
