Public Class frmAplicarSaldos
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents colFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontoFac As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAbonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DataSet_DevCompras1 As LcPymes_5._2.DataSet_DevCompras
    Friend WithEvents txtTotalAbono As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.DataSet_DevCompras1 = New LcPymes_5._2.DataSet_DevCompras
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMontoFac = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colAbonar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnGuardar = New System.Windows.Forms.Button
        Me.txtTotalAbono = New System.Windows.Forms.TextBox
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSet_DevCompras1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.DataMember = "AplicarSaldo"
        Me.GridControl1.DataSource = Me.DataSet_DevCompras1
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(16, 40)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(464, 176)
        Me.GridControl1.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.Text = "GridControl1"
        '
        'DataSet_DevCompras1
        '
        Me.DataSet_DevCompras1.DataSetName = "DataSet_DevCompras"
        Me.DataSet_DevCompras1.Locale = New System.Globalization.CultureInfo("es")
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFactura, Me.colMontoFac, Me.colSaldo, Me.colAbonar})
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colFactura
        '
        Me.colFactura.Caption = "Factura"
        Me.colFactura.FieldName = "Factura"
        Me.colFactura.Name = "colFactura"
        Me.colFactura.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFactura.VisibleIndex = 0
        '
        'colMontoFac
        '
        Me.colMontoFac.Caption = "Monto Original"
        Me.colMontoFac.FieldName = "Monto"
        Me.colMontoFac.Name = "colMontoFac"
        Me.colMontoFac.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMontoFac.VisibleIndex = 1
        '
        'colSaldo
        '
        Me.colSaldo.Caption = "Saldo Pend."
        Me.colSaldo.FieldName = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colSaldo.VisibleIndex = 2
        '
        'colAbonar
        '
        Me.colAbonar.Caption = "Abonar"
        Me.colAbonar.FieldName = "Abonar"
        Me.colAbonar.Name = "colAbonar"
        Me.colAbonar.VisibleIndex = 3
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(16, 8)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "Guardar"
        '
        'txtTotalAbono
        '
        Me.txtTotalAbono.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalAbono.Location = New System.Drawing.Point(304, 224)
        Me.txtTotalAbono.Name = "txtTotalAbono"
        Me.txtTotalAbono.Size = New System.Drawing.Size(168, 20)
        Me.txtTotalAbono.TabIndex = 2
        Me.txtTotalAbono.Text = "0"
        Me.txtTotalAbono.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frmAplicarSaldos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(496, 258)
        Me.Controls.Add(Me.txtTotalAbono)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.GridControl1)
        Me.Name = "frmAplicarSaldos"
        Me.Text = "Aplicar Saldo a Favor a:"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSet_DevCompras1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public CodigoProv As Integer

    Private Sub frmAplicarSaldos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Dim str As String = "SELECT Id_Compra, Factura, Fecha, TotalFactura As Monto,dbo.SaldoFacturaCompra(GETDATE(), Id_Compra, CodigoProv) AS Saldo, 0 AS Abonar, Cod_MonedaCompra FROM compras WHERE (FacturaCancelado = 0) AND (TipoCompra = 'CRE') AND (CodigoProv = " & CodigoProv & ") AND (dbo.SaldoFacturaCompra(GETDATE(), Id_Compra, CodigoProv) <> 0)"
		cFunciones.Llenar_Tabla_Generico(str, Me.DataSet_DevCompras1.AplicarSaldo, GetSetting("SeeSoft", "SeePOS", "Conexion"))
        

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub GridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridView1.KeyDown

        If e.KeyCode = Windows.Forms.Keys.Enter Then
            Dim t As Double = 0
            For i As Integer = 0 To Me.DataSet_DevCompras1.AplicarSaldo.Count - 1
                t += Me.DataSet_DevCompras1.AplicarSaldo(i).Abonar

            Next
            Me.txtTotalAbono.Text = Format(t, "#,#0.00")
        End If


    End Sub

    Private Sub GridView1_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Dim t As Double = 0
        For i As Integer = 0 To Me.DataSet_DevCompras1.AplicarSaldo.Count - 1
            t += Me.DataSet_DevCompras1.AplicarSaldo(i).Abonar

        Next
        Me.txtTotalAbono.Text = Format(t, "#,#0.00")
    End Sub
End Class
