Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data

Public Class Traslados
    Inherits FrmPlantilla

#Region "Variables"
    Public Cedula_usuario As String
    Public NombreUsuario As String
    Private sqlConexion As SqlConnection
    Public CConexion As New Conexion
    Dim NuevaConexion As String 'JCGA    
    Dim usua As Usuario_Logeado
    Dim strModulo As String = ""
    Dim buscando As Boolean = False
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario_Parametro As Object, Optional ByVal Conexion As String = "")
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        NuevaConexion = Conexion 'JCGA
        usua = Usuario_Parametro 'JCGA 19032007

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistenciaBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents AdapterTraslados As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterTrasladosDetalles As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSetTraslados1 As DataSetTraslados
    Friend WithEvents AdapterInventario As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterBodega As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents NuevoTraslado As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GuardarTraslado As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents EliminarTraslado As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents EditarTraslado As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel3 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents AdapterUsuarios As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents TxtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtPrecioBase As System.Windows.Forms.TextBox
    Friend WithEvents txtFletes As System.Windows.Forms.TextBox
    Friend WithEvents txtOtrosCargos As System.Windows.Forms.TextBox
    Friend WithEvents txtBarras As System.Windows.Forms.TextBox
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colBarras As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Traslados))
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtBarras = New System.Windows.Forms.TextBox
        Me.DataSetTraslados1 = New LcPymes_5._2.DataSetTraslados
        Me.EditarTraslado = New DevExpress.XtraEditors.SimpleButton
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.NuevoTraslado = New DevExpress.XtraEditors.SimpleButton
        Me.GuardarTraslado = New DevExpress.XtraEditors.SimpleButton
        Me.EliminarTraslado = New DevExpress.XtraEditors.SimpleButton
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colExistenciaBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colBarras = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtNombre = New System.Windows.Forms.TextBox
        Me.TxtDescripcion = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.AdapterTraslados = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.AdapterTrasladosDetalles = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.AdapterInventario = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.AdapterBodega = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel3 = New System.Windows.Forms.StatusBarPanel
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.AdapterUsuarios = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.txtPrecioBase = New System.Windows.Forms.TextBox
        Me.txtFletes = New System.Windows.Forms.TextBox
        Me.txtOtrosCargos = New System.Windows.Forms.TextBox
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataSetTraslados1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(610, 32)
        Me.TituloModulo.Text = "Traslados entre Bodegas"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 321)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(610, 53)
        Me.ToolBar1.TabIndex = 7
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(2362, 370)
        Me.DataNavigator.Name = "DataNavigator"
        Me.DataNavigator.Size = New System.Drawing.Size(134, 22)
        Me.DataNavigator.Visible = False
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Text = "Anular"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 14)
        Me.Label2.TabIndex = 70
        Me.Label2.Text = "Numero"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(96, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(512, 14)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "Descripci�n"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 14)
        Me.Label3.TabIndex = 72
        Me.Label3.Text = "Fecha"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(96, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(248, 14)
        Me.Label4.TabIndex = 73
        Me.Label4.Text = "Origen"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(352, 73)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(256, 14)
        Me.Label5.TabIndex = 74
        Me.Label5.Text = "Destino"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.txtBarras)
        Me.GroupBox1.Controls.Add(Me.EditarTraslado)
        Me.GroupBox1.Controls.Add(Me.NuevoTraslado)
        Me.GroupBox1.Controls.Add(Me.GuardarTraslado)
        Me.GroupBox1.Controls.Add(Me.EliminarTraslado)
        Me.GroupBox1.Controls.Add(Me.GridControl1)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 112)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(600, 208)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'txtBarras
        '
        Me.txtBarras.BackColor = System.Drawing.Color.RoyalBlue
        Me.txtBarras.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBarras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle.Barras"))
        Me.txtBarras.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.txtBarras.Location = New System.Drawing.Point(440, 0)
        Me.txtBarras.Name = "txtBarras"
        Me.txtBarras.Size = New System.Drawing.Size(72, 13)
        Me.txtBarras.TabIndex = 143
        Me.txtBarras.Text = ""
        '
        'DataSetTraslados1
        '
        Me.DataSetTraslados1.DataSetName = "DataSetTraslados"
        Me.DataSetTraslados1.Locale = New System.Globalization.CultureInfo("es-MX")
        '
        'EditarTraslado
        '
        Me.EditarTraslado.ImageIndex = 9
        Me.EditarTraslado.ImageList = Me.ImageList2
        Me.EditarTraslado.Location = New System.Drawing.Point(432, 56)
        Me.EditarTraslado.Name = "EditarTraslado"
        Me.EditarTraslado.TabIndex = 7
        Me.EditarTraslado.Text = "Editar"
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'NuevoTraslado
        '
        Me.NuevoTraslado.ImageIndex = 0
        Me.NuevoTraslado.ImageList = Me.ImageList2
        Me.NuevoTraslado.Location = New System.Drawing.Point(192, 56)
        Me.NuevoTraslado.Name = "NuevoTraslado"
        Me.NuevoTraslado.TabIndex = 4
        Me.NuevoTraslado.Text = "Nuevo"
        '
        'GuardarTraslado
        '
        Me.GuardarTraslado.ImageIndex = 2
        Me.GuardarTraslado.ImageList = Me.ImageList2
        Me.GuardarTraslado.Location = New System.Drawing.Point(272, 56)
        Me.GuardarTraslado.Name = "GuardarTraslado"
        Me.GuardarTraslado.TabIndex = 5
        Me.GuardarTraslado.Text = "Guardar"
        '
        'EliminarTraslado
        '
        Me.EliminarTraslado.ImageIndex = 3
        Me.EliminarTraslado.ImageList = Me.ImageList2
        Me.EliminarTraslado.Location = New System.Drawing.Point(352, 56)
        Me.EliminarTraslado.Name = "EliminarTraslado"
        Me.EliminarTraslado.TabIndex = 6
        Me.EliminarTraslado.Text = "Eliminar"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.DataMember = "Traslados.TrasladosTrasladosDetalle"
        Me.GridControl1.DataSource = Me.DataSetTraslados1
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.GridControl1.Location = New System.Drawing.Point(8, 88)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(584, 112)
        Me.GridControl1.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 142
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.colDescripcion, Me.colExistenciaBodega, Me.colBarras})
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowDetailButtons = False
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowVertLines = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Codigo"
        Me.GridColumn1.FieldName = "Codigo"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.VisibleIndex = 0
        '
        'colDescripcion
        '
        Me.colDescripcion.Caption = "Descripcion"
        Me.colDescripcion.FieldName = "Descripcion"
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDescripcion.VisibleIndex = 2
        Me.colDescripcion.Width = 463
        '
        'colExistenciaBodega
        '
        Me.colExistenciaBodega.Caption = "Cantidad"
        Me.colExistenciaBodega.FieldName = "cantidad"
        Me.colExistenciaBodega.Name = "colExistenciaBodega"
        Me.colExistenciaBodega.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colExistenciaBodega.VisibleIndex = 3
        Me.colExistenciaBodega.Width = 99
        '
        'colBarras
        '
        Me.colBarras.Caption = "Barras"
        Me.colBarras.FieldName = "Barras"
        Me.colBarras.Name = "colBarras"
        Me.colBarras.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colBarras.VisibleIndex = 1
        '
        'TextBox6
        '
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.ForeColor = System.Drawing.Color.Blue
        Me.TextBox6.Location = New System.Drawing.Point(96, 72)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(80, 13)
        Me.TextBox6.TabIndex = 3
        Me.TextBox6.Text = ""
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(96, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 14)
        Me.Label9.TabIndex = 140
        Me.Label9.Text = "Cantidad"
        '
        'TextBox5
        '
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox5.Enabled = False
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.ForeColor = System.Drawing.Color.Blue
        Me.TextBox5.Location = New System.Drawing.Point(10, 72)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(80, 13)
        Me.TextBox5.TabIndex = 2
        Me.TextBox5.Text = ""
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(11, 57)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 14)
        Me.Label8.TabIndex = 138
        Me.Label8.Text = "Existencia"
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox4.Enabled = False
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.ForeColor = System.Drawing.Color.Blue
        Me.TextBox4.Location = New System.Drawing.Point(97, 40)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(495, 13)
        Me.TextBox4.TabIndex = 1
        Me.TextBox4.Text = ""
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(97, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(495, 14)
        Me.Label7.TabIndex = 136
        Me.Label7.Text = "Descripci�n"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(10, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 14)
        Me.Label6.TabIndex = 134
        Me.Label6.Text = "C�digo"
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.ForeColor = System.Drawing.Color.Blue
        Me.TextBox3.Location = New System.Drawing.Point(10, 40)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(80, 13)
        Me.TextBox3.TabIndex = 0
        Me.TextBox3.Text = ""
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(0, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(600, 16)
        Me.Label12.TabIndex = 132
        Me.Label12.Text = "Detalles del Traslado"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombre
        '
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Enabled = False
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.ForeColor = System.Drawing.Color.Blue
        Me.txtNombre.Location = New System.Drawing.Point(8, 54)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(80, 13)
        Me.txtNombre.TabIndex = 1
        Me.txtNombre.Text = ""
        Me.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtDescripcion
        '
        Me.TxtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDescripcion.ForeColor = System.Drawing.Color.Blue
        Me.TxtDescripcion.Location = New System.Drawing.Point(96, 54)
        Me.TxtDescripcion.Name = "TxtDescripcion"
        Me.TxtDescripcion.Size = New System.Drawing.Size(512, 13)
        Me.TxtDescripcion.TabIndex = 2
        Me.TxtDescripcion.Text = ""
        Me.TxtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox2.Enabled = False
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.Blue
        Me.TextBox2.Location = New System.Drawing.Point(8, 88)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(80, 13)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Text = ""
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.DataSetTraslados1
        Me.ComboBox1.DisplayMember = "BodegaOrigen.Nombre"
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(96, 88)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(248, 21)
        Me.ComboBox1.TabIndex = 4
        Me.ComboBox1.ValueMember = "BodegaOrigen.IdBodega"
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.DataSetTraslados1
        Me.ComboBox2.DisplayMember = "BodegaDestino.Nombre"
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Location = New System.Drawing.Point(352, 88)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(256, 21)
        Me.ComboBox2.TabIndex = 5
        Me.ComboBox2.ValueMember = "BodegaDestino.IdBodega"
        '
        'AdapterTraslados
        '
        Me.AdapterTraslados.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterTraslados.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterTraslados.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterTraslados.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Traslados", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Traslados", "Id_Traslados"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("BodegaDestino", "BodegaDestino"), New System.Data.Common.DataColumnMapping("BodegaOrigen", "BodegaOrigen"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula")})})
        Me.AdapterTraslados.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Traslados WHERE (Id_Traslados = @Original_Id_Traslados) AND (Anulado " & _
        "= @Original_Anulado) AND (BodegaDestino = @Original_BodegaDestino) AND (BodegaOr" & _
        "igen = @Original_BodegaOrigen) AND (Descripcion = @Original_Descripcion) AND (Fe" & _
        "cha = @Original_Fecha)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Traslados", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Traslados", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BodegaDestino", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BodegaDestino", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BodegaOrigen", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BodegaOrigen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=Esteban1;packet size=4096;integrated security=SSPI;data source=""Es" & _
        "teban1\SQLEXPRESS"";persist security info=False;initial catalog=SeePos"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Traslados(Fecha, Descripcion, Anulado, BodegaDestino, BodegaOrigen, C" & _
        "edula) VALUES (@Fecha, @Descripcion, @Anulado, @BodegaDestino, @BodegaOrigen, @C" & _
        "edula); SELECT Id_Traslados, Fecha, Descripcion, Anulado, BodegaDestino, BodegaO" & _
        "rigen, Cedula FROM Traslados WHERE (Id_Traslados = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 50, "Descripcion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BodegaDestino", System.Data.SqlDbType.BigInt, 8, "BodegaDestino"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BodegaOrigen", System.Data.SqlDbType.BigInt, 8, "BodegaOrigen"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id_Traslados, Fecha, Descripcion, Anulado, BodegaDestino, BodegaOrigen, Ce" & _
        "dula FROM Traslados"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Traslados SET Fecha = @Fecha, Descripcion = @Descripcion, Anulado = @Anula" & _
        "do, BodegaDestino = @BodegaDestino, BodegaOrigen = @BodegaOrigen, Cedula = @Cedu" & _
        "la WHERE (Id_Traslados = @Original_Id_Traslados) AND (Anulado = @Original_Anulad" & _
        "o) AND (BodegaDestino = @Original_BodegaDestino) AND (BodegaOrigen = @Original_B" & _
        "odegaOrigen) AND (Descripcion = @Original_Descripcion) AND (Fecha = @Original_Fe" & _
        "cha); SELECT Id_Traslados, Fecha, Descripcion, Anulado, BodegaDestino, BodegaOri" & _
        "gen, Cedula FROM Traslados WHERE (Id_Traslados = @Id_Traslados)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 50, "Descripcion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BodegaDestino", System.Data.SqlDbType.BigInt, 8, "BodegaDestino"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BodegaOrigen", System.Data.SqlDbType.BigInt, 8, "BodegaOrigen"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Traslados", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Traslados", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BodegaDestino", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BodegaDestino", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BodegaOrigen", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BodegaOrigen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Traslados", System.Data.SqlDbType.BigInt, 8, "Id_Traslados"))
        '
        'AdapterTrasladosDetalles
        '
        Me.AdapterTrasladosDetalles.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterTrasladosDetalles.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterTrasladosDetalles.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterTrasladosDetalles.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TrasladosDetalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Traslados", "Id_Traslados"), New System.Data.Common.DataColumnMapping("cantidad", "cantidad"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("PrecioBase", "PrecioBase"), New System.Data.Common.DataColumnMapping("Fletes", "Fletes"), New System.Data.Common.DataColumnMapping("OtrosCargos", "OtrosCargos"), New System.Data.Common.DataColumnMapping("Barras", "Barras")})})
        Me.AdapterTrasladosDetalles.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM TrasladosDetalle WHERE (Id = @Original_Id) AND (Barras = @Original_Ba" & _
        "rras OR @Original_Barras IS NULL AND Barras IS NULL) AND (Codigo = @Original_Cod" & _
        "igo) AND (Descripcion = @Original_Descripcion) AND (Existencia = @Original_Exist" & _
        "encia) AND (Fletes = @Original_Fletes) AND (Id_Traslados = @Original_Id_Traslado" & _
        "s) AND (OtrosCargos = @Original_OtrosCargos) AND (PrecioBase = @Original_PrecioB" & _
        "ase) AND (cantidad = @Original_cantidad)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fletes", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fletes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Traslados", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Traslados", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OtrosCargos", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OtrosCargos", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PrecioBase", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioBase", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cantidad", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO TrasladosDetalle(Id_Traslados, cantidad, Codigo, Existencia, Descripc" & _
        "ion, PrecioBase, Fletes, OtrosCargos, Barras) VALUES (@Id_Traslados, @cantidad, " & _
        "@Codigo, @Existencia, @Descripcion, @PrecioBase, @Fletes, @OtrosCargos, @Barras)" & _
        "; SELECT Id_Traslados, cantidad, Codigo, Id, Existencia, Descripcion, PrecioBase" & _
        ", Fletes, OtrosCargos, Barras FROM TrasladosDetalle WHERE (Id = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Traslados", System.Data.SqlDbType.BigInt, 8, "Id_Traslados"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PrecioBase", System.Data.SqlDbType.Float, 8, "PrecioBase"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fletes", System.Data.SqlDbType.Float, 8, "Fletes"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OtrosCargos", System.Data.SqlDbType.Float, 8, "OtrosCargos"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 50, "Barras"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Id_Traslados, cantidad, Codigo, Id, Existencia, Descripcion, PrecioBase, F" & _
        "letes, OtrosCargos, Barras FROM TrasladosDetalle"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE TrasladosDetalle SET Id_Traslados = @Id_Traslados, cantidad = @cantidad, C" & _
        "odigo = @Codigo, Existencia = @Existencia, Descripcion = @Descripcion, PrecioBas" & _
        "e = @PrecioBase, Fletes = @Fletes, OtrosCargos = @OtrosCargos, Barras = @Barras " & _
        "WHERE (Id = @Original_Id) AND (Barras = @Original_Barras OR @Original_Barras IS " & _
        "NULL AND Barras IS NULL) AND (Codigo = @Original_Codigo) AND (Descripcion = @Ori" & _
        "ginal_Descripcion) AND (Existencia = @Original_Existencia) AND (Fletes = @Origin" & _
        "al_Fletes) AND (Id_Traslados = @Original_Id_Traslados) AND (OtrosCargos = @Origi" & _
        "nal_OtrosCargos) AND (PrecioBase = @Original_PrecioBase) AND (cantidad = @Origin" & _
        "al_cantidad); SELECT Id_Traslados, cantidad, Codigo, Id, Existencia, Descripcion" & _
        ", PrecioBase, Fletes, OtrosCargos, Barras FROM TrasladosDetalle WHERE (Id = @Id)" & _
        ""
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Traslados", System.Data.SqlDbType.BigInt, 8, "Id_Traslados"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@cantidad", System.Data.SqlDbType.Float, 8, "cantidad"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PrecioBase", System.Data.SqlDbType.Float, 8, "PrecioBase"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fletes", System.Data.SqlDbType.Float, 8, "Fletes"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OtrosCargos", System.Data.SqlDbType.Float, 8, "OtrosCargos"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 50, "Barras"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fletes", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fletes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Traslados", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Traslados", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OtrosCargos", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OtrosCargos", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PrecioBase", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioBase", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id"))
        '
        'AdapterInventario
        '
        Me.AdapterInventario.DeleteCommand = Me.SqlDeleteCommand3
        Me.AdapterInventario.InsertCommand = Me.SqlInsertCommand3
        Me.AdapterInventario.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterInventario.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Inventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion")})})
        Me.AdapterInventario.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Inventario WHERE (Codigo = @Original_Codigo) AND (Descripcion = @Orig" & _
        "inal_Descripcion)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Inventario(Codigo, Descripcion) VALUES (@Codigo, @Descripcion); SELEC" & _
        "T Codigo, Descripcion FROM Inventario WHERE (Codigo = @Codigo)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT Codigo, Descripcion FROM Inventario"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Inventario SET Codigo = @Codigo, Descripcion = @Descripcion WHERE (Codigo " & _
        "= @Original_Codigo) AND (Descripcion = @Original_Descripcion); SELECT Codigo, De" & _
        "scripcion FROM Inventario WHERE (Codigo = @Codigo)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        '
        'AdapterBodega
        '
        Me.AdapterBodega.DeleteCommand = Me.SqlDeleteCommand4
        Me.AdapterBodega.InsertCommand = Me.SqlInsertCommand4
        Me.AdapterBodega.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterBodega.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "BodegaOrigen", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("IdBodega", "IdBodega"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        Me.AdapterBodega.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Bodega WHERE (IdBodega = @Original_IdBodega) AND (Nombre = @Original_" & _
        "Nombre)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdBodega", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO Bodega(Nombre) VALUES (@Nombre); SELECT IdBodega, Nombre FROM Bodega " & _
        "WHERE (IdBodega = @@IDENTITY)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT IdBodega, Nombre FROM Bodega"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE Bodega SET Nombre = @Nombre WHERE (IdBodega = @Original_IdBodega) AND (Nom" & _
        "bre = @Original_Nombre); SELECT IdBodega, Nombre FROM Bodega WHERE (IdBodega = @" & _
        "IdBodega)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdBodega", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdBodega", System.Data.SqlDbType.BigInt, 8, "IdBodega"))
        '
        'Label36
        '
        Me.Label36.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(472, 336)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(48, 14)
        Me.Label36.TabIndex = 189
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(306, 382)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(2182, 13)
        Me.txtNombreUsuario.TabIndex = 190
        Me.txtNombreUsuario.Text = ""
        '
        'txtUsuario
        '
        Me.txtUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(528, 336)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(56, 13)
        Me.txtUsuario.TabIndex = 0
        Me.txtUsuario.Text = ""
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 374)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2, Me.StatusBarPanel3})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(610, 18)
        Me.StatusBar1.TabIndex = 196
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.StatusBarPanel1.Text = "Estado"
        '
        'StatusBarPanel3
        '
        Me.StatusBarPanel3.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.StatusBarPanel3.Width = 394
        '
        'CheckBox1
        '
        Me.CheckBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBox1.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox1.Enabled = False
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.Red
        Me.CheckBox1.Location = New System.Drawing.Point(376, 336)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(88, 15)
        Me.CheckBox1.TabIndex = 197
        Me.CheckBox1.Text = "Anulada"
        '
        'AdapterUsuarios
        '
        Me.AdapterUsuarios.InsertCommand = Me.SqlInsertCommand5
        Me.AdapterUsuarios.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterUsuarios.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Clave_Entrada", "Clave_Entrada"), New System.Data.Common.DataColumnMapping("Clave_Interna", "Clave_Interna"), New System.Data.Common.DataColumnMapping("Id_Usuario", "Id_Usuario"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula")})})
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO Usuarios(Nombre, Clave_Entrada, Clave_Interna, Id_Usuario) VALUES (@N" & _
        "ombre, @Clave_Entrada, @Clave_Interna, @Id_Usuario); SELECT Nombre, Clave_Entrad" & _
        "a, Clave_Interna, Id_Usuario, Id_Usuario AS Cedula FROM Usuarios"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Clave_Entrada", System.Data.SqlDbType.VarChar, 30, "Clave_Entrada"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Clave_Interna", System.Data.SqlDbType.VarChar, 30, "Clave_Interna"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Usuario", System.Data.SqlDbType.VarChar, 50, "Cedula"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT Nombre, Clave_Entrada, Clave_Interna, Id_Usuario, Id_Usuario AS Cedula FRO" & _
        "M Usuarios"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'txtPrecioBase
        '
        Me.txtPrecioBase.Location = New System.Drawing.Point(656, 104)
        Me.txtPrecioBase.Name = "txtPrecioBase"
        Me.txtPrecioBase.TabIndex = 198
        Me.txtPrecioBase.Text = ""
        '
        'txtFletes
        '
        Me.txtFletes.Location = New System.Drawing.Point(656, 152)
        Me.txtFletes.Name = "txtFletes"
        Me.txtFletes.TabIndex = 199
        Me.txtFletes.Text = ""
        '
        'txtOtrosCargos
        '
        Me.txtOtrosCargos.Location = New System.Drawing.Point(656, 200)
        Me.txtOtrosCargos.Name = "txtOtrosCargos"
        Me.txtOtrosCargos.TabIndex = 200
        Me.txtOtrosCargos.Text = ""
        '
        'Traslados
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(610, 392)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.txtOtrosCargos)
        Me.Controls.Add(Me.txtFletes)
        Me.Controls.Add(Me.txtPrecioBase)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TxtDescripcion)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.StatusBar1)
        Me.MaximumSize = New System.Drawing.Size(616, 424)
        Me.Name = "Traslados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Administrador de Traslados"
        Me.Controls.SetChildIndex(Me.StatusBar1, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.ComboBox1, 0)
        Me.Controls.SetChildIndex(Me.ComboBox2, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.TxtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.TextBox2, 0)
        Me.Controls.SetChildIndex(Me.txtNombreUsuario, 0)
        Me.Controls.SetChildIndex(Me.txtPrecioBase, 0)
        Me.Controls.SetChildIndex(Me.txtFletes, 0)
        Me.Controls.SetChildIndex(Me.txtOtrosCargos, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.Label36, 0)
        Me.Controls.SetChildIndex(Me.CheckBox1, 0)
        Me.Controls.SetChildIndex(Me.txtUsuario, 0)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataSetTraslados1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Load"
    Private Sub Traslados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cx As New cFunciones 'Cargan los adaptadores 
        Me.SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePOS", "CONEXION"), NuevaConexion) 'JCGA
        strModulo = IIf(NuevaConexion = "", "SeePOS", "SeePos") 'JCGA

        Try
            '******************************************************************************
            'Valores por Defecto
            '******************************************************************************
            Me.DataSetTraslados1.TrasladosDetalle.cantidadColumn.DefaultValue = 1
            Me.DataSetTraslados1.Traslados.FechaColumn.DefaultValue = Now.Date
            Me.DataSetTraslados1.Traslados.DescripcionColumn.DefaultValue = ""


            cx.Llenar_Tabla_Generico("SELECT ID_Bodega as IdBodega, Nombre_Bodega as Nombre FROM Bodegas", Me.DataSetTraslados1.BodegaDestino, Me.SqlConnection1.ConnectionString) 'Se le agrego el Me.SqlConnection1.ConnectionString JCGA
            cx.Llenar_Tabla_Generico("SELECT ID_Bodega as IdBodega, Nombre_Bodega as Nombre FROM Bodegas", Me.DataSetTraslados1.BodegaOrigen, Me.SqlConnection1.ConnectionString) 'Se le agrego el Me.SqlConnection1.ConnectionString JCGA            
            Me.AdapterUsuarios.Fill(Me.DataSetTraslados1.Usuarios)

            Me.DataSetTraslados1.TrasladosDetalle.cantidadColumn.DefaultValue = 0
            Me.DataSetTraslados1.Traslados.AnuladoColumn.DefaultValue = False
            Me.DataSetTraslados1.Traslados.BodegaDestinoColumn.DefaultValue = Me.DataSetTraslados1.BodegaOrigen.Rows(0).Item("IdBodega")
            Me.DataSetTraslados1.Traslados.BodegaOrigenColumn.DefaultValue = Me.DataSetTraslados1.BodegaOrigen.Rows(0).Item("IdBodega")
            Me.DataSetTraslados1.Traslados.BodegaDestinoColumn.DefaultValue = Me.DataSetTraslados1.BodegaOrigen.Rows(0).Item("IdBodega")
            Me.DataSetTraslados1.Traslados.BodegaDestinoColumn.DefaultValue = Me.DataSetTraslados1.BodegaOrigen.Rows(0).Item("IdBodega")
            Me.DataSetTraslados1.Traslados.DescripcionColumn.DefaultValue = ""

            Me.ToolBarNuevo.Enabled = False
            Me.ToolBarRegistrar.Enabled = False
            Me.ToolBarExcel.Enabled = False
            Me.ToolBarBuscar.Enabled = False
            Me.ToolBarImprimir.Enabled = False
            Me.ToolBarEliminar.Enabled = False
            Me.ToolBarCerrar.Enabled = True
            InHabilitarControlesTodos()
            'InHabilitar botones Hospedaje
            Me.NuevoTraslado.Enabled = False
            Me.EditarTraslado.Enabled = False
            Me.EliminarTraslado.Enabled = False
            Me.GuardarTraslado.Enabled = False

            'Traslado
            Me.DataSetTraslados1.Traslados.Id_TrasladosColumn.AutoIncrement = True
            Me.DataSetTraslados1.Traslados.Id_TrasladosColumn.AutoIncrementSeed = -1
            Me.DataSetTraslados1.Traslados.Id_TrasladosColumn.AutoIncrementStep = -1
            'Detalle Traslado
            Me.DataSetTraslados1.TrasladosDetalle.IdColumn.AutoIncrement = True
            Me.DataSetTraslados1.TrasladosDetalle.IdColumn.AutoIncrementSeed = -1
            Me.DataSetTraslados1.TrasladosDetalle.IdColumn.AutoIncrementStep = -1

            Bindings()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Bindings()
        Me.TextBox6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle.cantidad"))
        Me.TextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle.Existencia"))
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle.Descripcion"))
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle.Codigo"))
        Me.txtNombre.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.Id_Traslados"))
        Me.TxtDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.Descripcion"))
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.Fecha"))
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetTraslados1, "Traslados.BodegaOrigen"))
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetTraslados1, "Traslados.BodegaDestino"))
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetTraslados1, "Traslados.Anulado"))
        Me.txtPrecioBase.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle.PrecioBase"))
        Me.txtFletes.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle.Fletes"))
        Me.txtOtrosCargos.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle.OtrosCargos"))
    End Sub
#End Region

#Region "Controles"
    Function HabilitarControlesTodos()
        HabilitarControlesTraslados()
        HabilitarControlesDetalles()
    End Function

    Function HabilitarControlesTraslados()
        TxtDescripcion.Enabled = True
        ComboBox1.Enabled = True
        ComboBox2.Enabled = True
    End Function

    Function HabilitarControlesDetalles()
        TextBox3.Enabled = True
        TextBox6.Enabled = True
    End Function

    Function InHabilitarControlesTodos()
        InHabilitarControlesTraslados()
        InHabilitarControlesDetalles()
    End Function

    Function InHabilitarControlesTraslados()
        TxtDescripcion.Enabled = False
        ComboBox1.Enabled = False
        ComboBox2.Enabled = False
    End Function

    Function InHabilitarControlesDetalles()
        TextBox3.Enabled = False
        TextBox6.Enabled = False
    End Function
#End Region

#Region "Logueo Usuario"
    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.Loggin_Usuario() Then
            End If
        End If
    End Sub

    Function Loggin_Usuario() As Boolean
        Try
            If Me.BindingContext(Me.DataSetTraslados1.Usuarios).Count > 0 Then
                Dim Usuario_autorizadores() As System.Data.DataRow
                Dim Usua As System.Data.DataRow
                Usuario_autorizadores = Me.DataSetTraslados1.Usuarios.Select("Clave_Interna ='" & txtUsuario.Text & "'")
                If Usuario_autorizadores.Length <> 0 Then
                    Usua = Usuario_autorizadores(0)
                    txtNombreUsuario.Text = Usua("Nombre")
                    NombreUsuario = Usua("Nombre")
                    Cedula_usuario = Usua("Cedula")
                    Me.DataSetTraslados1.Traslados.CedulaColumn.DefaultValue = Usua("Cedula")
                    txtUsuario.Enabled = False ' se inabilita el campo de la contrase�a
                    Me.ToolBarNuevo.Enabled = True
                    Me.ToolBarRegistrar.Enabled = False
                    Me.ToolBarBuscar.Enabled = True
                    Return True
                Else ' si no existe una contrase�la como esta
                    MsgBox("Contrase�a interna incorrecta", MsgBoxStyle.Exclamation)
                    txtUsuario.Text = ""
                    Return False
                End If
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
            End If
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function
#End Region

#Region "Toolbar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
        PMU = VSM(usua.Cedula, Me.Name) 'Carga los privilegios del usuario con el modu

        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1

            Case 1 : Nuevos()

            Case 2 : If PMU.Find Then Buscar() Else MsgBox("No tiene permiso para Buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 3 : If PMU.Update Then Me.Guardar() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 4 : If PMU.Delete Then Anular() Else MsgBox("No tiene permiso para eliminar o anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 5 : If PMU.Print Then Imprimir(Me.BindingContext(Me.DataSetTraslados1, "Traslados").Current("Id_Traslados"))

                'Case 6 : Editar()

            Case 7 : Salir()
        End Select
    End Sub

#End Region

#Region "Imprimir"
    Function Imprimir(ByVal id As String)
        Me.ToolBar1.Buttons(4).Enabled = False
        Dim ReporteTraslados As New ReporteTraslados
        'Dim visor As New frmVisorReportes
        ReporteTraslados.SetParameterValue(0, id)
        CrystalReportsConexion.LoadShow(ReporteTraslados, MdiParent, Me.SqlConnection1.ConnectionString)
        Me.ToolBar1.Buttons(4).Enabled = True
    End Function
#End Region

#Region "Eliminar"
    Function Anular()

        If Me.BindingContext(Me.DataSetTraslados1, "Traslados").Current("Anulado") = True Then
            MsgBox("No se puede anular este traslado,por que ya fue anulado", MsgBoxStyle.Critical)
            Exit Function
        End If

        Dim Cx As New Conexion
        Dim valida As String
        Dim num_traslado As String = Me.txtNombre.Text

        valida = Cx.SlqExecuteScalar(Cx.Conectar("SeePos"), "SELECT Contabilizado FROM Traslados WHERE Id_Traslados= '" & num_traslado & "'")
        Cx.DesConectar(Cx.sQlconexion)
        If valida = True Then ' se verifica si la factura seleccionada puede ser eliminada
            MessageBox.Show("El traslado seleccionado no puede ser anulado, ya fue contabilizado....", "Informaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Exit Function
        End If

        If MsgBox("Deseas Anular este Traslado", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Try
                Me.BindingContext(Me.DataSetTraslados1, "Traslados").Current("Anulado") = True
                Me.BindingContext(Me.DataSetTraslados1, "Traslados").EndCurrentEdit()
                Me.AdapterTraslados.Update(Me.DataSetTraslados1.Traslados)
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarExcel.Enabled = False
                Me.ToolBarRegistrar.Enabled = False
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarImprimir.Enabled = True
                Me.ToolBarEliminar.Enabled = True
                Me.ToolBarCerrar.Enabled = True
                MsgBox("Traslado Anulado Satisfactoriamente", MsgBoxStyle.Information)
            Catch ex As Exception
                MsgBox(ex.ToString)
                MsgBox("Anular Borrar este Traslado", MsgBoxStyle.Critical)
            End Try
        End If
    End Function
#End Region

#Region "Nuevo"
    Function Nuevos()
        If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
            Try 'inicia la edicion
                Me.ToolBar1.Buttons(0).Text = "Cancelar"
                Me.ToolBar1.Buttons(0).ImageIndex = 8
                Me.DataSetTraslados1.TrasladosDetalle.Clear()
                Me.DataSetTraslados1.Traslados.Clear()
                Me.DataSetTraslados1.Traslados.FechaColumn.DefaultValue = Now
                Me.BindingContext(Me.DataSetTraslados1, "Traslados").EndCurrentEdit()
                Me.BindingContext(Me.DataSetTraslados1, "Traslados").AddNew()
                Me.HabilitarControlesTraslados()
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarRegistrar.Enabled = True
                Me.ToolBarBuscar.Enabled = False
                Me.ToolBarImprimir.Enabled = False
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarCerrar.Enabled = True
                '******************************************************************
                '                          Control Hijos
                '******************************************************************
                '+++++++++++++++++++++++++InHabilitar botones Traslados Detalles+++++++++++
                Me.NuevoTraslado.Enabled = False
                Me.EditarTraslado.Enabled = False
                Me.EliminarTraslado.Enabled = False
                Me.GuardarTraslado.Enabled = False
                'Control de icono
                Me.NuevoTraslado.Text = "Nuevo"
                Me.NuevoTraslado.ImageIndex = 0
                Me.EditarTraslado.Text = "Editar"
                Me.EditarTraslado.ImageIndex = 9
                'Habilitar Grid
                Me.GridControl1.Enabled = True
                Me.TxtDescripcion.Focus()
            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        Else
            Try
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                Me.BindingContext(Me.DataSetTraslados1, "Traslados").CancelCurrentEdit()
                Me.DataSetTraslados1.TrasladosDetalle.Clear()
                Me.DataSetTraslados1.Traslados.Clear()
                Me.ToolBarNuevo.Enabled = True
                Me.ToolBarRegistrar.Enabled = False
                Me.ToolBarBuscar.Enabled = True
                Me.ToolBarImprimir.Enabled = False
                Me.ToolBarEliminar.Enabled = False
                Me.ToolBarCerrar.Enabled = True
                Me.InHabilitarControlesTodos() '******************************************************************
                '                          Control Hijos
                '******************************************************************
                '+++++++++++++++++++++++++InHabilitar botones Hospedaje+++++++++++
                Me.NuevoTraslado.Enabled = False
                Me.EditarTraslado.Enabled = False
                Me.EliminarTraslado.Enabled = False
                Me.GuardarTraslado.Enabled = False
                'Control de icono
                Me.NuevoTraslado.Text = "Nuevo"
                Me.NuevoTraslado.ImageIndex = 0
                Me.EditarTraslado.Text = "Editar"
                Me.EditarTraslado.ImageIndex = 9
                'Habilitar Grid
                Me.GridControl1.Enabled = True
            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        End If
    End Function
#End Region

#Region "Guardar"
    Function ValidarGuardar() As Boolean
        If TxtDescripcion.Text = "" Then
            MsgBox("Debes digitar la descripci�n", MsgBoxStyle.Information)
            Return False
        End If

        If Me.DataSetTraslados1.TrasladosDetalle.Rows.Count = 0 Then
            MsgBox("Debes Ingresar un detalle como m�nimo", MsgBoxStyle.Critical)
            Return False
        End If
        Return True
    End Function
    Function Registar() As Boolean
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            'Padre
            Me.AdapterTraslados.InsertCommand.Transaction = Trans
            Me.AdapterTraslados.UpdateCommand.Transaction = Trans
            Me.AdapterTraslados.DeleteCommand.Transaction = Trans
            'Hijos
            Me.AdapterTrasladosDetalles.InsertCommand.Transaction = Trans
            Me.AdapterTrasladosDetalles.UpdateCommand.Transaction = Trans
            Me.AdapterTrasladosDetalles.DeleteCommand.Transaction = Trans

            Me.AdapterTraslados.Update(Me.DataSetTraslados1.Traslados)
            Me.AdapterTrasladosDetalles.Update(Me.DataSetTraslados1, "TrasladosDetalle")  '<<<<< se cambio . por ,
            Me.DataSetTraslados1.AcceptChanges()
            Trans.Commit()
            Return True
        Catch ex As Exception
            Trans.Rollback()

            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try
    End Function
    Function Guardar()
        Dim resp As Integer
        Dim id As String
        Try
            'se verifica si el traslado seleccionado esta mayorizado o no
            If buscando = True Then
                Dim Cx As New Conexion
                Dim valida As String
                Dim num_traslado As String = Me.txtNombre.Text
                valida = Cx.SlqExecuteScalar(Cx.Conectar("SeePos"), "SELECT Contabilizado FROM Traslados WHERE Id_Traslados= '" & num_traslado & "'")
                Cx.DesConectar(Cx.sQlconexion)
                If valida <> "" Then ' NO SE PARA QUE ES ESTA VARA PERO SI BIENE EN BLANCO QUE DESPICHA..
                    If valida = True Then ' se verifica si la factura seleccionada puede ser eliminada
                        MessageBox.Show("El traslado seleccionado no puede ser anulado, ya fue contabilizado....", "Informaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        Exit Function
                    End If
                End If
            End If
            '
            If Me.ValidarGuardar() Then
                resp = MessageBox.Show("�Deseas Guardar los cambios?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    'Termino la edicion
                    Me.BindingContext(Me.DataSetTraslados1, "Traslados").EndCurrentEdit()
                    Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").EndCurrentEdit()
                    If Registar() Then
                        'Para boton Nuevo
                        Me.ToolBarNuevo.Text = "Nuevo"
                        Me.ToolBarNuevo.ImageIndex = 0
                        'Para boton Acualizar
                        Me.ToolBarExcel.Text = "Editar"
                        Me.ToolBarExcel.ImageIndex = 5
                        Me.ToolBarNuevo.Enabled = True
                        Me.ToolBarExcel.Enabled = False
                        Me.ToolBarRegistrar.Enabled = False
                        Me.ToolBarBuscar.Enabled = True
                        Me.ToolBarImprimir.Enabled = False
                        Me.ToolBarEliminar.Enabled = False
                        Me.ToolBarCerrar.Enabled = True
                        id = Me.BindingContext(Me.DataSetTraslados1, "Traslados").Current("Id_Traslados")
                        Me.DataSetTraslados1.TrasladosDetalle.Clear()
                        Me.DataSetTraslados1.Traslados.Clear()
                        Me.InHabilitarControlesTodos()
                        '******************************************************************
                        '                          Control Hijos
                        '******************************************************************
                        '+++++++++++++++++++++++++InHabilitar botones Hospedaje+++++++++++
                        Me.NuevoTraslado.Enabled = False
                        Me.EditarTraslado.Enabled = False
                        Me.EliminarTraslado.Enabled = False
                        Me.GuardarTraslado.Enabled = False
                        'Control de icono
                        Me.NuevoTraslado.Text = "Nuevo"
                        Me.NuevoTraslado.ImageIndex = 0
                        Me.EditarTraslado.Text = "Editar"
                        Me.EditarTraslado.ImageIndex = 9
                        'Habilitar Grid
                        Me.GridControl1.Enabled = True
                        MsgBox("Traslado Relizado satisfactoriamente", MsgBoxStyle.Information)
                        If MsgBox("Deseas Imprimir el reporte de traslado", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                            Imprimir(id)
                        End If
                    End If
                Else
                    Me.BindingContext(Me.DataSetTraslados1, "Traslados").CancelCurrentEdit()
                    Me.DataSetTraslados1.RejectChanges()
                    'Para boton Nuevo
                    Me.ToolBarNuevo.Text = "Nuevo"
                    Me.ToolBarNuevo.ImageIndex = 0
                    'Para boton Acualizar
                    Me.ToolBarExcel.Text = "Editar"
                    Me.ToolBarExcel.ImageIndex = 5
                    Me.ToolBarNuevo.Enabled = True
                    Me.ToolBarExcel.Enabled = False
                    Me.ToolBarRegistrar.Enabled = False
                    Me.ToolBarBuscar.Enabled = True
                    Me.ToolBarImprimir.Enabled = False
                    Me.ToolBarEliminar.Enabled = False
                    Me.ToolBarCerrar.Enabled = True
                    Me.DataSetTraslados1.TrasladosDetalle.Clear()
                    Me.DataSetTraslados1.Traslados.Clear()
                    Me.InHabilitarControlesTodos()
                    '******************************************************************
                    '                          Control Hijos
                    '******************************************************************
                    '+++++++++++++++++++++++++InHabilitar botones Hospedaje+++++++++++
                    Me.NuevoTraslado.Enabled = False
                    Me.EditarTraslado.Enabled = False
                    Me.EliminarTraslado.Enabled = False
                    Me.GuardarTraslado.Enabled = False
                    'Control de icono
                    Me.NuevoTraslado.Text = "Nuevo"
                    Me.NuevoTraslado.ImageIndex = 0
                    Me.EditarTraslado.Text = "Editar"
                    Me.EditarTraslado.ImageIndex = 9
                    'Habilitar Grid
                    Me.GridControl1.Enabled = True
                End If
            End If
        Catch eEndEdit As System.Data.NoNullAllowedException
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
        End Try
    End Function

#End Region

#Region "Buscar"
    Function Buscar()
        buscando = True
        Dim cFunciones As New cFunciones
        Dim identificador As String = cFunciones.Buscar_X_Descripcion_Fecha("SELECT cast(Id_Traslados as varchar) as Id_Traslado,Descripcion, Fecha FROM Traslados order by Id_Traslados DESC", "Descripcion", "Fecha", "Buscar Traslado", Me.SqlConnection1.ConnectionString)
        If identificador = "" Then
            Exit Function
        Else
            Cargar(identificador)
            Me.ToolBarEliminar.Enabled = True
            Me.ToolBarExcel.Enabled = True
            Me.ToolBarImprimir.Enabled = True
            Me.ToolBarRegistrar.Enabled = False
        End If
    End Function

    Function Cargar(ByVal Cedula As String)
        Dim cFunciones As New cFunciones
        Me.DataSetTraslados1.TrasladosDetalle.Clear()
        Me.DataSetTraslados1.Traslados.Clear()
        cFunciones.Llenar_Tabla_Generico("Select * from Traslados Where Id_Traslados = " & Cedula, Me.DataSetTraslados1.Traslados, Me.SqlConnection1.ConnectionString)
        cFunciones.Llenar_Tabla_Generico("Select * from TrasladosDetalle Where Id_Traslados = " & Cedula, Me.DataSetTraslados1.TrasladosDetalle, Me.SqlConnection1.ConnectionString)
    End Function

#End Region

#Region "Editar"
    Function Editar()
        'If Me.ToolBarExcel.Text = "Editar" Then 'n si ya hay un registropendiente por agregar
        '    Try
        '        'Control de icono
        '        Me.ToolBarExcel.Text = "Cancelar"
        '        Me.ToolBarExcel.ImageIndex = 8
        '        Me.HabilitarControlesContrato()
        '        'controles del toolbar
        '        Me.ToolBarNuevo.Enabled = False
        '        Me.ToolBarRegistrar.Enabled = True
        '        Me.ToolBarExcel.Enabled = True
        '        Me.ToolBarBuscar.Enabled = False
        '        Me.ToolBarImprimir.Enabled = False
        '        Me.ToolBarEliminar.Enabled = False
        '        Me.ToolBarCerrar.Enabled = True
        '        '******************************************************************
        '        '                          Control Hijos
        '        '******************************************************************
        '        '+++++++++++++++++++++++++InHabilitar botones Hospedaje+++++++++++
        '        Me.NuevoHospedaje.Enabled = True
        '        Me.EditarHospedaje.Enabled = True
        '        Me.EliminarHospedaje.Enabled = True
        '        Me.GuardarHospedaje.Enabled = False
        '        'Control de icono
        '        Me.NuevoHospedaje.Text = "Nuevo"
        '        Me.NuevoHospedaje.ImageIndex = 0
        '        Me.EditarHospedaje.Text = "Editar"
        '        Me.EditarHospedaje.ImageIndex = 9
        '        'Habilitar Grid
        '        Me.GridControl1.Enabled = True
        '        '+++++++++++++++++++++++++InHabilitar botones Servicios+++++++++++
        '        Me.NuevoServicio.Enabled = True
        '        Me.EditarServicio.Enabled = True
        '        Me.EliminarServicio.Enabled = True
        '        Me.GuardarServicio.Enabled = False
        '        'Control de icono
        '        Me.NuevoServicio.Text = "Nuevo"
        '        Me.NuevoServicio.ImageIndex = 0
        '        Me.EditarServicio.Text = "Editar"
        '        Me.EditarServicio.ImageIndex = 9
        '        'Habilitar Grid
        '        Me.GridControl2.Enabled = True
        '        '+++++++++++++++++++++++++InHabilitar botones Paquetes+++++++++++
        '        Me.NuevoPaquete.Enabled = True
        '        Me.EditarPaquete.Enabled = True
        '        Me.EliminarPaquete.Enabled = True
        '        Me.GuardarPaquete.Enabled = False
        '        'Control de icono
        '        Me.NuevoPaquete.Text = "Nuevo"
        '        Me.NuevoPaquete.ImageIndex = 0
        '        Me.EditarPaquete.Text = "Editar"
        '        Me.EditarPaquete.ImageIndex = 9

        '        'si son otras temporadas
        '        If RadioButton2.Checked = True Then
        '            ComboBox7.Enabled = False
        '        End If


        '        'Habilitar 
        '        Me.GridControl3.Enabled = True
        '        Me.txtNombre.Focus()
        '    Catch eEndEdit As System.Data.NoNullAllowedException
        '        System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
        '    End Try

        'Else

        '    Try
        '        'cocntrol de icono
        '        Me.ToolBarExcel.Text = "Editar"
        '        Me.ToolBarExcel.ImageIndex = 5
        '        'terminar la edicion
        '        Me.BindingContext(Me.DataSetContratos1, "Contrato").CancelCurrentEdit()
        '        'limpiar las tablas
        '        Me.DataSetContratos1.Detalle_Contrato_Hospedaje.Clear()
        '        Me.DataSetContratos1.Detalle_Contrato_Servicio.Clear()
        '        Me.DataSetContratos1.Detalle_Contrato_Paquete.Clear()
        '        Me.DataSetContratos1.Contrato.Clear()
        '        'control del toobar
        '        Me.ToolBarNuevo.Enabled = True
        '        Me.ToolBarExcel.Enabled = True
        '        Me.ToolBarRegistrar.Enabled = False
        '        Me.ToolBarBuscar.Enabled = True
        '        Me.ToolBarImprimir.Enabled = False
        '        Me.ToolBarEliminar.Enabled = False
        '        Me.ToolBarCerrar.Enabled = True
        '        'Inhabilitar todos los controles
        '        Me.InHabilitarControlesTodos()
        '        '******************************************************************
        '        '                          Control Hijos
        '        '******************************************************************
        '        '+++++++++++++++++++++++++InHabilitar botones Hospedaje+++++++++++
        '        Me.NuevoHospedaje.Enabled = False
        '        Me.EditarHospedaje.Enabled = False
        '        Me.EliminarHospedaje.Enabled = False
        '        Me.GuardarHospedaje.Enabled = False
        '        'Control de icono
        '        Me.NuevoHospedaje.Text = "Nuevo"
        '        Me.NuevoHospedaje.ImageIndex = 0
        '        Me.EditarHospedaje.Text = "Editar"
        '        Me.EditarHospedaje.ImageIndex = 9
        '        'Habilitar Grid
        '        Me.GridControl1.Enabled = True
        '        '+++++++++++++++++++++++++InHabilitar botones Servicios+++++++++++
        '        Me.NuevoServicio.Enabled = False
        '        Me.EditarServicio.Enabled = False
        '        Me.EliminarServicio.Enabled = False
        '        Me.GuardarServicio.Enabled = False
        '        'Control de icono
        '        Me.NuevoServicio.Text = "Nuevo"
        '        Me.NuevoServicio.ImageIndex = 0
        '        Me.EditarServicio.Text = "Editar"
        '        Me.EditarServicio.ImageIndex = 9
        '        'Habilitar Grid
        '        Me.GridControl2.Enabled = True
        '        '+++++++++++++++++++++++++InHabilitar botones Paquetes+++++++++++
        '        Me.NuevoPaquete.Enabled = False
        '        Me.EditarPaquete.Enabled = False
        '        Me.EliminarPaquete.Enabled = False
        '        Me.GuardarPaquete.Enabled = False
        '        'Control de icono
        '        Me.NuevoPaquete.Text = "Nuevo"
        '        Me.NuevoPaquete.ImageIndex = 0
        '        Me.EditarPaquete.Text = "Editar"
        '        Me.EditarPaquete.ImageIndex = 9
        '        'Habilitar Grid
        '        Me.GridControl3.Enabled = True
        '    Catch eEndEdit As System.Data.NoNullAllowedException
        '        System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
        '    End Try
        'End If
    End Function
#End Region

#Region "Salir"
    Function Salir()
        Me.Close()
    End Function
#End Region

#Region "Terminar Edicion Contrato"
    Private Sub ComboBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox2.KeyDown
        If e.KeyCode = Keys.Enter Then
            'si son otras temporadas
            If Me.TxtDescripcion.Text = "" Then
                MsgBox("Debes escribir la descripcion", MsgBoxStyle.Critical)
                Exit Sub
            End If
            If Me.ComboBox1.SelectedValue = Me.ComboBox2.SelectedValue Then
                MsgBox("Las bodegas no deben ser las mismas", MsgBoxStyle.Critical)
                Exit Sub
            End If
            Me.BindingContext(Me.DataSetTraslados1, "Traslados").EndCurrentEdit()
            Me.ComboBox1.Enabled = False
            Me.ComboBox2.Enabled = False
            '     Me.HabilitarControlesDetalles()
            'Si se esta haciendo una edicion
            If Me.ToolBarNuevo.Text = "Cancelar" Then
                Me.NuevoTraslado.Enabled = True
                Me.EditarTraslado.Enabled = True
                Me.EliminarTraslado.Enabled = True
                Me.GuardarTraslado.Enabled = False
            End If
            Me.NuevoTraslado.Focus()
        End If
    End Sub
#End Region

#Region "Control Focus"

#Region "Focus Traslados"

    Private Sub TxtDescripcion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtDescripcion.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBox1.Focus()
        End If
    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBox2.Focus()
        End If
    End Sub
#End Region

#Region "Focus Contratos Hospedaje"

    'Private Sub ComboTipoHabitacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboTipoHabitacion.KeyDown
    '    Try
    '        If e.KeyCode = Keys.Enter Then
    '            ComboBox2.Focus()
    '        End If

    '        If e.KeyCode = Keys.F1 Then
    '            Dim Fx As New cFunciones
    '            Dim valor As String
    '            valor = Fx.BuscarDatos("SELECT Tipo_Habitacion.Codigo, Tipo_Habitacion.Descripcion  FROM Tipo_Habitacion", "Tipo_Habitacion.Descripcion", "Buscar Categoria...")
    '            If valor = "" Then
    '                'Me.ComboTipoHabitacion.SelectedIndex = -1
    '            Else
    '                Me.ComboTipoHabitacion.SelectedValue = valor
    '            End If
    '        End If

    '    Catch ex As SystemException
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub

    'Private Sub ComboBox3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox3.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        TextEdit3.Focus()
    '    End If
    'End Sub

    'Private Sub TextEdit3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextEdit3.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        TextSingle.Focus()
    '    End If
    'End Sub

    'Private Sub TextSingle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextSingle.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        TextEdit1.Focus()
    '    End If
    'End Sub

    'Private Sub TextEdit1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextEdit1.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        TextEdit4.Focus()
    '    End If
    'End Sub

    'Private Sub TextEdit4_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextEdit4.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        TextEdit6.Focus()
    '    End If
    'End Sub

    'Private Sub TextEdit6_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextEdit6.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        TextEdit8.Focus()
    '    End If
    'End Sub

    'Private Sub TextEdit8_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextEdit8.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        TextEdit10.Focus()
    '    End If
    'End Sub

    'Private Sub TextEdit10_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextEdit10.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        Me.GuardarHospedaje.Focus()
    '    End If
    'End Sub

#End Region

#End Region

#Region "Detalle Traslado"

#Region "Nuevo Detalle Traslado"
    Private Sub NuevoTraslado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoTraslado.Click
        If Me.NuevoTraslado.Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
            Try 'Inicia la edicion
                'Control de Icono
                Me.NuevoTraslado.Text = "Cancelar"
                Me.NuevoTraslado.ImageIndex = 4
                'Iniciar la edicion
                Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").EndCurrentEdit()
                Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").AddNew()
                'Inhabilitar Grid
                Me.GridControl1.Enabled = False
                'Inhabilitar Botones Relacionados
                Me.EliminarTraslado.Enabled = False
                Me.EditarTraslado.Enabled = False
                'Habilitar Botones Relacionados 
                Me.GuardarTraslado.Enabled = True
                Me.NuevoTraslado.Enabled = True
                'Habilitar Controles
                Me.HabilitarControlesDetalles()
                'Focus al control deseado
                Me.TextBox3.Focus()
            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        Else
            Try
                'Control de icono
                Me.NuevoTraslado.Text = "Nuevo"
                Me.NuevoTraslado.ImageIndex = 0
                'Terminar la edicion
                Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").CancelCurrentEdit()
                'Habilitar el grid
                Me.GridControl1.Enabled = True
                'Habilitar botones relacionados
                Me.EliminarTraslado.Enabled = True
                Me.EditarTraslado.Enabled = True
                Me.NuevoTraslado.Enabled = True
                'Inhabilitar botones relacionados
                Me.GuardarTraslado.Enabled = False
                'Inhabilitar controles
                Me.InHabilitarControlesDetalles()
            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        End If
    End Sub

    Private Function CargarInformacionArticulo(ByVal codigo As String) As Boolean
        Dim rs As SqlDataReader
        Dim Encontrado As Boolean
        If codigo <> Nothing Then
            sqlConexion = CConexion.Conectar()
            rs = CConexion.GetRecorset(sqlConexion, "SELECT Inventario.Codigo, Inventario.Descripcion AS Descripcion, ArticulosXBodega.Existencia,Inventario.Fletes,Inventario.OtrosCargos, Inventario.Barras,Inventario.Costo FROM Inventario INNER JOIN  ArticulosXBodega ON Inventario.Codigo = ArticulosXBodega.Codigo WHERE (Inhabilitado = 0)and (ArticulosXBodega.IdBodega = " & Me.BindingContext(Me.DataSetTraslados1, "BodegaOrigen").Current("IdBodega") & " ) and (Inventario.Codigo ='" & codigo & "') or (Inventario.Barras = '" & codigo & "') and (Inhabilitado = 0)and (ArticulosXBodega.IdBodega = " & Me.BindingContext(Me.DataSetTraslados1, "BodegaOrigen").Current("IdBodega") & " )")
            Encontrado = False

            While rs.Read
                Try
                    Encontrado = True
                    TextBox3.Text = rs("Codigo")
                    TextBox4.Text = rs("Descripcion")
                    TextBox5.Text = rs("Existencia")
                    txtBarras.Text = rs("Barras")
                    '------------------------------
                    '-Estos componentes se encuentran ocultos en el formulario
                    txtPrecioBase.Text = rs("Costo")
                    txtFletes.Text = rs("Fletes")
                    txtOtrosCargos.Text = rs("OtrosCargos")
                    '--------------------------------

                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    CConexion.DesConectar(CConexion.Conectar(strModulo))
                End Try
            End While
            rs.Close()

            If Not Encontrado Then
                MsgBox("No existe o est� inhabilitado un art�culo con este c�digo", MsgBoxStyle.Exclamation)
                GridControl1.Enabled = False
                Me.TextBox3.Focus()
                CConexion.DesConectar(CConexion.Conectar(strModulo))
                Return False
            End If
            CConexion.DesConectar(CConexion.Conectar(strModulo))
            Return True
        End If
    End Function
#End Region

#Region "Guardar Detalle Traslado"
    Private Sub Guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarTraslado.Click
        'Validar controles
        Try
            If Me.ValidarDetalleHospedaje() Then
                'Terminar edicion 
                Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").EndCurrentEdit()
                'Control de icono
                Me.NuevoTraslado.Text = "Nuevo"
                Me.NuevoTraslado.ImageIndex = 0
                Me.EditarTraslado.Text = "Editar"
                Me.EditarTraslado.ImageIndex = 9
                'Habilitar Grid
                Me.GridControl1.Enabled = True
                'Habilitar botones
                Me.NuevoTraslado.Enabled = True
                Me.EditarTraslado.Enabled = True
                Me.EliminarTraslado.Enabled = True
                'Inhabilitar botones 
                Me.GuardarTraslado.Enabled = False
                'Inhabilitar Controles
                Me.InHabilitarControlesDetalles()
                'Focus Control
                Me.NuevoTraslado.Focus()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
#End Region

#Region "Eliminar Detalle Traslado"

    Private Sub EliminarHospedaje_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarTraslado.Click
        EliminarDetalleHospedaje()
    End Sub

    Function EliminarDetalleHospedaje()
        If Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").Count > 0 Then
            Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").RemoveAt(Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").Position)
        End If
    End Function
#End Region

#Region "Editar Detalle Traslado"

    Private Sub EditarHospedaje_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditarTraslado.Click
        ' si hay detalles
        If Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").Count > 0 Then
            If Me.EditarTraslado.Text = "Editar" Then 'n si ya hay un registropendiente por agregar
                Try 'Inicia la edicion
                    'Control de Icono
                    Me.EditarTraslado.Text = "Cancelar"
                    Me.EditarTraslado.ImageIndex = 4
                    'Inhabilitar Grid
                    Me.GridControl1.Enabled = False
                    'Inhabilitar Botones Relacionados
                    Me.EliminarTraslado.Enabled = False
                    Me.NuevoTraslado.Enabled = False
                    'Habilitar Botones Relacionados 
                    Me.GuardarTraslado.Enabled = True
                    Me.EditarTraslado.Enabled = True
                    'Habilitar Controles
                    Me.HabilitarControlesDetalles()
                    'Focus al control deseado
                    Me.TextBox3.Focus()
                Catch eEndEdit As System.Data.NoNullAllowedException
                    System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                End Try
            Else
                Try
                    'Control de icono
                    Me.EditarTraslado.Text = "Editar"
                    Me.EditarTraslado.ImageIndex = 9
                    'Terminar la edicion
                    Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").CancelCurrentEdit()
                    'Habilitar el grid
                    Me.GridControl1.Enabled = True
                    'Habilitar botones relacionados
                    Me.EliminarTraslado.Enabled = True
                    Me.EditarTraslado.Enabled = True
                    Me.NuevoTraslado.Enabled = True
                    'Inhabilitar botones relacionados
                    Me.GuardarTraslado.Enabled = False
                    'Inhabilitar controles
                    Me.InHabilitarControlesDetalles()
                Catch eEndEdit As System.Data.NoNullAllowedException
                    System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                End Try
            End If
        End If
    End Sub
#End Region

#End Region

#Region "Validar"
    Function ValidarDetalleHospedaje() As Boolean



        'Si es una edicion Buscar que el articulo exista
        If Me.EditarTraslado.Text = "Cancelar" Then
            If Not CargarInformacionArticulo(Me.TextBox3.Text) Then
                Exit Function
            End If
        End If

        If Me.TextBox3.Text = "" Then
            Return False
        Else
            'Validar que ese codigo no alla sido ingresado anteriormente
            Dim rows() As DataRow
            'Si es editar
            If Me.EditarTraslado.Text = "Cancelar" Then
                rows = Me.DataSetTraslados1.TrasladosDetalle.Select("Codigo = " & Me.TextBox3.Text & " and codigo <> " & Me.BindingContext(Me.DataSetTraslados1, "Traslados.TrasladosTrasladosDetalle").Current("Codigo"))
            Else
                rows = Me.DataSetTraslados1.TrasladosDetalle.Select("Codigo = " & Me.TextBox3.Text)
            End If

            If rows.Length > 0 Then
                MsgBox("Ya se ingreso un traslado de ese item", MsgBoxStyle.Critical)
                Me.TextBox3.Focus()
                Return False
            End If
        End If

        If TextBox5.Text <> "" And TextBox6.Text <> "" Then
            If CDbl(TextBox6.Text) = 0 Then
                MsgBox("La cantidad no puede ser 0", MsgBoxStyle.Critical)
                TextBox6.Focus()
                Return False
            End If
            If CDbl(TextBox5.Text) = 0 Then
                MsgBox("No hay existencias de este art�culo", MsgBoxStyle.Critical)
                TextBox3.Focus()
                Return False
            End If
            If CDbl(TextBox5.Text) - CDbl(TextBox6.Text) < 0 Then
                MsgBox("La cantidad es mayor a la existencia", MsgBoxStyle.Critical)
                TextBox6.Focus()
                Return False
            End If

        End If
        Return True
    End Function
#End Region

#Region "KeyDown"
    Private Sub TextBox3_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox3.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim buscador As New FrmBuscarArticulo
            buscador.StartPosition = FormStartPosition.CenterParent
            buscador.Id_Bodega = Me.BindingContext(DataSetTraslados1, "BodegaOrigen").Current("IdBodega")
            buscador.ShowDialog()
            If buscador.Cancelado Or buscador.Codigo = 0 Then
                Exit Sub
            End If
            TextBox3.Text = buscador.Codigo
            buscador.Dispose()
            If CargarInformacionArticulo(Me.TextBox3.Text) Then
                Me.TextBox6.Focus()
            Else
                Me.TextBox3.Focus()
            End If
        End If

        If e.KeyCode = Keys.Enter Then
            If Me.TextBox3.Text <> "" Then
                If CargarInformacionArticulo(Me.TextBox3.Text) Then
                    Me.TextBox6.Focus()
                Else
                    Me.TextBox3.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub TextBox6_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox6.KeyDown
        If e.KeyCode = Keys.Enter Then
            GuardarTraslado.Focus()
        End If
    End Sub


    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub TextBox6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox6.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub
#End Region

End Class
