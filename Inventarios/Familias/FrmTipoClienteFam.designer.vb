﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTipoClienteFam
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTipoClienteFam))
        Me.dgvTipoCliente = New System.Windows.Forms.DataGridView()
        Me.NombreTipoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PorcentajeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.enlaceTipoCliente = New System.Windows.Forms.BindingSource(Me.components)
        Me.datos = New LcPymes_5._2.SubFamiliaTipoCliente()
        Me.btnCerrar = New System.Windows.Forms.Button()
        CType(Me.dgvTipoCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.enlaceTipoCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.datos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvTipoCliente
        '
        Me.dgvTipoCliente.AllowUserToAddRows = False
        Me.dgvTipoCliente.AllowUserToDeleteRows = False
        Me.dgvTipoCliente.AllowUserToResizeColumns = False
        Me.dgvTipoCliente.AllowUserToResizeRows = False
        Me.dgvTipoCliente.AutoGenerateColumns = False
        Me.dgvTipoCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTipoCliente.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTipoCliente.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTipoCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvTipoCliente.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NombreTipoDataGridViewTextBoxColumn, Me.PorcentajeDataGridViewTextBoxColumn})
        Me.dgvTipoCliente.DataSource = Me.enlaceTipoCliente
        Me.dgvTipoCliente.Location = New System.Drawing.Point(12, 12)
        Me.dgvTipoCliente.Name = "dgvTipoCliente"
        Me.dgvTipoCliente.RowHeadersVisible = False
        Me.dgvTipoCliente.Size = New System.Drawing.Size(425, 218)
        Me.dgvTipoCliente.TabIndex = 0
        '
        'NombreTipoDataGridViewTextBoxColumn
        '
        Me.NombreTipoDataGridViewTextBoxColumn.DataPropertyName = "NombreTipo"
        Me.NombreTipoDataGridViewTextBoxColumn.HeaderText = "Tipo"
        Me.NombreTipoDataGridViewTextBoxColumn.Name = "NombreTipoDataGridViewTextBoxColumn"
        '
        'PorcentajeDataGridViewTextBoxColumn
        '
        Me.PorcentajeDataGridViewTextBoxColumn.DataPropertyName = "porcentaje"
        Me.PorcentajeDataGridViewTextBoxColumn.HeaderText = "% IVA"
        Me.PorcentajeDataGridViewTextBoxColumn.Name = "PorcentajeDataGridViewTextBoxColumn"
        '
        'enlaceTipoCliente
        '
        Me.enlaceTipoCliente.DataMember = "SubFamiliasTipoClientes"
        Me.enlaceTipoCliente.DataSource = Me.datos
        '
        'datos
        '
        Me.datos.DataSetName = "SubFamiliaTipoCliente"
        Me.datos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnCerrar
        '
        Me.btnCerrar.BackColor = System.Drawing.Color.FromArgb(CType(CType(90, Byte), Integer), CType(CType(112, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.btnCerrar.FlatAppearance.BorderSize = 0
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCerrar.ForeColor = System.Drawing.Color.White
        Me.btnCerrar.Image = Global.LcPymes_5._2.My.Resources.Resources.Guardar_sincolor
        Me.btnCerrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCerrar.Location = New System.Drawing.Point(177, 245)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(78, 59)
        Me.btnCerrar.TabIndex = 1
        Me.btnCerrar.Text = "Guardar"
        Me.btnCerrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCerrar.UseVisualStyleBackColor = False
        '
        'FrmTipoClienteFam
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(449, 316)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.dgvTipoCliente)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmTipoClienteFam"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tipo de Cliente"
        CType(Me.dgvTipoCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.enlaceTipoCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.datos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvTipoCliente As Windows.Forms.DataGridView
    Friend WithEvents btnCerrar As Windows.Forms.Button
    Friend WithEvents datos As SubFamiliaTipoCliente
    Friend WithEvents enlaceTipoCliente As Windows.Forms.BindingSource
    Friend WithEvents NombreTipoDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajeDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
End Class
