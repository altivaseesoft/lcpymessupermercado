﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ImpuestoProducto
    Public Shared Function Obtener(IdCliente As String, IdInventario As String) As Double

        Dim porcentaje As Double = 0
        Dim cd As New SqlCommand

        cd.CommandText = "SELECT cedula, nombre, abierto,  Clientes.idTipoCliente, TipoCliente.porcentajeIVA, max_credito,
                        Plazo_credito, descuento, tipoprecio, sinrestriccion, identificacion, CodMonedaCredito 
                        FROM Clientes join TipoCliente on Clientes.idTipoCliente=TipoCliente.id
                        where identificacion = @IdCliente"
        cd.Parameters.AddWithValue("@IdCliente", IdCliente)
        Dim dtCliente As New DataTable
        cFunciones.CargarTablaSqlCommand(cd, dtCliente, GetSetting("SeeSoft", "SeePos", "Conexion"))
        cd.CommandText = "SELECT Max_Descuento, Precio_Promo, Promo_Activa, SubFamilia, Existencia, PrecioBase, Fletes, OtrosCargos, Costo, MonedaCosto, MonedaVenta, Precio_A, Precio_B, Precio_C, Precio_D, Cabys.porcentajeIVA 
                FROM Inventario INNER JOIN Presentaciones 
                ON Presentaciones.CodPres = Inventario.CodPresentacion and (Inhabilitado = 0) and (Barras = @Codigo)
                join SubFamilias on Inventario.SubFamilia=SubFamilias.Codigo
                join Cabys on SubFamilias.idCabys=Cabys.id"
        cd.Parameters.AddWithValue("@Codigo", IdInventario)
        Dim dtInventario As New DataTable
        cFunciones.CargarTablaSqlCommand(cd, dtInventario, GetSetting("SeeSoft", "SeePos", "Conexion"))
        If dtInventario.Rows.Count = 0 Then
            'Si no lo encuentra como barras que lo busque como codigo interno
            cd.CommandText = "SELECT Max_Descuento, Precio_Promo, Promo_Activa, SubFamilia, Existencia, PrecioBase, Fletes, OtrosCargos, Costo, MonedaCosto, MonedaVenta, Precio_A, Precio_B, Precio_C, Precio_D, Cabys.porcentajeIVA 
                FROM Inventario INNER JOIN Presentaciones 
                ON Presentaciones.CodPres = Inventario.CodPresentacion and (Inhabilitado = 0) and (Codigo = @Codigo)
                join SubFamilias on Inventario.SubFamilia=SubFamilias.Codigo
                join Cabys on SubFamilias.idCabys=Cabys.id"
            cFunciones.CargarTablaSqlCommand(cd, dtInventario, GetSetting("SeeSoft", "SeePos", "Conexion"))
        End If


        If dtInventario.Rows.Count > 0 And dtCliente.Rows.Count > 0 Then
            cd.CommandText = "SELECT * From SubFamiliasTipoClientes WHERE SubFamiliaId = @SubFamilia AND TipoClienteID = @TipoCliente "
            cd.Parameters.AddWithValue("@SubFamilia", dtInventario.Rows(0).Item("SubFamilia"))
            cd.Parameters.AddWithValue("@TipoCliente", dtCliente.Rows(0).Item("idTipoCliente"))
            Dim dtTipoClienteSubFamilia As New DataTable
            cFunciones.CargarTablaSqlCommand(cd, dtTipoClienteSubFamilia, GetSetting("SeeSoft", "SeePos", "Conexion"))
            If dtTipoClienteSubFamilia.Rows.Count > 0 Then
                Return dtTipoClienteSubFamilia.Rows(0).Item("porcentaje")
            End If
            If dtCliente.Rows(0).Item("porcentajeIVA") < dtInventario.Rows(0).Item("porcentajeIVA") Then
                Return dtCliente.Rows(0).Item("porcentajeIVA")
            End If
            Return dtInventario.Rows(0).Item("porcentajeIVA")
        Else
            If dtCliente.Rows.Count > 0 Then
                Return dtCliente.Rows(0).Item("porcentajeIVA")
            End If
            If dtInventario.Rows.Count > 0 Then
                Return dtInventario.Rows(0).Item("porcentajeIVA")
            End If

            Return 13
        End If
    End Function

End Class
