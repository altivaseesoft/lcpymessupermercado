﻿Imports System.Windows.Forms

Public Class FrmTipoClienteFam
    Public codigoSubFamilia As String
    Public Modelo As SubFamiliaTipoClienteModelo
    Private Sub FrmTipoCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Modelo = New SubFamiliaTipoClienteModelo(codigoSubFamilia
                                                 )
        enlaceTipoCliente.DataSource = Modelo
        enlaceTipoCliente.DataMember = Modelo.SubFamiliasTipoClientes.TableName

    End Sub


    Private Sub dgvTipoCliente_CellValidating(ByVal sender As Object, ByVal e As DataGridViewCellValidatingEventArgs) Handles dgvTipoCliente.CellValidating
        If e.ColumnIndex = 2 Then
            If String.IsNullOrEmpty(e.FormattedValue) Or e.FormattedValue.length > 2 Then
                MsgBox("Debe ingresar un porcentaje valido.", MsgBoxStyle.Exclamation)
                e.Cancel = True
            End If
        End If
    End Sub
    Private Sub dgvTipoCliente_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvTipoCliente.EditingControlShowing
        RemoveHandler e.Control.KeyPress, AddressOf typeOnlynumbers
        AddHandler e.Control.KeyPress, AddressOf typeOnlynumbers
    End Sub
    Sub typeOnlynumbers(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) >= 33 And Asc(e.KeyChar) <= 47 Or
            Asc(e.KeyChar) >= 58 Then
            e.Handled = True
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Guardar()
    End Sub
    Private Sub Guardar()
        Try
            Modelo.Guardar()
            Close()
        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.OkOnly)

        End Try



    End Sub

End Class