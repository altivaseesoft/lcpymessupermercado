﻿Imports System.Data.SqlClient
Public Class SubFamiliaTipoClienteModelo : Inherits SubFamiliaTipoCliente
    Public Sub New()

    End Sub
    Public Sub New(CodigoSubFamilia As String)
        consultarIdSubFamilia(CodigoSubFamilia)
        If SubFamiliasTipoClientes.Count = 0 Then
            crearLineas(CodigoSubFamilia)
            consultarIdSubFamilia(CodigoSubFamilia)
        End If
    End Sub
    Private Sub consultarIdSubFamilia(CodigoSubFamilia As String)
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT st.IdSubFamiliaTipoCliente, st.SubFamiliaId, st.TipoClienteId, st.porcentaje, t.descripcion AS NombreTipo FROM dbo.SubFamiliasTipoClientes AS st INNER JOIN  dbo.TipoCliente AS t ON st.TipoClienteId = t.id WHERE (st.SubFamiliaId = @Id)"
        cmd.Parameters.AddWithValue("@Id", CodigoSubFamilia)
        cFunciones.CargarTablaSqlCommand(cmd, Me.SubFamiliasTipoClientes, GetSetting("SeeSoft", "SeePos", "Conexion"))

    End Sub
    Private Sub crearLineas(CodigoSubFamilia As String)
        Dim cmd As New SqlCommand
        cmd.CommandText = "INSERT INTO [dbo].[SubFamiliasTipoClientes] ([SubFamiliaId], [TipoClienteId],[porcentaje]) " &
                           " Select @SubFamiliaID, TipoCliente.id, TipoCliente.porcentajeIVA From TipoCliente "
        cmd.Parameters.AddWithValue("@SubFamiliaID", CodigoSubFamilia)
        cFunciones.spEjecutar(cmd, GetSetting("SeeSoft", "SeePos", "Conexion"))


    End Sub
    Public Sub Guardar()
        Dim adp As New SubFamiliaTipoClienteTableAdapters.SubFamiliasTipoClientesTableAdapter
        adp.Connection.ConnectionString = GetSetting("Seesoft", "SeePos", "Conexion")
        adp.Update(Me.SubFamiliasTipoClientes)
    End Sub
End Class
