﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBuscarCabys
    Inherits System.Windows.Forms.Form

    Public idCabys As Integer
    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
		Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
		Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
		Me.dgvBusqueda = New System.Windows.Forms.DataGridView()
		Me.CodigoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.PorcentajeIVADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.bsBusquedaCabys = New System.Windows.Forms.BindingSource(Me.components)
		Me.DataSetFamilia = New LcPymes_5._2.DataSetFamilia()
		Me.CabysTableAdapter = New LcPymes_5._2.DataSetFamiliaTableAdapters.CabysTableAdapter()
		Me.txtBuscarDato = New System.Windows.Forms.TextBox()
		Me.btnAceptar = New System.Windows.Forms.Button()
		Me.btnCancelar = New System.Windows.Forms.Button()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.lbIdCabys = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		CType(Me.dgvBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.bsBusquedaCabys, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.DataSetFamilia, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'dgvBusqueda
		'
		Me.dgvBusqueda.AllowUserToAddRows = False
		Me.dgvBusqueda.AllowUserToDeleteRows = False
		Me.dgvBusqueda.AllowUserToResizeColumns = False
		Me.dgvBusqueda.AllowUserToResizeRows = False
		Me.dgvBusqueda.AutoGenerateColumns = False
		Me.dgvBusqueda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
		Me.dgvBusqueda.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
		DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
		DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
		DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
		DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
		DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
		DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
		Me.dgvBusqueda.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
		Me.dgvBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
		Me.dgvBusqueda.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CodigoDataGridViewTextBoxColumn, Me.DescripcionDataGridViewTextBoxColumn, Me.PorcentajeIVADataGridViewTextBoxColumn})
		Me.dgvBusqueda.DataSource = Me.bsBusquedaCabys
		Me.dgvBusqueda.Location = New System.Drawing.Point(12, 64)
		Me.dgvBusqueda.Name = "dgvBusqueda"
		Me.dgvBusqueda.ReadOnly = True
		Me.dgvBusqueda.RowHeadersVisible = False
		Me.dgvBusqueda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
		Me.dgvBusqueda.Size = New System.Drawing.Size(957, 236)
		Me.dgvBusqueda.TabIndex = 0
		'
		'CodigoDataGridViewTextBoxColumn
		'
		Me.CodigoDataGridViewTextBoxColumn.DataPropertyName = "codigo"
		DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
		Me.CodigoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
		Me.CodigoDataGridViewTextBoxColumn.FillWeight = 35.0!
		Me.CodigoDataGridViewTextBoxColumn.HeaderText = "Código"
		Me.CodigoDataGridViewTextBoxColumn.Name = "CodigoDataGridViewTextBoxColumn"
		Me.CodigoDataGridViewTextBoxColumn.ReadOnly = True
		Me.CodigoDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
		'
		'DescripcionDataGridViewTextBoxColumn
		'
		Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "Descripcion"
		Me.DescripcionDataGridViewTextBoxColumn.FillWeight = 150.0!
		Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Descripción"
		Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
		Me.DescripcionDataGridViewTextBoxColumn.ReadOnly = True
		Me.DescripcionDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
		'
		'PorcentajeIVADataGridViewTextBoxColumn
		'
		Me.PorcentajeIVADataGridViewTextBoxColumn.DataPropertyName = "porcentajeIVA"
		DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
		Me.PorcentajeIVADataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
		Me.PorcentajeIVADataGridViewTextBoxColumn.FillWeight = 30.0!
		Me.PorcentajeIVADataGridViewTextBoxColumn.HeaderText = "Porcentaje IVA"
		Me.PorcentajeIVADataGridViewTextBoxColumn.Name = "PorcentajeIVADataGridViewTextBoxColumn"
		Me.PorcentajeIVADataGridViewTextBoxColumn.ReadOnly = True
		Me.PorcentajeIVADataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
		'
		'bsBusquedaCabys
		'
		Me.bsBusquedaCabys.DataMember = "Cabys"
		Me.bsBusquedaCabys.DataSource = Me.DataSetFamilia
		'
		'DataSetFamilia
		'
		Me.DataSetFamilia.DataSetName = "DataSetFamilia"
		Me.DataSetFamilia.Locale = New System.Globalization.CultureInfo("es-MX")
		Me.DataSetFamilia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'CabysTableAdapter
		'
		Me.CabysTableAdapter.ClearBeforeFill = True
		'
		'txtBuscarDato
		'
		Me.txtBuscarDato.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtBuscarDato.Location = New System.Drawing.Point(125, 25)
		Me.txtBuscarDato.MaximumSize = New System.Drawing.Size(490, 29)
		Me.txtBuscarDato.MinimumSize = New System.Drawing.Size(490, 25)
		Me.txtBuscarDato.Name = "txtBuscarDato"
		Me.txtBuscarDato.Size = New System.Drawing.Size(490, 23)
		Me.txtBuscarDato.TabIndex = 2
		'
		'btnAceptar
		'
		Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnAceptar.ForeColor = System.Drawing.SystemColors.MenuText
		Me.btnAceptar.Location = New System.Drawing.Point(37, 317)
		Me.btnAceptar.Name = "btnAceptar"
		Me.btnAceptar.Size = New System.Drawing.Size(109, 29)
		Me.btnAceptar.TabIndex = 3
		Me.btnAceptar.Text = "Aceptar"
		Me.btnAceptar.UseVisualStyleBackColor = True
		'
		'btnCancelar
		'
		Me.btnCancelar.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnCancelar.ForeColor = System.Drawing.SystemColors.MenuText
		Me.btnCancelar.Location = New System.Drawing.Point(152, 317)
		Me.btnCancelar.Name = "btnCancelar"
		Me.btnCancelar.Size = New System.Drawing.Size(109, 29)
		Me.btnCancelar.TabIndex = 4
		Me.btnCancelar.Text = "Cancelar"
		Me.btnCancelar.UseVisualStyleBackColor = True
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(452, 323)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(157, 17)
		Me.Label1.TabIndex = 6
		Me.Label1.Text = "Código Selecionado:"
		'
		'lbIdCabys
		'
		Me.lbIdCabys.AutoSize = True
		Me.lbIdCabys.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbIdCabys.Location = New System.Drawing.Point(615, 323)
		Me.lbIdCabys.Name = "lbIdCabys"
		Me.lbIdCabys.Size = New System.Drawing.Size(17, 17)
		Me.lbIdCabys.TabIndex = 7
		Me.lbIdCabys.Text = "0"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(34, 28)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(85, 17)
		Me.Label2.TabIndex = 8
		Me.Label2.Text = "Busqueda:"
		'
		'FrmBuscarCabys
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.ClientSize = New System.Drawing.Size(981, 377)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.lbIdCabys)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.btnCancelar)
		Me.Controls.Add(Me.btnAceptar)
		Me.Controls.Add(Me.txtBuscarDato)
		Me.Controls.Add(Me.dgvBusqueda)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "FrmBuscarCabys"
		Me.ShowIcon = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Buscar Cabys"
		Me.TransparencyKey = System.Drawing.Color.Maroon
		CType(Me.dgvBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.bsBusquedaCabys, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.DataSetFamilia, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents dgvBusqueda As Windows.Forms.DataGridView
    Friend WithEvents bsBusquedaCabys As Windows.Forms.BindingSource
    Friend WithEvents DataSetFamilia As DataSetFamilia
    Friend WithEvents CabysTableAdapter As DataSetFamiliaTableAdapters.CabysTableAdapter
    Friend WithEvents btnAceptar As Windows.Forms.Button
    Friend WithEvents btnCancelar As Windows.Forms.Button
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents lbIdCabys As Windows.Forms.Label
    Friend WithEvents Label2 As Windows.Forms.Label
    Public WithEvents txtBuscarDato As Windows.Forms.TextBox
    Friend WithEvents CodigoDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajeIVADataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
End Class
