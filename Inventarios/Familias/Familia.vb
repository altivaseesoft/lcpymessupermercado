Imports System.Data.SqlClient
Imports System.Data
Imports System.Windows.Forms
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo

Public Class Familia
    Inherits FrmPlantilla
    Dim NuevaConexion As String
    Dim usuario As Usuario_Logeado
    Friend WithEvents cbActividadEconomica As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents bsActividadEconomica As BindingSource
    Friend WithEvents TB_CE_ActividadEconomicaTableAdapter As DataSetFamiliaTableAdapters.TB_CE_ActividadEconomicaTableAdapter
    Friend WithEvents btnGuardarSubUbicacion As Button
    Friend WithEvents btnNuevaSubFamilia As Button
    Friend WithEvents btnGuardarFamilia As Button
    Friend WithEvents TxtCodigo As Label
    Friend WithEvents txtCodigoCabys As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents colCabys As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label8 As Label
    Friend WithEvents txtDescripcionCabys As TextBox
    Friend WithEvents BtnBuscarCabys As Button
    Friend WithEvents SqlInsertCommand As SqlCommand
    Friend WithEvents SqlUpdateCommand As SqlCommand
    Friend WithEvents txtCodigoCabysI As TextBox
    Friend WithEvents GridView1 As GridView
    Dim strModulo As String = ""

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario_Parametro As Object, Optional ByVal Conexion As String = "")
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        AddHandler Me.BindingContext(Me.DataSetFamilia1, "Familia1").PositionChanged, AddressOf Me.Position_Changed
        NuevaConexion = Conexion
        usuario = Usuario_Parametro

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents DataSetFamilia1 As DataSetFamilia
    Friend WithEvents DataGrid2 As System.Windows.Forms.DataGrid
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents TxtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Txtdescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Txtobssub_familia As System.Windows.Forms.TextBox
    Friend WithEvents Txtdescsub_familia As System.Windows.Forms.TextBox
    Friend WithEvents Txtcodsub_familia As System.Windows.Forms.TextBox
    Friend WithEvents colCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colObservaciones As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Adapter_Familias As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents Adapter_Subfamilias As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtcod_sub As System.Windows.Forms.TextBox
    Friend WithEvents txtcodfam As System.Windows.Forms.TextBox
    Friend WithEvents DataNavigator2 As DevExpress.XtraEditors.DataNavigator
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Familia))
        Dim ColumnFilterInfo1 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo2 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo3 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo4 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCodigo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colObservaciones = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCabys = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.DataSetFamilia1 = New LcPymes_5._2.DataSetFamilia()
        Me.DataGrid2 = New System.Windows.Forms.DataGrid()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid()
        Me.TxtObservaciones = New System.Windows.Forms.TextBox()
        Me.Txtdescripcion = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtCodigoCabysI = New System.Windows.Forms.TextBox()
        Me.BtnBuscarCabys = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtDescripcionCabys = New System.Windows.Forms.TextBox()
        Me.txtCodigoCabys = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnGuardarSubUbicacion = New System.Windows.Forms.Button()
        Me.btnNuevaSubFamilia = New System.Windows.Forms.Button()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Txtobssub_familia = New System.Windows.Forms.TextBox()
        Me.Txtdescsub_familia = New System.Windows.Forms.TextBox()
        Me.Txtcodsub_familia = New System.Windows.Forms.TextBox()
        Me.Adapter_Familias = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.Adapter_Subfamilias = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand = New System.Data.SqlClient.SqlCommand()
        Me.txtcod_sub = New System.Windows.Forms.TextBox()
        Me.txtcodfam = New System.Windows.Forms.TextBox()
        Me.DataNavigator2 = New DevExpress.XtraEditors.DataNavigator()
        Me.cbActividadEconomica = New System.Windows.Forms.ComboBox()
        Me.bsActividadEconomica = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TB_CE_ActividadEconomicaTableAdapter = New LcPymes_5._2.DataSetFamiliaTableAdapters.TB_CE_ActividadEconomicaTableAdapter()
        Me.btnGuardarFamilia = New System.Windows.Forms.Button()
        Me.TxtCodigo = New System.Windows.Forms.Label()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetFamilia1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsActividadEconomica, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.ImageIndex = 4
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 5
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.Images.SetKeyName(0, "Nuevo.png")
        Me.ImageList.Images.SetKeyName(1, "Buscar.png")
        Me.ImageList.Images.SetKeyName(2, "Guardar.png")
        Me.ImageList.Images.SetKeyName(3, "Eliminar.png")
        Me.ImageList.Images.SetKeyName(4, "Imprimir.png")
        Me.ImageList.Images.SetKeyName(5, "Cerrar.png")
        Me.ImageList.Images.SetKeyName(6, "Cancelar.png")
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 489)
        Me.ToolBar1.Size = New System.Drawing.Size(584, 52)
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.ImageList = Me.ImageList
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(474, 706)
        '
        'TituloModulo
        '
        Me.TituloModulo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TituloModulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(143, Byte), Integer))
        Me.TituloModulo.Dock = System.Windows.Forms.DockStyle.None
        Me.TituloModulo.Image = Nothing
        Me.TituloModulo.Size = New System.Drawing.Size(584, 10)
        Me.TituloModulo.Text = ""
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCodigo, Me.colDescripcion, Me.colObservaciones, Me.colCabys})
        Me.GridView1.GroupPanelText = "Agrupe de acuerdo a la columna deseada"
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colCodigo
        '
        Me.colCodigo.Caption = "C�digo"
        Me.colCodigo.FieldName = "Codigo"
        Me.colCodigo.FilterInfo = ColumnFilterInfo1
        Me.colCodigo.Name = "colCodigo"
        Me.colCodigo.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCodigo.VisibleIndex = 0
        '
        'colDescripcion
        '
        Me.colDescripcion.Caption = "Descripci�n"
        Me.colDescripcion.FieldName = "Descripcion"
        Me.colDescripcion.FilterInfo = ColumnFilterInfo2
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDescripcion.VisibleIndex = 1
        '
        'colObservaciones
        '
        Me.colObservaciones.Caption = "Observaciones"
        Me.colObservaciones.FieldName = "Observaciones"
        Me.colObservaciones.FilterInfo = ColumnFilterInfo3
        Me.colObservaciones.Name = "colObservaciones"
        Me.colObservaciones.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colObservaciones.VisibleIndex = 2
        '
        'colCabys
        '
        Me.colCabys.Caption = "Cabys"
        Me.colCabys.FieldName = "DescCabys"
        Me.colCabys.FilterInfo = ColumnFilterInfo4
        Me.colCabys.Name = "colCabys"
        Me.colCabys.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCabys.VisibleIndex = 3
        '
        'DataSetFamilia1
        '
        Me.DataSetFamilia1.DataSetName = "DataSetFamilia"
        Me.DataSetFamilia1.Locale = New System.Globalization.CultureInfo("es-MX")
        Me.DataSetFamilia1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataGrid2
        '
        Me.DataGrid2.DataMember = "Familia.FamiliaSubFamilias"
        Me.DataGrid2.DataSource = Me.DataSetFamilia1
        Me.DataGrid2.Enabled = False
        Me.DataGrid2.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid2.Location = New System.Drawing.Point(599, 176)
        Me.DataGrid2.Name = "DataGrid2"
        Me.DataGrid2.Size = New System.Drawing.Size(205, 112)
        Me.DataGrid2.TabIndex = 117
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = "Familia"
        Me.DataGrid1.DataSource = Me.DataSetFamilia1
        Me.DataGrid1.Enabled = False
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(596, 32)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(208, 112)
        Me.DataGrid1.TabIndex = 116
        '
        'TxtObservaciones
        '
        Me.TxtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtObservaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtObservaciones.ForeColor = System.Drawing.Color.Black
        Me.TxtObservaciones.Location = New System.Drawing.Point(8, 75)
        Me.TxtObservaciones.Multiline = True
        Me.TxtObservaciones.Name = "TxtObservaciones"
        Me.TxtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TxtObservaciones.Size = New System.Drawing.Size(480, 32)
        Me.TxtObservaciones.TabIndex = 122
        '
        'Txtdescripcion
        '
        Me.Txtdescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txtdescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txtdescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtdescripcion.ForeColor = System.Drawing.Color.Black
        Me.Txtdescripcion.Location = New System.Drawing.Point(8, 35)
        Me.Txtdescripcion.Name = "Txtdescripcion"
        Me.Txtdescripcion.Size = New System.Drawing.Size(480, 20)
        Me.Txtdescripcion.TabIndex = 121
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(8, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(480, 14)
        Me.Label3.TabIndex = 126
        Me.Label3.Text = "Observaciones:"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(8, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(480, 14)
        Me.Label2.TabIndex = 125
        Me.Label2.Text = "Descripci�n:"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.txtCodigoCabysI)
        Me.GroupBox3.Controls.Add(Me.BtnBuscarCabys)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.txtDescripcionCabys)
        Me.GroupBox3.Controls.Add(Me.txtCodigoCabys)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.btnGuardarSubUbicacion)
        Me.GroupBox3.Controls.Add(Me.btnNuevaSubFamilia)
        Me.GroupBox3.Controls.Add(Me.GridControl1)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Txtobssub_familia)
        Me.GroupBox3.Controls.Add(Me.Txtdescsub_familia)
        Me.GroupBox3.Controls.Add(Me.Txtcodsub_familia)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.Black
        Me.GroupBox3.Location = New System.Drawing.Point(8, 154)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(564, 329)
        Me.GroupBox3.TabIndex = 124
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Sub-Familias"
        '
        'txtCodigoCabysI
        '
        Me.txtCodigoCabysI.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodigoCabysI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoCabysI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoCabysI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigoCabysI.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoCabysI.Location = New System.Drawing.Point(6, 114)
        Me.txtCodigoCabysI.Name = "txtCodigoCabysI"
        Me.txtCodigoCabysI.ReadOnly = True
        Me.txtCodigoCabysI.Size = New System.Drawing.Size(99, 20)
        Me.txtCodigoCabysI.TabIndex = 126
        '
        'BtnBuscarCabys
        '
        Me.BtnBuscarCabys.BackColor = System.Drawing.Color.LightSeaGreen
        Me.BtnBuscarCabys.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBuscarCabys.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.BtnBuscarCabys.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.BtnBuscarCabys.Image = Global.LcPymes_5._2.My.Resources.Resources.Buscar_sincolor
        Me.BtnBuscarCabys.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnBuscarCabys.Location = New System.Drawing.Point(474, 98)
        Me.BtnBuscarCabys.Name = "BtnBuscarCabys"
        Me.BtnBuscarCabys.Size = New System.Drawing.Size(84, 46)
        Me.BtnBuscarCabys.TabIndex = 125
        Me.BtnBuscarCabys.Text = " Cabys"
        Me.BtnBuscarCabys.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnBuscarCabys.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(111, 97)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(348, 14)
        Me.Label8.TabIndex = 124
        Me.Label8.Text = "Descripci�n Cabys"
        '
        'txtDescripcionCabys
        '
        Me.txtDescripcionCabys.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtDescripcionCabys.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcionCabys.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcionCabys.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcionCabys.ForeColor = System.Drawing.Color.Black
        Me.txtDescripcionCabys.Location = New System.Drawing.Point(111, 114)
        Me.txtDescripcionCabys.Name = "txtDescripcionCabys"
        Me.txtDescripcionCabys.ReadOnly = True
        Me.txtDescripcionCabys.Size = New System.Drawing.Size(348, 20)
        Me.txtDescripcionCabys.TabIndex = 123
        '
        'txtCodigoCabys
        '
        Me.txtCodigoCabys.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodigoCabys.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoCabys.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoCabys.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigoCabys.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoCabys.Location = New System.Drawing.Point(25, 114)
        Me.txtCodigoCabys.Name = "txtCodigoCabys"
        Me.txtCodigoCabys.ReadOnly = True
        Me.txtCodigoCabys.Size = New System.Drawing.Size(29, 20)
        Me.txtCodigoCabys.TabIndex = 122
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(6, 97)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(99, 14)
        Me.Label7.TabIndex = 115
        Me.Label7.Text = "C�d. Cabys"
        '
        'btnGuardarSubUbicacion
        '
        Me.btnGuardarSubUbicacion.BackColor = System.Drawing.Color.FromArgb(CType(CType(90, Byte), Integer), CType(CType(112, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.btnGuardarSubUbicacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarSubUbicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnGuardarSubUbicacion.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnGuardarSubUbicacion.Image = Global.LcPymes_5._2.My.Resources.Resources.Guardar_sincolor
        Me.btnGuardarSubUbicacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardarSubUbicacion.Location = New System.Drawing.Point(474, 52)
        Me.btnGuardarSubUbicacion.Name = "btnGuardarSubUbicacion"
        Me.btnGuardarSubUbicacion.Size = New System.Drawing.Size(84, 46)
        Me.btnGuardarSubUbicacion.TabIndex = 114
        Me.btnGuardarSubUbicacion.Text = "Agregar"
        Me.btnGuardarSubUbicacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGuardarSubUbicacion.UseVisualStyleBackColor = False
        '
        'btnNuevaSubFamilia
        '
        Me.btnNuevaSubFamilia.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.btnNuevaSubFamilia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevaSubFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnNuevaSubFamilia.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnNuevaSubFamilia.Image = Global.LcPymes_5._2.My.Resources.Resources.Nuevo_sincolor
        Me.btnNuevaSubFamilia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevaSubFamilia.Location = New System.Drawing.Point(474, 7)
        Me.btnNuevaSubFamilia.Name = "btnNuevaSubFamilia"
        Me.btnNuevaSubFamilia.Size = New System.Drawing.Size(84, 45)
        Me.btnNuevaSubFamilia.TabIndex = 113
        Me.btnNuevaSubFamilia.Text = "Nuevo"
        Me.btnNuevaSubFamilia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevaSubFamilia.UseVisualStyleBackColor = False
        '
        'GridControl1
        '
        '
        '
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(6, 146)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(552, 177)
        Me.GridControl1.TabIndex = 111
        Me.GridControl1.Text = "GridControl1"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(6, 54)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(453, 18)
        Me.Label6.TabIndex = 106
        Me.Label6.Text = "Observaciones:"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(6, 15)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 14)
        Me.Label5.TabIndex = 105
        Me.Label5.Text = "C�digo"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(93, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(366, 13)
        Me.Label4.TabIndex = 104
        Me.Label4.Text = "Descripci�n"
        '
        'Txtobssub_familia
        '
        Me.Txtobssub_familia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txtobssub_familia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txtobssub_familia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtobssub_familia.ForeColor = System.Drawing.Color.Black
        Me.Txtobssub_familia.Location = New System.Drawing.Point(6, 69)
        Me.Txtobssub_familia.Multiline = True
        Me.Txtobssub_familia.Name = "Txtobssub_familia"
        Me.Txtobssub_familia.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Txtobssub_familia.Size = New System.Drawing.Size(453, 25)
        Me.Txtobssub_familia.TabIndex = 5
        '
        'Txtdescsub_familia
        '
        Me.Txtdescsub_familia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txtdescsub_familia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txtdescsub_familia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtdescsub_familia.ForeColor = System.Drawing.Color.Black
        Me.Txtdescsub_familia.Location = New System.Drawing.Point(93, 32)
        Me.Txtdescsub_familia.Name = "Txtdescsub_familia"
        Me.Txtdescsub_familia.Size = New System.Drawing.Size(366, 20)
        Me.Txtdescsub_familia.TabIndex = 4
        '
        'Txtcodsub_familia
        '
        Me.Txtcodsub_familia.BackColor = System.Drawing.SystemColors.Window
        Me.Txtcodsub_familia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Txtcodsub_familia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtcodsub_familia.ForeColor = System.Drawing.Color.Black
        Me.Txtcodsub_familia.Location = New System.Drawing.Point(6, 32)
        Me.Txtcodsub_familia.Name = "Txtcodsub_familia"
        Me.Txtcodsub_familia.ReadOnly = True
        Me.Txtcodsub_familia.Size = New System.Drawing.Size(66, 20)
        Me.Txtcodsub_familia.TabIndex = 3
        '
        'Adapter_Familias
        '
        Me.Adapter_Familias.DeleteCommand = Me.SqlDeleteCommand1
        Me.Adapter_Familias.InsertCommand = Me.SqlInsertCommand1
        Me.Adapter_Familias.SelectCommand = Me.SqlSelectCommand1
        Me.Adapter_Familias.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Familia", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("IdActividadEconomica", "IdActividadEconomica")})})
        Me.Adapter_Familias.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_IdActividadEconomica", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdActividadEconomica", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=.;Initial Catalog=SEEPOS;Integrated Security=True"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.Int, 0, "Codigo"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 0, "Descripcion"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@IdActividadEconomica", System.Data.SqlDbType.BigInt, 0, "IdActividadEconomica")})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT        Codigo, Descripcion, Observaciones, IdActividadEconomica" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM     " &
    "       Familia"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.Int, 0, "Codigo"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 0, "Descripcion"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@IdActividadEconomica", System.Data.SqlDbType.BigInt, 0, "IdActividadEconomica"), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_IdActividadEconomica", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdActividadEconomica", System.Data.DataRowVersion.Original, Nothing)})
        '
        'Adapter_Subfamilias
        '
        Me.Adapter_Subfamilias.DeleteCommand = Me.SqlDeleteCommand2
        Me.Adapter_Subfamilias.InsertCommand = Me.SqlInsertCommand
        Me.Adapter_Subfamilias.SelectCommand = Me.SqlSelectCommand2
        Me.Adapter_Subfamilias.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "SubFamilias", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoFamilia", "CodigoFamilia"), New System.Data.Common.DataColumnMapping("SubCodigo", "SubCodigo"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("idCabys", "idCabys")})})
        Me.Adapter_Subfamilias.UpdateCommand = Me.SqlUpdateCommand
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodigoFamilia", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoFamilia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubCodigo", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubCodigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_idCabys", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "idCabys", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand
        '
        Me.SqlInsertCommand.CommandText = resources.GetString("SqlInsertCommand.CommandText")
        Me.SqlInsertCommand.Connection = Me.SqlConnection1
        Me.SqlInsertCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodigoFamilia", System.Data.SqlDbType.Int, 0, "CodigoFamilia"), New System.Data.SqlClient.SqlParameter("@SubCodigo", System.Data.SqlDbType.Int, 0, "SubCodigo"), New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 0, "Codigo"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 0, "Descripcion"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@idCabys", System.Data.SqlDbType.Int, 0, "idCabys")})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = resources.GetString("SqlSelectCommand2.CommandText")
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand
        '
        Me.SqlUpdateCommand.CommandText = resources.GetString("SqlUpdateCommand.CommandText")
        Me.SqlUpdateCommand.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodigoFamilia", System.Data.SqlDbType.Int, 0, "CodigoFamilia"), New System.Data.SqlClient.SqlParameter("@SubCodigo", System.Data.SqlDbType.Int, 0, "SubCodigo"), New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 0, "Codigo"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 0, "Descripcion"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@idCabys", System.Data.SqlDbType.Int, 0, "idCabys"), New System.Data.SqlClient.SqlParameter("@Original_CodigoFamilia", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoFamilia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubCodigo", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubCodigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_idCabys", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "idCabys", System.Data.DataRowVersion.Original, Nothing)})
        '
        'txtcod_sub
        '
        Me.txtcod_sub.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtcod_sub.Location = New System.Drawing.Point(595, 314)
        Me.txtcod_sub.Name = "txtcod_sub"
        Me.txtcod_sub.ReadOnly = True
        Me.txtcod_sub.Size = New System.Drawing.Size(10, 13)
        Me.txtcod_sub.TabIndex = 128
        '
        'txtcodfam
        '
        Me.txtcodfam.Location = New System.Drawing.Point(591, 345)
        Me.txtcodfam.Name = "txtcodfam"
        Me.txtcodfam.Size = New System.Drawing.Size(14, 20)
        Me.txtcodfam.TabIndex = 129
        '
        'DataNavigator2
        '
        Me.DataNavigator2.Buttons.Append.Visible = False
        Me.DataNavigator2.Buttons.CancelEdit.Visible = False
        Me.DataNavigator2.Buttons.EndEdit.Visible = False
        Me.DataNavigator2.Buttons.Remove.Visible = False
        Me.DataNavigator2.DataMember = "Familia1"
        Me.DataNavigator2.DataSource = Me.DataSetFamilia1
        Me.DataNavigator2.Location = New System.Drawing.Point(383, 505)
        Me.DataNavigator2.Name = "DataNavigator2"
        Me.DataNavigator2.Size = New System.Drawing.Size(134, 24)
        Me.DataNavigator2.TabIndex = 130
        '
        'cbActividadEconomica
        '
        Me.cbActividadEconomica.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbActividadEconomica.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbActividadEconomica.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetFamilia1, "Familia.IdActividadEconomica", True))
        Me.cbActividadEconomica.DataSource = Me.bsActividadEconomica
        Me.cbActividadEconomica.DisplayMember = "Descripcion"
        Me.cbActividadEconomica.FormattingEnabled = True
        Me.cbActividadEconomica.Location = New System.Drawing.Point(8, 127)
        Me.cbActividadEconomica.Name = "cbActividadEconomica"
        Me.cbActividadEconomica.Size = New System.Drawing.Size(480, 21)
        Me.cbActividadEconomica.TabIndex = 133
        Me.cbActividadEconomica.ValueMember = "Id"
        '
        'bsActividadEconomica
        '
        Me.bsActividadEconomica.DataMember = "TB_CE_ActividadEconomica"
        Me.bsActividadEconomica.DataSource = Me.DataSetFamilia1
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(8, 110)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(480, 14)
        Me.Label1.TabIndex = 134
        Me.Label1.Text = "Actividad Econ�mica:"
        '
        'TB_CE_ActividadEconomicaTableAdapter
        '
        Me.TB_CE_ActividadEconomicaTableAdapter.ClearBeforeFill = True
        '
        'btnGuardarFamilia
        '
        Me.btnGuardarFamilia.BackColor = System.Drawing.Color.FromArgb(CType(CType(90, Byte), Integer), CType(CType(112, Byte), Integer), CType(CType(121, Byte), Integer))
        Me.btnGuardarFamilia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarFamilia.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnGuardarFamilia.Image = Global.LcPymes_5._2.My.Resources.Resources.Guardar_sincolor
        Me.btnGuardarFamilia.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnGuardarFamilia.Location = New System.Drawing.Point(497, 71)
        Me.btnGuardarFamilia.Name = "btnGuardarFamilia"
        Me.btnGuardarFamilia.Size = New System.Drawing.Size(75, 53)
        Me.btnGuardarFamilia.TabIndex = 112
        Me.btnGuardarFamilia.Text = "Guardar"
        Me.btnGuardarFamilia.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnGuardarFamilia.UseVisualStyleBackColor = False
        '
        'TxtCodigo
        '
        Me.TxtCodigo.AutoSize = True
        Me.TxtCodigo.Location = New System.Drawing.Point(494, 10)
        Me.TxtCodigo.MaximumSize = New System.Drawing.Size(50, 13)
        Me.TxtCodigo.MinimumSize = New System.Drawing.Size(50, 13)
        Me.TxtCodigo.Name = "TxtCodigo"
        Me.TxtCodigo.Size = New System.Drawing.Size(50, 13)
        Me.TxtCodigo.TabIndex = 135
        '
        'Familia
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(584, 541)
        Me.Controls.Add(Me.TxtCodigo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnGuardarFamilia)
        Me.Controls.Add(Me.cbActividadEconomica)
        Me.Controls.Add(Me.DataNavigator2)
        Me.Controls.Add(Me.txtcod_sub)
        Me.Controls.Add(Me.txtcodfam)
        Me.Controls.Add(Me.TxtObservaciones)
        Me.Controls.Add(Me.Txtdescripcion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.DataGrid2)
        Me.Controls.Add(Me.DataGrid1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(600, 580)
        Me.MinimumSize = New System.Drawing.Size(551, 580)
        Me.Name = "Familia"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Inventario / Familia y Sub-Familia"
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.DataGrid1, 0)
        Me.Controls.SetChildIndex(Me.DataGrid2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Txtdescripcion, 0)
        Me.Controls.SetChildIndex(Me.TxtObservaciones, 0)
        Me.Controls.SetChildIndex(Me.txtcodfam, 0)
        Me.Controls.SetChildIndex(Me.txtcod_sub, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator2, 0)
        Me.Controls.SetChildIndex(Me.cbActividadEconomica, 0)
        Me.Controls.SetChildIndex(Me.btnGuardarFamilia, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.TxtCodigo, 0)
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetFamilia1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsActividadEconomica, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Public Cargando As Boolean = False


#Region "Load"
    Private Sub Familia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePOS", "CONEXION"), NuevaConexion)

        TB_CE_ActividadEconomicaTableAdapter.Connection = SqlConnection1
        Me.TB_CE_ActividadEconomicaTableAdapter.Fill(Me.DataSetFamilia1.TB_CE_ActividadEconomica)

        strModulo = IIf(NuevaConexion = "", "SeePOS", "Proveeduria")

        Cargando = True
        CargarMovimiento()
        If Me.BindingContext(Me.DataSetFamilia1, "Familia1").Count > 0 Then
            CargarFamiliasSubfamilias(Me.BindingContext(Me.DataSetFamilia1, "Familia1").Current("Codigo"))
        End If
        Binding()
        Me.DataSetFamilia1.Familia.ObservacionesColumn.DefaultValue = ""
        Me.DataSetFamilia1.Familia.DescripcionColumn.DefaultValue = ""
        Me.DataSetFamilia1.Familia.CodigoColumn.DefaultValue = "0"
        Me.DataSetFamilia1.SubFamilias.ObservacionesColumn.DefaultValue = ""
        Me.DataSetFamilia1.SubFamilias.DescripcionColumn.DefaultValue = ""
        Me.DataSetFamilia1.SubFamilias.CodigoColumn.DefaultValue = "0"
        Cargando = False
    End Sub

    Function Binding()
        Me.GridControl1.DataMember = "Familia.FamiliaSubFamilias"
        Me.GridControl1.DataSource = Me.DataSetFamilia1
        Me.Txtobssub_familia.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.FamiliaSubFamilias.Observaciones"))
        Me.Txtdescsub_familia.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.FamiliaSubFamilias.Descripcion"))
        Me.Txtcodsub_familia.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.FamiliaSubFamilias.Codigo"))
        Me.TxtObservaciones.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.Observaciones"))
        Me.TxtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.Codigo"))
        Me.Txtdescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.Descripcion"))
        Me.txtcod_sub.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.FamiliaSubFamilias.SubCodigo"))
        Me.txtcodfam.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.FamiliaSubFamilias.CodigoFamilia"))
        Me.txtCodigoCabys.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.FamiliaSubFamilias.idCabys"))
        Me.txtDescripcionCabys.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.FamiliaSubFamilias.DescCabys"))
        Me.txtCodigoCabysI.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetFamilia1, "Familia.FamiliaSubFamilias.CodCabys"))
    End Function

    Function CargarMovimiento()
        Dim cx As New cFunciones
        cx.Llenar_Tabla_Generico("Select Codigo from familia", Me.DataSetFamilia1.Familia1, Me.SqlConnection1.ConnectionString)
    End Function

    Function CargarFamiliasSubfamilias(ByVal codigo As Integer)
        Dim cx As New cFunciones
        Dim dt As New DataTable
        If Me.DataSetFamilia1.Familia1.Rows.Count > 0 Then
            cx.Llenar_Tabla_Generico("Select * from familia where Codigo = " & codigo, Me.DataSetFamilia1.Familia, Me.SqlConnection1.ConnectionString)
            cx.Llenar_Tabla_Generico("SELECT SubFamilias.CodigoFamilia, SubFamilias.SubCodigo, SubFamilias.Codigo, SubFamilias.Descripcion, SubFamilias.Observaciones, SubFamilias.idCabys, Cabys.Descripcion AS DescCabys,Cabys.codigo AS CodCabys
                    FROM SubFamilias INNER JOIN Cabys ON SubFamilias.idCabys = Cabys.id where CodigoFamilia = " & codigo, Me.DataSetFamilia1.SubFamilias, Me.SqlConnection1.ConnectionString)
            'cargarCabysDescipcion(codigo, cx, dt)
        End If
    End Function

#End Region

#Region "Familia"

    Function NuevaFamilia()
        Try
            If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
                Me.ToolBar1.Buttons(0).Text = "Cancelar"
                Me.ToolBar1.Buttons(0).ImageIndex = 6
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.GridControl1.Enabled = False
                Try
                    If Me.BindingContext(Me.DataSetFamilia1, "Familia").Count = 98 Then
                        MsgBox("No pueden existir m�s de 99 Familias", MsgBoxStyle.Exclamation)
                        Exit Function
                    End If
                    DataNavigator2.Enabled = False
                    Me.DataSetFamilia1.SubFamilias.Clear()
                    Me.DataSetFamilia1.Familia.Clear()
                    Autonumerico()
                    Me.BindingContext(Me.DataSetFamilia1, "Familia").AddNew()
                    Me.cbActividadEconomica.SelectedIndex = 0
                    Txtdescripcion.Focus()
                    deshabilitar_sububicacion()
                Catch ex As SystemException
                    MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
                End Try
            Else
                If MessageBox.Show("Desea Guardar esta Familia", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                    Me.registrar() ' se guarda en la base de datos
                Else
                    Me.BindingContext(Me.DataSetFamilia1, "Familia").CancelCurrentEdit()
                    Me.DataSetFamilia1.SubFamilias.Clear()
                    Me.DataSetFamilia1.Familia.Clear()
                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    Me.ToolBar1.Buttons(0).Enabled = True
                    Me.ToolBar1.Buttons(1).Enabled = True
                    Me.ToolBar1.Buttons(2).Enabled = True
                    Me.ToolBar1.Buttons(3).Enabled = True
                    Me.ToolBar1.Buttons(4).Enabled = True
                    Me.GridControl1.Enabled = True
                    'Me.boton_nueva_subFamilia.Enabled = True
                    'Me.boton_guardar_sububicacion.Enabled = True    09/09/2020
                    Me.btnNuevaSubFamilia.Enabled = True
                    Me.btnGuardarSubUbicacion.Enabled = True        '09/09/2020
                    DataNavigator2.Enabled = True
                    If Me.BindingContext(Me.DataSetFamilia1, "Familia1").Count > 0 Then
                        CargarFamiliasSubfamilias(Me.BindingContext(Me.DataSetFamilia1, "Familia1").Current("Codigo"))
                    End If
                End If
            End If

        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Function

    Function Autonumerico()
        Dim fun As New Conexion
        If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open()
        Me.DataSetFamilia1.Familia.CodigoColumn.DefaultValue = CDbl(fun.SlqExecuteScalar(Me.SqlConnection1, "SELECT isnull(MAX(Codigo) + 1,1) FROM Familia"))
        Me.SqlConnection1.Close()
    End Function

    Function ValidarFamilia() As Boolean
        'nombtre no este vacio
        If Txtdescripcion.Text = "" Then
            MsgBox("Debes escribir la descripci�n de la familia", MsgBoxStyle.Information)
            Txtdescripcion.Focus()
            Return False
        End If

        If Me.ToolBarNuevo.Text = "Cancelar" Then
            Dim tabla As New DataTable
            Dim cfunciones As New cFunciones
            cFunciones.Llenar_Tabla_Generico("SELECT Codigo FROM Familia WHERE Descripcion = '" & Txtdescripcion.Text & "'", tabla, Me.SqlConnection1.ConnectionString)
            If tabla.Rows.Count > 0 Then
                MsgBox("Ya Existe una familia con ese nombre", MsgBoxStyle.Information)
                Txtdescripcion.Focus()
                Return False
            End If
        End If

        Return True
    End Function

    Sub Buscar_Familia()
        If Me.ToolBarNuevo.Text = "Cancelar" Then
            If MsgBox("Actualmente se esta creando una famila, desea cancelar esta edicion y hacer la busqueda", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Me.DataSetFamilia1.SubFamilias.Clear()
                Me.DataSetFamilia1.Familia.Clear()
                Me.BindingContext(Me.DataSetFamilia1, "Familia").CancelCurrentEdit()
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                Me.ToolBar1.Buttons(0).Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBar1.Buttons(2).Enabled = True
                Me.ToolBar1.Buttons(3).Enabled = True
                Me.ToolBar1.Buttons(4).Enabled = True
                'Me.boton_nueva_subFamilia.Enabled = True
                'Me.boton_guardar_sububicacion.Enabled = True     09/09/2020
                Me.btnNuevaSubFamilia.Enabled = True
                Me.btnGuardarSubUbicacion.Enabled = True
                DataNavigator2.Enabled = True
            Else
                Exit Sub
            End If
        End If

        Try
            Dim Fx As New cFunciones
            Dim valor As String
            Dim pos As Integer
            Dim vista As DataView
            valor = Fx.BuscarDatos("Select Codigo,Descripcion From Familia", "Descripcion", "Buscar Familia...", Me.SqlConnection1.ConnectionString)
            If valor = "" Then
                Exit Sub
            Else
                Me.DataSetFamilia1.SubFamilias.Clear()
                Me.DataSetFamilia1.Familia.Clear()
                vista = Me.DataSetFamilia1.Familia1.DefaultView
                vista.Sort = "Codigo"
                pos = vista.Find(CDbl(valor))
                If pos = Me.BindingContext(Me.DataSetFamilia1, "Familia1").Position Then
                    If Me.BindingContext(Me.DataSetFamilia1, "Familia1").Count > 0 Then
                        CargarFamiliasSubfamilias(Me.BindingContext(Me.DataSetFamilia1, "Familia1").Current("Codigo"))
                    End If
                Else
                    Me.BindingContext(Me.DataSetFamilia1, "Familia1").Position = pos
                End If

                'CargarFamiliasSubfamilias(CInt(valor))
            End If

        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try

    End Sub

    Sub Eliminar_Familia()

        Dim resp As Integer
        If Me.ToolBarNuevo.Text = "Cancelar" Then
            MsgBox("No se puede eliminar por que se esta creando un familia", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try 'se intenta hacer
            Dim tabla As New DataTable
            Dim tabla2 As New DataTable
            Dim cfunciones As New cFunciones
            Dim conexion As New Conexion

            Dim conectado As SqlConnection = Me.SqlConnection1
            If Me.BindingContext(Me.DataSetFamilia1, "Familia").Count > 0 Then  ' si hay ubicaciones

                If Me.BindingContext(Me.DataSetFamilia1.SubFamilias).Count > 0 Then

                    'Evaluo si las subfamilias estan asociadas a un articulo 
                    'Dim tabla1 As New DataTable
                    'Dim cfunciones1 As New cFunciones
                    cfunciones.Llenar_Tabla_Generico("select Familia.Codigo from Familia
                        join SubFamilias on Familia.Codigo=SubFamilias.CodigoFamilia
                        join Inventario on SubFamilias.Codigo=Inventario.SubFamilia
                        where Familia.Codigo= " & Me.BindingContext(Me.DataSetFamilia1, "Familia").Current("Codigo") & "", tabla, Me.SqlConnection1.ConnectionString)
                    If tabla.Rows.Count > 0 Then
                        MsgBox("No se puede eliminar esta Sub-Familia por que existe articulos en el inventario que pertenecen a esta.")
                        Exit Sub
                    End If

                    cfunciones.Llenar_Tabla_Generico("select Familia.Codigo from Familia
                                join SubFamilias on Familia.Codigo=SubFamilias.CodigoFamilia
                                join SubFamiliasTipoClientes
                                on SubFamilias.Codigo=SubFamiliasTipoClientes.SubFamiliaId where Familia.Codigo= " & Me.BindingContext(Me.DataSetFamilia1, "Familia").Current("Codigo") & "", tabla2, Me.SqlConnection1.ConnectionString)
                    If tabla2.Rows.Count > 0 Then
                        MsgBox("No se puede eliminar esta Familia, por que existen tipos de clientes que pertenecen a la Sub-Familia de esta.")
                        Exit Sub
                    End If
                End If
                'Cargo el codigo de la familia al que pertenecen las familias que se eliminaran
                Dim strCodigoFamilia As String = Me.BindingContext(DataSetFamilia1, "Familia").Current("Codigo")

                resp = MessageBox.Show("�Desea eliminar permanentemente esta Familia?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    Me.BindingContext(Me.DataSetFamilia1, "Familia").RemoveAt(Me.BindingContext(Me.DataSetFamilia1, "Familia").Position)
                    Me.BindingContext(Me.DataSetFamilia1, "Familia").EndCurrentEdit()
                    Me.Adapter_Familias.Update(Me.DataSetFamilia1, "Familia")
                    CargarFamiliaAcutal(True, )
                Else
                    Exit Sub
                End If
            Else
                MsgBox("No Existen Familias que Eliminar", MsgBoxStyle.Information)
            End If
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub imprimir()
        If Me.ToolBarNuevo.Text = "Cancelar" Then
            MsgBox("No se puede imprimir por que actualmente se esta creando una familia", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            Me.ToolBar1.Buttons(2).Enabled = False
            Dim Familias_Reporte As New Reporte_Familias
            CrystalReportsConexion.LoadShow(Familias_Reporte, MdiParent, Me.SqlConnection1.ConnectionString)
            Me.ToolBar1.Buttons(2).Enabled = True
        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub AbrirFrmTipoCliente()
        Dim frmTipoCliente As New FrmTipoClienteFam
        Dim tabla As New DataTable
        Dim cfunciones As New cFunciones
        cfunciones.Llenar_Tabla_Generico("SELECT Codigo FROM SubFamilias WHERE SubFamilias.Codigo = '" & Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").Current("Codigo") & "'", tabla, Me.SqlConnection1.ConnectionString)
        If tabla.Rows.Count > 0 Then
            frmTipoCliente.codigoSubFamilia = tabla.Rows(0).Item("Codigo").ToString
            frmTipoCliente.ShowDialog()
        Else
            MsgBox("Debe registrar la SubFamilia permanentemente.", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As DevExpress.Utils.DXMouseEventArgs) Handles GridControl1.DoubleClick
        If GridView1.SelectedRowsCount > 0 Then
            AbrirFrmTipoCliente()
        Else
            MsgBox("Debe seleccionar una SubFamilia.", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub GridControl1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl1.KeyDown
        If e.KeyCode = Keys.Delete Then
            Me.Eliminar_SubFamilia()
        End If
    End Sub

    Private Sub btnGuardarFamilia_Click(sender As Object, e As EventArgs) Handles btnGuardarFamilia.Click
        Try
            If Me.ValidarFamilia Then
                Me.BindingContext(Me.DataSetFamilia1, "Familia").EndCurrentEdit()
                btnNuevaSubFamilia.Enabled = True
                btnGuardarSubUbicacion.Enabled = True
                Me.ToolBar1.Buttons(3).Enabled = True
                btnNuevaSubFamilia.Focus()
            End If
        Catch ex As SystemException  'Cacha los distintos errores que se pueden dar
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try

    End Sub

    Private Sub Txtdescripcion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtdescripcion.KeyDown
        If e.KeyCode = Keys.Enter Then
            TxtObservaciones.Focus()
        End If
    End Sub

    Private Sub TxtObservaciones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtObservaciones.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnGuardarFamilia.Focus()
        End If
    End Sub

#End Region

#Region "SubFamilia"

    Sub nueva_subfamilia()
        Dim cod As Integer

        Try
            If Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").Count = 99 Then
                MsgBox("No pueden existir m�s de 99 Subfamilias", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            '***********************************************************************************
            '***********************************************************************************

            If Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").Count > 0 Then
                cod = consecutivoCodSubFamilia()
            Else
                cod = 0
            End If
            Me.DataSetFamilia1.SubFamilias.SubCodigoColumn.DefaultValue = cod
            '***********************************************************************************
            '***********************************************************************************
            Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").EndCurrentEdit()
            Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").AddNew()
            Me.Txtcodsub_familia.Text = Me.TxtCodigo.Text + "-" + txtcod_sub.Text
            Me.txtcodfam.Text = Me.TxtCodigo.Text
            Me.btnNuevaSubFamilia.Enabled = False
            Me.GridControl1.Enabled = False

            Txtdescsub_familia.Focus()
        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Sub guardar_subfamilia()
        If Txtdescsub_familia.Text = "" Then
            MsgBox("Debes digitar la descripci�n de la Sub-Familia.")
            Txtdescsub_familia.Focus()
            Exit Sub
        End If
        If txtCodigoCabys.Text = "" Then
            MsgBox("Debes agregar el c�digo de Cabys.")
            txtCodigoCabys.Focus()
            Exit Sub
        End If
        Try
            Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").EndCurrentEdit()
            Me.btnNuevaSubFamilia.Enabled = True
            Me.btnNuevaSubFamilia.Focus()
            If Me.ToolBarNuevo.Text = "Cancelar" Then
                Me.GridControl1.Enabled = False
            Else
                Me.GridControl1.Enabled = True
            End If
        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Sub Eliminar_SubFamilia()

        If Me.ToolBarNuevo.Text <> "Cancelar" Then

            Dim tabla As New DataTable
            Dim tabla2 As New DataTable
            Dim cfunciones As New cFunciones
            cfunciones.Llenar_Tabla_Generico("SELECT Codigo FROM Inventario WHERE SubFamilia = '" & Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").Current("Codigo") & "'", tabla, Me.SqlConnection1.ConnectionString)
            If tabla.Rows.Count > 0 Then
                MsgBox("No se puede eliminar esta Sub-Familia por que existe articulos en el inventario que pertenecen a esta")
                Exit Sub
            End If

            cfunciones.Llenar_Tabla_Generico("SELECT SubFamiliaId FROM SubFamiliasTipoClientes WHERE SubFamiliaId =  '" & Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").Current("Codigo") & "'", tabla2, Me.SqlConnection1.ConnectionString)
            If tabla2.Rows.Count > 0 Then
                MsgBox("No se puede eliminar esta Sub-Familia por que existen tipos de clientes que pertenecen a esta.")
                Exit Sub
            End If
        End If

        Try 'se intenta hacer
            If Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").Count > 0 Then  ' si hay ubicaciones

                Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").RemoveAt(Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").Position)
                Me.BindingContext(Me.DataSetFamilia1, "Familia.FamiliaSubFamilias").EndCurrentEdit()

            Else
                MsgBox("No Existen SubFamilias que Eliminar", MsgBoxStyle.Information)
            End If
        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try

    End Sub

    Private Sub btnNuevaSubFamilia_Click(sender As Object, e As EventArgs) Handles btnNuevaSubFamilia.Click
        Me.nueva_subfamilia()
    End Sub

    Private Sub btnGuardarSubUbicacion_Click(sender As Object, e As EventArgs) Handles btnGuardarSubUbicacion.Click
        guardar_subfamilia()
    End Sub
#End Region

#Region "Cambio de posicion"
    Private Sub Position_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ToolBarNuevo.Text = "Cancelar" Then
            Exit Sub
        End If
        If Cargando = False Then
            If Me.BindingContext(Me.DataSetFamilia1, "Familia1").Count > 0 Then
                Me.DataSetFamilia1.SubFamilias.Clear()
                Me.DataSetFamilia1.Familia.Clear()
                CargarFamiliasSubfamilias(Me.BindingContext(Me.DataSetFamilia1, "Familia1").Current("Codigo"))
            End If
        End If
    End Sub
#End Region

    Private Sub deshabilitar_sububicacion()
        Me.btnNuevaSubFamilia.Enabled = False
        Me.btnGuardarSubUbicacion.Enabled = False
    End Sub


#Region "Toolbar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Try


            Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
            PMU = VSM(usuario.Cedula, Me.Name) 'Carga los privilegios del usuario con el modu

            Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
                Case 1
                    Me.NuevaFamilia()
                Case 2
                    If PMU.Find Then Buscar_Familia() Else MsgBox("No tiene permiso para Buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 5
                    If PMU.Print Then imprimir() Else MsgBox("No tiene permiso para imprimir los datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 3
                    If PMU.Update Then registrar() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 4
                    If PMU.Delete Then Eliminar_Familia() Else MsgBox("No tiene permiso para eliminar o anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 7
                    If MessageBox.Show("�Desea Cerrar el M�dulo de Familias?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                        Me.Close()
                    End If
            End Select
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

#End Region


#Region "Registrar"
    Sub registrar()
        Dim pos As Integer
        If Me.ToolBarNuevo.Text = "Cancelar" Then
            If Me.ValidarFamilia = False Then
                Exit Sub
            End If
        Else
            pos = Me.BindingContext(Me.DataSetFamilia1, "Familia1").Position
        End If
        Try
            Me.ToolBar1.Buttons(2).Enabled = False
            If MessageBox.Show("�Desea Guardar los Cambios en Familia?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.No Then 'si no desea guardar la facturacion
                Me.ToolBar1.Buttons(2).Enabled = True
                Exit Sub
            End If

            Me.BindingContext(Me.DataSetFamilia1, "Familia").EndCurrentEdit()
            If Registrar_Familia() Then 'se registra en la base de datos then
                CargarMovimiento()
                If Me.ToolBarNuevo.Text = "Cancelar" Then
                    Me.BindingContext(Me.DataSetFamilia1, "Familia1").Position = (Me.BindingContext(Me.DataSetFamilia1, "Familia1").Count - 1)
                Else
                    Me.BindingContext(Me.DataSetFamilia1, "Familia1").Position = pos
                End If
                Me.ToolBar1.Buttons(4).Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                MsgBox("Datos Guardados Satisfactoriamente", MsgBoxStyle.Information)
                Me.GridControl1.Enabled = True
                Me.ToolBar1.Buttons(2).Enabled = True
                DataNavigator2.Enabled = True

            Else
                MsgBox("Error al Registrar", MsgBoxStyle.Critical)
                Me.ToolBar1.Buttons(2).Enabled = True
                DataNavigator2.Enabled = True
            End If
        Catch ex As System.Exception
            Me.ToolBar1.Buttons(2).Enabled = True
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try

    End Sub

    Function Registrar_Familia() As Boolean
        If Me.SqlConnection1.State <> Data.ConnectionState.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            Me.Adapter_Familias.InsertCommand.Transaction = Trans
            Me.Adapter_Familias.SelectCommand.Transaction = Trans
            Me.Adapter_Familias.DeleteCommand.Transaction = Trans
            Me.Adapter_Familias.UpdateCommand.Transaction = Trans
            Me.Adapter_Subfamilias.SelectCommand.Transaction = Trans
            Me.Adapter_Subfamilias.InsertCommand.Transaction = Trans
            Me.Adapter_Subfamilias.DeleteCommand.Transaction = Trans
            Me.Adapter_Subfamilias.UpdateCommand.Transaction = Trans
            Me.Adapter_Familias.Update(Me.DataSetFamilia1, "Familia")
            Me.Adapter_Subfamilias.Update(Me.DataSetFamilia1, "SubFamilias")
            Trans.Commit()
            Me.DataSetFamilia1.AcceptChanges()
            Return True
        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message & vbCrLf & "Posible Causa un error de Red, o no se puede eliminar porque la Familia esta ligada a almenos un art�culo en el inventario")
            Me.ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try
    End Function
#End Region


    Function CargarFamiliaAcutal(Optional ByVal Eliminar As Boolean = False, Optional ByVal Registrar As Boolean = False)

        If Me.ToolBarNuevo.Text <> "Cancelar" Then
            If Me.BindingContext(Me.DataSetFamilia1, "Familia1").Count > 0 Or Registrar = True Then
                Dim pos As Integer
                pos = Me.BindingContext(Me.DataSetFamilia1, "Familia1").Position
                CargarMovimiento()
                'si se llama por eliminar
                If Eliminar = True Then
                    'si solo no quedan registro
                    If Me.BindingContext(Me.DataSetFamilia1, "Familia1").Count = 0 Then

                    Else
                        'si quedan 
                        Me.DataSetFamilia1.SubFamilias.Clear()
                        Me.DataSetFamilia1.Familia.Clear()
                        CargarFamiliasSubfamilias(Me.BindingContext(Me.DataSetFamilia1, "Familia1").Current("Codigo"))
                    End If

                Else
                    Me.DataSetFamilia1.SubFamilias.Clear()
                    Me.DataSetFamilia1.Familia.Clear()
                    Me.BindingContext(Me.DataSetFamilia1, "Familia1").Position = pos
                    CargarFamiliasSubfamilias(Me.BindingContext(Me.DataSetFamilia1, "Familia1").Current("Codigo"))
                End If
            End If
        End If
    End Function

    Private Sub buscarCabys()
        Dim idCabys As Integer
        Dim buscarCabys As New FrmBuscarCabys

        buscarCabys.ShowDialog()
        idCabys = buscarCabys.idCabys
        Me.cargarCabys(idCabys)
    End Sub

    Private Sub cargarCabys(ByVal id As Integer)
        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("select id,Descripcion,codigo from Cabys where id=" & id & "", dt, GetSetting("seSoft", "Seepos", "Conexion"))
        If dt.Rows.Count > 0 Then
            Me.txtCodigoCabys.Text = dt.Rows(0).Item("id")
            Me.txtDescripcionCabys.Text = dt.Rows(0).Item("Descripcion")
            Me.txtCodigoCabysI.Text = dt.Rows(0).Item("codigo")
        End If
    End Sub

    Private Sub txtCodigoCabysBuscarI(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoCabysI.KeyDown
        Select Case e.KeyCode
            Case Keys.F1 : Me.buscarCabys()
        End Select
    End Sub
    Private Sub Txtdescsub_familia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtdescsub_familia.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.Txtobssub_familia.Focus()
        End If
    End Sub

    Private Sub Txtobssub_familia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtobssub_familia.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.btnGuardarSubUbicacion.Focus()
        End If
    End Sub
    Private Sub btnBuscarCabys_event(sender As Object, e As EventArgs) Handles BtnBuscarCabys.Click
        Me.buscarCabys()
    End Sub
    Private Function consecutivoCodSubFamilia() As Integer
        Dim lastRow As DataRow = DataSetFamilia1.SubFamilias.Rows(DataSetFamilia1.SubFamilias.Rows.Count - 1)
        Dim codigoS As Integer = lastRow.Item("SubCodigo")
        While (existeCodigoSubFamilia(codigoS))
            codigoS = codigoS + 1
        End While
        Return codigoS
    End Function
    Private Function existeCodigoSubFamilia(subCodigoC As Integer) As Boolean
        Dim dataR As DataRow() = DataSetFamilia1.SubFamilias.Select("SubCodigo = " & subCodigoC)
        If dataR.Length > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
