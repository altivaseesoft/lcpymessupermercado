﻿Imports System.Data

Public Class FrmBuscarCabys
    Private Sub FrmBuscarCabys_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CabysTableAdapter.Connection.ConnectionString = GetSetting("SeeSOFT", "SeePOS", "CONEXION")
        buscarDatos("%%%")
    End Sub
    Private Sub FrmBuscarCabys_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.txtBuscarDato.Focus()
    End Sub

    Private Sub buscarDatos(ByVal descripcion As String)
        cFunciones.Llenar_Tabla_Generico("select id,codigo,LEFT(Descripcion,255) As Descripcion,idPadre,Asociable,porcentajeIVA from Cabys where Descripcion like '%" & descripcion & "%'" & " OR codigo like '%" & descripcion & "%'",
                                         Me.DataSetFamilia.Cabys, GetSetting("SeeSoft", "Seepos", "Conexion"))
    End Sub

    Private Sub DataGridBuscar_RowSelect(sender As Object, e As EventArgs) Handles dgvBusqueda.CellClick
        If dgvBusqueda.Rows.Count > 0 Then
            lbIdCabys.Text = dgvBusqueda.CurrentRow.Cells.Item(0).Value
        Else
            lbIdCabys.Text = 0
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If lbIdCabys.Text.Length > 1 Then
            Dim dt As New DataTable
            cFunciones.Llenar_Tabla_Generico("select id from Cabys where codigo = '" & lbIdCabys.Text & "'",
                                         dt, GetSetting("SeeSoft", "Seepos", "Conexion"))
            If dt.Rows.Count > 0 Then
                idCabys = dt.Rows(0).Item("id")
            End If
            Close()
        Else
            MsgBox("Debe elegir un Cabys.", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub txtBuscarDato_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarDato.TextChanged
        If txtBuscarDato.Text.Length > 2 Then
            buscarDatos(txtBuscarDato.Text)
        ElseIf txtBuscarDato.Text = "".ToString Then
            buscarDatos("%%%")
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Close()
    End Sub

    Private Sub dgvBusqueda_CellDoubleClick(sender As Object, e As Windows.Forms.DataGridViewCellEventArgs) Handles dgvBusqueda.CellDoubleClick
        Try
            Dim frm As New frmActualizarCabys
            frm.codigo = dgvBusqueda.CurrentRow.Cells.Item(0).Value
            frm.ShowDialog()
            buscarDatos(txtBuscarDato.Text)
        Catch ex As Exception

        End Try

    End Sub
End Class