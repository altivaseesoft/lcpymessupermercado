﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActualizarCabys
	Inherits System.Windows.Forms.Form

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.txtDescripcionCabysXml = New System.Windows.Forms.TextBox()
		Me.txtCodigoCabysXml = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.txtPorcentajeIVA = New System.Windows.Forms.TextBox()
		Me.btnGuardar = New System.Windows.Forms.Button()
		Me.SuspendLayout()
		'
		'Label3
		'
		Me.Label3.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.ForeColor = System.Drawing.Color.White
		Me.Label3.Location = New System.Drawing.Point(12, 77)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(427, 14)
		Me.Label3.TabIndex = 13
		Me.Label3.Text = "Descripción Cabys"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label2
		'
		Me.Label2.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.ForeColor = System.Drawing.Color.White
		Me.Label2.Location = New System.Drawing.Point(12, 18)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(252, 15)
		Me.Label2.TabIndex = 12
		Me.Label2.Text = "Código Cabys"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'txtDescripcionCabysXml
		'
		Me.txtDescripcionCabysXml.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtDescripcionCabysXml.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDescripcionCabysXml.Location = New System.Drawing.Point(12, 94)
		Me.txtDescripcionCabysXml.Multiline = True
		Me.txtDescripcionCabysXml.Name = "txtDescripcionCabysXml"
		Me.txtDescripcionCabysXml.ReadOnly = True
		Me.txtDescripcionCabysXml.Size = New System.Drawing.Size(427, 52)
		Me.txtDescripcionCabysXml.TabIndex = 11
		'
		'txtCodigoCabysXml
		'
		Me.txtCodigoCabysXml.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtCodigoCabysXml.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCodigoCabysXml.Location = New System.Drawing.Point(12, 35)
		Me.txtCodigoCabysXml.Name = "txtCodigoCabysXml"
		Me.txtCodigoCabysXml.ReadOnly = True
		Me.txtCodigoCabysXml.Size = New System.Drawing.Size(252, 21)
		Me.txtCodigoCabysXml.TabIndex = 10
		Me.txtCodigoCabysXml.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		'
		'Label1
		'
		Me.Label1.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.White
		Me.Label1.Location = New System.Drawing.Point(297, 18)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(142, 15)
		Me.Label1.TabIndex = 15
		Me.Label1.Text = "Porcentaje IVA"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'txtPorcentajeIVA
		'
		Me.txtPorcentajeIVA.BackColor = System.Drawing.SystemColors.ButtonHighlight
		Me.txtPorcentajeIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPorcentajeIVA.Location = New System.Drawing.Point(297, 35)
		Me.txtPorcentajeIVA.Name = "txtPorcentajeIVA"
		Me.txtPorcentajeIVA.Size = New System.Drawing.Size(142, 21)
		Me.txtPorcentajeIVA.TabIndex = 0
		Me.txtPorcentajeIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'btnGuardar
		'
		Me.btnGuardar.BackColor = System.Drawing.SystemColors.ControlDark
		Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
		Me.btnGuardar.Image = Global.LcPymes_5._2.My.Resources.Resources.Guardar_sincolor
		Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.btnGuardar.Location = New System.Drawing.Point(173, 165)
		Me.btnGuardar.Name = "btnGuardar"
		Me.btnGuardar.Size = New System.Drawing.Size(114, 55)
		Me.btnGuardar.TabIndex = 1
		Me.btnGuardar.Text = "Guardar"
		Me.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.btnGuardar.UseVisualStyleBackColor = False
		'
		'frmActualizarCabys
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(454, 246)
		Me.Controls.Add(Me.btnGuardar)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.txtPorcentajeIVA)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.txtDescripcionCabysXml)
		Me.Controls.Add(Me.txtCodigoCabysXml)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
		Me.Name = "frmActualizarCabys"
		Me.ShowIcon = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Actualizar Cabys"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents Label3 As Windows.Forms.Label
	Friend WithEvents Label2 As Windows.Forms.Label
	Friend WithEvents txtDescripcionCabysXml As Windows.Forms.TextBox
	Friend WithEvents txtCodigoCabysXml As Windows.Forms.TextBox
	Friend WithEvents Label1 As Windows.Forms.Label
	Friend WithEvents txtPorcentajeIVA As Windows.Forms.TextBox
	Friend WithEvents btnGuardar As Windows.Forms.Button
End Class
