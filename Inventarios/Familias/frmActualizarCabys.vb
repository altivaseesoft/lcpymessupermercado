﻿Imports System.Data
Imports System.Windows.Forms



Public Class frmActualizarCabys


	Public codigo As String = ""

	Private Sub frmActualizarCabys_Load(sender As Object, e As EventArgs) Handles Me.Load
		spIniciarForm()
	End Sub

	Private Sub spIniciarForm()
		Try
			spCargarDatos()
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub spCargarDatos()
		Try
			Dim sql As String
			Dim dt As New DataTable

			sql = "select * from cabys where codigo = '" & codigo & "'"
			cFunciones.Llenar_Tabla_Generico(sql, dt)
			If dt.Rows.Count > 0 Then
				txtCodigoCabysXml.Text = dt.Rows(0).Item("Codigo")
				txtDescripcionCabysXml.Text = dt.Rows(0).Item("Descripcion")
				txtPorcentajeIVA.Text = dt.Rows(0).Item("porcentajeIVA")
			Else
				txtCodigoCabysXml.Text = ""
				txtDescripcionCabysXml.Text = ""
				txtPorcentajeIVA.Text = ""
			End If
			txtPorcentajeIVA.Focus()
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub
	Private Function fnValidarPorcentaje() As Boolean
		Try
			If Not IsNumeric(txtPorcentajeIVA.Text) Then
				MessageBox.Show("Por favor, ingrese un porcentaje.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning)
				Return False
			End If
			Return True
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Function

	Private Sub spGuardar()
		Try
			If Not fnValidarPorcentaje() Or txtCodigoCabysXml.Text = "" Then Exit Sub

			Dim sql As New SqlClient.SqlCommand
			sql.CommandText = "update cabys set porcentajeIVA = " & txtPorcentajeIVA.Text & " where codigo = '" & codigo & "'"

			cFunciones.spEjecutar(sql, GetSetting("seesoft", "Seepos", "Conexion"))

			MessageBox.Show("La información se guardó exitosamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
			Me.Close()
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
		spGuardar()
	End Sub
End Class