Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Public Class frmCambiarCuentas
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDesc_Articulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEntrada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSalida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colobservacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCuentaContable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombreCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DsAjusteInv2 As LcPymes_5._2.DsAjusteInv
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents op_Salida As System.Windows.Forms.RadioButton
    Friend WithEvents op_Entrada As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents cnxConta As System.Data.SqlClient.SqlConnection
    Friend WithEvents adpASD As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents adpAS As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Asientos_dts1 As LcPymes_5._2.Asientos_dts
    Friend WithEvents lblAsiento As System.Windows.Forms.Label
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents adAjusteInv As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents adAjusteInvDetalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand8 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.DsAjusteInv2 = New LcPymes_5._2.DsAjusteInv
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDesc_Articulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEntrada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSalida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colobservacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCuentaContable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNombreCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label11 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtDescripcion = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.op_Salida = New System.Windows.Forms.RadioButton
        Me.op_Entrada = New System.Windows.Forms.RadioButton
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.lblAsiento = New System.Windows.Forms.Label
        Me.Asientos_dts1 = New LcPymes_5._2.Asientos_dts
        Me.cnxConta = New System.Data.SqlClient.SqlConnection
        Me.adpASD = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.adpAS = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adAjusteInv = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand4 = New System.Data.SqlClient.SqlCommand
        Me.adAjusteInvDetalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand8 = New System.Data.SqlClient.SqlCommand
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsAjusteInv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.Asientos_dts1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "AjusteInventario.AjusteInventarioAjusteInventario_Detalle"
        Me.GridControl1.DataSource = Me.DsAjusteInv2
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 101)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(904, 211)
        Me.GridControl1.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 110
        Me.GridControl1.Text = "GridControl1"
        '
        'DsAjusteInv2
        '
        Me.DsAjusteInv2.DataSetName = "DsAjusteInv"
        Me.DsAjusteInv2.Locale = New System.Globalization.CultureInfo("es-MX")
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCantidad, Me.colDesc_Articulo, Me.colEntrada, Me.colSalida, Me.colobservacion, Me.colCuentaContable, Me.colNombreCuenta})
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsPrint.AutoWidth = False
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colCantidad
        '
        Me.colCantidad.Caption = "Cantidad"
        Me.colCantidad.FieldName = "Cantidad"
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCantidad.VisibleIndex = 1
        Me.colCantidad.Width = 58
        '
        'colDesc_Articulo
        '
        Me.colDesc_Articulo.Caption = "Descripci�n"
        Me.colDesc_Articulo.FieldName = "Desc_Articulo"
        Me.colDesc_Articulo.Name = "colDesc_Articulo"
        Me.colDesc_Articulo.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDesc_Articulo.VisibleIndex = 0
        Me.colDesc_Articulo.Width = 200
        '
        'colEntrada
        '
        Me.colEntrada.Caption = "Entrada"
        Me.colEntrada.FieldName = "Entrada"
        Me.colEntrada.Name = "colEntrada"
        Me.colEntrada.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colEntrada.VisibleIndex = 2
        '
        'colSalida
        '
        Me.colSalida.Caption = "Salida"
        Me.colSalida.FieldName = "Salida"
        Me.colSalida.Name = "colSalida"
        Me.colSalida.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colSalida.VisibleIndex = 3
        '
        'colobservacion
        '
        Me.colobservacion.Caption = "Observaciones"
        Me.colobservacion.FieldName = "observacion"
        Me.colobservacion.Name = "colobservacion"
        Me.colobservacion.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colobservacion.VisibleIndex = 4
        Me.colobservacion.Width = 150
        '
        'colCuentaContable
        '
        Me.colCuentaContable.Caption = "Cuenta Contable"
        Me.colCuentaContable.FieldName = "Cuenta_Contable"
        Me.colCuentaContable.Name = "colCuentaContable"
        Me.colCuentaContable.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCuentaContable.VisibleIndex = 5
        Me.colCuentaContable.Width = 125
        '
        'colNombreCuenta
        '
        Me.colNombreCuenta.Caption = "Nombre Cuenta"
        Me.colNombreCuenta.FieldName = "Nombre_Cuenta"
        Me.colNombreCuenta.Name = "colNombreCuenta"
        Me.colNombreCuenta.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNombreCuenta.VisibleIndex = 6
        Me.colNombreCuenta.Width = 200
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Nombre_Cuenta"))
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Blue
        Me.Label11.Location = New System.Drawing.Point(240, 80)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(536, 16)
        Me.Label11.TabIndex = 161
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Cuenta_Contable"))
        Me.TextBox1.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(32, 80)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(208, 13)
        Me.TextBox1.TabIndex = 159
        Me.TextBox1.Text = ""
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Blue
        Me.Label12.Location = New System.Drawing.Point(32, 64)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(744, 16)
        Me.Label12.TabIndex = 160
        Me.Label12.Text = "Cuenta Contable"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDescripcion
        '
        Me.txtDescripcion.AutoSize = False
        Me.txtDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Desc_Articulo"))
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(32, 32)
        Me.txtDescripcion.MaxLength = 255
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.ReadOnly = True
        Me.txtDescripcion.Size = New System.Drawing.Size(440, 20)
        Me.txtDescripcion.TabIndex = 165
        Me.txtDescripcion.Text = ""
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label4.Location = New System.Drawing.Point(32, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(440, 16)
        Me.Label4.TabIndex = 164
        Me.Label4.Text = "Descripci�n"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.op_Salida)
        Me.GroupBox3.Controls.Add(Me.op_Entrada)
        Me.GroupBox3.Location = New System.Drawing.Point(480, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(296, 48)
        Me.GroupBox3.TabIndex = 166
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Movimientos"
        '
        'op_Salida
        '
        Me.op_Salida.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Salida"))
        Me.op_Salida.Enabled = False
        Me.op_Salida.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.op_Salida.ForeColor = System.Drawing.SystemColors.Highlight
        Me.op_Salida.Location = New System.Drawing.Point(160, 24)
        Me.op_Salida.Name = "op_Salida"
        Me.op_Salida.Size = New System.Drawing.Size(128, 16)
        Me.op_Salida.TabIndex = 1
        Me.op_Salida.Text = "Salida de Inventario"
        '
        'op_Entrada
        '
        Me.op_Entrada.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Entrada"))
        Me.op_Entrada.Enabled = False
        Me.op_Entrada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.op_Entrada.ForeColor = System.Drawing.SystemColors.Highlight
        Me.op_Entrada.Location = New System.Drawing.Point(8, 24)
        Me.op_Entrada.Name = "op_Entrada"
        Me.op_Entrada.Size = New System.Drawing.Size(136, 15)
        Me.op_Entrada.TabIndex = 0
        Me.op_Entrada.Text = "&Entrada al Inventario"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(568, 320)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(336, 48)
        Me.Button1.TabIndex = 167
        Me.Button1.Text = "Cancelar(Salir)"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(232, 320)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(336, 48)
        Me.Button2.TabIndex = 168
        Me.Button2.Text = "Aceptar(Guardar)"
        '
        'lblAsiento
        '
        Me.lblAsiento.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Asientos_dts1, "DetallesAsientosContable.NumAsiento"))
        Me.lblAsiento.Location = New System.Drawing.Point(784, 24)
        Me.lblAsiento.Name = "lblAsiento"
        Me.lblAsiento.Size = New System.Drawing.Size(112, 24)
        Me.lblAsiento.TabIndex = 169
        '
        'Asientos_dts1
        '
        Me.Asientos_dts1.DataSetName = "Asientos_dts"
        Me.Asientos_dts1.Locale = New System.Globalization.CultureInfo("es-ES")
        '
        'cnxConta
        '
        Me.cnxConta.ConnectionString = "workstation id=""ROLANDO-PC"";packet size=4096;integrated security=SSPI;data source" & _
        "="".\SQL2008R2"";persist security info=False;initial catalog=Contabilidad"
        '
        'adpASD
        '
        Me.adpASD.DeleteCommand = Me.SqlDeleteCommand2
        Me.adpASD.InsertCommand = Me.SqlInsertCommand2
        Me.adpASD.SelectCommand = Me.SqlSelectCommand2
        Me.adpASD.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "DetallesAsientosContable", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ID_Detalle", "ID_Detalle"), New System.Data.Common.DataColumnMapping("NumAsiento", "NumAsiento"), New System.Data.Common.DataColumnMapping("Cuenta", "Cuenta"), New System.Data.Common.DataColumnMapping("NombreCuenta", "NombreCuenta"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Debe", "Debe"), New System.Data.Common.DataColumnMapping("Haber", "Haber"), New System.Data.Common.DataColumnMapping("DescripcionAsiento", "DescripcionAsiento"), New System.Data.Common.DataColumnMapping("Tipocambio", "Tipocambio")})})
        Me.adpASD.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM DetallesAsientosContable WHERE (ID_Detalle = @Original_ID_Detalle) AN" & _
        "D (Cuenta = @Original_Cuenta) AND (Debe = @Original_Debe) AND (DescripcionAsient" & _
        "o = @Original_DescripcionAsiento) AND (Haber = @Original_Haber) AND (Monto = @Or" & _
        "iginal_Monto) AND (NombreCuenta = @Original_NombreCuenta) AND (NumAsiento = @Ori" & _
        "ginal_NumAsiento) AND (Tipocambio = @Original_Tipocambio OR @Original_Tipocambio" & _
        " IS NULL AND Tipocambio IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.cnxConta
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO DetallesAsientosContable(NumAsiento, Cuenta, NombreCuenta, Monto, Deb" & _
        "e, Haber, DescripcionAsiento, Tipocambio) VALUES (@NumAsiento, @Cuenta, @NombreC" & _
        "uenta, @Monto, @Debe, @Haber, @DescripcionAsiento, @Tipocambio); SELECT ID_Detal" & _
        "le, NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Ti" & _
        "pocambio FROM DetallesAsientosContable WHERE (ID_Detalle = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.cnxConta
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT ID_Detalle, NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, Descripc" & _
        "ionAsiento, Tipocambio FROM DetallesAsientosContable"
        Me.SqlSelectCommand2.Connection = Me.cnxConta
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE DetallesAsientosContable SET NumAsiento = @NumAsiento, Cuenta = @Cuenta, N" & _
        "ombreCuenta = @NombreCuenta, Monto = @Monto, Debe = @Debe, Haber = @Haber, Descr" & _
        "ipcionAsiento = @DescripcionAsiento, Tipocambio = @Tipocambio WHERE (ID_Detalle " & _
        "= @Original_ID_Detalle) AND (Cuenta = @Original_Cuenta) AND (Debe = @Original_De" & _
        "be) AND (DescripcionAsiento = @Original_DescripcionAsiento) AND (Haber = @Origin" & _
        "al_Haber) AND (Monto = @Original_Monto) AND (NombreCuenta = @Original_NombreCuen" & _
        "ta) AND (NumAsiento = @Original_NumAsiento) AND (Tipocambio = @Original_Tipocamb" & _
        "io OR @Original_Tipocambio IS NULL AND Tipocambio IS NULL); SELECT ID_Detalle, N" & _
        "umAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Tipocam" & _
        "bio FROM DetallesAsientosContable WHERE (ID_Detalle = @ID_Detalle)"
        Me.SqlUpdateCommand2.Connection = Me.cnxConta
        '
        'adpAS
        '
        Me.adpAS.DeleteCommand = Me.SqlDeleteCommand1
        Me.adpAS.InsertCommand = Me.SqlInsertCommand1
        Me.adpAS.SelectCommand = Me.SqlSelectCommand1
        Me.adpAS.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AsientosContables", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumAsiento", "NumAsiento"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("IdNumDoc", "IdNumDoc"), New System.Data.Common.DataColumnMapping("NumDoc", "NumDoc"), New System.Data.Common.DataColumnMapping("Beneficiario", "Beneficiario"), New System.Data.Common.DataColumnMapping("TipoDoc", "TipoDoc"), New System.Data.Common.DataColumnMapping("Accion", "Accion"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("FechaEntrada", "FechaEntrada"), New System.Data.Common.DataColumnMapping("Mayorizado", "Mayorizado"), New System.Data.Common.DataColumnMapping("Periodo", "Periodo"), New System.Data.Common.DataColumnMapping("NumMayorizado", "NumMayorizado"), New System.Data.Common.DataColumnMapping("Modulo", "Modulo"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario"), New System.Data.Common.DataColumnMapping("TotalDebe", "TotalDebe"), New System.Data.Common.DataColumnMapping("TotalHaber", "TotalHaber"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio")})})
        Me.adpAS.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM AsientosContables WHERE (NumAsiento = @Original_NumAsiento) AND (Acci" & _
        "on = @Original_Accion) AND (Anulado = @Original_Anulado) AND (Beneficiario = @Or" & _
        "iginal_Beneficiario) AND (CodMoneda = @Original_CodMoneda) AND (Fecha = @Origina" & _
        "l_Fecha) AND (FechaEntrada = @Original_FechaEntrada) AND (IdNumDoc = @Original_I" & _
        "dNumDoc) AND (Mayorizado = @Original_Mayorizado) AND (Modulo = @Original_Modulo)" & _
        " AND (NombreUsuario = @Original_NombreUsuario) AND (NumDoc = @Original_NumDoc) A" & _
        "ND (NumMayorizado = @Original_NumMayorizado) AND (Observaciones = @Original_Obse" & _
        "rvaciones) AND (Periodo = @Original_Periodo) AND (TipoCambio = @Original_TipoCam" & _
        "bio) AND (TipoDoc = @Original_TipoDoc) AND (TotalDebe = @Original_TotalDebe) AND" & _
        " (TotalHaber = @Original_TotalHaber)"
        Me.SqlDeleteCommand1.Connection = Me.cnxConta
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO AsientosContables(NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, " & _
        "TipoDoc, Accion, Anulado, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modu" & _
        "lo, Observaciones, NombreUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio) " & _
        "VALUES (@NumAsiento, @Fecha, @IdNumDoc, @NumDoc, @Beneficiario, @TipoDoc, @Accio" & _
        "n, @Anulado, @FechaEntrada, @Mayorizado, @Periodo, @NumMayorizado, @Modulo, @Obs" & _
        "ervaciones, @NombreUsuario, @TotalDebe, @TotalHaber, @CodMoneda, @TipoCambio); S" & _
        "ELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDoc, Accion, Anulad" & _
        "o, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Observaciones, Nomb" & _
        "reUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM AsientosContables W" & _
        "HERE (NumAsiento = @NumAsiento)"
        Me.SqlInsertCommand1.Connection = Me.cnxConta
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDoc, Accion, Anulad" & _
        "o, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Observaciones, Nomb" & _
        "reUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM AsientosContables"
        Me.SqlSelectCommand1.Connection = Me.cnxConta
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE AsientosContables SET NumAsiento = @NumAsiento, Fecha = @Fecha, IdNumDoc =" & _
        " @IdNumDoc, NumDoc = @NumDoc, Beneficiario = @Beneficiario, TipoDoc = @TipoDoc, " & _
        "Accion = @Accion, Anulado = @Anulado, FechaEntrada = @FechaEntrada, Mayorizado =" & _
        " @Mayorizado, Periodo = @Periodo, NumMayorizado = @NumMayorizado, Modulo = @Modu" & _
        "lo, Observaciones = @Observaciones, NombreUsuario = @NombreUsuario, TotalDebe = " & _
        "@TotalDebe, TotalHaber = @TotalHaber, CodMoneda = @CodMoneda, TipoCambio = @Tipo" & _
        "Cambio WHERE (NumAsiento = @Original_NumAsiento) AND (Accion = @Original_Accion)" & _
        " AND (Anulado = @Original_Anulado) AND (Beneficiario = @Original_Beneficiario) A" & _
        "ND (CodMoneda = @Original_CodMoneda) AND (Fecha = @Original_Fecha) AND (FechaEnt" & _
        "rada = @Original_FechaEntrada) AND (IdNumDoc = @Original_IdNumDoc) AND (Mayoriza" & _
        "do = @Original_Mayorizado) AND (Modulo = @Original_Modulo) AND (NombreUsuario = " & _
        "@Original_NombreUsuario) AND (NumDoc = @Original_NumDoc) AND (NumMayorizado = @O" & _
        "riginal_NumMayorizado) AND (Observaciones = @Original_Observaciones) AND (Period" & _
        "o = @Original_Periodo) AND (TipoCambio = @Original_TipoCambio) AND (TipoDoc = @O" & _
        "riginal_TipoDoc) AND (TotalDebe = @Original_TotalDebe) AND (TotalHaber = @Origin" & _
        "al_TotalHaber); SELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDo" & _
        "c, Accion, Anulado, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Ob" & _
        "servaciones, NombreUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM As" & _
        "ientosContables WHERE (NumAsiento = @NumAsiento)"
        Me.SqlUpdateCommand1.Connection = Me.cnxConta
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=IALVAREZ;packet size=4096;integrated security=SSPI;data source=""25" & _
        ".148.249.133"";persist security info=False;initial catalog=SEEPOS"
        '
        'adAjusteInv
        '
        Me.adAjusteInv.DeleteCommand = Me.SqlCommand1
        Me.adAjusteInv.InsertCommand = Me.SqlCommand2
        Me.adAjusteInv.SelectCommand = Me.SqlCommand3
        Me.adAjusteInv.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AjusteInventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Consecutivo", "Consecutivo"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Anula", "Anula"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("TotalEntrada", "TotalEntrada"), New System.Data.Common.DataColumnMapping("TotalSalida", "TotalSalida"), New System.Data.Common.DataColumnMapping("SaldoAjuste", "SaldoAjuste")})})
        Me.adAjusteInv.UpdateCommand = Me.SqlCommand4
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "DELETE FROM AjusteInventario WHERE (Consecutivo = @Original_Consecutivo) AND (Anu" & _
        "la = @Original_Anula) AND (Cedula = @Original_Cedula) AND (Fecha = @Original_Fec" & _
        "ha) AND (SaldoAjuste = @Original_SaldoAjuste) AND (TotalEntrada = @Original_Tota" & _
        "lEntrada) AND (TotalSalida = @Original_TotalSalida)"
        Me.SqlCommand1.Connection = Me.SqlConnection1
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SaldoAjuste", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SaldoAjuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = "INSERT INTO AjusteInventario(Fecha, Anula, Cedula, TotalEntrada, TotalSalida, Sal" & _
        "doAjuste) VALUES (@Fecha, @Anula, @Cedula, @TotalEntrada, @TotalSalida, @SaldoAj" & _
        "uste); SELECT Consecutivo, Fecha, Anula, Cedula, TotalEntrada, TotalSalida, Sald" & _
        "oAjuste FROM AjusteInventario WHERE (Consecutivo = @@IDENTITY)"
        Me.SqlCommand2.Connection = Me.SqlConnection1
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 1, "Anula"))
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 150, "Cedula"))
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SaldoAjuste", System.Data.SqlDbType.Float, 8, "SaldoAjuste"))
        '
        'SqlCommand3
        '
        Me.SqlCommand3.CommandText = "SELECT Consecutivo, Fecha, Anula, Cedula, TotalEntrada, TotalSalida, SaldoAjuste " & _
        "FROM AjusteInventario"
        Me.SqlCommand3.Connection = Me.SqlConnection1
        '
        'SqlCommand4
        '
        Me.SqlCommand4.CommandText = "UPDATE AjusteInventario SET Fecha = @Fecha, Anula = @Anula, Cedula = @Cedula, Tot" & _
        "alEntrada = @TotalEntrada, TotalSalida = @TotalSalida, SaldoAjuste = @SaldoAjust" & _
        "e WHERE (Consecutivo = @Original_Consecutivo) AND (Anula = @Original_Anula) AND " & _
        "(Cedula = @Original_Cedula) AND (Fecha = @Original_Fecha) AND (SaldoAjuste = @Or" & _
        "iginal_SaldoAjuste) AND (TotalEntrada = @Original_TotalEntrada) AND (TotalSalida" & _
        " = @Original_TotalSalida); SELECT Consecutivo, Fecha, Anula, Cedula, TotalEntrad" & _
        "a, TotalSalida, SaldoAjuste FROM AjusteInventario WHERE (Consecutivo = @Consecut" & _
        "ivo)"
        Me.SqlCommand4.Connection = Me.SqlConnection1
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 1, "Anula"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 150, "Cedula"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SaldoAjuste", System.Data.SqlDbType.Float, 8, "SaldoAjuste"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SaldoAjuste", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SaldoAjuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consecutivo", System.Data.SqlDbType.BigInt, 8, "Consecutivo"))
        '
        'adAjusteInvDetalle
        '
        Me.adAjusteInvDetalle.DeleteCommand = Me.SqlCommand5
        Me.adAjusteInvDetalle.InsertCommand = Me.SqlCommand6
        Me.adAjusteInvDetalle.SelectCommand = Me.SqlCommand7
        Me.adAjusteInvDetalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AjusteInventario_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Consecutivo", "Consecutivo"), New System.Data.Common.DataColumnMapping("Cons_Ajuste", "Cons_Ajuste"), New System.Data.Common.DataColumnMapping("Cod_Articulo", "Cod_Articulo"), New System.Data.Common.DataColumnMapping("IdBodega", "IdBodega"), New System.Data.Common.DataColumnMapping("Desc_Articulo", "Desc_Articulo"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Entrada", "Entrada"), New System.Data.Common.DataColumnMapping("Salida", "Salida"), New System.Data.Common.DataColumnMapping("observacion", "observacion"), New System.Data.Common.DataColumnMapping("Cuenta_Contable", "Cuenta_Contable"), New System.Data.Common.DataColumnMapping("Nombre_Cuenta", "Nombre_Cuenta"), New System.Data.Common.DataColumnMapping("TotalEntrada", "TotalEntrada"), New System.Data.Common.DataColumnMapping("TotalSalida", "TotalSalida"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("CostoUnit", "CostoUnit"), New System.Data.Common.DataColumnMapping("Barras", "Barras")})})
        Me.adAjusteInvDetalle.UpdateCommand = Me.SqlCommand8
        '
        'SqlCommand5
        '
        Me.SqlCommand5.CommandText = "DELETE FROM AjusteInventario_Detalle WHERE (Consecutivo = @Original_Consecutivo) " & _
        "AND (Barras = @Original_Barras OR @Original_Barras IS NULL AND Barras IS NULL) A" & _
        "ND (Cantidad = @Original_Cantidad) AND (Cod_Articulo = @Original_Cod_Articulo) A" & _
        "ND (Cons_Ajuste = @Original_Cons_Ajuste) AND (CostoUnit = @Original_CostoUnit) A" & _
        "ND (Cuenta_Contable = @Original_Cuenta_Contable) AND (Desc_Articulo = @Original_" & _
        "Desc_Articulo) AND (Entrada = @Original_Entrada) AND (Existencia = @Original_Exi" & _
        "stencia) AND (IdBodega = @Original_IdBodega) AND (Nombre_Cuenta = @Original_Nomb" & _
        "re_Cuenta) AND (Salida = @Original_Salida) AND (TotalEntrada = @Original_TotalEn" & _
        "trada) AND (TotalSalida = @Original_TotalSalida) AND (observacion = @Original_ob" & _
        "servacion)"
        Me.SqlCommand5.Connection = Me.SqlConnection1
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Articulo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cons_Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoUnit", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoUnit", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Contable", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Contable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Desc_Articulo", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Desc_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Entrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdBodega", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Salida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Salida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_observacion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observacion", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand6
        '
        Me.SqlCommand6.CommandText = "INSERT INTO AjusteInventario_Detalle(Cons_Ajuste, Cod_Articulo, IdBodega, Desc_Ar" & _
        "ticulo, Cantidad, Entrada, Salida, observacion, Cuenta_Contable, Nombre_Cuenta, " & _
        "TotalEntrada, TotalSalida, Existencia, CostoUnit, Barras) VALUES (@Cons_Ajuste, " & _
        "@Cod_Articulo, @IdBodega, @Desc_Articulo, @Cantidad, @Entrada, @Salida, @observa" & _
        "cion, @Cuenta_Contable, @Nombre_Cuenta, @TotalEntrada, @TotalSalida, @Existencia" & _
        ", @CostoUnit, @Barras); SELECT Consecutivo, Cons_Ajuste, Cod_Articulo, IdBodega," & _
        " Desc_Articulo, Cantidad, Entrada, Salida, observacion, Cuenta_Contable, Nombre_" & _
        "Cuenta, TotalEntrada, TotalSalida, Existencia, CostoUnit, Barras FROM AjusteInve" & _
        "ntario_Detalle WHERE (Consecutivo = @@IDENTITY)"
        Me.SqlCommand6.Connection = Me.SqlConnection1
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, "Cons_Ajuste"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdBodega", System.Data.SqlDbType.BigInt, 8, "IdBodega"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Desc_Articulo", System.Data.SqlDbType.VarChar, 250, "Desc_Articulo"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Entrada", System.Data.SqlDbType.Bit, 1, "Entrada"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Salida", System.Data.SqlDbType.Bit, 1, "Salida"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@observacion", System.Data.SqlDbType.VarChar, 250, "observacion"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta_Contable", System.Data.SqlDbType.VarChar, 100, "Cuenta_Contable"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Cuenta", System.Data.SqlDbType.VarChar, 250, "Nombre_Cuenta"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoUnit", System.Data.SqlDbType.Float, 8, "CostoUnit"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 50, "Barras"))
        '
        'SqlCommand7
        '
        Me.SqlCommand7.CommandText = "SELECT Consecutivo, Cons_Ajuste, Cod_Articulo, IdBodega, Desc_Articulo, Cantidad," & _
        " Entrada, Salida, observacion, Cuenta_Contable, Nombre_Cuenta, TotalEntrada, Tot" & _
        "alSalida, Existencia, CostoUnit, Barras FROM AjusteInventario_Detalle"
        Me.SqlCommand7.Connection = Me.SqlConnection1
        '
        'SqlCommand8
        '
        Me.SqlCommand8.CommandText = "UPDATE AjusteInventario_Detalle SET Cons_Ajuste = @Cons_Ajuste, Cod_Articulo = @C" & _
        "od_Articulo, IdBodega = @IdBodega, Desc_Articulo = @Desc_Articulo, Cantidad = @C" & _
        "antidad, Entrada = @Entrada, Salida = @Salida, observacion = @observacion, Cuent" & _
        "a_Contable = @Cuenta_Contable, Nombre_Cuenta = @Nombre_Cuenta, TotalEntrada = @T" & _
        "otalEntrada, TotalSalida = @TotalSalida, Existencia = @Existencia, CostoUnit = @" & _
        "CostoUnit, Barras = @Barras WHERE (Consecutivo = @Original_Consecutivo) AND (Bar" & _
        "ras = @Original_Barras OR @Original_Barras IS NULL AND Barras IS NULL) AND (Cant" & _
        "idad = @Original_Cantidad) AND (Cod_Articulo = @Original_Cod_Articulo) AND (Cons" & _
        "_Ajuste = @Original_Cons_Ajuste) AND (CostoUnit = @Original_CostoUnit) AND (Cuen" & _
        "ta_Contable = @Original_Cuenta_Contable) AND (Desc_Articulo = @Original_Desc_Art" & _
        "iculo) AND (Entrada = @Original_Entrada) AND (Existencia = @Original_Existencia)" & _
        " AND (IdBodega = @Original_IdBodega) AND (Nombre_Cuenta = @Original_Nombre_Cuent" & _
        "a) AND (Salida = @Original_Salida) AND (TotalEntrada = @Original_TotalEntrada) A" & _
        "ND (TotalSalida = @Original_TotalSalida) AND (observacion = @Original_observacio" & _
        "n); SELECT Consecutivo, Cons_Ajuste, Cod_Articulo, IdBodega, Desc_Articulo, Cant" & _
        "idad, Entrada, Salida, observacion, Cuenta_Contable, Nombre_Cuenta, TotalEntrada" & _
        ", TotalSalida, Existencia, CostoUnit, Barras FROM AjusteInventario_Detalle WHERE" & _
        " (Consecutivo = @Consecutivo)"
        Me.SqlCommand8.Connection = Me.SqlConnection1
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, "Cons_Ajuste"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdBodega", System.Data.SqlDbType.BigInt, 8, "IdBodega"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Desc_Articulo", System.Data.SqlDbType.VarChar, 250, "Desc_Articulo"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Entrada", System.Data.SqlDbType.Bit, 1, "Entrada"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Salida", System.Data.SqlDbType.Bit, 1, "Salida"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@observacion", System.Data.SqlDbType.VarChar, 250, "observacion"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta_Contable", System.Data.SqlDbType.VarChar, 100, "Cuenta_Contable"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Cuenta", System.Data.SqlDbType.VarChar, 250, "Nombre_Cuenta"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoUnit", System.Data.SqlDbType.Float, 8, "CostoUnit"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 50, "Barras"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Articulo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cons_Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoUnit", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoUnit", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Contable", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Contable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Desc_Articulo", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Desc_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Entrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdBodega", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Salida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Salida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_observacion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consecutivo", System.Data.SqlDbType.BigInt, 8, "Consecutivo"))
        '
        'frmCambiarCuentas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(904, 375)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblAsiento)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.GridControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmCambiarCuentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambiar Cuentas Contables"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsAjusteInv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.Asientos_dts1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Id As String = "0"
    Public CXP As Boolean = False
    Public Imp As Decimal
    Public fecha As Date
    Public CodigoProv As String

    Private Sub CargarDatos(ByVal _cons_Ajuste As String)
        Dim dts As New DataTable
        cFunciones.Llenar_Tabla_Generico("Select * from AjusteInventario where Consecutivo = " & _cons_Ajuste, DsAjusteInv2.AjusteInventario, GetSetting("SeeSOFT", "SeePOs", "Conexion"))
        cFunciones.Llenar_Tabla_Generico("Select * from AjusteInventario_Detalle where Cons_Ajuste = " & _cons_Ajuste, DsAjusteInv2.AjusteInventario_Detalle, GetSetting("SeeSOFT", "SeePOs", "Conexion"))
        cFunciones.Llenar_Tabla_Generico("Select * from AjusteInventario where Consecutivo = " & _cons_Ajuste, dts, GetSetting("SeeSOFT", "SeeSOFT", "Conexion"))
        If dts.Rows.Count > 0 Then
            Me.lblAsiento.Text = dts.Rows(0).Item("AsientoEntrada") 'AsientoEntrada
            Me.CXP = dts.Rows(0).Item("CXP")
            Me.Imp = dts.Rows(0).Item("Imp")
            Me.fecha = dts.Rows(0).Item("Fecha")
            Me.CodigoProv = dts.Rows(0).Item("CodigoProv")
            cFunciones.Llenar_Tabla_Generico("select * from AsientosContables where NumAsiento = '" & Me.lblAsiento.Text & "'", Me.Asientos_dts1.AsientosContables, GetSetting("SeeSOFT", "contabilidad", "Conexion"))
            cFunciones.Llenar_Tabla_Generico("select * from DetallesAsientosContable where NumAsiento = '" & Me.lblAsiento.Text & "'", Me.Asientos_dts1.DetallesAsientosContable, GetSetting("SeeSOFT", "contabilidad", "Conexion"))
        End If

        For i As Integer = 0 To Me.DsAjusteInv2.AjusteInventario_Detalle.Count - 1
            Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Position = i
            Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()
        Next
    End Sub

    Private Sub frmCambiarCuentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Id <> "0" Then
            Me.cnxConta.ConnectionString = GetSetting("SeeSOFT", "Contabilidad", "Conexion")
            Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePOs", "Conexion")
            Me.CargarDatos(Me.Id)
        Else
            Me.Close()
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim Fx As New cFunciones
            Dim Id As String
            Id = Fx.BuscarDatos("SELECT CuentaContable, Descripcion FROM cuentacontable where (Movimiento = 1) ", "Descripcion", "Buscar Cuenta Contable .....", GetSetting("Seesoft", "Contabilidad", "Conexion"))

            If Id = "" Then
                Exit Sub
            End If

            TextBox1.Text = Id
            If CargarInformacionCuentaContable(Me.TextBox1.Text) Then
            Else
                Me.TextBox1.Focus()
            End If
        End If
        If e.KeyCode = Keys.Enter Then
            If Me.CargarInformacionCuentaContable(Me.TextBox1.Text) = True Then
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Cuenta_Contable") = Me.TextBox1.Text
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Nombre_Cuenta") = Label11.Text
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()
            End If
        End If
    End Sub
    Private cConexion As New Conexion
    Private sqlConexion As SqlConnection
    Function CargarInformacionCuentaContable(ByVal codigo As String) As Boolean
        Dim rs As SqlDataReader
        Dim Encontrado As Boolean
        If codigo <> Nothing Then
            sqlConexion = cConexion.Conectar("Contabilidad")
            rs = cConexion.GetRecorset(sqlConexion, "SELECT CuentaContable, Descripcion FROM  CuentaContable where CuentaContable = '" & codigo & "'")
            Encontrado = False
            While rs.Read
                Try
                    TextBox1.Text = rs("CuentaContable")
                    Label11.Text = rs("Descripcion")
                    Encontrado = True
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    cConexion.DesConectar(cConexion.Conectar("Contabilidad"))
                End Try
            End While
            rs.Close()

            If Not Encontrado Then
                MsgBox("No existe Esta Cuenta Contable", MsgBoxStyle.Exclamation)
                cConexion.DesConectar(cConexion.Conectar("Contabilidad"))
                Return False
            End If
            cConexion.DesConectar(cConexion.Conectar("Contabilidad"))
            Return True
        End If
    End Function

    Private Sub Regenerar_Asiento()
        Dim db As New GestioDatos
        db.Ejecuta("delete from contabilidad.dbo.DetallesAsientosContable where NumAsiento = '" & Me.lblAsiento.Text & "'")
        Me.Asientos_dts1.DetallesAsientosContable.Clear()

        Dim fx As New cFunciones
        Dim tc As Double = 0
        Dim cmd As New SqlClient.SqlCommand

        Dim cuentaInv As String = ""
        Dim cuentaInvNom As String = ""
        Dim dtConf As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT CuentaContable.CuentaContable, CuentaContable.Descripcion FROM SettingCuentaContable INNER JOIN  CuentaContable ON SettingCuentaContable.IdCompraGrabado = CuentaContable.id", dtConf, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
        If dtConf.Rows.Count > 0 Then
            cuentaInv = dtConf.Rows(0).Item("CuentaContable")
            cuentaInvNom = dtConf.Rows(0).Item("Descripcion")
        End If

        Dim cIv As String = ""
        Dim cDIv As String = ""
        cFunciones.Llenar_Tabla_Generico("SELECT c.CuentaContable, c.Descripcion FROM CuentaContable AS c INNER JOIN    SettingCuentaContable AS s ON c.id = s.IdImpuestoVenta ", dtConf, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
        If dtConf.Rows.Count > 0 Then
            cIv = dtConf.Rows(0).Item("CuentaContable")
            cDIv = dtConf.Rows(0).Item("Descripcion")
        End If

        If Me.CXP = True Then
            Dim dtProv As New DataTable
            cFunciones.Llenar_Tabla_Generico("SELECT CuentaContable, DescripcionCuentaContable FROM Proveedores WHERE (CodigoProv = " & Me.CodigoProv & ")", dtProv)
            If dtProv.Rows.Count > 0 Then
                GuardaAsientoDetalle(Me.Imp, True, False, dtProv.Rows(0).Item("CuentaContable"), dtProv.Rows(0).Item("DescripcionCuentaContable"), tc)
                GuardaAsientoDetalle(Me.Imp, False, True, cIv, cDIv, tc)
            End If
        End If

        Dim _dt As New DataTable
        cmd.CommandText = "SELECT Cuenta_Contable, Nombre_Cuenta, TotalEntrada + TotalSalida AS Monto, Salida, observacion  FROM AjusteInventario_Detalle WHERE (Cons_Ajuste = " & Me.Id & ")"
        cFunciones.Llenar_Tabla_Generico(cmd.CommandText, _dt, GetSetting("SeeSoft", "SeePOS", "Conexion"))
        'If _dt.Rows.Count > 0 Then
        '    .Current("Observaciones") &= ". " & _dt.Rows(0).Item("observacion")
        '    .EndCurrentEdit()
        'End If
        For id As Integer = 0 To _dt.Rows.Count - 1
            If _dt.Rows(id).Item("Salida") Then
                GuardaAsientoDetalle(_dt.Rows(id).Item("Monto"), True, False, _dt.Rows(id).Item("Cuenta_Contable"), _dt.Rows(id).Item("Nombre_Cuenta"), tc)
                GuardaAsientoDetalle(_dt.Rows(id).Item("Monto"), False, True, cuentaInv, cuentaInvNom, tc)
            Else
                GuardaAsientoDetalle(_dt.Rows(id).Item("Monto"), True, False, cuentaInv, cuentaInvNom, tc)
                GuardaAsientoDetalle(_dt.Rows(id).Item("Monto"), False, True, _dt.Rows(id).Item("Cuenta_Contable"), _dt.Rows(id).Item("Nombre_Cuenta"), tc)
            End If
        Next
        Me.sp_totalesAsiento(BindingContext(Me.Asientos_dts1, "AsientosContables").Current("NumAsiento"))
    End Sub
    Sub sp_totalesAsiento(ByVal numAs As String)
        Dim tDebe As Double = 0
        Dim tHaber As Double = 0
        For i As Integer = 0 To Me.Asientos_dts1.DetallesAsientosContable.Count - 1
            If Me.Asientos_dts1.DetallesAsientosContable(i).NumAsiento = numAs Then
                If Me.Asientos_dts1.DetallesAsientosContable(i).Debe Then
                    tDebe += Me.Asientos_dts1.DetallesAsientosContable(i).Monto
                Else
                    tHaber += Me.Asientos_dts1.DetallesAsientosContable(i).Monto
                End If
            End If
        Next
        BindingContext(Me.Asientos_dts1, "AsientosContables").Current("TotalDebe") = tDebe
        BindingContext(Me.Asientos_dts1, "AsientosContables").Current("TotalHaber") = tHaber
        'BindingContext(Me.Asientos_dts1, "AsientosContables").Current("Dif") = tDebe - tHaber
        BindingContext(Me.Asientos_dts1, "AsientosContables").EndCurrentEdit()
    End Sub
    Public Function GuardaAsientoDetalle(ByVal Monto As Double, ByVal Debe As Boolean, ByVal Haber As Boolean, ByVal Cuenta As String, ByVal NombreCuenta As String, ByVal TipoCambio As Double) As Boolean
        Try
            If Monto <> 0 And (Not Cuenta.Equals("0")) And (Not Cuenta.Equals("")) Then
                If engrosarlacuenta(Monto, Debe, Haber, Cuenta, NombreCuenta) Then
                    Return True
                End If
                'CREA LOS DETALLES DE ASIENTOS CONTABLES
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").EndCurrentEdit()
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").AddNew()
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").Current("NumAsiento") = BindingContext(Me.Asientos_dts1, "AsientosContables").Current("NumAsiento")
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").Current("DescripcionAsiento") = BindingContext(Me.Asientos_dts1, "AsientosContables").Current("Observaciones")
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").Current("Cuenta") = Cuenta
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").Current("NombreCuenta") = NombreCuenta
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").Current("Monto") = Monto
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").Current("Debe") = Debe
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").Current("Haber") = Haber
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").Current("Tipocambio") = TipoCambio
                BindingContext(Me.Asientos_dts1, "DetallesAsientosContable").EndCurrentEdit()
            Else
                Return False
            End If
        Catch ex As System.Exception
            'MsgBox("ERROR A INCLUIR DATO: " & ex.ToString, MsgBoxStyle.Information, "Atenci�n...")
            BindingContext(Asientos_dts1, "DetallesAsientosContable").CancelCurrentEdit()
            Return False
        End Try
        Return True
    End Function
    Function engrosarlacuenta(ByVal Monto As Double, ByVal Debe As Boolean, ByVal Haber As Boolean, ByVal Cuenta As String, ByVal NombreCuenta As String) As Boolean
        Try
            For i As Integer = 0 To Me.Asientos_dts1.DetallesAsientosContable.Count - 1
                If BindingContext(Me.Asientos_dts1, "AsientosContables").Current("NumAsiento") = Me.Asientos_dts1.DetallesAsientosContable(i).NumAsiento And Me.Asientos_dts1.DetallesAsientosContable(i).Cuenta = Cuenta And Me.Asientos_dts1.DetallesAsientosContable(i).Debe = Debe And Me.Asientos_dts1.DetallesAsientosContable(i).Haber = Haber Then
                    Me.Asientos_dts1.DetallesAsientosContable(i).Monto += Monto
                    Return True
                End If
            Next
            Return False
        Catch ex As Exception
            ' MsgBox(ex.ToString, MsgBoxStyle.Critical)
            Return False
        End Try
    End Function
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Sub sp_guardarAsiento()
        If MsgBox("�Desea guardar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        Try
            'Me.btnGuardar.Enabled = False
            If TransAsiento() Then
                'sp_actualizar_Datos()
            End If
        Catch ex As Exception
            'Me.btnGuardar.Enabled = True
            MsgBox(ex.ToString, MsgBoxStyle.OKOnly)
        End Try
    End Sub
    Private Sub spTrans()
        Dim db As New GestioDatos
        With BindingContext(Asientos_dts1, "DetallesAsientosContable")
            For i As Integer = 0 To .Count - 1
                .Position = i
                Dim debe, haber As Integer
                debe = IIf(.Current("Debe") = True, 1, 0)
                haber = IIf(.Current("Haber") = True, 1, 0)
                db.Ejecuta("INSERT INTO Contabilidad.dbo.DetallesAsientosContable(NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Tipocambio) Values('" & .Current("NumAsiento") & "', '" & .Current("Cuenta") & "', '" & .Current("NombreCuenta") & "', " & .Current("Monto") & ", " & debe & ", " & haber & ", '" & .Current("DescripcionAsiento") & "', " & .Current("Tipocambio") & ")")
            Next
        End With
    End Sub

    Function TransAsiento() As Boolean
        Me.spTrans()
    End Function

    Function Registrar_Ajuste() As Boolean
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            Me.adAjusteInv.InsertCommand.Transaction = Trans
            Me.adAjusteInv.DeleteCommand.Transaction = Trans
            Me.adAjusteInv.UpdateCommand.Transaction = Trans
            Me.adAjusteInv.SelectCommand.Transaction = Trans
            Me.adAjusteInvDetalle.InsertCommand.Transaction = Trans
            Me.adAjusteInvDetalle.DeleteCommand.Transaction = Trans
            Me.adAjusteInvDetalle.UpdateCommand.Transaction = Trans
            Me.adAjusteInvDetalle.SelectCommand.Transaction = Trans
            Me.adAjusteInv.Update(Me.DsAjusteInv2, "AjusteInventario")
            Me.adAjusteInvDetalle.Update(Me.DsAjusteInv2, "AjusteInventario_Detalle")
            Trans.Commit()
            Return True
        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Me.Registrar_Ajuste()
            If Me.lblAsiento.Text <> "0" And Me.lblAsiento.Text <> "" Then
                Me.Regenerar_Asiento()
                Me.sp_guardarAsiento()
            End If
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Me.Close()
        End Try
    End Sub
End Class
