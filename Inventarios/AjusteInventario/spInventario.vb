Imports System.Data
Imports System.Data.SqlClient
Public Class spInventario

    Sub sp_GenerarAsientoAjusteINV(ByVal _pF1 As Date, ByVal _pF2 As Date)
        Dim dt As New DataTable
        Dim cmd As New SqlClient.SqlCommand
        cmd.CommandText = "SELECT Consecutivo, Fecha, Imp, CodigoProv, CXP FROM AjusteInventario WHERE (ContaEntrada = 0) AND (ContaSalida = 0) AND (Anula = 0) AND (dbo.DateOnly(Fecha) >= @F1 AND dbo.DateOnly(Fecha) <= @F2)" '"SELECT d.Devolucion, p.CuentaContable AS CC, p.DescripcionCuentaContable AS DCC, p.Nombre, c.Factura, d.Monto, d.Fecha, d.Impuesto, d.Descuento, d.SubTotalExcento,  d.SubTotalGravado, d.Cod_Moneda, d.TipoCambio, d.Contabilizado, d.Asiento FROM dbo.devoluciones_Compras AS d INNER JOIN  dbo.compras AS c ON d.Id_Factura_Compra = c.Id_Compra INNER JOIN  dbo.Proveedores AS p ON c.CodigoProv = p.CodigoProv WHERE (d.Anulado = 0) AND (d.Contabilizado = 0) AND (CAST(d.Fecha AS DATE) >= @F1) AND (CAST(d.Fecha AS DATE) <= @F2)"

        cmd.Parameters.AddWithValue("@F1", _pF1.Date)
        cmd.Parameters.AddWithValue("@F2", _pF2.Date)
        cFunciones.Llenar_Tabla_Generico(cmd, dt, GetSetting("SEESOFT", "SeePOS", Conexion))
        Dim cuentaInv As String = ""
        Dim cuentaInvNom As String = ""

        If dt.Rows.Count = 0 Then
            MsgBox("No existen  pendientes para este rango de fechas", MsgBoxStyle.OKOnly)
            Exit Sub

        End If

        Dim cuentaIv As String = ""
        Dim cuentaIvNom As String = ""


        Dim dtConf As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT CuentaContable.CuentaContable, CuentaContable.Descripcion FROM SettingCuentaContable INNER JOIN  CuentaContable ON SettingCuentaContable.IdCompraGrabado = CuentaContable.id", dtConf, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
        If dtConf.Rows.Count > 0 Then
            cuentaInv = dtConf.Rows(0).Item("CuentaContable")
            cuentaInvNom = dtConf.Rows(0).Item("Descripcion")
        End If

        'Buscar Información de Impuesto de Ventas
        Dim cIv As String = ""
        Dim cDIv As String = ""

        cFunciones.Llenar_Tabla_Generico("SELECT c.CuentaContable, c.Descripcion FROM CuentaContable AS c INNER JOIN    SettingCuentaContable AS s ON c.id = s.IdImpuestoVenta ", dtConf, GetSetting("SeeSoft", "Contabilidad", "Conexion"))

        If dtConf.Rows.Count > 0 Then
            cIv = dtConf.Rows(0).Item("CuentaContable")
            cDIv = dtConf.Rows(0).Item("Descripcion")
        End If
        Dim fx As New cFunciones

        Dim periodo As String = fx.BuscaPeriodo(_pF1)
        Dim tc As Double = 0
        For ic As Integer = 0 To dt.Rows.Count - 1
            Dim modulo As String = "AJUST INV "

            With Me.BindingContext(Me.dsAs, "AsientosContables")
                .AddNew()
                .Current("NumAsiento") = Me.NumeroAsiento(_pF1, ic, "INV")
                .Current("Fecha") = dt.Rows(ic).Item("Fecha")
                .Current("IdNumDoc") = dt.Rows(ic).Item("Consecutivo")
                .Current("NumDoc") = dt.Rows(ic).Item("Consecutivo")
                .Current("Beneficiario") = " Doc No.: " & dt.Rows(ic).Item("Consecutivo")
                .Current("TipoDoc") = 0
                .Current("Accion") = "AUT"
                .Current("Anulado") = 0
                .Current("FechaEntrada") = dt.Rows(ic).Item("Fecha")
                .Current("Mayorizado") = mayo > 0
                .Current("Periodo") = periodo
                .Current("NumMayorizado") = 0
                .Current("Modulo") = modulo
                .Current("Observaciones") = modulo & ". Ajuste Inv compra: " & dt.Rows(ic).Item("Consecutivo")
                .Current("NombreUsuario") = Usuario.Nombre
                .Current("TotalDebe") = 0
                .Current("TotalHaber") = 0
                .Current("CodMoneda") = 1
                tc = fx.TipoCambio(dt.Rows(ic).Item("Fecha"), False)
                .Current("TipoCambio") = tc
                .EndCurrentEdit()



                ' ''LINEA CUENTA DEL PROVEEDOR PASIVO
                ''If dt.Rows(ic).Item("Cod_Moneda") = 1 Then 'COLONES
                If dt.Rows(ic).Item("CXP") Then
                    Dim dtProv As New DataTable
                    cFunciones.Llenar_Tabla_Generico("SELECT CuentaContable, DescripcionCuentaContable FROM Proveedores WHERE (CodigoProv = " & dt.Rows(ic).Item("CodigoProv") & ")", dtProv)
                    If dtProv.Rows.Count > 0 Then
                        GuardaAsientoDetalle(dt.Rows(ic).Item("Imp"), True, False, dtProv.Rows(0).Item("CuentaContable"), dtProv.Rows(0).Item("DescripcionCuentaContable"), tc)
                        GuardaAsientoDetalle(dt.Rows(ic).Item("Imp"), False, True, cIv, cDIv, tc)
                    End If

                End If


                ''Else 'DOLARES 
                'If dt.Rows(ic).Item("Tipo") = "CRE" Then
                '    GuardaAsientoDetalle(dt.Rows(ic).Item("Monto"), True, False, dt.Rows(ic).Item("CD"), dt.Rows(ic).Item("DCD"), tc)
                'Else
                '    GuardaAsientoDetalle(dt.Rows(ic).Item("Monto"), False, True, dt.Rows(ic).Item("CD"), dt.Rows(ic).Item("DCD"), tc)
                'End If


                'End If

                Dim _dt As New DataTable
                cmd.CommandText = "SELECT Cuenta_Contable, Nombre_Cuenta, TotalEntrada + TotalSalida AS Monto, Salida, observacion  FROM AjusteInventario_Detalle WHERE (Cons_Ajuste = " & dt.Rows(ic).Item("Consecutivo") & ")"
                '"SELECT a.ID_Ajuste, a.AjusteNo, d.CuentaContable, d.DescripcionCuentaContable, d.Ajuste FROM  Ajustescpagar AS a INNER JOIN      Proveedores AS p ON a.Cod_Proveedor = p.CodigoProv INNER JOIN  Detalle_AjustescPagar AS d ON a.ID_Ajuste = d.Id_AjustecPagar " & _
                '"WHERE   (a.ID_Ajuste = " & dt.Rows(ic).Item("Concecutivo") & ") AND  (a.Anula = 0) AND (a.ContaCre = 0) AND (a.ContaDeb = 0)"
                cFunciones.Llenar_Tabla_Generico(cmd, _dt, GetSetting("SeeSoft", "SeePOS", Conexion))
                If _dt.Rows.Count > 0 Then
                    .Current("Observaciones") &= ". " & _dt.Rows(0).Item("observacion")
                    .EndCurrentEdit()
                End If
                For id As Integer = 0 To _dt.Rows.Count - 1
                    If _dt.Rows(id).Item("Salida") Then
                        GuardaAsientoDetalle(_dt.Rows(id).Item("Monto"), True, False, _dt.Rows(id).Item("Cuenta_Contable"), _dt.Rows(id).Item("Nombre_Cuenta"), tc)
                        GuardaAsientoDetalle(_dt.Rows(id).Item("Monto"), False, True, cuentaInv, cuentaInvNom, tc)

                    Else
                        GuardaAsientoDetalle(_dt.Rows(id).Item("Monto"), True, False, cuentaInv, cuentaInvNom, tc)
                        GuardaAsientoDetalle(_dt.Rows(id).Item("Monto"), False, True, _dt.Rows(id).Item("Cuenta_Contable"), _dt.Rows(id).Item("Nombre_Cuenta"), tc)
                    End If
                Next
            End With

            Me.sp_totalesAsiento(BindingContext(Me.dsAs, "AsientosContables").Current("NumAsiento"))
        Next


    End Sub

    Public Function GuardaAsientoDetalle(ByVal Monto As Double, ByVal Debe As Boolean, ByVal Haber As Boolean, ByVal Cuenta As String, ByVal NombreCuenta As String, ByVal TipoCambio As Double) As Boolean
        Try
            If Monto <> 0 And (Not Cuenta.Equals("0")) And (Not Cuenta.Equals("")) Then

                If engrosarlacuenta(Monto, Debe, Haber, Cuenta, NombreCuenta) Then

                    Return True
                End If
                'CREA LOS DETALLES DE ASIENTOS CONTABLES
                BindingContext(dsAs, "DetallesAsientosContable").EndCurrentEdit()
                BindingContext(dsAs, "DetallesAsientosContable").AddNew()
                BindingContext(dsAs, "DetallesAsientosContable").Current("NumAsiento") = BindingContext(Me.dsAs, "AsientosContables").Current("NumAsiento")
                BindingContext(dsAs, "DetallesAsientosContable").Current("DescripcionAsiento") = BindingContext(Me.dsAs, "AsientosContables").Current("Observaciones")
                BindingContext(dsAs, "DetallesAsientosContable").Current("Cuenta") = Cuenta
                BindingContext(dsAs, "DetallesAsientosContable").Current("NombreCuenta") = NombreCuenta
                BindingContext(dsAs, "DetallesAsientosContable").Current("Monto") = Monto
                BindingContext(dsAs, "DetallesAsientosContable").Current("Debe") = Debe
                BindingContext(dsAs, "DetallesAsientosContable").Current("Haber") = Haber
                BindingContext(dsAs, "DetallesAsientosContable").Current("Tipocambio") = TipoCambio
                BindingContext(dsAs, "DetallesAsientosContable").EndCurrentEdit()
            Else
                Return False
            End If
        Catch ex As System.Exception
            'MsgBox("ERROR A INCLUIR DATO: " & ex.ToString, MsgBoxStyle.Information, "Atención...")
            BindingContext(dsAs, "DetallesAsientosContable").CancelCurrentEdit()
            Return False
        End Try
        Return True
    End Function

    Sub sp_totalesAsiento(ByVal numAs As String)
        Dim tDebe As Double = 0
        Dim tHaber As Double = 0
        For i As Integer = 0 To Me.dsAs.DetallesAsientosContable.Count - 1
            If Me.dsAs.DetallesAsientosContable(i).NumAsiento = numAs Then
                If Me.dsAs.DetallesAsientosContable(i).Debe Then
                    tDebe += Me.dsAs.DetallesAsientosContable(i).Monto
                Else
                    tHaber += Me.dsAs.DetallesAsientosContable(i).Monto
                End If
            End If
        Next
        'tDebe = Math.Round(tDebe, 2)
        'tHaber = Math.Round(tHaber, 2)
        BindingContext(Me.dsAs, "AsientosContables").Current("TotalDebe") = tDebe
        BindingContext(Me.dsAs, "AsientosContables").Current("TotalHaber") = tHaber
        BindingContext(Me.dsAs, "AsientosContables").Current("Dif") = tDebe - tHaber
        BindingContext(Me.dsAs, "AsientosContables").EndCurrentEdit()
    End Sub

End Class
