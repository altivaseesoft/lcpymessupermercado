Imports System.data.SqlClient
Imports System.Windows.Forms
Imports System.Data

Public Class frmReporteAjusteInventarioSalidas
    Inherits System.Windows.Forms.Form

    Dim usua As Usuario_Logeado
    Dim PMU As New PerfilModulo_Class
    Dim NuevaConexion As String

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents VisorReporte As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents rbProveedor As System.Windows.Forms.RadioButton
    Friend WithEvents rbRazonAjuste As System.Windows.Forms.RadioButton
    Friend WithEvents rbAjuste As System.Windows.Forms.RadioButton
    Friend WithEvents cbProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents cbRazonAjuste As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonMostrar As System.Windows.Forms.Button
    Friend WithEvents DsAjusteInv2 As LcPymes_5._2.DsAjusteInv
    Friend WithEvents adProveedores As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents adRazonAjuste As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents FechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents lblFechaInicio As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.VisorReporte = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
		Me.rbProveedor = New System.Windows.Forms.RadioButton()
		Me.cbProveedor = New System.Windows.Forms.ComboBox()
		Me.DsAjusteInv2 = New LcPymes_5._2.DsAjusteInv()
		Me.cbRazonAjuste = New System.Windows.Forms.ComboBox()
		Me.rbRazonAjuste = New System.Windows.Forms.RadioButton()
		Me.rbAjuste = New System.Windows.Forms.RadioButton()
		Me.ButtonMostrar = New System.Windows.Forms.Button()
		Me.adProveedores = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
		Me.adRazonAjuste = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand()
		Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
		Me.FechaFinal = New System.Windows.Forms.DateTimePicker()
		Me.FechaInicio = New System.Windows.Forms.DateTimePicker()
		Me.lblHasta = New System.Windows.Forms.Label()
		Me.lblFechaInicio = New System.Windows.Forms.Label()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		CType(Me.DsAjusteInv2, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		'
		'VisorReporte
		'
		Me.VisorReporte.ActiveViewIndex = -1
		Me.VisorReporte.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.VisorReporte.AutoScroll = True
		Me.VisorReporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.VisorReporte.Cursor = System.Windows.Forms.Cursors.Default
		Me.VisorReporte.Location = New System.Drawing.Point(192, 0)
		Me.VisorReporte.Name = "VisorReporte"
		Me.VisorReporte.ShowCloseButton = False
		Me.VisorReporte.Size = New System.Drawing.Size(608, 560)
		Me.VisorReporte.TabIndex = 77
		Me.VisorReporte.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
		'
		'rbProveedor
		'
		Me.rbProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.rbProveedor.Location = New System.Drawing.Point(8, 176)
		Me.rbProveedor.Name = "rbProveedor"
		Me.rbProveedor.Size = New System.Drawing.Size(104, 24)
		Me.rbProveedor.TabIndex = 79
		Me.rbProveedor.Text = "Por Proveedor"
		'
		'cbProveedor
		'
		Me.cbProveedor.DataSource = Me.DsAjusteInv2
		Me.cbProveedor.DisplayMember = "Proveedor.Nombre"
		Me.cbProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cbProveedor.Location = New System.Drawing.Point(24, 208)
		Me.cbProveedor.Name = "cbProveedor"
		Me.cbProveedor.Size = New System.Drawing.Size(160, 21)
		Me.cbProveedor.TabIndex = 80
		Me.cbProveedor.ValueMember = "Proveedor.CodigoProv"
		Me.cbProveedor.Visible = False
		'
		'DsAjusteInv2
		'
		Me.DsAjusteInv2.DataSetName = "DsAjusteInv"
		Me.DsAjusteInv2.Locale = New System.Globalization.CultureInfo("es-MX")
		Me.DsAjusteInv2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'cbRazonAjuste
		'
		Me.cbRazonAjuste.DataSource = Me.DsAjusteInv2
		Me.cbRazonAjuste.DisplayMember = "tb_S_RazonAjuste.Nombre"
		Me.cbRazonAjuste.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cbRazonAjuste.Location = New System.Drawing.Point(24, 280)
		Me.cbRazonAjuste.Name = "cbRazonAjuste"
		Me.cbRazonAjuste.Size = New System.Drawing.Size(160, 21)
		Me.cbRazonAjuste.TabIndex = 82
		Me.cbRazonAjuste.ValueMember = "tb_S_RazonAjuste.Id"
		Me.cbRazonAjuste.Visible = False
		'
		'rbRazonAjuste
		'
		Me.rbRazonAjuste.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.rbRazonAjuste.Location = New System.Drawing.Point(8, 248)
		Me.rbRazonAjuste.Name = "rbRazonAjuste"
		Me.rbRazonAjuste.Size = New System.Drawing.Size(144, 24)
		Me.rbRazonAjuste.TabIndex = 81
		Me.rbRazonAjuste.Text = "Por Raz�n de Ajuste"
		'
		'rbAjuste
		'
		Me.rbAjuste.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.rbAjuste.Location = New System.Drawing.Point(8, 320)
		Me.rbAjuste.Name = "rbAjuste"
		Me.rbAjuste.Size = New System.Drawing.Size(104, 24)
		Me.rbAjuste.TabIndex = 83
		Me.rbAjuste.Text = "Por Ajuste"
		'
		'ButtonMostrar
		'
		Me.ButtonMostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.ButtonMostrar.Location = New System.Drawing.Point(48, 448)
		Me.ButtonMostrar.Name = "ButtonMostrar"
		Me.ButtonMostrar.Size = New System.Drawing.Size(88, 40)
		Me.ButtonMostrar.TabIndex = 85
		Me.ButtonMostrar.Text = "Mostrar"
		'
		'adProveedores
		'
		Me.adProveedores.SelectCommand = Me.SqlSelectCommand1
		Me.adProveedores.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Proveedores", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoProv", "CodigoProv"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
		'
		'SqlSelectCommand1
		'
		Me.SqlSelectCommand1.CommandText = "SELECT CodigoProv, Nombre FROM Proveedores"
		'
		'adRazonAjuste
		'
		Me.adRazonAjuste.SelectCommand = Me.SqlSelectCommand2
		Me.adRazonAjuste.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "tb_S_RazonAjuste", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
		'
		'SqlSelectCommand2
		'
		Me.SqlSelectCommand2.CommandText = "SELECT Id, Nombre FROM tb_S_RazonAjuste"
		'
		'SqlConnection1
		'
		Me.SqlConnection1.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" &
	"e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
		Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
		'
		'FechaFinal
		'
		Me.FechaFinal.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.FechaFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.FechaFinal.Location = New System.Drawing.Point(76, 64)
		Me.FechaFinal.Name = "FechaFinal"
		Me.FechaFinal.Size = New System.Drawing.Size(96, 20)
		Me.FechaFinal.TabIndex = 41
		Me.FechaFinal.Value = New Date(2016, 2, 18, 10, 49, 46, 183)
		'
		'FechaInicio
		'
		Me.FechaInicio.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.FechaInicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.FechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.FechaInicio.Location = New System.Drawing.Point(76, 24)
		Me.FechaInicio.Name = "FechaInicio"
		Me.FechaInicio.Size = New System.Drawing.Size(96, 20)
		Me.FechaInicio.TabIndex = 40
		Me.FechaInicio.Value = New Date(2006, 4, 10, 0, 0, 0, 0)
		'
		'lblHasta
		'
		Me.lblHasta.BackColor = System.Drawing.SystemColors.ControlLight
		Me.lblHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.lblHasta.ForeColor = System.Drawing.Color.Blue
		Me.lblHasta.Location = New System.Drawing.Point(12, 64)
		Me.lblHasta.Name = "lblHasta"
		Me.lblHasta.Size = New System.Drawing.Size(56, 16)
		Me.lblHasta.TabIndex = 39
		Me.lblHasta.Text = "Hasta : "
		'
		'lblFechaInicio
		'
		Me.lblFechaInicio.BackColor = System.Drawing.SystemColors.ControlLight
		Me.lblFechaInicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.lblFechaInicio.ForeColor = System.Drawing.Color.Blue
		Me.lblFechaInicio.Location = New System.Drawing.Point(12, 24)
		Me.lblFechaInicio.Name = "lblFechaInicio"
		Me.lblFechaInicio.Size = New System.Drawing.Size(56, 16)
		Me.lblFechaInicio.TabIndex = 38
		Me.lblFechaInicio.Text = "Desde : "
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.FechaFinal)
		Me.GroupBox1.Controls.Add(Me.FechaInicio)
		Me.GroupBox1.Controls.Add(Me.lblHasta)
		Me.GroupBox1.Controls.Add(Me.lblFechaInicio)
		Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
		Me.GroupBox1.Location = New System.Drawing.Point(0, 24)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(184, 104)
		Me.GroupBox1.TabIndex = 78
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Filtro por Fecha"
		'
		'frmReporteAjusteInventarioSalidas
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(790, 564)
		Me.Controls.Add(Me.ButtonMostrar)
		Me.Controls.Add(Me.rbAjuste)
		Me.Controls.Add(Me.cbRazonAjuste)
		Me.Controls.Add(Me.rbRazonAjuste)
		Me.Controls.Add(Me.cbProveedor)
		Me.Controls.Add(Me.rbProveedor)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.VisorReporte)
		Me.ForeColor = System.Drawing.Color.Blue
		Me.Name = "frmReporteAjusteInventarioSalidas"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Reporte Ajuste de Inventario"
		CType(Me.DsAjusteInv2, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox1.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub

#End Region


	Private Sub frmReporteAjusteInventarioSalidas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        Me.spCargarComboBox()
        Me.FechaInicio.Value = Now.AddDays(-15)
		Me.FechaFinal.Value = Now
		WindowState = FormWindowState.Maximized
	End Sub
    Private Sub spCargarComboBox()
        Dim Fx As New cFunciones

        Dim strConexion As String = SqlConnection1.ConnectionString

        Fx.Cargar_Tabla_Generico(Me.adProveedores, "SELECT [CodigoProv],[Nombre]FROM [Seepos].[dbo].[Proveedores] ", strConexion)  'JCGA 19032007
        Me.adProveedores.Fill(Me.DsAjusteInv2, "Proveedor")

        Dim Fila As DataRow
        Fila = Me.DsAjusteInv2.Proveedor.NewRow()
        Fila("CodigoProv") = 0
        Fila("Nombre") = "TODOS"
        Me.DsAjusteInv2.Proveedor.Rows.Add(Fila)
        Me.cbProveedor.SelectedValue = 0

        Fx.Cargar_Tabla_Generico(Me.adRazonAjuste, "  SELECT [Id],[Nombre] FROM [Seepos].[dbo].[tb_S_RazonAjuste]", strConexion)  'JCGA 19032007
        Me.adRazonAjuste.Fill(Me.DsAjusteInv2, "tb_S_RazonAjuste")

        Dim Fila2 As DataRow
        Fila2 = Me.DsAjusteInv2.tb_S_RazonAjuste.NewRow()
        Fila2("Id") = 0
        Fila2("Nombre") = "TODOS"
        Me.DsAjusteInv2.tb_S_RazonAjuste.Rows.Add(Fila2)
        Me.cbRazonAjuste.SelectedValue = 0

    End Sub

    Private Sub rbProveedor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbProveedor.CheckedChanged
        If Me.rbProveedor.Checked Then
            Me.cbProveedor.Visible = True
            Me.cbRazonAjuste.Visible = False
            Me.cbProveedor.Focus()
        End If
    End Sub

    Private Sub rbRazonAjuste_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbRazonAjuste.CheckedChanged
        If Me.rbRazonAjuste.Checked Then
            Me.cbProveedor.Visible = False
            Me.cbRazonAjuste.Visible = True
            Me.cbRazonAjuste.Focus()
        End If
    End Sub

    Private Sub rbAjuste_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAjuste.CheckedChanged
        If Me.rbAjuste.Checked Then
            Me.cbProveedor.Visible = False
            Me.cbRazonAjuste.Visible = False
            ButtonMostrar.Focus()
        End If
    End Sub

    Private Function fnValidarFechas()
        If Me.FechaInicio.Value > Me.FechaFinal.Value Then
            Return False
        End If
        Return True
    End Function
    Private Function fnValidarRadioButton()
        If Not Me.rbProveedor.Checked And Not Me.rbRazonAjuste.Checked And Not Me.rbAjuste.Checked Then
            Return False
        End If
        Return True
    End Function
    Private Sub spMostrarPorProveedor()
        Dim Reporte As New rptAjusteInventario_X_Proveedor
        Reporte.SetParameterValue("CodigoProveedor", CDbl(Me.cbProveedor.SelectedValue))
        If Me.cbProveedor.Text.Equals("TODOS") Then
            Reporte.SetParameterValue("Filtro", False)
        Else
            Reporte.SetParameterValue("Filtro", True)
        End If
        Reporte.SetParameterValue("FechaDesde", CDate(Me.FechaInicio.Value))
        Reporte.SetParameterValue("FechaHasta", CDate(Me.FechaFinal.Value))
        CrystalReportsConexion.LoadReportViewer(VisorReporte, Reporte)
        VisorReporte.Show()
    End Sub
    Private Sub spMostrarPorRazonAjuste()
        Dim Reporte As New rptAjusteInventario_X_RazonAJuste
        Reporte.SetParameterValue("Codigo", CDbl(Me.cbRazonAjuste.SelectedValue))
        If Me.cbRazonAjuste.Text.Equals("TODOS") Then
            Reporte.SetParameterValue("Filtro", False)
        Else
            Reporte.SetParameterValue("Filtro", True)
        End If
        Reporte.SetParameterValue("FechaDesde", CDate(Me.FechaInicio.Value))
        Reporte.SetParameterValue("FechaHasta", CDate(Me.FechaFinal.Value))
        CrystalReportsConexion.LoadReportViewer(VisorReporte, Reporte)
        VisorReporte.Show()
    End Sub
    Private Sub spMostrarPorAjuste()
        Dim Reporte As New rptAjusteInventario_X_Ajuste
        Reporte.SetParameterValue("FechaDesde", CDate(Me.FechaInicio.Value))
        Reporte.SetParameterValue("FechaHasta", CDate(Me.FechaFinal.Value))
        CrystalReportsConexion.LoadReportViewer(VisorReporte, Reporte)
        VisorReporte.Show()
    End Sub

    Private Sub spMostrarReporte()
        Try

            If Me.fnValidarFechas Then

                If Me.fnValidarRadioButton Then
                    Try
                        Me.ButtonMostrar.Enabled = False

                        If Me.rbProveedor.Checked Then
                            Me.spMostrarPorProveedor()
                        End If

                        If Me.rbRazonAjuste.Checked Then
                            Me.spMostrarPorRazonAjuste()
                        End If

                        If Me.rbAjuste.Checked Then
                            Me.spMostrarPorAjuste()
                        End If

                        Me.WindowState = Windows.Forms.FormWindowState.Maximized
                        Me.ButtonMostrar.Enabled = True
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        Me.ButtonMostrar.Enabled = True
                    End Try
                Else
                    MsgBox("Seleccione una de las opciones del filtro.", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("La FechaDesde no puede ser mayor a FechaHasta.", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMostrar.Click
        PMU = VSM(usua.Cedula, Me.Name)  'Carga los privilegios del usuario con el modulo  

        If PMU.Print Then Me.spMostrarReporte() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

    End Sub


    Private Sub cbProveedor_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbProveedor.KeyUp
        If e.KeyCode = Keys.Enter Then
            ButtonMostrar.Focus()
        End If
    End Sub
    Private Sub cbRazonAjuste_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbRazonAjuste.KeyUp
        If e.KeyCode = Keys.Enter Then
            ButtonMostrar.Focus()
        End If
    End Sub

	Private Sub frmReporteAjusteInventarioSalidas_LocationChanged(sender As Object, e As EventArgs) Handles Me.LocationChanged

	End Sub
End Class
