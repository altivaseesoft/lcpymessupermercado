Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class Toma_Fisica
    Inherits FrmPlantilla

#Region "Variables"
    Dim usua As Usuario_Logeado
    Dim Buscando As Boolean = False
    Dim TomaFisica As New CrystalDecisions.CrystalReports.Engine.ReportDocument
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents AdapterTomaFisica As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterTomaFisicaDetalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsTomaFisica1 As DsTomaFisica
    Friend WithEvents txtNumero As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents LUsuario As System.Windows.Forms.Label
    Friend WithEvents Check_Anulado As System.Windows.Forms.CheckBox
    Friend WithEvents GBTomaFisica As System.Windows.Forms.GroupBox
    Friend WithEvents LFecha As System.Windows.Forms.Label
    Friend WithEvents Check_Inicial As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LOrdenado As System.Windows.Forms.Label
    Friend WithEvents Cb_Ordenar As System.Windows.Forms.ComboBox
    Friend WithEvents GB_Reporte As System.Windows.Forms.GroupBox
    Friend WithEvents LAgrupado As System.Windows.Forms.Label
    Friend WithEvents CB_Agrupar As System.Windows.Forms.ComboBox
    Friend WithEvents txtCedula As System.Windows.Forms.TextBox
    Friend WithEvents BGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GC_Articulos As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBarras As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTomaFisica As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCosto_Promedio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFamilia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUbicacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdapterAjuste As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterAjuste_Detalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents LAjuste As System.Windows.Forms.Label
    Friend WithEvents LAsiento As System.Windows.Forms.Label
    Friend WithEvents TxtAjuste As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtAsiento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToolBarAsiento As System.Windows.Forms.ToolBarButton
    Friend WithEvents DT_Fecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemCalcEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand41 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Toma_Fisica))
        Me.AdapterTomaFisica = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.AdapterTomaFisicaDetalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.DsTomaFisica1 = New LcPymes_5._2.DsTomaFisica
        Me.txtNumero = New System.Windows.Forms.TextBox
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.LUsuario = New System.Windows.Forms.Label
        Me.GBTomaFisica = New System.Windows.Forms.GroupBox
        Me.DT_Fecha = New DevExpress.XtraEditors.DateEdit
        Me.TxtAsiento = New DevExpress.XtraEditors.TextEdit
        Me.TxtAjuste = New DevExpress.XtraEditors.TextEdit
        Me.LAsiento = New System.Windows.Forms.Label
        Me.LAjuste = New System.Windows.Forms.Label
        Me.GB_Reporte = New System.Windows.Forms.GroupBox
        Me.LAgrupado = New System.Windows.Forms.Label
        Me.CB_Agrupar = New System.Windows.Forms.ComboBox
        Me.LOrdenado = New System.Windows.Forms.Label
        Me.Cb_Ordenar = New System.Windows.Forms.ComboBox
        Me.Check_Inicial = New DevExpress.XtraEditors.CheckEdit
        Me.LFecha = New System.Windows.Forms.Label
        Me.BGenerar = New DevExpress.XtraEditors.SimpleButton
        Me.Check_Anulado = New System.Windows.Forms.CheckBox
        Me.txtCedula = New System.Windows.Forms.TextBox
        Me.GC_Articulos = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCodigo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colBarras = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colExistencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTomaFisica = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.colCosto_Promedio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCalcEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Me.colFamilia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colUbicacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.AdapterAjuste = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.AdapterAjuste_Detalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.ToolBarAsiento = New System.Windows.Forms.ToolBarButton
        CType(Me.DsTomaFisica1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GBTomaFisica.SuspendLayout()
        CType(Me.DT_Fecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtAsiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtAjuste.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GB_Reporte.SuspendLayout()
        CType(Me.Check_Inicial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GC_Articulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(824, 420)
        Me.DataNavigator.Name = "DataNavigator"
        Me.DataNavigator.Visible = False
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        '
        'ToolBar1
        '
        Me.ToolBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarAsiento})
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(779, 52)
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.Text = "Anular"
        '
        'ToolBarExcel
        '
        Me.ToolBarExcel.Enabled = False
        Me.ToolBarExcel.ImageIndex = 2
        Me.ToolBarExcel.Text = "Ajuste Inv."
        Me.ToolBarExcel.Visible = True
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Text = "Toma Fis�ca de Inventario"
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'AdapterTomaFisica
        '
        Me.AdapterTomaFisica.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterTomaFisica.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterTomaFisica.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterTomaFisica.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TomaFisica", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Periodo", "Periodo"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Inicial", "Inicial"), New System.Data.Common.DataColumnMapping("Ajuste", "Ajuste"), New System.Data.Common.DataColumnMapping("Num_Ajuste", "Num_Ajuste"), New System.Data.Common.DataColumnMapping("Asiento", "Asiento"), New System.Data.Common.DataColumnMapping("Num_Asiento", "Num_Asiento"), New System.Data.Common.DataColumnMapping("Ordenado", "Ordenado"), New System.Data.Common.DataColumnMapping("Agrupado", "Agrupado")})})
        Me.AdapterTomaFisica.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM TomaFisica WHERE (Codigo = @Original_Codigo) AND (Agrupado = @Origina" & _
        "l_Agrupado) AND (Ajuste = @Original_Ajuste) AND (Anulado = @Original_Anulado) AN" & _
        "D (Asiento = @Original_Asiento) AND (Fecha = @Original_Fecha) AND (Inicial = @Or" & _
        "iginal_Inicial) AND (Num_Ajuste = @Original_Num_Ajuste) AND (Num_Asiento = @Orig" & _
        "inal_Num_Asiento) AND (Ordenado = @Original_Ordenado) AND (Periodo = @Original_P" & _
        "eriodo) AND (Usuario = @Original_Usuario)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Agrupado", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Agrupado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ajuste", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Asiento", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Asiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Inicial", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inicial", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Ajuste", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Asiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Asiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ordenado", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ordenado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Periodo", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periodo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO TomaFisica(Fecha, Periodo, Usuario, Anulado, Inicial, Ajuste, Num_Aju" & _
        "ste, Asiento, Num_Asiento, Ordenado, Agrupado) VALUES (@Fecha, @Periodo, @Usuari" & _
        "o, @Anulado, @Inicial, @Ajuste, @Num_Ajuste, @Asiento, @Num_Asiento, @Ordenado, " & _
        "@Agrupado); SELECT Codigo, Fecha, Periodo, Usuario, Anulado, Inicial, Ajuste, Nu" & _
        "m_Ajuste, Asiento, Num_Asiento, Ordenado, Agrupado FROM TomaFisica WHERE (Codigo" & _
        " = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Periodo", System.Data.SqlDbType.DateTime, 8, "Periodo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 100, "Usuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Inicial", System.Data.SqlDbType.Bit, 1, "Inicial"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ajuste", System.Data.SqlDbType.Bit, 1, "Ajuste"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Ajuste", System.Data.SqlDbType.BigInt, 8, "Num_Ajuste"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Asiento", System.Data.SqlDbType.Bit, 1, "Asiento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Asiento", System.Data.SqlDbType.VarChar, 15, "Num_Asiento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ordenado", System.Data.SqlDbType.VarChar, 50, "Ordenado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Agrupado", System.Data.SqlDbType.VarChar, 50, "Agrupado"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Codigo, Fecha, Periodo, Usuario, Anulado, Inicial, Ajuste, Num_Ajuste, Asi" & _
        "ento, Num_Asiento, Ordenado, Agrupado FROM TomaFisica"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE TomaFisica SET Fecha = @Fecha, Periodo = @Periodo, Usuario = @Usuario, Anu" & _
        "lado = @Anulado, Inicial = @Inicial, Ajuste = @Ajuste, Num_Ajuste = @Num_Ajuste," & _
        " Asiento = @Asiento, Num_Asiento = @Num_Asiento, Ordenado = @Ordenado, Agrupado " & _
        "= @Agrupado WHERE (Codigo = @Original_Codigo) AND (Agrupado = @Original_Agrupado" & _
        ") AND (Ajuste = @Original_Ajuste) AND (Anulado = @Original_Anulado) AND (Asiento" & _
        " = @Original_Asiento) AND (Fecha = @Original_Fecha) AND (Inicial = @Original_Ini" & _
        "cial) AND (Num_Ajuste = @Original_Num_Ajuste) AND (Num_Asiento = @Original_Num_A" & _
        "siento) AND (Ordenado = @Original_Ordenado) AND (Periodo = @Original_Periodo) AN" & _
        "D (Usuario = @Original_Usuario); SELECT Codigo, Fecha, Periodo, Usuario, Anulado" & _
        ", Inicial, Ajuste, Num_Ajuste, Asiento, Num_Asiento, Ordenado, Agrupado FROM Tom" & _
        "aFisica WHERE (Codigo = @Codigo)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Periodo", System.Data.SqlDbType.DateTime, 8, "Periodo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 100, "Usuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Inicial", System.Data.SqlDbType.Bit, 1, "Inicial"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ajuste", System.Data.SqlDbType.Bit, 1, "Ajuste"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Ajuste", System.Data.SqlDbType.BigInt, 8, "Num_Ajuste"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Asiento", System.Data.SqlDbType.Bit, 1, "Asiento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Asiento", System.Data.SqlDbType.VarChar, 15, "Num_Asiento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ordenado", System.Data.SqlDbType.VarChar, 50, "Ordenado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Agrupado", System.Data.SqlDbType.VarChar, 50, "Agrupado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Agrupado", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Agrupado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ajuste", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Asiento", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Asiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Inicial", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inicial", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Ajuste", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Asiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Asiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ordenado", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ordenado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Periodo", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periodo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        '
        'AdapterTomaFisicaDetalle
        '
        Me.AdapterTomaFisicaDetalle.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterTomaFisicaDetalle.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterTomaFisicaDetalle.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterTomaFisicaDetalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TomaFisica_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("IdTomaFisica", "IdTomaFisica"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("TomaFisica", "TomaFisica"), New System.Data.Common.DataColumnMapping("CostoPromedio", "CostoPromedio"), New System.Data.Common.DataColumnMapping("Barras", "Barras"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Familia", "Familia"), New System.Data.Common.DataColumnMapping("Ubicacion", "Ubicacion")})})
        Me.AdapterTomaFisicaDetalle.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM TomaFisica_Detalle WHERE (Id = @Original_Id) AND (Barras = @Original_" & _
        "Barras OR @Original_Barras IS NULL AND Barras IS NULL) AND (Codigo = @Original_C" & _
        "odigo) AND (CostoPromedio = @Original_CostoPromedio) AND (Descripcion = @Origina" & _
        "l_Descripcion) AND (Existencia = @Original_Existencia) AND (Familia = @Original_" & _
        "Familia) AND (IdTomaFisica = @Original_IdTomaFisica) AND (TomaFisica = @Original" & _
        "_TomaFisica) AND (Ubicacion = @Original_Ubicacion)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoPromedio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoPromedio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Familia", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Familia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdTomaFisica", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdTomaFisica", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TomaFisica", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TomaFisica", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ubicacion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ubicacion", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO TomaFisica_Detalle(IdTomaFisica, Codigo, Existencia, TomaFisica, Cost" & _
        "oPromedio, Barras, Descripcion, Familia, Ubicacion) VALUES (@IdTomaFisica, @Codi" & _
        "go, @Existencia, @TomaFisica, @CostoPromedio, @Barras, @Descripcion, @Familia, @" & _
        "Ubicacion); SELECT Id, IdTomaFisica, Codigo, Existencia, TomaFisica, CostoPromed" & _
        "io, Barras, Descripcion, Familia, Ubicacion FROM TomaFisica_Detalle WHERE (Id = " & _
        "@@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdTomaFisica", System.Data.SqlDbType.BigInt, 8, "IdTomaFisica"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TomaFisica", System.Data.SqlDbType.Float, 8, "TomaFisica"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoPromedio", System.Data.SqlDbType.Float, 8, "CostoPromedio"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 250, "Descripcion"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Familia", System.Data.SqlDbType.VarChar, 250, "Familia"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ubicacion", System.Data.SqlDbType.VarChar, 250, "Ubicacion"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Id, IdTomaFisica, Codigo, Existencia, TomaFisica, CostoPromedio, Barras, D" & _
        "escripcion, Familia, Ubicacion FROM TomaFisica_Detalle"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE TomaFisica_Detalle SET IdTomaFisica = @IdTomaFisica, Codigo = @Codigo, Exi" & _
        "stencia = @Existencia, TomaFisica = @TomaFisica, CostoPromedio = @CostoPromedio," & _
        " Barras = @Barras, Descripcion = @Descripcion, Familia = @Familia, Ubicacion = @" & _
        "Ubicacion WHERE (Id = @Original_Id) AND (Barras = @Original_Barras OR @Original_" & _
        "Barras IS NULL AND Barras IS NULL) AND (Codigo = @Original_Codigo) AND (CostoPro" & _
        "medio = @Original_CostoPromedio) AND (Descripcion = @Original_Descripcion) AND (" & _
        "Existencia = @Original_Existencia) AND (Familia = @Original_Familia) AND (IdToma" & _
        "Fisica = @Original_IdTomaFisica) AND (TomaFisica = @Original_TomaFisica) AND (Ub" & _
        "icacion = @Original_Ubicacion); SELECT Id, IdTomaFisica, Codigo, Existencia, Tom" & _
        "aFisica, CostoPromedio, Barras, Descripcion, Familia, Ubicacion FROM TomaFisica_" & _
        "Detalle WHERE (Id = @Id)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdTomaFisica", System.Data.SqlDbType.BigInt, 8, "IdTomaFisica"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TomaFisica", System.Data.SqlDbType.Float, 8, "TomaFisica"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoPromedio", System.Data.SqlDbType.Float, 8, "CostoPromedio"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 250, "Descripcion"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Familia", System.Data.SqlDbType.VarChar, 250, "Familia"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ubicacion", System.Data.SqlDbType.VarChar, 250, "Ubicacion"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoPromedio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoPromedio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Familia", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Familia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdTomaFisica", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdTomaFisica", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TomaFisica", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TomaFisica", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ubicacion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ubicacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id"))
        '
        'DsTomaFisica1
        '
        Me.DsTomaFisica1.DataSetName = "DsTomaFisica"
        Me.DsTomaFisica1.Locale = New System.Globalization.CultureInfo("es-ES")
        '
        'txtNumero
        '
        Me.txtNumero.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNumero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsTomaFisica1, "TomaFisica.Codigo"))
        Me.txtNumero.Enabled = False
        Me.txtNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumero.ForeColor = System.Drawing.Color.RoyalBlue
        Me.txtNumero.Location = New System.Drawing.Point(72, 16)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(96, 13)
        Me.txtNumero.TabIndex = 106
        Me.txtNumero.Text = ""
        Me.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(608, 424)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(163, 13)
        Me.txtNombreUsuario.TabIndex = 109
        Me.txtNombreUsuario.Text = ""
        '
        'txtUsuario
        '
        Me.txtUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(528, 424)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(80, 13)
        Me.txtUsuario.TabIndex = 0
        Me.txtUsuario.Text = ""
        '
        'LUsuario
        '
        Me.LUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LUsuario.BackColor = System.Drawing.Color.RoyalBlue
        Me.LUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LUsuario.ForeColor = System.Drawing.Color.White
        Me.LUsuario.Location = New System.Drawing.Point(456, 424)
        Me.LUsuario.Name = "LUsuario"
        Me.LUsuario.Size = New System.Drawing.Size(72, 13)
        Me.LUsuario.TabIndex = 107
        Me.LUsuario.Text = "Usuario->"
        Me.LUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GBTomaFisica
        '
        Me.GBTomaFisica.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GBTomaFisica.BackColor = System.Drawing.Color.Transparent
        Me.GBTomaFisica.Controls.Add(Me.DT_Fecha)
        Me.GBTomaFisica.Controls.Add(Me.TxtAsiento)
        Me.GBTomaFisica.Controls.Add(Me.TxtAjuste)
        Me.GBTomaFisica.Controls.Add(Me.LAsiento)
        Me.GBTomaFisica.Controls.Add(Me.LAjuste)
        Me.GBTomaFisica.Controls.Add(Me.GB_Reporte)
        Me.GBTomaFisica.Controls.Add(Me.Check_Inicial)
        Me.GBTomaFisica.Controls.Add(Me.LFecha)
        Me.GBTomaFisica.Controls.Add(Me.BGenerar)
        Me.GBTomaFisica.Enabled = False
        Me.GBTomaFisica.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GBTomaFisica.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GBTomaFisica.Location = New System.Drawing.Point(8, 40)
        Me.GBTomaFisica.Name = "GBTomaFisica"
        Me.GBTomaFisica.Size = New System.Drawing.Size(760, 88)
        Me.GBTomaFisica.TabIndex = 110
        Me.GBTomaFisica.TabStop = False
        Me.GBTomaFisica.Text = "Datos de la Toma F�sica"
        '
        'DT_Fecha
        '
        Me.DT_Fecha.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsTomaFisica1, "TomaFisica.Periodo"))
        Me.DT_Fecha.EditValue = New Date(2009, 3, 24, 0, 0, 0, 0)
        Me.DT_Fecha.Location = New System.Drawing.Point(16, 40)
        Me.DT_Fecha.Name = "DT_Fecha"
        '
        'DT_Fecha.Properties
        '
        Me.DT_Fecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DT_Fecha.Size = New System.Drawing.Size(104, 19)
        Me.DT_Fecha.TabIndex = 150
        '
        'TxtAsiento
        '
        Me.TxtAsiento.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtAsiento.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsTomaFisica1, "TomaFisica.Num_Asiento"))
        Me.TxtAsiento.EditValue = ""
        Me.TxtAsiento.Location = New System.Drawing.Point(656, 64)
        Me.TxtAsiento.Name = "TxtAsiento"
        '
        'TxtAsiento.Properties
        '
        Me.TxtAsiento.Properties.Enabled = False
        Me.TxtAsiento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.TxtAsiento.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlDark)
        Me.TxtAsiento.Size = New System.Drawing.Size(96, 19)
        Me.TxtAsiento.TabIndex = 149
        Me.TxtAsiento.Visible = False
        '
        'TxtAjuste
        '
        Me.TxtAjuste.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtAjuste.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsTomaFisica1, "TomaFisica.Num_Ajuste"))
        Me.TxtAjuste.EditValue = ""
        Me.TxtAjuste.Location = New System.Drawing.Point(656, 24)
        Me.TxtAjuste.Name = "TxtAjuste"
        '
        'TxtAjuste.Properties
        '
        Me.TxtAjuste.Properties.Enabled = False
        Me.TxtAjuste.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.TxtAjuste.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlDark)
        Me.TxtAjuste.Size = New System.Drawing.Size(96, 19)
        Me.TxtAjuste.TabIndex = 148
        Me.TxtAjuste.Visible = False
        '
        'LAsiento
        '
        Me.LAsiento.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LAsiento.BackColor = System.Drawing.Color.RoyalBlue
        Me.LAsiento.Cursor = System.Windows.Forms.Cursors.Default
        Me.LAsiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LAsiento.ForeColor = System.Drawing.Color.White
        Me.LAsiento.Location = New System.Drawing.Point(656, 48)
        Me.LAsiento.Name = "LAsiento"
        Me.LAsiento.Size = New System.Drawing.Size(96, 16)
        Me.LAsiento.TabIndex = 147
        Me.LAsiento.Text = "Asiento # :"
        Me.LAsiento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LAsiento.Visible = False
        '
        'LAjuste
        '
        Me.LAjuste.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LAjuste.BackColor = System.Drawing.Color.RoyalBlue
        Me.LAjuste.Cursor = System.Windows.Forms.Cursors.Default
        Me.LAjuste.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LAjuste.ForeColor = System.Drawing.Color.White
        Me.LAjuste.Location = New System.Drawing.Point(656, 8)
        Me.LAjuste.Name = "LAjuste"
        Me.LAjuste.Size = New System.Drawing.Size(96, 16)
        Me.LAjuste.TabIndex = 146
        Me.LAjuste.Text = "Ajuste # :"
        Me.LAjuste.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LAjuste.Visible = False
        '
        'GB_Reporte
        '
        Me.GB_Reporte.Controls.Add(Me.LAgrupado)
        Me.GB_Reporte.Controls.Add(Me.CB_Agrupar)
        Me.GB_Reporte.Controls.Add(Me.LOrdenado)
        Me.GB_Reporte.Controls.Add(Me.Cb_Ordenar)
        Me.GB_Reporte.Location = New System.Drawing.Point(152, 16)
        Me.GB_Reporte.Name = "GB_Reporte"
        Me.GB_Reporte.Size = New System.Drawing.Size(304, 64)
        Me.GB_Reporte.TabIndex = 144
        Me.GB_Reporte.TabStop = False
        Me.GB_Reporte.Text = "Ordenamiento"
        '
        'LAgrupado
        '
        Me.LAgrupado.BackColor = System.Drawing.Color.RoyalBlue
        Me.LAgrupado.Cursor = System.Windows.Forms.Cursors.Default
        Me.LAgrupado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LAgrupado.ForeColor = System.Drawing.Color.White
        Me.LAgrupado.Location = New System.Drawing.Point(160, 16)
        Me.LAgrupado.Name = "LAgrupado"
        Me.LAgrupado.Size = New System.Drawing.Size(136, 16)
        Me.LAgrupado.TabIndex = 145
        Me.LAgrupado.Text = "Agrupar Por :"
        Me.LAgrupado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CB_Agrupar
        '
        Me.CB_Agrupar.DataBindings.Add(New System.Windows.Forms.Binding("SelectedItem", Me.DsTomaFisica1, "TomaFisica.Agrupado"))
        Me.CB_Agrupar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Agrupar.Items.AddRange(New Object() {"Ubicacion", "Familia", "NO"})
        Me.CB_Agrupar.Location = New System.Drawing.Point(160, 32)
        Me.CB_Agrupar.Name = "CB_Agrupar"
        Me.CB_Agrupar.Size = New System.Drawing.Size(136, 21)
        Me.CB_Agrupar.TabIndex = 1
        '
        'LOrdenado
        '
        Me.LOrdenado.BackColor = System.Drawing.Color.RoyalBlue
        Me.LOrdenado.Cursor = System.Windows.Forms.Cursors.Default
        Me.LOrdenado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LOrdenado.ForeColor = System.Drawing.Color.White
        Me.LOrdenado.Location = New System.Drawing.Point(8, 16)
        Me.LOrdenado.Name = "LOrdenado"
        Me.LOrdenado.Size = New System.Drawing.Size(136, 16)
        Me.LOrdenado.TabIndex = 143
        Me.LOrdenado.Text = "Ordenado Por :"
        Me.LOrdenado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Cb_Ordenar
        '
        Me.Cb_Ordenar.DataBindings.Add(New System.Windows.Forms.Binding("SelectedItem", Me.DsTomaFisica1, "TomaFisica.Ordenado"))
        Me.Cb_Ordenar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cb_Ordenar.Items.AddRange(New Object() {"Descripcion", "Codigo"})
        Me.Cb_Ordenar.Location = New System.Drawing.Point(8, 32)
        Me.Cb_Ordenar.Name = "Cb_Ordenar"
        Me.Cb_Ordenar.Size = New System.Drawing.Size(136, 21)
        Me.Cb_Ordenar.TabIndex = 0
        '
        'Check_Inicial
        '
        Me.Check_Inicial.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsTomaFisica1, "TomaFisica.Inicial"))
        Me.Check_Inicial.EditValue = False
        Me.Check_Inicial.Location = New System.Drawing.Point(16, 64)
        Me.Check_Inicial.Name = "Check_Inicial"
        '
        'Check_Inicial.Properties
        '
        Me.Check_Inicial.Properties.Caption = "Inventario Inicial"
        Me.Check_Inicial.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.Color.RoyalBlue)
        Me.Check_Inicial.Size = New System.Drawing.Size(112, 19)
        Me.Check_Inicial.TabIndex = 141
        '
        'LFecha
        '
        Me.LFecha.BackColor = System.Drawing.Color.RoyalBlue
        Me.LFecha.Cursor = System.Windows.Forms.Cursors.Default
        Me.LFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LFecha.ForeColor = System.Drawing.Color.White
        Me.LFecha.Location = New System.Drawing.Point(16, 24)
        Me.LFecha.Name = "LFecha"
        Me.LFecha.Size = New System.Drawing.Size(104, 16)
        Me.LFecha.TabIndex = 140
        Me.LFecha.Text = "Fecha"
        Me.LFecha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BGenerar
        '
        Me.BGenerar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BGenerar.Location = New System.Drawing.Point(464, 40)
        Me.BGenerar.Name = "BGenerar"
        Me.BGenerar.Size = New System.Drawing.Size(88, 23)
        Me.BGenerar.TabIndex = 2
        Me.BGenerar.Text = "Generar"
        '
        'Check_Anulado
        '
        Me.Check_Anulado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Check_Anulado.BackColor = System.Drawing.Color.Transparent
        Me.Check_Anulado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsTomaFisica1, "TomaFisica.Anulado"))
        Me.Check_Anulado.Enabled = False
        Me.Check_Anulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Anulado.ForeColor = System.Drawing.Color.Red
        Me.Check_Anulado.Location = New System.Drawing.Point(696, 8)
        Me.Check_Anulado.Name = "Check_Anulado"
        Me.Check_Anulado.Size = New System.Drawing.Size(81, 16)
        Me.Check_Anulado.TabIndex = 15
        Me.Check_Anulado.Text = "Anulado"
        '
        'txtCedula
        '
        Me.txtCedula.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCedula.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtCedula.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCedula.Enabled = False
        Me.txtCedula.ForeColor = System.Drawing.Color.Blue
        Me.txtCedula.Location = New System.Drawing.Point(608, 408)
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.ReadOnly = True
        Me.txtCedula.Size = New System.Drawing.Size(163, 13)
        Me.txtCedula.TabIndex = 112
        Me.txtCedula.Text = ""
        Me.txtCedula.Visible = False
        '
        'GC_Articulos
        '
        Me.GC_Articulos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GC_Articulos.DataMember = "TomaFisica.TomaFisicaTomaFisica_Detalle"
        Me.GC_Articulos.DataSource = Me.DsTomaFisica1
        '
        'GC_Articulos.EmbeddedNavigator
        '
        Me.GC_Articulos.EmbeddedNavigator.Name = ""
        Me.GC_Articulos.Enabled = False
        Me.GC_Articulos.Location = New System.Drawing.Point(8, 136)
        Me.GC_Articulos.MainView = Me.GridView1
        Me.GC_Articulos.Name = "GC_Articulos"
        Me.GC_Articulos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1, Me.RepositoryItemTextEdit1, Me.RepositoryItemCalcEdit2})
        Me.GC_Articulos.Size = New System.Drawing.Size(768, 248)
        Me.GC_Articulos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GC_Articulos.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GC_Articulos.TabIndex = 113
        Me.GC_Articulos.Text = "GridControl1"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCodigo, Me.colBarras, Me.colDescripcion, Me.colExistencia, Me.colTomaFisica, Me.colCosto_Promedio, Me.colFamilia, Me.colUbicacion})
        Me.GridView1.GroupPanelText = ""
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colCodigo
        '
        Me.colCodigo.Caption = "Codigo"
        Me.colCodigo.FieldName = "Codigo"
        Me.colCodigo.Name = "colCodigo"
        Me.colCodigo.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCodigo.VisibleIndex = 0
        Me.colCodigo.Width = 63
        '
        'colBarras
        '
        Me.colBarras.Caption = "Barras"
        Me.colBarras.FieldName = "Barras"
        Me.colBarras.Name = "colBarras"
        Me.colBarras.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colBarras.VisibleIndex = 1
        Me.colBarras.Width = 63
        '
        'colDescripcion
        '
        Me.colDescripcion.Caption = "Descripcion"
        Me.colDescripcion.FieldName = "Descripcion"
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDescripcion.VisibleIndex = 2
        Me.colDescripcion.Width = 204
        '
        'colExistencia
        '
        Me.colExistencia.Caption = "Existencia"
        Me.colExistencia.DisplayFormat.FormatString = "###,##0.00"
        Me.colExistencia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colExistencia.FieldName = "Existencia"
        Me.colExistencia.Name = "colExistencia"
        Me.colExistencia.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colExistencia.VisibleIndex = 3
        Me.colExistencia.Width = 60
        '
        'colTomaFisica
        '
        Me.colTomaFisica.Caption = "Toma Fisica"
        Me.colTomaFisica.ColumnEdit = Me.RepositoryItemCalcEdit1
        Me.colTomaFisica.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colTomaFisica.FieldName = "TomaFisica"
        Me.colTomaFisica.Name = "colTomaFisica"
        Me.colTomaFisica.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTomaFisica.VisibleIndex = 4
        Me.colTomaFisica.Width = 60
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'colCosto_Promedio
        '
        Me.colCosto_Promedio.Caption = "Costo Promedio"
        Me.colCosto_Promedio.ColumnEdit = Me.RepositoryItemCalcEdit2
        Me.colCosto_Promedio.DisplayFormat.FormatString = "###,##0.00"
        Me.colCosto_Promedio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colCosto_Promedio.FieldName = "CostoPromedio"
        Me.colCosto_Promedio.Name = "colCosto_Promedio"
        Me.colCosto_Promedio.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCosto_Promedio.VisibleIndex = 5
        Me.colCosto_Promedio.Width = 93
        '
        'RepositoryItemCalcEdit2
        '
        Me.RepositoryItemCalcEdit2.AutoHeight = False
        Me.RepositoryItemCalcEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit2.DisplayFormat.FormatString = "###,##0.00"
        Me.RepositoryItemCalcEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit2.EditFormat.FormatString = "###,##0.00"
        Me.RepositoryItemCalcEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemCalcEdit2.Enabled = False
        Me.RepositoryItemCalcEdit2.Name = "RepositoryItemCalcEdit2"
        Me.RepositoryItemCalcEdit2.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.RepositoryItemCalcEdit2.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlDark)
        '
        'colFamilia
        '
        Me.colFamilia.Caption = "Familia"
        Me.colFamilia.FieldName = "Familia"
        Me.colFamilia.Name = "colFamilia"
        Me.colFamilia.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'colUbicacion
        '
        Me.colUbicacion.Caption = "Ubicacion"
        Me.colUbicacion.FieldName = "Ubicacion"
        Me.colUbicacion.Name = "colUbicacion"
        Me.colUbicacion.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.DisplayFormat.FormatString = "�###,##0.00"
        Me.RepositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemTextEdit1.EditFormat.FormatString = "###,##0.00"
        Me.RepositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'AdapterAjuste
        '
        Me.AdapterAjuste.DeleteCommand = Me.SqlDeleteCommand3
        Me.AdapterAjuste.InsertCommand = Me.SqlInsertCommand3
        Me.AdapterAjuste.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterAjuste.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AjusteInventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Consecutivo", "Consecutivo"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Anula", "Anula"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("TotalEntrada", "TotalEntrada"), New System.Data.Common.DataColumnMapping("TotalSalida", "TotalSalida"), New System.Data.Common.DataColumnMapping("SaldoAjuste", "SaldoAjuste"), New System.Data.Common.DataColumnMapping("ContaEntrada", "ContaEntrada"), New System.Data.Common.DataColumnMapping("ContaSalida", "ContaSalida"), New System.Data.Common.DataColumnMapping("AsientoEntrada", "AsientoEntrada"), New System.Data.Common.DataColumnMapping("AsientoSalida", "AsientoSalida")})})
        Me.AdapterAjuste.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM AjusteInventario WHERE (Consecutivo = @Original_Consecutivo) AND (Anu" & _
        "la = @Original_Anula) AND (AsientoEntrada = @Original_AsientoEntrada) AND (Asien" & _
        "toSalida = @Original_AsientoSalida) AND (Cedula = @Original_Cedula) AND (ContaEn" & _
        "trada = @Original_ContaEntrada) AND (ContaSalida = @Original_ContaSalida) AND (F" & _
        "echa = @Original_Fecha) AND (SaldoAjuste = @Original_SaldoAjuste) AND (TotalEntr" & _
        "ada = @Original_TotalEntrada) AND (TotalSalida = @Original_TotalSalida)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AsientoEntrada", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AsientoEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AsientoSalida", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AsientoSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContaEntrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContaSalida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContaSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SaldoAjuste", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SaldoAjuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO AjusteInventario(Fecha, Anula, Cedula, TotalEntrada, TotalSalida, Sal" & _
        "doAjuste, ContaEntrada, ContaSalida, AsientoEntrada, AsientoSalida) VALUES (@Fec" & _
        "ha, @Anula, @Cedula, @TotalEntrada, @TotalSalida, @SaldoAjuste, @ContaEntrada, @" & _
        "ContaSalida, @AsientoEntrada, @AsientoSalida); SELECT Consecutivo, Fecha, Anula," & _
        " Cedula, TotalEntrada, TotalSalida, SaldoAjuste, ContaEntrada, ContaSalida, Asie" & _
        "ntoEntrada, AsientoSalida FROM AjusteInventario WHERE (Consecutivo = @@IDENTITY)" & _
        ""
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 1, "Anula"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 150, "Cedula"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SaldoAjuste", System.Data.SqlDbType.Float, 8, "SaldoAjuste"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContaEntrada", System.Data.SqlDbType.Bit, 1, "ContaEntrada"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContaSalida", System.Data.SqlDbType.Bit, 1, "ContaSalida"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AsientoEntrada", System.Data.SqlDbType.VarChar, 15, "AsientoEntrada"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AsientoSalida", System.Data.SqlDbType.VarChar, 15, "AsientoSalida"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT Consecutivo, Fecha, Anula, Cedula, TotalEntrada, TotalSalida, SaldoAjuste," & _
        " ContaEntrada, ContaSalida, AsientoEntrada, AsientoSalida FROM AjusteInventario"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE AjusteInventario SET Fecha = @Fecha, Anula = @Anula, Cedula = @Cedula, Tot" & _
        "alEntrada = @TotalEntrada, TotalSalida = @TotalSalida, SaldoAjuste = @SaldoAjust" & _
        "e, ContaEntrada = @ContaEntrada, ContaSalida = @ContaSalida, AsientoEntrada = @A" & _
        "sientoEntrada, AsientoSalida = @AsientoSalida WHERE (Consecutivo = @Original_Con" & _
        "secutivo) AND (Anula = @Original_Anula) AND (AsientoEntrada = @Original_AsientoE" & _
        "ntrada) AND (AsientoSalida = @Original_AsientoSalida) AND (Cedula = @Original_Ce" & _
        "dula) AND (ContaEntrada = @Original_ContaEntrada) AND (ContaSalida = @Original_C" & _
        "ontaSalida) AND (Fecha = @Original_Fecha) AND (SaldoAjuste = @Original_SaldoAjus" & _
        "te) AND (TotalEntrada = @Original_TotalEntrada) AND (TotalSalida = @Original_Tot" & _
        "alSalida); SELECT Consecutivo, Fecha, Anula, Cedula, TotalEntrada, TotalSalida, " & _
        "SaldoAjuste, ContaEntrada, ContaSalida, AsientoEntrada, AsientoSalida FROM Ajust" & _
        "eInventario WHERE (Consecutivo = @Consecutivo)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 1, "Anula"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 150, "Cedula"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SaldoAjuste", System.Data.SqlDbType.Float, 8, "SaldoAjuste"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContaEntrada", System.Data.SqlDbType.Bit, 1, "ContaEntrada"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContaSalida", System.Data.SqlDbType.Bit, 1, "ContaSalida"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AsientoEntrada", System.Data.SqlDbType.VarChar, 15, "AsientoEntrada"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AsientoSalida", System.Data.SqlDbType.VarChar, 15, "AsientoSalida"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AsientoEntrada", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AsientoEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AsientoSalida", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AsientoSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContaEntrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContaSalida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContaSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SaldoAjuste", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SaldoAjuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consecutivo", System.Data.SqlDbType.BigInt, 8, "Consecutivo"))
        '
        'AdapterAjuste_Detalle
        '
        Me.AdapterAjuste_Detalle.DeleteCommand = Me.SqlDeleteCommand4
        Me.AdapterAjuste_Detalle.InsertCommand = Me.SqlInsertCommand4
        Me.AdapterAjuste_Detalle.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterAjuste_Detalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AjusteInventario_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Consecutivo", "Consecutivo"), New System.Data.Common.DataColumnMapping("Cons_Ajuste", "Cons_Ajuste"), New System.Data.Common.DataColumnMapping("Cod_Articulo", "Cod_Articulo"), New System.Data.Common.DataColumnMapping("Desc_Articulo", "Desc_Articulo"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Entrada", "Entrada"), New System.Data.Common.DataColumnMapping("Salida", "Salida"), New System.Data.Common.DataColumnMapping("observacion", "observacion"), New System.Data.Common.DataColumnMapping("TotalEntrada", "TotalEntrada"), New System.Data.Common.DataColumnMapping("TotalSalida", "TotalSalida"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("CostoUnit", "CostoUnit"), New System.Data.Common.DataColumnMapping("CUENTACONTABLE", "CUENTACONTABLE"), New System.Data.Common.DataColumnMapping("DESCRIPCIONCUENTACONTABLE", "DESCRIPCIONCUENTACONTABLE")})})
        Me.AdapterAjuste_Detalle.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM AjusteInventario_Detalle WHERE (Consecutivo = @Original_Consecutivo) " & _
        "AND (CUENTACONTABLE = @Original_CUENTACONTABLE OR @Original_CUENTACONTABLE IS NU" & _
        "LL AND CUENTACONTABLE IS NULL) AND (Cantidad = @Original_Cantidad) AND (Cod_Arti" & _
        "culo = @Original_Cod_Articulo) AND (Cons_Ajuste = @Original_Cons_Ajuste) AND (Co" & _
        "stoUnit = @Original_CostoUnit) AND (DESCRIPCIONCUENTACONTABLE = @Original_DESCRI" & _
        "PCIONCUENTACONTABLE OR @Original_DESCRIPCIONCUENTACONTABLE IS NULL AND DESCRIPCI" & _
        "ONCUENTACONTABLE IS NULL) AND (Desc_Articulo = @Original_Desc_Articulo) AND (Ent" & _
        "rada = @Original_Entrada) AND (Existencia = @Original_Existencia) AND (Salida = " & _
        "@Original_Salida) AND (TotalEntrada = @Original_TotalEntrada) AND (TotalSalida =" & _
        " @Original_TotalSalida) AND (observacion = @Original_observacion)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CUENTACONTABLE", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CUENTACONTABLE", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Articulo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cons_Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoUnit", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoUnit", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DESCRIPCIONCUENTACONTABLE", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DESCRIPCIONCUENTACONTABLE", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Desc_Articulo", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Desc_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Entrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Salida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Salida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_observacion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observacion", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO AjusteInventario_Detalle(Cons_Ajuste, Cod_Articulo, Desc_Articulo, Ca" & _
        "ntidad, Entrada, Salida, observacion, TotalEntrada, TotalSalida, Existencia, Cos" & _
        "toUnit, CUENTACONTABLE, DESCRIPCIONCUENTACONTABLE) VALUES (@Cons_Ajuste, @Cod_Ar" & _
        "ticulo, @Desc_Articulo, @Cantidad, @Entrada, @Salida, @observacion, @TotalEntrad" & _
        "a, @TotalSalida, @Existencia, @CostoUnit, @CUENTACONTABLE, @DESCRIPCIONCUENTACON" & _
        "TABLE); SELECT Consecutivo, Cons_Ajuste, Cod_Articulo, Desc_Articulo, Cantidad, " & _
        "Entrada, Salida, observacion, TotalEntrada, TotalSalida, Existencia, CostoUnit, " & _
        "CUENTACONTABLE, DESCRIPCIONCUENTACONTABLE FROM AjusteInventario_Detalle WHERE (C" & _
        "onsecutivo = @@IDENTITY)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, "Cons_Ajuste"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Desc_Articulo", System.Data.SqlDbType.VarChar, 250, "Desc_Articulo"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Entrada", System.Data.SqlDbType.Bit, 1, "Entrada"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Salida", System.Data.SqlDbType.Bit, 1, "Salida"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@observacion", System.Data.SqlDbType.VarChar, 250, "observacion"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoUnit", System.Data.SqlDbType.Float, 8, "CostoUnit"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CUENTACONTABLE", System.Data.SqlDbType.VarChar, 255, "CUENTACONTABLE"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DESCRIPCIONCUENTACONTABLE", System.Data.SqlDbType.VarChar, 255, "DESCRIPCIONCUENTACONTABLE"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Consecutivo, Cons_Ajuste, Cod_Articulo, Desc_Articulo, Cantidad, Entrada, " & _
        "Salida, observacion, TotalEntrada, TotalSalida, Existencia, CostoUnit, CUENTACON" & _
        "TABLE, DESCRIPCIONCUENTACONTABLE FROM AjusteInventario_Detalle"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE AjusteInventario_Detalle SET Cons_Ajuste = @Cons_Ajuste, Cod_Articulo = @C" & _
        "od_Articulo, Desc_Articulo = @Desc_Articulo, Cantidad = @Cantidad, Entrada = @En" & _
        "trada, Salida = @Salida, observacion = @observacion, TotalEntrada = @TotalEntrad" & _
        "a, TotalSalida = @TotalSalida, Existencia = @Existencia, CostoUnit = @CostoUnit," & _
        " CUENTACONTABLE = @CUENTACONTABLE, DESCRIPCIONCUENTACONTABLE = @DESCRIPCIONCUENT" & _
        "ACONTABLE WHERE (Consecutivo = @Original_Consecutivo) AND (CUENTACONTABLE = @Ori" & _
        "ginal_CUENTACONTABLE OR @Original_CUENTACONTABLE IS NULL AND CUENTACONTABLE IS N" & _
        "ULL) AND (Cantidad = @Original_Cantidad) AND (Cod_Articulo = @Original_Cod_Artic" & _
        "ulo) AND (Cons_Ajuste = @Original_Cons_Ajuste) AND (CostoUnit = @Original_CostoU" & _
        "nit) AND (DESCRIPCIONCUENTACONTABLE = @Original_DESCRIPCIONCUENTACONTABLE OR @Or" & _
        "iginal_DESCRIPCIONCUENTACONTABLE IS NULL AND DESCRIPCIONCUENTACONTABLE IS NULL) " & _
        "AND (Desc_Articulo = @Original_Desc_Articulo) AND (Entrada = @Original_Entrada) " & _
        "AND (Existencia = @Original_Existencia) AND (Salida = @Original_Salida) AND (Tot" & _
        "alEntrada = @Original_TotalEntrada) AND (TotalSalida = @Original_TotalSalida) AN" & _
        "D (observacion = @Original_observacion); SELECT Consecutivo, Cons_Ajuste, Cod_Ar" & _
        "ticulo, Desc_Articulo, Cantidad, Entrada, Salida, observacion, TotalEntrada, Tot" & _
        "alSalida, Existencia, CostoUnit, CUENTACONTABLE, DESCRIPCIONCUENTACONTABLE FROM " & _
        "AjusteInventario_Detalle WHERE (Consecutivo = @Consecutivo)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, "Cons_Ajuste"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Desc_Articulo", System.Data.SqlDbType.VarChar, 250, "Desc_Articulo"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Entrada", System.Data.SqlDbType.Bit, 1, "Entrada"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Salida", System.Data.SqlDbType.Bit, 1, "Salida"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@observacion", System.Data.SqlDbType.VarChar, 250, "observacion"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoUnit", System.Data.SqlDbType.Float, 8, "CostoUnit"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CUENTACONTABLE", System.Data.SqlDbType.VarChar, 255, "CUENTACONTABLE"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DESCRIPCIONCUENTACONTABLE", System.Data.SqlDbType.VarChar, 255, "DESCRIPCIONCUENTACONTABLE"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CUENTACONTABLE", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CUENTACONTABLE", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Articulo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cons_Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoUnit", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoUnit", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DESCRIPCIONCUENTACONTABLE", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DESCRIPCIONCUENTACONTABLE", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Desc_Articulo", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Desc_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Entrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Salida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Salida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_observacion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consecutivo", System.Data.SqlDbType.BigInt, 8, "Consecutivo"))
        '
        'ToolBarAsiento
        '
        Me.ToolBarAsiento.Enabled = False
        Me.ToolBarAsiento.ImageIndex = 5
        Me.ToolBarAsiento.Text = "Asiento"
        '
        'Toma_Fisica
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(778, 453)
        Me.Controls.Add(Me.GC_Articulos)
        Me.Controls.Add(Me.txtCedula)
        Me.Controls.Add(Me.Check_Anulado)
        Me.Controls.Add(Me.GBTomaFisica)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.LUsuario)
        Me.Controls.Add(Me.txtNumero)
        Me.MaximizeBox = True
        Me.MinimumSize = New System.Drawing.Size(784, 478)
        Me.Name = "Toma_Fisica"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Toma F�sica de inventario"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.txtNumero, 0)
        Me.Controls.SetChildIndex(Me.LUsuario, 0)
        Me.Controls.SetChildIndex(Me.txtUsuario, 0)
        Me.Controls.SetChildIndex(Me.txtNombreUsuario, 0)
        Me.Controls.SetChildIndex(Me.GBTomaFisica, 0)
        Me.Controls.SetChildIndex(Me.Check_Anulado, 0)
        Me.Controls.SetChildIndex(Me.txtCedula, 0)
        Me.Controls.SetChildIndex(Me.GC_Articulos, 0)
        CType(Me.DsTomaFisica1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GBTomaFisica.ResumeLayout(False)
        CType(Me.DT_Fecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtAsiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtAjuste.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GB_Reporte.ResumeLayout(False)
        CType(Me.Check_Inicial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GC_Articulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Load"
    Private Sub Toma_Fisica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePOs", "Conexion")
            ValoresDefecto()
            Cb_Ordenar.SelectedIndex = 0
            CB_Agrupar.SelectedIndex = 0
            DT_Fecha.EditValue = Now
            VerificaContabilidad()
            txtUsuario.Focus()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub VerificaContabilidad()
        Dim cConexion As New Conexion   'VERIFICA SI LA VARIABLE CONTABILIDAD EN CONFIGURACIONES
        Try                             'ESTA HABILITADAPARA MOSTRAR EL BOTON DEL ASIENTO
            cConexion.DesConectar(cConexion.sQlconexion)
            ToolBarAsiento.Visible = cConexion.SlqExecuteScalar(cConexion.Conectar("SeePos"), "SELECT ISNULL(Contabilidad,0) FROM Configuraciones")
        Catch ex As Exception
            ToolBarAsiento.Visible = False
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        Finally
            cConexion.DesConectar(cConexion.sQlconexion)
        End Try
    End Sub

    Private Sub ValoresDefecto()
        '-----------------------------------------------------------------------------------
        'ESTABLECER VALORES POR DEFECTO DE TOMA FISICA
        Me.DsTomaFisica1.TomaFisica.CodigoColumn.AutoIncrement = True
        Me.DsTomaFisica1.TomaFisica.CodigoColumn.AutoIncrementSeed = -1
        Me.DsTomaFisica1.TomaFisica.CodigoColumn.AutoIncrementStep = -1
        Me.DsTomaFisica1.TomaFisica.FechaColumn.DefaultValue = Now.Date
        Me.DsTomaFisica1.TomaFisica.PeriodoColumn.DefaultValue = Now.Date
        Me.DsTomaFisica1.TomaFisica.UsuarioColumn.DefaultValue = ""
        Me.DsTomaFisica1.TomaFisica.AnuladoColumn.DefaultValue = False
        Me.DsTomaFisica1.TomaFisica.InicialColumn.DefaultValue = False
        Me.DsTomaFisica1.TomaFisica.AjusteColumn.DefaultValue = False
        Me.DsTomaFisica1.TomaFisica.Num_AjusteColumn.DefaultValue = -1
        Me.DsTomaFisica1.TomaFisica.AsientoColumn.DefaultValue = False
        Me.DsTomaFisica1.TomaFisica.Num_AsientoColumn.DefaultValue = "-1"
        Me.DsTomaFisica1.TomaFisica.OrdenadoColumn.DefaultValue = ""
        Me.DsTomaFisica1.TomaFisica.AgrupadoColumn.DefaultValue = ""
        '-----------------------------------------------------------------------------------

        '-----------------------------------------------------------------------------------
        'ESTABLECER VALORES POR DEFECTO DE TOMA FISICA DETALLE
        Me.DsTomaFisica1.TomaFisica_Detalle.IdColumn.AutoIncrement = True
        Me.DsTomaFisica1.TomaFisica_Detalle.IdColumn.AutoIncrementSeed = -1
        Me.DsTomaFisica1.TomaFisica_Detalle.IdColumn.AutoIncrementStep = -1
        Me.DsTomaFisica1.TomaFisica_Detalle.IdTomaFisicaColumn.DefaultValue = 0
        Me.DsTomaFisica1.TomaFisica_Detalle.CodigoColumn.DefaultValue = 0
        Me.DsTomaFisica1.TomaFisica_Detalle.ExistenciaColumn.DefaultValue = 0
        Me.DsTomaFisica1.TomaFisica_Detalle.TomaFisicaColumn.DefaultValue = 0
        Me.DsTomaFisica1.TomaFisica_Detalle.CostoPromedioColumn.DefaultValue = 0
        '-----------------------------------------------------------------------------------

        '-----------------------------------------------------------------------------------
        'ESTABLECER VALORES POR DEFECTO DE AJUSTE DE INVENTARIO
        Me.DsTomaFisica1.AjusteInventario.ConsecutivoColumn.AutoIncrement = True
        Me.DsTomaFisica1.AjusteInventario.ConsecutivoColumn.AutoIncrementSeed = -1
        Me.DsTomaFisica1.AjusteInventario.ConsecutivoColumn.AutoIncrementStep = -1
        Me.DsTomaFisica1.AjusteInventario.FechaColumn.DefaultValue = Now.Date
        Me.DsTomaFisica1.AjusteInventario.AnulaColumn.DefaultValue = False
        Me.DsTomaFisica1.AjusteInventario.CedulaColumn.DefaultValue = ""
        Me.DsTomaFisica1.AjusteInventario.TotalEntradaColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario.TotalSalidaColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario.SaldoAjusteColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario.ContaEntradaColumn.DefaultValue = True
        Me.DsTomaFisica1.AjusteInventario.ContaSalidaColumn.DefaultValue = True
        Me.DsTomaFisica1.AjusteInventario.AsientoEntradaColumn.DefaultValue = "0"
        Me.DsTomaFisica1.AjusteInventario.AsientoSalidaColumn.DefaultValue = "0"
        '-----------------------------------------------------------------------------------

        '-----------------------------------------------------------------------------------
        'ESTABLECER VALORES POR DEFECTO DE AJUSTE INVENTARIO DETALLE
        Me.DsTomaFisica1.AjusteInventario_Detalle.ConsecutivoColumn.AutoIncrement = True
        Me.DsTomaFisica1.AjusteInventario_Detalle.ConsecutivoColumn.AutoIncrementSeed = -1
        Me.DsTomaFisica1.AjusteInventario_Detalle.ConsecutivoColumn.AutoIncrementStep = -1
        Me.DsTomaFisica1.AjusteInventario_Detalle.Cons_AjusteColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario_Detalle.Cod_ArticuloColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario_Detalle.Desc_ArticuloColumn.DefaultValue = ""
        Me.DsTomaFisica1.AjusteInventario_Detalle.CantidadColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario_Detalle.EntradaColumn.DefaultValue = False
        Me.DsTomaFisica1.AjusteInventario_Detalle.SalidaColumn.DefaultValue = False
        Me.DsTomaFisica1.AjusteInventario_Detalle.observacionColumn.DefaultValue = ""
        Me.DsTomaFisica1.AjusteInventario_Detalle.CUENTACONTABLEColumn.DefaultValue = ""
        Me.DsTomaFisica1.AjusteInventario_Detalle.DESCRIPCIONCUENTACONTABLEColumn.DefaultValue = ""
        Me.DsTomaFisica1.AjusteInventario_Detalle.TotalEntradaColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario_Detalle.TotalSalidaColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario_Detalle.ExistenciaColumn.DefaultValue = 0
        Me.DsTomaFisica1.AjusteInventario_Detalle.CostoUnitColumn.DefaultValue = 0
        '-----------------------------------------------------------------------------------
    End Sub

    Private Sub Toma_Fisica_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Me.SqlConnection1.State <> ConnectionState.Closed Then Me.SqlConnection1.Close()
    End Sub
#End Region

#Region "Controles"
    Private Sub Controles(ByVal Estado As Boolean)
        GBTomaFisica.Enabled = Estado
        TxtAjuste.Enabled = False
        TxtAsiento.Enabled = False
    End Sub

    Private Sub Check_Inicial_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Check_Inicial.CheckedChanged
        If Me.ToolBar1.Buttons(0).Text = "Cancelar" Then
            If Check_Inicial.Checked = True Then
                If VerificaInicial() Then
                    MsgBox("Ya existe un inventario inicial!", MsgBoxStyle.Critical, "Toma Fisica")
                    Check_Inicial.Checked = False
                End If
            End If
            RepositoryItemCalcEdit2.Enabled = Check_Inicial.Checked
        End If
    End Sub

#Region "KeyDown"
    Private Sub DT_Fecha_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DT_Fecha.KeyDown
        If e.KeyCode = Keys.Enter Then
            Cb_Ordenar.Focus()
        End If
    End Sub

    Private Sub Cb_Ordenar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Cb_Ordenar.KeyDown
        If e.KeyCode = Keys.Enter Then
            CB_Agrupar.Focus()
        End If
    End Sub

    Private Sub CB_Agrupar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CB_Agrupar.KeyDown
        If e.KeyCode = Keys.Enter Then
            BGenerar.Focus()
        End If
    End Sub
#End Region

#End Region

#Region "ToolBar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Try
            Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
            PMU = VSM(usua.Cedula, Me.Name) 'Carga los privilegios del usuario con el modu

            Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
                Case 1 : Nuevo()

                Case 2 : If PMU.Find Then Buscar() Else MsgBox("No tiene permiso para Buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 3 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 4 : If PMU.Delete Then Anular() Else MsgBox("No tiene permiso para eliminar o anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 5 : If PMU.Print Then Me.Imprimir() Else MsgBox("No tiene permiso para imprimir los datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 6 : If PMU.Update Then Ajuste() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 7 : Me.Close()

                Case 8 : If PMU.Update Then GuardaAsiento() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            End Select

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Nuevo"
    Private Sub Nuevo()
        Buscando = False
        Me.ToolBarNuevo.Enabled = True
        Me.ToolBarRegistrar.Enabled = False
        Me.ToolBarBuscar.Enabled = True
        Me.ToolBarImprimir.Enabled = False
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarExcel.Enabled = False
        GC_Articulos.Enabled = False
        Me.LAjuste.Visible = False
        Me.TxtAjuste.Visible = False
        Me.LAsiento.Visible = False
        Me.TxtAsiento.Visible = False
        DT_Fecha.Enabled = True
        GB_Reporte.Enabled = True

        If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then
            Try
                Me.ToolBar1.Buttons(0).Text = "Cancelar"
                Me.ToolBar1.Buttons(0).ImageIndex = 8
                Me.DsTomaFisica1.TomaFisica_Detalle.Clear()
                Me.DsTomaFisica1.TomaFisica.Clear()
                Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").EndCurrentEdit()
                Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").AddNew()
                Controles(True)
                DT_Fecha.Focus()

            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try

        Else
            Try
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").CancelCurrentEdit()
                Me.DsTomaFisica1.TomaFisica_Detalle.Clear()
                Me.DsTomaFisica1.TomaFisica.Clear()
                Controles(False)
                txtUsuario.Focus()

            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        End If
    End Sub
#End Region

#Region "Buscar"
    Private Sub Buscar()
        Dim identificador As Double
        Dim strConexion As String = SqlConnection1.ConnectionString

        Try
            If Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Count > 0 Then
                If (MsgBox("Actualmente se est� realizando una Toma Fisica de Inventario, si contin�a se perderan los datos del mismo, �desea continuar?", MsgBoxStyle.YesNo)) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If

            '-----------------------------------------------------------------------------------
            'LIMPIA LOS DATOS PARA REALIZAR LA BUSQUEDA
            DsTomaFisica1.TomaFisica_Detalle.Clear()
            DsTomaFisica1.TomaFisica.Clear()
            Me.ToolBar1.Buttons(0).Text = "Nuevo"
            Me.ToolBar1.Buttons(0).ImageIndex = 0
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'CREA EL FOMULARIO DE BUSQUEDA
            Dim frmBuscar As New BuscarToma
            Dim codigo As String
            frmBuscar.sqlstring = "SELECT TomaFisica.Codigo, TomaFisica.Periodo AS Fecha, (CASE WHEN Anulado = 0 Then 'SI' ELSE 'NO' END) AS Anulada FROM TomaFisica"
            frmBuscar.Adicional = " Order BY TomaFisica.Codigo DESC"
            frmBuscar.Text = "Buscar Toma Fisica de Inventario"
            frmBuscar.campo = "Fecha"
            frmBuscar.NuevaConexion = strConexion
            frmBuscar.ShowDialog()
            identificador = frmBuscar.codigo
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            ' SI NO SELECCIONO NINGUNA TOMA FISICA
            If identificador = 0.0 Then
                Controles(False)
                GC_Articulos.Enabled = False
                Buscando = False
                Exit Sub
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'SI SELECCIONO TOMA FISICA
            CargarToma(identificador)
            '-----------------------------------------------------------------------------------

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub CargarToma(ByVal Identificador As Double)
        Try
            If Identificador = 0.0 Then
                Buscando = False
                Exit Sub
            End If

            Dim Fx As New cFunciones
            Dim strConexion As String = SqlConnection1.ConnectionString
            Buscando = True

            '-----------------------------------------------------------------------------------
            'CARGAR LA TOMA FISICA
            Fx.Cargar_Tabla_Generico(Me.AdapterTomaFisica, "SELECT * FROM TomaFisica WHERE Codigo = " & Identificador, strConexion)
            Me.AdapterTomaFisica.Fill(Me.DsTomaFisica1, "TomaFisica")
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'CARGAR LOS DETALLES DE LA TOMA FISICA
            Fx.Cargar_Tabla_Generico(Me.AdapterTomaFisicaDetalle, "SELECT * FROM TomaFisica_Detalle WHERE IdTomaFisica = " & Identificador, strConexion)
            Me.AdapterTomaFisicaDetalle.Fill(Me.DsTomaFisica1, "TomaFisica_Detalle")
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'ACTIVA CONTROLES
            Me.GC_Articulos.Enabled = True
            Controles(True)
            Me.ToolBar1.Buttons(4).Enabled = True
            Me.ToolBar1.Buttons(0).Enabled = True
            Me.ToolBar1.Buttons(5).Enabled = True
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'CRITERIOS DE ORDENAMIENTO
            If Cb_Ordenar.SelectedIndex = 0 Then    'ORDENADO POR DESCRIPCION
                colDescripcion.SortIndex = 0
                colCodigo.SortIndex = -1
                colDescripcion.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
                colCodigo.SortOrder = DevExpress.Data.ColumnSortOrder.None
            Else                                    'ORDENADO POR CODIGO
                colDescripcion.SortIndex = -1
                colCodigo.SortIndex = 0
                colDescripcion.SortOrder = DevExpress.Data.ColumnSortOrder.None
                colCodigo.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'CRITERIOS DE AGRUPACION
            If CB_Agrupar.SelectedIndex = 0 Then    'AGRUPADO POR UBICACION
                colUbicacion.GroupIndex = 0
                colFamilia.GroupIndex = -1
                GridView1.ExpandAllGroups()
            ElseIf CB_Agrupar.SelectedIndex = 1 Then 'AGRUPADO POR FAMILIA
                colUbicacion.GroupIndex = -1
                colFamilia.GroupIndex = 0
                GridView1.ExpandAllGroups()
            Else                                    'SIN AGRUPAR
                colUbicacion.GroupIndex = -1
                colFamilia.GroupIndex = -1
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA SI LA TOMA FISICA ESTA ANULADA PARA ACTIVAR O NO EL BOTON DE REGISTRAR, ANULAR Y AJUSTE
            If Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Current("Anulado") = True Then
                Me.ToolBar1.Buttons(2).Enabled = False  'SI ESTA ANULADO
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBarExcel.Enabled = False
                Controles(False)
                GC_Articulos.Enabled = False
            Else
                Me.ToolBar1.Buttons(2).Enabled = True   'SI NO ESTA ANULADO
                Me.ToolBar1.Buttons(3).Enabled = True
                '-----------------------------------------------------------------------------------
                'VERIFICA SI A LA TOMA FISICA YA SE LE APLICO EL AJUSTE DE INVENTARIO
                If Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Current("Ajuste") = True Then
                    Me.ToolBarExcel.Enabled = False     'SI YA SE APLICO AJUSTE
                    '-----------------------------------------------------------------------------------
                    'VERIFICA SI A LA TOMA FISICA YA SE LE APLICO EL ASIENTO CONTABLE
                    If Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Current("Asiento") = True Then
                        Me.ToolBarAsiento.Enabled = False     'SI YA SE APLICO ASIENTO
                    Else
                        Me.ToolBarAsiento.Enabled = True
                    End If
                    '-----------------------------------------------------------------------------------
                Else
                    Me.ToolBarExcel.Enabled = True
                    Me.ToolBarAsiento.Enabled = False
                End If
                '-----------------------------------------------------------------------------------
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA SI A LA TOMA FISICA YA SE LE APLICO EL AJUSTE DE INVENTARIO
            If Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Current("Ajuste") = True Then
                Me.LAjuste.Visible = True     'SI YA SE APLICO AJUSTE
                Me.TxtAjuste.Visible = True
                Controles(False)
                GC_Articulos.Enabled = False
            Else
                Me.LAjuste.Visible = False     'SI NO SE APLICO AJUSTE
                Me.TxtAjuste.Visible = False
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA SI A LA TOMA FISICA YA SE LE APLICO EL ASIENTO CONTABLE
            If Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Current("Asiento") = True Then
                Me.LAsiento.Visible = True     'SI YA SE APLICO ASIENTO
                Me.TxtAsiento.Visible = True
            Else
                Me.LAsiento.Visible = False     'SI NO SE APLICO ASIENTO
                Me.TxtAsiento.Visible = False
            End If
            '-----------------------------------------------------------------------------------

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Registrar"
    Function Registrar()
        Dim Funciones As New Conexion
        Try
            '-----------------------------------------------------------------------------------
            'VERIFICA SI YA SE APLICO UN AJUSTE PARA LA TOMA FISICA
            If Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Ajuste") = True Then
                MsgBox("No se puede modificar la toma Fisica!!" & vbCrLf & "Porque ya fue aplicado un ajuste de inventario!!", MsgBoxStyle.Critical, "Toma Fisica")
                Exit Function
            End If
            '-----------------------------------------------------------------------------------

            If MessageBox.Show("�Desea guardar la Toma F�sica?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Current("Usuario") = txtCedula.Text

                '-----------------------------------------------------------------------------------
                'ACTUALIZA LOS ULTIMOS CAMBIOS DEL GRID
                GridView1.CloseEditor()
                GridView1.UpdateCurrentRow()
                '-----------------------------------------------------------------------------------

                Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").EndCurrentEdit()
                Me.BindingContext(DsTomaFisica1, "TomaFisica").EndCurrentEdit()

                If Me.Registrar_Toma() Then
                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    Me.ToolBar1.Buttons(1).Enabled = True
                    MsgBox("La Toma Fisica se guardo Satisfactoriamente", MsgBoxStyle.Information)

                    '-----------------------------------------------------------------------------------
                    'IMPRIMIR
                    If MessageBox.Show("�Desea Imprimir la Toma F�sica?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        Imprimir()
                    End If
                    '-----------------------------------------------------------------------------------

                    '-----------------------------------------------------------------------------------
                    'LIMPIA DATOS Y BLOQUEA CONTROLES               
                    DsTomaFisica1.TomaFisica_Detalle.Clear()
                    DsTomaFisica1.TomaFisica.Clear()
                    Controles(False)
                    GC_Articulos.Enabled = False
                    Buscando = False
                    Me.ToolBar1.Buttons(2).Enabled = False
                    Me.ToolBar1.Buttons(3).Enabled = False
                    Me.ToolBar1.Buttons(4).Enabled = False
                    Me.ToolBarExcel.Enabled = False
                    Me.LAjuste.Visible = False
                    Me.TxtAjuste.Visible = False
                    Me.LAsiento.Visible = False
                    Me.TxtAsiento.Visible = False
                    '-----------------------------------------------------------------------------------

                Else
                    MsgBox("Error al Guardar la Toma Fisica de inventario", MsgBoxStyle.Critical)
                End If
            Else
                Exit Function
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Function Registrar_Toma() As Boolean    'TRANSACCION PARA LA TOMA FISICA
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            Me.AdapterTomaFisica.InsertCommand.Transaction = Trans
            Me.AdapterTomaFisica.DeleteCommand.Transaction = Trans
            Me.AdapterTomaFisica.UpdateCommand.Transaction = Trans
            Me.AdapterTomaFisica.SelectCommand.Transaction = Trans

            Me.AdapterTomaFisicaDetalle.InsertCommand.Transaction = Trans
            Me.AdapterTomaFisicaDetalle.DeleteCommand.Transaction = Trans
            Me.AdapterTomaFisicaDetalle.UpdateCommand.Transaction = Trans
            Me.AdapterTomaFisicaDetalle.SelectCommand.Transaction = Trans

            Me.AdapterTomaFisica.Update(Me.DsTomaFisica1.TomaFisica)
            Me.AdapterTomaFisicaDetalle.Update(Me.DsTomaFisica1.TomaFisica_Detalle)

            Trans.Commit()
            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try
    End Function

    Function Registrar_Actualizacion_Toma() As Boolean      'TRANSACCION PARA LA ACTUALIZACION DE LA TOMA FISICA
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction

        Try
            Me.AdapterTomaFisica.UpdateCommand.Transaction = Trans
            Me.AdapterTomaFisica.Update(Me.DsTomaFisica1, "TomaFisica")
            Trans.Commit()
            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(3).Enabled = True
            Return False
        End Try
    End Function
#End Region

#Region "Anular"
    Function Anular()
        Try
            Dim resp As Integer
            '-----------------------------------------------------------------------------------
            'VERIFICA SI YA SE APLICO UN AJUSTE PARA LA TOMA FISICA
            If Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Ajuste") = True Then
                MsgBox("La toma Fisica no se puede anular porque ya se le aplico el ajuste de inventario!!", MsgBoxStyle.Critical, "Toma Fisica")
                Exit Function
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA SI YA SE APLICO EL ASIENTO PARA LA TOMA FISICA
            If Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Asiento") = True Then
                MsgBox("La toma Fisica no se puede anular porque ya se le aplico el asiento contable!!", MsgBoxStyle.Critical, "Toma Fisica")
                Exit Function
            End If
            '-----------------------------------------------------------------------------------

            If Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Count > 0 Then
                resp = MessageBox.Show("�Desea Anular esta Toma Fisica?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").Current("Anulado") = True
                    Me.BindingContext(Me.DsTomaFisica1, "TomaFisica").EndCurrentEdit()

                    If Registrar_Actualizacion_Toma() Then
                        Me.DsTomaFisica1.AcceptChanges()
                        MsgBox("La Toma Fisica ha sido anulado correctamente", MsgBoxStyle.Information)

                        '-----------------------------------------------------------------------------------
                        'LIMPIA DATOS Y BLOQUEA CONTROLES               
                        DsTomaFisica1.TomaFisica_Detalle.Clear()
                        DsTomaFisica1.TomaFisica.Clear()
                        Me.ToolBar1.Buttons(0).Text = "Nuevo"
                        Me.ToolBar1.Buttons(0).ImageIndex = 0
                        Me.ToolBar1.Buttons(0).Enabled = True
                        Me.ToolBar1.Buttons(1).Enabled = True
                        Me.ToolBar1.Buttons(2).Enabled = False
                        Me.ToolBar1.Buttons(3).Enabled = False
                        Me.ToolBar1.Buttons(4).Enabled = False
                        Me.ToolBarExcel.Enabled = False
                        Controles(False)
                        Buscando = False
                        Me.LAjuste.Visible = False
                        Me.TxtAjuste.Visible = False
                        Me.LAsiento.Visible = False
                        Me.TxtAsiento.Visible = False
                        '-----------------------------------------------------------------------------------

                    End If

                Else : Exit Function
                End If
            Else
                MsgBox("No hay ninguna toma fisica seleccionada para anular!!", MsgBoxStyle.Critical, "Toma Fisica")
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Function
#End Region

#Region "Imprimir"
    Function Imprimir()
        Try
            Me.ToolBar1.Buttons(4).Enabled = False

            '-----------------------------------------------------------------------------------
            'CRITERIOS DE AGRUPAMIENTO PARA EL REPORTE
            If CB_Agrupar.SelectedIndex = 0 Then
                Dim Reporte As New Reporte_Toma_Fisica_Ubicacion    'POR UBICACION
                TomaFisica = Reporte
            ElseIf CB_Agrupar.SelectedIndex = 1 Then
                Dim Reporte As New Reporte_Toma_Fisica_Familia      'POR FAMILIA
                TomaFisica = Reporte
            Else
                Dim Reporte As New Reporte_Toma_Fisica              'SIN AGRUPAR
                TomaFisica = Reporte
            End If
            '-----------------------------------------------------------------------------------

            Dim Ordenamiento As CrystalDecisions.CrystalReports.Engine.DatabaseFieldDefinition
            TomaFisica.SetParameterValue(0, BindingContext(DsTomaFisica1, "TomaFisica").Current("Codigo"))

            '-----------------------------------------------------------------------------------
            'CRITERIOS DE ORDENAMIENTO PARA EL REPORTE
            Ordenamiento = TomaFisica.Database.Tables.Item(2).Fields.Item(Cb_Ordenar.Text)
            TomaFisica.DataDefinition.SortFields.Item(1).Field = Ordenamiento
            TomaFisica.DataDefinition.SortFields(1).SortDirection = CrystalDecisions.[Shared].SortDirection.AscendingOrder
            '-----------------------------------------------------------------------------------

            CrystalReportsConexion.LoadShow(TomaFisica, MdiParent, Me.SqlConnection1.ConnectionString)
            Me.ToolBar1.Buttons(4).Enabled = True

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

#Region "Imprimir Ajuste"
    Function Imprimir_Ajuste()
        Try
            Dim Ajuste_Reporte As New Reporte_Ajuste_Inventario

            Ajuste_Reporte.SetParameterValue(0, BindingContext(DsTomaFisica1, "AjusteInventario").Current("Consecutivo"))
            CrystalReportsConexion.LoadShow(Ajuste_Reporte, MdiParent, SqlConnection1.ConnectionString)

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function
#End Region

#End Region

#Region "Validaciones"
    Function VerificaInicial() As Boolean
        Dim cConexion As New Conexion   'VALIDA SI SE HA GENERADO EL INVENTARIO INICIAL
        Dim Cantidad As Integer
        Try
            VerificaInicial = False
            cantidad = cConexion.SlqExecuteScalar(cConexion.Conectar("SeePos"), "SELECT ISNULL(COUNT(Codigo),0) AS Inicial FROM TomaFisica " & _
                        "WHERE (Inicial = 1) AND (Anulado = 0)")
            If Cantidad > 0 Then
                VerificaInicial = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(cConexion.sQlconexion)
        End Try
    End Function

    Function ValidaMes() As Boolean
        Dim cConexion As New Conexion   'VALIDA SI NO SE HA GENERADO UNA TOMA FISICA PARA EL PERIODO Y LA BODEGA
        Dim Cantidad As Integer
        Try
            ValidaMes = True
            Cantidad = cConexion.SlqExecuteScalar(cConexion.Conectar("SeePos"), "SELECT ISNULL(COUNT(Codigo),0) AS Periodo FROM TomaFisica " & _
                        "WHERE (Periodo = dbo.DateOnly('" & DT_Fecha.EditValue & "')) AND (Anulado = 0)")
            If cantidad > 0 Then
                ValidaMes = False
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(cConexion.sQlconexion)
        End Try
    End Function

#Region "Validacion Usuario"
    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim cConexion As New Conexion
            Dim Conx As New SqlConnection
            Conx.ConnectionString = Me.SqlConnection1.ConnectionString
            Try
                If Conx.State <> ConnectionState.Open Then Conx.Open()
                Dim rs As SqlDataReader

                If txtUsuario.Text <> "" Then
                    rs = cConexion.GetRecorset(Conx, "SELECT Cedula, Nombre from Usuarios where Clave_Interna ='" & txtUsuario.Text & "'")

                    If rs.HasRows = False Then
                        MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
                        Me.ToolBar1.Buttons(0).Enabled = False
                        Me.ToolBar1.Buttons(1).Enabled = False
                        txtUsuario.Text = ""
                        txtUsuario.Focus()
                    End If
                    While rs.Read
                        Try
                            txtNombreUsuario.Text = rs("Nombre")
                            txtCedula.Text = rs("Cedula")
                            txtUsuario.Enabled = False ' se inabilita el campo de la contrase�a
                            Me.ToolBar1.Buttons(0).Enabled = True
                            Me.ToolBar1.Buttons(1).Enabled = True
                            Nuevo()

                        Catch ex As SystemException
                            MsgBox(ex.Message)
                        End Try
                    End While
                    rs.Close()
                    cConexion.DesConectar(cConexion.Conectar("SeePos"))

                Else
                    MsgBox("Debe de digitar la clave de usuario", MsgBoxStyle.Exclamation)
                    txtUsuario.Focus()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
            Finally
                If Conx.State <> ConnectionState.Closed Then Conx.Close()
            End Try
        End If
    End Sub
#End Region

#End Region

#Region "Generar"
    Private Sub BGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BGenerar.Click
        Generar()
    End Sub

    Private Sub BGenerar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles BGenerar.KeyDown
        If e.KeyCode = Keys.Enter Then
            Generar()
        End If
    End Sub

    Private Sub Generar()
        Dim cConexion As New Conexion
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader

        Try
            '-----------------------------------------------------------------------------------
            'VERIFICA SI YA SE APLICO UN AJUSTE PARA LA TOMA FISICA
            If Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Ajuste") = True Then
                MsgBox("No se puede volver a generar la toma Fisica!!" & vbCrLf & "Porque ya fue aplicado un ajuste de inventario!!", MsgBoxStyle.Critical, "Toma Fisica")
                Exit Sub
            End If
            '-----------------------------------------------------------------------------------
            DT_Fecha.Enabled = False : GB_Reporte.Enabled = False

            Me.BindingContext(DsTomaFisica1, "TomaFisica").EndCurrentEdit()

            If Buscando = False Then        'SI NO ESTA EDITANDO HACE LOS CALCULOS
                If ValidaMes() Then     'VALIDA EL MES DE LA TOMA FISICA
                    DsTomaFisica1.TomaFisica_Detalle.Clear()
                    rs = cConexion.GetRecorset(cConexion.Conectar("SeePos"), "SELECT dbo.Inventario.Codigo, dbo.Inventario.Barras, dbo.Inventario.Descripcion, dbo.Inventario.Existencia, " & _
                        "dbo.Inventario.Costo, dbo.Familia.Descripcion AS Familia, dbo.Ubicaciones.Descripcion AS Ubicacion " & _
                        "FROM dbo.Inventario INNER JOIN " & _
                        "dbo.SubFamilias INNER JOIN dbo.Familia ON dbo.SubFamilias.CodigoFamilia = dbo.Familia.Codigo ON dbo.Inventario.SubFamilia = dbo.SubFamilias.Codigo INNER JOIN " & _
                        "dbo.Ubicaciones INNER JOIN dbo.SubUbicacion ON dbo.Ubicaciones.Codigo = dbo.SubUbicacion.Cod_Ubicacion ON dbo.Inventario.SubUbicacion = dbo.SubUbicacion.Codigo " & _
                        "WHERE dbo.Inventario.Inhabilitado = 0 AND  dbo.Inventario.Servicio = 0")

                    While rs.Read
                        '-----------------------------------------------------------------------------------
                        'CREA LOS ARTICULOS DE LA TOMA FISICA
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").CancelCurrentEdit()
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").AddNew()
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Current("Codigo") = rs("Codigo")
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Current("Barras") = rs("Barras")
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Current("Descripcion") = rs("Descripcion")
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Current("TomaFisica") = 0
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Current("Existencia") = rs("Existencia")
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Current("CostoPromedio") = rs("Costo")
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Current("Familia") = rs("Familia")
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Current("Ubicacion") = rs("Ubicacion")
                        Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").EndCurrentEdit()
                        '-----------------------------------------------------------------------------------
                    End While
                    rs.Close()
                Else
                    MsgBox("Ya existe una toma fisica de este dia!!", MsgBoxStyle.Information, "Toma Fisica")
                    GC_Articulos.Enabled = False
                    Me.ToolBarRegistrar.Enabled = False
                    Exit Sub
                End If
            End If

            '-----------------------------------------------------------------------------------
            'CRITERIOS DE ORDENAMIENTO
            If Cb_Ordenar.SelectedIndex = 0 Then    'ORDENADO POR DESCRIPCION
                colDescripcion.SortIndex = 0
                colCodigo.SortIndex = -1
                colDescripcion.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
                colCodigo.SortOrder = DevExpress.Data.ColumnSortOrder.None
            Else                                    'ORDENADO POR CODIGO
                colDescripcion.SortIndex = -1
                colCodigo.SortIndex = 0
                colDescripcion.SortOrder = DevExpress.Data.ColumnSortOrder.None
                colCodigo.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'CRITERIOS DE AGRUPACION
            If CB_Agrupar.SelectedIndex = 0 Then    'AGRUPADO POR UBICACION
                colUbicacion.GroupIndex = 0
                colFamilia.GroupIndex = -1
                GridView1.ExpandAllGroups()
            ElseIf CB_Agrupar.SelectedIndex = 1 Then 'AGRUPADO POR FAMILIA
                colUbicacion.GroupIndex = -1
                colFamilia.GroupIndex = 0
                GridView1.ExpandAllGroups()
            Else                                    'SIN AGRUPAR
                colUbicacion.GroupIndex = -1
                colFamilia.GroupIndex = -1
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA SI HAY DETALLES PARA HABILITAR O NO EL GIRD Y EL BOTON DE REGISTRAR
            If Me.BindingContext(DsTomaFisica1, "TomaFisica.TomaFisicaTomaFisica_Detalle").Count > 0 Then
                GC_Articulos.Enabled = True     'HABILITA
                Me.ToolBarRegistrar.Enabled = True
            Else
                GC_Articulos.Enabled = False    'DESHABILITA
                Me.ToolBarRegistrar.Enabled = False
            End If
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
    End Sub
#End Region

#Region "Ajuste de Inventario"
    Private Sub Ajuste()
        Dim Cantidad, TEntrada, TSalida, Identificador As Double

        Try
            '-----------------------------------------------------------------------------------
            'VERIFICA SI YA SE APLICO UN AJUSTE PARA LA TOMA FISICA
            If Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Ajuste") = True Then
                MsgBox("Ya fue aplicado un ajuste de inventario para esta toma fisica!!", MsgBoxStyle.Critical, "Toma Fisica")
                Exit Sub
            End If
            '-----------------------------------------------------------------------------------

            If MessageBox.Show("�Desea crear el ajuste de inventario para la Toma F�sica?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                Exit Sub
            End If
            Me.ToolBarExcel.Enabled = False

            '-----------------------------------------------------------------------------------
            'CREA LOS DATOS DE LA TABLA DE AJUSTE DE INVENTARIO
            Me.BindingContext(DsTomaFisica1, "AjusteInventario").CancelCurrentEdit()
            Me.BindingContext(DsTomaFisica1, "AjusteInventario").AddNew()
            Me.BindingContext(DsTomaFisica1, "AjusteInventario").Current("Fecha") = DT_Fecha.EditValue
            Me.BindingContext(DsTomaFisica1, "AjusteInventario").Current("Cedula") = txtCedula.Text
            Me.BindingContext(DsTomaFisica1, "AjusteInventario").EndCurrentEdit()
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'CREA LOS DETALLES DEL AJUSTE
            For i As Integer = 0 To DsTomaFisica1.TomaFisica_Detalle.Count - 1
                Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
                Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").AddNew()
                Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Cod_Articulo") = DsTomaFisica1.TomaFisica_Detalle(i).Codigo
                Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Desc_Articulo") = DsTomaFisica1.TomaFisica_Detalle(i).Descripcion
                Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Existencia") = DsTomaFisica1.TomaFisica_Detalle(i).TomaFisica
                Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Observacion") = "Toma F�sica de inventario # " & BindingContext(DsTomaFisica1, "TomaFisica").Current("Codigo")
                Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("CostoUnit") = DsTomaFisica1.TomaFisica_Detalle(i).CostoPromedio

                '-----------------------------------------------------------------------------------
                'REALIZA EL CALCULO DE LA CANTIDAD DE AJUSTE
                If DsTomaFisica1.TomaFisica_Detalle(i).Existencia > 0 Then
                    Cantidad = (DsTomaFisica1.TomaFisica_Detalle(i).TomaFisica - DsTomaFisica1.TomaFisica_Detalle(i).Existencia)
                Else
                    'Cuando la existencia es negativa cambia el c�lculo.
                    If DsTomaFisica1.TomaFisica_Detalle(i).TomaFisica > DsTomaFisica1.TomaFisica_Detalle(i).Existencia Then
                        Cantidad = Math.Abs((DsTomaFisica1.TomaFisica_Detalle(i).TomaFisica - DsTomaFisica1.TomaFisica_Detalle(i).Existencia))
                    Else
                        Cantidad = -1 * (DsTomaFisica1.TomaFisica_Detalle(i).Existencia - DsTomaFisica1.TomaFisica_Detalle(i).TomaFisica)
                    End If
                End If
                Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Cantidad") = Math.Abs(Cantidad)
                '-----------------------------------------------------------------------------------

                '-----------------------------------------------------------------------------------
                'VERIFICA LA CANTIDAD DE AJUSTE PARA SABER SI ES ENTRADA, SALIDA O SI NO SE HACE AJUSTE PARA EL ARTICULO
                If Cantidad > 0 Then        'ENTRADA
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Entrada") = 1
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("TotalEntrada") = (Math.Abs(Cantidad) * DsTomaFisica1.TomaFisica_Detalle(i).CostoPromedio)
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Salida") = 0
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()
                    TEntrada += Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("TotalEntrada")
                ElseIf Cantidad < 0 Then    'SALIDA
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Salida") = 1
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("TotalSalida") = (Math.Abs(Cantidad) * DsTomaFisica1.TomaFisica_Detalle(i).CostoPromedio)
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Entrada") = 0
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()
                    TSalida += Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("TotalSalida")
                Else                        'SIN AJUSTE
                    Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
                End If
                '-----------------------------------------------------------------------------------
            Next
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'ACTUALIZA LOS TOTALES DEL AJUSTE
            Me.BindingContext(DsTomaFisica1, "AjusteInventario").Current("TotalEntrada") = TEntrada
            Me.BindingContext(DsTomaFisica1, "AjusteInventario").Current("TotalSalida") = TSalida
            Me.BindingContext(DsTomaFisica1, "AjusteInventario").Current("SaldoAjuste") = (TEntrada - TSalida)
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA SI SE HIZO ALGUN DETALLE DEL AJUSTE
            If Me.BindingContext(DsTomaFisica1, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Count > 0 Then
                Me.BindingContext(DsTomaFisica1, "AjusteInventario").EndCurrentEdit()

                If Registrar_Ajuste() Then
                    Me.ToolBar1.Buttons(1).Enabled = True
                    MsgBox("El ajuste de inventario para la Toma Fisica se guardo Satisfactoriamente", MsgBoxStyle.Information)

                    '-----------------------------------------------------------------------------------
                    'IMPRIMIR
                    If MessageBox.Show("�Desea Imprimir el ajuste de Inventario?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        Imprimir_Ajuste()
                    End If
                    '-----------------------------------------------------------------------------------

                    '-----------------------------------------------------------------------------------
                    'LIMPIA DATOS Y BLOQUEA CONTROLES
                    Identificador = Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Codigo")
                    DsTomaFisica1.TomaFisica_Detalle.Clear()
                    DsTomaFisica1.TomaFisica.Clear()
                    DsTomaFisica1.AjusteInventario_Detalle.Clear()
                    DsTomaFisica1.AjusteInventario.Clear()
                    Controles(False)
                    GC_Articulos.Enabled = False
                    Buscando = False
                    Me.ToolBar1.Buttons(2).Enabled = False
                    Me.ToolBar1.Buttons(3).Enabled = False
                    Me.ToolBar1.Buttons(4).Enabled = False
                    Me.ToolBarExcel.Enabled = False
                    Me.LAjuste.Visible = False
                    Me.TxtAjuste.Visible = False
                    Me.LAsiento.Visible = False
                    Me.TxtAsiento.Visible = False
                    '-----------------------------------------------------------------------------------

                    '-----------------------------------------------------------------------------------
                    'SE VUELVE A CARGAR LA TOMA FISICA
                    CargarToma(Identificador)
                    '-----------------------------------------------------------------------------------
                Else
                    MsgBox("Error al Guardar el Ajuste de inventario", MsgBoxStyle.Critical)
                End If
            Else        'SI NO HAY DETALLES SE CANCELA LA EDICION
                Me.BindingContext(DsTomaFisica1, "AjusteInventario").CancelCurrentEdit()
                MsgBox("No se creo el ajuste de inventario porque no se cambiaron las existencias!!!", MsgBoxStyle.Information)
            End If
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub

    Function Registrar_Ajuste() As Boolean
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Dim Cx As New Conexion

        Try
            Me.AdapterAjuste.InsertCommand.Transaction = Trans
            Me.AdapterAjuste.DeleteCommand.Transaction = Trans
            Me.AdapterAjuste.UpdateCommand.Transaction = Trans
            Me.AdapterAjuste.SelectCommand.Transaction = Trans

            Me.AdapterAjuste_Detalle.InsertCommand.Transaction = Trans
            Me.AdapterAjuste_Detalle.DeleteCommand.Transaction = Trans
            Me.AdapterAjuste_Detalle.UpdateCommand.Transaction = Trans
            Me.AdapterAjuste_Detalle.SelectCommand.Transaction = Trans

            Me.AdapterAjuste.Update(Me.DsTomaFisica1.AjusteInventario)
            Me.AdapterAjuste_Detalle.Update(Me.DsTomaFisica1.AjusteInventario_Detalle)

            Trans.Commit()
            '-----------------------------------------------------------------------------------
            'ACTUALIZA EL AJUSTE DE INVENTARIO
            Cx.UpdateRecords("TomaFisica", "Ajuste = 1, Num_Ajuste = " & Me.BindingContext(DsTomaFisica1, "AjusteInventario").Current("Consecutivo"), "Codigo = " & Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Codigo"), "SeePos")
            '-----------------------------------------------------------------------------------

            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(5).Enabled = True
            Return False
        End Try
    End Function
#End Region

#Region "Asientos Contables"
    Public Sub GuardaAsiento()
        Dim Fx As New cFunciones
        Dim Identificador As Double

        Try
            '-----------------------------------------------------------------------------------
            'VERIFICA SI YA SE APLICO UN AJUSTE PARA LA TOMA FISICA
            If Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Ajuste") = False Then
                MsgBox("NO se ha aplicado un ajuste de inventario para esta toma fisica!!" & vbCrLf & "Debe aplicar el ajuste de inventario primero!!", MsgBoxStyle.Critical, "Toma Fisica")
                Exit Sub
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA SI YA SE APLICO UN ASIENTO PARA LA TOMA FISICA
            If Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Asiento") = True Then
                MsgBox("Ya fue aplicado un asiento contable para esta toma fisica!!", MsgBoxStyle.Critical, "Toma Fisica")
                Exit Sub
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA EL PERIODO DE TRABAJO
            'If Fx.ValidarPeriodo(DT_Fecha.EditValue) = False Then
            '    MsgBox("La fecha NO corresponde al periodo de trabajo! O el periodo esta cerrado!" & vbCrLf & "No se puede Generar el Asiento", MsgBoxStyle.Information, "Sistema SeeSoft")
            '    Exit Sub
            'End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'LLAMA AL FORMULARIO PARA EL ASIENTO CONTABLE
            Dim AsientoTomaFisica As New Asiento_Toma
            AsientoTomaFisica.IdToma = Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Codigo")
            AsientoTomaFisica.TituloModulo.Text = "Asiento para la Toma Fisica # " & Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Codigo")
            'AsientoTomaFisica.Produccion = Me.BindingContext(DsTomaFisica1, "Bodega").Current("Produccion")
            'AsientoTomaFisica.CuentaBodega = Me.BindingContext(DsTomaFisica1, "Bodega").Current("CuentaContable")
            'AsientoTomaFisica.DescripcionCuentaBodega = Me.BindingContext(DsTomaFisica1, "Bodega").Current("DescripcionCuentaContable")
            AsientoTomaFisica.Usuario = txtNombreUsuario.Text
            AsientoTomaFisica.ShowDialog()
            If AsientoTomaFisica.Registro Then
                '-----------------------------------------------------------------------------------
                'LIMPIA DATOS Y BLOQUEA CONTROLES 
                Identificador = Me.BindingContext(DsTomaFisica1, "TomaFisica").Current("Codigo")
                DsTomaFisica1.TomaFisica_Detalle.Clear()
                DsTomaFisica1.TomaFisica.Clear()
                Controles(False)
                GC_Articulos.Enabled = False
                Buscando = False
                Me.ToolBar1.Buttons(2).Enabled = False
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = False
                Me.ToolBarExcel.Enabled = False
                Me.ToolBarAsiento.Enabled = False
                Me.LAjuste.Visible = False
                Me.TxtAjuste.Visible = False
                Me.LAsiento.Visible = False
                Me.TxtAsiento.Visible = False
                '-----------------------------------------------------------------------------------

                '-----------------------------------------------------------------------------------
                'SE VUELVE A CARGAR LA TOMA FISICA
                CargarToma(Identificador)
                '-----------------------------------------------------------------------------------
            Else
                MsgBox("No se guardo el asiento Contable para la toma fisica!!", MsgBoxStyle.Information, "Sistema SeeSoft")
            End If
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub
#End Region

End Class
