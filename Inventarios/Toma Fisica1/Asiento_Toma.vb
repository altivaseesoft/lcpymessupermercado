Imports System.Windows.Forms
Imports System.Data.SqlClient

Public Class Asiento_Toma
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Public WithEvents TituloModulo As System.Windows.Forms.Label
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterAsientos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterDetallesAsientos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsTomaAsiento1 As DsTomaAsiento
    Friend WithEvents AdapterTomaFisica As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection2 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterTomaFisica_Detalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GC_Asientos As DevExpress.XtraGrid.GridControl
    Friend WithEvents colCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDebe As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHaber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TxtDetalle As System.Windows.Forms.TextBox
    Friend WithEvents BotonCerrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButtonAgregarDetalle As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelAgregar As System.Windows.Forms.Panel
    Friend WithEvents LAgregar As System.Windows.Forms.Label
    Friend WithEvents txtCuentaContable As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcionCuenta As System.Windows.Forms.TextBox
    Friend WithEvents colCuentaC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMonto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescripcionC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtMonto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridAgregar As DevExpress.XtraGrid.GridControl
    Friend WithEvents TxtTotalDebe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTotalHaber As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Asiento_Toma))
        Me.TituloModulo = New System.Windows.Forms.Label
        Me.GC_Asientos = New DevExpress.XtraGrid.GridControl
        Me.DsTomaAsiento1 = New DsTomaAsiento
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDebe = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colHaber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.AdapterAsientos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.AdapterDetallesAsientos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.AdapterTomaFisica = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection2 = New System.Data.SqlClient.SqlConnection
        Me.AdapterTomaFisica_Detalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.BGuardar = New DevExpress.XtraEditors.SimpleButton
        Me.BCancelar = New DevExpress.XtraEditors.SimpleButton
        Me.PanelAgregar = New System.Windows.Forms.Panel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtDescripcionCuenta = New System.Windows.Forms.TextBox
        Me.txtCuentaContable = New System.Windows.Forms.TextBox
        Me.BNuevo = New DevExpress.XtraEditors.SimpleButton
        Me.TxtDetalle = New System.Windows.Forms.TextBox
        Me.BotonCerrar = New DevExpress.XtraEditors.SimpleButton
        Me.GridAgregar = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCuentaC = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescripcionC = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMonto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ButtonAgregarDetalle = New DevExpress.XtraEditors.SimpleButton
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtMonto = New DevExpress.XtraEditors.TextEdit
        Me.LAgregar = New System.Windows.Forms.Label
        Me.txtTotalHaber = New DevExpress.XtraEditors.TextEdit
        Me.TxtTotalDebe = New DevExpress.XtraEditors.TextEdit
        CType(Me.GC_Asientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsTomaAsiento1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelAgregar.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.GridAgregar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalHaber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTotalDebe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TituloModulo
        '
        Me.TituloModulo.BackColor = System.Drawing.Color.FromArgb(CType(56, Byte), CType(91, Byte), CType(165, Byte))
        Me.TituloModulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.TituloModulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.TituloModulo.ForeColor = System.Drawing.Color.White
        Me.TituloModulo.Image = CType(resources.GetObject("TituloModulo.Image"), System.Drawing.Image)
        Me.TituloModulo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.TituloModulo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.TituloModulo.Location = New System.Drawing.Point(0, 0)
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(800, 32)
        Me.TituloModulo.TabIndex = 59
        Me.TituloModulo.Text = "Asiento Toma Fisica"
        Me.TituloModulo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GC_Asientos
        '
        Me.GC_Asientos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GC_Asientos.DataSource = Me.DsTomaAsiento1.GridDetalles
        '
        'GC_Asientos.EmbeddedNavigator
        '
        Me.GC_Asientos.EmbeddedNavigator.Name = ""
        Me.GC_Asientos.Location = New System.Drawing.Point(8, 40)
        Me.GC_Asientos.MainView = Me.GridView1
        Me.GC_Asientos.Name = "GC_Asientos"
        Me.GC_Asientos.Size = New System.Drawing.Size(784, 304)
        Me.GC_Asientos.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GC_Asientos.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GC_Asientos.TabIndex = 114
        '
        'DsTomaAsiento1
        '
        Me.DsTomaAsiento1.DataSetName = "DsTomaAsiento"
        Me.DsTomaAsiento1.Locale = New System.Globalization.CultureInfo("es-ES")
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCuenta, Me.colNombre, Me.colDescripcion, Me.colDebe, Me.colHaber})
        Me.GridView1.GroupPanelText = ""
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colCuenta
        '
        Me.colCuenta.Caption = "Cuenta Contable"
        Me.colCuenta.FieldName = "Cuenta"
        Me.colCuenta.Name = "colCuenta"
        Me.colCuenta.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCuenta.VisibleIndex = 0
        Me.colCuenta.Width = 80
        '
        'colNombre
        '
        Me.colNombre.Caption = "Nombre Cuenta"
        Me.colNombre.FieldName = "NombreCuenta"
        Me.colNombre.Name = "colNombre"
        Me.colNombre.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNombre.VisibleIndex = 1
        Me.colNombre.Width = 150
        '
        'colDescripcion
        '
        Me.colDescripcion.Caption = "Descripcion"
        Me.colDescripcion.FieldName = "Descripcion"
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDescripcion.VisibleIndex = 2
        Me.colDescripcion.Width = 200
        '
        'colDebe
        '
        Me.colDebe.Caption = "Debe"
        Me.colDebe.DisplayFormat.FormatString = "�###,##0.00"
        Me.colDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colDebe.FieldName = "Debe"
        Me.colDebe.Name = "colDebe"
        Me.colDebe.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDebe.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colDebe.VisibleIndex = 3
        Me.colDebe.Width = 65
        '
        'colHaber
        '
        Me.colHaber.Caption = "Haber"
        Me.colHaber.DisplayFormat.FormatString = "�###,##0.00"
        Me.colHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colHaber.FieldName = "Haber"
        Me.colHaber.Name = "colHaber"
        Me.colHaber.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colHaber.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colHaber.VisibleIndex = 4
        Me.colHaber.Width = 65
        '
        'AdapterAsientos
        '
        Me.AdapterAsientos.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterAsientos.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterAsientos.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterAsientos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AsientosContables", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumAsiento", "NumAsiento"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("IdNumDoc", "IdNumDoc"), New System.Data.Common.DataColumnMapping("NumDoc", "NumDoc"), New System.Data.Common.DataColumnMapping("Beneficiario", "Beneficiario"), New System.Data.Common.DataColumnMapping("TipoDoc", "TipoDoc"), New System.Data.Common.DataColumnMapping("Accion", "Accion"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("FechaEntrada", "FechaEntrada"), New System.Data.Common.DataColumnMapping("Mayorizado", "Mayorizado"), New System.Data.Common.DataColumnMapping("Periodo", "Periodo"), New System.Data.Common.DataColumnMapping("NumMayorizado", "NumMayorizado"), New System.Data.Common.DataColumnMapping("Modulo", "Modulo"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario"), New System.Data.Common.DataColumnMapping("TotalDebe", "TotalDebe"), New System.Data.Common.DataColumnMapping("TotalHaber", "TotalHaber"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio")})})
        Me.AdapterAsientos.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM AsientosContables WHERE (NumAsiento = @Original_NumAsiento) AND (Acci" & _
        "on = @Original_Accion) AND (Anulado = @Original_Anulado) AND (Beneficiario = @Or" & _
        "iginal_Beneficiario) AND (CodMoneda = @Original_CodMoneda) AND (Fecha = @Origina" & _
        "l_Fecha) AND (FechaEntrada = @Original_FechaEntrada) AND (IdNumDoc = @Original_I" & _
        "dNumDoc) AND (Mayorizado = @Original_Mayorizado) AND (Modulo = @Original_Modulo)" & _
        " AND (NombreUsuario = @Original_NombreUsuario) AND (NumDoc = @Original_NumDoc) A" & _
        "ND (NumMayorizado = @Original_NumMayorizado) AND (Observaciones = @Original_Obse" & _
        "rvaciones) AND (Periodo = @Original_Periodo) AND (TipoCambio = @Original_TipoCam" & _
        "bio) AND (TipoDoc = @Original_TipoDoc) AND (TotalDebe = @Original_TotalDebe) AND" & _
        " (TotalHaber = @Original_TotalHaber)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Accion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Accion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Beneficiario", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Beneficiario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdNumDoc", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdNumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Mayorizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Modulo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Modulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Periodo", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periodo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDoc", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalDebe", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalDebe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalHaber", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalHaber", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=Contabilidad"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO AsientosContables(NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, " & _
        "TipoDoc, Accion, Anulado, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modu" & _
        "lo, Observaciones, NombreUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio) " & _
        "VALUES (@NumAsiento, @Fecha, @IdNumDoc, @NumDoc, @Beneficiario, @TipoDoc, @Accio" & _
        "n, @Anulado, @FechaEntrada, @Mayorizado, @Periodo, @NumMayorizado, @Modulo, @Obs" & _
        "ervaciones, @NombreUsuario, @TotalDebe, @TotalHaber, @CodMoneda, @TipoCambio); S" & _
        "ELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDoc, Accion, Anulad" & _
        "o, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Observaciones, Nomb" & _
        "reUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM AsientosContables W" & _
        "HERE (NumAsiento = @NumAsiento)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 4, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdNumDoc", System.Data.SqlDbType.BigInt, 8, "IdNumDoc"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumDoc", System.Data.SqlDbType.VarChar, 50, "NumDoc"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Beneficiario", System.Data.SqlDbType.VarChar, 250, "Beneficiario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDoc", System.Data.SqlDbType.Int, 4, "TipoDoc"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 50, "Accion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 4, "FechaEntrada"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Mayorizado", System.Data.SqlDbType.Bit, 1, "Mayorizado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Periodo", System.Data.SqlDbType.VarChar, 8, "Periodo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Modulo", System.Data.SqlDbType.VarChar, 50, "Modulo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalDebe", System.Data.SqlDbType.Float, 8, "TotalDebe"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalHaber", System.Data.SqlDbType.Float, 8, "TotalHaber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDoc, Accion, Anulad" & _
        "o, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Observaciones, Nomb" & _
        "reUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM AsientosContables"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE AsientosContables SET NumAsiento = @NumAsiento, Fecha = @Fecha, IdNumDoc =" & _
        " @IdNumDoc, NumDoc = @NumDoc, Beneficiario = @Beneficiario, TipoDoc = @TipoDoc, " & _
        "Accion = @Accion, Anulado = @Anulado, FechaEntrada = @FechaEntrada, Mayorizado =" & _
        " @Mayorizado, Periodo = @Periodo, NumMayorizado = @NumMayorizado, Modulo = @Modu" & _
        "lo, Observaciones = @Observaciones, NombreUsuario = @NombreUsuario, TotalDebe = " & _
        "@TotalDebe, TotalHaber = @TotalHaber, CodMoneda = @CodMoneda, TipoCambio = @Tipo" & _
        "Cambio WHERE (NumAsiento = @Original_NumAsiento) AND (Accion = @Original_Accion)" & _
        " AND (Anulado = @Original_Anulado) AND (Beneficiario = @Original_Beneficiario) A" & _
        "ND (CodMoneda = @Original_CodMoneda) AND (Fecha = @Original_Fecha) AND (FechaEnt" & _
        "rada = @Original_FechaEntrada) AND (IdNumDoc = @Original_IdNumDoc) AND (Mayoriza" & _
        "do = @Original_Mayorizado) AND (Modulo = @Original_Modulo) AND (NombreUsuario = " & _
        "@Original_NombreUsuario) AND (NumDoc = @Original_NumDoc) AND (NumMayorizado = @O" & _
        "riginal_NumMayorizado) AND (Observaciones = @Original_Observaciones) AND (Period" & _
        "o = @Original_Periodo) AND (TipoCambio = @Original_TipoCambio) AND (TipoDoc = @O" & _
        "riginal_TipoDoc) AND (TotalDebe = @Original_TotalDebe) AND (TotalHaber = @Origin" & _
        "al_TotalHaber); SELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDo" & _
        "c, Accion, Anulado, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Ob" & _
        "servaciones, NombreUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM As" & _
        "ientosContables WHERE (NumAsiento = @NumAsiento)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 4, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdNumDoc", System.Data.SqlDbType.BigInt, 8, "IdNumDoc"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumDoc", System.Data.SqlDbType.VarChar, 50, "NumDoc"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Beneficiario", System.Data.SqlDbType.VarChar, 250, "Beneficiario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDoc", System.Data.SqlDbType.Int, 4, "TipoDoc"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 50, "Accion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 4, "FechaEntrada"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Mayorizado", System.Data.SqlDbType.Bit, 1, "Mayorizado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Periodo", System.Data.SqlDbType.VarChar, 8, "Periodo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Modulo", System.Data.SqlDbType.VarChar, 50, "Modulo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalDebe", System.Data.SqlDbType.Float, 8, "TotalDebe"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalHaber", System.Data.SqlDbType.Float, 8, "TotalHaber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Accion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Accion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Beneficiario", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Beneficiario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdNumDoc", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdNumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Mayorizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Modulo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Modulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Periodo", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periodo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDoc", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalDebe", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalDebe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalHaber", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalHaber", System.Data.DataRowVersion.Original, Nothing))
        '
        'AdapterDetallesAsientos
        '
        Me.AdapterDetallesAsientos.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterDetallesAsientos.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterDetallesAsientos.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterDetallesAsientos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "DetallesAsientosContable", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ID_Detalle", "ID_Detalle"), New System.Data.Common.DataColumnMapping("NumAsiento", "NumAsiento"), New System.Data.Common.DataColumnMapping("Cuenta", "Cuenta"), New System.Data.Common.DataColumnMapping("NombreCuenta", "NombreCuenta"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Debe", "Debe"), New System.Data.Common.DataColumnMapping("Haber", "Haber"), New System.Data.Common.DataColumnMapping("DescripcionAsiento", "DescripcionAsiento"), New System.Data.Common.DataColumnMapping("Tipocambio", "Tipocambio")})})
        Me.AdapterDetallesAsientos.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM DetallesAsientosContable WHERE (ID_Detalle = @Original_ID_Detalle) AN" & _
        "D (Cuenta = @Original_Cuenta) AND (Debe = @Original_Debe) AND (DescripcionAsient" & _
        "o = @Original_DescripcionAsiento) AND (Haber = @Original_Haber) AND (Monto = @Or" & _
        "iginal_Monto) AND (NombreCuenta = @Original_NombreCuenta) AND (NumAsiento = @Ori" & _
        "ginal_NumAsiento) AND (Tipocambio = @Original_Tipocambio OR @Original_Tipocambio" & _
        " IS NULL AND Tipocambio IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID_Detalle", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID_Detalle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Debe", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Debe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Haber", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Haber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipocambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipocambio", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO DetallesAsientosContable(NumAsiento, Cuenta, NombreCuenta, Monto, Deb" & _
        "e, Haber, DescripcionAsiento, Tipocambio) VALUES (@NumAsiento, @Cuenta, @NombreC" & _
        "uenta, @Monto, @Debe, @Haber, @DescripcionAsiento, @Tipocambio); SELECT ID_Detal" & _
        "le, NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Ti" & _
        "pocambio FROM DetallesAsientosContable WHERE (ID_Detalle = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta", System.Data.SqlDbType.VarChar, 255, "Cuenta"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Debe", System.Data.SqlDbType.Bit, 1, "Debe"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Haber", System.Data.SqlDbType.Bit, 1, "Haber"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, "DescripcionAsiento"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipocambio", System.Data.SqlDbType.Float, 8, "Tipocambio"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT ID_Detalle, NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, Descripc" & _
        "ionAsiento, Tipocambio FROM DetallesAsientosContable"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE DetallesAsientosContable SET NumAsiento = @NumAsiento, Cuenta = @Cuenta, N" & _
        "ombreCuenta = @NombreCuenta, Monto = @Monto, Debe = @Debe, Haber = @Haber, Descr" & _
        "ipcionAsiento = @DescripcionAsiento, Tipocambio = @Tipocambio WHERE (ID_Detalle " & _
        "= @Original_ID_Detalle) AND (Cuenta = @Original_Cuenta) AND (Debe = @Original_De" & _
        "be) AND (DescripcionAsiento = @Original_DescripcionAsiento) AND (Haber = @Origin" & _
        "al_Haber) AND (Monto = @Original_Monto) AND (NombreCuenta = @Original_NombreCuen" & _
        "ta) AND (NumAsiento = @Original_NumAsiento) AND (Tipocambio = @Original_Tipocamb" & _
        "io OR @Original_Tipocambio IS NULL AND Tipocambio IS NULL); SELECT ID_Detalle, N" & _
        "umAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Tipocam" & _
        "bio FROM DetallesAsientosContable WHERE (ID_Detalle = @ID_Detalle)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta", System.Data.SqlDbType.VarChar, 255, "Cuenta"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Debe", System.Data.SqlDbType.Bit, 1, "Debe"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Haber", System.Data.SqlDbType.Bit, 1, "Haber"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, "DescripcionAsiento"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipocambio", System.Data.SqlDbType.Float, 8, "Tipocambio"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID_Detalle", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID_Detalle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Debe", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Debe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Haber", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Haber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipocambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipocambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID_Detalle", System.Data.SqlDbType.BigInt, 8, "ID_Detalle"))
        '
        'AdapterTomaFisica
        '
        Me.AdapterTomaFisica.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterTomaFisica.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TomaFisica", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("IdBodega", "IdBodega"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Periodo", "Periodo"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Inicial", "Inicial"), New System.Data.Common.DataColumnMapping("Ajuste", "Ajuste"), New System.Data.Common.DataColumnMapping("Num_Ajuste", "Num_Ajuste"), New System.Data.Common.DataColumnMapping("Asiento", "Asiento"), New System.Data.Common.DataColumnMapping("Num_Asiento", "Num_Asiento"), New System.Data.Common.DataColumnMapping("Ordenado", "Ordenado"), New System.Data.Common.DataColumnMapping("Agrupado", "Agrupado")})})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT Codigo, IdBodega, Fecha, Periodo, Usuario, Anulado, Inicial, Ajuste, Num_A" & _
        "juste, Asiento, Num_Asiento, Ordenado, Agrupado FROM TomaFisica"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection2
        '
        'SqlConnection2
        '
        Me.SqlConnection2.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=Proveeduria"
        '
        'AdapterTomaFisica_Detalle
        '
        Me.AdapterTomaFisica_Detalle.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterTomaFisica_Detalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TomaFisica_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("IdTomaFisica", "IdTomaFisica"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Barras", "Barras"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("SaldoInicial", "SaldoInicial"), New System.Data.Common.DataColumnMapping("Compras", "Compras"), New System.Data.Common.DataColumnMapping("Requisiciones", "Requisiciones"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("TomaFisica", "TomaFisica"), New System.Data.Common.DataColumnMapping("CostoPromedio", "CostoPromedio"), New System.Data.Common.DataColumnMapping("Familia", "Familia"), New System.Data.Common.DataColumnMapping("Ubicacion", "Ubicacion")})})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Id, IdTomaFisica, Codigo, Barras, Descripcion, SaldoInicial, Compras, Requ" & _
        "isiciones, Existencia, TomaFisica, CostoPromedio, Familia, Ubicacion FROM TomaFi" & _
        "sica_Detalle"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection2
        '
        'BGuardar
        '
        Me.BGuardar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.BGuardar.Location = New System.Drawing.Point(304, 376)
        Me.BGuardar.Name = "BGuardar"
        Me.BGuardar.Size = New System.Drawing.Size(104, 23)
        Me.BGuardar.TabIndex = 115
        Me.BGuardar.Text = "Guardar"
        '
        'BCancelar
        '
        Me.BCancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.BCancelar.Location = New System.Drawing.Point(432, 376)
        Me.BCancelar.Name = "BCancelar"
        Me.BCancelar.Size = New System.Drawing.Size(104, 23)
        Me.BCancelar.TabIndex = 116
        Me.BCancelar.Text = "Cancelar"
        '
        'PanelAgregar
        '
        Me.PanelAgregar.BackColor = System.Drawing.Color.Silver
        Me.PanelAgregar.Controls.Add(Me.GroupBox2)
        Me.PanelAgregar.Controls.Add(Me.LAgregar)
        Me.PanelAgregar.Location = New System.Drawing.Point(-500, 56)
        Me.PanelAgregar.Name = "PanelAgregar"
        Me.PanelAgregar.Size = New System.Drawing.Size(456, 219)
        Me.PanelAgregar.TabIndex = 203
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.txtDescripcionCuenta)
        Me.GroupBox2.Controls.Add(Me.txtCuentaContable)
        Me.GroupBox2.Controls.Add(Me.BNuevo)
        Me.GroupBox2.Controls.Add(Me.TxtDetalle)
        Me.GroupBox2.Controls.Add(Me.BotonCerrar)
        Me.GroupBox2.Controls.Add(Me.GridAgregar)
        Me.GroupBox2.Controls.Add(Me.ButtonAgregarDetalle)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.txtMonto)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(4, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(436, 200)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'txtDescripcionCuenta
        '
        Me.txtDescripcionCuenta.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsTomaAsiento1, "GridAgregar.NombreCC"))
        Me.txtDescripcionCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcionCuenta.Location = New System.Drawing.Point(8, 40)
        Me.txtDescripcionCuenta.Name = "txtDescripcionCuenta"
        Me.txtDescripcionCuenta.ReadOnly = True
        Me.txtDescripcionCuenta.Size = New System.Drawing.Size(304, 20)
        Me.txtDescripcionCuenta.TabIndex = 206
        Me.txtDescripcionCuenta.Text = ""
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsTomaAsiento1, "GridAgregar.Cuenta"))
        Me.txtCuentaContable.Enabled = False
        Me.txtCuentaContable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuentaContable.Location = New System.Drawing.Point(120, 16)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Size = New System.Drawing.Size(192, 20)
        Me.txtCuentaContable.TabIndex = 205
        Me.txtCuentaContable.Text = ""
        '
        'BNuevo
        '
        Me.BNuevo.Location = New System.Drawing.Point(112, 72)
        Me.BNuevo.Name = "BNuevo"
        Me.BNuevo.Size = New System.Drawing.Size(72, 20)
        Me.BNuevo.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        Me.BNuevo.TabIndex = 204
        Me.BNuevo.Text = "Nuevo"
        '
        'TxtDetalle
        '
        Me.TxtDetalle.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtDetalle.Enabled = False
        Me.TxtDetalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtDetalle.ForeColor = System.Drawing.Color.Blue
        Me.TxtDetalle.Location = New System.Drawing.Point(328, 56)
        Me.TxtDetalle.Name = "TxtDetalle"
        Me.TxtDetalle.ReadOnly = True
        Me.TxtDetalle.Size = New System.Drawing.Size(88, 13)
        Me.TxtDetalle.TabIndex = 203
        Me.TxtDetalle.Text = "0.00"
        Me.TxtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BotonCerrar
        '
        Me.BotonCerrar.Location = New System.Drawing.Point(272, 72)
        Me.BotonCerrar.Name = "BotonCerrar"
        Me.BotonCerrar.Size = New System.Drawing.Size(72, 20)
        Me.BotonCerrar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        Me.BotonCerrar.TabIndex = 202
        Me.BotonCerrar.Text = "Cerrar"
        '
        'GridAgregar
        '
        Me.GridAgregar.DataSource = Me.DsTomaAsiento1.GridAgregar
        '
        'GridAgregar.EmbeddedNavigator
        '
        Me.GridAgregar.EmbeddedNavigator.Name = ""
        Me.GridAgregar.Location = New System.Drawing.Point(8, 104)
        Me.GridAgregar.MainView = Me.GridView2
        Me.GridAgregar.Name = "GridAgregar"
        Me.GridAgregar.Size = New System.Drawing.Size(416, 88)
        Me.GridAgregar.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridAgregar.TabIndex = 201
        Me.GridAgregar.Text = "GridControl1"
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCuentaC, Me.colDescripcionC, Me.colMonto})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colCuentaC
        '
        Me.colCuentaC.Caption = "Cuenta"
        Me.colCuentaC.FieldName = "Cuenta"
        Me.colCuentaC.Name = "colCuentaC"
        Me.colCuentaC.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCuentaC.VisibleIndex = 0
        Me.colCuentaC.Width = 100
        '
        'colDescripcionC
        '
        Me.colDescripcionC.Caption = "Nombre Cuenta"
        Me.colDescripcionC.FieldName = "NombreCC"
        Me.colDescripcionC.Name = "colDescripcionC"
        Me.colDescripcionC.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDescripcionC.VisibleIndex = 1
        Me.colDescripcionC.Width = 200
        '
        'colMonto
        '
        Me.colMonto.Caption = "Monto"
        Me.colMonto.DisplayFormat.FormatString = "#,#0.00"
        Me.colMonto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colMonto.FieldName = "Monto"
        Me.colMonto.Name = "colMonto"
        Me.colMonto.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMonto.VisibleIndex = 2
        Me.colMonto.Width = 80
        '
        'ButtonAgregarDetalle
        '
        Me.ButtonAgregarDetalle.Enabled = False
        Me.ButtonAgregarDetalle.Location = New System.Drawing.Point(192, 72)
        Me.ButtonAgregarDetalle.Name = "ButtonAgregarDetalle"
        Me.ButtonAgregarDetalle.Size = New System.Drawing.Size(72, 20)
        Me.ButtonAgregarDetalle.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        Me.ButtonAgregarDetalle.TabIndex = 200
        Me.ButtonAgregarDetalle.Text = "Agregar"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(320, 16)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(104, 13)
        Me.Label20.TabIndex = 59
        Me.Label20.Text = "Monto"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label21.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(8, 19)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(104, 15)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Cuenta Contable :"
        '
        'txtMonto
        '
        Me.txtMonto.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsTomaAsiento1, "GridAgregar.Monto"))
        Me.txtMonto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMonto.Location = New System.Drawing.Point(320, 32)
        Me.txtMonto.Name = "txtMonto"
        '
        'txtMonto.Properties
        '
        Me.txtMonto.Properties.DisplayFormat.FormatString = "�###,##0.00"
        Me.txtMonto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtMonto.Properties.EditFormat.FormatString = "###,##0.00"
        Me.txtMonto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtMonto.Properties.Enabled = False
        Me.txtMonto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtMonto.Size = New System.Drawing.Size(104, 21)
        Me.txtMonto.TabIndex = 5
        '
        'LAgregar
        '
        Me.LAgregar.BackColor = System.Drawing.Color.RoyalBlue
        Me.LAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LAgregar.ForeColor = System.Drawing.Color.White
        Me.LAgregar.Location = New System.Drawing.Point(88, 1)
        Me.LAgregar.Name = "LAgregar"
        Me.LAgregar.Size = New System.Drawing.Size(269, 13)
        Me.LAgregar.TabIndex = 0
        Me.LAgregar.Text = "Familia"
        Me.LAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTotalHaber
        '
        Me.txtTotalHaber.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalHaber.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalHaber.Location = New System.Drawing.Point(688, 352)
        Me.txtTotalHaber.Name = "txtTotalHaber"
        '
        'txtTotalHaber.Properties
        '
        Me.txtTotalHaber.Properties.DisplayFormat.FormatString = "�###,##0.00"
        Me.txtTotalHaber.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalHaber.Properties.EditFormat.FormatString = "###,##0.00"
        Me.txtTotalHaber.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalHaber.Properties.Enabled = False
        Me.txtTotalHaber.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtTotalHaber.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlDark)
        Me.txtTotalHaber.Size = New System.Drawing.Size(96, 21)
        Me.txtTotalHaber.TabIndex = 204
        '
        'TxtTotalDebe
        '
        Me.TxtTotalDebe.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtTotalDebe.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.TxtTotalDebe.Location = New System.Drawing.Point(584, 352)
        Me.TxtTotalDebe.Name = "TxtTotalDebe"
        '
        'TxtTotalDebe.Properties
        '
        Me.TxtTotalDebe.Properties.DisplayFormat.FormatString = "�###,##0.00"
        Me.TxtTotalDebe.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtTotalDebe.Properties.EditFormat.FormatString = "###,##0.00"
        Me.TxtTotalDebe.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtTotalDebe.Properties.Enabled = False
        Me.TxtTotalDebe.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.TxtTotalDebe.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlDark)
        Me.TxtTotalDebe.Size = New System.Drawing.Size(96, 21)
        Me.TxtTotalDebe.TabIndex = 205
        '
        'Asiento_Toma
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(800, 406)
        Me.Controls.Add(Me.TxtTotalDebe)
        Me.Controls.Add(Me.txtTotalHaber)
        Me.Controls.Add(Me.PanelAgregar)
        Me.Controls.Add(Me.BCancelar)
        Me.Controls.Add(Me.BGuardar)
        Me.Controls.Add(Me.GC_Asientos)
        Me.Controls.Add(Me.TituloModulo)
        Me.Name = "Asiento_Toma"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asiento de Toma Fisica"
        CType(Me.GC_Asientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsTomaAsiento1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelAgregar.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.GridAgregar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalHaber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTotalDebe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Variables"
    Public IdToma As Integer
    Public Registro, Produccion, EditaDetalle, Edito As Boolean
    Public Usuario, CuentaBodega, DescripcionCuentaBodega As String
    Dim TotalAgregar As Double = 0
    Dim Pos As Integer
#End Region

#Region "Load"
    Private Sub Asiento_Toma_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Contabilidad", "Conexion")
        SqlConnection2.ConnectionString = GetSetting("SeeSOFT", "SeePos", "Conexion")
        CargarToma()
        CrearAsiento()
    End Sub

    Private Sub ValoresDefecto()
        'VALORES POR DEFECTO PARA LA TABLA ASIENTOS
        DsTomaAsiento1.AsientosContables.FechaColumn.DefaultValue = Now.Date
        DsTomaAsiento1.AsientosContables.NumDocColumn.DefaultValue = "0"
        DsTomaAsiento1.AsientosContables.IdNumDocColumn.DefaultValue = 0
        DsTomaAsiento1.AsientosContables.BeneficiarioColumn.DefaultValue = ""
        DsTomaAsiento1.AsientosContables.TipoDocColumn.DefaultValue = 22
        DsTomaAsiento1.AsientosContables.AccionColumn.DefaultValue = "AUT"
        DsTomaAsiento1.AsientosContables.AnuladoColumn.DefaultValue = 0
        DsTomaAsiento1.AsientosContables.FechaEntradaColumn.DefaultValue = Now.Date
        DsTomaAsiento1.AsientosContables.MayorizadoColumn.DefaultValue = 0
        DsTomaAsiento1.AsientosContables.PeriodoColumn.DefaultValue = Now.Month & "/" & Now.Year
        DsTomaAsiento1.AsientosContables.NumMayorizadoColumn.DefaultValue = 0
        DsTomaAsiento1.AsientosContables.ModuloColumn.DefaultValue = "Toma Fisica"
        DsTomaAsiento1.AsientosContables.ObservacionesColumn.DefaultValue = ""
        DsTomaAsiento1.AsientosContables.NombreUsuarioColumn.DefaultValue = ""
        DsTomaAsiento1.AsientosContables.TotalDebeColumn.DefaultValue = 0
        DsTomaAsiento1.AsientosContables.TotalHaberColumn.DefaultValue = 0
        DsTomaAsiento1.AsientosContables.CodMonedaColumn.DefaultValue = 1
        DsTomaAsiento1.AsientosContables.TipoCambioColumn.DefaultValue = 1

        'VALORES POR DEFECTO PARA LA TABLA DETALLES ASIENTOS
        DsTomaAsiento1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrement = True
        DsTomaAsiento1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrementSeed = -1
        DsTomaAsiento1.DetallesAsientosContable.ID_DetalleColumn.AutoIncrementStep = -1
        DsTomaAsiento1.DetallesAsientosContable.NumAsientoColumn.DefaultValue = ""
        DsTomaAsiento1.DetallesAsientosContable.DescripcionAsientoColumn.DefaultValue = ""
        DsTomaAsiento1.DetallesAsientosContable.CuentaColumn.DefaultValue = ""
        DsTomaAsiento1.DetallesAsientosContable.NombreCuentaColumn.DefaultValue = ""
        DsTomaAsiento1.DetallesAsientosContable.MontoColumn.DefaultValue = 0
        DsTomaAsiento1.DetallesAsientosContable.DebeColumn.DefaultValue = 0
        DsTomaAsiento1.DetallesAsientosContable.HaberColumn.DefaultValue = 0
        DsTomaAsiento1.DetallesAsientosContable.TipocambioColumn.DefaultValue = 1

        'VALORES POR DEFECTO PARA LA TABLA GRID DETALLES
        DsTomaAsiento1.GridDetalles.CuentaColumn.DefaultValue = ""
        DsTomaAsiento1.GridDetalles.NombreCuentaColumn.DefaultValue = ""
        DsTomaAsiento1.GridDetalles.DescripcionColumn.DefaultValue = ""
        DsTomaAsiento1.GridDetalles.DebeColumn.DefaultValue = 0.0
        DsTomaAsiento1.GridDetalles.HaberColumn.DefaultValue = 0.0
        DsTomaAsiento1.GridDetalles.FamiliaColumn.DefaultValue = ""
        DsTomaAsiento1.GridDetalles.SistemaColumn.DefaultValue = False

        'VALORES POR DEFECTO PARA LA TABLA GRID AGREGAR
        DsTomaAsiento1.GridAgregar.CuentaColumn.DefaultValue = ""
        DsTomaAsiento1.GridAgregar.NombreCCColumn.DefaultValue = ""
        DsTomaAsiento1.GridAgregar.MontoColumn.DefaultValue = 0.0
        DsTomaAsiento1.GridAgregar.FamiliaColumn.DefaultValue = ""
    End Sub

    Private Sub CargarToma()
        Dim Fx As New cFunciones
        Dim strConexion As String = SqlConnection2.ConnectionString

        Try
            '-----------------------------------------------------------------------------------
            'CARGAR LA TOMA FISICA
            Fx.Cargar_Tabla_Generico(Me.AdapterTomaFisica, "SELECT * FROM TomaFisica WHERE Codigo = " & IdToma, strConexion)
            Me.AdapterTomaFisica.Fill(Me.DsTomaAsiento1, "TomaFisica")
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'CARGAR LOS DETALLES DE LA TOMA FISICA
            Fx.Cargar_Tabla_Generico(Me.AdapterTomaFisica_Detalle, "SELECT * FROM TomaFisica_Detalle WHERE IdTomaFisica = " & IdToma, strConexion)
            Me.AdapterTomaFisica_Detalle.Fill(Me.DsTomaAsiento1, "TomaFisica_Detalle")
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "CrearAsiento"
    Public Sub CrearAsiento()
        Dim DrCuenta As System.Data.DataRow
        Dim Cuentas(DsTomaAsiento1.TomaFisica_Detalle.Count - 1) As String
        Dim Encontrado As Boolean = False
        Dim C As Integer = 0

        Try
            DsTomaAsiento1.DetallesAsientosContable.Clear()
            DsTomaAsiento1.AsientosContables.Clear()
            DsTomaAsiento1.GridDetalles.Clear()
            DsTomaAsiento1.GridAgregar.Clear()

            If DsTomaAsiento1.TomaFisica_Detalle.Count > 0 Then
                For i As Integer = 0 To DsTomaAsiento1.TomaFisica_Detalle.Count - 1
                    DrCuenta = DsTomaAsiento1.TomaFisica_Detalle(i)

                    '----------------------------------------------------------------------------
                    'RECORRE TODOS LOS DETALLES DE LA TOMA FISICA
                    For b As Integer = 0 To DsTomaAsiento1.TomaFisica_Detalle.Count - 1
                        If Cuentas(b) = DrCuenta("Familia") Then
                            Encontrado = True
                        End If
                    Next
                    '----------------------------------------------------------------------------

                    '----------------------------------------------------------------------------
                    If Encontrado = False Then  'SI TODAVIA NO SE REALIZADO EL ASIENTO PARA LA FAMILIA
                        Cuentas(C) = DrCuenta("Familia")
                        C += 1
                        BuscaMontoFamilia(DrCuenta("Familia")) 'CREA EL DETALLE EN EL GRID DE LA FAMILIA
                        '----------------------------------------------------------------------------
                    Else    'SI YA HA SIDO TOMADO EN CUENTA
                        Encontrado = False
                    End If
                    '----------------------------------------------------------------------------
                Next
                ActualizaTotales()
                '-----------------------------------------------------------------------------------
                'POSICIONA EL BINDING
                BindingContext(DsTomaAsiento1, "GridDetalles").Position = 0
                '-----------------------------------------------------------------------------------
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub BuscaMontoFamilia(ByVal Familia As String)
        Dim DrCuentas() As System.Data.DataRow
        Dim Monto As Double

        Try
            If DsTomaAsiento1.TomaFisica_Detalle.Count > 0 Then
                DrCuentas = DsTomaAsiento1.TomaFisica_Detalle.Select("Familia = '" & Familia & "'")

                If DrCuentas.Length <> 0 Then 'SI EXISTE
                    '----------------------------------------------------------------------------
                    For i As Integer = 0 To DrCuentas.Length - 1
                        '-----------------------------------------------------------------------------------
                        'VERIFICA EL TIPO DE BODEGA PARA REALIZAR EL CALCULO DEL MONTO
                        If Produccion Then
                            Monto += (((DrCuentas(i)("SaldoInicial") + DrCuentas(i)("Compras") - DrCuentas(i)("Requisiciones")) - DrCuentas(i)("TomaFisica")) * DrCuentas(i)("CostoPromedio"))
                        Else
                            Monto += ((DrCuentas(i)("TomaFisica") - DrCuentas(i)("Existencia")) * DrCuentas(i)("CostoPromedio"))
                        End If
                    Next
                    '----------------------------------------------------------------------------

                    If Monto > 0 Then
                        '----------------------------------------------------------------------------
                        'CREA EL DETALLE PARA LA CUENTA CONTABLE DE LA FAMILIA
                        GuardaGridDetalle(Familia, Monto, 0, CuentaBodega, DescripcionCuentaBodega, True)
                        '----------------------------------------------------------------------------
                    Else
                        '----------------------------------------------------------------------------
                        'CREA EL DETALLE PARA LA CUENTA CONTABLE DE LA FAMILIA
                        GuardaGridDetalle(Familia, 0, Math.Abs(Monto), CuentaBodega, DescripcionCuentaBodega, True)
                        '----------------------------------------------------------------------------
                    End If
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub


    Public Sub GuardaGridDetalle(ByVal Familia As String, ByVal Debe As Double, ByVal Haber As Double, ByVal Cuenta As String, ByVal NombreCuenta As String, ByVal Sistema As Boolean)
        If Debe > 0 Or Haber > 0 Then 'GUARDA LOS DETALLES DEL GRID
            BindingContext(DsTomaAsiento1, "GridDetalles").EndCurrentEdit()
            BindingContext(DsTomaAsiento1, "GridDetalles").AddNew()
            BindingContext(DsTomaAsiento1, "GridDetalles").Current("Cuenta") = Cuenta
            BindingContext(DsTomaAsiento1, "GridDetalles").Current("NombreCuenta") = NombreCuenta
            BindingContext(DsTomaAsiento1, "GridDetalles").Current("Descripcion") = "Asiento de Toma Fisica # " & BindingContext(DsTomaAsiento1, "TomaFisica").Current("Codigo") & " - Familia: " & Familia
            BindingContext(DsTomaAsiento1, "GridDetalles").Current("Debe") = Math.Round(Debe, 2)
            BindingContext(DsTomaAsiento1, "GridDetalles").Current("Haber") = Math.Round(Haber, 2)
            BindingContext(DsTomaAsiento1, "GridDetalles").Current("Familia") = Familia
            BindingContext(DsTomaAsiento1, "GridDetalles").Current("Sistema") = Sistema
            BindingContext(DsTomaAsiento1, "GridDetalles").EndCurrentEdit()
        End If
    End Sub
#End Region

#Region "GuardaAsiento"
    Public Sub GuardaAsiento()
        Dim Fx As New cFunciones
        Dim Fecha As DateTime = BindingContext(DsTomaAsiento1, "TomaFisica").Current("Periodo")

        Try
            'BindingContext(DsTomaAsiento1, "AsientosContables").CancelCurrentEdit()
            'BindingContext(DsTomaAsiento1, "AsientosContables").AddNew()
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("NumAsiento") = Fx.BuscaNumeroAsiento("INV-" & Format(Fecha, "MM") & Format(Fecha, "yy") & "-")
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Fecha") = Fecha
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("IdNumDoc") = BindingContext(DsTomaAsiento1, "TomaFisica").Current("Codigo")
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("NumDoc") = BindingContext(DsTomaAsiento1, "TomaFisica").Current("Codigo")
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Beneficiario") = ""
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("TipoDoc") = 22
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Accion") = "AUT"
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Anulado") = 0
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Mayorizado") = 0
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("FechaEntrada") = Now.Date
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Periodo") = Fx.BuscaPeriodo(Fecha)
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("NumMayorizado") = 0
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Modulo") = "Toma Fisica"
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Observaciones") = "Asiento de Toma Fisica # " & BindingContext(DsTomaAsiento1, "TomaFisica").Current("Codigo")
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("NombreUsuario") = Usuario
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("TotalDebe") = CDbl(TxtTotalDebe.EditValue)
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("TotalHaber") = CDbl(txtTotalHaber.EditValue)
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("CodMoneda") = 1
            'BindingContext(DsTomaAsiento1, "AsientosContables").Current("TipoCambio") = Fx.TipoCambio(Fecha)
            'BindingContext(DsTomaAsiento1, "AsientosContables").EndCurrentEdit()

            '-----------------------------------------------------------------------------------
            'CREA LOS DETALLES DEL ASIENTO
            AsientoDetalle()
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VERIFICA SI CREO EL ASIENTO CON DATOS
            If BindingContext(DsTomaAsiento1, "AsientosContables").Current("TotalDebe") <= 0 Then
                BindingContext(DsTomaAsiento1, "AsientosContables").CancelCurrentEdit()
                MsgBox("No se creo el asiento contable porque no se cambiaron las existencias!!!", MsgBoxStyle.Information)
            Else
                '-----------------------------------------------------------------------------------
                'SE GUARDA EL ASIENTO CONTABLE
                BindingContext(DsTomaAsiento1, "AsientosContables").EndCurrentEdit()
                If TransAsiento() Then
                    MsgBox("El asiento contable para la Toma Fisica se guardo Satisfactoriamente", MsgBoxStyle.Information)
                    Registro = True
                    Me.Close()
                Else
                    MsgBox("Error al Guardar el Asiento Contable!!", MsgBoxStyle.Critical)
                End If
                '-----------------------------------------------------------------------------------
            End If
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub

    Public Sub GuardaAsientoDetalle(ByVal Monto As Double, ByVal Debe As Boolean, ByVal Haber As Boolean, ByVal Cuenta As String, ByVal NombreCuenta As String, ByVal Observacion As String)
        If Monto > 0 Then   'GUARDA LOS DETALLES DEL ASIENTO CONTABLE
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").AddNew()
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("NumAsiento") = BindingContext(DsTomaAsiento1, "AsientosContables").Current("NumAsiento")
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("DescripcionAsiento") = Observacion 'BindingContext(DsTomaAsiento1, "AsientosContables").Current("Observaciones") & " Familia "
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Cuenta") = Cuenta
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("NombreCuenta") = NombreCuenta
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Monto") = Monto
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Debe") = Debe
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Haber") = Haber
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
        End If
    End Sub


    Public Sub AsientoDetalle()
        Try
            If DsTomaAsiento1.GridDetalles.Count > 0 Then
                '----------------------------------------------------------------------------
                'RECORRE TODOS LOS DETALLES DEL GRID PARA EL DEBE
                For i As Integer = 0 To DsTomaAsiento1.GridDetalles.Count - 1
                    If DsTomaAsiento1.GridDetalles(i).Debe > 0 Then
                        '-----------------------------------------------------------------------------------
                        'GUARDA EL DETALLE DEL ASIENTO CONTABLE PARA EL DEBE
                        GuardaAsientoDetalle(DsTomaAsiento1.GridDetalles(i).Debe, True, False, DsTomaAsiento1.GridDetalles(i).Cuenta, DsTomaAsiento1.GridDetalles(i).NombreCuenta, DsTomaAsiento1.GridDetalles(i).Descripcion)
                        '-----------------------------------------------------------------------------------
                    End If
                Next
                '-----------------------------------------------------------------------------------

                '----------------------------------------------------------------------------
                'RECORRE TODOS LOS DETALLES DEL GRID PARA EL HABER
                For i As Integer = 0 To DsTomaAsiento1.GridDetalles.Count - 1
                    If DsTomaAsiento1.GridDetalles(i).Haber > 0 Then
                        '-----------------------------------------------------------------------------------
                        'GUARDA EL DETALLE DEL ASIENTO CONTABLE PARA EL HABER
                        GuardaAsientoDetalle(DsTomaAsiento1.GridDetalles(i).Haber, False, True, DsTomaAsiento1.GridDetalles(i).Cuenta, DsTomaAsiento1.GridDetalles(i).NombreCuenta, DsTomaAsiento1.GridDetalles(i).Descripcion)
                        '-----------------------------------------------------------------------------------
                    End If
                Next
                '----------------------------------------------------------------------------
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
#End Region

#Region "Registrar"
    Function TransAsiento() As Boolean
        Dim Trans As SqlTransaction     'REALIZA LA TRANSACCION DE LOS ASIENTOS CONTABLES
        Dim Cx As New Conexion

        Try
            If SqlConnection1.State <> SqlConnection1.State.Open Then SqlConnection1.Open()

            Trans = SqlConnection1.BeginTransaction
            BindingContext(DsTomaAsiento1, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
            BindingContext(DsTomaAsiento1, "AsientosContables").EndCurrentEdit()

            AdapterDetallesAsientos.UpdateCommand.Transaction = Trans
            AdapterDetallesAsientos.DeleteCommand.Transaction = Trans
            AdapterDetallesAsientos.InsertCommand.Transaction = Trans

            AdapterAsientos.UpdateCommand.Transaction = Trans
            AdapterAsientos.DeleteCommand.Transaction = Trans
            AdapterAsientos.InsertCommand.Transaction = Trans

            '-----------------------------------------------------------------------------------
            'INICIA LA TRANSACCION....
            AdapterDetallesAsientos.Update(DsTomaAsiento1.DetallesAsientosContable)
            AdapterAsientos.Update(DsTomaAsiento1.AsientosContables)
            '-----------------------------------------------------------------------------------
            Trans.Commit()

            '-----------------------------------------------------------------------------------
            'ACTUALIZA EL AJUSTE DE INVENTARIO
            Cx.UpdateRecords("TomaFisica", "Asiento = 1, Num_Asiento = '" & Me.BindingContext(DsTomaAsiento1, "AsientosContables").Current("NumAsiento") & "'", "Codigo = " & Me.BindingContext(DsTomaAsiento1, "TomaFisica").Current("Codigo"), "Proveeduria")
            '-----------------------------------------------------------------------------------

            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message, MsgBoxStyle.Information)
            Return False
        End Try
    End Function
#End Region

#Region "Controles"
    Private Sub BGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BGuardar.Click
        If ValidarCampos() Then
            '-----------------------------------------------------------------------------------
            'GUARDA EL ASIENTO CONTABLE
            GuardaAsiento()
            '-----------------------------------------------------------------------------------
        End If
    End Sub

    Private Sub BCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCancelar.Click
        Registro = False
        Me.Close()
    End Sub
#End Region

#Region "Funciones"
    Private Sub ActualizaTotales()  'ACTUALIZA LOS TOTALES PARA SABER SI ESTA BALANCEADO
        Me.TxtTotalDebe.EditValue = colDebe.SummaryItem.SummaryValue
        Me.txtTotalHaber.EditValue = colHaber.SummaryItem.SummaryValue
    End Sub

    Private Function ValidarCampos() As Boolean
        If Me.txtTotalHaber.EditValue <> Me.TxtTotalDebe.EditValue Then
            MsgBox("No se puede registrar porque el balance no es correcto", MsgBoxStyle.Information)
            Exit Function
        End If

        If Me.txtTotalHaber.Text = "" Or Me.TxtTotalDebe.Text = "" Then
            MsgBox("No se puede registrar porque el balance no es correcto", MsgBoxStyle.Information)
            Exit Function
        End If
        ValidarCampos = True
    End Function
#End Region

#Region "Agregar Cuentas"

#Region "Botones"
    Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        '-----------------------------------------------------------------------------------
        'POSICIONA EL BINDING
        BindingContext(DsTomaAsiento1, "GridDetalles").Position = e.FocusedRowHandle
        '-----------------------------------------------------------------------------------
    End Sub


    Private Sub GC_Asientos_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GC_Asientos.DoubleClick
        Try
            If BindingContext(DsTomaAsiento1, "GridDetalles").Count > 0 Then
                '-----------------------------------------------------------------------------------
                'VALIDA SI EL DETALLE FUE CREADO POR EL SISTEMA AUTOMATICAMENTE
                If BindingContext(DsTomaAsiento1, "GridDetalles").Current("Sistema") = False Then
                    MsgBox("No puede agregar detalles contra una cuenta no generada por el sistema!!", MsgBoxStyle.Critical, "Datos Incorrectos")
                    Exit Sub
                End If
                '-----------------------------------------------------------------------------------

                '-----------------------------------------------------------------------------------
                'VALIDA QUE EL MONTO DEL DETALLE DEL ASIENTO SEA EL CORRECTO
                If BindingContext(DsTomaAsiento1, "GridDetalles").Current("Debe") <= 0 And BindingContext(DsTomaAsiento1, "GridDetalles").Current("Haber") <= 0 Then
                    MsgBox("El monto del movimiento es incorrecto", MsgBoxStyle.Critical, "Datos Incorrectos")
                    Exit Sub
                End If
                '-----------------------------------------------------------------------------------

                '-----------------------------------------------------------------------------------
                'VERIFICA SI VA AL DEBE O AL HABER
                If BindingContext(DsTomaAsiento1, "GridDetalles").Current("Debe") > 0 Then
                    colMonto.Caption = "Haber"
                Else
                    colMonto.Caption = "Debe"
                End If
                '-----------------------------------------------------------------------------------

                '-----------------------------------------------------------------------------------
                'INICIALIZA LOS DATOS
                DsTomaAsiento1.GridAgregar.Clear()
                TotalAgregar = 0
                Edito = False
                Pos = BindingContext(DsTomaAsiento1, "GridDetalles").Position
                LAgregar.Text = "Familia : " & BindingContext(DsTomaAsiento1, "GridDetalles").Current("Familia")
                TxtDetalle.Text = (BindingContext(DsTomaAsiento1, "GridDetalles").Current("Debe") + BindingContext(DsTomaAsiento1, "GridDetalles").Current("Haber"))
                '-----------------------------------------------------------------------------------
                'CARGA LOS DETALLES YA INCLUIDOS EN CASO DE HABERLOS
                CargaDetalles(BindingContext(DsTomaAsiento1, "GridDetalles").Current("Familia"))
                '-----------------------------------------------------------------------------------
                Panel_Centrar()
                BNuevo.Focus()
                '-----------------------------------------------------------------------------------

            Else
                MsgBox("Debe de existir un detalle del asiento!!", MsgBoxStyle.Critical, "Datos Incorrectos")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub


    Private Sub BNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNuevo.Click
        Try
            If BNuevo.Text = "Nuevo" Then
                '-----------------------------------------------------------------------------------
                'CUANDO SE AGREGA UN NUEVO DETALLE A LA FAMILIA
                Controles(True)
                BNuevo.Text = "Cancelar"
                ButtonAgregarDetalle.Enabled = True
                BindingContext(DsTomaAsiento1, "GridAgregar").EndCurrentEdit()
                BindingContext(DsTomaAsiento1, "GridAgregar").AddNew()
                BindingContext(DsTomaAsiento1, "GridAgregar").Current("Familia") = BindingContext(DsTomaAsiento1, "GridDetalles").Current("Familia")
                txtCuentaContable.Focus()
                '-----------------------------------------------------------------------------------
            Else
                '-----------------------------------------------------------------------------------
                'CUANDO SE CANCELA LA EDICION DE UNO NUEVO
                TxtDetalle.Text = (BindingContext(DsTomaAsiento1, "GridDetalles").Current("Debe") + BindingContext(DsTomaAsiento1, "GridDetalles").Current("Haber"))
                BindingContext(DsTomaAsiento1, "GridAgregar").CancelCurrentEdit()
                Controles(False)
                BNuevo.Text = "Nuevo"
                ButtonAgregarDetalle.Enabled = False
                '-----------------------------------------------------------------------------------
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub


    Private Sub ButtonAgregarDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgregarDetalle.Click
        Try
            '-----------------------------------------------------------------------------------
            'VALIDA QUE EL MONTO A AGREGAR NO MAYOR AL MONTO TOTAL DE LA FAMILIA
            If TotalAgregar > CDbl(TxtDetalle.Text) Or (CDbl(TxtDetalle.Text) < CDbl(txtMonto.EditValue) + TotalAgregar) Then
                MsgBox("El monto es incorrecto, falta por asignar " & (CDbl(TxtDetalle.Text) - TotalAgregar), MsgBoxStyle.Critical, "Favor Revisar el Monto")
                txtMonto.Focus()
                Exit Sub
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'VALIDAD QUE EL MONTO NO SEA 0 � MENOR
            If CDbl(txtMonto.EditValue) <= 0 Then
                MsgBox("El monto no puede ser " & CDbl(txtMonto.EditValue), MsgBoxStyle.Critical, "Favor Revisar el Monto")
                txtMonto.Focus()
                Exit Sub
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'GUARDA EL NUEVO DETALLE DE LA FAMILIA Y DESHABILITA CONTROLES
            TotalAgregar += CDbl(txtMonto.EditValue)
            BindingContext(DsTomaAsiento1, "GridAgregar").EndCurrentEdit()
            TxtDetalle.Text = (BindingContext(DsTomaAsiento1, "GridDetalles").Current("Debe") + BindingContext(DsTomaAsiento1, "GridDetalles").Current("Haber"))
            Controles(False)
            BNuevo.Text = "Nuevo"
            ButtonAgregarDetalle.Enabled = False
            BNuevo.Focus()
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub


    Private Sub BotonCerrar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BotonCerrar.Click
        Try
            '-----------------------------------------------------------------------------------
            'VALIDA QUE EL MONTO A AGREGAR NO MAYOR AL MONTO TOTAL DE LA FAMILIA
            If TotalAgregar < CDbl(TxtDetalle.Text) Then
                MsgBox("El monto es incorrecto, falta por asignar " & (CDbl(TxtDetalle.Text) - TotalAgregar), MsgBoxStyle.Critical, "Favor Revisar el Monto")
                BNuevo.Focus()
                Exit Sub
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'EN CASO DE ESTAR EDITANDO ELIMINA LOS DETALLES PARA VOLVERLOS A CREAR
            If EditaDetalle = True And Edito = True Then
                EliminarDetalleAsiento()
                AgregarDetalles()
            ElseIf EditaDetalle = False Then
                AgregarDetalles()
            End If
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'CUANDO SE CIERRA EL PANEL SE LIMPIAN LOS CONTROLES Y DATOS
            BindingContext(DsTomaAsiento1, "GridAgregar").CancelCurrentEdit()
            DsTomaAsiento1.GridAgregar.Clear()
            Panel_Ocultar()
            Controles(False)
            BNuevo.Text = "Nuevo"
            ButtonAgregarDetalle.Enabled = False
            TotalAgregar = 0
            ActualizaTotales()
            BindingContext(DsTomaAsiento1, "GridDetalles").Position = Pos
            EditaDetalle = False
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub
#End Region

#Region "Funciones"
    Public Sub AgregarDetalles()
        Try
            '-----------------------------------------------------------------------------------
            'AGREGA LOS DETALLES AL ASIENTO CONTABLE    
            For i As Integer = 0 To DsTomaAsiento1.GridAgregar.Count - 1
                If colMonto.Caption = "Debe" Then       'VERIFICA SI VA AL DEBE O AL HABER
                    GuardaGridDetalle(DsTomaAsiento1.GridAgregar(i).Familia, DsTomaAsiento1.GridAgregar(i).Monto, 0, DsTomaAsiento1.GridAgregar(i).Cuenta, DsTomaAsiento1.GridAgregar(i).NombreCC, False)
                Else
                    GuardaGridDetalle(DsTomaAsiento1.GridAgregar(i).Familia, 0, DsTomaAsiento1.GridAgregar(i).Monto, DsTomaAsiento1.GridAgregar(i).Cuenta, DsTomaAsiento1.GridAgregar(i).NombreCC, False)
                End If
            Next
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub


    Public Sub CargaDetalles(ByVal Familia As String)
        Try
            '-----------------------------------------------------------------------------------
            'CARGAR LOS DETALLES YA INGRESADOS PARA ESA FAMILIA EN CASO DE QUE SE QUIERAN CAMBIAR
            Dim Detalles() As System.Data.DataRow
            TotalAgregar = 0
            DsTomaAsiento1.GridAgregar.Clear()
            If DsTomaAsiento1.GridDetalles.Count > 0 Then
                For i As Integer = 0 To DsTomaAsiento1.GridDetalles.Count - 1
                    If DsTomaAsiento1.GridDetalles(i).Familia = Familia And DsTomaAsiento1.GridDetalles(i).Sistema = False Then
                        BindingContext(DsTomaAsiento1, "GridAgregar").EndCurrentEdit()
                        BindingContext(DsTomaAsiento1, "GridAgregar").AddNew()
                        BindingContext(DsTomaAsiento1, "GridAgregar").Current("Cuenta") = DsTomaAsiento1.GridDetalles(i).Cuenta
                        BindingContext(DsTomaAsiento1, "GridAgregar").Current("NombreCC") = DsTomaAsiento1.GridDetalles(i).NombreCuenta
                        BindingContext(DsTomaAsiento1, "GridAgregar").Current("Monto") = (DsTomaAsiento1.GridDetalles(i).Debe + DsTomaAsiento1.GridDetalles(i).Haber)
                        BindingContext(DsTomaAsiento1, "GridAgregar").Current("Familia") = DsTomaAsiento1.GridDetalles(i).Familia
                        BindingContext(DsTomaAsiento1, "GridAgregar").EndCurrentEdit()
                        TotalAgregar += (DsTomaAsiento1.GridDetalles(i).Debe + DsTomaAsiento1.GridDetalles(i).Haber)
                        EditaDetalle = True
                    End If
                Next
                GridAgregar.Refresh()
            End If
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub


    Private Sub EliminarDetalle()
        Try
            If MsgBox("Desea Eliminar este item del detalle..", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If

            If DsTomaAsiento1.GridAgregar.Count = 0 Then Exit Sub
            Dim posicion As Integer
            posicion = BindingContext(DsTomaAsiento1.GridAgregar).Position()

            '-----------------------------------------------------------------------------------
            'ACTUALIZA LOS TOTALES
            TotalAgregar = (TotalAgregar - BindingContext(DsTomaAsiento1.GridAgregar).Current("Monto"))
            TxtDetalle.Text = (BindingContext(DsTomaAsiento1, "GridDetalles").Current("Debe") + BindingContext(DsTomaAsiento1, "GridDetalles").Current("Haber"))
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'ELIMINA EL DETALLE DE LA FAMILIA
            DsTomaAsiento1.GridAgregar.Rows.RemoveAt(posicion)
            BindingContext(DsTomaAsiento1, "GridAgregar").CancelCurrentEdit()
            Edito = True
            '-----------------------------------------------------------------------------------

            '-----------------------------------------------------------------------------------
            'ACTUALIZA CONTROLES
            Controles(False)
            BNuevo.Text = "Nuevo"
            ButtonAgregarDetalle.Enabled = False
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub

    Private Sub EliminarDetalleAsiento()
        Try
            If DsTomaAsiento1.GridDetalles.Count = 0 Then Exit Sub
            '-----------------------------------------------------------------------------------
            'BUSCA LA POSICION DEL DETALLE DE ASIENTO QUE SE QUIERE ELIMINAR DE LOS DETALLES DE LA FAMILIA
            For i As Integer = 0 To DsTomaAsiento1.GridDetalles.Count - 1
                If i >= DsTomaAsiento1.GridDetalles.Count Then Exit For
                If DsTomaAsiento1.GridDetalles(i).Familia = BindingContext(DsTomaAsiento1.GridAgregar).Current("Familia") And DsTomaAsiento1.GridDetalles(i).Sistema = False Then
                    Elimina(DsTomaAsiento1.GridDetalles(i).Cuenta)
                    i = -1
                End If
            Next
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub

    Private Sub Elimina(ByVal Cuenta As String)
        Try
            '-----------------------------------------------------------------------------------
            'ELIMINA LOS DETALLES DEL ASIENTO
            For i As Integer = 0 To DsTomaAsiento1.GridDetalles.Count - 1
                If DsTomaAsiento1.GridDetalles(i).Cuenta = Cuenta And DsTomaAsiento1.GridDetalles(i).Familia = BindingContext(DsTomaAsiento1.GridAgregar).Current("Familia") And DsTomaAsiento1.GridDetalles(i).Sistema = False Then
                    DsTomaAsiento1.GridDetalles.Rows.RemoveAt(i)
                    BindingContext(DsTomaAsiento1, "GridDetalles").EndCurrentEdit()
                    Exit For
                End If
            Next
            '-----------------------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub
#End Region

#Region "Otras"
    Private Sub txtCuentaContable_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCuentaContable.KeyDown
        Try
            If e.KeyCode = Keys.F1 Then
                '-----------------------------------------------------------------------------------
                'ABRE BUSCARDOR DE LA CUENTA CONTABLE CUANDO SE PRESIONA F1
                Dim busca As New fmrBuscarMayorizacionAsiento
                busca.NuevaConexion = GetSetting("SeeSoft", "Contabilidad", "CONEXION")
                busca.sqlstring = " select CC.cuentacontable as [Cuenta contable],CC.descripcion as Descripcion,(SELECT descripcion from cuentacontable where id = cc.parentid) as [Cuenta madre] from cuentacontable CC " & _
                                " where Movimiento=1 "
                busca.campo = "descripcion"
                busca.sqlStringAdicional = " ORDER BY CuentaContable  "
                busca.ShowDialog()

                If busca.codigo Is Nothing Then Exit Sub

                txtCuentaContable.Text = busca.codigo
                txtDescripcionCuenta.Text = busca.descrip
                '-----------------------------------------------------------------------------------
            End If

            If e.KeyCode = Keys.Enter Then
                '-----------------------------------------------------------------------------------
                'VALIDA LA CUENTA CONTABLE DIGITADA AL PRESIONAR ENTER Y PASA AL SIGUIENTE CONTROL
                Dim Cx As New Conexion
                Dim valida As String
                Dim num_cuenta As String = txtCuentaContable.Text
                valida = Cx.SlqExecuteScalar(Cx.Conectar("Contabilidad"), "SELECT CuentaContable FROM CuentaContable WHERE CuentaContable= '" & num_cuenta & "' AND Movimiento=1")
                Cx.DesConectar(Cx.sQlconexion)
                If valida = "" Then
                    MessageBox.Show("La cuenta digitada no esta registrada..", "Informaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    txtCuentaContable.Focus()
                Else
                    Dim nombre As String
                    nombre = Cx.SlqExecuteScalar(Cx.Conectar("Contabilidad"), "SELECT Descripcion FROM CuentaContable WHERE CuentaContable= '" & num_cuenta & "' AND Movimiento=1")
                    Cx.DesConectar(Cx.sQlconexion)
                    txtDescripcionCuenta.Text = nombre
                    txtMonto.Focus()
                End If
                '-----------------------------------------------------------------------------------
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        End Try
    End Sub


    Private Sub txtMonto_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMonto.KeyDown
        If e.KeyCode = Keys.Enter Then
            ButtonAgregarDetalle.Focus()
        End If
    End Sub


    Private Sub GridCentroCosto_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridAgregar.KeyDown
        If e.KeyCode = Keys.Delete Then
            EliminarDetalle()
        End If
    End Sub


    Private Sub Panel_Centrar()
        BGuardar.Enabled = False
        BCancelar.Enabled = False
        GC_Asientos.Enabled = False
        PanelAgregar.Left = (Width - PanelAgregar.Width) \ 2
        PanelAgregar.Top = (Height - PanelAgregar.Height) \ 2
    End Sub


    Private Sub Panel_Ocultar()
        PanelAgregar.Left = -PanelAgregar.Width
        BGuardar.Enabled = True
        BCancelar.Enabled = True
        GC_Asientos.Enabled = True
    End Sub


    Private Sub Controles(ByVal estado As Boolean)
        txtCuentaContable.Enabled = estado
        txtMonto.Enabled = estado
    End Sub
#End Region

#End Region

End Class
