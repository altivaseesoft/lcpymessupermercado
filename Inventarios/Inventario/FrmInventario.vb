Imports DevExpress.Utils
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Data
Imports System.Windows.Forms
Imports LcPymes_5._2.UtilidadesImagenes
Imports System.Drawing

Public Class FrmInventario
    Inherits System.Windows.Forms.Form

    Dim usua As Usuario_Logeado
    Dim PMU As New PerfilModulo_Class
    Dim strNuevo As String = ""
    Dim Codigo_Equi As Integer
    Friend WithEvents txtCopiaEquivalencia As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcDesc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label50 As Label
    Friend WithEvents txtCostoCompra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label49 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents btnPegar As Button
    Friend WithEvents gbObservaciones As GroupBox
    Friend WithEvents Label51 As Label
    Friend WithEvents txtPorcIvaGv As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label52 As Label
    Friend WithEvents chControEntrega As CheckBox
    Friend WithEvents lbBarra As Label

#Region " Windows Form Designer generated code "
    Dim dlg As WaitDialogForm
    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        dlg = New WaitDialogForm("Cargando Componentes ...")
        dlg.Text = ""
        dlg.Caption = "Inicializando M�dulo...."
        InitializeComponent()
        dlg.Caption = "M�dulo Cargado...."
        'Add any initialization after the InitializeComponent() call

        dlg.Close()
        'AddHandler Me.BindingContext(Me.DataSetInventario, "Inventario2").PositionChanged, AddressOf Me.Position_Changed
    End Sub

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        AddHandler Me.BindingContext(Me.DataSetInventario, "Inventario2").PositionChanged, AddressOf Me.Position_Changed
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        'Asigna la imagen al imagenbox


    End Sub


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents AdapterAUbicacion As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterCasaConsignante As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterFamilia As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterMarcas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterPresentacion As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ComboBox_Ubicacion_SubUbicacion As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxBodegas As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxFamilia As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxPresentacion As System.Windows.Forms.ComboBox
    Friend WithEvents DataNavigator1 As DevExpress.XtraEditors.DataNavigator

    Friend WithEvents ErrorProvider As System.Windows.Forms.ErrorProvider
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TxtBarras As System.Windows.Forms.TextBox

    Friend WithEvents TxtCodigo As System.Windows.Forms.Label

    Friend WithEvents TxtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents TxtExistencia As System.Windows.Forms.TextBox
    Friend WithEvents TxtMax As System.Windows.Forms.TextBox
    Friend WithEvents TxtMed As System.Windows.Forms.TextBox
    Friend WithEvents TxtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents TxtPrecioVenta_A As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_B As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_C As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_D As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_A As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_B As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_C As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_D As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUtilidad_A As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUtilidad_B As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUtilidad_C As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUtilidad_D As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSetInventario As DataSetInventario
    Friend WithEvents AdapterInventario As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents TxtCanPresentacion As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxMonedaVenta1 As System.Windows.Forms.ComboBox
    Friend WithEvents AdapterArticulosXproveedor As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents colFechaUltimaCompra As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colCodigoProveedor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colUltimoCosto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents TextBoxValorMonedaEnVenta As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TxtUtilidad_P As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_P As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_P As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents Adapter_cod_Inventario As System.Data.SqlClient.SqlDataAdapter

    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Adaptador_Inventraio_AUX As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Text_Cod_AUX As System.Windows.Forms.TextBox
    Friend WithEvents TxtMin As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterBitacora As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ToolBarEliminador As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblCodigoProveedor As System.Windows.Forms.Label
    Friend WithEvents daProveedores As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents cmboxProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents ckVerCodBarraInv As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtPorSugerido As System.Windows.Forms.TextBox
    Friend WithEvents txtVentaSugerido As System.Windows.Forms.TextBox
    Friend WithEvents txtSugeridoIV As System.Windows.Forms.TextBox
    Friend WithEvents Ck_Consignacion As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents AdapterLote As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtExistenciaBodega As ValidText.ValidText
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TxtCostoEquivalente As System.Windows.Forms.TextBox
    Friend WithEvents TxtOtrosCargosEquivalente As System.Windows.Forms.TextBox
    Friend WithEvents TxtFleteEquivalente As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxValorMonedaEnCosto As System.Windows.Forms.TextBox
    Friend WithEvents TxtBaseEquivalente As System.Windows.Forms.TextBox
    Friend WithEvents TxtImpuesto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TxtFlete As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtOtros As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtCosto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboBoxMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TxtBase As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Ck_Lote As System.Windows.Forms.CheckBox
    Friend WithEvents Ck_PreguntaPrecio As System.Windows.Forms.CheckBox
    Friend WithEvents Check_Inhabilitado As System.Windows.Forms.CheckBox
    Friend WithEvents Check_Servicio As System.Windows.Forms.CheckBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Hasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Check_Promo As System.Windows.Forms.CheckBox
    Friend WithEvents Desde As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents TxtMaxDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCodigoProveedor1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLote As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCant_Actual As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents ButtonAgreBodega As System.Windows.Forms.Button
    Friend WithEvents ComboBoxBodega As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonAgregar As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminar As System.Windows.Forms.Button
    Friend WithEvents txtMedidas As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtAlterno As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents TabLotes As System.Windows.Forms.TabPage
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents TxtCostoPromedio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Tabequivalencia As System.Windows.Forms.TabPage
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtEquivalencia As System.Windows.Forms.TextBox
    Friend WithEvents GridControlEquivalencias As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents ButtonAgregarEqui As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminarEqui As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents TxtEquivalen As System.Windows.Forms.TextBox
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmInventario))
        Dim ColumnFilterInfo10 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo11 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo12 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo5 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo6 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo7 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo8 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo9 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo1 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo2 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo3 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo4 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo13 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo14 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo15 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo16 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo17 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo18 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Me.TxtCodigo = New System.Windows.Forms.Label()
        Me.DataSetInventario = New LcPymes_5._2.DataSetInventario()
        Me.TxtBarras = New System.Windows.Forms.TextBox()
        Me.TxtDescripcion = New System.Windows.Forms.TextBox()
        Me.TxtMed = New System.Windows.Forms.TextBox()
        Me.TxtMax = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ComboBoxPresentacion = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TxtObservaciones = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TxtPrecioVenta_A = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPrecioVenta_B = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPrecioVenta_C = New DevExpress.XtraEditors.TextEdit()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TxtPrecioVenta_D = New DevExpress.XtraEditors.TextEdit()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBoxValorMonedaEnVenta = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TxtUtilidad_D = New DevExpress.XtraEditors.TextEdit()
        Me.TxtUtilidad_C = New DevExpress.XtraEditors.TextEdit()
        Me.TxtUtilidad_B = New DevExpress.XtraEditors.TextEdit()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TxtPrecioVenta_IV_D = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPrecioVenta_IV_A = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPrecioVenta_IV_C = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPrecioVenta_IV_B = New DevExpress.XtraEditors.TextEdit()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TxtUtilidad_A = New DevExpress.XtraEditors.TextEdit()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.ComboBoxMonedaVenta1 = New System.Windows.Forms.ComboBox()
        Me.TxtUtilidad_P = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPrecioVenta_IV_P = New DevExpress.XtraEditors.TextEdit()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.TxtPrecioVenta_P = New DevExpress.XtraEditors.TextEdit()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TxtExistencia = New System.Windows.Forms.TextBox()
        Me.TxtMin = New System.Windows.Forms.TextBox()
        Me.ComboBoxBodegas = New System.Windows.Forms.ComboBox()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar()
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarEliminador = New System.Windows.Forms.ToolBarButton()
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton()
        Me.AdapterMarcas = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.ComboBoxFamilia = New System.Windows.Forms.ComboBox()
        Me.ComboBox_Ubicacion_SubUbicacion = New System.Windows.Forms.ComboBox()
        Me.AdapterFamilia = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterPresentacion = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterCasaConsignante = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand()
        Me.DataNavigator1 = New DevExpress.XtraEditors.DataNavigator()
        Me.AdapterAUbicacion = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand9 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand9 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand9 = New System.Data.SqlClient.SqlCommand()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommand10 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterInventario = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.TxtCanPresentacion = New System.Windows.Forms.TextBox()
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.AdapterArticulosXproveedor = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand()
        Me.colFechaUltimaCompra = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colCodigoProveedor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colUltimoCosto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Text_Cod_AUX = New System.Windows.Forms.TextBox()
        Me.Adaptador_Inventraio_AUX = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.Ck_Consignacion = New System.Windows.Forms.CheckBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtCopiaEquivalencia = New System.Windows.Forms.TextBox()
        Me.txtMedidas = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtAlterno = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.cmboxProveedor = New System.Windows.Forms.ComboBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.lblCodigoProveedor = New System.Windows.Forms.Label()
        Me.chControEntrega = New System.Windows.Forms.CheckBox()
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand()
        Me.AdapterBitacora = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand11 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand()
        Me.daProveedores = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand10 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand11 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand12 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand10 = New System.Data.SqlClient.SqlCommand()
        Me.ckVerCodBarraInv = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtPorSugerido = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txtVentaSugerido = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txtSugeridoIV = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxtExistenciaBodega = New ValidText.ValidText()
        Me.AdapterLote = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand11 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand12 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand13 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand11 = New System.Data.SqlClient.SqlCommand()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtPorcIvaGv = New DevExpress.XtraEditors.TextEdit()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.txtPorcDesc = New DevExpress.XtraEditors.TextEdit()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtCostoCompra = New DevExpress.XtraEditors.TextEdit()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.TxtCostoPromedio = New DevExpress.XtraEditors.TextEdit()
        Me.TxtCostoEquivalente = New System.Windows.Forms.TextBox()
        Me.TxtOtrosCargosEquivalente = New System.Windows.Forms.TextBox()
        Me.TxtFleteEquivalente = New System.Windows.Forms.TextBox()
        Me.TextBoxValorMonedaEnCosto = New System.Windows.Forms.TextBox()
        Me.TxtBaseEquivalente = New System.Windows.Forms.TextBox()
        Me.TxtImpuesto = New DevExpress.XtraEditors.TextEdit()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TxtFlete = New DevExpress.XtraEditors.TextEdit()
        Me.TxtOtros = New DevExpress.XtraEditors.TextEdit()
        Me.TxtCosto = New DevExpress.XtraEditors.TextEdit()
        Me.ComboBoxMoneda = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TxtBase = New DevExpress.XtraEditors.TextEdit()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Ck_Lote = New System.Windows.Forms.CheckBox()
        Me.Ck_PreguntaPrecio = New System.Windows.Forms.CheckBox()
        Me.Check_Inhabilitado = New System.Windows.Forms.CheckBox()
        Me.Check_Servicio = New System.Windows.Forms.CheckBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Hasta = New System.Windows.Forms.DateTimePicker()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Check_Promo = New System.Windows.Forms.CheckBox()
        Me.Desde = New System.Windows.Forms.DateTimePicker()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.TxtMaxDesc = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnPegar = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ButtonAgreBodega = New System.Windows.Forms.Button()
        Me.ComboBoxBodega = New System.Windows.Forms.ComboBox()
        Me.ButtonAgregar = New System.Windows.Forms.Button()
        Me.ButtonEliminar = New System.Windows.Forms.Button()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.TxtEquivalen = New System.Windows.Forms.TextBox()
        Me.Tabequivalencia = New System.Windows.Forms.TabPage()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtEquivalencia = New System.Windows.Forms.TextBox()
        Me.ButtonAgregarEqui = New System.Windows.Forms.Button()
        Me.ButtonEliminarEqui = New System.Windows.Forms.Button()
        Me.GridControlEquivalencias = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.TabLotes = New System.Windows.Forms.TabPage()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCodigoProveedor1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLote = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCant_Actual = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gbObservaciones = New System.Windows.Forms.GroupBox()
        Me.lbBarra = New System.Windows.Forms.Label()
        CType(Me.DataSetInventario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_A.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_B.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_C.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_D.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.TxtUtilidad_D.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUtilidad_C.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUtilidad_B.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_D.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_A.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_C.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_B.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUtilidad_A.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUtilidad_P.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_P.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_P.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.txtPorcIvaGv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPorcDesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCostoCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtCostoPromedio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtImpuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtOtros.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtBase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        Me.Tabequivalencia.SuspendLayout()
        CType(Me.GridControlEquivalencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabLotes.SuspendLayout()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbObservaciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'TxtCodigo
        '
        Me.TxtCodigo.BackColor = System.Drawing.Color.White
        Me.TxtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Codigo", True))
        Me.TxtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCodigo.ForeColor = System.Drawing.Color.Red
        Me.TxtCodigo.Location = New System.Drawing.Point(40, 116)
        Me.TxtCodigo.Name = "TxtCodigo"
        Me.TxtCodigo.Size = New System.Drawing.Size(89, 13)
        Me.TxtCodigo.TabIndex = 2
        Me.TxtCodigo.Text = "0"
        Me.TxtCodigo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DataSetInventario
        '
        Me.DataSetInventario.DataSetName = "DataSetInventario"
        Me.DataSetInventario.Locale = New System.Globalization.CultureInfo("es")
        Me.DataSetInventario.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TxtBarras
        '
        Me.TxtBarras.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtBarras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtBarras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Barras", True))
        Me.TxtBarras.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBarras.ForeColor = System.Drawing.Color.Blue
        Me.TxtBarras.Location = New System.Drawing.Point(4, 24)
        Me.TxtBarras.Name = "TxtBarras"
        Me.TxtBarras.Size = New System.Drawing.Size(125, 13)
        Me.TxtBarras.TabIndex = 1
        '
        'TxtDescripcion
        '
        Me.TxtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Descripcion", True))
        Me.TxtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDescripcion.ForeColor = System.Drawing.Color.Blue
        Me.TxtDescripcion.Location = New System.Drawing.Point(139, 24)
        Me.TxtDescripcion.Name = "TxtDescripcion"
        Me.TxtDescripcion.Size = New System.Drawing.Size(388, 13)
        Me.TxtDescripcion.TabIndex = 3
        '
        'TxtMed
        '
        Me.TxtMed.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMed.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.PuntoMedio", True))
        Me.TxtMed.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtMed.ForeColor = System.Drawing.Color.Blue
        Me.TxtMed.Location = New System.Drawing.Point(88, 32)
        Me.TxtMed.Name = "TxtMed"
        Me.TxtMed.Size = New System.Drawing.Size(52, 13)
        Me.TxtMed.TabIndex = 3
        Me.TxtMed.Text = "0.00"
        Me.TxtMed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtMax
        '
        Me.TxtMax.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMax.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Maxima", True))
        Me.TxtMax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtMax.ForeColor = System.Drawing.Color.Blue
        Me.TxtMax.Location = New System.Drawing.Point(160, 32)
        Me.TxtMax.Name = "TxtMax"
        Me.TxtMax.Size = New System.Drawing.Size(52, 13)
        Me.TxtMax.TabIndex = 5
        Me.TxtMax.Text = "0.00"
        Me.TxtMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(139, 4)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(388, 17)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Descripci�n"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(548, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(147, 14)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Presentaci�n"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(139, 39)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 21)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Familia"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(139, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(99, 19)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Equivalencia 2"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(12, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 12)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "M�nima"
        '
        'ComboBoxPresentacion
        '
        Me.ComboBoxPresentacion.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.CodPresentacion", True))
        Me.ComboBoxPresentacion.DataSource = Me.DataSetInventario
        Me.ComboBoxPresentacion.DisplayMember = "Presentaciones.Presentaciones"
        Me.ComboBoxPresentacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPresentacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxPresentacion.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxPresentacion.ItemHeight = 13
        Me.ComboBoxPresentacion.Location = New System.Drawing.Point(589, 19)
        Me.ComboBoxPresentacion.Name = "ComboBoxPresentacion"
        Me.ComboBoxPresentacion.Size = New System.Drawing.Size(106, 21)
        Me.ComboBoxPresentacion.TabIndex = 6
        Me.ComboBoxPresentacion.ValueMember = "Presentaciones.CodPres"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(88, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 12)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Media"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(160, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 12)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "M�xima"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(4, 572)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(333, 11)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Observaciones"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(139, 64)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(99, 21)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "Ubicaci�n"
        '
        'TxtObservaciones
        '
        Me.TxtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtObservaciones.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Observaciones", True))
        Me.TxtObservaciones.ForeColor = System.Drawing.Color.Blue
        Me.TxtObservaciones.Location = New System.Drawing.Point(7, 19)
        Me.TxtObservaciones.Multiline = True
        Me.TxtObservaciones.Name = "TxtObservaciones"
        Me.TxtObservaciones.Size = New System.Drawing.Size(670, 30)
        Me.TxtObservaciones.TabIndex = 9
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.Control
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label19.ForeColor = System.Drawing.Color.Blue
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(66, 106)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(11, 12)
        Me.Label19.TabIndex = 17
        Me.Label19.Text = "C"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.Control
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label20.ForeColor = System.Drawing.Color.Blue
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(66, 83)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(11, 12)
        Me.Label20.TabIndex = 12
        Me.Label20.Text = "B"
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.Control
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label21.ForeColor = System.Drawing.Color.Blue
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(66, 60)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(11, 16)
        Me.Label21.TabIndex = 7
        Me.Label21.Text = "A"
        '
        'TxtPrecioVenta_A
        '
        Me.TxtPrecioVenta_A.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_A", True))
        Me.TxtPrecioVenta_A.EditValue = "0.00"
        Me.TxtPrecioVenta_A.Location = New System.Drawing.Point(84, 60)
        Me.TxtPrecioVenta_A.Name = "TxtPrecioVenta_A"
        '
        '
        '
        Me.TxtPrecioVenta_A.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_A.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_A.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_A.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_A.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_A.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_A.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_A.TabIndex = 8
        '
        'TxtPrecioVenta_B
        '
        Me.TxtPrecioVenta_B.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_B", True))
        Me.TxtPrecioVenta_B.EditValue = "0.00"
        Me.TxtPrecioVenta_B.Location = New System.Drawing.Point(84, 83)
        Me.TxtPrecioVenta_B.Name = "TxtPrecioVenta_B"
        '
        '
        '
        Me.TxtPrecioVenta_B.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_B.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_B.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_B.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_B.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_B.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_B.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_B.TabIndex = 13
        '
        'TxtPrecioVenta_C
        '
        Me.TxtPrecioVenta_C.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_C", True))
        Me.TxtPrecioVenta_C.EditValue = "0.00"
        Me.TxtPrecioVenta_C.Location = New System.Drawing.Point(84, 106)
        Me.TxtPrecioVenta_C.Name = "TxtPrecioVenta_C"
        '
        '
        '
        Me.TxtPrecioVenta_C.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_C.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_C.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_C.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_C.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_C.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_C.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_C.TabIndex = 18
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.Control
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label22.ForeColor = System.Drawing.Color.Blue
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(66, 128)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(11, 12)
        Me.Label22.TabIndex = 22
        Me.Label22.Text = "D"
        '
        'TxtPrecioVenta_D
        '
        Me.TxtPrecioVenta_D.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_D", True))
        Me.TxtPrecioVenta_D.EditValue = "0.00"
        Me.TxtPrecioVenta_D.Location = New System.Drawing.Point(84, 128)
        Me.TxtPrecioVenta_D.Name = "TxtPrecioVenta_D"
        '
        '
        '
        Me.TxtPrecioVenta_D.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_D.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_D.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_D.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_D.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_D.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_D.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_D.TabIndex = 23
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.Control
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label23.ForeColor = System.Drawing.Color.Blue
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(164, 60)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(12, 12)
        Me.Label23.TabIndex = 9
        Me.Label23.Text = "+"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.TextBoxValorMonedaEnVenta)
        Me.GroupBox2.Controls.Add(Me.Label29)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_D)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_C)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_B)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_D)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_A)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_C)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_B)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_D)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_A)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_C)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_B)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_A)
        Me.GroupBox2.Controls.Add(Me.Label32)
        Me.GroupBox2.Controls.Add(Me.ComboBoxMonedaVenta1)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_P)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_P)
        Me.GroupBox2.Controls.Add(Me.Label35)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_P)
        Me.GroupBox2.Controls.Add(Me.Label36)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(439, 152)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(268, 186)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Precio de Venta"
        '
        'TextBoxValorMonedaEnVenta
        '
        Me.TextBoxValorMonedaEnVenta.AcceptsTab = True
        Me.TextBoxValorMonedaEnVenta.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxValorMonedaEnVenta.Enabled = False
        Me.TextBoxValorMonedaEnVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TextBoxValorMonedaEnVenta.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxValorMonedaEnVenta.Location = New System.Drawing.Point(188, 20)
        Me.TextBoxValorMonedaEnVenta.Name = "TextBoxValorMonedaEnVenta"
        Me.TextBoxValorMonedaEnVenta.ReadOnly = True
        Me.TextBoxValorMonedaEnVenta.Size = New System.Drawing.Size(72, 13)
        Me.TextBoxValorMonedaEnVenta.TabIndex = 2
        Me.TextBoxValorMonedaEnVenta.Text = "0.00"
        Me.TextBoxValorMonedaEnVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.Label29.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label29.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label29.Location = New System.Drawing.Point(8, 40)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(48, 16)
        Me.Label29.TabIndex = 3
        Me.Label29.Text = "Utilidad"
        '
        'TxtUtilidad_D
        '
        Me.TxtUtilidad_D.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.TxtUtilidad_D.Location = New System.Drawing.Point(8, 128)
        Me.TxtUtilidad_D.Name = "TxtUtilidad_D"
        '
        '
        '
        Me.TxtUtilidad_D.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_D.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_D.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_D.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_D.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_D.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_D.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_D.TabIndex = 21
        '
        'TxtUtilidad_C
        '
        Me.TxtUtilidad_C.EditValue = 0
        Me.TxtUtilidad_C.Location = New System.Drawing.Point(8, 106)
        Me.TxtUtilidad_C.Name = "TxtUtilidad_C"
        '
        '
        '
        Me.TxtUtilidad_C.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_C.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_C.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_C.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_C.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_C.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_C.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_C.TabIndex = 16
        '
        'TxtUtilidad_B
        '
        Me.TxtUtilidad_B.EditValue = 0
        Me.TxtUtilidad_B.Location = New System.Drawing.Point(8, 83)
        Me.TxtUtilidad_B.Name = "TxtUtilidad_B"
        '
        '
        '
        Me.TxtUtilidad_B.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_B.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_B.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_B.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_B.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_B.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_B.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_B.TabIndex = 11
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.Label28.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label28.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label28.Location = New System.Drawing.Point(180, 40)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(80, 16)
        Me.Label28.TabIndex = 5
        Me.Label28.Text = "Precio + I.V."
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.Label27.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label27.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label27.Location = New System.Drawing.Point(84, 40)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(76, 16)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Precio Venta"
        '
        'TxtPrecioVenta_IV_D
        '
        Me.TxtPrecioVenta_IV_D.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_D.Location = New System.Drawing.Point(180, 128)
        Me.TxtPrecioVenta_IV_D.Name = "TxtPrecioVenta_IV_D"
        '
        '
        '
        Me.TxtPrecioVenta_IV_D.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_D.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_D.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_D.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_D.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_D.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_D.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_D.TabIndex = 25
        '
        'TxtPrecioVenta_IV_A
        '
        Me.TxtPrecioVenta_IV_A.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_A.Location = New System.Drawing.Point(180, 60)
        Me.TxtPrecioVenta_IV_A.Name = "TxtPrecioVenta_IV_A"
        '
        '
        '
        Me.TxtPrecioVenta_IV_A.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_A.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_A.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_A.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_A.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_A.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_A.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_A.TabIndex = 10
        '
        'TxtPrecioVenta_IV_C
        '
        Me.TxtPrecioVenta_IV_C.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_C.Location = New System.Drawing.Point(180, 106)
        Me.TxtPrecioVenta_IV_C.Name = "TxtPrecioVenta_IV_C"
        '
        '
        '
        Me.TxtPrecioVenta_IV_C.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_C.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_C.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_C.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_C.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_C.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_C.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_C.TabIndex = 20
        '
        'TxtPrecioVenta_IV_B
        '
        Me.TxtPrecioVenta_IV_B.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_B.Location = New System.Drawing.Point(180, 83)
        Me.TxtPrecioVenta_IV_B.Name = "TxtPrecioVenta_IV_B"
        '
        '
        '
        Me.TxtPrecioVenta_IV_B.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_B.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_B.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_B.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_B.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_B.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_B.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_B.TabIndex = 15
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.SystemColors.Control
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label26.ForeColor = System.Drawing.Color.Blue
        Me.Label26.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label26.Location = New System.Drawing.Point(164, 128)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(12, 12)
        Me.Label26.TabIndex = 24
        Me.Label26.Text = "+"
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.Control
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label25.ForeColor = System.Drawing.Color.Blue
        Me.Label25.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label25.Location = New System.Drawing.Point(164, 106)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(12, 12)
        Me.Label25.TabIndex = 19
        Me.Label25.Text = "+"
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.SystemColors.Control
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label24.ForeColor = System.Drawing.Color.Blue
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(164, 83)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(12, 12)
        Me.Label24.TabIndex = 14
        Me.Label24.Text = "+"
        '
        'TxtUtilidad_A
        '
        Me.TxtUtilidad_A.EditValue = "0.00"
        Me.TxtUtilidad_A.Location = New System.Drawing.Point(8, 60)
        Me.TxtUtilidad_A.Name = "TxtUtilidad_A"
        '
        '
        '
        Me.TxtUtilidad_A.Properties.DisplayFormat.FormatString = "#,#0.0000"
        Me.TxtUtilidad_A.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_A.Properties.EditFormat.FormatString = "#,#0.0000"
        Me.TxtUtilidad_A.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_A.Properties.MaskData.EditMask = "#,#0.0000"
        Me.TxtUtilidad_A.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_A.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_A.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_A.TabIndex = 6
        '
        'Label32
        '
        Me.Label32.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label32.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label32.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label32.Location = New System.Drawing.Point(8, 20)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(48, 16)
        Me.Label32.TabIndex = 0
        Me.Label32.Text = "Moneda"
        '
        'ComboBoxMonedaVenta1
        '
        Me.ComboBoxMonedaVenta1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.MonedaVenta", True))
        Me.ComboBoxMonedaVenta1.DataSource = Me.DataSetInventario.Moneda
        Me.ComboBoxMonedaVenta1.DisplayMember = "MonedaNombre"
        Me.ComboBoxMonedaVenta1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMonedaVenta1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxMonedaVenta1.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxMonedaVenta1.ItemHeight = 13
        Me.ComboBoxMonedaVenta1.Location = New System.Drawing.Point(82, 16)
        Me.ComboBoxMonedaVenta1.Name = "ComboBoxMonedaVenta1"
        Me.ComboBoxMonedaVenta1.Size = New System.Drawing.Size(92, 21)
        Me.ComboBoxMonedaVenta1.TabIndex = 1
        Me.ComboBoxMonedaVenta1.ValueMember = "Moneda.CodMoneda"
        '
        'TxtUtilidad_P
        '
        Me.TxtUtilidad_P.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.TxtUtilidad_P.Location = New System.Drawing.Point(8, 150)
        Me.TxtUtilidad_P.Name = "TxtUtilidad_P"
        '
        '
        '
        Me.TxtUtilidad_P.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_P.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_P.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_P.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_P.Properties.Enabled = False
        Me.TxtUtilidad_P.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_P.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_P.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_P.TabIndex = 26
        '
        'TxtPrecioVenta_IV_P
        '
        Me.TxtPrecioVenta_IV_P.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_P.Location = New System.Drawing.Point(180, 150)
        Me.TxtPrecioVenta_IV_P.Name = "TxtPrecioVenta_IV_P"
        '
        '
        '
        Me.TxtPrecioVenta_IV_P.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_P.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_P.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_P.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_P.Properties.Enabled = False
        Me.TxtPrecioVenta_IV_P.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_P.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_P.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_P.TabIndex = 30
        '
        'Label35
        '
        Me.Label35.BackColor = System.Drawing.SystemColors.Control
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label35.ForeColor = System.Drawing.Color.Blue
        Me.Label35.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label35.Location = New System.Drawing.Point(164, 150)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(12, 12)
        Me.Label35.TabIndex = 29
        Me.Label35.Text = "+"
        '
        'TxtPrecioVenta_P
        '
        Me.TxtPrecioVenta_P.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_Promo", True))
        Me.TxtPrecioVenta_P.EditValue = "0.00"
        Me.TxtPrecioVenta_P.Location = New System.Drawing.Point(84, 150)
        Me.TxtPrecioVenta_P.Name = "TxtPrecioVenta_P"
        '
        '
        '
        Me.TxtPrecioVenta_P.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_P.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_P.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_P.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_P.Properties.Enabled = False
        Me.TxtPrecioVenta_P.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_P.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_P.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_P.TabIndex = 28
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.SystemColors.Control
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label36.ForeColor = System.Drawing.Color.Blue
        Me.Label36.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label36.Location = New System.Drawing.Point(66, 151)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(11, 12)
        Me.Label36.TabIndex = 27
        Me.Label36.Text = "P"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.TxtExistencia)
        Me.GroupBox3.Controls.Add(Me.TxtMin)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.TxtMed)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.TxtMax)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox3.Location = New System.Drawing.Point(8, 360)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(342, 49)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Existencias"
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label30.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(236, 16)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(72, 12)
        Me.Label30.TabIndex = 6
        Me.Label30.Text = "Actual"
        '
        'TxtExistencia
        '
        Me.TxtExistencia.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtExistencia.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Existencia", True))
        Me.TxtExistencia.Enabled = False
        Me.TxtExistencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtExistencia.ForeColor = System.Drawing.Color.Blue
        Me.TxtExistencia.Location = New System.Drawing.Point(236, 32)
        Me.TxtExistencia.Name = "TxtExistencia"
        Me.TxtExistencia.Size = New System.Drawing.Size(72, 13)
        Me.TxtExistencia.TabIndex = 7
        Me.TxtExistencia.Text = "0.00"
        Me.TxtExistencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtMin
        '
        Me.TxtMin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMin.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Minima", True))
        Me.TxtMin.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtMin.ForeColor = System.Drawing.Color.Blue
        Me.TxtMin.Location = New System.Drawing.Point(12, 31)
        Me.TxtMin.Name = "TxtMin"
        Me.TxtMin.Size = New System.Drawing.Size(52, 13)
        Me.TxtMin.TabIndex = 1
        Me.TxtMin.Text = "0.00"
        Me.TxtMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ComboBoxBodegas
        '
        Me.ComboBoxBodegas.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.Id_Bodega", True))
        Me.ComboBoxBodegas.DataSource = Me.DataSetInventario
        Me.ComboBoxBodegas.DisplayMember = "Bodegas.Nombre_Bodega"
        Me.ComboBoxBodegas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBodegas.Enabled = False
        Me.ComboBoxBodegas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxBodegas.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ComboBoxBodegas.ItemHeight = 13
        Me.ComboBoxBodegas.Location = New System.Drawing.Point(8, 40)
        Me.ComboBoxBodegas.Name = "ComboBoxBodegas"
        Me.ComboBoxBodegas.Size = New System.Drawing.Size(256, 21)
        Me.ComboBoxBodegas.TabIndex = 1
        Me.ComboBoxBodegas.ValueMember = "Bodegas.ID_Bodega"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Nuevo.png")
        Me.ImageList1.Images.SetKeyName(1, "Buscar.png")
        Me.ImageList1.Images.SetKeyName(2, "Guardar.png")
        Me.ImageList1.Images.SetKeyName(3, "Cancelar.png")
        Me.ImageList1.Images.SetKeyName(4, "Imprimir.png")
        Me.ImageList1.Images.SetKeyName(5, "Editar.png")
        Me.ImageList1.Images.SetKeyName(6, "Cerrar.png")
        Me.ImageList1.Images.SetKeyName(7, "Imprimir.png")
        Me.ImageList1.Images.SetKeyName(8, "Eliminar.png")
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.AutoSize = False
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarEliminador, Me.ToolBarCerrar})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 465)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(707, 54)
        Me.ToolBar1.TabIndex = 11
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Name = "ToolBarNuevo"
        Me.ToolBarNuevo.Text = "Nuevo"
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.ImageIndex = 1
        Me.ToolBarBuscar.Name = "ToolBarBuscar"
        Me.ToolBarBuscar.Text = "Buscar"
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
        Me.ToolBarRegistrar.Text = "Actualizar"
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.ImageIndex = 3
        Me.ToolBarEliminar.Name = "ToolBarEliminar"
        Me.ToolBarEliminar.Text = "Inhabilitar"
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.ImageIndex = 7
        Me.ToolBarImprimir.Name = "ToolBarImprimir"
        Me.ToolBarImprimir.Text = "Imprimir"
        '
        'ToolBarEliminador
        '
        Me.ToolBarEliminador.ImageIndex = 9
        Me.ToolBarEliminador.Name = "ToolBarEliminador"
        Me.ToolBarEliminador.Text = "Eliminar"
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Name = "ToolBarCerrar"
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'AdapterMarcas
        '
        Me.AdapterMarcas.DeleteCommand = Me.SqlDeleteCommand3
        Me.AdapterMarcas.InsertCommand = Me.SqlInsertCommand3
        Me.AdapterMarcas.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterMarcas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Marcas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMarca", "CodMarca"), New System.Data.Common.DataColumnMapping("Marca", "Marca")})})
        Me.AdapterMarcas.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Marcas WHERE (CodMarca = @Original_CodMarca)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodMarca", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMarca", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=.;Initial Catalog=SeePos;Integrated Security=True"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Marcas(Marca) VALUES (@Marca); SELECT CodMarca, Marca FROM Marcas WHE" &
    "RE (CodMarca = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Marca", System.Data.SqlDbType.VarChar, 50, "Marca")})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT CodMarca, Marca FROM Marcas"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Marcas SET Marca = @Marca WHERE (CodMarca = @Original_CodMarca); SELECT Co" &
    "dMarca, Marca FROM Marcas WHERE (CodMarca = @CodMarca)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Marca", System.Data.SqlDbType.VarChar, 50, "Marca"), New System.Data.SqlClient.SqlParameter("@Original_CodMarca", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMarca", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@CodMarca", System.Data.SqlDbType.Int, 4, "CodMarca")})
        '
        'ComboBoxFamilia
        '
        Me.ComboBoxFamilia.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.SubFamilia", True))
        Me.ComboBoxFamilia.DataSource = Me.DataSetInventario
        Me.ComboBoxFamilia.DisplayMember = "SubFamilias.Familiares"
        Me.ComboBoxFamilia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxFamilia.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxFamilia.ItemHeight = 13
        Me.ComboBoxFamilia.Location = New System.Drawing.Point(244, 39)
        Me.ComboBoxFamilia.Name = "ComboBoxFamilia"
        Me.ComboBoxFamilia.Size = New System.Drawing.Size(451, 21)
        Me.ComboBoxFamilia.TabIndex = 8
        Me.ComboBoxFamilia.ValueMember = "SubFamilias.Codigo"
        '
        'ComboBox_Ubicacion_SubUbicacion
        '
        Me.ComboBox_Ubicacion_SubUbicacion.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.SubUbicacion", True))
        Me.ComboBox_Ubicacion_SubUbicacion.DataSource = Me.DataSetInventario
        Me.ComboBox_Ubicacion_SubUbicacion.DisplayMember = "SubUbicacion.Ubicaciones"
        Me.ComboBox_Ubicacion_SubUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox_Ubicacion_SubUbicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBox_Ubicacion_SubUbicacion.ForeColor = System.Drawing.Color.Blue
        Me.ComboBox_Ubicacion_SubUbicacion.ItemHeight = 13
        Me.ComboBox_Ubicacion_SubUbicacion.Location = New System.Drawing.Point(244, 64)
        Me.ComboBox_Ubicacion_SubUbicacion.Name = "ComboBox_Ubicacion_SubUbicacion"
        Me.ComboBox_Ubicacion_SubUbicacion.Size = New System.Drawing.Size(451, 21)
        Me.ComboBox_Ubicacion_SubUbicacion.TabIndex = 10
        Me.ComboBox_Ubicacion_SubUbicacion.ValueMember = "SubUbicacion.Codigo"
        '
        'AdapterFamilia
        '
        Me.AdapterFamilia.DeleteCommand = Me.SqlDeleteCommand6
        Me.AdapterFamilia.InsertCommand = Me.SqlInsertCommand6
        Me.AdapterFamilia.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterFamilia.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "SubFamilias", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Familiares", "Familiares")})})
        Me.AdapterFamilia.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM SubFamilias WHERE (Codigo = @Original_Codigo)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO SubFamilias(Codigo) VALUES (@Codigo)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection1
        Me.SqlInsertCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 10, "Codigo")})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT SubFamilias.Codigo, Familia.Descripcion + '/' + SubFamilias.Descripcion AS" &
    " Familiares FROM SubFamilias INNER JOIN Familia ON SubFamilias.CodigoFamilia = F" &
    "amilia.Codigo"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE SubFamilias SET Codigo = @Codigo WHERE (Codigo = @Original_Codigo)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand6.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 10, "Codigo"), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'AdapterPresentacion
        '
        Me.AdapterPresentacion.DeleteCommand = Me.SqlDeleteCommand7
        Me.AdapterPresentacion.InsertCommand = Me.SqlInsertCommand7
        Me.AdapterPresentacion.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterPresentacion.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Presentaciones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Presentaciones", "Presentaciones"), New System.Data.Common.DataColumnMapping("CodPres", "CodPres")})})
        Me.AdapterPresentacion.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = "DELETE FROM Presentaciones WHERE (CodPres = @Original_CodPres)"
        Me.SqlDeleteCommand7.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodPres", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodPres", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO Presentaciones(Presentaciones) VALUES (@Presentaciones); SELECT Prese" &
    "ntaciones, CodPres FROM Presentaciones WHERE (CodPres = @@IDENTITY)"
        Me.SqlInsertCommand7.Connection = Me.SqlConnection1
        Me.SqlInsertCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Presentaciones", System.Data.SqlDbType.VarChar, 50, "Presentaciones")})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Presentaciones, CodPres FROM Presentaciones"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = "UPDATE Presentaciones SET Presentaciones = @Presentaciones WHERE (CodPres = @Orig" &
    "inal_CodPres); SELECT Presentaciones, CodPres FROM Presentaciones WHERE (CodPres" &
    " = @CodPres)"
        Me.SqlUpdateCommand7.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand7.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Presentaciones", System.Data.SqlDbType.VarChar, 50, "Presentaciones"), New System.Data.SqlClient.SqlParameter("@Original_CodPres", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodPres", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@CodPres", System.Data.SqlDbType.Int, 4, "CodPres")})
        '
        'AdapterCasaConsignante
        '
        Me.AdapterCasaConsignante.DeleteCommand = Me.SqlDeleteCommand8
        Me.AdapterCasaConsignante.InsertCommand = Me.SqlInsertCommand8
        Me.AdapterCasaConsignante.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterCasaConsignante.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Bodegas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Nombre_Bodega", "Nombre_Bodega"), New System.Data.Common.DataColumnMapping("ID_Bodega", "ID_Bodega")})})
        Me.AdapterCasaConsignante.UpdateCommand = Me.SqlUpdateCommand8
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = "DELETE FROM Bodegas WHERE (ID_Bodega = @Original_ID_Bodega)"
        Me.SqlDeleteCommand8.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_ID_Bodega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID_Bodega", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = "INSERT INTO Bodegas(Nombre_Bodega, ID_Bodega) VALUES (@Nombre_Bodega, @ID_Bodega)" &
    "; SELECT Nombre_Bodega, ID_Bodega FROM Bodegas WHERE (ID_Bodega = @ID_Bodega)"
        Me.SqlInsertCommand8.Connection = Me.SqlConnection1
        Me.SqlInsertCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre_Bodega", System.Data.SqlDbType.VarChar, 100, "Nombre_Bodega"), New System.Data.SqlClient.SqlParameter("@ID_Bodega", System.Data.SqlDbType.Int, 4, "ID_Bodega")})
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT Nombre_Bodega, ID_Bodega FROM Bodegas"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = "UPDATE Bodegas SET Nombre_Bodega = @Nombre_Bodega, ID_Bodega = @ID_Bodega WHERE (" &
    "ID_Bodega = @Original_ID_Bodega); SELECT Nombre_Bodega, ID_Bodega FROM Bodegas W" &
    "HERE (ID_Bodega = @ID_Bodega)"
        Me.SqlUpdateCommand8.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand8.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nombre_Bodega", System.Data.SqlDbType.VarChar, 100, "Nombre_Bodega"), New System.Data.SqlClient.SqlParameter("@ID_Bodega", System.Data.SqlDbType.Int, 4, "ID_Bodega"), New System.Data.SqlClient.SqlParameter("@Original_ID_Bodega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID_Bodega", System.Data.DataRowVersion.Original, Nothing)})
        '
        'DataNavigator1
        '
        Me.DataNavigator1.Buttons.Append.Visible = False
        Me.DataNavigator1.Buttons.CancelEdit.Visible = False
        Me.DataNavigator1.Buttons.EndEdit.Visible = False
        Me.DataNavigator1.Buttons.Remove.Visible = False
        Me.DataNavigator1.DataMember = "Inventario2"
        Me.DataNavigator1.DataSource = Me.DataSetInventario
        Me.DataNavigator1.Location = New System.Drawing.Point(486, 492)
        Me.DataNavigator1.Name = "DataNavigator1"
        Me.DataNavigator1.Size = New System.Drawing.Size(134, 21)
        Me.DataNavigator1.TabIndex = 68
        Me.DataNavigator1.Text = "DataNavigator1"
        '
        'AdapterAUbicacion
        '
        Me.AdapterAUbicacion.DeleteCommand = Me.SqlDeleteCommand9
        Me.AdapterAUbicacion.InsertCommand = Me.SqlInsertCommand9
        Me.AdapterAUbicacion.SelectCommand = Me.SqlSelectCommand6
        Me.AdapterAUbicacion.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "SubUbicacion", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Ubicaciones", "Ubicaciones")})})
        Me.AdapterAUbicacion.UpdateCommand = Me.SqlUpdateCommand9
        '
        'SqlDeleteCommand9
        '
        Me.SqlDeleteCommand9.CommandText = "DELETE FROM SubUbicacion WHERE (Codigo = @Original_Codigo)"
        Me.SqlDeleteCommand9.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand9.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand9
        '
        Me.SqlInsertCommand9.CommandText = "INSERT INTO SubUbicacion(Codigo) VALUES (@Codigo)"
        Me.SqlInsertCommand9.Connection = Me.SqlConnection1
        Me.SqlInsertCommand9.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 10, "Codigo")})
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT SubUbicacion.Codigo, Ubicaciones.Descripcion + '/' + SubUbicacion.Descripc" &
    "ionD AS Ubicaciones FROM SubUbicacion INNER JOIN Ubicaciones ON SubUbicacion.Cod" &
    "_Ubicacion = Ubicaciones.Codigo"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand9
        '
        Me.SqlUpdateCommand9.CommandText = "UPDATE SubUbicacion SET Codigo = @Codigo WHERE (Codigo = @Original_Codigo)"
        Me.SqlUpdateCommand9.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand9.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 10, "Codigo"), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.InsertCommand = Me.SqlInsertCommand10
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand7
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra")})})
        '
        'SqlInsertCommand10
        '
        Me.SqlInsertCommand10.CommandText = "INSERT INTO Moneda(CodMoneda, MonedaNombre, ValorCompra) VALUES (@CodMoneda, @Mon" &
    "edaNombre, @ValorCompra); SELECT CodMoneda, MonedaNombre, ValorCompra FROM Moned" &
    "a"
        Me.SqlInsertCommand10.Connection = Me.SqlConnection1
        Me.SqlInsertCommand10.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra")})
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra FROM Moneda"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection1
        '
        'AdapterInventario
        '
        Me.AdapterInventario.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterInventario.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterInventario.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterInventario.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Inventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Barras", "Barras"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("PresentaCant", "PresentaCant"), New System.Data.Common.DataColumnMapping("CodPresentacion", "CodPresentacion"), New System.Data.Common.DataColumnMapping("CodMarca", "CodMarca"), New System.Data.Common.DataColumnMapping("SubFamilia", "SubFamilia"), New System.Data.Common.DataColumnMapping("Minima", "Minima"), New System.Data.Common.DataColumnMapping("PuntoMedio", "PuntoMedio"), New System.Data.Common.DataColumnMapping("Maxima", "Maxima"), New System.Data.Common.DataColumnMapping("SubUbicacion", "SubUbicacion"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("MonedaCosto", "MonedaCosto"), New System.Data.Common.DataColumnMapping("PrecioBase", "PrecioBase"), New System.Data.Common.DataColumnMapping("Fletes", "Fletes"), New System.Data.Common.DataColumnMapping("OtrosCargos", "OtrosCargos"), New System.Data.Common.DataColumnMapping("Costo", "Costo"), New System.Data.Common.DataColumnMapping("MonedaVenta", "MonedaVenta"), New System.Data.Common.DataColumnMapping("IVenta", "IVenta"), New System.Data.Common.DataColumnMapping("Precio_A", "Precio_A"), New System.Data.Common.DataColumnMapping("Precio_B", "Precio_B"), New System.Data.Common.DataColumnMapping("Precio_C", "Precio_C"), New System.Data.Common.DataColumnMapping("Precio_D", "Precio_D"), New System.Data.Common.DataColumnMapping("Precio_Promo", "Precio_Promo"), New System.Data.Common.DataColumnMapping("Promo_Activa", "Promo_Activa"), New System.Data.Common.DataColumnMapping("Promo_Inicio", "Promo_Inicio"), New System.Data.Common.DataColumnMapping("Promo_Finaliza", "Promo_Finaliza"), New System.Data.Common.DataColumnMapping("Max_Comision", "Max_Comision"), New System.Data.Common.DataColumnMapping("Max_Descuento", "Max_Descuento"), New System.Data.Common.DataColumnMapping("FechaIngreso", "FechaIngreso"), New System.Data.Common.DataColumnMapping("Servicio", "Servicio"), New System.Data.Common.DataColumnMapping("Inhabilitado", "Inhabilitado"), New System.Data.Common.DataColumnMapping("Proveedor", "Proveedor"), New System.Data.Common.DataColumnMapping("Precio_Sugerido", "Precio_Sugerido"), New System.Data.Common.DataColumnMapping("SugeridoIV", "SugeridoIV"), New System.Data.Common.DataColumnMapping("PreguntaPrecio", "PreguntaPrecio"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("Lote", "Lote"), New System.Data.Common.DataColumnMapping("Consignacion", "Consignacion"), New System.Data.Common.DataColumnMapping("Id_Bodega", "Id_Bodega"), New System.Data.Common.DataColumnMapping("ExistenciaBodega", "ExistenciaBodega"), New System.Data.Common.DataColumnMapping("Alterno", "Alterno"), New System.Data.Common.DataColumnMapping("Medidas", "Medidas"), New System.Data.Common.DataColumnMapping("CostoPromedio", "CostoPromedio"), New System.Data.Common.DataColumnMapping("Equivalencia", "Equivalencia"), New System.Data.Common.DataColumnMapping("Imagen", "Imagen"), New System.Data.Common.DataColumnMapping("ControlEntrega", "ControlEntrega")})})
        Me.AdapterInventario.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PresentaCant", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PresentaCant", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodPresentacion", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodPresentacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMarca", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMarca", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubFamilia", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubFamilia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Minima", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Minima", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PuntoMedio", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PuntoMedio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Maxima", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maxima", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubUbicacion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubUbicacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaCosto", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaCosto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PrecioBase", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioBase", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fletes", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fletes", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_OtrosCargos", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OtrosCargos", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Costo", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Costo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaVenta", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaVenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_IVenta", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IVenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_A", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_A", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_B", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_B", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_C", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_C", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_D", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_D", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Promo", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Promo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Promo_Activa", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Activa", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Promo_Inicio", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Inicio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Promo_Finaliza", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Finaliza", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Max_Comision", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Comision", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Max_Descuento", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FechaIngreso", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaIngreso", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Servicio", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Servicio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Inhabilitado", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inhabilitado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Proveedor", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Proveedor", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Sugerido", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Sugerido", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SugeridoIV", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SugeridoIV", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PreguntaPrecio", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PreguntaPrecio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Lote", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Lote", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Consignacion", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consignacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Bodega", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Bodega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ExistenciaBodega", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExistenciaBodega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Alterno", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Alterno", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Alterno", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Alterno", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Medidas", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Medidas", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CostoPromedio", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoPromedio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Equivalencia", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Equivalencia", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ControlEntrega", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ControlEntrega", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 0, "Codigo"), New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 0, "Barras"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 0, "Descripcion"), New System.Data.SqlClient.SqlParameter("@PresentaCant", System.Data.SqlDbType.Float, 0, "PresentaCant"), New System.Data.SqlClient.SqlParameter("@CodPresentacion", System.Data.SqlDbType.Int, 0, "CodPresentacion"), New System.Data.SqlClient.SqlParameter("@CodMarca", System.Data.SqlDbType.Int, 0, "CodMarca"), New System.Data.SqlClient.SqlParameter("@SubFamilia", System.Data.SqlDbType.VarChar, 0, "SubFamilia"), New System.Data.SqlClient.SqlParameter("@Minima", System.Data.SqlDbType.Float, 0, "Minima"), New System.Data.SqlClient.SqlParameter("@PuntoMedio", System.Data.SqlDbType.Float, 0, "PuntoMedio"), New System.Data.SqlClient.SqlParameter("@Maxima", System.Data.SqlDbType.Float, 0, "Maxima"), New System.Data.SqlClient.SqlParameter("@SubUbicacion", System.Data.SqlDbType.VarChar, 0, "SubUbicacion"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@MonedaCosto", System.Data.SqlDbType.Int, 0, "MonedaCosto"), New System.Data.SqlClient.SqlParameter("@PrecioBase", System.Data.SqlDbType.Float, 0, "PrecioBase"), New System.Data.SqlClient.SqlParameter("@Fletes", System.Data.SqlDbType.Float, 0, "Fletes"), New System.Data.SqlClient.SqlParameter("@OtrosCargos", System.Data.SqlDbType.Float, 0, "OtrosCargos"), New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 0, "Costo"), New System.Data.SqlClient.SqlParameter("@MonedaVenta", System.Data.SqlDbType.Int, 0, "MonedaVenta"), New System.Data.SqlClient.SqlParameter("@IVenta", System.Data.SqlDbType.Float, 0, "IVenta"), New System.Data.SqlClient.SqlParameter("@Precio_A", System.Data.SqlDbType.Float, 0, "Precio_A"), New System.Data.SqlClient.SqlParameter("@Precio_B", System.Data.SqlDbType.Float, 0, "Precio_B"), New System.Data.SqlClient.SqlParameter("@Precio_C", System.Data.SqlDbType.Float, 0, "Precio_C"), New System.Data.SqlClient.SqlParameter("@Precio_D", System.Data.SqlDbType.Float, 0, "Precio_D"), New System.Data.SqlClient.SqlParameter("@Precio_Promo", System.Data.SqlDbType.Float, 0, "Precio_Promo"), New System.Data.SqlClient.SqlParameter("@Promo_Activa", System.Data.SqlDbType.Bit, 0, "Promo_Activa"), New System.Data.SqlClient.SqlParameter("@Promo_Inicio", System.Data.SqlDbType.SmallDateTime, 0, "Promo_Inicio"), New System.Data.SqlClient.SqlParameter("@Promo_Finaliza", System.Data.SqlDbType.SmallDateTime, 0, "Promo_Finaliza"), New System.Data.SqlClient.SqlParameter("@Max_Comision", System.Data.SqlDbType.Float, 0, "Max_Comision"), New System.Data.SqlClient.SqlParameter("@Max_Descuento", System.Data.SqlDbType.Float, 0, "Max_Descuento"), New System.Data.SqlClient.SqlParameter("@FechaIngreso", System.Data.SqlDbType.DateTime, 0, "FechaIngreso"), New System.Data.SqlClient.SqlParameter("@Servicio", System.Data.SqlDbType.Bit, 0, "Servicio"), New System.Data.SqlClient.SqlParameter("@Inhabilitado", System.Data.SqlDbType.Bit, 0, "Inhabilitado"), New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 0, "Proveedor"), New System.Data.SqlClient.SqlParameter("@Precio_Sugerido", System.Data.SqlDbType.Float, 0, "Precio_Sugerido"), New System.Data.SqlClient.SqlParameter("@SugeridoIV", System.Data.SqlDbType.Float, 0, "SugeridoIV"), New System.Data.SqlClient.SqlParameter("@PreguntaPrecio", System.Data.SqlDbType.Bit, 0, "PreguntaPrecio"), New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 0, "Existencia"), New System.Data.SqlClient.SqlParameter("@Lote", System.Data.SqlDbType.Bit, 0, "Lote"), New System.Data.SqlClient.SqlParameter("@Consignacion", System.Data.SqlDbType.Bit, 0, "Consignacion"), New System.Data.SqlClient.SqlParameter("@Id_Bodega", System.Data.SqlDbType.Int, 0, "Id_Bodega"), New System.Data.SqlClient.SqlParameter("@ExistenciaBodega", System.Data.SqlDbType.Float, 0, "ExistenciaBodega"), New System.Data.SqlClient.SqlParameter("@Alterno", System.Data.SqlDbType.VarChar, 0, "Alterno"), New System.Data.SqlClient.SqlParameter("@Medidas", System.Data.SqlDbType.VarChar, 0, "Medidas"), New System.Data.SqlClient.SqlParameter("@CostoPromedio", System.Data.SqlDbType.Float, 0, "CostoPromedio"), New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.VarChar, 0, "Equivalencia"), New System.Data.SqlClient.SqlParameter("@Imagen", System.Data.SqlDbType.Image, 0, "Imagen"), New System.Data.SqlClient.SqlParameter("@ControlEntrega", System.Data.SqlDbType.Bit, 0, "ControlEntrega")})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = resources.GetString("SqlSelectCommand1.CommandText")
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"), New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"), New System.Data.SqlClient.SqlParameter("@PresentaCant", System.Data.SqlDbType.Float, 8, "PresentaCant"), New System.Data.SqlClient.SqlParameter("@CodPresentacion", System.Data.SqlDbType.Int, 4, "CodPresentacion"), New System.Data.SqlClient.SqlParameter("@CodMarca", System.Data.SqlDbType.Int, 4, "CodMarca"), New System.Data.SqlClient.SqlParameter("@SubFamilia", System.Data.SqlDbType.VarChar, 10, "SubFamilia"), New System.Data.SqlClient.SqlParameter("@Minima", System.Data.SqlDbType.Float, 8, "Minima"), New System.Data.SqlClient.SqlParameter("@PuntoMedio", System.Data.SqlDbType.Float, 8, "PuntoMedio"), New System.Data.SqlClient.SqlParameter("@Maxima", System.Data.SqlDbType.Float, 8, "Maxima"), New System.Data.SqlClient.SqlParameter("@SubUbicacion", System.Data.SqlDbType.VarChar, 10, "SubUbicacion"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"), New System.Data.SqlClient.SqlParameter("@MonedaCosto", System.Data.SqlDbType.Int, 4, "MonedaCosto"), New System.Data.SqlClient.SqlParameter("@PrecioBase", System.Data.SqlDbType.Float, 8, "PrecioBase"), New System.Data.SqlClient.SqlParameter("@Fletes", System.Data.SqlDbType.Float, 8, "Fletes"), New System.Data.SqlClient.SqlParameter("@OtrosCargos", System.Data.SqlDbType.Float, 8, "OtrosCargos"), New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 8, "Costo"), New System.Data.SqlClient.SqlParameter("@MonedaVenta", System.Data.SqlDbType.Int, 4, "MonedaVenta"), New System.Data.SqlClient.SqlParameter("@IVenta", System.Data.SqlDbType.Float, 8, "IVenta"), New System.Data.SqlClient.SqlParameter("@Precio_A", System.Data.SqlDbType.Float, 8, "Precio_A"), New System.Data.SqlClient.SqlParameter("@Precio_B", System.Data.SqlDbType.Float, 8, "Precio_B"), New System.Data.SqlClient.SqlParameter("@Precio_C", System.Data.SqlDbType.Float, 8, "Precio_C"), New System.Data.SqlClient.SqlParameter("@Precio_D", System.Data.SqlDbType.Float, 8, "Precio_D"), New System.Data.SqlClient.SqlParameter("@Precio_Promo", System.Data.SqlDbType.Float, 8, "Precio_Promo"), New System.Data.SqlClient.SqlParameter("@Promo_Activa", System.Data.SqlDbType.Bit, 1, "Promo_Activa"), New System.Data.SqlClient.SqlParameter("@Promo_Inicio", System.Data.SqlDbType.SmallDateTime, 4, "Promo_Inicio"), New System.Data.SqlClient.SqlParameter("@Promo_Finaliza", System.Data.SqlDbType.SmallDateTime, 4, "Promo_Finaliza"), New System.Data.SqlClient.SqlParameter("@Max_Comision", System.Data.SqlDbType.Float, 8, "Max_Comision"), New System.Data.SqlClient.SqlParameter("@Max_Descuento", System.Data.SqlDbType.Float, 8, "Max_Descuento"), New System.Data.SqlClient.SqlParameter("@FechaIngreso", System.Data.SqlDbType.DateTime, 8, "FechaIngreso"), New System.Data.SqlClient.SqlParameter("@Servicio", System.Data.SqlDbType.Bit, 1, "Servicio"), New System.Data.SqlClient.SqlParameter("@Inhabilitado", System.Data.SqlDbType.Bit, 1, "Inhabilitado"), New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 4, "Proveedor"), New System.Data.SqlClient.SqlParameter("@Precio_Sugerido", System.Data.SqlDbType.Float, 8, "Precio_Sugerido"), New System.Data.SqlClient.SqlParameter("@SugeridoIV", System.Data.SqlDbType.Float, 8, "SugeridoIV"), New System.Data.SqlClient.SqlParameter("@PreguntaPrecio", System.Data.SqlDbType.Bit, 1, "PreguntaPrecio"), New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"), New System.Data.SqlClient.SqlParameter("@Lote", System.Data.SqlDbType.Bit, 1, "Lote"), New System.Data.SqlClient.SqlParameter("@Consignacion", System.Data.SqlDbType.Bit, 1, "Consignacion"), New System.Data.SqlClient.SqlParameter("@Id_Bodega", System.Data.SqlDbType.Int, 4, "Id_Bodega"), New System.Data.SqlClient.SqlParameter("@ExistenciaBodega", System.Data.SqlDbType.Float, 8, "ExistenciaBodega"), New System.Data.SqlClient.SqlParameter("@Alterno", System.Data.SqlDbType.VarChar, 255, "Alterno"), New System.Data.SqlClient.SqlParameter("@Medidas", System.Data.SqlDbType.VarChar, 50, "Medidas"), New System.Data.SqlClient.SqlParameter("@CostoPromedio", System.Data.SqlDbType.Float, 8, "CostoPromedio"), New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.VarChar, 300, "Equivalencia"), New System.Data.SqlClient.SqlParameter("@Imagen", System.Data.SqlDbType.Image, 2147483647, "Imagen"), New System.Data.SqlClient.SqlParameter("@ControlEntrega", System.Data.SqlDbType.Bit, 1, "ControlEntrega"), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PresentaCant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PresentaCant", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodPresentacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodPresentacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMarca", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMarca", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubFamilia", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubFamilia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Minima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Minima", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PuntoMedio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PuntoMedio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Maxima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maxima", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubUbicacion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubUbicacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaCosto", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaCosto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PrecioBase", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioBase", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fletes", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fletes", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_OtrosCargos", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OtrosCargos", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Costo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Costo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaVenta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaVenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_IVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IVenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_A", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_A", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_B", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_B", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_C", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_C", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_D", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_D", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Promo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Promo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Promo_Activa", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Activa", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Promo_Inicio", System.Data.SqlDbType.SmallDateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Inicio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Promo_Finaliza", System.Data.SqlDbType.SmallDateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Finaliza", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Max_Comision", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Comision", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Max_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_FechaIngreso", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaIngreso", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Servicio", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Servicio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Inhabilitado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inhabilitado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Proveedor", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Sugerido", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Sugerido", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SugeridoIV", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SugeridoIV", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PreguntaPrecio", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PreguntaPrecio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Lote", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Lote", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Consignacion", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consignacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Bodega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Bodega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ExistenciaBodega", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExistenciaBodega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Alterno", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Alterno", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Medidas", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Medidas", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CostoPromedio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoPromedio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Equivalencia", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_ControlEntrega", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ControlEntrega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Alterno", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Alterno", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Equivalencia", System.Data.SqlDbType.VarChar, 300, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, Nothing)})
        '
        'TxtCanPresentacion
        '
        Me.TxtCanPresentacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtCanPresentacion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.PresentaCant", True))
        Me.TxtCanPresentacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCanPresentacion.ForeColor = System.Drawing.Color.Blue
        Me.TxtCanPresentacion.Location = New System.Drawing.Point(551, 19)
        Me.TxtCanPresentacion.Name = "TxtCanPresentacion"
        Me.TxtCanPresentacion.Size = New System.Drawing.Size(32, 20)
        Me.TxtCanPresentacion.TabIndex = 5
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Agregar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Eliminar"
        '
        'AdapterArticulosXproveedor
        '
        Me.AdapterArticulosXproveedor.SelectCommand = Me.SqlSelectCommand8
        Me.AdapterArticulosXproveedor.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Articulos x Proveedor", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoArticulo", "CodigoArticulo"), New System.Data.Common.DataColumnMapping("CodigoProveedor", "CodigoProveedor"), New System.Data.Common.DataColumnMapping("FechaUltimaCompra", "FechaUltimaCompra"), New System.Data.Common.DataColumnMapping("UltimoCosto", "UltimoCosto"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = resources.GetString("SqlSelectCommand8.CommandText")
        Me.SqlSelectCommand8.Connection = Me.SqlConnection1
        '
        'colFechaUltimaCompra
        '
        Me.colFechaUltimaCompra.Caption = "Fecha Compra"
        Me.colFechaUltimaCompra.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.colFechaUltimaCompra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colFechaUltimaCompra.FieldName = "FechaUltimaCompra"
        Me.colFechaUltimaCompra.FilterInfo = ColumnFilterInfo10
        Me.colFechaUltimaCompra.Name = "colFechaUltimaCompra"
        Me.colFechaUltimaCompra.Visible = True
        Me.colFechaUltimaCompra.Width = 48
        '
        'colCodigoProveedor
        '
        Me.colCodigoProveedor.Caption = "Proveedor"
        Me.colCodigoProveedor.FieldName = "CodigoProveedor"
        Me.colCodigoProveedor.FilterInfo = ColumnFilterInfo11
        Me.colCodigoProveedor.Name = "colCodigoProveedor"
        Me.colCodigoProveedor.Visible = True
        Me.colCodigoProveedor.Width = 205
        '
        'colUltimoCosto
        '
        Me.colUltimoCosto.Caption = "Costo"
        Me.colUltimoCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colUltimoCosto.FieldName = "CodigoArticulo"
        Me.colUltimoCosto.FilterInfo = ColumnFilterInfo12
        Me.colUltimoCosto.Name = "colUltimoCosto"
        Me.colUltimoCosto.Visible = True
        Me.colUltimoCosto.Width = 63
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Columns.Add(Me.colCodigoProveedor)
        Me.GridBand1.Columns.Add(Me.colFechaUltimaCompra)
        Me.GridBand1.Columns.Add(Me.colUltimoCosto)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.Visible = False
        Me.GridBand1.Width = 10
        '
        'Text_Cod_AUX
        '
        Me.Text_Cod_AUX.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario2.Codigo", True))
        Me.Text_Cod_AUX.Location = New System.Drawing.Point(380, 500)
        Me.Text_Cod_AUX.Name = "Text_Cod_AUX"
        Me.Text_Cod_AUX.Size = New System.Drawing.Size(64, 20)
        Me.Text_Cod_AUX.TabIndex = 61
        '
        'Adaptador_Inventraio_AUX
        '
        Me.Adaptador_Inventraio_AUX.DeleteCommand = Me.SqlDeleteCommand2
        Me.Adaptador_Inventraio_AUX.InsertCommand = Me.SqlInsertCommand2
        Me.Adaptador_Inventraio_AUX.SelectCommand = Me.SqlSelectCommand9
        Me.Adaptador_Inventraio_AUX.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Inventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo")})})
        Me.Adaptador_Inventraio_AUX.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM [Inventario] WHERE (([Codigo] = @Original_Codigo))"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO [Inventario] ([Codigo]) VALUES (@Codigo);" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "SELECT Codigo FROM Inventa" &
    "rio WHERE (Codigo = @Codigo) ORDER BY Codigo"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 0, "Codigo")})
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT Codigo FROM Inventario ORDER BY Codigo"
        Me.SqlSelectCommand9.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE [Inventario] SET [Codigo] = @Codigo WHERE (([Codigo] = @Original_Codigo));" &
    "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "SELECT Codigo FROM Inventario WHERE (Codigo = @Codigo) ORDER BY Codigo"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 0, "Codigo"), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'Ck_Consignacion
        '
        Me.Ck_Consignacion.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Consignacion", True))
        Me.Ck_Consignacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ck_Consignacion.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Ck_Consignacion.Location = New System.Drawing.Point(8, 16)
        Me.Ck_Consignacion.Name = "Ck_Consignacion"
        Me.Ck_Consignacion.Size = New System.Drawing.Size(152, 16)
        Me.Ck_Consignacion.TabIndex = 2
        Me.Ck_Consignacion.Text = "Consignaci�n"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel2.Controls.Add(Me.txtCopiaEquivalencia)
        Me.Panel2.Controls.Add(Me.txtMedidas)
        Me.Panel2.Controls.Add(Me.Label42)
        Me.Panel2.Controls.Add(Me.txtAlterno)
        Me.Panel2.Controls.Add(Me.TxtCodigo)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label46)
        Me.Panel2.Controls.Add(Me.Label31)
        Me.Panel2.Controls.Add(Me.cmboxProveedor)
        Me.Panel2.Controls.Add(Me.Label41)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.ComboBoxFamilia)
        Me.Panel2.Controls.Add(Me.TxtDescripcion)
        Me.Panel2.Controls.Add(Me.TxtCanPresentacion)
        Me.Panel2.Controls.Add(Me.TxtBarras)
        Me.Panel2.Controls.Add(Me.ComboBoxPresentacion)
        Me.Panel2.Controls.Add(Me.ComboBox_Ubicacion_SubUbicacion)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.lblCodigoProveedor)
        Me.Panel2.Location = New System.Drawing.Point(0, 10)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(707, 136)
        Me.Panel2.TabIndex = 3
        '
        'txtCopiaEquivalencia
        '
        Me.txtCopiaEquivalencia.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Equivalencia", True))
        Me.txtCopiaEquivalencia.ForeColor = System.Drawing.Color.Blue
        Me.txtCopiaEquivalencia.Location = New System.Drawing.Point(244, 112)
        Me.txtCopiaEquivalencia.Name = "txtCopiaEquivalencia"
        Me.txtCopiaEquivalencia.Size = New System.Drawing.Size(451, 20)
        Me.txtCopiaEquivalencia.TabIndex = 38
        '
        'txtMedidas
        '
        Me.txtMedidas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMedidas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMedidas.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Medidas", True))
        Me.txtMedidas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMedidas.ForeColor = System.Drawing.Color.Blue
        Me.txtMedidas.Location = New System.Drawing.Point(4, 99)
        Me.txtMedidas.Name = "txtMedidas"
        Me.txtMedidas.Size = New System.Drawing.Size(125, 13)
        Me.txtMedidas.TabIndex = 37
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label42.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label42.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label42.Location = New System.Drawing.Point(4, 75)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(125, 21)
        Me.Label42.TabIndex = 36
        Me.Label42.Text = "Medidas"
        '
        'txtAlterno
        '
        Me.txtAlterno.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAlterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAlterno.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Alterno", True))
        Me.txtAlterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlterno.ForeColor = System.Drawing.Color.Blue
        Me.txtAlterno.Location = New System.Drawing.Point(4, 59)
        Me.txtAlterno.Name = "txtAlterno"
        Me.txtAlterno.ReadOnly = True
        Me.txtAlterno.Size = New System.Drawing.Size(125, 13)
        Me.txtAlterno.TabIndex = 35
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 116)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "ID:"
        '
        'Label46
        '
        Me.Label46.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label46.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label46.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label46.Location = New System.Drawing.Point(4, 39)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(125, 16)
        Me.Label46.TabIndex = 34
        Me.Label46.Text = "C�digo Alterno"
        '
        'Label31
        '
        Me.Label31.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label31.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label31.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label31.Location = New System.Drawing.Point(4, 4)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(125, 17)
        Me.Label31.TabIndex = 30
        Me.Label31.Text = "C�digo"
        '
        'cmboxProveedor
        '
        Me.cmboxProveedor.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.Proveedor", True))
        Me.cmboxProveedor.DataSource = Me.DataSetInventario
        Me.cmboxProveedor.DisplayMember = "Proveedores.Nombre"
        Me.cmboxProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmboxProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.cmboxProveedor.ForeColor = System.Drawing.Color.Blue
        Me.cmboxProveedor.ItemHeight = 13
        Me.cmboxProveedor.Location = New System.Drawing.Point(244, 88)
        Me.cmboxProveedor.Name = "cmboxProveedor"
        Me.cmboxProveedor.Size = New System.Drawing.Size(451, 21)
        Me.cmboxProveedor.TabIndex = 12
        Me.cmboxProveedor.ValueMember = "Proveedores.CodigoProv"
        '
        'Label41
        '
        Me.Label41.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label41.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label41.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label41.Location = New System.Drawing.Point(139, 88)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(99, 21)
        Me.Label41.TabIndex = 11
        Me.Label41.Text = "Proveedor "
        '
        'lblCodigoProveedor
        '
        Me.lblCodigoProveedor.Location = New System.Drawing.Point(444, 116)
        Me.lblCodigoProveedor.Name = "lblCodigoProveedor"
        Me.lblCodigoProveedor.Size = New System.Drawing.Size(100, 16)
        Me.lblCodigoProveedor.TabIndex = 29
        Me.lblCodigoProveedor.Text = "Label42"
        Me.lblCodigoProveedor.Visible = False
        '
        'chControEntrega
        '
        Me.chControEntrega.AutoSize = True
        Me.chControEntrega.BackColor = System.Drawing.Color.White
        Me.chControEntrega.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.ControlEntrega", True))
        Me.chControEntrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chControEntrega.Location = New System.Drawing.Point(585, 395)
        Me.chControEntrega.Name = "chControEntrega"
        Me.chControEntrega.Size = New System.Drawing.Size(114, 17)
        Me.chControEntrega.TabIndex = 39
        Me.chControEntrega.Text = "Control Entrega"
        Me.chControEntrega.UseVisualStyleBackColor = False
        '
        'AdapterBitacora
        '
        Me.AdapterBitacora.DeleteCommand = Me.SqlDeleteCommand5
        Me.AdapterBitacora.InsertCommand = Me.SqlInsertCommand5
        Me.AdapterBitacora.SelectCommand = Me.SqlSelectCommand11
        Me.AdapterBitacora.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Bitacora", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Secuencia", "Secuencia"), New System.Data.Common.DataColumnMapping("Tabla", "Tabla"), New System.Data.Common.DataColumnMapping("Campo_Clave", "Campo_Clave"), New System.Data.Common.DataColumnMapping("DescripcionCampo", "DescripcionCampo"), New System.Data.Common.DataColumnMapping("Accion", "Accion"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Costo", "Costo"), New System.Data.Common.DataColumnMapping("VentaA", "VentaA"), New System.Data.Common.DataColumnMapping("VentaB", "VentaB"), New System.Data.Common.DataColumnMapping("VentaC", "VentaC"), New System.Data.Common.DataColumnMapping("VentaD", "VentaD")})})
        Me.AdapterBitacora.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Bitacora WHERE (Secuencia = @Original_Secuencia)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Secuencia", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Secuencia", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = resources.GetString("SqlInsertCommand5.CommandText")
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Tabla", System.Data.SqlDbType.VarChar, 75, "Tabla"), New System.Data.SqlClient.SqlParameter("@Campo_Clave", System.Data.SqlDbType.VarChar, 75, "Campo_Clave"), New System.Data.SqlClient.SqlParameter("@DescripcionCampo", System.Data.SqlDbType.VarChar, 250, "DescripcionCampo"), New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 75, "Accion"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 150, "Usuario"), New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 8, "Costo"), New System.Data.SqlClient.SqlParameter("@VentaA", System.Data.SqlDbType.Float, 8, "VentaA"), New System.Data.SqlClient.SqlParameter("@VentaB", System.Data.SqlDbType.Float, 8, "VentaB"), New System.Data.SqlClient.SqlParameter("@VentaC", System.Data.SqlDbType.Float, 8, "VentaC"), New System.Data.SqlClient.SqlParameter("@VentaD", System.Data.SqlDbType.Float, 8, "VentaD")})
        '
        'SqlSelectCommand11
        '
        Me.SqlSelectCommand11.CommandText = "SELECT Secuencia, Tabla, Campo_Clave, DescripcionCampo, Accion, Fecha, Usuario, C" &
    "osto, VentaA, VentaB, VentaC, VentaD FROM Bitacora"
        Me.SqlSelectCommand11.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = resources.GetString("SqlUpdateCommand5.CommandText")
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand5.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Tabla", System.Data.SqlDbType.VarChar, 75, "Tabla"), New System.Data.SqlClient.SqlParameter("@Campo_Clave", System.Data.SqlDbType.VarChar, 75, "Campo_Clave"), New System.Data.SqlClient.SqlParameter("@DescripcionCampo", System.Data.SqlDbType.VarChar, 250, "DescripcionCampo"), New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 75, "Accion"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 150, "Usuario"), New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 8, "Costo"), New System.Data.SqlClient.SqlParameter("@VentaA", System.Data.SqlDbType.Float, 8, "VentaA"), New System.Data.SqlClient.SqlParameter("@VentaB", System.Data.SqlDbType.Float, 8, "VentaB"), New System.Data.SqlClient.SqlParameter("@VentaC", System.Data.SqlDbType.Float, 8, "VentaC"), New System.Data.SqlClient.SqlParameter("@VentaD", System.Data.SqlDbType.Float, 8, "VentaD"), New System.Data.SqlClient.SqlParameter("@Original_Secuencia", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Secuencia", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Secuencia", System.Data.SqlDbType.BigInt, 8, "Secuencia")})
        '
        'daProveedores
        '
        Me.daProveedores.DeleteCommand = Me.SqlDeleteCommand10
        Me.daProveedores.InsertCommand = Me.SqlInsertCommand11
        Me.daProveedores.SelectCommand = Me.SqlSelectCommand12
        Me.daProveedores.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Proveedores", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoProv", "CodigoProv"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        Me.daProveedores.UpdateCommand = Me.SqlUpdateCommand10
        '
        'SqlDeleteCommand10
        '
        Me.SqlDeleteCommand10.CommandText = "DELETE FROM Proveedores WHERE (CodigoProv = @Original_CodigoProv)"
        Me.SqlDeleteCommand10.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand10.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodigoProv", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoProv", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand11
        '
        Me.SqlInsertCommand11.CommandText = "INSERT INTO Proveedores(CodigoProv, Nombre) VALUES (@CodigoProv, @Nombre); SELECT" &
    " CodigoProv, Nombre FROM Proveedores WHERE (CodigoProv = @CodigoProv)"
        Me.SqlInsertCommand11.Connection = Me.SqlConnection1
        Me.SqlInsertCommand11.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodigoProv", System.Data.SqlDbType.Int, 4, "CodigoProv"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre")})
        '
        'SqlSelectCommand12
        '
        Me.SqlSelectCommand12.CommandText = "SELECT CodigoProv, Nombre FROM Proveedores"
        Me.SqlSelectCommand12.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand10
        '
        Me.SqlUpdateCommand10.CommandText = "UPDATE Proveedores SET CodigoProv = @CodigoProv, Nombre = @Nombre WHERE (CodigoPr" &
    "ov = @Original_CodigoProv); SELECT CodigoProv, Nombre FROM Proveedores WHERE (Co" &
    "digoProv = @CodigoProv)"
        Me.SqlUpdateCommand10.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand10.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodigoProv", System.Data.SqlDbType.Int, 4, "CodigoProv"), New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"), New System.Data.SqlClient.SqlParameter("@Original_CodigoProv", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoProv", System.Data.DataRowVersion.Original, Nothing)})
        '
        'ckVerCodBarraInv
        '
        Me.ckVerCodBarraInv.Location = New System.Drawing.Point(486, 470)
        Me.ckVerCodBarraInv.Name = "ckVerCodBarraInv"
        Me.ckVerCodBarraInv.Size = New System.Drawing.Size(124, 16)
        Me.ckVerCodBarraInv.TabIndex = 69
        Me.ckVerCodBarraInv.Text = "Verificar C�d. Barra"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.txtPorSugerido)
        Me.GroupBox5.Controls.Add(Me.Label43)
        Me.GroupBox5.Controls.Add(Me.txtVentaSugerido)
        Me.GroupBox5.Controls.Add(Me.Label44)
        Me.GroupBox5.Controls.Add(Me.txtSugeridoIV)
        Me.GroupBox5.Controls.Add(Me.Label45)
        Me.GroupBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox5.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox5.Location = New System.Drawing.Point(439, 344)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(262, 49)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Precio Venta Sugerido:"
        '
        'txtPorSugerido
        '
        Me.txtPorSugerido.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPorSugerido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorSugerido.ForeColor = System.Drawing.Color.Blue
        Me.txtPorSugerido.Location = New System.Drawing.Point(16, 32)
        Me.txtPorSugerido.Name = "txtPorSugerido"
        Me.txtPorSugerido.Size = New System.Drawing.Size(48, 13)
        Me.txtPorSugerido.TabIndex = 1
        Me.txtPorSugerido.Text = "0.00"
        Me.txtPorSugerido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label43.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label43.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label43.Location = New System.Drawing.Point(96, 16)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(64, 12)
        Me.Label43.TabIndex = 2
        Me.Label43.Text = "Venta Sug."
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtVentaSugerido
        '
        Me.txtVentaSugerido.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVentaSugerido.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Precio_Sugerido", True))
        Me.txtVentaSugerido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVentaSugerido.ForeColor = System.Drawing.Color.Blue
        Me.txtVentaSugerido.Location = New System.Drawing.Point(88, 32)
        Me.txtVentaSugerido.Name = "txtVentaSugerido"
        Me.txtVentaSugerido.Size = New System.Drawing.Size(72, 13)
        Me.txtVentaSugerido.TabIndex = 3
        Me.txtVentaSugerido.Text = "0.00"
        Me.txtVentaSugerido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label44
        '
        Me.Label44.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label44.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label44.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label44.Location = New System.Drawing.Point(184, 16)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(72, 12)
        Me.Label44.TabIndex = 4
        Me.Label44.Text = "Sug. + I.V."
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtSugeridoIV
        '
        Me.txtSugeridoIV.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSugeridoIV.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.SugeridoIV", True))
        Me.txtSugeridoIV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSugeridoIV.ForeColor = System.Drawing.Color.Blue
        Me.txtSugeridoIV.Location = New System.Drawing.Point(184, 32)
        Me.txtSugeridoIV.Name = "txtSugeridoIV"
        Me.txtSugeridoIV.Size = New System.Drawing.Size(72, 13)
        Me.txtSugeridoIV.TabIndex = 5
        Me.txtSugeridoIV.Text = "0.00"
        Me.txtSugeridoIV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label45
        '
        Me.Label45.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label45.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label45.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label45.Location = New System.Drawing.Point(16, 16)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(52, 12)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Porc."
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.Label7)
        Me.GroupBox6.Controls.Add(Me.TxtExistenciaBodega)
        Me.GroupBox6.Controls.Add(Me.Ck_Consignacion)
        Me.GroupBox6.Controls.Add(Me.ComboBoxBodegas)
        Me.GroupBox6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox6.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox6.Location = New System.Drawing.Point(4, 600)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(336, 68)
        Me.GroupBox6.TabIndex = 8
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Bodega"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(264, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 14)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Existencia"
        '
        'TxtExistenciaBodega
        '
        Me.TxtExistenciaBodega.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtExistenciaBodega.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.ExistenciaBodega", True))
        Me.TxtExistenciaBodega.Enabled = False
        Me.TxtExistenciaBodega.FieldReference = Nothing
        Me.TxtExistenciaBodega.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtExistenciaBodega.ForeColor = System.Drawing.Color.Blue
        Me.TxtExistenciaBodega.Location = New System.Drawing.Point(272, 40)
        Me.TxtExistenciaBodega.MaskEdit = ""
        Me.TxtExistenciaBodega.Name = "TxtExistenciaBodega"
        Me.TxtExistenciaBodega.RegExPattern = ValidText.ValidText.RegularExpressionModes.Custom
        Me.TxtExistenciaBodega.Required = False
        Me.TxtExistenciaBodega.ShowErrorIcon = False
        Me.TxtExistenciaBodega.Size = New System.Drawing.Size(60, 20)
        Me.TxtExistenciaBodega.TabIndex = 5
        Me.TxtExistenciaBodega.Text = "0.00"
        Me.TxtExistenciaBodega.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtExistenciaBodega.ValidationMode = ValidText.ValidText.ValidationModes.Numbers
        Me.TxtExistenciaBodega.ValidText = ""
        '
        'AdapterLote
        '
        Me.AdapterLote.DeleteCommand = Me.SqlDeleteCommand11
        Me.AdapterLote.InsertCommand = Me.SqlInsertCommand12
        Me.AdapterLote.SelectCommand = Me.SqlSelectCommand13
        Me.AdapterLote.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Lotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Numero", "Numero"), New System.Data.Common.DataColumnMapping("Vencimiento", "Vencimiento"), New System.Data.Common.DataColumnMapping("Cant_Actual", "Cant_Actual"), New System.Data.Common.DataColumnMapping("Cod_Articulo", "Cod_Articulo"), New System.Data.Common.DataColumnMapping("Id", "Id")})})
        Me.AdapterLote.UpdateCommand = Me.SqlUpdateCommand11
        '
        'SqlDeleteCommand11
        '
        Me.SqlDeleteCommand11.CommandText = "DELETE FROM Lotes WHERE (Id = @Original_Id)"
        Me.SqlDeleteCommand11.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand11.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand12
        '
        Me.SqlInsertCommand12.CommandText = resources.GetString("SqlInsertCommand12.CommandText")
        Me.SqlInsertCommand12.Connection = Me.SqlConnection1
        Me.SqlInsertCommand12.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Numero", System.Data.SqlDbType.VarChar, 250, "Numero"), New System.Data.SqlClient.SqlParameter("@Vencimiento", System.Data.SqlDbType.DateTime, 8, "Vencimiento"), New System.Data.SqlClient.SqlParameter("@Cant_Actual", System.Data.SqlDbType.Float, 8, "Cant_Actual"), New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo")})
        '
        'SqlSelectCommand13
        '
        Me.SqlSelectCommand13.CommandText = "SELECT Numero, Vencimiento, Cant_Actual, Cod_Articulo, Id FROM Lotes"
        Me.SqlSelectCommand13.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand11
        '
        Me.SqlUpdateCommand11.CommandText = resources.GetString("SqlUpdateCommand11.CommandText")
        Me.SqlUpdateCommand11.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand11.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Numero", System.Data.SqlDbType.VarChar, 250, "Numero"), New System.Data.SqlClient.SqlParameter("@Vencimiento", System.Data.SqlDbType.DateTime, 8, "Vencimiento"), New System.Data.SqlClient.SqlParameter("@Cant_Actual", System.Data.SqlDbType.Float, 8, "Cant_Actual"), New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtPorcIvaGv)
        Me.TabPage1.Controls.Add(Me.Label52)
        Me.TabPage1.Controls.Add(Me.Label51)
        Me.TabPage1.Controls.Add(Me.txtPorcDesc)
        Me.TabPage1.Controls.Add(Me.Label50)
        Me.TabPage1.Controls.Add(Me.txtCostoCompra)
        Me.TabPage1.Controls.Add(Me.Label49)
        Me.TabPage1.Controls.Add(Me.Label48)
        Me.TabPage1.Controls.Add(Me.TxtCostoPromedio)
        Me.TabPage1.Controls.Add(Me.TxtCostoEquivalente)
        Me.TabPage1.Controls.Add(Me.TxtOtrosCargosEquivalente)
        Me.TabPage1.Controls.Add(Me.TxtFleteEquivalente)
        Me.TabPage1.Controls.Add(Me.TextBoxValorMonedaEnCosto)
        Me.TabPage1.Controls.Add(Me.TxtBaseEquivalente)
        Me.TabPage1.Controls.Add(Me.TxtImpuesto)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.TxtFlete)
        Me.TabPage1.Controls.Add(Me.TxtOtros)
        Me.TabPage1.Controls.Add(Me.TxtCosto)
        Me.TabPage1.Controls.Add(Me.ComboBoxMoneda)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.TxtBase)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(425, 180)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "�ltimo Costo"
        '
        'txtPorcIvaGv
        '
        Me.txtPorcIvaGv.EditValue = "0.00"
        Me.txtPorcIvaGv.Location = New System.Drawing.Point(336, 74)
        Me.txtPorcIvaGv.Name = "txtPorcIvaGv"
        '
        '
        '
        Me.txtPorcIvaGv.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtPorcIvaGv.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPorcIvaGv.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtPorcIvaGv.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPorcIvaGv.Properties.ReadOnly = True
        Me.txtPorcIvaGv.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtPorcIvaGv.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtPorcIvaGv.Size = New System.Drawing.Size(80, 19)
        Me.txtPorcIvaGv.TabIndex = 94
        '
        'Label52
        '
        Me.Label52.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label52.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label52.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label52.Location = New System.Drawing.Point(198, 74)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(134, 16)
        Me.Label52.TabIndex = 93
        Me.Label52.Text = "% IVA General Venta"
        '
        'Label51
        '
        Me.Label51.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label51.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label51.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label51.Location = New System.Drawing.Point(8, 32)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(98, 18)
        Me.Label51.TabIndex = 92
        Me.Label51.Text = "% IVA Compra"
        '
        'txtPorcDesc
        '
        Me.txtPorcDesc.EditValue = "0.00"
        Me.txtPorcDesc.Location = New System.Drawing.Point(336, 52)
        Me.txtPorcDesc.Name = "txtPorcDesc"
        '
        '
        '
        Me.txtPorcDesc.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtPorcDesc.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPorcDesc.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtPorcDesc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPorcDesc.Properties.ReadOnly = True
        Me.txtPorcDesc.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtPorcDesc.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtPorcDesc.Size = New System.Drawing.Size(80, 19)
        Me.txtPorcDesc.TabIndex = 91
        '
        'Label50
        '
        Me.Label50.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label50.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label50.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label50.Location = New System.Drawing.Point(200, 52)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(132, 19)
        Me.Label50.TabIndex = 90
        Me.Label50.Text = "Porc. Desc"
        '
        'txtCostoCompra
        '
        Me.txtCostoCompra.EditValue = "0.00"
        Me.txtCostoCompra.Location = New System.Drawing.Point(336, 31)
        Me.txtCostoCompra.Name = "txtCostoCompra"
        '
        '
        '
        Me.txtCostoCompra.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtCostoCompra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCostoCompra.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtCostoCompra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCostoCompra.Properties.ReadOnly = True
        Me.txtCostoCompra.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtCostoCompra.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtCostoCompra.Size = New System.Drawing.Size(80, 19)
        Me.txtCostoCompra.TabIndex = 89
        '
        'Label49
        '
        Me.Label49.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label49.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label49.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label49.Location = New System.Drawing.Point(198, 32)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(134, 20)
        Me.Label49.TabIndex = 88
        Me.Label49.Text = "Costo Compra"
        '
        'Label48
        '
        Me.Label48.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label48.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label48.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label48.Location = New System.Drawing.Point(8, 133)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(98, 17)
        Me.Label48.TabIndex = 87
        Me.Label48.Text = "Costo Prom."
        '
        'TxtCostoPromedio
        '
        Me.TxtCostoPromedio.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.CostoPromedio", True))
        Me.TxtCostoPromedio.EditValue = "0.00"
        Me.TxtCostoPromedio.Location = New System.Drawing.Point(112, 132)
        Me.TxtCostoPromedio.Name = "TxtCostoPromedio"
        '
        '
        '
        Me.TxtCostoPromedio.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TxtCostoPromedio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtCostoPromedio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtCostoPromedio.Properties.Enabled = False
        Me.TxtCostoPromedio.Properties.ReadOnly = True
        Me.TxtCostoPromedio.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtCostoPromedio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtCostoPromedio.Size = New System.Drawing.Size(80, 19)
        Me.TxtCostoPromedio.TabIndex = 86
        '
        'TxtCostoEquivalente
        '
        Me.TxtCostoEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtCostoEquivalente.Enabled = False
        Me.TxtCostoEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtCostoEquivalente.ForeColor = System.Drawing.Color.Blue
        Me.TxtCostoEquivalente.Location = New System.Drawing.Point(248, 137)
        Me.TxtCostoEquivalente.Name = "TxtCostoEquivalente"
        Me.TxtCostoEquivalente.ReadOnly = True
        Me.TxtCostoEquivalente.Size = New System.Drawing.Size(84, 13)
        Me.TxtCostoEquivalente.TabIndex = 0
        Me.TxtCostoEquivalente.Text = "0.00"
        Me.TxtCostoEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtCostoEquivalente.Visible = False
        '
        'TxtOtrosCargosEquivalente
        '
        Me.TxtOtrosCargosEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtOtrosCargosEquivalente.Enabled = False
        Me.TxtOtrosCargosEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtOtrosCargosEquivalente.ForeColor = System.Drawing.Color.Blue
        Me.TxtOtrosCargosEquivalente.Location = New System.Drawing.Point(248, 116)
        Me.TxtOtrosCargosEquivalente.Name = "TxtOtrosCargosEquivalente"
        Me.TxtOtrosCargosEquivalente.ReadOnly = True
        Me.TxtOtrosCargosEquivalente.Size = New System.Drawing.Size(84, 13)
        Me.TxtOtrosCargosEquivalente.TabIndex = 14
        Me.TxtOtrosCargosEquivalente.Text = "0.00"
        Me.TxtOtrosCargosEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtOtrosCargosEquivalente.Visible = False
        '
        'TxtFleteEquivalente
        '
        Me.TxtFleteEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtFleteEquivalente.Enabled = False
        Me.TxtFleteEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtFleteEquivalente.ForeColor = System.Drawing.Color.Blue
        Me.TxtFleteEquivalente.Location = New System.Drawing.Point(248, 96)
        Me.TxtFleteEquivalente.Name = "TxtFleteEquivalente"
        Me.TxtFleteEquivalente.ReadOnly = True
        Me.TxtFleteEquivalente.Size = New System.Drawing.Size(84, 13)
        Me.TxtFleteEquivalente.TabIndex = 11
        Me.TxtFleteEquivalente.Text = "0.00"
        Me.TxtFleteEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtFleteEquivalente.Visible = False
        '
        'TextBoxValorMonedaEnCosto
        '
        Me.TextBoxValorMonedaEnCosto.AcceptsTab = True
        Me.TextBoxValorMonedaEnCosto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxValorMonedaEnCosto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Moneda.ValorCompra", True))
        Me.TextBoxValorMonedaEnCosto.Enabled = False
        Me.TextBoxValorMonedaEnCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TextBoxValorMonedaEnCosto.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxValorMonedaEnCosto.Location = New System.Drawing.Point(252, 10)
        Me.TextBoxValorMonedaEnCosto.Name = "TextBoxValorMonedaEnCosto"
        Me.TextBoxValorMonedaEnCosto.ReadOnly = True
        Me.TextBoxValorMonedaEnCosto.Size = New System.Drawing.Size(80, 13)
        Me.TextBoxValorMonedaEnCosto.TabIndex = 3
        Me.TextBoxValorMonedaEnCosto.Text = "0.00"
        Me.TextBoxValorMonedaEnCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtBaseEquivalente
        '
        Me.TxtBaseEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtBaseEquivalente.Enabled = False
        Me.TxtBaseEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtBaseEquivalente.ForeColor = System.Drawing.Color.Blue
        Me.TxtBaseEquivalente.Location = New System.Drawing.Point(208, 74)
        Me.TxtBaseEquivalente.Name = "TxtBaseEquivalente"
        Me.TxtBaseEquivalente.ReadOnly = True
        Me.TxtBaseEquivalente.Size = New System.Drawing.Size(84, 13)
        Me.TxtBaseEquivalente.TabIndex = 8
        Me.TxtBaseEquivalente.Text = "0.00"
        Me.TxtBaseEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtBaseEquivalente.Visible = False
        '
        'TxtImpuesto
        '
        Me.TxtImpuesto.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.IVenta", True))
        Me.TxtImpuesto.EditValue = "0.00"
        Me.TxtImpuesto.Location = New System.Drawing.Point(112, 31)
        Me.TxtImpuesto.Name = "TxtImpuesto"
        '
        '
        '
        Me.TxtImpuesto.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtImpuesto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtImpuesto.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtImpuesto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtImpuesto.Properties.ReadOnly = True
        Me.TxtImpuesto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtImpuesto.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtImpuesto.Size = New System.Drawing.Size(80, 19)
        Me.TxtImpuesto.TabIndex = 5
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(14, 7)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(92, 16)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Moneda"
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(8, 110)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(98, 12)
        Me.Label18.TabIndex = 15
        Me.Label18.Text = "Costo"
        '
        'TxtFlete
        '
        Me.TxtFlete.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Fletes", True))
        Me.TxtFlete.EditValue = "0.00"
        Me.TxtFlete.Location = New System.Drawing.Point(112, 71)
        Me.TxtFlete.Name = "TxtFlete"
        '
        '
        '
        Me.TxtFlete.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtFlete.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtFlete.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtFlete.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtFlete.Size = New System.Drawing.Size(80, 19)
        Me.TxtFlete.TabIndex = 10
        '
        'TxtOtros
        '
        Me.TxtOtros.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.OtrosCargos", True))
        Me.TxtOtros.EditValue = "0.00"
        Me.TxtOtros.Location = New System.Drawing.Point(112, 90)
        Me.TxtOtros.Name = "TxtOtros"
        '
        '
        '
        Me.TxtOtros.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtOtros.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtOtros.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtOtros.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtOtros.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtOtros.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtOtros.Size = New System.Drawing.Size(80, 19)
        Me.TxtOtros.TabIndex = 13
        '
        'TxtCosto
        '
        Me.TxtCosto.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Costo", True))
        Me.TxtCosto.EditValue = "0.00"
        Me.TxtCosto.Location = New System.Drawing.Point(112, 110)
        Me.TxtCosto.Name = "TxtCosto"
        '
        '
        '
        Me.TxtCosto.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtCosto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtCosto.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtCosto.Properties.Enabled = False
        Me.TxtCosto.Properties.ReadOnly = True
        Me.TxtCosto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtCosto.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtCosto.Size = New System.Drawing.Size(80, 19)
        Me.TxtCosto.TabIndex = 16
        '
        'ComboBoxMoneda
        '
        Me.ComboBoxMoneda.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.MonedaCosto", True))
        Me.ComboBoxMoneda.DataSource = Me.DataSetInventario
        Me.ComboBoxMoneda.DisplayMember = "Moneda.MonedaNombre"
        Me.ComboBoxMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxMoneda.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxMoneda.ItemHeight = 13
        Me.ComboBoxMoneda.Location = New System.Drawing.Point(112, 7)
        Me.ComboBoxMoneda.Name = "ComboBoxMoneda"
        Me.ComboBoxMoneda.Size = New System.Drawing.Size(84, 21)
        Me.ComboBoxMoneda.TabIndex = 1
        Me.ComboBoxMoneda.ValueMember = "Moneda.CodMoneda"
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(8, 52)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(98, 19)
        Me.Label17.TabIndex = 6
        Me.Label17.Text = "Base"
        '
        'TxtBase
        '
        Me.TxtBase.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.PrecioBase", True))
        Me.TxtBase.EditValue = "0.00"
        Me.TxtBase.Location = New System.Drawing.Point(112, 52)
        Me.TxtBase.Name = "TxtBase"
        '
        '
        '
        Me.TxtBase.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtBase.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtBase.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtBase.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtBase.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtBase.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtBase.Size = New System.Drawing.Size(80, 19)
        Me.TxtBase.TabIndex = 7
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(8, 71)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(98, 13)
        Me.Label16.TabIndex = 9
        Me.Label16.Text = "Fletes"
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label15.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 90)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(98, 17)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Otro Cargo"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Ck_Lote)
        Me.TabPage4.Controls.Add(Me.Ck_PreguntaPrecio)
        Me.TabPage4.Controls.Add(Me.Check_Inhabilitado)
        Me.TabPage4.Controls.Add(Me.Check_Servicio)
        Me.TabPage4.Controls.Add(Me.Label34)
        Me.TabPage4.Controls.Add(Me.Hasta)
        Me.TabPage4.Controls.Add(Me.Label33)
        Me.TabPage4.Controls.Add(Me.Check_Promo)
        Me.TabPage4.Controls.Add(Me.Desde)
        Me.TabPage4.Controls.Add(Me.Label37)
        Me.TabPage4.Controls.Add(Me.TxtMaxDesc)
        Me.TabPage4.Controls.Add(Me.Label38)
        Me.TabPage4.Controls.Add(Me.Label39)
        Me.TabPage4.Controls.Add(Me.TextBox4)
        Me.TabPage4.Controls.Add(Me.Label40)
        Me.TabPage4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(425, 180)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Varios"
        '
        'Ck_Lote
        '
        Me.Ck_Lote.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Lote", True))
        Me.Ck_Lote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ck_Lote.Location = New System.Drawing.Point(216, 72)
        Me.Ck_Lote.Name = "Ck_Lote"
        Me.Ck_Lote.Size = New System.Drawing.Size(112, 16)
        Me.Ck_Lote.TabIndex = 86
        Me.Ck_Lote.Text = "Lote"
        '
        'Ck_PreguntaPrecio
        '
        Me.Ck_PreguntaPrecio.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.PreguntaPrecio", True))
        Me.Ck_PreguntaPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ck_PreguntaPrecio.Location = New System.Drawing.Point(216, 88)
        Me.Ck_PreguntaPrecio.Name = "Ck_PreguntaPrecio"
        Me.Ck_PreguntaPrecio.Size = New System.Drawing.Size(112, 16)
        Me.Ck_PreguntaPrecio.TabIndex = 85
        Me.Ck_PreguntaPrecio.Text = "Requiere Precio"
        '
        'Check_Inhabilitado
        '
        Me.Check_Inhabilitado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Inhabilitado", True))
        Me.Check_Inhabilitado.Enabled = False
        Me.Check_Inhabilitado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Inhabilitado.Location = New System.Drawing.Point(216, 120)
        Me.Check_Inhabilitado.Name = "Check_Inhabilitado"
        Me.Check_Inhabilitado.Size = New System.Drawing.Size(88, 16)
        Me.Check_Inhabilitado.TabIndex = 84
        Me.Check_Inhabilitado.Text = "Inhabilitado"
        '
        'Check_Servicio
        '
        Me.Check_Servicio.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Servicio", True))
        Me.Check_Servicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Servicio.Location = New System.Drawing.Point(216, 104)
        Me.Check_Servicio.Name = "Check_Servicio"
        Me.Check_Servicio.Size = New System.Drawing.Size(88, 16)
        Me.Check_Servicio.TabIndex = 83
        Me.Check_Servicio.Text = "Servicio"
        '
        'Label34
        '
        Me.Label34.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label34.Enabled = False
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label34.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label34.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label34.Location = New System.Drawing.Point(169, 32)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(41, 12)
        Me.Label34.TabIndex = 76
        Me.Label34.Text = "Hasta"
        '
        'Hasta
        '
        Me.Hasta.CalendarForeColor = System.Drawing.Color.RoyalBlue
        Me.Hasta.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Promo_Finaliza", True))
        Me.Hasta.Enabled = False
        Me.Hasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Hasta.Location = New System.Drawing.Point(216, 30)
        Me.Hasta.Name = "Hasta"
        Me.Hasta.Size = New System.Drawing.Size(88, 20)
        Me.Hasta.TabIndex = 75
        Me.Hasta.Value = New Date(2010, 1, 18, 11, 52, 19, 600)
        '
        'Label33
        '
        Me.Label33.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label33.Enabled = False
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label33.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label33.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label33.Location = New System.Drawing.Point(168, 8)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(41, 12)
        Me.Label33.TabIndex = 74
        Me.Label33.Text = "Desde"
        '
        'Check_Promo
        '
        Me.Check_Promo.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Promo_Activa", True))
        Me.Check_Promo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Promo.Location = New System.Drawing.Point(48, 16)
        Me.Check_Promo.Name = "Check_Promo"
        Me.Check_Promo.Size = New System.Drawing.Size(108, 20)
        Me.Check_Promo.TabIndex = 72
        Me.Check_Promo.Text = "Promo Activa"
        '
        'Desde
        '
        Me.Desde.CalendarForeColor = System.Drawing.Color.RoyalBlue
        Me.Desde.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Promo_Inicio", True))
        Me.Desde.Enabled = False
        Me.Desde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Desde.Location = New System.Drawing.Point(216, 8)
        Me.Desde.Name = "Desde"
        Me.Desde.Size = New System.Drawing.Size(88, 20)
        Me.Desde.TabIndex = 73
        Me.Desde.Value = New Date(2010, 1, 18, 11, 52, 19, 640)
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label37.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label37.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label37.Location = New System.Drawing.Point(16, 64)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(104, 12)
        Me.Label37.TabIndex = 77
        Me.Label37.Text = "M�ximo Descuento"
        '
        'TxtMaxDesc
        '
        Me.TxtMaxDesc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMaxDesc.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Max_Descuento", True))
        Me.TxtMaxDesc.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtMaxDesc.Location = New System.Drawing.Point(136, 64)
        Me.TxtMaxDesc.Name = "TxtMaxDesc"
        Me.TxtMaxDesc.Size = New System.Drawing.Size(28, 13)
        Me.TxtMaxDesc.TabIndex = 78
        '
        'Label38
        '
        Me.Label38.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label38.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label38.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label38.Location = New System.Drawing.Point(168, 64)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(16, 12)
        Me.Label38.TabIndex = 79
        Me.Label38.Text = "%"
        '
        'Label39
        '
        Me.Label39.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label39.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label39.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label39.Location = New System.Drawing.Point(168, 80)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(16, 12)
        Me.Label39.TabIndex = 82
        Me.Label39.Text = "%"
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Max_Comision", True))
        Me.TextBox4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TextBox4.Location = New System.Drawing.Point(136, 80)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(28, 13)
        Me.TextBox4.TabIndex = 81
        '
        'Label40
        '
        Me.Label40.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label40.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label40.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label40.Location = New System.Drawing.Point(16, 80)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(104, 12)
        Me.Label40.TabIndex = 80
        Me.Label40.Text = "M�ximo Comisi�n"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GridControl1)
        Me.TabPage2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(425, 180)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Detalle Art�culos por Proveedor"
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "Inventario.InventarioArticulos_x0020_x_x0020_Proveedor"
        Me.GridControl1.DataSource = Me.DataSetInventario
        '
        '
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(328, 144)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.Text = "GridControl1"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.GridView1.GroupPanelText = ""
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Fecha"
        Me.GridColumn6.DisplayFormat.FormatString = "d"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn6.FieldName = "FechaUltimaCompra"
        Me.GridColumn6.FilterInfo = ColumnFilterInfo5
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.Width = 62
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Id P."
        Me.GridColumn7.FieldName = "CodigoProveedor"
        Me.GridColumn7.FilterInfo = ColumnFilterInfo6
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn7.Width = 34
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Proveedor"
        Me.GridColumn8.FieldName = "Nombre"
        Me.GridColumn8.FilterInfo = ColumnFilterInfo7
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn8.VisibleIndex = 1
        Me.GridColumn8.Width = 111
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "M"
        Me.GridColumn9.FieldName = "Simbolo"
        Me.GridColumn9.FilterInfo = ColumnFilterInfo8
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn9.VisibleIndex = 2
        Me.GridColumn9.Width = 20
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Costo"
        Me.GridColumn10.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn10.FieldName = "UltimoCosto"
        Me.GridColumn10.FilterInfo = ColumnFilterInfo9
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn10.VisibleIndex = 0
        Me.GridColumn10.Width = 87
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.btnPegar)
        Me.TabPage3.Controls.Add(Me.Button1)
        Me.TabPage3.Controls.Add(Me.PictureBox1)
        Me.TabPage3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(425, 180)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Imagen"
        '
        'btnPegar
        '
        Me.btnPegar.ForeColor = System.Drawing.Color.Black
        Me.btnPegar.Location = New System.Drawing.Point(267, 94)
        Me.btnPegar.Name = "btnPegar"
        Me.btnPegar.Size = New System.Drawing.Size(75, 51)
        Me.btnPegar.TabIndex = 75
        Me.btnPegar.Text = "Pegar"
        Me.btnPegar.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(267, 22)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 51)
        Me.Button1.TabIndex = 74
        Me.Button1.Text = "Agregar Imagen"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.DataSetInventario, "Inventario.Imagen", True))
        Me.PictureBox1.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(256, 151)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 73
        Me.PictureBox1.TabStop = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.Tabequivalencia)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabLotes)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabControl1.ItemSize = New System.Drawing.Size(71, 18)
        Me.TabControl1.Location = New System.Drawing.Point(0, 152)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(433, 206)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.GridControl2)
        Me.TabPage5.Controls.Add(Me.ButtonAgreBodega)
        Me.TabPage5.Controls.Add(Me.ComboBoxBodega)
        Me.TabPage5.Controls.Add(Me.ButtonAgregar)
        Me.TabPage5.Controls.Add(Me.ButtonEliminar)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(425, 180)
        Me.TabPage5.TabIndex = 6
        Me.TabPage5.Text = "Articulos Bodega"
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "Inventario.InventarioArticulosXBodega"
        Me.GridControl2.DataSource = Me.DataSetInventario
        '
        '
        '
        Me.GridControl2.EmbeddedNavigator.Name = ""
        Me.GridControl2.Location = New System.Drawing.Point(-1, 36)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(332, 124)
        Me.GridControl2.TabIndex = 10
        Me.GridControl2.Text = "GridControl2"
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Nombre"
        Me.GridColumn4.FieldName = "Nombre"
        Me.GridColumn4.FilterInfo = ColumnFilterInfo1
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.VisibleIndex = 0
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Existencia"
        Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn5.FieldName = "Existencia"
        Me.GridColumn5.FilterInfo = ColumnFilterInfo2
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.VisibleIndex = 1
        '
        'ButtonAgreBodega
        '
        Me.ButtonAgreBodega.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgreBodega.Location = New System.Drawing.Point(272, 4)
        Me.ButtonAgreBodega.Name = "ButtonAgreBodega"
        Me.ButtonAgreBodega.Size = New System.Drawing.Size(24, 16)
        Me.ButtonAgreBodega.TabIndex = 9
        Me.ButtonAgreBodega.Text = "+"
        Me.ButtonAgreBodega.Visible = False
        '
        'ComboBoxBodega
        '
        Me.ComboBoxBodega.DataSource = Me.DataSetInventario.Bodega
        Me.ComboBoxBodega.DisplayMember = "Nombre"
        Me.ComboBoxBodega.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBodega.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxBodega.Location = New System.Drawing.Point(88, 4)
        Me.ComboBoxBodega.Name = "ComboBoxBodega"
        Me.ComboBoxBodega.Size = New System.Drawing.Size(184, 21)
        Me.ComboBoxBodega.TabIndex = 8
        Me.ComboBoxBodega.ValueMember = "Id"
        Me.ComboBoxBodega.Visible = False
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgregar.Location = New System.Drawing.Point(24, 4)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(24, 16)
        Me.ButtonAgregar.TabIndex = 7
        Me.ButtonAgregar.Text = "+"
        '
        'ButtonEliminar
        '
        Me.ButtonEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminar.ForeColor = System.Drawing.Color.Crimson
        Me.ButtonEliminar.Location = New System.Drawing.Point(56, 4)
        Me.ButtonEliminar.Name = "ButtonEliminar"
        Me.ButtonEliminar.Size = New System.Drawing.Size(24, 16)
        Me.ButtonEliminar.TabIndex = 6
        Me.ButtonEliminar.Text = "X"
        Me.ButtonEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.TxtEquivalen)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(425, 180)
        Me.TabPage6.TabIndex = 8
        Me.TabPage6.Text = "Equivalencia 2"
        '
        'TxtEquivalen
        '
        Me.TxtEquivalen.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Equivalencia", True))
        Me.TxtEquivalen.Location = New System.Drawing.Point(8, 14)
        Me.TxtEquivalen.MaxLength = 300
        Me.TxtEquivalen.Multiline = True
        Me.TxtEquivalen.Name = "TxtEquivalen"
        Me.TxtEquivalen.Size = New System.Drawing.Size(312, 130)
        Me.TxtEquivalen.TabIndex = 0
        '
        'Tabequivalencia
        '
        Me.Tabequivalencia.Controls.Add(Me.Label2)
        Me.Tabequivalencia.Controls.Add(Me.txtEquivalencia)
        Me.Tabequivalencia.Controls.Add(Me.ButtonAgregarEqui)
        Me.Tabequivalencia.Controls.Add(Me.ButtonEliminarEqui)
        Me.Tabequivalencia.Controls.Add(Me.GridControlEquivalencias)
        Me.Tabequivalencia.Controls.Add(Me.Label47)
        Me.Tabequivalencia.Location = New System.Drawing.Point(4, 22)
        Me.Tabequivalencia.Name = "Tabequivalencia"
        Me.Tabequivalencia.Size = New System.Drawing.Size(425, 180)
        Me.Tabequivalencia.TabIndex = 7
        Me.Tabequivalencia.Text = "Equivalencias"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(248, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 16)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "[F1]"
        '
        'txtEquivalencia
        '
        Me.txtEquivalencia.Location = New System.Drawing.Point(120, 13)
        Me.txtEquivalencia.Name = "txtEquivalencia"
        Me.txtEquivalencia.Size = New System.Drawing.Size(128, 20)
        Me.txtEquivalencia.TabIndex = 38
        '
        'ButtonAgregarEqui
        '
        Me.ButtonAgregarEqui.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgregarEqui.Location = New System.Drawing.Point(272, 16)
        Me.ButtonAgregarEqui.Name = "ButtonAgregarEqui"
        Me.ButtonAgregarEqui.Size = New System.Drawing.Size(24, 16)
        Me.ButtonAgregarEqui.TabIndex = 37
        Me.ButtonAgregarEqui.Text = "+"
        '
        'ButtonEliminarEqui
        '
        Me.ButtonEliminarEqui.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminarEqui.ForeColor = System.Drawing.Color.Crimson
        Me.ButtonEliminarEqui.Location = New System.Drawing.Point(304, 16)
        Me.ButtonEliminarEqui.Name = "ButtonEliminarEqui"
        Me.ButtonEliminarEqui.Size = New System.Drawing.Size(24, 16)
        Me.ButtonEliminarEqui.TabIndex = 36
        Me.ButtonEliminarEqui.Text = "X"
        Me.ButtonEliminarEqui.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GridControlEquivalencias
        '
        Me.GridControlEquivalencias.DataMember = "Inventario.InventarioEquivalencias"
        Me.GridControlEquivalencias.DataSource = Me.DataSetInventario
        '
        '
        '
        Me.GridControlEquivalencias.EmbeddedNavigator.Name = ""
        Me.GridControlEquivalencias.Location = New System.Drawing.Point(5, 45)
        Me.GridControlEquivalencias.MainView = Me.GridView4
        Me.GridControlEquivalencias.Name = "GridControlEquivalencias"
        Me.GridControlEquivalencias.Size = New System.Drawing.Size(320, 104)
        Me.GridControlEquivalencias.TabIndex = 35
        Me.GridControlEquivalencias.Text = "GridControl2"
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn11, Me.GridColumn12})
        Me.GridView4.GroupPanelText = ""
        Me.GridView4.Name = "GridView4"
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Codigo"
        Me.GridColumn11.FieldName = "Barras_Equi"
        Me.GridColumn11.FilterInfo = ColumnFilterInfo3
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn11.VisibleIndex = 0
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Description"
        Me.GridColumn12.FieldName = "Descripcion"
        Me.GridColumn12.FilterInfo = ColumnFilterInfo4
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn12.VisibleIndex = 1
        '
        'Label47
        '
        Me.Label47.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label47.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label47.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label47.Location = New System.Drawing.Point(5, 13)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(112, 16)
        Me.Label47.TabIndex = 39
        Me.Label47.Text = "Cod. Equivalencia :"
        '
        'TabLotes
        '
        Me.TabLotes.Controls.Add(Me.GridControl3)
        Me.TabLotes.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabLotes.Location = New System.Drawing.Point(4, 22)
        Me.TabLotes.Name = "TabLotes"
        Me.TabLotes.Size = New System.Drawing.Size(425, 180)
        Me.TabLotes.TabIndex = 5
        Me.TabLotes.Text = "Lotes"
        '
        'GridControl3
        '
        Me.GridControl3.DataMember = "Inventario.InventarioLotes"
        Me.GridControl3.DataSource = Me.DataSetInventario
        '
        '
        '
        Me.GridControl3.EmbeddedNavigator.Name = ""
        Me.GridControl3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridControl3.Location = New System.Drawing.Point(8, 8)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(320, 136)
        Me.GridControl3.TabIndex = 0
        '
        'GridView3
        '
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'colCodigoProveedor1
        '
        Me.colCodigoProveedor1.Caption = "Id P."
        Me.colCodigoProveedor1.FieldName = "CodigoProveedor"
        Me.colCodigoProveedor1.FilterInfo = ColumnFilterInfo13
        Me.colCodigoProveedor1.Name = "colCodigoProveedor1"
        Me.colCodigoProveedor1.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCodigoProveedor1.VisibleIndex = 1
        Me.colCodigoProveedor1.Width = 34
        '
        'colLote
        '
        Me.colLote.Caption = "Lote"
        Me.colLote.FieldName = "Numero"
        Me.colLote.FilterInfo = ColumnFilterInfo14
        Me.colLote.Name = "colLote"
        Me.colLote.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colLote.VisibleIndex = 0
        '
        'colCant_Actual
        '
        Me.colCant_Actual.Caption = "Existencia"
        Me.colCant_Actual.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colCant_Actual.FieldName = "Cant_Actual"
        Me.colCant_Actual.FilterInfo = ColumnFilterInfo15
        Me.colCant_Actual.Name = "colCant_Actual"
        Me.colCant_Actual.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCant_Actual.VisibleIndex = 2
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Existencia"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn3.FieldName = "Cant_Actual"
        Me.GridColumn3.FilterInfo = ColumnFilterInfo16
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Vencimiento"
        Me.GridColumn2.DisplayFormat.FormatString = "d"
        Me.GridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn2.FieldName = "Vencimiento"
        Me.GridColumn2.FilterInfo = ColumnFilterInfo17
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.VisibleIndex = 0
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Lote"
        Me.GridColumn1.FieldName = "Numero"
        Me.GridColumn1.FilterInfo = ColumnFilterInfo18
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'gbObservaciones
        '
        Me.gbObservaciones.Controls.Add(Me.TxtObservaciones)
        Me.gbObservaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbObservaciones.ForeColor = System.Drawing.Color.MidnightBlue
        Me.gbObservaciones.Location = New System.Drawing.Point(8, 410)
        Me.gbObservaciones.Name = "gbObservaciones"
        Me.gbObservaciones.Size = New System.Drawing.Size(691, 55)
        Me.gbObservaciones.TabIndex = 70
        Me.gbObservaciones.TabStop = False
        Me.gbObservaciones.Text = "Observaciones"
        '
        'lbBarra
        '
        Me.lbBarra.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbBarra.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(143, Byte), Integer))
        Me.lbBarra.Location = New System.Drawing.Point(-119, -3)
        Me.lbBarra.Name = "lbBarra"
        Me.lbBarra.Size = New System.Drawing.Size(950, 10)
        Me.lbBarra.TabIndex = 71
        '
        'FrmInventario
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(707, 519)
        Me.Controls.Add(Me.chControEntrega)
        Me.Controls.Add(Me.lbBarra)
        Me.Controls.Add(Me.gbObservaciones)
        Me.Controls.Add(Me.ckVerCodBarraInv)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.DataNavigator1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.Text_Cod_AUX)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.ForeColor = System.Drawing.Color.RoyalBlue
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmInventario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inventarios"
        CType(Me.DataSetInventario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_A.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_B.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_C.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_D.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.TxtUtilidad_D.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUtilidad_C.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUtilidad_B.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_D.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_A.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_C.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_B.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUtilidad_A.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUtilidad_P.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_P.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_P.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.txtPorcIvaGv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPorcDesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCostoCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtCostoPromedio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtImpuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtOtros.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtBase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.Tabequivalencia.ResumeLayout(False)
        Me.Tabequivalencia.PerformLayout()
        CType(Me.GridControlEquivalencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabLotes.ResumeLayout(False)
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbObservaciones.ResumeLayout(False)
        Me.gbObservaciones.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Position_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If Text_Cod_AUX.Text = "" Then Exit Sub
            cargando = True
            If LlenarInventario(CDbl(Text_Cod_AUX.Text)) Then
                cargando = False
                EjecutarCalculos()
                'ActualizaCampo("TxtBase")
            End If
            Application.DoEvents()
            TxtBarras.Focus()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

#Region " Variable "
    '  Dim Salto As Boolean
    Dim cargando As Boolean

#End Region

    Private Sub CargarAdaptadores()
        Try
            Adaptador_Inventraio_AUX.Fill(DataSetInventario, "Inventario2")
            AdapterMarcas.Fill(DataSetInventario, "Marcas")
            AdapterCasaConsignante.Fill(DataSetInventario, "Bodegas")
            AdapterAUbicacion.Fill(DataSetInventario, "Sububicacion")
            AdapterFamilia.Fill(DataSetInventario, "SubFamilias")
            AdapterMoneda.Fill(DataSetInventario, "Moneda")
            AdapterPresentacion.Fill(DataSetInventario, "Presentaciones")
            'AdapterEquivalencias.Fill(DataSetInventario, "Equivalencias")
            daProveedores.Fill(DataSetInventario, "Proveedores")
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            PMU = VSM(usua.Cedula, Name)
            ComboBoxFamilia.Enabled = PMU.Others
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            CargarAdaptadores()
            DataSetInventario.Inventario.BarrasColumn.DefaultValue = ""
            If DataSetInventario.Marcas.Rows.Count = 0 Then
                MsgBox("�Debe Registrar al menos un Familia en el Catalogo de Familias!", MsgBoxStyle.OkOnly, "�Atenci�n por Favor!")
                Return
            Else
                DataSetInventario.Inventario.SubFamiliaColumn.DefaultValue = DataSetInventario.SubFamilias.Rows(0).Item(0)
            End If


            If DataSetInventario.Marcas.Rows.Count = 0 Then
                MsgBox("�Debe Registrar al menos una Ubicaci�n en el Catalogo de Ubicaciones!", MsgBoxStyle.OkOnly, "�Atenci�n por Favor!")
                Return
            Else
                DataSetInventario.Inventario.SubUbicacionColumn.DefaultValue = DataSetInventario.SubUbicacion.Rows(0).Item(0)
            End If

            If DataSetInventario.Marcas.Rows.Count = 0 Then
                MsgBox("�Debe Registrar al menos una Marca en el Catalogo de Marcas!", MsgBoxStyle.OkOnly, "�Atenci�n por Favor!")

                Return
            Else
                DataSetInventario.Inventario.CodMarcaColumn.DefaultValue = DataSetInventario.Marcas.Rows(0).Item(0)
            End If
            DataSetInventario.Inventario.MinimaColumn.DefaultValue = 0
            DataSetInventario.Inventario.MaximaColumn.DefaultValue = 0
            DataSetInventario.Inventario.PuntoMedioColumn.DefaultValue = 0
            'DataSetInventario.Inventario.ConsignacionColumn.DefaultValue = 0
            'DataSetInventario.Inventario.BodegaConsignacionColumn.DefaultValue = 0
            DataSetInventario.Inventario.PrecioBaseColumn.DefaultValue = 0
            DataSetInventario.Inventario.OtrosCargosColumn.DefaultValue = 0
            DataSetInventario.Inventario.FletesColumn.DefaultValue = 0
            DataSetInventario.Inventario.CostoColumn.DefaultValue = 0
            DataSetInventario.Inventario.Precio_AColumn.DefaultValue = 0
            DataSetInventario.Inventario.Precio_BColumn.DefaultValue = 0
            DataSetInventario.Inventario.Precio_CColumn.DefaultValue = 0
            DataSetInventario.Inventario.Precio_DColumn.DefaultValue = 0
            DataSetInventario.Inventario.IVentaColumn.DefaultValue = 0
            DataSetInventario.Inventario.ObservacionesColumn.DefaultValue = ""
            DataSetInventario.Inventario.Promo_ActivaColumn.DefaultValue = False
            DataSetInventario.Inventario.Precio_PromoColumn.DefaultValue = 0
            DataSetInventario.Inventario.Promo_InicioColumn.DefaultValue = Now
            DataSetInventario.Inventario.Promo_FinalizaColumn.DefaultValue = Now
            DataSetInventario.Inventario.Max_DescuentoColumn.DefaultValue = 100
            DataSetInventario.Inventario.Max_ComisionColumn.DefaultValue = 0
            DataSetInventario.Inventario.ServicioColumn.DefaultValue = False
            DataSetInventario.Inventario.InhabilitadoColumn.DefaultValue = False
            DataSetInventario.Inventario.ProveedorColumn.DefaultValue = 0
            DataSetInventario.Inventario.Precio_SugeridoColumn.DefaultValue = 0
            DataSetInventario.Inventario.SugeridoIVColumn.DefaultValue = 0
            DataSetInventario.Inventario.PreguntaPrecioColumn.DefaultValue = 0
            DataSetInventario.Inventario.LoteColumn.DefaultValue = False
            DataSetInventario.Inventario.ConsignacionColumn.DefaultValue = False
            DataSetInventario.Inventario.Id_BodegaColumn.DefaultValue = 0
            DataSetInventario.Inventario.ExistenciaBodegaColumn.DefaultValue = 0
            DataSetInventario.Inventario.AlternoColumn.DefaultValue = 0
            DataSetInventario.Inventario.MedidasColumn.DefaultValue = 0
            DataSetInventario.Inventario.ControlEntregaColumn.DefaultValue = True
            TxtMaxDesc.Text = "100"
            'CenterToScreen()

            TxtImpuesto.Properties.ReadOnly = True

            If PMU.Others Then
                TxtUtilidad_A.Visible = True
                TxtUtilidad_B.Visible = True
                TxtUtilidad_C.Visible = True
                TxtUtilidad_D.Visible = True
                Label29.Visible = True
            Else
                'TxtUtilidad_A.Visible = False
                'TxtUtilidad_B.Visible = False
                'TxtUtilidad_C.Visible = False
                'TxtUtilidad_D.Visible = False
                'Label29.Visible = False
                TabPage1.Enabled = False
                'TxtBarras.Enabled = False
            End If

            If Text_Cod_AUX.Text = "" Then Exit Sub
            cargando = True
            If LlenarInventario(CDbl(Text_Cod_AUX.Text)) Then
                cargando = False
                EjecutarCalculos()
            End If

            'SAJ 041207 VERIFICA SI SE DEBE DE CHECHEAR EL CODIGO DE BARRAS 
            '      If GetSetting("SeeSOFT", "SeePOS", "VerCodBarraInv") = "" Then
            SaveSetting("SeeSOFT", "SeePOS", "VerCodBarraInv", "1")
            '      End If
            ckVerCodBarraInv.Checked = True
            ckVerCodBarraInv.Visible = False

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
    'Public Sub ActualizaCampo(ByVal nombre As String)
    '    Select Case nombre
    '        Case TxtBase.Name, TxtFlete.Name, TxtOtros.Name
    '            If TxtBase.Text = "" Then TxtBase.Text = 0
    '            If TxtFlete.Text = "" Then TxtFlete.Text = 0
    '            If TxtOtros.Text = "" Then TxtOtros.Text = 0
    '            ' TxtCosto.Text = (CDbl(TxtBase.Text) + CDbl(TxtFlete.Text) + CDbl(TxtOtros.Text))
    '            CalcularEquivalenciaMoneda()
    '            'calculo de precios por la utilidad
    '        Case TxtUtilidad_A.Name
    '            If TxtUtilidad_A.Text = "" Then TxtUtilidad_A.Text = 0
    '            TxtPrecioVenta_A.Text = TxtBaseEquivalente.Text * (TxtUtilidad_A.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
    '            TxtPrecioVenta_IV_A.Text = TxtPrecioVenta_A.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_A.Text

    '        Case TxtUtilidad_B.Name
    '            If TxtUtilidad_B.Text = "" Then TxtUtilidad_B.Text = 0
    '            TxtPrecioVenta_B.Text = TxtBaseEquivalente.Text * (TxtUtilidad_B.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
    '            TxtPrecioVenta_IV_B.Text = TxtPrecioVenta_B.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_B.Text

    '        Case TxtUtilidad_C.Name
    '            If TxtUtilidad_C.Text = "" Then TxtUtilidad_C.Text = 0
    '            TxtPrecioVenta_C.Text = TxtBaseEquivalente.Text * (TxtUtilidad_C.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
    '            TxtPrecioVenta_IV_C.Text = TxtPrecioVenta_C.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_C.Text

    '        Case TxtUtilidad_D.Name
    '            If TxtUtilidad_D.Text = "" Then TxtUtilidad_D.Text = 0
    '            TxtPrecioVenta_D.Text = TxtBaseEquivalente.Text * (TxtUtilidad_D.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
    '            TxtPrecioVenta_IV_D.Text = TxtPrecioVenta_D.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_D.Text

    '        Case TxtUtilidad_P.Name
    '            If TxtUtilidad_P.Text = "" Then TxtUtilidad_P.Text = 0
    '            TxtPrecioVenta_P.Text = TxtBaseEquivalente.Text * (TxtUtilidad_P.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
    '            TxtPrecioVenta_IV_P.Text = TxtPrecioVenta_P.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_P.Text


    '            'calculo de de utilidad por el precio A y calculo del precio com I.V.
    '        Case TxtPrecioVenta_A.Name
    '            If TxtPrecioVenta_A.Text = "" Then TxtPrecioVenta_A.Text = 0
    '            TxtUtilidad_A.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_A.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
    '            TxtPrecioVenta_IV_A.Text = TxtPrecioVenta_A.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_A.Text

    '        Case TxtPrecioVenta_B.Name
    '            If TxtPrecioVenta_B.Text = "" Then TxtPrecioVenta_B.Text = 0
    '            TxtUtilidad_B.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_B.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
    '            TxtPrecioVenta_IV_B.Text = TxtPrecioVenta_B.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_B.Text

    '        Case TxtPrecioVenta_C.Name
    '            If TxtPrecioVenta_C.Text = "" Then TxtPrecioVenta_C.Text = 0
    '            TxtUtilidad_C.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_C.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
    '            TxtPrecioVenta_IV_C.Text = TxtPrecioVenta_C.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_C.Text

    '        Case TxtPrecioVenta_D.Name
    '            If TxtPrecioVenta_D.Text = "" Then TxtPrecioVenta_D.Text = 0
    '            TxtUtilidad_D.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_D.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
    '            TxtPrecioVenta_IV_D.Text = TxtPrecioVenta_D.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_D.Text

    '        Case TxtPrecioVenta_P.Name
    '            If TxtPrecioVenta_P.Text = "" Then TxtPrecioVenta_P.Text = 0
    '            TxtUtilidad_P.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_P.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
    '            TxtPrecioVenta_IV_P.Text = TxtPrecioVenta_P.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_P.Text

    '            'CALCULO DE PRECIO DE VENTA Y RECALCULAR LA  UTILIDAD CON BASE A LOS NUEVOS PRECIOS 
    '        Case TxtPrecioVenta_IV_A.Name
    '            If TxtPrecioVenta_IV_A.Text = "" Then TxtPrecioVenta_IV_A.Text = 0
    '            TxtPrecioVenta_A.Text = TxtPrecioVenta_IV_A.Text / (1 + (TxtImpuesto.Text / 100))
    '            TxtUtilidad_A.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_A.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

    '        Case TxtPrecioVenta_IV_B.Name
    '            If TxtPrecioVenta_IV_B.Text = "" Then TxtPrecioVenta_IV_B.Text = 0
    '            TxtPrecioVenta_B.Text = TxtPrecioVenta_IV_B.Text / (1 + (TxtImpuesto.Text / 100))
    '            TxtUtilidad_B.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_B.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

    '        Case TxtPrecioVenta_IV_C.Name
    '            If TxtPrecioVenta_IV_C.Text = "" Then TxtPrecioVenta_IV_C.Text = 0
    '            TxtPrecioVenta_C.Text = TxtPrecioVenta_IV_C.Text / (1 + (TxtImpuesto.Text / 100))
    '            TxtUtilidad_C.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_C.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

    '        Case TxtPrecioVenta_IV_D.Name
    '            If TxtPrecioVenta_IV_D.Text = "" Then TxtPrecioVenta_IV_D.Text = 0
    '            TxtPrecioVenta_D.Text = TxtPrecioVenta_IV_D.Text / (1 + (TxtImpuesto.Text / 100))
    '            TxtUtilidad_D.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_D.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

    '        Case TxtPrecioVenta_IV_P.Name
    '            If TxtPrecioVenta_IV_P.Text = "" Then TxtPrecioVenta_IV_P.Text = 0
    '            TxtPrecioVenta_P.Text = TxtPrecioVenta_IV_P.Text / (1 + (TxtImpuesto.Text / 100))
    '            TxtUtilidad_P.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_P.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
    '    End Select


    'End Sub


    Private Sub TextBox1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCodigo.KeyPress, TxtCanPresentacion.KeyPress, TxtMed.KeyPress, TxtMax.KeyPress, TxtPrecioVenta_A.KeyPress, TxtPrecioVenta_B.KeyPress, TxtPrecioVenta_C.KeyPress, TxtPrecioVenta_D.KeyPress, TxtUtilidad_D.KeyPress, TxtUtilidad_A.KeyPress, TxtUtilidad_C.KeyPress, TxtUtilidad_B.KeyPress, TxtPrecioVenta_IV_D.KeyPress, TxtPrecioVenta_IV_A.KeyPress, TxtPrecioVenta_IV_C.KeyPress, TxtPrecioVenta_IV_B.KeyPress, TxtExistencia.KeyPress, TxtUtilidad_P.KeyPress, TxtPrecioVenta_P.KeyPress, TxtPrecioVenta_IV_P.KeyPress, TxtMin.KeyPress, txtPorcIvaGv.KeyPress, TxtFlete.KeyPress, TxtOtros.KeyPress, TxtCosto.KeyPress, ComboBoxMoneda.KeyPress, TxtBase.KeyPress
        'REALIZA LOS CALCULOS PARA LAS DIFERENTES CAJAS DE TEXTO DEPENDIENDO DEL OBJETO ENTRANTE. SAJ 
        Try
            ' If cargando = True Then Exit Sub

            If Keys.Enter = Asc(e.KeyChar) Then
                'ActualizaCampo(sender.name)
                calculaMontos(sender)
            End If
            ' esto invalida la tecla pulsada CUANDO NO ES NUMERICO SAJ:190906
            If Not IsNumeric(sender.text & e.KeyChar) Then If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then e.Handled = True

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function Utilidad(ByVal Costo As Double, ByVal Precio As Double) As Double
        Utilidad = IIf(Costo = 0, 0, Math.Round(((Precio / Costo) - 1) * 100, 5))
        Return Utilidad
    End Function

    Private Sub BuscarRegistros()
        Try
            If BindingContext(DataSetInventario, "Inventario").Count > 0 Then
                BindingContext(DataSetInventario, "Inventario").CancelCurrentEdit()
                BindingContext(DataSetInventario, "Inventario2").CancelCurrentEdit()
                ToolBar1.Buttons(0).Text = "Nuevo"
                ToolBar1.Buttons(0).ImageIndex = 0
                DataNavigator1.Enabled = True
            End If


            'Dim BuscarArt As New FrmBuscarArticuloPorSucursal(usua)
            'BuscarArt.StartPosition = FormStartPosition.CenterParent
            'BuscarArt.CheckBoxInHabilitados.Enabled = True
            'BuscarArt.ShowDialog()
            Dim BuscarArt As New FrmBuscarArticulo
            BuscarArt.StartPosition = FormStartPosition.CenterParent
            BuscarArt.CheckBoxInHabilitados.Enabled = True
            BuscarArt.ShowDialog()

            cargando = True
            If BuscarArt.Cancelado Then
                cargando = False
                Exit Sub
            End If

            Dim pos As Integer
            Dim vista As DataView
            ToolBarRegistrar.Text = "Actualizar"
            vista = DataSetInventario.Inventario2.DefaultView
            vista.Sort = "Codigo"
            pos = vista.Find(CDbl(BuscarArt.Codigo))
            BindingContext(DataSetInventario, "Inventario2").Position = pos

            If DataSetInventario.Inventario.Rows(0).Item("Inhabilitado") = False Then
                ToolBar1.Buttons(3).Text = "Inhabilitar"
            Else
                ToolBar1.Buttons(3).Text = "Habilitar"
            End If
            ToolBarEliminar.Enabled = True
            ToolBarImprimir.Enabled = True

            cargando = False
        Catch EX As SystemException
            MsgBox(EX.Message)
        End Try
        'CargarAdaptadores()
    End Sub

    Public Sub spBuscarRegistros(ByVal Codigo As Integer)
        Try
            If BindingContext(DataSetInventario, "Inventario").Count > 0 Then
                BindingContext(DataSetInventario, "Inventario").CancelCurrentEdit()
                BindingContext(DataSetInventario, "Inventario2").CancelCurrentEdit()
                ToolBar1.Buttons(0).Text = "Nuevo"
                ToolBar1.Buttons(0).ImageIndex = 0
                DataNavigator1.Enabled = True
            End If

            cargando = True

            Dim pos As Integer
            Dim vista As DataView
            ToolBarRegistrar.Text = "Actualizar"
            vista = DataSetInventario.Inventario2.DefaultView
            vista.Sort = "Codigo"
            pos = vista.Find(CDbl(Codigo))
            BindingContext(DataSetInventario, "Inventario2").Position = pos

            If DataSetInventario.Inventario.Rows(0).Item("Inhabilitado") = False Then
                ToolBar1.Buttons(3).Text = "Inhabilitar"
            Else
                ToolBar1.Buttons(3).Text = "Habilitar"
            End If
            ToolBarEliminar.Enabled = True
            ToolBarImprimir.Enabled = True

            cargando = False
        Catch EX As SystemException
            MsgBox(EX.Message)
        End Try
        'CargarAdaptadores()
    End Sub

    Function LlenarInventario(ByVal Id As Double) As Boolean

        Dim cnnv As SqlConnection = Nothing
        Dim dt As New DataTable
        '
        Try
            DataSetInventario.Lotes.Clear()
            DataSetInventario.Articulos_x_Proveedor.Clear()
            DataSetInventario.ArticulosXBodega.Clear()
            DataSetInventario.Equivalencias.Clear()
            DataSetInventario.Inventario.Clear()
            cFunciones.Cargar_Tabla_Generico(AdapterInventario, "Select * from Inventario where codigo =" & Id)
            AdapterInventario.Fill(DataSetInventario.Inventario)

            If DataSetInventario.Inventario.Rows.Count() = 0 Then
                MsgBox("No existe un art�culo con ese c�digo en el inventario", MsgBoxStyle.Information)
                Return False
            End If
            '''''''''LLENAR ARTICULOS X PROVEEDOR'''''''''''''''''''''''''''''''''''''''
            cFunciones.Cargar_Tabla_Generico(AdapterArticulosXproveedor, "SELECT CodigoArticulo,CodigoProveedor,FechaUltimaCompra,UltimoCosto, Moneda.Simbolo,Proveedores.Nombre  FROM [Articulos x Proveedor] INNER JOIN Moneda ON  [Articulos x Proveedor].Moneda = Moneda.CodMoneda INNER JOIN Proveedores ON [Articulos x Proveedor].CodigoProveedor = Proveedores.CodigoProv and CodigoArticulo =" & Id)
            AdapterArticulosXproveedor.Fill(DataSetInventario.Articulos_x_Proveedor)

            '''''''''LLENAR ARTICULOS X LOTE'''''''''''''''''''''''''''''''''''''''
            cFunciones.Cargar_Tabla_Generico(AdapterLote, "SELECT Id, Numero, Vencimiento, Cant_Actual, Cod_Articulo FROM Lotes WHERE Cod_Articulo =" & Id)
            AdapterLote.Fill(DataSetInventario.Lotes)
            '--------------------------------------------------------------------------

            '''''''''LLENAR ARTICULOS X BODEGA'''''''''''''''''''''''''''''''''''''''
            cFunciones.Llenar_Tabla_Generico("SELECT  ArticulosXBodega.Existencia as Existencia, Bodegas.Nombre_Bodega as Nombre, ArticulosXBodega.Codigo as Codigo,  ArticulosXBodega.Id as Id, Bodegas.ID_Bodega as IdBodega FROM ArticulosXBodega INNER JOIN  Bodegas ON ArticulosXBodega.IdBodega = Bodegas.ID_Bodega WHERE ArticulosXBodega.Codigo = " & Id, DataSetInventario.ArticulosXBodega, SqlConnection1.ConnectionString)

            cFunciones.Llenar_Tabla_Generico("SELECT dbo.Equivalencias.Id, dbo.Equivalencias.Codigo, dbo.Equivalencias.Barras, dbo.Equivalencias.Barras_Equi, dbo.Inventario.Descripcion,dbo.Equivalencias.Equivalente FROM dbo.Inventario INNER JOIN  dbo.Equivalencias ON dbo.Inventario.Codigo = dbo.Equivalencias.Equivalente WHERE dbo.Equivalencias.Codigo = " & Id, DataSetInventario.Equivalencias, SqlConnection1.ConnectionString)


            '''''''''''LLENAR ULTIMO COSTO COMPRA''''''''''''''''''''''''''''''''''''
            spCargarCostoUltimaCompra(Id)


            If Check_Inhabilitado.Checked = False Then
                ToolBar1.Buttons(3).Text = "Inhabilitar"
            Else
                ToolBar1.Buttons(3).Text = "Habilitar"
            End If
            'If PMU.Others Then
            '	CheckBox1.Enabled = True
            '	TxtImpuesto.Properties.ReadOnly = False
            'Else
            '	CheckBox1.Enabled = False
            '	TxtImpuesto.Properties.ReadOnly = True
            'End If
            Eliminar_Articulo_Validacion()
            Return True

        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atenci�n...")
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Function

    Private Sub spCargarCostoUltimaCompra(ByVal Id As Double)
        Try
            Dim dt As New DataTable
            Dim sql As String

            sql = "select top 1 round((Base + Monto_Flete + OtrosCargos),2) as CostoCompra, Descuento_P  from articulos_comprados where codigo = '" & Id & "' order by  Id_ArticuloComprados  desc "
            cFunciones.Llenar_Tabla_Generico(sql, dt)

            If dt.Rows.Count > 0 Then
                txtCostoCompra.Text = Format(dt.Rows(0).Item("CostoCompra"), "#,#0.00")
                txtPorcDesc.Text = Format(dt.Rows(0).Item("Descuento_P"), "#,#0.00")
            Else
                txtCostoCompra.Text = "0"
                txtPorcDesc.Text = "0"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        PMU = VSM(usua.Cedula, Name)  'Carga los privilegios del usuario con el modulo  

        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : NuevosDatos()
            Case 2 : If PMU.Find Then BuscarRegistros() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Update Then RegistrarDatos() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Delete Then Habilitar_Inhabilitar() Else MsgBox("No tiene permiso para desactivar items...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : If PMU.Delete Then Eliminar_Articulo() Else MsgBox("No tiene permiso para desactivar items...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : If MessageBox.Show("�Desea Cerrar el modulo actual..?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then Close()
        End Select
    End Sub

    Private Sub Eliminar_Articulo_Validacion()
        Dim Items(1) As Integer
        Dim Cx As New Conexion
        Items(0) = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT COUNT(Ventas_Detalle.Cantidad) FROM Ventas_Detalle INNER JOIN  Ventas ON Ventas_Detalle.Id_Factura = Ventas.Id WHERE (Ventas.Anulado = 0) and codigo = " & TxtCodigo.Text & "GROUP BY Ventas_Detalle.Codigo")
        Items(1) = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT COUNT(Cantidad) FROM articulos_comprados GROUP BY Codigo HAVING Codigo = " & TxtCodigo.Text)

        ToolBarEliminador.Enabled = IIf((Items(0) + Items(1)) > 0, False, True)
        Cx.DesConectar(Cx.sQlconexion)
    End Sub
    Private Sub Eliminar_Articulo()
        Dim Items(1) As Integer
        Dim Cx As New Conexion
        Items(0) = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT COUNT(Ventas_Detalle.Cantidad) FROM Ventas_Detalle INNER JOIN  Ventas ON Ventas_Detalle.Id_Factura = Ventas.Id WHERE (Ventas.Anulado = 0) and codigo = " & TxtCodigo.Text & "GROUP BY Ventas_Detalle.Codigo")
        Items(1) = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT COUNT(Cantidad) FROM articulos_comprados GROUP BY Codigo HAVING Codigo = " & TxtCodigo.Text)

        If Items(0) = 0 And Items(1) = 0 Then
            If MsgBox("Esta seguro que desea proceder con la ELIMINACION del art�culo seleccionado...", MsgBoxStyle.YesNo, "Atenci�n...") = MsgBoxResult.Yes Then
                BindingContext(DataSetInventario, "Inventario").RemoveAt(BindingContext(DataSetInventario, "Inventario").Position)
                BindingContext(DataSetInventario, "Inventario").EndCurrentEdit()
                AdapterInventario.Update(DataSetInventario, "Inventario")

                DataSetInventario.Inventario2.Clear()
                Adaptador_Inventraio_AUX.Fill(DataSetInventario, "Inventario2")

                MsgBox("Se ha eliminado el registro del inventario.", MsgBoxStyle.Information, "Atenci�n...")
                NuevoRegistros()
            End If
        Else
            MsgBox("Existen movimientos registrados para para el art�culo " & TxtDescripcion.Text & vbCrLf & "Cantidad de items Vendidos.. " & Items(0) & vbCrLf & "Cantidad de Items Comprados.. " & Items(1), MsgBoxStyle.Critical, "Atenci�n...")
        End If
        Cx.DesConectar(Cx.sQlconexion)
    End Sub
    Function Imprimir()
        Try
            ToolBar1.Buttons(4).Enabled = False
            Dim Articulo_reporte As New Reporte_Ficha_Articulo
            Articulo_reporte.SetParameterValue(0, CDbl(TxtCodigo.Text))
            CrystalReportsConexion.LoadShow(Articulo_reporte, Nothing)
            'Dim Visor As New frmVisorReportes
            'CrystalReportsConexion.LoadReportViewer(Visor.rptViewer, Articulo_reporte, False, SqlConnection1.ConnectionString)
            'Visor.rptViewer.ReportSource = Articulo_reporte
            'Visor.MdiParent = MdiParent
            'Visor.rptViewer.Visible = True
            'Visor.Show()


            ToolBar1.Buttons(4).Enabled = True
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function
    Public Sub NuevosDatos()

        If ToolBar1.Buttons(0).Text = "Nuevo" Then

            TabPage1.Enabled = True
            txtCostoCompra.Text = "0.00"
            txtPorcDesc.Text = "0.00"

            ToolBar1.Buttons(0).Text = "Cancelar"
            'SAJ 07122006 CAMBIO DE NOMBRE DE REGISTAR A ACTUALIZA SEGUN 
            ToolBarRegistrar.Text = "Registrar"
            ToolBar1.Buttons(0).ImageIndex = 8
            ToolBar1.Buttons(2).Enabled = True
            ToolBarEliminar.Enabled = True
            strNuevo = "1"
            Try
                DataSetInventario.Inventario.FechaIngresoColumn.DefaultValue = Now
                BindingContext(DataSetInventario, "Inventario").CancelCurrentEdit()
                BindingContext(DataSetInventario, "Inventario").EndCurrentEdit()
                BindingContext(DataSetInventario, "Inventario").AddNew()
                NuevoRegistros()
                'LFCHG 07122006
                'CONTROLAR LA NAVEGACION ENTRE REGISTROS
                DataNavigator1.Enabled = False
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        Else
            TabPage1.Enabled = False
            BindingContext(DataSetInventario, "Inventario").CancelCurrentEdit()
            spCargarCostoUltimaCompra(CDbl(BindingContext(DataSetInventario, "Inventario").Current("Codigo")))
            'LFCHG 07122006
            'CONTROLAR LA NAVEGACION ENTRE REGISTROS
            DataNavigator1.Enabled = False
            ToolBar1.Buttons(0).Text = "Nuevo"
            ToolBar1.Buttons(0).ImageIndex = 0
            ToolBar1.Buttons(2).Enabled = False
            DataNavigator1.Enabled = True
            ToolBarRegistrar.Text = "Actualizar"
            ToolBarRegistrar.Enabled = True
            ToolBarImprimir.Enabled = True
            strNuevo = ""
        End If
    End Sub

    'Private Sub ActualizarDatos()
    '    Try
    '        If BindingContext(DataSetInventario, "inventario").Count > 0 Then
    '            BindingContext(DataSetInventario, "inventario").EndCurrentEdit()
    '            MsgBox("Los datos se actualiazaron satisfactoriamente...", MsgBoxStyle.Information, "Atenci�n...")
    '            ToolBar1.Buttons(0).Text = "Nuevo"
    '            ToolBar1.Buttons(0).ImageIndex = 0
    '        Else
    '            MsgBox("No existen datos a ser actualizados...", MsgBoxStyle.Information, "Atenci�n...")
    '        End If
    '    Catch ex As SystemException
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub Habilitar_Inhabilitar()

        Dim funciones As New Conexion
        Dim mensaje As String
        Dim Descripcion_Accion As String
        Dim mensaje2 As String

        If Check_Inhabilitado.Checked = False Then
            mensaje = "Desea Inhabilitar este art�culo en el inventario"
            mensaje2 = "INHABILITADO"
            Descripcion_Accion = "ARTICULO INHABILITADO"
        Else
            mensaje = "Desea Habilitar este art�culo en el inventario"
            mensaje2 = "HABILITADO"
            Descripcion_Accion = "ARTICULO HABILITADO"
        End If

        If MsgBox(mensaje, MsgBoxStyle.YesNo, "Atenci�n...") = MsgBoxResult.Yes Then
            Try 'se intenta hacer
                If BindingContext(DataSetInventario, "Inventario").Count > 0 Then

                    Dim Nombre_Tabla As String


                    Nombre_Tabla = "INVENTARIO"
                    'datos = "" & Nombre_Tabla & "','" & TxtCodigo.Text & "','" & TxtDescripcion.Text & "', 'ARTICULO  " & mensaje2 & "','" & usua.Nombre & "'," & CDbl(TxtCosto.Text) & "," & CDbl(TxtPrecioVenta_A.Text) & "," & CDbl(TxtPrecioVenta_B.Text) & "," & CDbl(TxtPrecioVenta_C.Text) & "," & CDbl(TxtPrecioVenta_D.Text)
                    'datos = "" & Nombre_Tabla & "','" & TxtCodigo.Text & "','" & TxtDescripcion.Text & "','" & Descripcion_Accion & "','" & Date.Now & "','" & usua.Nombre & "','" & CDbl(TxtCosto.Text) & "','" & CDbl(TxtPrecioVenta_A.Text) & "','" & CDbl(TxtPrecioVenta_B.Text) & "','" & CDbl(TxtPrecioVenta_C.Text) & "','" & CDbl(TxtPrecioVenta_D.Text)
                    'If funciones.AddNewRecord("Bitacora", "Tabla,Campo_Clave,DescripcionCampo,Accion,Fecha,Usuario,Costo,VentaA,VentaB,VentaC,VentaD", datos) <> "" Then
                    '    MsgBox("Problemas al Inhabilitar el art�culo", MsgBoxStyle.Critical)
                    '    'MsgBox(s)
                    '    Exit Sub
                    'End If

                    '+++++++++++++++++++++++++++++++++++++++++Esto es una prueba++++++++++++++++++++++++++++++++++++++++++'

                    BindingContext(DataSetInventario, "Bitacora").AddNew()
                    BindingContext(DataSetInventario, "Bitacora").Current("Tabla") = "INVENTARIO"
                    BindingContext(DataSetInventario, "Bitacora").Current("Campo_Clave") = TxtCodigo.Text
                    BindingContext(DataSetInventario, "Bitacora").Current("DescripcionCampo") = TxtDescripcion.Text
                    'Se especifica que tipo de accion es si es un nuevo articulo o una actualizacion'
                    'If ToolBarNuevo.Text = "Cancelar" Then
                    BindingContext(DataSetInventario, "Bitacora").Current("Accion") = Descripcion_Accion
                    'Else
                    '   BindingContext(DataSetInventario, "Bitacora").Current("Accion") = "ARTICULO ACTUALIZADO"
                    'End If
                    BindingContext(DataSetInventario, "Bitacora").Current("Usuario") = usua.Nombre
                    BindingContext(DataSetInventario, "Bitacora").Current("Fecha") = Now
                    BindingContext(DataSetInventario, "Bitacora").Current("Costo") = CDbl(TxtCosto.Text)
                    BindingContext(DataSetInventario, "Bitacora").Current("VentaA") = CDbl(TxtPrecioVenta_A.Text)
                    BindingContext(DataSetInventario, "Bitacora").Current("VentaB") = CDbl(TxtPrecioVenta_B.Text)
                    BindingContext(DataSetInventario, "Bitacora").Current("VentaC") = CDbl(TxtPrecioVenta_C.Text)
                    BindingContext(DataSetInventario, "Bitacora").Current("VentaD") = CDbl(TxtPrecioVenta_D.Text)
                    BindingContext(DataSetInventario, "Bitacora").EndCurrentEdit()
                    '+++++++++++++++++++++++++++++++++++++++++Esto es una prueba++++++++++++++++++++++++++++++++++++++++++'

                    If Check_Inhabilitado.Checked = True Then
                        Check_Inhabilitado.Checked = False
                        ToolBar1.Buttons(3).Text = "Inhabilitar"
                        BindingContext(DataSetInventario, "Inventario").Current("Inhabilitado") = False
                    Else
                        Check_Inhabilitado.Checked = True
                        ToolBar1.Buttons(3).Text = "Habilitar"
                        BindingContext(DataSetInventario, "Inventario").Current("Inhabilitado") = True
                    End If

                    'BindingContext(DataSetInventario, "Inventario").RemoveAt(BindingContext(DataSetInventario, "Inventario").Position)
                    BindingContext(DataSetInventario, "Inventario").EndCurrentEdit()
                    AdapterInventario.Update(DataSetInventario, "Inventario")


                    MsgBox("El art�culo fue " & mensaje2 & " satisfactoriamente", MsgBoxStyle.Information)

                Else
                    MsgBox("No existen datos a ser Habilitados/Habilitados", MsgBoxStyle.Information)
                End If
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        End If
    End Sub
    Private Function buscarCodigoBarra2() As String  'JCGA 12042007

        Try

            Dim strCodigoBarras As String = TxtBarras.Text
            Dim strCodigoArticulo As String = TxtCodigo.Text
            Dim strQuery As String = "SELECT Descripcion FROM inventario WHERE barras = '" & strCodigoBarras & "' and Codigo= '" & strCodigoArticulo & "'"
            Dim strArticulo As String = ""
            Dim strMensaje As String = ""

            'Evaluo si el ariculo conserva el mismo codigo de barras 
            strArticulo = cFunciones.BuscaString(strQuery, SqlConnection1.ConnectionString)

            'Si articulo no trae informacion indica que el articulo cambio el codigo de barras
            If strArticulo = "" Then

                'Evaluo si algun articulo tiene asignado el codigo de barras que se le acaba de asignar al articulo
                strQuery = "SELECT Descripcion FROM inventario WHERE barras = '" & strCodigoBarras & "'"

                strArticulo = cFunciones.BuscaString(strQuery, SqlConnection1.ConnectionString)
                'Si no trae nada indica que no hay ariculo con el codigo de barras
                If strArticulo = "" Then
                    Return strMensaje
                Else
                    'Si se encontro algun articulo con ese codigo de barras se enviara el la decripcion de este
                    strMensaje = strArticulo
                    Return strMensaje
                End If

            Else
                'Si el articulo conserva el mismo codigo de barra se envia el mensaje de igual
                strMensaje = "Igual"
                Return strMensaje

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function


    Private Function buscarCodigoBarra() As String  'JCGA 12042007

        Try
            Dim strCodigoBarras As String = TxtBarras.Text
            Dim strQuery As String = "SELECT Descripcion FROM inventario WHERE barras = '" & strCodigoBarras & "'"
            Dim strArticulo As String = ""
            Dim strMensaje As String = ""
            strArticulo = cFunciones.BuscaString(strQuery, SqlConnection1.ConnectionString)
            If strArticulo = "" Then
                Return strMensaje
            Else
                strMensaje = strArticulo
                Return strMensaje
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Private Sub RegistrarDatos()
        Try
            Dim funciones As New Conexion

            Dim strActualiza As String = ""
            If CDbl(TxtCosto.Text) = 0 Or CDbl(TxtCosto.Text) < 0 Then
                MsgBox("El costo del art�culo no puede ser 0, corrijalo", MsgBoxStyle.Information)
                Exit Sub
            End If

            If TxtBarras.Text.Trim().Length() = 0 Then
                MsgBox("El c�digo no puede estar vaci�, ingrese un c�digo.", MsgBoxStyle.Information)
                Exit Sub
            End If

            If ckVerCodBarraInv.Checked = True Then
                '-------------------------------------------------------------------------------------------------
                'Variable que indica si se ha presionado el boton de nuevo
                If strNuevo <> "" Then
                    Dim strExistencia As String = buscarCodigoBarra()
                    If strExistencia <> "" Then
                        MessageBox.Show("El C�digo ya existe con la descripci�n : " & strExistencia, "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                        Exit Sub
                    End If
                End If

                '-Si se realiza una actualizacion la aplicacion realiza una busqueda del Codigo de Barras--------------------

                strActualiza = buscarCodigoBarra2()

                If strActualiza <> "" Then
                    If strActualiza <> "Igual" Then
                        MessageBox.Show("El C�digo ya existe con la descripci�n : " & strActualiza, "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If
                End If

                '------------------------------------------------------------------------------------------------------------
            End If
            '********************************************************************************************
            'LFCHG 07122006 
            'DESCRIPCION DEL CAMBIO:
            'ELIMINACION DE LA VARIABLE NUEVO
            'VALIDACION EN CASO DE HABER UN CAMBIO EN LA TABLA INVENTARIO
            'INCLUIR EN UNA TRASSACCION EL PROCEDIMIENTO DE REGISTAR

            'If ToolBarNuevo.Text = "Cancelar" Then
            BindingContext(DataSetInventario, "Bitacora").AddNew()
            BindingContext(DataSetInventario, "Bitacora").Current("Tabla") = "INVENTARIO"
            BindingContext(DataSetInventario, "Bitacora").Current("Campo_Clave") = TxtCodigo.Text
            BindingContext(DataSetInventario, "Bitacora").Current("DescripcionCampo") = TxtDescripcion.Text
            'Se especifica que tipo de accion es si es un nuevo articulo o una actualizacion'
            If ToolBarNuevo.Text = "Cancelar" Then
                BindingContext(DataSetInventario, "Bitacora").Current("Accion") = "ARTICULO AGREGADO"
            Else
                BindingContext(DataSetInventario, "Bitacora").Current("Accion") = "ARTICULO ACTUALIZADO"
            End If
            BindingContext(DataSetInventario, "Bitacora").Current("Usuario") = usua.Nombre
            BindingContext(DataSetInventario, "Bitacora").Current("Fecha") = Now
            BindingContext(DataSetInventario, "Bitacora").Current("Costo") = CDbl(TxtCosto.Text)
            BindingContext(DataSetInventario, "Bitacora").Current("VentaA") = CDbl(TxtPrecioVenta_A.Text)
            BindingContext(DataSetInventario, "Bitacora").Current("VentaB") = CDbl(TxtPrecioVenta_B.Text)
            BindingContext(DataSetInventario, "Bitacora").Current("VentaC") = CDbl(TxtPrecioVenta_C.Text)
            BindingContext(DataSetInventario, "Bitacora").Current("VentaD") = CDbl(TxtPrecioVenta_D.Text)
            BindingContext(DataSetInventario, "Bitacora").EndCurrentEdit()
            'Else

            If SqlConnection1.State <> SqlConnection1.State.Open Then SqlConnection1.Open()
            Dim Trans As SqlTransaction = SqlConnection1.BeginTransaction
            Try
                AdapterInventario.InsertCommand.Transaction = Trans
                AdapterInventario.SelectCommand.Transaction = Trans
                AdapterInventario.DeleteCommand.Transaction = Trans
                AdapterInventario.UpdateCommand.Transaction = Trans
                AdapterBitacora.SelectCommand.Transaction = Trans
                AdapterBitacora.InsertCommand.Transaction = Trans
                AdapterBitacora.DeleteCommand.Transaction = Trans
                AdapterBitacora.UpdateCommand.Transaction = Trans

                '**************************************************************************
                If ToolBarNuevo.Text = "Cancelar" Then
                    BindingContext(DataSetInventario, "Inventario").Current("CostoPromedio") = 0
                Else
                    BindingContext(DataSetInventario, "Inventario").Current("CostoPromedio") = CInt(TxtCostoPromedio.Text)
                End If
                BindingContext(DataSetInventario, "Inventario").Current("Subfamilia") = ComboBoxFamilia.SelectedValue.ToString
                BindingContext(DataSetInventario, "Inventario").Current("CodMarca") = 0 'ComboBoxMarca.SelectedValue.ToString
                BindingContext(DataSetInventario, "Inventario").Current("Proveedor") = CInt(cmboxProveedor.SelectedValue)
                BindingContext(DataSetInventario, "Inventario").Current("MonedaVenta") = CInt(ComboBoxMonedaVenta1.SelectedValue)
                BindingContext(DataSetInventario, "Inventario").Current("Medidas") = (txtMedidas.Text)
                BindingContext(DataSetInventario, "Inventario").Current("Alterno") = (txtAlterno.Text)
                BindingContext(DataSetInventario, "Inventario").Current("ControlEntrega") = chControEntrega.Checked
                BindingContext(DataSetInventario, "Inventario").EndCurrentEdit()
                BindingContext(DataSetInventario, "Inventario").Position -= 1
                BindingContext(DataSetInventario, "Inventario").Position += 1
                If BindingContext(DataSetInventario, "Inventario").Current("Precio_Sugerido") < 0 Then
                    BindingContext(DataSetInventario, "Inventario").Current("Precio_Sugerido") = 0
                    BindingContext(DataSetInventario, "Inventario").Current("SugeridoIV") = 0
                End If

                '**************************************************************************

                AdapterInventario.Update(DataSetInventario, "Inventario")
                AdapterBitacora.Update(DataSetInventario.Bitacora)
                Trans.Commit()
                DataSetInventario.AcceptChanges()



            Catch ex As Exception
                Trans.Rollback()
                If ToolBarNuevo.Text = "Cancelar" Then
                    MsgBox("Error al intentar Guardar el articulo en inventario" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                Else
                    MsgBox("Error al intentar Actualizar el articulo en inventario" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                End If

            End Try

            'UNA VEZ QUE SE REALIZARON LOS CAMBIOS EN EL INVENTARIO
            'LLAMAMOS A LA FUNCION PARA CARGAR EL INVENTARIO

            ''NUEVO REGISTRO
            'If ToolBarNuevo.ImageIndex = 8 Then
            '	Adaptador_Inventraio_AUX.Fill(DataSetInventario, "Inventario2")
            '	BindingContext(DataSetInventario, "Inventario2").Position = BindingContext(DataSetInventario, "Inventario2").Count - 1
            'Else
            '	'EDICION


            'End If


            'FIN DE CAMBIOS
            '************************************************************************************

            MsgBox("Los datos se guardaron satisfactoriamente...", MsgBoxStyle.Information, "Atenci�n...")
            ToolBar1.Buttons(0).Text = "Nuevo"
            ToolBar1.Buttons(0).ImageIndex = 0
            DataNavigator1.Enabled = True
            ToolBarRegistrar.Text = "Actualizar"
            strNuevo = ""
            TabPage1.Enabled = False

        Catch ex As SystemException
            MsgBox(ex, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub NuevoRegistros()
        Try

            Dim Link As New Conexion
            TxtCodigo.Text = CDbl(Link.SQLExeScalar("SELECT isnull(MAX(Codigo),0) FROM Inventario") + 1)
            Text_Cod_AUX.Text = TxtCodigo.Text
            ToolBar1.Buttons(3).Enabled = False
            ToolBar1.Buttons(4).Enabled = False
            TxtBarras.Text = ""
            TxtDescripcion.Text = ""
            TxtCanPresentacion.Text = "1"
            ComboBoxPresentacion.SelectedIndex = 0

            ComboBoxMoneda.SelectedIndex = 0
            ComboBoxMonedaVenta1.SelectedIndex = 0

            'ComboBoxMarca.SelectedIndex = -1
            'ComboBox_Ubicacion_SubUbicacion.SelectedIndex = -1
            'ComboBoxFamilia.SelectedIndex = -1
            'ComboBoxMarca.SelectedValue = DataSetInventario.Marcas.Rows(0).Item(0)
            ComboBox_Ubicacion_SubUbicacion.SelectedValue = DataSetInventario.SubUbicacion.Rows(0).Item(0)
            ComboBoxFamilia.SelectedValue = DataSetInventario.SubFamilias.Rows(0).Item(0)
            cmboxProveedor.SelectedValue = DataSetInventario.Proveedores.Rows(0).Item(0)


            llenarTxtPorcentajeVentaGeneral()
            TxtMin.Text = "0.00"
            TxtMax.Text = "0.00"
            TxtMed.Text = "0.00"
            TxtBase.Text = "0.00"
            TxtFlete.Text = "0.00"
            TxtCosto.Text = "0.00"
            TxtOtros.Text = "0.00"
            TxtPrecioVenta_A.Text = "0.00"
            TxtPrecioVenta_B.Text = "0.00"
            TxtPrecioVenta_C.Text = "0.00"
            TxtPrecioVenta_D.Text = "0.00"
            TxtPrecioVenta_IV_A.Text = "0.00"
            TxtPrecioVenta_IV_B.Text = "0.00"
            TxtPrecioVenta_IV_C.Text = "0.00"
            TxtPrecioVenta_IV_D.Text = "0.00"
            TxtObservaciones.Text = ""
            TxtExistencia.Text = "0.00"
            TxtBarras.Focus()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub TxtCanPresentacion_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Select Case sender.Name.ToString
            Case TxtCanPresentacion.Name
        End Select

        If TxtCanPresentacion.Text = "" Then
            ErrorProvider.SetError(sender, "Debe de digitar una cantidad...")
            e.Cancel = True
        Else
            ErrorProvider.SetError(sender, "")
        End If

    End Sub

    Private Sub TxtCodigo_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtBarras.GotFocus, TxtDescripcion.GotFocus, TxtCanPresentacion.GotFocus, TxtMed.GotFocus, TxtMax.GotFocus, TxtPrecioVenta_A.GotFocus, TxtPrecioVenta_B.GotFocus, TxtPrecioVenta_C.GotFocus, TxtPrecioVenta_D.GotFocus, TxtUtilidad_D.GotFocus, TxtUtilidad_A.GotFocus, TxtUtilidad_C.GotFocus, TxtUtilidad_B.GotFocus, TxtPrecioVenta_IV_D.GotFocus, TxtPrecioVenta_IV_A.GotFocus, TxtPrecioVenta_IV_C.GotFocus, TxtPrecioVenta_IV_B.GotFocus, TxtExistencia.GotFocus, TxtObservaciones.GotFocus, TxtMin.GotFocus, txtPorcIvaGv.GotFocus, TxtFlete.GotFocus, TxtOtros.GotFocus, TxtCosto.GotFocus, TxtBase.GotFocus
        If sender.name = ComboBoxMonedaVenta1.Name Then
            DolarVenta()
        Else
            sender.SelectionStart = 0
            sender.SelectionLength = Len(sender.Text)
        End If
    End Sub

    Private Sub ComboBoxMonedaVenta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxMonedaVenta1.SelectedIndexChanged
        DolarVenta()
    End Sub

    Private Sub DolarVenta()
        Dim Link As New Conexion
        If ComboBoxMonedaVenta1.Text <> "" Then
            TextBoxValorMonedaEnVenta.Clear()
            TextBoxValorMonedaEnVenta.Text = CDbl(Link.SQLExeScalar("Select isnull(max(ValorCompra),0) from moneda where codmoneda = " & ComboBoxMonedaVenta1.SelectedValue))
            Link.DesConectar(Link.Conectar)
            CalcularEquivalenciaMoneda()
        End If
    End Sub

    Private Sub CalcularEquivalenciaMoneda()
        If cargando = True Then Exit Sub
        Try
            TxtBaseEquivalente.Text = TxtBase.Text * (TextBoxValorMonedaEnCosto.Text / TextBoxValorMonedaEnVenta.Text)
            TxtFleteEquivalente.Text = TxtFlete.Text * (TextBoxValorMonedaEnCosto.Text / TextBoxValorMonedaEnVenta.Text)
            TxtOtrosCargosEquivalente.Text = TxtOtros.Text * (TextBoxValorMonedaEnCosto.Text / TextBoxValorMonedaEnVenta.Text)
            TxtCostoEquivalente.Text = TxtCosto.Text * (TextBoxValorMonedaEnCosto.Text / TextBoxValorMonedaEnVenta.Text)
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub EjecutarCalculos()
        If cargando = True Then Exit Sub
        Try
            Dim Evento As New System.Windows.Forms.KeyPressEventArgs(Chr(Keys.Enter))
            ' Salto = False
            DolarVenta()
            TextBox1_KeyPress(TxtPrecioVenta_A, Evento)
            TextBox1_KeyPress(TxtPrecioVenta_B, Evento)
            TextBox1_KeyPress(TxtPrecioVenta_C, Evento)
            TextBox1_KeyPress(TxtPrecioVenta_D, Evento)
            TextBox1_KeyPress(TxtPrecioVenta_P, Evento)
            TextBox1_KeyPress(txtVentaSugerido, Evento)
            ' Salto = True
            TxtImpuesto.Enabled = False

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TxtUtilidad_A_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtCodigo.LostFocus, TxtBarras.LostFocus, TxtCanPresentacion.LostFocus, TxtMed.LostFocus, TxtMax.LostFocus, TxtPrecioVenta_A.LostFocus, TxtPrecioVenta_B.LostFocus, TxtPrecioVenta_C.LostFocus, TxtPrecioVenta_D.LostFocus, TxtUtilidad_D.LostFocus, TxtUtilidad_A.LostFocus, TxtUtilidad_C.LostFocus, TxtUtilidad_B.LostFocus, TxtPrecioVenta_IV_D.LostFocus, TxtPrecioVenta_IV_A.LostFocus, TxtPrecioVenta_IV_C.LostFocus, TxtPrecioVenta_IV_B.LostFocus, TxtExistencia.LostFocus, TxtObservaciones.LostFocus, ComboBoxBodegas.LostFocus, ComboBoxPresentacion.LostFocus, ComboBox_Ubicacion_SubUbicacion.LostFocus, ComboBoxFamilia.LostFocus, TxtUtilidad_P.LostFocus, TxtPrecioVenta_P.LostFocus, TxtPrecioVenta_IV_P.LostFocus, TxtMin.LostFocus, txtPorcIvaGv.LostFocus, TxtFlete.LostFocus, TxtOtros.LostFocus, TxtCosto.LostFocus, ComboBoxMoneda.LostFocus, TxtBase.LostFocus, ComboBoxMonedaVenta1.LostFocus
        Try
            If TxtBarras.Text <> "" Then txtAlterno.Text = CifraDecifra.Cifra(TxtBarras.Text)
            If cargando = True Then Exit Sub
            calculaMontos(sender)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Check_Promo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Promo.CheckedChanged
        If Check_Promo.Checked = True Then
            Desde.Enabled = True
            Hasta.Enabled = True
            Label33.Enabled = True
            Label34.Enabled = True
            TxtPrecioVenta_P.Enabled = True
            TxtUtilidad_P.Enabled = True
            TxtPrecioVenta_IV_P.Enabled = True
        Else
            Desde.Enabled = False
            Hasta.Enabled = False
            Label33.Enabled = False
            Label34.Enabled = False
            TxtPrecioVenta_P.Enabled = False
            TxtUtilidad_P.Enabled = False
            TxtPrecioVenta_IV_P.Enabled = False
        End If
    End Sub

    Private Sub Desde_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Desde.ValueChanged
        Validate()
    End Sub

    Private Sub Desde_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Desde.Validating
        If CType(Desde.Value, Date) > CType(Hasta.Value, Date) Then
            ErrorProvider.SetError(sender, "La fecha Inicial no puede ser mayor que la fecha Final...")
            e.Cancel = True
        Else
            ErrorProvider.SetError(sender, "")
            e.Cancel = False
        End If
    End Sub

    Private Sub Hasta_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Hasta.ValueChanged
        Validate()
    End Sub

    Private Sub Hasta_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Hasta.Validating
        If CType(Desde.Value, Date) > CType(Hasta.Value, Date) Then
            ErrorProvider.SetError(sender, "La fecha Inicial no puede ser mayor que la fecha Final...")
            e.Cancel = True
        Else
            ErrorProvider.SetError(sender, "")
            e.Cancel = False
        End If
    End Sub

    Private Sub TxtBarras_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtBarras.TextChanged
        If Len(TxtBarras.Text) = 5 Then
            If TxtBarras.Text.Chars(4) = "%" Then Buscar_Codigo()
        End If
    End Sub

    Private Sub TxtBarras_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtBarras.KeyDown
        txtAlterno.Text = CifraDecifra.Cifra(TxtBarras.Text)
        Select Case e.KeyCode

            Case Keys.Enter : TxtDescripcion.Focus()
            Case Keys.F1 : BuscarRegistros()

        End Select

    End Sub
    Private Sub TxtDescripcion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtDescripcion.KeyDown
        If e.KeyCode = Keys.Enter Then
            TxtCanPresentacion.Focus()
        End If

        If e.KeyCode = Keys.F1 Then
            BuscarRegistros()
        End If
    End Sub

    Private Sub TxtCanPresentacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtCanPresentacion.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBoxPresentacion.Focus()
        End If

        If e.KeyCode = Keys.F1 Then
            BuscarRegistros()
        End If
    End Sub

    Private Sub ComboBoxPresentacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxPresentacion.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBoxFamilia.Focus()
        End If
    End Sub

    Private Sub ComboBoxFamilia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxFamilia.KeyDown
        Try
            If e.KeyCode = Keys.F1 Then
                Dim Fx As New cFunciones
                Dim valor As String
                valor = Fx.BuscarDatos("SELECT SubFamilias.Codigo, Familia.Descripcion + '/' + SubFamilias.Descripcion AS Familiares FROM SubFamilias INNER JOIN Familia ON SubFamilias.CodigoFamilia = Familia.Codigo", "Familia.Descripcion + '/' + SubFamilias.Descripcion", "Buscar Familia...")

                If valor = "" Then
                    ComboBoxFamilia.SelectedIndex = -1
                Else

                    ComboBoxFamilia.SelectedValue = valor
                    BindingContext(DataSetInventario, "Inventario").Current("SubFamilia") = valor
                    Dim p As Integer = BindingContext(DataSetInventario, "SubFamilias").Position
                    BindingContext(DataSetInventario, "SubFamilias").Position += 1
                    BindingContext(DataSetInventario, "SubFamilias").Position = p

                End If

            End If


            If e.KeyCode = Keys.Enter Then

                Dim valor As String
                valor = ComboBoxFamilia.SelectedValue
                BindingContext(DataSetInventario, "Inventario").Current("SubFamilia") = valor
                Dim p As Integer = BindingContext(DataSetInventario, "SubFamilias").Position
                BindingContext(DataSetInventario, "SubFamilias").Position += 1
                BindingContext(DataSetInventario, "SubFamilias").Position = p

                ComboBox_Ubicacion_SubUbicacion.Focus()


            End If

        Catch ex As SystemException
            MsgBox(ex, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub ComboBox_Ubicacion_SubUbicacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox_Ubicacion_SubUbicacion.KeyDown
        Try
            If e.KeyCode = Keys.F1 Then
                Dim Fx As New cFunciones
                Dim valor As String
                valor = Fx.BuscarDatos("SELECT SubUbicacion.Codigo, Ubicaciones.Descripcion + '/' + SubUbicacion.DescripcionD AS Ubicaciones FROM SubUbicacion INNER JOIN Ubicaciones ON SubUbicacion.Cod_Ubicacion = Ubicaciones.Codigo", "Ubicaciones.Descripcion + '/' + SubUbicacion.DescripcionD", "Buscar Ubicaci�n...")
                If valor = "" Then
                    ComboBox_Ubicacion_SubUbicacion.SelectedIndex = -1
                Else
                    ComboBox_Ubicacion_SubUbicacion.SelectedValue = valor
                    BindingContext(DataSetInventario, "Inventario").Current("SubUbicacion") = valor
                End If

            End If

            If e.KeyCode = Keys.Enter Then
                Dim valor As String

                valor = ComboBox_Ubicacion_SubUbicacion.SelectedValue
                BindingContext(DataSetInventario, "Inventario").Current("SubUbicacion") = valor
                cmboxProveedor.Focus()

            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ComboBoxMarca_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            If e.KeyCode = Keys.F1 Then
                Dim Fx As New cFunciones
                Dim valor As String
                valor = Fx.BuscarDatos("Select CodMarca,Marca from Marcas", "Marca", "Buscar Marca...")

                'If valor = "" Then
                '    ComboBoxMarca.SelectedIndex = -1
                'Else
                '    ComboBoxMarca.SelectedValue = CInt(valor)
                '    BindingContext(DataSetInventario, "Inventario").Current("CodMarca") = valor
                'End If

            End If

            If e.KeyCode = Keys.Enter Then

                Dim valor As String
                valor = "" 'ComboBoxMarca.SelectedValue
                BindingContext(DataSetInventario, "Inventario").Current("CodMarca") = valor
                ComboBoxMoneda.Focus()

            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBoxMoneda_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxMoneda.KeyDown
        If e.KeyCode = Keys.Enter Then
            'TxtImpuesto.Focus()
            TxtBase.Focus()
        End If
    End Sub

    'Private Sub TxtImpuesto_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtImpuesto.KeyDown
    '	Try
    '		If e.KeyCode = Keys.Enter Then
    '			TxtBase.Focus()

    '			If TxtImpuesto.Text <> "" Then TxtBase.Focus() Else TxtImpuesto.Text = "0"

    '		End If

    '	Catch ex As SyntaxErrorException
    '		MsgBox(ex.Message)
    '	End Try
    'End Sub

    Private Sub TxtCosto_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtCosto.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then

                If TxtCosto.Text <> "" And TxtCosto.Text <> "0" Then
                    ComboBoxMonedaVenta1.Focus()
                Else
                    TxtCosto.Text = "0"
                End If
            End If

        Catch ex As SyntaxErrorException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBoxMonedaVenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxMonedaVenta1.KeyDown
        If e.KeyCode = Keys.Enter Then
            TxtUtilidad_A.Focus()
        End If
    End Sub

    Private Sub TxtUtilidad_A_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtUtilidad_A.KeyDown, TxtUtilidad_B.KeyDown, TxtUtilidad_C.KeyDown, TxtUtilidad_D.KeyDown, TxtPrecioVenta_A.KeyDown, TxtPrecioVenta_B.KeyDown, TxtPrecioVenta_C.KeyDown, TxtPrecioVenta_D.KeyDown, TxtPrecioVenta_P.KeyDown, TxtPrecioVenta_IV_A.KeyDown, TxtPrecioVenta_IV_B.KeyDown, TxtPrecioVenta_IV_C.KeyDown, TxtPrecioVenta_IV_D.KeyDown, TxtPrecioVenta_IV_P.KeyDown, TxtBase.KeyDown, TxtOtros.KeyDown, TxtMin.KeyDown, TxtMed.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub TxtMaxDesc_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtMaxDesc.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub TxtBarras_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtBarras.KeyPress
        Try

            'If (Not e.KeyChar.IsDigit(e.KeyChar)) And (Not e.KeyChar.IsLetter(e.KeyChar)) Then   ' valida que en este campo solo se digiten numeros y/o "-"
            '    If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "%"c) And Not (e.KeyChar = "#"c) Then
            '        e.Handled = True  ' esto invalida la tecla pulsada
            '    End If
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Sub Buscar_Codigo()
        Dim func As New Conexion
        Dim func2 As New Conexion

        Dim Nuevo_Codigo As String
        Dim cod As Integer
        Dim rs As SqlDataReader
        Dim Codigo_Subfamilia As String
        Dim existe As Boolean

        rs = func.GetRecorset(func.Conectar, "SELECT RIGHT('00' + CAST(CodigoFamilia AS varchar), 2) + RIGHT('00' + CAST(SubCodigo AS varchar), 2) AS Codigo_Ceros, Codigo FROM SubFamilias WHERE (RIGHT('00' + CAST(CodigoFamilia AS varchar), 2) + RIGHT('00' + CAST(SubCodigo AS varchar), 2) = '" & Mid(TxtBarras.Text, 1, 4) & "')")
        While rs.Read
            Codigo_Subfamilia = rs("Codigo")

            cod = func.SlqExecuteScalar(func2.Conectar, "SELECT max(ISNULL(CodConsFam, 0)) AS Maximo FROM Vista_Generador_Barras WHERE (CodBarrasArticulo LIKE '" & TxtBarras.Text & "') GROUP BY CodigoSubFamilia")
            cod = cod + 1
            func2.DesConectar(func2.Conectar)

            Nuevo_Codigo = Mid(TxtBarras.Text, 1, 4) & Format(cod, "0000")

            'mientras ya exista un articulo en el inventario con ese c�digo de barras
            Do While func.SlqExecuteScalar(func2.Conectar, "select Barras from Inventario where Barras ='" & Nuevo_Codigo & "'") <> Nothing
                cod = cod + 1
                Nuevo_Codigo = Mid(TxtBarras.Text, 1, 4) & Format(cod, "0000")
            Loop
            TxtBarras.Text = Nuevo_Codigo
            ComboBoxFamilia.SelectedValue = Codigo_Subfamilia
            existe = True
            TxtDescripcion.Focus()

        End While

        func.DesConectar(func.Conectar)
        func2.DesConectar(func2.Conectar)


        If existe = False Then
            MsgBox("No existe una subfamilia con ese c�digo", MsgBoxStyle.Information)
            TxtBarras.Text = ""
        End If
    End Sub
    'Private Sub CheckBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CheckBox1.KeyDown

    '	If e.KeyCode = Keys.Enter Then
    '		Select Case sender.name
    '			Case CheckBox1.Name
    '				TxtImpuesto.Focus()
    '		End Select
    '	End If
    'End Sub
    Private Sub TxtFlete_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtFlete.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub
    Private Sub ComboBoxBodegas_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxBodegas.GotFocus, ComboBoxMoneda.GotFocus
        SendKeys.Send("{F4}")
    End Sub
    Private Sub ComboBoxBodegas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxBodegas.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub txtNombreProveedor_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmboxProveedor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmboxProveedor.KeyDown
        Try

            If e.KeyCode = Keys.F1 Then
                Dim Fx As New cFunciones
                Dim valor As String
                valor = Fx.BuscarDatos("SELECT CodigoProv, Nombre FROM Proveedores", "Nombre", "Buscar Proveedor...")
                If valor = "" Then
                    cmboxProveedor.SelectedIndex = -1
                Else
                    cmboxProveedor.SelectedValue = valor
                    BindingContext(DataSetInventario, "Proveedores").Current("CodigoProv") = valor
                    Dim p As Integer = BindingContext(DataSetInventario, "Proveedores").Position

                    BindingContext(DataSetInventario, "Proveedores").Position += 1
                    BindingContext(DataSetInventario, "Proveedores").Position = p

                End If

            End If
            If e.KeyCode = Keys.Enter Then

                Dim valor As String
                valor = cmboxProveedor.SelectedValue
                BindingContext(DataSetInventario, "Proveedores").Current("CodigoProv") = valor
                Dim p As Integer = BindingContext(DataSetInventario, "Proveedores").Position

                BindingContext(DataSetInventario, "Proveedores").Position += 1
                BindingContext(DataSetInventario, "Proveedores").Position = p
                'ComboBoxMarca.Focus()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Mensaje")
        End Try
    End Sub

    Private Sub ckVerCodBarraInv_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckVerCodBarraInv.CheckedChanged
        SaveSetting("SeeSOFT", "SeePOS", "VerCodBarraInv", ckVerCodBarraInv.Checked)
    End Sub

    Private Sub txtPorSugerido_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPorSugerido.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtVentaSugerido.Text = Format(Math.Round((TxtPrecioVenta_C.Text * (txtPorSugerido.Text / 100)) + TxtPrecioVenta_C.Text, 2), "#,#0.00")
            txtSugeridoIV.Text = Format(Math.Round(txtVentaSugerido.Text * (1 + (txtPorcIvaGv.Text / 100)), 2), "#,#0.00")
            If txtPorSugerido.Text = "NeuN" Then
                txtPorSugerido.Text = 0
            End If
            If txtVentaSugerido.Text = "NeuN" Then
                txtVentaSugerido.Text = 0
            End If
            If txtSugeridoIV.Text = "NeuN" Then
                txtSugeridoIV.Text = 0
            End If

            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub txtVentaSugerido_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtVentaSugerido.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtPorSugerido.Text = Format(Math.Round(((txtVentaSugerido.Text / TxtPrecioVenta_C.Text) * 100) - 100, 2), "#,#0.00")
            txtSugeridoIV.Text = Format(Math.Round(txtVentaSugerido.Text * (1 + (txtPorcIvaGv.Text / 100)), 2), "#,#0.00")
            If txtPorSugerido.Text = "NeuN" Then
                txtPorSugerido.Text = 0
            End If
            If txtVentaSugerido.Text = "NeuN" Then
                txtVentaSugerido.Text = 0
            End If
            If txtSugeridoIV.Text = "NeuN" Then
                txtSugeridoIV.Text = 0
            End If

            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub txtSugeridoIV_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSugeridoIV.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtVentaSugerido.Text = Format(Math.Round(txtSugeridoIV.Text / (1 + (txtPorcIvaGv.Text / 100)), 2), "#,#0.00")
            txtPorSugerido.Text = Format(Math.Round(((txtVentaSugerido.Text / TxtPrecioVenta_C.Text) * 100) - 100, 2), "#,#0.00")
            If txtPorSugerido.Text = "NeuN" Then
                txtPorSugerido.Text = 0
            End If
            If txtVentaSugerido.Text = "NeuN" Then
                txtVentaSugerido.Text = 0
            End If
            If txtSugeridoIV.Text = "NeuN" Then
                txtSugeridoIV.Text = 0
            End If

            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub Ck_Consignacion_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ck_Consignacion.CheckedChanged
        ComboBoxBodegas.Enabled = Ck_Consignacion.Checked
    End Sub


    Private Sub ButtonAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgregar.Click
        incluirEnBodega()
    End Sub
    Sub incluirEnBodega()
        If MsgBox("Desea incluir una bodega", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim cf As New cFunciones
            Dim cond As String = ""
            Dim i As Integer = 0
            For i = 0 To DataSetInventario.ArticulosXBodega.Count - 1
                If DataSetInventario.ArticulosXBodega(i).IdBodega <> 0 Then
                    cond = cond & " AND  ID_Bodega <> " & DataSetInventario.ArticulosXBodega(i).IdBodega
                End If
            Next
            cf.Llenar_Tabla_Generico("SELECT [ID_Bodega] as Id, [Nombre_Bodega]as Nombre FROM [Bodegas] WHERE ID_Bodega > 0 " & cond, DataSetInventario.Bodega, GetSetting("SeeSoft", "SeePos", "Conexion"))
            ComboBoxBodega.Visible = True
            ButtonAgreBodega.Visible = True
            ButtonAgregar.Visible = False
            ButtonEliminar.Visible = False
        End If
    End Sub

    Private Sub ButtonAgreBodega_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgreBodega.Click
        agregarBodega()
    End Sub
    Sub agregarBodega()
        Dim cx As New Conexion
        Dim i As Integer = 0

        cx.SlqExecute(cx.Conectar("SeePos"), "INSERT INTO [SeePos].[dbo].[ArticulosXBodega]( [Codigo], [IdBodega], [Existencia] )" &
        "VALUES(" & BindingContext(DataSetInventario, "Inventario").Current("Codigo") & ", " & ComboBoxBodega.SelectedValue & ",0)")
        cx.DesConectar(cx.sQlconexion)
        '''''''''LLENAR ARTICULOS X BODEGAS'''''''''''''''''''''''''''''''''''''''
        cFunciones.Llenar_Tabla_Generico("SELECT  ArticulosXBodega.Existencia as Existencia, Bodegas.Nombre_Bodega as Nombre, ArticulosXBodega.Codigo as Codigo,  ArticulosXBodega.Id as Id, Bodegas.ID_Bodega as IdBodega FROM ArticulosXBodega INNER JOIN  Bodegas ON ArticulosXBodega.IdBodega = Bodegas.ID_Bodega WHERE ArticulosXBodega.Codigo = " & BindingContext(DataSetInventario, "Inventario").Current("Codigo"), DataSetInventario.ArticulosXBodega, SqlConnection1.ConnectionString)

        ComboBoxBodega.Visible = False
        ButtonAgreBodega.Visible = False
        DataSetInventario.Bodega.Clear()
        ButtonAgregar.Visible = True
        ButtonEliminar.Visible = True
    End Sub

    Private Sub ButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminar.Click
        eliminarArticuloBodega()
    End Sub

    Sub eliminarArticuloBodega()
        If BindingContext(DataSetInventario, "Inventario.InventarioArticulosXBodega").Current("Existencia") = 0 And BindingContext(DataSetInventario, "Inventario.InventarioArticulosXBodega").Current("IdBodega") <> 0 Then
            If MsgBox("�Desea quitarlo de esta bodega?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim cx As New Conexion

                cx.SlqExecute(cx.Conectar("SeePos"), "DELETE FROM ArticulosXBodega Where Id =  " & BindingContext(DataSetInventario, "Inventario.InventarioArticulosXBodega").Current("Id"))
                cx.DesConectar(cx.sQlconexion)
                '''''''''LLENAR ARTICULOS X BODEGAS'''''''''''''''''''''''''''''''''''''''
                cFunciones.Llenar_Tabla_Generico("SELECT  ArticulosXBodega.Existencia as Existencia, Bodegas.Nombre_Bodega as Nombre, ArticulosXBodega.Codigo as Codigo,  ArticulosXBodega.Id as Id, Bodegas.ID_Bodega as IdBodega FROM ArticulosXBodega INNER JOIN  Bodegas ON ArticulosXBodega.IdBodega = Bodegas.ID_Bodega WHERE ArticulosXBodega.Codigo = " & BindingContext(DataSetInventario, "Inventario").Current("Codigo"), DataSetInventario.ArticulosXBodega, SqlConnection1.ConnectionString)
            End If
        Else
            MsgBox("No puede eliminar la bodega porque hay existencias del articulo!", MsgBoxStyle.Exclamation, "Inventario")
        End If
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged

    End Sub

    Private Sub TxtUtilidad_A_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtUtilidad_A.EditValueChanged

    End Sub


    Private Sub ComboBox_Ubicacion_SubUbicacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_Ubicacion_SubUbicacion.SelectedIndexChanged

    End Sub

    Private Sub ButtonAgregarEqui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgregarEqui.Click
        AgregarEquivalencia()
    End Sub
#Region "METODOS"

    Sub Eliminar_Equivalencia()

        If BindingContext(DataSetInventario, "Inventario.InventarioEquivalencias").Count <> 0 Then
            If MsgBox("�Desea quitar esta Equivalencia?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim cx As New Conexion

                cx.SlqExecute(cx.Conectar("SeePos"), "DELETE FROM Equivalencias Where id =  " & BindingContext(DataSetInventario, "Inventario.InventarioEquivalencias").Current("id"))
                cx.DesConectar(cx.sQlconexion)

                cFunciones.Llenar_Tabla_Generico("SELECT dbo.Equivalencias.Id, dbo.Equivalencias.Codigo, dbo.Equivalencias.Barras, dbo.Equivalencias.Barras_Equi, dbo.Inventario.Descripcion,dbo.Equivalencias.Equivalente FROM dbo.Inventario INNER JOIN  dbo.Equivalencias ON dbo.Inventario.Codigo = dbo.Equivalencias.Equivalente WHERE dbo.Equivalencias.Codigo = " & BindingContext(DataSetInventario, "Inventario").Current("Codigo"), DataSetInventario.Equivalencias, SqlConnection1.ConnectionString)

            End If
        Else
            MsgBox("�No existen Equivalencias para Eliminar!", MsgBoxStyle.Exclamation, "Equivalencias")
            Exit Sub
        End If

    End Sub

    Public Function Verifica_Equivalencia() As Boolean

        Dim SQL As New GestioDatos
        Dim sentencia As String = "SELECT * FROM Inventario WHERE Barras = '" & txtEquivalencia.Text & "'"
        Dim dtsUsuario As New DataTable
        dtsUsuario = SQL.Ejecuta(sentencia)

        If dtsUsuario.Rows.Count > 0 Then

            Return True

        Else

            Return False

        End If

    End Function

    Private Sub AgregarEquivalencia()

        Try
            If txtEquivalencia.Text = "" Then
                MsgBox("�Debe seleccionar un codigo precionando la Tecla F1!", MsgBoxStyle.Exclamation, "Equivalencias")
                Exit Sub

            Else

                If Verifica_Equivalencia() = True Then

                    Dim sentencia = "INSERT INTO dbo.Equivalencias(Codigo,Barras,Equivalente,Barras_Equi)VALUES('" & BindingContext(DataSetInventario, "Inventario").Current("Codigo") & "','" & BindingContext(DataSetInventario, "Inventario").Current("Barras") & "','" & Codigo_Equi & "','" & txtEquivalencia.Text & "')"
                    Dim SQL As New GestioDatos
                    SQL.Ejecuta(sentencia)

                    cFunciones.Llenar_Tabla_Generico("SELECT dbo.Equivalencias.Id, dbo.Equivalencias.Codigo, dbo.Equivalencias.Barras, dbo.Equivalencias.Barras_Equi, dbo.Inventario.Descripcion,dbo.Equivalencias.Equivalente FROM dbo.Inventario INNER JOIN  dbo.Equivalencias ON dbo.Inventario.Codigo = dbo.Equivalencias.Equivalente WHERE dbo.Equivalencias.Codigo = " & BindingContext(DataSetInventario, "Inventario").Current("Codigo"), DataSetInventario.Equivalencias, SqlConnection1.ConnectionString)
                    txtEquivalencia.Text = ""
                Else
                    MsgBox("�El codigo Seleccionado No se encuentra el Catalogo de Inventario!", MsgBoxStyle.Exclamation, "Equivalencias")
                    Exit Sub
                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub CargarInformacion_articulos(ByVal codigo As String)
        Dim Link As New Conexion
        Dim Articulo As SqlDataReader
        Dim encontrado As Boolean = False
        If codigo = "" Then
            txtEquivalencia.Focus()
            Exit Sub
        End If

        Articulo = Link.GetRecorset(Link.Conectar, "SELECT Inventario.Codigo, Inventario.ALTERNO, Inventario.Barras, Inventario.Descripcion, Inventario.PrecioBase, Inventario.Fletes, Inventario.OtrosCargos, Inventario.Costo,  Inventario.MonedaVenta, Inventario.IVenta, Inventario.Precio_A, Inventario.Precio_B, Inventario.Precio_C, Inventario.Precio_D, Moneda.ValorCompra AS TipoCambio, Inventario.Lote FROM Inventario INNER JOIN Moneda ON Inventario.MonedaVenta = Moneda.CodMoneda where (cast(CODIGO as varchar)  = '" & codigo & "' or  Barras = '" & codigo & "')" & "and Inhabilitado = 0 and Servicio = 0")

        Try

            If Articulo.Read Then

                txtEquivalencia.Text = Articulo!Barras
                Codigo_Equi = Articulo!codigo
            Else
                MsgBox("No existe un art�culo con ese c�digo o est� inhabilitado", MsgBoxStyle.Exclamation)
                txtEquivalencia.Focus()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Link.DesConectar(Link.sQlconexion)
        End Try
        Link.DesConectar(Link.sQlconexion)
    End Sub
#End Region


    Private Sub txtEquivalencia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtEquivalencia.KeyDown

        Try

            Select Case e.KeyCode
                Case Keys.F1
                    Try
                        'BindingContext(DataSetActualizaLotes1, "Lotes").CancelCurrentEdit()
                        'BindingContext(DataSetActualizaLotes1, "Lotes").AddNew()
                        Dim BuscarArt As New FrmBuscarArticuloPorSucursal(usua)

                        BuscarArt.StartPosition = FormStartPosition.CenterParent
                        'BuscarArt.Cod_Articulo = True
                        BuscarArt.ShowDialog()
                        If BuscarArt.Cancelado Then Exit Sub
                        Dim CODART As Integer = BuscarArt.Codigo
                        BuscarArt.Dispose()
                        sender.SelectionStart = 0
                        sender.SelectionLength = Len(sender.Text)
                        CargarInformacion_articulos(CODART)
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
                    End Try

                    'Case Keys.F2

                    '    Registrar()

                    'Case Keys.Enter

                    '    TxtCantidad.Focus()

            End Select

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Mensaje")
        End Try

    End Sub

    Private Sub txtEquivalencia_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEquivalencia.TextChanged

    End Sub

    Private Sub Label9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DataNavigator1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataNavigator1.Click

    End Sub

    Private Sub ButtonEliminarEqui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminarEqui.Click
        Eliminar_Equivalencia()

    End Sub



    'Private Sub ComboBoxMarca_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TxtEquivalen.Text = ComboBoxMarca.Text
    'End Sub

    Private Sub TxtPrecioVenta_A_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtPrecioVenta_A.EditValueChanged

    End Sub

    Private Sub TxtEquivalen_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtEquivalen.TextChanged
        txtCopiaEquivalencia.Text = TxtEquivalen.Text

    End Sub

    Sub calculaMontos(ByVal sender As Object)
        Dim pBaseImpuesto As Double = 0
        Select Case sender.Name.ToString()
            Case TxtBase.Name, TxtFlete.Name, TxtOtros.Name
                If TxtBase.Text = "" Then TxtBase.Text = 0
                If TxtFlete.Text = "" Then TxtFlete.Text = 0
                If TxtOtros.Text = "" Then TxtOtros.Text = 0
                TxtCosto.Text = (CDbl(TxtBase.Text) + CDbl(TxtFlete.Text) + CDbl(TxtOtros.Text))
                '  TxtCosto.Text = (CDbl(TxtBase.Text) + CDbl(TxtFlete.Text) + CDbl(TxtOtros.Text))
                CalcularEquivalenciaMoneda()
                'calculo de precios por la utilidad
            Case TxtUtilidad_A.Name
                If TxtUtilidad_A.Text = "" Then TxtUtilidad_A.Text = 0
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_A.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_A.Text = pBaseImpuesto + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)
                TxtPrecioVenta_IV_A.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

            Case TxtUtilidad_B.Name
                If TxtUtilidad_B.Text = "" Then TxtUtilidad_B.Text = 0
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_B.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_B.Text = pBaseImpuesto + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)
                TxtPrecioVenta_IV_B.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

            Case TxtUtilidad_C.Name
                If TxtUtilidad_C.Text = "" Then TxtUtilidad_C.Text = 0
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_C.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_C.Text = pBaseImpuesto + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)
                TxtPrecioVenta_IV_C.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

            Case TxtUtilidad_D.Name
                If TxtUtilidad_D.Text = "" Then TxtUtilidad_D.Text = 0
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_D.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_D.Text = pBaseImpuesto + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)
                TxtPrecioVenta_IV_D.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

            Case TxtUtilidad_P.Name
                If TxtUtilidad_P.Text = "" Then TxtUtilidad_P.Text = 0
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_P.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_P.Text = pBaseImpuesto + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)
                TxtPrecioVenta_IV_P.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)


                'calculo de de utilidad por el precio A y calculo del precio com I.V.
            Case TxtPrecioVenta_A.Name
                If TxtPrecioVenta_A.Text = "" Then TxtPrecioVenta_A.Text = 0
                TxtUtilidad_A.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_A.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_A.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_IV_A.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

            Case TxtPrecioVenta_B.Name
                If TxtPrecioVenta_B.Text = "" Then TxtPrecioVenta_B.Text = 0
                TxtUtilidad_B.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_B.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_B.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_IV_B.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

            Case TxtPrecioVenta_C.Name
                If TxtPrecioVenta_C.Text = "" Then TxtPrecioVenta_C.Text = 0
                TxtUtilidad_C.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_C.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_C.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_IV_C.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

            Case TxtPrecioVenta_D.Name
                If TxtPrecioVenta_D.Text = "" Then TxtPrecioVenta_D.Text = 0
                TxtUtilidad_D.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_D.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_D.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_IV_D.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

            Case TxtPrecioVenta_P.Name
                If TxtPrecioVenta_P.Text = "" Then TxtPrecioVenta_P.Text = 0
                TxtUtilidad_P.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_P.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                pBaseImpuesto = (TxtBaseEquivalente.Text * (TxtUtilidad_P.Text / 100)) + TxtBaseEquivalente.Text
                TxtPrecioVenta_IV_P.Text = ((pBaseImpuesto * (txtPorcIvaGv.Text / 100)) + pBaseImpuesto) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text)

                'CALCULO DE PRECIO DE VENTA Y RECALCULAR LA  UTILIDAD CON BASE A LOS NUEVOS PRECIOS 
            Case TxtPrecioVenta_IV_A.Name
                If TxtPrecioVenta_IV_A.Text = "" Then TxtPrecioVenta_IV_A.Text = 0

                pBaseImpuesto = Me.TxtPrecioVenta_IV_A.Text - Me.TxtFleteEquivalente.Text - Me.TxtOtrosCargosEquivalente.Text
                pBaseImpuesto = pBaseImpuesto / (1 + (Me.txtPorcIvaGv.Text / 100))
                TxtPrecioVenta_A.Text = pBaseImpuesto + Me.TxtFleteEquivalente.Text + TxtOtrosCargosEquivalente.Text

                TxtUtilidad_A.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_A.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

            Case TxtPrecioVenta_IV_B.Name
                If TxtPrecioVenta_IV_B.Text = "" Then TxtPrecioVenta_IV_B.Text = 0
                pBaseImpuesto = Me.TxtPrecioVenta_IV_B.Text - Me.TxtFleteEquivalente.Text - Me.TxtOtrosCargosEquivalente.Text
                pBaseImpuesto = pBaseImpuesto / (1 + (Me.txtPorcIvaGv.Text / 100))
                TxtPrecioVenta_B.Text = pBaseImpuesto + Me.TxtFleteEquivalente.Text + TxtOtrosCargosEquivalente.Text
                TxtUtilidad_B.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_B.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

            Case TxtPrecioVenta_IV_C.Name
                If TxtPrecioVenta_IV_C.Text = "" Then TxtPrecioVenta_IV_C.Text = 0
                pBaseImpuesto = Me.TxtPrecioVenta_IV_C.Text - Me.TxtFleteEquivalente.Text - Me.TxtOtrosCargosEquivalente.Text
                pBaseImpuesto = pBaseImpuesto / (1 + (Me.txtPorcIvaGv.Text / 100))
                TxtPrecioVenta_C.Text = pBaseImpuesto + Me.TxtFleteEquivalente.Text + TxtOtrosCargosEquivalente.Text
                TxtUtilidad_C.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_C.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

            Case TxtPrecioVenta_IV_D.Name
                If TxtPrecioVenta_IV_D.Text = "" Then TxtPrecioVenta_IV_D.Text = 0
                pBaseImpuesto = Me.TxtPrecioVenta_IV_D.Text - Me.TxtFleteEquivalente.Text - Me.TxtOtrosCargosEquivalente.Text
                pBaseImpuesto = pBaseImpuesto / (1 + (Me.txtPorcIvaGv.Text / 100))
                TxtPrecioVenta_D.Text = pBaseImpuesto + Me.TxtFleteEquivalente.Text + TxtOtrosCargosEquivalente.Text
                TxtUtilidad_D.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_D.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

            Case TxtPrecioVenta_IV_P.Name
                If TxtPrecioVenta_IV_P.Text = "" Then TxtPrecioVenta_IV_P.Text = 0
                pBaseImpuesto = Me.TxtPrecioVenta_IV_P.Text - Me.TxtFleteEquivalente.Text - Me.TxtOtrosCargosEquivalente.Text
                pBaseImpuesto = pBaseImpuesto / (1 + (Me.txtPorcIvaGv.Text / 100))
                TxtPrecioVenta_P.Text = pBaseImpuesto + Me.TxtFleteEquivalente.Text + TxtOtrosCargosEquivalente.Text
                TxtUtilidad_P.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_P.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
        End Select
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try

            Dim op As New OpenFileDialog
            op.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"
            If op.ShowDialog = DialogResult.OK Then
                PictureBox1.Image = Image.FromFile(op.FileName)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnPegar_Click(sender As Object, e As EventArgs) Handles btnPegar.Click
        Try
            PictureBox1.Image = Clipboard.GetImage()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenarTxtPorcentajeVentaGeneral()
        Dim dt As New DataTable
        If ComboBoxFamilia.SelectedIndex >= 0 Then
            cFunciones.Llenar_Tabla_Generico("select Cabys.porcentajeIVA from Cabys 
               join SubFamilias on Cabys.id=SubFamilias.idCabys where SubFamilias.Codigo='" & ComboBoxFamilia.SelectedValue.ToString & "'", dt, Me.SqlConnection1.ConnectionString)
            If dt.Rows.Count > 0 Then
                txtPorcIvaGv.Text = dt.Rows(0).Item("porcentajeIVA").ToString
            End If
        End If
    End Sub

    Private Sub ComboBoxFamilia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxFamilia.SelectedIndexChanged
        llenarTxtPorcentajeVentaGeneral()
    End Sub
End Class


