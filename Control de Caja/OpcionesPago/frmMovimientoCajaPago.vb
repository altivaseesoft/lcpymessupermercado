Imports System.data.SqlClient
Imports System.Data
Imports System.Windows.Forms

Public Class frmMovimientoCajaPago
    Inherits System.Windows.Forms.Form
    'Public Movimiento_Pago_Factura As New frmMovimientoCajaPagoAbono
    Dim usua

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarExcel As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents dafactura As System.Data.SqlClient.SqlDataAdapter
    ' Friend WithEvents Dsfactura As dsfactura
    Friend WithEvents DGFactpendiente As DevExpress.XtraGrid.GridControl
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents colNum_Factura As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colTipo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colNombre_Cliente As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colCod_Vendedor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colTotal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colFecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Dsfactura21 As Dsfactura2
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMovimientoCajaPago))
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarExcel = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.DGFactpendiente = New DevExpress.XtraGrid.GridControl
        Me.Dsfactura21 = New LcPymes_5._2.Dsfactura2
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.colNum_Factura = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colNombre_Cliente = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colTotal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colCod_Vendedor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colTipo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colFecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.Label15 = New System.Windows.Forms.Label
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.dafactura = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.DGFactpendiente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dsfactura21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.AutoSize = False
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarExcel, Me.ToolBarCerrar})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 415)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(736, 56)
        Me.ToolBar1.TabIndex = 30
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Text = "Actualizar"
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.ImageIndex = 1
        Me.ToolBarBuscar.Text = "Buscar"
        Me.ToolBarBuscar.Visible = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Text = "Registrar"
        Me.ToolBarRegistrar.Visible = False
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.ImageIndex = 3
        Me.ToolBarEliminar.Text = "Eliminar"
        Me.ToolBarEliminar.Visible = False
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.ImageIndex = 7
        Me.ToolBarImprimir.Text = "Imprimir"
        Me.ToolBarImprimir.Visible = False
        '
        'ToolBarExcel
        '
        Me.ToolBarExcel.Enabled = False
        Me.ToolBarExcel.ImageIndex = 5
        Me.ToolBarExcel.Text = "Exportar"
        Me.ToolBarExcel.Visible = False
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'DGFactpendiente
        '
        Me.DGFactpendiente.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGFactpendiente.DataMember = "Ventas"
        Me.DGFactpendiente.DataSource = Me.Dsfactura21
        '
        'DGFactpendiente.EmbeddedNavigator
        '
        Me.DGFactpendiente.EmbeddedNavigator.Name = ""
        Me.DGFactpendiente.Location = New System.Drawing.Point(8, 32)
        Me.DGFactpendiente.MainView = Me.AdvBandedGridView1
        Me.DGFactpendiente.Name = "DGFactpendiente"
        Me.DGFactpendiente.Size = New System.Drawing.Size(720, 376)
        Me.DGFactpendiente.TabIndex = 32
        '
        'Dsfactura21
        '
        Me.Dsfactura21.DataSetName = "Dsfactura2"
        Me.Dsfactura21.Locale = New System.Globalization.CultureInfo("es-MX")
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.AdvBandedGridView1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.colNum_Factura, Me.colTipo, Me.colNombre_Cliente, Me.colCod_Vendedor, Me.colTotal, Me.colFecha})
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        Me.AdvBandedGridView1.OptionsCustomization.AllowGroup = False
        Me.AdvBandedGridView1.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.Columns.Add(Me.colNum_Factura)
        Me.GridBand1.Columns.Add(Me.colNombre_Cliente)
        Me.GridBand1.Columns.Add(Me.colTotal)
        Me.GridBand1.Columns.Add(Me.colCod_Vendedor)
        Me.GridBand1.Columns.Add(Me.colTipo)
        Me.GridBand1.Columns.Add(Me.colFecha)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.Width = 706
        '
        'colNum_Factura
        '
        Me.colNum_Factura.Caption = "Factura"
        Me.colNum_Factura.FieldName = "Num_Factura"
        Me.colNum_Factura.Name = "colNum_Factura"
        Me.colNum_Factura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNum_Factura.Visible = True
        Me.colNum_Factura.Width = 73
        '
        'colNombre_Cliente
        '
        Me.colNombre_Cliente.Caption = "Cliente"
        Me.colNombre_Cliente.FieldName = "Nombre_Cliente"
        Me.colNombre_Cliente.Name = "colNombre_Cliente"
        Me.colNombre_Cliente.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNombre_Cliente.Visible = True
        Me.colNombre_Cliente.Width = 209
        '
        'colTotal
        '
        Me.colTotal.Caption = "Monto"
        Me.colTotal.DisplayFormat.FormatString = "#,#0.00"
        Me.colTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colTotal.FieldName = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTotal.Visible = True
        Me.colTotal.Width = 96
        '
        'colCod_Vendedor
        '
        Me.colCod_Vendedor.Caption = "Vendedor"
        Me.colCod_Vendedor.FieldName = "Vendedor"
        Me.colCod_Vendedor.Name = "colCod_Vendedor"
        Me.colCod_Vendedor.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCod_Vendedor.Visible = True
        Me.colCod_Vendedor.Width = 190
        '
        'colTipo
        '
        Me.colTipo.Caption = "Tipo"
        Me.colTipo.FieldName = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTipo.Visible = True
        Me.colTipo.Width = 68
        '
        'colFecha
        '
        Me.colFecha.Caption = "Fecha"
        Me.colFecha.FieldName = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFecha.Visible = True
        Me.colFecha.Width = 70
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Image = CType(resources.GetObject("Label15.Image"), System.Drawing.Image)
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(0, -8)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(736, 40)
        Me.Label15.TabIndex = 62
        Me.Label15.Text = "Facturas Pendientes de Pago"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'dafactura
        '
        Me.dafactura.SelectCommand = Me.SqlSelectCommand1
        Me.dafactura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Ventas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Cod_Cliente", "Cod_Cliente"), New System.Data.Common.DataColumnMapping("Nombre_Cliente", "Nombre_Cliente"), New System.Data.Common.DataColumnMapping("Orden", "Orden"), New System.Data.Common.DataColumnMapping("Pago_Comision", "Pago_Comision"), New System.Data.Common.DataColumnMapping("Vendedor", "Vendedor"), New System.Data.Common.DataColumnMapping("SubTotal", "SubTotal"), New System.Data.Common.DataColumnMapping("Descuento", "Descuento"), New System.Data.Common.DataColumnMapping("Imp_Venta", "Imp_Venta"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Vence", "Vence"), New System.Data.Common.DataColumnMapping("Cod_Encargado_Compra", "Cod_Encargado_Compra"), New System.Data.Common.DataColumnMapping("Contabilizado", "Contabilizado"), New System.Data.Common.DataColumnMapping("AsientoVenta", "AsientoVenta"), New System.Data.Common.DataColumnMapping("ContabilizadoCVenta", "ContabilizadoCVenta"), New System.Data.Common.DataColumnMapping("AsientoCosto", "AsientoCosto"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("PagoImpuesto", "PagoImpuesto"), New System.Data.Common.DataColumnMapping("FacturaCancelado", "FacturaCancelado"), New System.Data.Common.DataColumnMapping("Num_Apertura", "Num_Apertura"), New System.Data.Common.DataColumnMapping("Entregado", "Entregado"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Moneda_Nombre", "Moneda_Nombre"), New System.Data.Common.DataColumnMapping("Id", "Id")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Ventas.Num_Factura, Ventas.Tipo, Ventas.Cod_Cliente, Ventas.Nombre_Cliente" & _
        ", Ventas.Orden, Ventas.Pago_Comision, ISNULL(Usuarios.Nombre, Ventas.Cedula_Usua" & _
        "rio) AS Vendedor, Ventas.SubTotal, Ventas.Descuento, Ventas.Imp_Venta, Ventas.To" & _
        "tal, Ventas.Fecha, Ventas.Vence, Ventas.Cod_Encargado_Compra, Ventas.Contabiliza" & _
        "do, Ventas.AsientoVenta, Ventas.ContabilizadoCVenta, Ventas.AsientoCosto, Ventas" & _
        ".Anulado, Ventas.PagoImpuesto, Ventas.FacturaCancelado, Ventas.Num_Apertura, Ven" & _
        "tas.Entregado, Ventas.Cod_Moneda, Ventas.Moneda_Nombre, Ventas.Id FROM Ventas LE" & _
        "FT OUTER JOIN Usuarios ON Usuarios.Cedula = Ventas.Cedula_Usuario WHERE (Ventas." & _
        "FacturaCancelado = 0) AND (Ventas.Anulado = 0) AND (Ventas.Tipo <> 'CRE')"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 15000
        '
        'frmMovimientoCajaPago
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(736, 471)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.DGFactpendiente)
        Me.Controls.Add(Me.ToolBar1)
        Me.Name = "frmMovimientoCajaPago"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Movimiento Caja de Pago"
        CType(Me.DGFactpendiente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dsfactura21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "variables"
    Private cConexion As Conexion
    Private sqlConexion As SqlConnection
    Private nuevo As Boolean = True
#End Region

    Function BuscarById(ByVal columna As DataRow)
        Try
            Dim Tipo As String
            Dim Movimiento_Pago_Factura = New frmMovimientoCajaPagoAbono(usua)
            Movimiento_Pago_Factura.Factura = columna!Num_Factura
            Movimiento_Pago_Factura.fecha = "" & columna!fecha
            Movimiento_Pago_Factura.Total = columna!total
            Tipo = columna!Tipo

            If columna!Tipo = "PVE" Then
                Movimiento_Pago_Factura.Tipo = "FVP"
            Else
                Movimiento_Pago_Factura.Tipo = "FVC"
            End If

            Movimiento_Pago_Factura.codmod = columna!Cod_Moneda
            Movimiento_Pago_Factura.ShowDialog()

            If Movimiento_Pago_Factura.Registra Then
                Dim Cx As New Conexion

                If Movimiento_Pago_Factura.RegistrarOpcionesPago() And (Cx.UpdateRecords("Ventas", "FacturaCancelado = 1, Num_Apertura = " & Movimiento_Pago_Factura.NApertura, "Num_Factura = " & Movimiento_Pago_Factura.Factura & " and Tipo = '" & Tipo & "'")) = "" Then
                    Movimiento_Pago_Factura.DataSet_Opciones_Pago1.Detalle_pago_caja.Clear()
                    Movimiento_Pago_Factura.DataSet_Opciones_Pago1.OpcionesDePago.Clear()
                    MsgBox("Factura registrada satisfactoriamente...")
                    Dim strFactura As String = Movimiento_Pago_Factura.Factura
                    Cx.FacturaCancelada(strFactura)
                End If
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function

    Private Sub frmMovimientoCajaPago_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")

            Me.dafactura.Fill(Me.Dsfactura21, "ventas")
            Refrescar()

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Refrescar()
        Try
            Dsfactura21.Clear()
            dafactura.Fill(Me.Dsfactura21, "ventas")
            BindingContext(Me.Dsfactura21, "ventas").Position = Me.BindingContext(Me.Dsfactura21, "ventas").Count
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1
                Refrescar()
            Case 7
                Close()
        End Select
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Refrescar()
    End Sub

    Private Sub DGFactpendiente_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGFactpendiente.DoubleClick
        Try
            Dim hi As DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo = _
                    AdvBandedGridView1.CalcHitInfo(CType(DGFactpendiente, Control).PointToClient(Control.MousePosition))
            Me.DGFactpendiente.Enabled = False
            If hi.RowHandle >= 0 Then

                BuscarById(AdvBandedGridView1.GetDataRow(hi.RowHandle))
                Me.DialogResult = DialogResult.OK

            ElseIf AdvBandedGridView1.FocusedRowHandle >= 0 Then
                BuscarById(AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle))
                Me.DialogResult = DialogResult.OK
            Else
                BuscarById(Nothing)
            End If


        Catch ex As SystemException
            MsgBox(ex.Message)
        Finally
            Refrescar()
            Me.DGFactpendiente.Enabled = True
        End Try
    End Sub

End Class
