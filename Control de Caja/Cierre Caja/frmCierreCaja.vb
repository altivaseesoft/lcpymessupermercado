Imports System.data.SqlClient
Imports System.Data
Imports System.Windows.Forms
Public Class frmCierreCaja
    'Inherits System.Windows.Forms.Form
    Inherits FrmPlantilla
    Public tabla As New DataTable
    Public tablaresumen As New DataTable
    Public tablacierre As New DataTable
    Public Usuario As String
    Public Cedula_Usuario As String
    Public SubTotal As Double
    Public Total As Double
    Public Devolucion As Double
    Public Vuelto As Double

    Dim PMU As New PerfilModulo_Class


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents btndone As System.Windows.Forms.Button
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents dacajeros As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtcodcajero As System.Windows.Forms.TextBox
    Friend WithEvents DTPFechafinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents DTPFechainicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents TimeEdit1 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents TimeEdit2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents daAperturacaja As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daAperturatope As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daOpcionesdepago As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daDenominacionApertura As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents gcMontoApertura As DevExpress.XtraGrid.GridControl
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dgOperaciones As System.Windows.Forms.DataGrid
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Dscierrecaja1 As Dscierrecaja
    Friend WithEvents daCierrecaja As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents chkAnulado As System.Windows.Forms.CheckBox
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daVenta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daDetalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents lblnumerocierre As System.Windows.Forms.Label
    Friend WithEvents lblnumcierre As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblanulado As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents dgCierre As System.Windows.Forms.DataGrid
    Friend WithEvents txtnomcajero As System.Windows.Forms.Label
    Friend WithEvents txtcodaperturacajero As System.Windows.Forms.Label
    Friend WithEvents txtfechaapertura As System.Windows.Forms.Label
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterDenominacionesApertura As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents colMonto_Total As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMonedaNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents dgResumen As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    'Protected Friend WithEvents ImageList As System.Windows.Forms.ImageList
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCierreCaja))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit
        Me.Dscierrecaja1 = New Dscierrecaja
        Me.txtnomcajero = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtcodcajero = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.TimeEdit2 = New DevExpress.XtraEditors.TimeEdit
        Me.TimeEdit1 = New DevExpress.XtraEditors.TimeEdit
        Me.DTPFechafinal = New System.Windows.Forms.DateTimePicker
        Me.DTPFechainicial = New System.Windows.Forms.DateTimePicker
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.btndone = New System.Windows.Forms.Button
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.dacajeros = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.daAperturacaja = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtfechaapertura = New System.Windows.Forms.Label
        Me.txtcodaperturacajero = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.gcMontoApertura = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMonto_Total = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMonedaNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand
        Me.daAperturatope = New System.Data.SqlClient.SqlDataAdapter
        Me.daOpcionesdepago = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand9 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.dgResumen = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand10 = New System.Data.SqlClient.SqlCommand
        Me.daDenominacionApertura = New System.Data.SqlClient.SqlDataAdapter
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.dgOperaciones = New System.Windows.Forms.DataGrid
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand12 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand12 = New System.Data.SqlClient.SqlCommand
        Me.daMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.dgCierre = New System.Windows.Forms.DataGrid
        Me.daCierrecaja = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand13 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand13 = New System.Data.SqlClient.SqlCommand
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.TextBox7 = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.chkAnulado = New System.Windows.Forms.CheckBox
        Me.lblnumerocierre = New System.Windows.Forms.Label
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand11 = New System.Data.SqlClient.SqlCommand
        Me.daVenta = New System.Data.SqlClient.SqlDataAdapter
        Me.daDetalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand14 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand14 = New System.Data.SqlClient.SqlCommand
        Me.lblnumcierre = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.lblanulado = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtNombreUsuario = New System.Windows.Forms.Label
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand15 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand15 = New System.Data.SqlClient.SqlCommand
        Me.AdapterDenominacionesApertura = New System.Data.SqlClient.SqlDataAdapter
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.GroupBox1.SuspendLayout()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dscierrecaja1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.gcMontoApertura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgResumen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgOperaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.dgCierre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarExcel
        '
        Me.ToolBarExcel.Enabled = False
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 468)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(698, 52)
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.DataMember = "cierrecaja"
        Me.DataNavigator.DataSource = Me.Dscierrecaja1
        Me.DataNavigator.Location = New System.Drawing.Point(540, 472)
        Me.DataNavigator.Name = "DataNavigator"
        Me.DataNavigator.Visible = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(698, 32)
        Me.TituloModulo.Text = "Cierre de Caja"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.DateEdit1)
        Me.GroupBox1.Controls.Add(Me.txtnomcajero)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtcodcajero)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TimeEdit2)
        Me.GroupBox1.Controls.Add(Me.TimeEdit1)
        Me.GroupBox1.Controls.Add(Me.DTPFechafinal)
        Me.GroupBox1.Controls.Add(Me.DTPFechainicial)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.btndone)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox1.Location = New System.Drawing.Point(8, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(256, 96)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Per�odo Cierre"
        '
        'DateEdit1
        '
        Me.DateEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.Dscierrecaja1, "cierrecaja.Fecha"))
        Me.DateEdit1.EditValue = New Date(2006, 5, 10, 0, 0, 0, 0)
        Me.DateEdit1.Location = New System.Drawing.Point(152, 32)
        Me.DateEdit1.Name = "DateEdit1"
        '
        'DateEdit1.Properties
        '
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.Enabled = False
        Me.DateEdit1.Size = New System.Drawing.Size(88, 19)
        Me.DateEdit1.TabIndex = 5
        '
        'Dscierrecaja1
        '
        Me.Dscierrecaja1.DataSetName = "dscierrecaja"
        Me.Dscierrecaja1.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'txtnomcajero
        '
        Me.txtnomcajero.BackColor = System.Drawing.Color.Transparent
        Me.txtnomcajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.Nombre"))
        Me.txtnomcajero.Location = New System.Drawing.Point(8, 72)
        Me.txtnomcajero.Name = "txtnomcajero"
        Me.txtnomcajero.Size = New System.Drawing.Size(240, 16)
        Me.txtnomcajero.TabIndex = 14
        Me.txtnomcajero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label25.Location = New System.Drawing.Point(152, 16)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(88, 16)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Fecha Cierre"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(112, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "C�digo Cajero"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtcodcajero
        '
        Me.txtcodcajero.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtcodcajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.Cajera"))
        Me.txtcodcajero.Enabled = False
        Me.txtcodcajero.ForeColor = System.Drawing.Color.Blue
        Me.txtcodcajero.Location = New System.Drawing.Point(8, 32)
        Me.txtcodcajero.Multiline = True
        Me.txtcodcajero.Name = "txtcodcajero"
        Me.txtcodcajero.Size = New System.Drawing.Size(112, 16)
        Me.txtcodcajero.TabIndex = 4
        Me.txtcodcajero.Text = ""
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.Location = New System.Drawing.Point(8, 56)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(240, 16)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Nombre"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TimeEdit2
        '
        Me.TimeEdit2.EditValue = New Date(2006, 3, 15, 11, 33, 32, 375)
        Me.TimeEdit2.Location = New System.Drawing.Point(496, 56)
        Me.TimeEdit2.Name = "TimeEdit2"
        '
        'TimeEdit2.Properties
        '
        Me.TimeEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.TimeEdit2.Properties.UseCtrlIncrement = False
        Me.TimeEdit2.Size = New System.Drawing.Size(96, 19)
        Me.TimeEdit2.TabIndex = 12
        '
        'TimeEdit1
        '
        Me.TimeEdit1.EditValue = New Date(2006, 3, 15, 11, 33, 32, 375)
        Me.TimeEdit1.Location = New System.Drawing.Point(496, 32)
        Me.TimeEdit1.Name = "TimeEdit1"
        '
        'TimeEdit1.Properties
        '
        Me.TimeEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.TimeEdit1.Properties.UseCtrlIncrement = False
        Me.TimeEdit1.Size = New System.Drawing.Size(96, 19)
        Me.TimeEdit1.TabIndex = 8
        '
        'DTPFechafinal
        '
        Me.DTPFechafinal.Enabled = False
        Me.DTPFechafinal.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DTPFechafinal.Location = New System.Drawing.Point(360, 56)
        Me.DTPFechafinal.MaxDate = New Date(2017, 1, 1, 0, 0, 0, 0)
        Me.DTPFechafinal.MinDate = New Date(2006, 1, 1, 0, 0, 0, 0)
        Me.DTPFechafinal.Name = "DTPFechafinal"
        Me.DTPFechafinal.Size = New System.Drawing.Size(104, 20)
        Me.DTPFechafinal.TabIndex = 11
        Me.DTPFechafinal.Value = New Date(2006, 1, 1, 0, 0, 0, 0)
        '
        'DTPFechainicial
        '
        Me.DTPFechainicial.Enabled = False
        Me.DTPFechainicial.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DTPFechainicial.Location = New System.Drawing.Point(360, 32)
        Me.DTPFechainicial.MaxDate = New Date(2017, 1, 1, 0, 0, 0, 0)
        Me.DTPFechainicial.MinDate = New Date(2006, 1, 1, 0, 0, 0, 0)
        Me.DTPFechainicial.Name = "DTPFechainicial"
        Me.DTPFechainicial.Size = New System.Drawing.Size(104, 20)
        Me.DTPFechainicial.TabIndex = 7
        Me.DTPFechainicial.Value = New Date(2006, 1, 1, 0, 0, 0, 0)
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Location = New System.Drawing.Point(304, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 16)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Final"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(304, 32)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Inicial"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(496, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Hora"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(368, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Fecha"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btndone
        '
        Me.btndone.Enabled = False
        Me.btndone.Location = New System.Drawing.Point(624, 24)
        Me.btndone.Name = "btndone"
        Me.btndone.Size = New System.Drawing.Size(32, 56)
        Me.btndone.TabIndex = 13
        Me.btndone.Text = "OK"
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Proveedores WHERE (CodigoProv = @Original_CodigoProv) AND (Cedula = @" & _
        "Original_Cedula) AND (Contacto = @Original_Contacto) AND (CostoTotal = @Original" & _
        "_CostoTotal) AND (Direccion = @Original_Direccion) AND (Email = @Original_Email)" & _
        " AND (Fax1 = @Original_Fax1) AND (Fax2 = @Original_Fax2) AND (ImpIncluido = @Ori" & _
        "ginal_ImpIncluido) AND (MontoCredito = @Original_MontoCredito) AND (Nombre = @Or" & _
        "iginal_Nombre) AND (Observaciones = @Original_Observaciones) AND (PedidosMes = @" & _
        "Original_PedidosMes) AND (Plazo = @Original_Plazo) AND (Telefono1 = @Original_Te" & _
        "lefono1) AND (Telefono2 = @Original_Telefono2) AND (Telefono_Cont = @Original_Te" & _
        "lefono_Cont)"
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodigoProv", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoProv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contacto", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contacto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoTotal", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoTotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ImpIncluido", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ImpIncluido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoCredito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PedidosMes", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PedidosMes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Plazo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono_Cont", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_Cont", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Inventario WHERE (Codigo = @Original_Codigo) AND (Barras = @Original_" & _
        "Barras) AND (BodegaConsignacion = @Original_BodegaConsignacion) AND (CodMarca = " & _
        "@Original_CodMarca) AND (CodPresentacion = @Original_CodPresentacion) AND (Costo" & _
        " = @Original_Costo) AND (Descripcion = @Original_Descripcion) AND (Existencia = " & _
        "@Original_Existencia) AND (ExistenciaBodega = @Original_ExistenciaBodega) AND (F" & _
        "letes = @Original_Fletes) AND (IVenta = @Original_IVenta) AND (Maxima = @Origina" & _
        "l_Maxima) AND (Minima = @Original_Minima) AND (MonedaCosto = @Original_MonedaCos" & _
        "to) AND (MonedaVenta = @Original_MonedaVenta) AND (Observaciones = @Original_Obs" & _
        "ervaciones) AND (OtrosCargos = @Original_OtrosCargos) AND (PrecioBase = @Origina" & _
        "l_PrecioBase) AND (Precio_A = @Original_Precio_A) AND (Precio_B = @Original_Prec" & _
        "io_B) AND (Precio_C = @Original_Precio_C) AND (Precio_D = @Original_Precio_D) AN" & _
        "D (PresentaCant = @Original_PresentaCant) AND (PuntoMedio = @Original_PuntoMedio" & _
        ") AND (SubFamilia = @Original_SubFamilia) AND (SubUbicacion = @Original_SubUbica" & _
        "cion)"
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BodegaConsignacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BodegaConsignacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMarca", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMarca", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodPresentacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodPresentacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Costo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Costo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExistenciaBodega", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExistenciaBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fletes", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fletes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Maxima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maxima", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Minima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Minima", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaCosto", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaCosto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaVenta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OtrosCargos", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OtrosCargos", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PrecioBase", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioBase", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_A", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_A", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_B", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_B", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_C", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_C", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_D", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_D", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PresentaCant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PresentaCant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PuntoMedio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PuntoMedio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubFamilia", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubFamilia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubUbicacion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubUbicacion", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE Usuarios SET Cedula = @Cedula, Nombre = @Nombre, Clave_Entrada = @Clave_En" & _
        "trada, Clave_Interna = @Clave_Interna, Perfil = @Perfil, Venta = @Venta, Anu_Ven" & _
        "ta = @Anu_Venta, Anu_Cotizacion = @Anu_Cotizacion, CambiarPrecio = @CambiarPreci" & _
        "o, Porc_Precio = @Porc_Precio, Aplicar_Desc = @Aplicar_Desc, Porc_Desc = @Porc_D" & _
        "esc, Exist_Negativa = @Exist_Negativa WHERE (Cedula = @Original_Cedula) AND (Anu" & _
        "_Cotizacion = @Original_Anu_Cotizacion) AND (Anu_Venta = @Original_Anu_Venta) AN" & _
        "D (Aplicar_Desc = @Original_Aplicar_Desc) AND (CambiarPrecio = @Original_Cambiar" & _
        "Precio) AND (Clave_Entrada = @Original_Clave_Entrada) AND (Clave_Interna = @Orig" & _
        "inal_Clave_Interna) AND (Exist_Negativa = @Original_Exist_Negativa) AND (Nombre " & _
        "= @Original_Nombre) AND (Perfil = @Original_Perfil) AND (Porc_Desc = @Original_P" & _
        "orc_Desc) AND (Porc_Precio = @Original_Porc_Precio) AND (Venta = @Original_Venta" & _
        "); SELECT Cedula, Nombre, Clave_Entrada, Clave_Interna, Perfil, Venta, Anu_Venta" & _
        ", Anu_Cotizacion, CambiarPrecio, Porc_Precio, Aplicar_Desc, Porc_Desc, Exist_Neg" & _
        "ativa FROM Usuarios WHERE (Cedula = @Cedula)"
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Clave_Entrada", System.Data.SqlDbType.VarChar, 50, "Clave_Entrada"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Clave_Interna", System.Data.SqlDbType.VarChar, 50, "Clave_Interna"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Perfil", System.Data.SqlDbType.VarChar, 250, "Perfil"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Venta", System.Data.SqlDbType.VarChar, 2, "Venta"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anu_Venta", System.Data.SqlDbType.VarChar, 2, "Anu_Venta"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anu_Cotizacion", System.Data.SqlDbType.VarChar, 2, "Anu_Cotizacion"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CambiarPrecio", System.Data.SqlDbType.VarChar, 2, "CambiarPrecio"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Porc_Precio", System.Data.SqlDbType.Float, 8, "Porc_Precio"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Aplicar_Desc", System.Data.SqlDbType.VarChar, 2, "Aplicar_Desc"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Porc_Desc", System.Data.SqlDbType.Float, 8, "Porc_Desc"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Exist_Negativa", System.Data.SqlDbType.VarChar, 2, "Exist_Negativa"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anu_Cotizacion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anu_Cotizacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anu_Venta", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anu_Venta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Aplicar_Desc", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Aplicar_Desc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CambiarPrecio", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CambiarPrecio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Clave_Entrada", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Clave_Interna", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Interna", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Exist_Negativa", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Exist_Negativa", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Perfil", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Perfil", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Porc_Desc", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Porc_Desc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Porc_Precio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Porc_Precio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Venta", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Venta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE Proveedores SET CodigoProv = @CodigoProv, Cedula = @Cedula, Nombre = @Nomb" & _
        "re, Contacto = @Contacto, Telefono_Cont = @Telefono_Cont, Observaciones = @Obser" & _
        "vaciones, Telefono1 = @Telefono1, Telefono2 = @Telefono2, Fax1 = @Fax1, Fax2 = @" & _
        "Fax2, Email = @Email, Direccion = @Direccion, MontoCredito = @MontoCredito, Plaz" & _
        "o = @Plazo, CostoTotal = @CostoTotal, ImpIncluido = @ImpIncluido, PedidosMes = @" & _
        "PedidosMes WHERE (CodigoProv = @Original_CodigoProv) AND (Cedula = @Original_Ced" & _
        "ula) AND (Contacto = @Original_Contacto) AND (CostoTotal = @Original_CostoTotal)" & _
        " AND (Direccion = @Original_Direccion) AND (Email = @Original_Email) AND (Fax1 =" & _
        " @Original_Fax1) AND (Fax2 = @Original_Fax2) AND (ImpIncluido = @Original_ImpInc" & _
        "luido) AND (MontoCredito = @Original_MontoCredito) AND (Nombre = @Original_Nombr" & _
        "e) AND (Observaciones = @Original_Observaciones) AND (PedidosMes = @Original_Ped" & _
        "idosMes) AND (Plazo = @Original_Plazo) AND (Telefono1 = @Original_Telefono1) AND" & _
        " (Telefono2 = @Original_Telefono2) AND (Telefono_Cont = @Original_Telefono_Cont)" & _
        "; SELECT CodigoProv, Cedula, Nombre, Contacto, Telefono_Cont, Observaciones, Tel" & _
        "efono1, Telefono2, Fax1, Fax2, Email, Direccion, MontoCredito, Plazo, CostoTotal" & _
        ", ImpIncluido, PedidosMes FROM Proveedores WHERE (CodigoProv = @CodigoProv)"
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodigoProv", System.Data.SqlDbType.Int, 4, "CodigoProv"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 20, "Cedula"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contacto", System.Data.SqlDbType.VarChar, 250, "Contacto"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono_Cont", System.Data.SqlDbType.VarChar, 15, "Telefono_Cont"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono1", System.Data.SqlDbType.VarChar, 15, "Telefono1"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono2", System.Data.SqlDbType.VarChar, 15, "Telefono2"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax1", System.Data.SqlDbType.VarChar, 15, "Fax1"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax2", System.Data.SqlDbType.VarChar, 15, "Fax2"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 100, "Email"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 250, "Direccion"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoCredito", System.Data.SqlDbType.Float, 8, "MontoCredito"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Plazo", System.Data.SqlDbType.Int, 4, "Plazo"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoTotal", System.Data.SqlDbType.Bit, 1, "CostoTotal"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ImpIncluido", System.Data.SqlDbType.Bit, 1, "ImpIncluido"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PedidosMes", System.Data.SqlDbType.Int, 4, "PedidosMes"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodigoProv", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoProv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contacto", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contacto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoTotal", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoTotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ImpIncluido", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ImpIncluido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoCredito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PedidosMes", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PedidosMes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Plazo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono_Cont", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_Cont", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM detalle_ordencompra WHERE (Id = @Original_Id) AND (Cantidad = @Origin" & _
        "al_Cantidad) AND (Codigo = @Original_Codigo) AND (CostoUnitario = @Original_Cost" & _
        "oUnitario) AND (Descripcion = @Original_Descripcion) AND (Descuento = @Original_" & _
        "Descuento) AND (Orden = @Original_Orden) AND (TotalCompra = @Original_TotalCompr" & _
        "a) AND (impuesto = @Original_impuesto)"
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoUnitario", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoUnitario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Orden", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Orden", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_impuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "impuesto", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Usuarios WHERE (Cedula = @Original_Cedula) AND (Anu_Cotizacion = @Ori" & _
        "ginal_Anu_Cotizacion) AND (Anu_Venta = @Original_Anu_Venta) AND (Aplicar_Desc = " & _
        "@Original_Aplicar_Desc) AND (CambiarPrecio = @Original_CambiarPrecio) AND (Clave" & _
        "_Entrada = @Original_Clave_Entrada) AND (Clave_Interna = @Original_Clave_Interna" & _
        ") AND (Exist_Negativa = @Original_Exist_Negativa) AND (Nombre = @Original_Nombre" & _
        ") AND (Perfil = @Original_Perfil) AND (Porc_Desc = @Original_Porc_Desc) AND (Por" & _
        "c_Precio = @Original_Porc_Precio) AND (Venta = @Original_Venta)"
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anu_Cotizacion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anu_Cotizacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anu_Venta", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anu_Venta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Aplicar_Desc", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Aplicar_Desc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CambiarPrecio", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CambiarPrecio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Clave_Entrada", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Clave_Interna", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Interna", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Exist_Negativa", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Exist_Negativa", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Perfil", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Perfil", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Porc_Desc", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Porc_Desc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Porc_Precio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Porc_Precio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Venta", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Venta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE ordencompra SET Orden = @Orden, Proveedor = @Proveedor, Nombre = @Nombre, " & _
        "Fecha = @Fecha, contado = @contado, credito = @credito, diascredito = @diascredi" & _
        "to, Plazo = @Plazo, SubTotal = @SubTotal, Descuento = @Descuento, Impuesto = @Im" & _
        "puesto, Monto = @Monto, Observaciones = @Observaciones, Usuario = @Usuario, Nomb" & _
        "reUsuario = @NombreUsuario, entregar = @entregar WHERE (Orden = @Original_Orden)" & _
        " AND (Descuento = @Original_Descuento) AND (Fecha = @Original_Fecha) AND (Impues" & _
        "to = @Original_Impuesto) AND (Monto = @Original_Monto) AND (Nombre = @Original_N" & _
        "ombre) AND (NombreUsuario = @Original_NombreUsuario) AND (Observaciones = @Origi" & _
        "nal_Observaciones) AND (Plazo = @Original_Plazo) AND (Proveedor = @Original_Prov" & _
        "eedor) AND (SubTotal = @Original_SubTotal) AND (Usuario = @Original_Usuario) AND" & _
        " (contado = @Original_contado) AND (credito = @Original_credito) AND (diascredit" & _
        "o = @Original_diascredito) AND (entregar = @Original_entregar); SELECT Orden, Pr" & _
        "oveedor, Nombre, Fecha, contado, credito, diascredito, Plazo, SubTotal, Descuent" & _
        "o, Impuesto, Monto, Observaciones, Usuario, NombreUsuario, entregar FROM ordenco" & _
        "mpra WHERE (Orden = @Orden)"
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Orden", System.Data.SqlDbType.Float, 8, "Orden"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 4, "Proveedor"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@contado", System.Data.SqlDbType.Bit, 1, "contado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@credito", System.Data.SqlDbType.Bit, 1, "credito"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@diascredito", System.Data.SqlDbType.Float, 8, "diascredito"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Plazo", System.Data.SqlDbType.Int, 4, "Plazo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 8, "SubTotal"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 8, "Descuento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Impuesto", System.Data.SqlDbType.Float, 8, "Impuesto"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@entregar", System.Data.SqlDbType.VarChar, 255, "entregar"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Orden", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Orden", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Impuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Impuesto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Plazo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Proveedor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubTotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_contado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "contado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_credito", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "credito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_diascredito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "diascredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_entregar", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "entregar", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE detalle_ordencompra SET Orden = @Orden, Codigo = @Codigo, Descripcion = @D" & _
        "escripcion, CostoUnitario = @CostoUnitario, Cantidad = @Cantidad, TotalCompra = " & _
        "@TotalCompra, impuesto = @impuesto, Descuento = @Descuento WHERE (Id = @Original" & _
        "_Id) AND (Cantidad = @Original_Cantidad) AND (Codigo = @Original_Codigo) AND (Co" & _
        "stoUnitario = @Original_CostoUnitario) AND (Descripcion = @Original_Descripcion)" & _
        " AND (Descuento = @Original_Descuento) AND (Orden = @Original_Orden) AND (TotalC" & _
        "ompra = @Original_TotalCompra) AND (impuesto = @Original_impuesto); SELECT Id, O" & _
        "rden, Codigo, Descripcion, CostoUnitario, Cantidad, TotalCompra, impuesto, Descu" & _
        "ento FROM detalle_ordencompra WHERE (Id = @Id)"
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Orden", System.Data.SqlDbType.Float, 8, "Orden"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoUnitario", System.Data.SqlDbType.Float, 8, "CostoUnitario"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalCompra", System.Data.SqlDbType.Float, 8, "TotalCompra"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Float, 8, "impuesto"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 8, "Descuento"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoUnitario", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoUnitario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Orden", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Orden", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_impuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "impuesto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id"))
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM ordencompra WHERE (Orden = @Original_Orden) AND (Descuento = @Origina" & _
        "l_Descuento) AND (Fecha = @Original_Fecha) AND (Impuesto = @Original_Impuesto) A" & _
        "ND (Monto = @Original_Monto) AND (Nombre = @Original_Nombre) AND (NombreUsuario " & _
        "= @Original_NombreUsuario) AND (Observaciones = @Original_Observaciones) AND (Pl" & _
        "azo = @Original_Plazo) AND (Proveedor = @Original_Proveedor) AND (SubTotal = @Or" & _
        "iginal_SubTotal) AND (Usuario = @Original_Usuario) AND (contado = @Original_cont" & _
        "ado) AND (credito = @Original_credito) AND (diascredito = @Original_diascredito)" & _
        " AND (entregar = @Original_entregar)"
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Orden", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Orden", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Impuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Impuesto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Plazo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Proveedor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubTotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_contado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "contado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_credito", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "credito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_diascredito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "diascredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_entregar", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "entregar", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Inventario SET Codigo = @Codigo, Barras = @Barras, Descripcion = @Descripc" & _
        "ion, PresentaCant = @PresentaCant, CodPresentacion = @CodPresentacion, CodMarca " & _
        "= @CodMarca, SubFamilia = @SubFamilia, Minima = @Minima, PuntoMedio = @PuntoMedi" & _
        "o, Maxima = @Maxima, Existencia = @Existencia, ExistenciaBodega = @ExistenciaBod" & _
        "ega, BodegaConsignacion = @BodegaConsignacion, SubUbicacion = @SubUbicacion, Obs" & _
        "ervaciones = @Observaciones, MonedaCosto = @MonedaCosto, PrecioBase = @PrecioBas" & _
        "e, Fletes = @Fletes, OtrosCargos = @OtrosCargos, Costo = @Costo, MonedaVenta = @" & _
        "MonedaVenta, Precio_A = @Precio_A, Precio_B = @Precio_B, Precio_C = @Precio_C, P" & _
        "recio_D = @Precio_D, IVenta = @IVenta WHERE (Codigo = @Original_Codigo) AND (Bar" & _
        "ras = @Original_Barras) AND (BodegaConsignacion = @Original_BodegaConsignacion) " & _
        "AND (CodMarca = @Original_CodMarca) AND (CodPresentacion = @Original_CodPresenta" & _
        "cion) AND (Costo = @Original_Costo) AND (Descripcion = @Original_Descripcion) AN" & _
        "D (Existencia = @Original_Existencia) AND (ExistenciaBodega = @Original_Existenc" & _
        "iaBodega) AND (Fletes = @Original_Fletes) AND (IVenta = @Original_IVenta) AND (M" & _
        "axima = @Original_Maxima) AND (Minima = @Original_Minima) AND (MonedaCosto = @Or" & _
        "iginal_MonedaCosto) AND (MonedaVenta = @Original_MonedaVenta) AND (Observaciones" & _
        " = @Original_Observaciones) AND (OtrosCargos = @Original_OtrosCargos) AND (Preci" & _
        "oBase = @Original_PrecioBase) AND (Precio_A = @Original_Precio_A) AND (Precio_B " & _
        "= @Original_Precio_B) AND (Precio_C = @Original_Precio_C) AND (Precio_D = @Origi" & _
        "nal_Precio_D) AND (PresentaCant = @Original_PresentaCant) AND (PuntoMedio = @Ori" & _
        "ginal_PuntoMedio) AND (SubFamilia = @Original_SubFamilia) AND (SubUbicacion = @O" & _
        "riginal_SubUbicacion); SELECT Codigo, Barras, Descripcion, PresentaCant, CodPres" & _
        "entacion, CodMarca, SubFamilia, Minima, PuntoMedio, Maxima, Existencia, Existenc" & _
        "iaBodega, BodegaConsignacion, SubUbicacion, Observaciones, MonedaCosto, PrecioBa" & _
        "se, Fletes, OtrosCargos, Costo, MonedaVenta, Precio_A, Precio_B, Precio_C, Preci" & _
        "o_D, IVenta FROM Inventario WHERE (Codigo = @Codigo)"
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PresentaCant", System.Data.SqlDbType.Float, 8, "PresentaCant"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodPresentacion", System.Data.SqlDbType.Int, 4, "CodPresentacion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMarca", System.Data.SqlDbType.Int, 4, "CodMarca"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubFamilia", System.Data.SqlDbType.VarChar, 10, "SubFamilia"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Minima", System.Data.SqlDbType.Float, 8, "Minima"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PuntoMedio", System.Data.SqlDbType.Float, 8, "PuntoMedio"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Maxima", System.Data.SqlDbType.Float, 8, "Maxima"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExistenciaBodega", System.Data.SqlDbType.Float, 8, "ExistenciaBodega"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BodegaConsignacion", System.Data.SqlDbType.Int, 4, "BodegaConsignacion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubUbicacion", System.Data.SqlDbType.VarChar, 10, "SubUbicacion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaCosto", System.Data.SqlDbType.Int, 4, "MonedaCosto"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PrecioBase", System.Data.SqlDbType.Float, 8, "PrecioBase"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fletes", System.Data.SqlDbType.Float, 8, "Fletes"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OtrosCargos", System.Data.SqlDbType.Float, 8, "OtrosCargos"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 8, "Costo"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaVenta", System.Data.SqlDbType.Int, 4, "MonedaVenta"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_A", System.Data.SqlDbType.Float, 8, "Precio_A"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_B", System.Data.SqlDbType.Float, 8, "Precio_B"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_C", System.Data.SqlDbType.Float, 8, "Precio_C"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_D", System.Data.SqlDbType.Float, 8, "Precio_D"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IVenta", System.Data.SqlDbType.Float, 8, "IVenta"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BodegaConsignacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BodegaConsignacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMarca", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMarca", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodPresentacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodPresentacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Costo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Costo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExistenciaBodega", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExistenciaBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fletes", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fletes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Maxima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maxima", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Minima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Minima", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaCosto", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaCosto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaVenta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OtrosCargos", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OtrosCargos", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PrecioBase", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioBase", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_A", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_A", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_B", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_B", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_C", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_C", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_D", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_D", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PresentaCant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PresentaCant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PuntoMedio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PuntoMedio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubFamilia", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubFamilia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubUbicacion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubUbicacion", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=HAZEL;packet size=4096;integrated security=SSPI;data source=""(loca" & _
        "l)"";persist security info=False;initial catalog=SEEPOS"
        '
        'dacajeros
        '
        Me.dacajeros.DeleteCommand = Me.SqlDeleteCommand6
        Me.dacajeros.InsertCommand = Me.SqlInsertCommand1
        Me.dacajeros.SelectCommand = Me.SqlSelectCommand1
        Me.dacajeros.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Clave_Entrada", "Clave_Entrada"), New System.Data.Common.DataColumnMapping("Clave_Interna", "Clave_Interna"), New System.Data.Common.DataColumnMapping("Perfil", "Perfil")})})
        Me.dacajeros.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM Usuarios WHERE (Cedula = @Original_Cedula) AND (Clave_Entrada = @Orig" & _
        "inal_Clave_Entrada) AND (Clave_Interna = @Original_Clave_Interna) AND (Nombre = " & _
        "@Original_Nombre) AND (Perfil = @Original_Perfil)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Clave_Entrada", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Clave_Interna", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Interna", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Perfil", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Perfil", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Usuarios(Cedula, Nombre, Clave_Entrada, Clave_Interna, Perfil) VALUES" & _
        " (@Cedula, @Nombre, @Clave_Entrada, @Clave_Interna, @Perfil); SELECT Cedula, Nom" & _
        "bre, Clave_Entrada, Clave_Interna, Perfil FROM Usuarios WHERE (Cedula = @Cedula)" & _
        ""
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Clave_Entrada", System.Data.SqlDbType.VarChar, 30, "Clave_Entrada"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Clave_Interna", System.Data.SqlDbType.VarChar, 30, "Clave_Interna"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Perfil", System.Data.SqlDbType.VarChar, 75, "Perfil"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Cedula, Nombre, Clave_Entrada, Clave_Interna, Perfil FROM Usuarios"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE Usuarios SET Cedula = @Cedula, Nombre = @Nombre, Clave_Entrada = @Clave_En" & _
        "trada, Clave_Interna = @Clave_Interna, Perfil = @Perfil WHERE (Cedula = @Origina" & _
        "l_Cedula) AND (Clave_Entrada = @Original_Clave_Entrada) AND (Clave_Interna = @Or" & _
        "iginal_Clave_Interna) AND (Nombre = @Original_Nombre) AND (Perfil = @Original_Pe" & _
        "rfil); SELECT Cedula, Nombre, Clave_Entrada, Clave_Interna, Perfil FROM Usuarios" & _
        " WHERE (Cedula = @Cedula)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Clave_Entrada", System.Data.SqlDbType.VarChar, 30, "Clave_Entrada"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Clave_Interna", System.Data.SqlDbType.VarChar, 30, "Clave_Interna"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Perfil", System.Data.SqlDbType.VarChar, 75, "Perfil"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Clave_Entrada", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Clave_Interna", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Clave_Interna", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Perfil", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Perfil", System.Data.DataRowVersion.Original, Nothing))
        '
        'daAperturacaja
        '
        Me.daAperturacaja.DeleteCommand = Me.SqlDeleteCommand7
        Me.daAperturacaja.InsertCommand = Me.SqlInsertCommand2
        Me.daAperturacaja.SelectCommand = Me.SqlSelectCommand2
        Me.daAperturacaja.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "aperturacaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Estado", "Estado"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula")})})
        Me.daAperturacaja.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = "DELETE FROM aperturacaja WHERE (NApertura = @Original_NApertura) AND (Anulado = @" & _
        "Original_Anulado) AND (Cedula = @Original_Cedula) AND (Estado = @Original_Estado" & _
        ") AND (Fecha = @Original_Fecha) AND (Nombre = @Original_Nombre) AND (Observacion" & _
        "es = @Original_Observaciones)"
        Me.SqlDeleteCommand7.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO aperturacaja(Fecha, Nombre, Estado, Observaciones, Anulado, Cedula) V" & _
        "ALUES (@Fecha, @Nombre, @Estado, @Observaciones, @Anulado, @Cedula); SELECT NApe" & _
        "rtura, Fecha, Nombre, Estado, Observaciones, Anulado, Cedula FROM aperturacaja W" & _
        "HERE (NApertura = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT NApertura, Fecha, Nombre, Estado, Observaciones, Anulado, Cedula FROM aper" & _
        "turacaja"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = "UPDATE aperturacaja SET Fecha = @Fecha, Nombre = @Nombre, Estado = @Estado, Obser" & _
        "vaciones = @Observaciones, Anulado = @Anulado, Cedula = @Cedula WHERE (NApertura" & _
        " = @Original_NApertura) AND (Anulado = @Original_Anulado) AND (Cedula = @Origina" & _
        "l_Cedula) AND (Estado = @Original_Estado) AND (Fecha = @Original_Fecha) AND (Nom" & _
        "bre = @Original_Nombre) AND (Observaciones = @Original_Observaciones); SELECT NA" & _
        "pertura, Fecha, Nombre, Estado, Observaciones, Anulado, Cedula FROM aperturacaja" & _
        " WHERE (NApertura = @NApertura)"
        Me.SqlUpdateCommand7.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.VarChar, 1, "Estado"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Estado", System.Data.SqlDbType.VarChar, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Estado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"))
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.txtfechaapertura)
        Me.GroupBox2.Controls.Add(Me.txtcodaperturacajero)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(8, 129)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(256, 63)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Apertura"
        '
        'txtfechaapertura
        '
        Me.txtfechaapertura.BackColor = System.Drawing.Color.Transparent
        Me.txtfechaapertura.Location = New System.Drawing.Point(80, 40)
        Me.txtfechaapertura.Name = "txtfechaapertura"
        Me.txtfechaapertura.Size = New System.Drawing.Size(168, 16)
        Me.txtfechaapertura.TabIndex = 3
        Me.txtfechaapertura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtcodaperturacajero
        '
        Me.txtcodaperturacajero.BackColor = System.Drawing.Color.Transparent
        Me.txtcodaperturacajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.Apertura"))
        Me.txtcodaperturacajero.Location = New System.Drawing.Point(8, 40)
        Me.txtcodaperturacajero.Name = "txtcodaperturacajero"
        Me.txtcodaperturacajero.Size = New System.Drawing.Size(64, 16)
        Me.txtcodaperturacajero.TabIndex = 2
        Me.txtcodaperturacajero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(80, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Fecha / Hora"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(8, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "N�mero"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.gcMontoApertura)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox3.Location = New System.Drawing.Point(8, 201)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(256, 127)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Monto de Apertura"
        '
        'gcMontoApertura
        '
        Me.gcMontoApertura.BackColor = System.Drawing.Color.White
        Me.gcMontoApertura.DataMember = "DenominacionesAperturas"
        Me.gcMontoApertura.DataSource = Me.Dscierrecaja1
        '
        'gcMontoApertura.EmbeddedNavigator
        '
        Me.gcMontoApertura.EmbeddedNavigator.Name = ""
        Me.gcMontoApertura.Location = New System.Drawing.Point(8, 16)
        Me.gcMontoApertura.MainView = Me.GridView1
        Me.gcMontoApertura.Name = "gcMontoApertura"
        Me.gcMontoApertura.Size = New System.Drawing.Size(240, 104)
        Me.gcMontoApertura.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.gcMontoApertura.TabIndex = 0
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMonto_Total, Me.colMonedaNombre})
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colMonto_Total
        '
        Me.colMonto_Total.Caption = "Total"
        Me.colMonto_Total.FieldName = "Monto_Total"
        Me.colMonto_Total.Name = "colMonto_Total"
        Me.colMonto_Total.VisibleIndex = 1
        '
        'colMonedaNombre
        '
        Me.colMonedaNombre.Caption = "Moneda"
        Me.colMonedaNombre.FieldName = "MonedaNombre"
        Me.colMonedaNombre.Name = "colMonedaNombre"
        Me.colMonedaNombre.VisibleIndex = 0
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT id_total_tope, NApertura, CodMoneda, Monto_Tope, MonedaNombre FROM Apertur" & _
        "a_Total_Tope"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Apertura_Total_Tope(NApertura, CodMoneda, Monto_Tope, MonedaNombre) V" & _
        "ALUES (@NApertura, @CodMoneda, @Monto_Tope, @MonedaNombre); SELECT id_total_tope" & _
        ", NApertura, CodMoneda, Monto_Tope, MonedaNombre FROM Apertura_Total_Tope WHERE " & _
        "(id_total_tope = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto_Tope", System.Data.SqlDbType.Float, 8, "Monto_Tope"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = "UPDATE Apertura_Total_Tope SET NApertura = @NApertura, CodMoneda = @CodMoneda, Mo" & _
        "nto_Tope = @Monto_Tope, MonedaNombre = @MonedaNombre WHERE (id_total_tope = @Ori" & _
        "ginal_id_total_tope) AND (CodMoneda = @Original_CodMoneda) AND (MonedaNombre = @" & _
        "Original_MonedaNombre) AND (Monto_Tope = @Original_Monto_Tope) AND (NApertura = " & _
        "@Original_NApertura); SELECT id_total_tope, NApertura, CodMoneda, Monto_Tope, Mo" & _
        "nedaNombre FROM Apertura_Total_Tope WHERE (id_total_tope = @id_total_tope)"
        Me.SqlUpdateCommand8.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto_Tope", System.Data.SqlDbType.Float, 8, "Monto_Tope"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id_total_tope", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_total_tope", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto_Tope", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Tope", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id_total_tope", System.Data.SqlDbType.Int, 4, "id_total_tope"))
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = "DELETE FROM Apertura_Total_Tope WHERE (id_total_tope = @Original_id_total_tope) A" & _
        "ND (CodMoneda = @Original_CodMoneda) AND (MonedaNombre = @Original_MonedaNombre)" & _
        " AND (Monto_Tope = @Original_Monto_Tope) AND (NApertura = @Original_NApertura)"
        Me.SqlDeleteCommand8.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id_total_tope", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_total_tope", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto_Tope", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Tope", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing))
        '
        'daAperturatope
        '
        Me.daAperturatope.DeleteCommand = Me.SqlDeleteCommand8
        Me.daAperturatope.InsertCommand = Me.SqlInsertCommand3
        Me.daAperturatope.SelectCommand = Me.SqlSelectCommand3
        Me.daAperturatope.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Apertura_Total_Tope", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id_total_tope", "id_total_tope"), New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Monto_Tope", "Monto_Tope"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre")})})
        Me.daAperturatope.UpdateCommand = Me.SqlUpdateCommand8
        '
        'daOpcionesdepago
        '
        Me.daOpcionesdepago.DeleteCommand = Me.SqlDeleteCommand9
        Me.daOpcionesdepago.InsertCommand = Me.SqlInsertCommand4
        Me.daOpcionesdepago.SelectCommand = Me.SqlSelectCommand4
        Me.daOpcionesdepago.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "OpcionesDePago", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id", "id"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("TipoDocumento", "TipoDocumento"), New System.Data.Common.DataColumnMapping("MontoPago", "MontoPago"), New System.Data.Common.DataColumnMapping("FormaPago", "FormaPago"), New System.Data.Common.DataColumnMapping("Denominacion", "Denominacion"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Nombremoneda", "Nombremoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Numapertura", "Numapertura")})})
        Me.daOpcionesdepago.UpdateCommand = Me.SqlUpdateCommand9
        '
        'SqlDeleteCommand9
        '
        Me.SqlDeleteCommand9.CommandText = "DELETE FROM OpcionesDePago WHERE (id = @Original_id) AND (CodMoneda = @Original_C" & _
        "odMoneda OR @Original_CodMoneda IS NULL AND CodMoneda IS NULL) AND (Denominacion" & _
        " = @Original_Denominacion OR @Original_Denominacion IS NULL AND Denominacion IS " & _
        "NULL) AND (Documento = @Original_Documento) AND (Fecha = @Original_Fecha OR @Ori" & _
        "ginal_Fecha IS NULL AND Fecha IS NULL) AND (FormaPago = @Original_FormaPago OR @" & _
        "Original_FormaPago IS NULL AND FormaPago IS NULL) AND (MontoPago = @Original_Mon" & _
        "toPago OR @Original_MontoPago IS NULL AND MontoPago IS NULL) AND (Nombre = @Orig" & _
        "inal_Nombre OR @Original_Nombre IS NULL AND Nombre IS NULL) AND (Nombremoneda = " & _
        "@Original_Nombremoneda OR @Original_Nombremoneda IS NULL AND Nombremoneda IS NUL" & _
        "L) AND (Numapertura = @Original_Numapertura OR @Original_Numapertura IS NULL AND" & _
        " Numapertura IS NULL) AND (TipoCambio = @Original_TipoCambio OR @Original_TipoCa" & _
        "mbio IS NULL AND TipoCambio IS NULL) AND (TipoDocumento = @Original_TipoDocument" & _
        "o OR @Original_TipoDocumento IS NULL AND TipoDocumento IS NULL) AND (Usuario = @" & _
        "Original_Usuario OR @Original_Usuario IS NULL AND Usuario IS NULL)"
        Me.SqlDeleteCommand9.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoPago", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoPago", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombremoneda", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombremoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Numapertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numapertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO OpcionesDePago(Documento, TipoDocumento, MontoPago, FormaPago, Denomi" & _
        "nacion, Usuario, Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha, Numapertura" & _
        ") VALUES (@Documento, @TipoDocumento, @MontoPago, @FormaPago, @Denominacion, @Us" & _
        "uario, @Nombre, @CodMoneda, @Nombremoneda, @TipoCambio, @Fecha, @Numapertura); S" & _
        "ELECT id, Documento, TipoDocumento, MontoPago, FormaPago, Denominacion, Usuario," & _
        " Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha, Numapertura FROM OpcionesDe" & _
        "Pago WHERE (id = @@IDENTITY)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 50, "TipoDocumento"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoPago", System.Data.SqlDbType.Float, 8, "MontoPago"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.Float, 8, "Denominacion"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 75, "Usuario"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Float, 8, "CodMoneda"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombremoneda", System.Data.SqlDbType.VarChar, 50, "Nombremoneda"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Numapertura", System.Data.SqlDbType.Int, 4, "Numapertura"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT id, Documento, TipoDocumento, MontoPago, FormaPago, Denominacion, Usuario," & _
        " Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha, Numapertura FROM OpcionesDe" & _
        "Pago"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand9
        '
        Me.SqlUpdateCommand9.CommandText = "UPDATE OpcionesDePago SET Documento = @Documento, TipoDocumento = @TipoDocumento," & _
        " MontoPago = @MontoPago, FormaPago = @FormaPago, Denominacion = @Denominacion, U" & _
        "suario = @Usuario, Nombre = @Nombre, CodMoneda = @CodMoneda, Nombremoneda = @Nom" & _
        "bremoneda, TipoCambio = @TipoCambio, Fecha = @Fecha, Numapertura = @Numapertura " & _
        "WHERE (id = @Original_id) AND (CodMoneda = @Original_CodMoneda OR @Original_CodM" & _
        "oneda IS NULL AND CodMoneda IS NULL) AND (Denominacion = @Original_Denominacion " & _
        "OR @Original_Denominacion IS NULL AND Denominacion IS NULL) AND (Documento = @Or" & _
        "iginal_Documento) AND (Fecha = @Original_Fecha OR @Original_Fecha IS NULL AND Fe" & _
        "cha IS NULL) AND (FormaPago = @Original_FormaPago OR @Original_FormaPago IS NULL" & _
        " AND FormaPago IS NULL) AND (MontoPago = @Original_MontoPago OR @Original_MontoP" & _
        "ago IS NULL AND MontoPago IS NULL) AND (Nombre = @Original_Nombre OR @Original_N" & _
        "ombre IS NULL AND Nombre IS NULL) AND (Nombremoneda = @Original_Nombremoneda OR " & _
        "@Original_Nombremoneda IS NULL AND Nombremoneda IS NULL) AND (Numapertura = @Ori" & _
        "ginal_Numapertura OR @Original_Numapertura IS NULL AND Numapertura IS NULL) AND " & _
        "(TipoCambio = @Original_TipoCambio OR @Original_TipoCambio IS NULL AND TipoCambi" & _
        "o IS NULL) AND (TipoDocumento = @Original_TipoDocumento OR @Original_TipoDocumen" & _
        "to IS NULL AND TipoDocumento IS NULL) AND (Usuario = @Original_Usuario OR @Origi" & _
        "nal_Usuario IS NULL AND Usuario IS NULL); SELECT id, Documento, TipoDocumento, M" & _
        "ontoPago, FormaPago, Denominacion, Usuario, Nombre, CodMoneda, Nombremoneda, Tip" & _
        "oCambio, Fecha, Numapertura FROM OpcionesDePago WHERE (id = @id)"
        Me.SqlUpdateCommand9.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.VarChar, 50, "Documento"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 50, "TipoDocumento"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoPago", System.Data.SqlDbType.Float, 8, "MontoPago"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FormaPago", System.Data.SqlDbType.VarChar, 50, "FormaPago"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.Float, 8, "Denominacion"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 75, "Usuario"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Float, 8, "CodMoneda"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombremoneda", System.Data.SqlDbType.VarChar, 50, "Nombremoneda"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Numapertura", System.Data.SqlDbType.Int, 4, "Numapertura"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FormaPago", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FormaPago", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoPago", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoPago", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombremoneda", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombremoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Numapertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numapertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.BigInt, 8, "id"))
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.dgResumen)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox4.Location = New System.Drawing.Point(272, 32)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(424, 184)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Detalle Operaciones"
        '
        'dgResumen
        '
        '
        'dgResumen.EmbeddedNavigator
        '
        Me.dgResumen.EmbeddedNavigator.Name = ""
        Me.dgResumen.Location = New System.Drawing.Point(8, 16)
        Me.dgResumen.MainView = Me.GridView2
        Me.dgResumen.Name = "dgResumen"
        Me.dgResumen.Size = New System.Drawing.Size(408, 160)
        Me.dgResumen.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.TabIndex = 1
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn5, Me.GridColumn4, Me.GridColumn3, Me.GridColumn2, Me.GridColumn1})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowFilterPanel = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Factura"
        Me.GridColumn6.FieldName = "Factura"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.VisibleIndex = 0
        Me.GridColumn6.Width = 55
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Tipo"
        Me.GridColumn5.FieldName = "Tipo"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 46
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Moneda"
        Me.GridColumn4.FieldName = "Moneda"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 69
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Pago"
        Me.GridColumn3.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn3.FieldName = "Pago"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn3.VisibleIndex = 4
        Me.GridColumn3.Width = 69
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Forma Pago"
        Me.GridColumn2.FieldName = "Forma Pago"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.VisibleIndex = 3
        Me.GridColumn2.Width = 76
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Equivalencia"
        Me.GridColumn1.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn1.FieldName = "Equivalencia"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.VisibleIndex = 5
        Me.GridColumn1.Width = 79
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT Denominacion, Cantidad, Valor, id_denominacion, id_total_tope, Monto_Total" & _
        " FROM denominacionesapertura"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO denominacionesapertura(Denominacion, Cantidad, Valor, id_total_tope, " & _
        "Monto_Total) VALUES (@Denominacion, @Cantidad, @Valor, @id_total_tope, @Monto_To" & _
        "tal); SELECT Denominacion, Cantidad, Valor, id_denominacion, id_total_tope, Mont" & _
        "o_Total FROM denominacionesapertura WHERE (id_denominacion = @@IDENTITY)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.VarChar, 50, "Denominacion"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Int, 4, "Cantidad"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Valor", System.Data.SqlDbType.Float, 8, "Valor"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id_total_tope", System.Data.SqlDbType.Int, 4, "id_total_tope"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto_Total", System.Data.SqlDbType.Float, 8, "Monto_Total"))
        '
        'SqlUpdateCommand10
        '
        Me.SqlUpdateCommand10.CommandText = "UPDATE denominacionesapertura SET Denominacion = @Denominacion, Cantidad = @Canti" & _
        "dad, Valor = @Valor, id_total_tope = @id_total_tope, Monto_Total = @Monto_Total " & _
        "WHERE (id_denominacion = @Original_id_denominacion) AND (Cantidad = @Original_Ca" & _
        "ntidad) AND (Denominacion = @Original_Denominacion) AND (Monto_Total = @Original" & _
        "_Monto_Total) AND (Valor = @Original_Valor) AND (id_total_tope = @Original_id_to" & _
        "tal_tope); SELECT Denominacion, Cantidad, Valor, id_denominacion, id_total_tope," & _
        " Monto_Total FROM denominacionesapertura WHERE (id_denominacion = @id_denominaci" & _
        "on)"
        Me.SqlUpdateCommand10.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.VarChar, 50, "Denominacion"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Int, 4, "Cantidad"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Valor", System.Data.SqlDbType.Float, 8, "Valor"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id_total_tope", System.Data.SqlDbType.Int, 4, "id_total_tope"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto_Total", System.Data.SqlDbType.Float, 8, "Monto_Total"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id_denominacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_denominacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Valor", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Valor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id_total_tope", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_total_tope", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id_denominacion", System.Data.SqlDbType.Int, 4, "id_denominacion"))
        '
        'SqlDeleteCommand10
        '
        Me.SqlDeleteCommand10.CommandText = "DELETE FROM denominacionesapertura WHERE (id_denominacion = @Original_id_denomina" & _
        "cion) AND (Cantidad = @Original_Cantidad) AND (Denominacion = @Original_Denomina" & _
        "cion) AND (Monto_Total = @Original_Monto_Total) AND (Valor = @Original_Valor) AN" & _
        "D (id_total_tope = @Original_id_total_tope)"
        Me.SqlDeleteCommand10.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id_denominacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_denominacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Valor", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Valor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id_total_tope", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_total_tope", System.Data.DataRowVersion.Original, Nothing))
        '
        'daDenominacionApertura
        '
        Me.daDenominacionApertura.DeleteCommand = Me.SqlDeleteCommand10
        Me.daDenominacionApertura.InsertCommand = Me.SqlInsertCommand5
        Me.daDenominacionApertura.SelectCommand = Me.SqlSelectCommand5
        Me.daDenominacionApertura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "denominacionesapertura", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Denominacion", "Denominacion"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Valor", "Valor"), New System.Data.Common.DataColumnMapping("id_denominacion", "id_denominacion"), New System.Data.Common.DataColumnMapping("id_total_tope", "id_total_tope"), New System.Data.Common.DataColumnMapping("Monto_Total", "Monto_Total")})})
        Me.daDenominacionApertura.UpdateCommand = Me.SqlUpdateCommand10
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.dgOperaciones)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox5.Location = New System.Drawing.Point(272, 220)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(424, 128)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Resumen Operaciones"
        '
        'dgOperaciones
        '
        Me.dgOperaciones.DataMember = ""
        Me.dgOperaciones.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgOperaciones.Location = New System.Drawing.Point(8, 16)
        Me.dgOperaciones.Name = "dgOperaciones"
        Me.dgOperaciones.ReadOnly = True
        Me.dgOperaciones.Size = New System.Drawing.Size(408, 104)
        Me.dgOperaciones.TabIndex = 0
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO Moneda(CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo) VAL" & _
        "UES (@CodMoneda, @MonedaNombre, @ValorCompra, @ValorVenta, @Simbolo); SELECT Cod" & _
        "Moneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda WHERE (CodMon" & _
        "eda = @CodMoneda)"
        Me.SqlInsertCommand7.Connection = Me.SqlConnection1
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"))
        '
        'SqlUpdateCommand12
        '
        Me.SqlUpdateCommand12.CommandText = "UPDATE Moneda SET CodMoneda = @CodMoneda, MonedaNombre = @MonedaNombre, ValorComp" & _
        "ra = @ValorCompra, ValorVenta = @ValorVenta, Simbolo = @Simbolo WHERE (CodMoneda" & _
        " = @Original_CodMoneda) AND (MonedaNombre = @Original_MonedaNombre) AND (Simbolo" & _
        " = @Original_Simbolo) AND (ValorCompra = @Original_ValorCompra) AND (ValorVenta " & _
        "= @Original_ValorVenta); SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta" & _
        ", Simbolo FROM Moneda WHERE (CodMoneda = @CodMoneda)"
        Me.SqlUpdateCommand12.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand12
        '
        Me.SqlDeleteCommand12.CommandText = "DELETE FROM Moneda WHERE (CodMoneda = @Original_CodMoneda) AND (MonedaNombre = @O" & _
        "riginal_MonedaNombre) AND (Simbolo = @Original_Simbolo) AND (ValorCompra = @Orig" & _
        "inal_ValorCompra) AND (ValorVenta = @Original_ValorVenta)"
        Me.SqlDeleteCommand12.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'daMoneda
        '
        Me.daMoneda.DeleteCommand = Me.SqlDeleteCommand12
        Me.daMoneda.InsertCommand = Me.SqlInsertCommand7
        Me.daMoneda.SelectCommand = Me.SqlSelectCommand7
        Me.daMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        Me.daMoneda.UpdateCommand = Me.SqlUpdateCommand12
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.dgCierre)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox6.Location = New System.Drawing.Point(8, 328)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(256, 136)
        Me.GroupBox6.TabIndex = 8
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Cierre"
        '
        'dgCierre
        '
        Me.dgCierre.BackgroundColor = System.Drawing.Color.White
        Me.dgCierre.DataMember = ""
        Me.dgCierre.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgCierre.Location = New System.Drawing.Point(8, 16)
        Me.dgCierre.Name = "dgCierre"
        Me.dgCierre.Size = New System.Drawing.Size(240, 112)
        Me.dgCierre.TabIndex = 0
        '
        'daCierrecaja
        '
        Me.daCierrecaja.DeleteCommand = Me.SqlDeleteCommand13
        Me.daCierrecaja.InsertCommand = Me.SqlInsertCommand8
        Me.daCierrecaja.SelectCommand = Me.SqlSelectCommand8
        Me.daCierrecaja.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "cierrecaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumeroCierre", "NumeroCierre"), New System.Data.Common.DataColumnMapping("Cajera", "Cajera"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Apertura", "Apertura"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Devoluciones", "Devoluciones"), New System.Data.Common.DataColumnMapping("Subtotal", "Subtotal"), New System.Data.Common.DataColumnMapping("TotalSistema", "TotalSistema"), New System.Data.Common.DataColumnMapping("Equivalencia", "Equivalencia")})})
        Me.daCierrecaja.UpdateCommand = Me.SqlUpdateCommand13
        '
        'SqlDeleteCommand13
        '
        Me.SqlDeleteCommand13.CommandText = "DELETE FROM cierrecaja WHERE (NumeroCierre = @Original_NumeroCierre) AND (Anulado" & _
        " = @Original_Anulado) AND (Apertura = @Original_Apertura) AND (Cajera = @Origina" & _
        "l_Cajera) AND (Devoluciones = @Original_Devoluciones) AND (Equivalencia = @Origi" & _
        "nal_Equivalencia) AND (Fecha = @Original_Fecha) AND (Nombre = @Original_Nombre) " & _
        "AND (NombreUsuario = @Original_NombreUsuario) AND (Subtotal = @Original_Subtotal" & _
        ") AND (TotalSistema = @Original_TotalSistema) AND (Usuario = @Original_Usuario)"
        Me.SqlDeleteCommand13.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroCierre", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroCierre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Apertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Apertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cajera", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajera", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Equivalencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Subtotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Subtotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSistema", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = "INSERT INTO cierrecaja(Cajera, Nombre, Apertura, Usuario, Fecha, NombreUsuario, A" & _
        "nulado, Devoluciones, Subtotal, TotalSistema, Equivalencia) VALUES (@Cajera, @No" & _
        "mbre, @Apertura, @Usuario, @Fecha, @NombreUsuario, @Anulado, @Devoluciones, @Sub" & _
        "total, @TotalSistema, @Equivalencia); SELECT NumeroCierre, Cajera, Nombre, Apert" & _
        "ura, Usuario, Fecha, NombreUsuario, Anulado, Devoluciones, Subtotal, TotalSistem" & _
        "a, Equivalencia FROM cierrecaja WHERE (NumeroCierre = @@IDENTITY)"
        Me.SqlInsertCommand8.Connection = Me.SqlConnection1
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cajera", System.Data.SqlDbType.VarChar, 255, "Cajera"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Apertura", System.Data.SqlDbType.Int, 4, "Apertura"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 8, "Devoluciones"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Subtotal", System.Data.SqlDbType.Float, 8, "Subtotal"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSistema", System.Data.SqlDbType.Float, 8, "TotalSistema"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.Float, 8, "Equivalencia"))
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT NumeroCierre, Cajera, Nombre, Apertura, Usuario, Fecha, NombreUsuario, Anu" & _
        "lado, Devoluciones, Subtotal, TotalSistema, Equivalencia FROM cierrecaja"
        Me.SqlSelectCommand8.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand13
        '
        Me.SqlUpdateCommand13.CommandText = "UPDATE cierrecaja SET Cajera = @Cajera, Nombre = @Nombre, Apertura = @Apertura, U" & _
        "suario = @Usuario, Fecha = @Fecha, NombreUsuario = @NombreUsuario, Anulado = @An" & _
        "ulado, Devoluciones = @Devoluciones, Subtotal = @Subtotal, TotalSistema = @Total" & _
        "Sistema, Equivalencia = @Equivalencia WHERE (NumeroCierre = @Original_NumeroCier" & _
        "re) AND (Anulado = @Original_Anulado) AND (Apertura = @Original_Apertura) AND (C" & _
        "ajera = @Original_Cajera) AND (Devoluciones = @Original_Devoluciones) AND (Equiv" & _
        "alencia = @Original_Equivalencia) AND (Fecha = @Original_Fecha) AND (Nombre = @O" & _
        "riginal_Nombre) AND (NombreUsuario = @Original_NombreUsuario) AND (Subtotal = @O" & _
        "riginal_Subtotal) AND (TotalSistema = @Original_TotalSistema) AND (Usuario = @Or" & _
        "iginal_Usuario); SELECT NumeroCierre, Cajera, Nombre, Apertura, Usuario, Fecha, " & _
        "NombreUsuario, Anulado, Devoluciones, Subtotal, TotalSistema, Equivalencia FROM " & _
        "cierrecaja WHERE (NumeroCierre = @NumeroCierre)"
        Me.SqlUpdateCommand13.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cajera", System.Data.SqlDbType.VarChar, 255, "Cajera"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Apertura", System.Data.SqlDbType.Int, 4, "Apertura"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 8, "Devoluciones"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Subtotal", System.Data.SqlDbType.Float, 8, "Subtotal"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSistema", System.Data.SqlDbType.Float, 8, "TotalSistema"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.Float, 8, "Equivalencia"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroCierre", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroCierre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Apertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Apertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cajera", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajera", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Equivalencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Subtotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Subtotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSistema", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroCierre", System.Data.SqlDbType.Int, 4, "NumeroCierre"))
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(320, 576)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 16)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "N�mero"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.Location = New System.Drawing.Point(16, 536)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(128, 16)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Monto Colones"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.NumeroCierre"))
        Me.TextBox1.ForeColor = System.Drawing.Color.Blue
        Me.TextBox1.Location = New System.Drawing.Point(16, 552)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(128, 13)
        Me.TextBox1.TabIndex = 23
        Me.TextBox1.Text = ""
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.Location = New System.Drawing.Point(16, 576)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(128, 16)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "Monto Dolares"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.NumeroCierre"))
        Me.TextBox2.ForeColor = System.Drawing.Color.Blue
        Me.TextBox2.Location = New System.Drawing.Point(16, 592)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(128, 13)
        Me.TextBox2.TabIndex = 29
        Me.TextBox2.Text = ""
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label12.Location = New System.Drawing.Point(16, 616)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(128, 16)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "Efectivo Colones"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.NumeroCierre"))
        Me.TextBox3.ForeColor = System.Drawing.Color.Blue
        Me.TextBox3.Location = New System.Drawing.Point(16, 632)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(128, 13)
        Me.TextBox3.TabIndex = 33
        Me.TextBox3.Text = ""
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label16.Location = New System.Drawing.Point(160, 616)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(128, 16)
        Me.Label16.TabIndex = 32
        Me.Label16.Text = "Anulado"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox7
        '
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.NumeroCierre"))
        Me.TextBox7.ForeColor = System.Drawing.Color.Blue
        Me.TextBox7.Location = New System.Drawing.Point(160, 632)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(128, 13)
        Me.TextBox7.TabIndex = 34
        Me.TextBox7.Text = ""
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label17.Location = New System.Drawing.Point(320, 536)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(128, 16)
        Me.Label17.TabIndex = 21
        Me.Label17.Text = "Devoluciones"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox8
        '
        Me.TextBox8.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox8.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.NumeroCierre"))
        Me.TextBox8.ForeColor = System.Drawing.Color.Blue
        Me.TextBox8.Location = New System.Drawing.Point(320, 552)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(128, 13)
        Me.TextBox8.TabIndex = 25
        Me.TextBox8.Text = ""
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.Location = New System.Drawing.Point(160, 536)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(128, 16)
        Me.Label13.TabIndex = 20
        Me.Label13.Text = "Cheques"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.NumeroCierre"))
        Me.TextBox4.ForeColor = System.Drawing.Color.Blue
        Me.TextBox4.Location = New System.Drawing.Point(160, 552)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(128, 13)
        Me.TextBox4.TabIndex = 24
        Me.TextBox4.Text = ""
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label14.Location = New System.Drawing.Point(160, 576)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(128, 16)
        Me.Label14.TabIndex = 27
        Me.Label14.Text = "Tarjeta"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox5
        '
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.NumeroCierre"))
        Me.TextBox5.ForeColor = System.Drawing.Color.Blue
        Me.TextBox5.Location = New System.Drawing.Point(160, 592)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(128, 13)
        Me.TextBox5.TabIndex = 30
        Me.TextBox5.Text = ""
        '
        'chkAnulado
        '
        Me.chkAnulado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.Dscierrecaja1, "cierrecaja.Anulado"))
        Me.chkAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAnulado.Location = New System.Drawing.Point(320, 632)
        Me.chkAnulado.Name = "chkAnulado"
        Me.chkAnulado.Size = New System.Drawing.Size(88, 16)
        Me.chkAnulado.TabIndex = 35
        Me.chkAnulado.Text = "Anulado"
        '
        'lblnumerocierre
        '
        Me.lblnumerocierre.BackColor = System.Drawing.Color.White
        Me.lblnumerocierre.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dscierrecaja1, "cierrecaja.NumeroCierre"))
        Me.lblnumerocierre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnumerocierre.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblnumerocierre.Location = New System.Drawing.Point(22, 16)
        Me.lblnumerocierre.Name = "lblnumerocierre"
        Me.lblnumerocierre.Size = New System.Drawing.Size(136, 13)
        Me.lblnumerocierre.TabIndex = 1
        Me.lblnumerocierre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT Id, Num_Factura, Tipo, Cod_Cliente, Nombre_Cliente, Orden, Cedula_Usuario," & _
        " Pago_Comision, SubTotal, Descuento, Imp_Venta, Total, Fecha, Vence, Cod_Encarga" & _
        "do_Compra, Contabilizado, AsientoVenta, ContabilizadoCVenta, AsientoCosto, Anula" & _
        "do, PagoImpuesto, FacturaCancelado, Num_Apertura, Entregado, Cod_Moneda, Moneda_" & _
        "Nombre, Direccion, Telefono, SubTotalGravada, SubTotalExento FROM Ventas"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO Ventas(Num_Factura, Tipo, Cod_Cliente, Nombre_Cliente, Orden, Cedula_" & _
        "Usuario, Pago_Comision, SubTotal, Descuento, Imp_Venta, Total, Fecha, Vence, Cod" & _
        "_Encargado_Compra, Contabilizado, AsientoVenta, ContabilizadoCVenta, AsientoCost" & _
        "o, Anulado, PagoImpuesto, FacturaCancelado, Num_Apertura, Entregado, Cod_Moneda," & _
        " Moneda_Nombre, Direccion, Telefono, SubTotalGravada, SubTotalExento) VALUES (@N" & _
        "um_Factura, @Tipo, @Cod_Cliente, @Nombre_Cliente, @Orden, @Cedula_Usuario, @Pago" & _
        "_Comision, @SubTotal, @Descuento, @Imp_Venta, @Total, @Fecha, @Vence, @Cod_Encar" & _
        "gado_Compra, @Contabilizado, @AsientoVenta, @ContabilizadoCVenta, @AsientoCosto," & _
        " @Anulado, @PagoImpuesto, @FacturaCancelado, @Num_Apertura, @Entregado, @Cod_Mon" & _
        "eda, @Moneda_Nombre, @Direccion, @Telefono, @SubTotalGravada, @SubTotalExento); " & _
        "SELECT Id, Num_Factura, Tipo, Cod_Cliente, Nombre_Cliente, Orden, Cedula_Usuario" & _
        ", Pago_Comision, SubTotal, Descuento, Imp_Venta, Total, Fecha, Vence, Cod_Encarg" & _
        "ado_Compra, Contabilizado, AsientoVenta, ContabilizadoCVenta, AsientoCosto, Anul" & _
        "ado, PagoImpuesto, FacturaCancelado, Num_Apertura, Entregado, Cod_Moneda, Moneda" & _
        "_Nombre, Direccion, Telefono, SubTotalGravada, SubTotalExento FROM Ventas WHERE " & _
        "(Id = @@IDENTITY)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection1
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.BigInt, 8, "Num_Factura"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 3, "Tipo"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 4, "Cod_Cliente"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, "Nombre_Cliente"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Orden", System.Data.SqlDbType.BigInt, 8, "Orden"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula_Usuario", System.Data.SqlDbType.VarChar, 75, "Cedula_Usuario"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Pago_Comision", System.Data.SqlDbType.Bit, 1, "Pago_Comision"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 8, "SubTotal"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 8, "Descuento"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 8, "Imp_Venta"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Vence", System.Data.SqlDbType.DateTime, 8, "Vence"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Encargado_Compra", System.Data.SqlDbType.VarChar, 30, "Cod_Encargado_Compra"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 1, "Contabilizado"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AsientoVenta", System.Data.SqlDbType.BigInt, 8, "AsientoVenta"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContabilizadoCVenta", System.Data.SqlDbType.Bit, 1, "ContabilizadoCVenta"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AsientoCosto", System.Data.SqlDbType.BigInt, 8, "AsientoCosto"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PagoImpuesto", System.Data.SqlDbType.Int, 4, "PagoImpuesto"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FacturaCancelado", System.Data.SqlDbType.Bit, 1, "FacturaCancelado"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Apertura", System.Data.SqlDbType.BigInt, 8, "Num_Apertura"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Entregado", System.Data.SqlDbType.Bit, 1, "Entregado"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Moneda_Nombre", System.Data.SqlDbType.VarChar, 50, "Moneda_Nombre"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 250, "Direccion"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono", System.Data.SqlDbType.VarChar, 15, "Telefono"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubTotalGravada", System.Data.SqlDbType.Float, 8, "SubTotalGravada"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubTotalExento", System.Data.SqlDbType.Float, 8, "SubTotalExento"))
        '
        'SqlUpdateCommand11
        '
        Me.SqlUpdateCommand11.CommandText = "UPDATE Ventas SET Num_Factura = @Num_Factura, Tipo = @Tipo, Cod_Cliente = @Cod_Cl" & _
        "iente, Nombre_Cliente = @Nombre_Cliente, Orden = @Orden, Cedula_Usuario = @Cedul" & _
        "a_Usuario, Pago_Comision = @Pago_Comision, SubTotal = @SubTotal, Descuento = @De" & _
        "scuento, Imp_Venta = @Imp_Venta, Total = @Total, Fecha = @Fecha, Vence = @Vence," & _
        " Cod_Encargado_Compra = @Cod_Encargado_Compra, Contabilizado = @Contabilizado, A" & _
        "sientoVenta = @AsientoVenta, ContabilizadoCVenta = @ContabilizadoCVenta, Asiento" & _
        "Costo = @AsientoCosto, Anulado = @Anulado, PagoImpuesto = @PagoImpuesto, Factura" & _
        "Cancelado = @FacturaCancelado, Num_Apertura = @Num_Apertura, Entregado = @Entreg" & _
        "ado, Cod_Moneda = @Cod_Moneda, Moneda_Nombre = @Moneda_Nombre, Direccion = @Dire" & _
        "ccion, Telefono = @Telefono, SubTotalGravada = @SubTotalGravada, SubTotalExento " & _
        "= @SubTotalExento WHERE (Id = @Original_Id) AND (Anulado = @Original_Anulado) AN" & _
        "D (AsientoCosto = @Original_AsientoCosto) AND (AsientoVenta = @Original_AsientoV" & _
        "enta) AND (Cedula_Usuario = @Original_Cedula_Usuario) AND (Cod_Cliente = @Origin" & _
        "al_Cod_Cliente) AND (Cod_Encargado_Compra = @Original_Cod_Encargado_Compra) AND " & _
        "(Cod_Moneda = @Original_Cod_Moneda) AND (Contabilizado = @Original_Contabilizado" & _
        ") AND (ContabilizadoCVenta = @Original_ContabilizadoCVenta) AND (Descuento = @Or" & _
        "iginal_Descuento) AND (Direccion = @Original_Direccion) AND (Entregado = @Origin" & _
        "al_Entregado) AND (FacturaCancelado = @Original_FacturaCancelado) AND (Fecha = @" & _
        "Original_Fecha) AND (Imp_Venta = @Original_Imp_Venta) AND (Moneda_Nombre = @Orig" & _
        "inal_Moneda_Nombre) AND (Nombre_Cliente = @Original_Nombre_Cliente) AND (Num_Ape" & _
        "rtura = @Original_Num_Apertura) AND (Num_Factura = @Original_Num_Factura) AND (O" & _
        "rden = @Original_Orden) AND (PagoImpuesto = @Original_PagoImpuesto) AND (Pago_Co" & _
        "mision = @Original_Pago_Comision) AND (SubTotal = @Original_SubTotal) AND (SubTo" & _
        "talExento = @Original_SubTotalExento) AND (SubTotalGravada = @Original_SubTotalG" & _
        "ravada) AND (Telefono = @Original_Telefono) AND (Tipo = @Original_Tipo) AND (Tot" & _
        "al = @Original_Total) AND (Vence = @Original_Vence); SELECT Id, Num_Factura, Tip" & _
        "o, Cod_Cliente, Nombre_Cliente, Orden, Cedula_Usuario, Pago_Comision, SubTotal, " & _
        "Descuento, Imp_Venta, Total, Fecha, Vence, Cod_Encargado_Compra, Contabilizado, " & _
        "AsientoVenta, ContabilizadoCVenta, AsientoCosto, Anulado, PagoImpuesto, FacturaC" & _
        "ancelado, Num_Apertura, Entregado, Cod_Moneda, Moneda_Nombre, Direccion, Telefon" & _
        "o, SubTotalGravada, SubTotalExento FROM Ventas WHERE (Id = @Id)"
        Me.SqlUpdateCommand11.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.BigInt, 8, "Num_Factura"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 3, "Tipo"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 4, "Cod_Cliente"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, "Nombre_Cliente"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Orden", System.Data.SqlDbType.BigInt, 8, "Orden"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula_Usuario", System.Data.SqlDbType.VarChar, 75, "Cedula_Usuario"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Pago_Comision", System.Data.SqlDbType.Bit, 1, "Pago_Comision"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 8, "SubTotal"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 8, "Descuento"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 8, "Imp_Venta"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Vence", System.Data.SqlDbType.DateTime, 8, "Vence"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Encargado_Compra", System.Data.SqlDbType.VarChar, 30, "Cod_Encargado_Compra"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 1, "Contabilizado"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AsientoVenta", System.Data.SqlDbType.BigInt, 8, "AsientoVenta"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContabilizadoCVenta", System.Data.SqlDbType.Bit, 1, "ContabilizadoCVenta"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AsientoCosto", System.Data.SqlDbType.BigInt, 8, "AsientoCosto"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PagoImpuesto", System.Data.SqlDbType.Int, 4, "PagoImpuesto"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FacturaCancelado", System.Data.SqlDbType.Bit, 1, "FacturaCancelado"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Apertura", System.Data.SqlDbType.BigInt, 8, "Num_Apertura"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Entregado", System.Data.SqlDbType.Bit, 1, "Entregado"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Moneda_Nombre", System.Data.SqlDbType.VarChar, 50, "Moneda_Nombre"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 250, "Direccion"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono", System.Data.SqlDbType.VarChar, 15, "Telefono"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubTotalGravada", System.Data.SqlDbType.Float, 8, "SubTotalGravada"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubTotalExento", System.Data.SqlDbType.Float, 8, "SubTotalExento"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AsientoCosto", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AsientoCosto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AsientoVenta", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AsientoVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula_Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Cliente", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Cliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Encargado_Compra", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Encargado_Compra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContabilizadoCVenta", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContabilizadoCVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Entregado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Entregado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FacturaCancelado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FacturaCancelado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Imp_Venta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Venta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Moneda_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda_Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Apertura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Apertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Factura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Orden", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Orden", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PagoImpuesto", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PagoImpuesto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Pago_Comision", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Pago_Comision", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubTotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubTotalExento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalExento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubTotalGravada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalGravada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Vence", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Vence", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id"))
        '
        'SqlDeleteCommand11
        '
        Me.SqlDeleteCommand11.CommandText = "DELETE FROM Ventas WHERE (Id = @Original_Id) AND (Anulado = @Original_Anulado) AN" & _
        "D (AsientoCosto = @Original_AsientoCosto) AND (AsientoVenta = @Original_AsientoV" & _
        "enta) AND (Cedula_Usuario = @Original_Cedula_Usuario) AND (Cod_Cliente = @Origin" & _
        "al_Cod_Cliente) AND (Cod_Encargado_Compra = @Original_Cod_Encargado_Compra) AND " & _
        "(Cod_Moneda = @Original_Cod_Moneda) AND (Contabilizado = @Original_Contabilizado" & _
        ") AND (ContabilizadoCVenta = @Original_ContabilizadoCVenta) AND (Descuento = @Or" & _
        "iginal_Descuento) AND (Direccion = @Original_Direccion) AND (Entregado = @Origin" & _
        "al_Entregado) AND (FacturaCancelado = @Original_FacturaCancelado) AND (Fecha = @" & _
        "Original_Fecha) AND (Imp_Venta = @Original_Imp_Venta) AND (Moneda_Nombre = @Orig" & _
        "inal_Moneda_Nombre) AND (Nombre_Cliente = @Original_Nombre_Cliente) AND (Num_Ape" & _
        "rtura = @Original_Num_Apertura) AND (Num_Factura = @Original_Num_Factura) AND (O" & _
        "rden = @Original_Orden) AND (PagoImpuesto = @Original_PagoImpuesto) AND (Pago_Co" & _
        "mision = @Original_Pago_Comision) AND (SubTotal = @Original_SubTotal) AND (SubTo" & _
        "talExento = @Original_SubTotalExento) AND (SubTotalGravada = @Original_SubTotalG" & _
        "ravada) AND (Telefono = @Original_Telefono) AND (Tipo = @Original_Tipo) AND (Tot" & _
        "al = @Original_Total) AND (Vence = @Original_Vence)"
        Me.SqlDeleteCommand11.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AsientoCosto", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AsientoCosto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AsientoVenta", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AsientoVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula_Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Cliente", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Cliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Encargado_Compra", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Encargado_Compra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContabilizadoCVenta", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContabilizadoCVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Entregado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Entregado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FacturaCancelado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FacturaCancelado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Imp_Venta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Venta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Moneda_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda_Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cliente", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Apertura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Apertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Factura", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Orden", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Orden", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PagoImpuesto", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PagoImpuesto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Pago_Comision", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Pago_Comision", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubTotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubTotalExento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalExento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubTotalGravada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalGravada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Vence", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Vence", System.Data.DataRowVersion.Original, Nothing))
        '
        'daVenta
        '
        Me.daVenta.DeleteCommand = Me.SqlDeleteCommand11
        Me.daVenta.InsertCommand = Me.SqlInsertCommand6
        Me.daVenta.SelectCommand = Me.SqlSelectCommand6
        Me.daVenta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Ventas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Cod_Cliente", "Cod_Cliente"), New System.Data.Common.DataColumnMapping("Nombre_Cliente", "Nombre_Cliente"), New System.Data.Common.DataColumnMapping("Orden", "Orden"), New System.Data.Common.DataColumnMapping("Cedula_Usuario", "Cedula_Usuario"), New System.Data.Common.DataColumnMapping("Pago_Comision", "Pago_Comision"), New System.Data.Common.DataColumnMapping("SubTotal", "SubTotal"), New System.Data.Common.DataColumnMapping("Descuento", "Descuento"), New System.Data.Common.DataColumnMapping("Imp_Venta", "Imp_Venta"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Vence", "Vence"), New System.Data.Common.DataColumnMapping("Cod_Encargado_Compra", "Cod_Encargado_Compra"), New System.Data.Common.DataColumnMapping("Contabilizado", "Contabilizado"), New System.Data.Common.DataColumnMapping("AsientoVenta", "AsientoVenta"), New System.Data.Common.DataColumnMapping("ContabilizadoCVenta", "ContabilizadoCVenta"), New System.Data.Common.DataColumnMapping("AsientoCosto", "AsientoCosto"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("PagoImpuesto", "PagoImpuesto"), New System.Data.Common.DataColumnMapping("FacturaCancelado", "FacturaCancelado"), New System.Data.Common.DataColumnMapping("Num_Apertura", "Num_Apertura"), New System.Data.Common.DataColumnMapping("Entregado", "Entregado"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Moneda_Nombre", "Moneda_Nombre"), New System.Data.Common.DataColumnMapping("Direccion", "Direccion"), New System.Data.Common.DataColumnMapping("Telefono", "Telefono"), New System.Data.Common.DataColumnMapping("SubTotalGravada", "SubTotalGravada"), New System.Data.Common.DataColumnMapping("SubTotalExento", "SubTotalExento")})})
        Me.daVenta.UpdateCommand = Me.SqlUpdateCommand11
        '
        'daDetalle
        '
        Me.daDetalle.DeleteCommand = Me.SqlDeleteCommand14
        Me.daDetalle.InsertCommand = Me.SqlInsertCommand9
        Me.daDetalle.SelectCommand = Me.SqlSelectCommand9
        Me.daDetalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Detalle_Cierrecaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id", "id"), New System.Data.Common.DataColumnMapping("Num_cierre", "Num_cierre"), New System.Data.Common.DataColumnMapping("Moneda", "Moneda"), New System.Data.Common.DataColumnMapping("Efectivo", "Efectivo"), New System.Data.Common.DataColumnMapping("Tarjeta", "Tarjeta"), New System.Data.Common.DataColumnMapping("Cheque", "Cheque"), New System.Data.Common.DataColumnMapping("Transferencia", "Transferencia"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Devoluciones", "Devoluciones")})})
        Me.daDetalle.UpdateCommand = Me.SqlUpdateCommand14
        '
        'SqlDeleteCommand14
        '
        Me.SqlDeleteCommand14.CommandText = "DELETE FROM Detalle_Cierrecaja WHERE (id = @Original_id) AND (Cheque = @Original_" & _
        "Cheque) AND (Devoluciones = @Original_Devoluciones) AND (Efectivo = @Original_Ef" & _
        "ectivo) AND (Moneda = @Original_Moneda) AND (Num_cierre = @Original_Num_cierre) " & _
        "AND (Tarjeta = @Original_Tarjeta) AND (Total = @Original_Total) AND (Transferenc" & _
        "ia = @Original_Transferencia)"
        Me.SqlDeleteCommand14.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cheque", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cheque", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Efectivo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Efectivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_cierre", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_cierre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tarjeta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tarjeta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Transferencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Transferencia", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand9
        '
        Me.SqlInsertCommand9.CommandText = "INSERT INTO Detalle_Cierrecaja(Num_cierre, Moneda, Efectivo, Tarjeta, Cheque, Tra" & _
        "nsferencia, Total, Devoluciones) VALUES (@Num_cierre, @Moneda, @Efectivo, @Tarje" & _
        "ta, @Cheque, @Transferencia, @Total, @Devoluciones); SELECT id, Num_cierre, Mone" & _
        "da, Efectivo, Tarjeta, Cheque, Transferencia, Total, Devoluciones FROM Detalle_C" & _
        "ierrecaja WHERE (id = @@IDENTITY)"
        Me.SqlInsertCommand9.Connection = Me.SqlConnection1
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_cierre", System.Data.SqlDbType.Int, 4, "Num_cierre"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.VarChar, 50, "Moneda"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Efectivo", System.Data.SqlDbType.Float, 8, "Efectivo"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tarjeta", System.Data.SqlDbType.Float, 8, "Tarjeta"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cheque", System.Data.SqlDbType.Float, 8, "Cheque"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Transferencia", System.Data.SqlDbType.Float, 8, "Transferencia"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 8, "Devoluciones"))
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT id, Num_cierre, Moneda, Efectivo, Tarjeta, Cheque, Transferencia, Total, D" & _
        "evoluciones FROM Detalle_Cierrecaja"
        Me.SqlSelectCommand9.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand14
        '
        Me.SqlUpdateCommand14.CommandText = "UPDATE Detalle_Cierrecaja SET Num_cierre = @Num_cierre, Moneda = @Moneda, Efectiv" & _
        "o = @Efectivo, Tarjeta = @Tarjeta, Cheque = @Cheque, Transferencia = @Transferen" & _
        "cia, Total = @Total, Devoluciones = @Devoluciones WHERE (id = @Original_id) AND " & _
        "(Cheque = @Original_Cheque) AND (Devoluciones = @Original_Devoluciones) AND (Efe" & _
        "ctivo = @Original_Efectivo) AND (Moneda = @Original_Moneda) AND (Num_cierre = @O" & _
        "riginal_Num_cierre) AND (Tarjeta = @Original_Tarjeta) AND (Total = @Original_Tot" & _
        "al) AND (Transferencia = @Original_Transferencia); SELECT id, Num_cierre, Moneda" & _
        ", Efectivo, Tarjeta, Cheque, Transferencia, Total, Devoluciones FROM Detalle_Cie" & _
        "rrecaja WHERE (id = @id)"
        Me.SqlUpdateCommand14.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_cierre", System.Data.SqlDbType.Int, 4, "Num_cierre"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Moneda", System.Data.SqlDbType.VarChar, 50, "Moneda"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Efectivo", System.Data.SqlDbType.Float, 8, "Efectivo"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tarjeta", System.Data.SqlDbType.Float, 8, "Tarjeta"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cheque", System.Data.SqlDbType.Float, 8, "Cheque"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Transferencia", System.Data.SqlDbType.Float, 8, "Transferencia"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 8, "Devoluciones"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cheque", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cheque", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Efectivo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Efectivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Moneda", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_cierre", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_cierre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tarjeta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tarjeta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Transferencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Transferencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int, 4, "id"))
        '
        'lblnumcierre
        '
        Me.lblnumcierre.Location = New System.Drawing.Point(480, 536)
        Me.lblnumcierre.Name = "lblnumcierre"
        Me.lblnumcierre.TabIndex = 22
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label15.Location = New System.Drawing.Point(368, 352)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(136, 36)
        Me.Label15.TabIndex = 9
        Me.Label15.Text = "SubTotal"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label18.Location = New System.Drawing.Point(368, 392)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(136, 36)
        Me.Label18.TabIndex = 10
        Me.Label18.Text = "Devoluciones"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label19.Location = New System.Drawing.Point(368, 432)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(136, 36)
        Me.Label19.TabIndex = 13
        Me.Label19.Text = "Total Sistema"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblanulado
        '
        Me.lblanulado.AutoSize = True
        Me.lblanulado.BackColor = System.Drawing.Color.Transparent
        Me.lblanulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblanulado.ForeColor = System.Drawing.Color.Red
        Me.lblanulado.Location = New System.Drawing.Point(312, 472)
        Me.lblanulado.Name = "lblanulado"
        Me.lblanulado.Size = New System.Drawing.Size(160, 46)
        Me.lblanulado.TabIndex = 7
        Me.lblanulado.Text = "Anulado"
        Me.lblanulado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblanulado.Visible = False
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(274, 522)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 16
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.Color.Transparent
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(402, 522)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.Size = New System.Drawing.Size(294, 13)
        Me.txtNombreUsuario.TabIndex = 18
        '
        'TextBox6
        '
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox6.ForeColor = System.Drawing.Color.Blue
        Me.TextBox6.Location = New System.Drawing.Point(346, 522)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.TextBox6.Size = New System.Drawing.Size(56, 13)
        Me.TextBox6.TabIndex = 17
        Me.TextBox6.Text = ""
        '
        'SqlSelectCommand10
        '
        Me.SqlSelectCommand10.CommandText = "SELECT Denominacion, Cantidad, Valor, Monto_Total, CodMoneda, MonedaNombre, Id, N" & _
        "Apertura FROM DenominacionesAperturas"
        Me.SqlSelectCommand10.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand10
        '
        Me.SqlInsertCommand10.CommandText = "INSERT INTO DenominacionesAperturas(Denominacion, Cantidad, Valor, Monto_Total, C" & _
        "odMoneda, MonedaNombre, NApertura) VALUES (@Denominacion, @Cantidad, @Valor, @Mo" & _
        "nto_Total, @CodMoneda, @MonedaNombre, @NApertura); SELECT Denominacion, Cantidad" & _
        ", Valor, Monto_Total, CodMoneda, MonedaNombre, Id, NApertura FROM Denominaciones" & _
        "Aperturas WHERE (Id = @@IDENTITY)"
        Me.SqlInsertCommand10.Connection = Me.SqlConnection1
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.VarChar, 50, "Denominacion"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Int, 4, "Cantidad"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Valor", System.Data.SqlDbType.Float, 8, "Valor"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto_Total", System.Data.SqlDbType.Float, 8, "Monto_Total"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"))
        '
        'SqlUpdateCommand15
        '
        Me.SqlUpdateCommand15.CommandText = "UPDATE DenominacionesAperturas SET Denominacion = @Denominacion, Cantidad = @Cant" & _
        "idad, Valor = @Valor, Monto_Total = @Monto_Total, CodMoneda = @CodMoneda, Moneda" & _
        "Nombre = @MonedaNombre, NApertura = @NApertura WHERE (Id = @Original_Id) AND (Ca" & _
        "ntidad = @Original_Cantidad) AND (CodMoneda = @Original_CodMoneda) AND (Denomina" & _
        "cion = @Original_Denominacion) AND (MonedaNombre = @Original_MonedaNombre) AND (" & _
        "Monto_Total = @Original_Monto_Total) AND (NApertura = @Original_NApertura) AND (" & _
        "Valor = @Original_Valor); SELECT Denominacion, Cantidad, Valor, Monto_Total, Cod" & _
        "Moneda, MonedaNombre, Id, NApertura FROM DenominacionesAperturas WHERE (Id = @Id" & _
        ")"
        Me.SqlUpdateCommand15.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Denominacion", System.Data.SqlDbType.VarChar, 50, "Denominacion"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Int, 4, "Cantidad"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Valor", System.Data.SqlDbType.Float, 8, "Valor"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto_Total", System.Data.SqlDbType.Float, 8, "Monto_Total"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NApertura", System.Data.SqlDbType.Int, 4, "NApertura"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Valor", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Valor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id"))
        '
        'SqlDeleteCommand15
        '
        Me.SqlDeleteCommand15.CommandText = "DELETE FROM DenominacionesAperturas WHERE (Id = @Original_Id) AND (Cantidad = @Or" & _
        "iginal_Cantidad) AND (CodMoneda = @Original_CodMoneda) AND (Denominacion = @Orig" & _
        "inal_Denominacion) AND (MonedaNombre = @Original_MonedaNombre) AND (Monto_Total " & _
        "= @Original_Monto_Total) AND (NApertura = @Original_NApertura) AND (Valor = @Ori" & _
        "ginal_Valor)"
        Me.SqlDeleteCommand15.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Denominacion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Denominacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NApertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NApertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Valor", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Valor", System.Data.DataRowVersion.Original, Nothing))
        '
        'AdapterDenominacionesApertura
        '
        Me.AdapterDenominacionesApertura.DeleteCommand = Me.SqlDeleteCommand15
        Me.AdapterDenominacionesApertura.InsertCommand = Me.SqlInsertCommand10
        Me.AdapterDenominacionesApertura.SelectCommand = Me.SqlSelectCommand10
        Me.AdapterDenominacionesApertura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "DenominacionesAperturas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Denominacion", "Denominacion"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Valor", "Valor"), New System.Data.Common.DataColumnMapping("Monto_Total", "Monto_Total"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("NApertura", "NApertura")})})
        Me.AdapterDenominacionesApertura.UpdateCommand = Me.SqlUpdateCommand15
        '
        'TextEdit1
        '
        Me.TextEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.Dscierrecaja1, "cierrecaja.Subtotal"))
        Me.TextEdit1.EditValue = "0.00"
        Me.TextEdit1.Location = New System.Drawing.Point(504, 352)
        Me.TextEdit1.Name = "TextEdit1"
        '
        'TextEdit1.Properties
        '
        Me.TextEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TextEdit1.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit1.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.Color.RoyalBlue)
        Me.TextEdit1.Size = New System.Drawing.Size(184, 34)
        Me.TextEdit1.TabIndex = 36
        '
        'TextEdit2
        '
        Me.TextEdit2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.Dscierrecaja1, "cierrecaja.Devoluciones"))
        Me.TextEdit2.EditValue = "0.00"
        Me.TextEdit2.Location = New System.Drawing.Point(504, 392)
        Me.TextEdit2.Name = "TextEdit2"
        '
        'TextEdit2.Properties
        '
        Me.TextEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TextEdit2.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit2.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit2.Properties.ReadOnly = True
        Me.TextEdit2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.Color.RoyalBlue)
        Me.TextEdit2.Size = New System.Drawing.Size(184, 34)
        Me.TextEdit2.TabIndex = 37
        '
        'TextEdit3
        '
        Me.TextEdit3.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.Dscierrecaja1, "cierrecaja.TotalSistema"))
        Me.TextEdit3.EditValue = "0.00"
        Me.TextEdit3.Location = New System.Drawing.Point(504, 432)
        Me.TextEdit3.Name = "TextEdit3"
        '
        'TextEdit3.Properties
        '
        Me.TextEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TextEdit3.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit3.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit3.Properties.ReadOnly = True
        Me.TextEdit3.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.Color.RoyalBlue)
        Me.TextEdit3.Size = New System.Drawing.Size(184, 34)
        Me.TextEdit3.TabIndex = 38
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 520)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Size = New System.Drawing.Size(698, 16)
        Me.StatusBar1.TabIndex = 71
        '
        'frmCierreCaja
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(698, 536)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblanulado)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.TextBox8)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.TextEdit3)
        Me.Controls.Add(Me.TextEdit2)
        Me.Controls.Add(Me.TextEdit1)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.chkAnulado)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.lblnumcierre)
        Me.Controls.Add(Me.lblnumerocierre)
        Me.Controls.Add(Me.StatusBar1)
        Me.MaximumSize = New System.Drawing.Size(704, 568)
        Me.MinimumSize = New System.Drawing.Size(704, 568)
        Me.Name = "frmCierreCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cierre Caja"
        Me.Controls.SetChildIndex(Me.StatusBar1, 0)
        Me.Controls.SetChildIndex(Me.lblnumerocierre, 0)
        Me.Controls.SetChildIndex(Me.lblnumcierre, 0)
        Me.Controls.SetChildIndex(Me.GroupBox4, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.GroupBox5, 0)
        Me.Controls.SetChildIndex(Me.GroupBox6, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label10, 0)
        Me.Controls.SetChildIndex(Me.Label11, 0)
        Me.Controls.SetChildIndex(Me.Label12, 0)
        Me.Controls.SetChildIndex(Me.Label13, 0)
        Me.Controls.SetChildIndex(Me.Label14, 0)
        Me.Controls.SetChildIndex(Me.Label16, 0)
        Me.Controls.SetChildIndex(Me.Label17, 0)
        Me.Controls.SetChildIndex(Me.chkAnulado, 0)
        Me.Controls.SetChildIndex(Me.Label15, 0)
        Me.Controls.SetChildIndex(Me.Label18, 0)
        Me.Controls.SetChildIndex(Me.Label19, 0)
        Me.Controls.SetChildIndex(Me.txtNombreUsuario, 0)
        Me.Controls.SetChildIndex(Me.Label36, 0)
        Me.Controls.SetChildIndex(Me.TextEdit1, 0)
        Me.Controls.SetChildIndex(Me.TextEdit2, 0)
        Me.Controls.SetChildIndex(Me.TextEdit3, 0)
        Me.Controls.SetChildIndex(Me.TextBox1, 0)
        Me.Controls.SetChildIndex(Me.TextBox2, 0)
        Me.Controls.SetChildIndex(Me.TextBox3, 0)
        Me.Controls.SetChildIndex(Me.TextBox4, 0)
        Me.Controls.SetChildIndex(Me.TextBox5, 0)
        Me.Controls.SetChildIndex(Me.TextBox7, 0)
        Me.Controls.SetChildIndex(Me.TextBox8, 0)
        Me.Controls.SetChildIndex(Me.TextBox6, 0)
        Me.Controls.SetChildIndex(Me.lblanulado, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dscierrecaja1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.gcMontoApertura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgResumen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.dgOperaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.dgCierre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " Variable "                 'Definicion de Variable
    Private cConexion As Conexion
    Private sqlConexion As SqlConnection
    Private nuevo As Boolean = True
#End Region

#Region "Codigo General"
    Public Function FormatoTablas()
        'Declara de columnas
        Dim dtfact, dttipo, dtcoin, dtpago, dtformapago, dtmonto As System.Data.DataColumn
        Dim dtmoneda, dtefectivo, dttarjeta, dtcheque, dttransferencia, dtdevolucion, dttotal As System.Data.DataColumn
        Dim dtMonto_cierre, dtmoneda_cierre, dtequivalencia_cierre As System.Data.DataColumn
        'Creacion de las columnas  
        '*******************************************Tabla
        dtmoneda = New System.Data.DataColumn("Moneda")
        dtefectivo = New System.Data.DataColumn("Efectivo")
        dttarjeta = New System.Data.DataColumn("Tarjeta")
        dtcheque = New System.Data.DataColumn("Cheque")
        dttransferencia = New System.Data.DataColumn("Transferencia")
        dtdevolucion = New System.Data.DataColumn("Devoluciones")
        dttotal = New System.Data.DataColumn("Total")
        '********************************************************Tabla Resumen
        dtfact = New System.Data.DataColumn("Factura")
        dttipo = New System.Data.DataColumn("Tipo")
        dtcoin = New System.Data.DataColumn("Moneda")
        dtpago = New System.Data.DataColumn("Pago")
        dtformapago = New System.Data.DataColumn("Forma Pago")
        dtmonto = New System.Data.DataColumn("Equivalencia")
        '**********************************************************Tabla Cierre
        dtMonto_cierre = New System.Data.DataColumn("Monto")
        dtmoneda_cierre = New System.Data.DataColumn("Moneda")
        dtequivalencia_cierre = New System.Data.DataColumn("Equivalencia")
        Me.chkAnulado.Checked = False

        'Agregar columnas a la data tabla
        '*********************************************************Tabla
        tabla.Columns.Add(dtmoneda)
        tabla.Columns.Add(dtefectivo)
        tabla.Columns.Add(dttarjeta)
        tabla.Columns.Add(dtcheque)
        tabla.Columns.Add(dttransferencia)
        tabla.Columns.Add(dtdevolucion)
        tabla.Columns.Add(dttotal)
        '*********************************************************Tabla Resumen
        tablaresumen.Columns.Add(dtfact)
        tablaresumen.Columns.Add(dttipo)
        tablaresumen.Columns.Add(dtcoin)
        tablaresumen.Columns.Add(dtpago)
        tablaresumen.Columns.Add(dtformapago)
        tablaresumen.Columns.Add(dtmonto)
        '*********************************************************Tabla Cierre
        tablacierre.Columns.Add(dtMonto_cierre)
        tablacierre.Columns.Add(dtmoneda_cierre)
        tablacierre.Columns.Add(dtequivalencia_cierre)
    End Function
    Function IniciarEdicion()
        'Limpiar Tablas
        tabla.Clear()
        tablaresumen.Clear()
        tablacierre.Clear()
        'Habilitar Barra de Botones
        Me.ToolBar1.Buttons(0).Text = "Cancelar"
        Me.ToolBar1.Buttons(0).ImageIndex = 8
        Me.ToolBar1.Buttons(0).Enabled = True
        Me.ToolBar1.Buttons(1).Enabled = False
        Me.ToolBar1.Buttons(2).Enabled = True
        Me.ToolBar1.Buttons(3).Enabled = False
        Me.ToolBar1.Buttons(4).Enabled = False
    End Function

    Private Sub frmCierreCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Seepos", "CONEXION")

            Me.Dscierrecaja1.cierrecaja.NumeroCierreColumn.AutoIncrement = True
            Me.Dscierrecaja1.cierrecaja.NumeroCierreColumn.AutoIncrementSeed = -1
            Me.Dscierrecaja1.cierrecaja.NumeroCierreColumn.AutoIncrementStep = -1

            Me.Dscierrecaja1.Detalle_Cierrecaja.idColumn.AutoIncrement = True
            Me.Dscierrecaja1.Detalle_Cierrecaja.idColumn.AutoIncrementSeed = -1
            Me.Dscierrecaja1.Detalle_Cierrecaja.idColumn.AutoIncrementStep = -1
            'no es necesario especificar
            Carga_adaptadores()
            'Restriccion botones
            'Valores x defecto
            defaultvalue()
            FormatoTablas()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub Carga_adaptadores()
        Try
            Me.dacajeros.Fill(Me.Dscierrecaja1.Usuarios)
            Me.daMoneda.Fill(Me.Dscierrecaja1.Moneda)
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Consultar Cliente"
    Private Sub consultarcliente()
        Dim cFunciones As New cFunciones
        Dim Cedula As String = cFunciones.BuscarDatos("select cedula as Identificaci�n,nombre as Nombre from aperturacaja where estado =  '" & "A" & "'", "nombre", "CAJEROS")
        If Cedula = Nothing Then
            MsgBox("Debes selecionar un usuario para realizar el cierre de Caja", MsgBoxStyle.Exclamation)
        Else
            Dscierrecaja1.aperturacaja.Clear()
            Me.Dscierrecaja1.denominacionesapertura.Clear()
            CargarApertura(Cedula)
            If Me.Dscierrecaja1.aperturacaja.Count > 0 Then
                Me.tabla.Clear()
                Me.tablacierre.Clear()
                Me.tablaresumen.Clear()
                Me.Dscierrecaja1.DenominacionesAperturas.Clear()
                Me.Dscierrecaja1.OpcionesDePago.Clear()
                CargarOpcionesPago(Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura"))
                CargarDenominacionesApertura(Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura"))
                txtcodaperturacajero.Text = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura").ToString
                txtfechaapertura.Text = CDate(Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
                CargarCierre()
            Else
                MsgBox("Este Usuario no tiene una caja abierta")
            End If
        End If
    End Sub

#End Region
    
#Region "Cargar Apertura"
    Function CargarApertura(ByVal Id As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Seepos", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM aperturacaja where cedula = '" & Id & "' AND Estado = 'A'"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.Dscierrecaja1.aperturacaja)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Function

    Function CargarAperturaConsulta(ByVal Id As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Seepos", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM aperturacaja where  NApertura = '" & Id & "'"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.Dscierrecaja1.aperturacaja)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Function
#End Region

#Region "Cargar Opciones Pago"
    Function CargarOpcionesPago(ByVal Numapertura As String)
        Dim cnnv As SqlConnection = Nothing
        Try

            Dim Command As New SqlClient.SqlCommand("CierreDetalleFormasDePago")
            Dim con As New SqlConnection(GetSetting("SeeSOFT", "Seepos", "CONEXION"))
            Command.CommandType = CommandType.StoredProcedure
            Command.Connection = con
            Dim Apertura As New SqlClient.SqlParameter
            Apertura.ParameterName = "@Apertura"
            Apertura.SqlDbType = SqlDbType.BigInt

            Apertura.Value = Numapertura

            Command.Parameters.Add(Apertura)

            Dim daCierre As New SqlClient.SqlDataAdapter
            daCierre.SelectCommand = Command
            'Dim dsNorthwind As New DataSet
            daCierre.Fill(Me.Dscierrecaja1.OpcionesDePago)
        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
        Finally
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Function
#End Region

#Region "Cargar Denominaciones Apertura"
    Function CargarDenominacionesApertura(ByVal Numapertura As String)
        Dim cnnv As SqlConnection = Nothing
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Seepos", "CONEXION")
            cnnv = New SqlConnection(sConn)
            cnnv.Open()
            ' Creamos el comando para la consulta
            Dim cmdv As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM DenominacionesAperturas WHERE NApertura = @Numapertura "
            cmdv.CommandText = sel
            cmdv.Connection = cnnv
            cmdv.CommandType = CommandType.Text
            cmdv.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmdv.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmdv.Parameters("@Numapertura").Value = Numapertura
            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim dv As New SqlDataAdapter
            dv.SelectCommand = cmdv
            ' Llenamos la tabla
            dv.Fill(Me.Dscierrecaja1.DenominacionesAperturas)
        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
        Finally
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Function
#End Region

#Region "Cargar Cierre"
   

    Function CargarCierre()
        Try


            Dim I As Integer
            Dim Dr As DataRow
            Dim Dr1 As DataRow
            Dim Dr2 As DataRow

            'Valores Vacios a los grid
            Me.dgResumen.DataSource = Nothing
            Me.dgOperaciones.DataSource = Nothing
            Me.dgCierre.DataSource = Nothing

            Me.txtcodcajero.Text = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Cedula")
            Me.txtnomcajero.Text = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Nombre")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").Current("Cajera") = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Cedula")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").Current("Nombre") = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Nombre")
            txtcodaperturacajero.Text = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura").ToString
            txtfechaapertura.Text = CDate(Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").Current("Apertura") = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura").ToString

            SubTotal = 0
            Devolucion = 0
            Vuelto = 0


            'LLenar Los Totales Por monedas

            For I = 0 To (Me.Dscierrecaja1.Moneda.Count - 1)
                Dr2 = tablacierre.NewRow
                Dr = tabla.NewRow
                Dr(0) = Me.Dscierrecaja1.Moneda.Rows(I).Item("MonedaNombre")
                TotalesPorMonedas(Dr, Dr2, Me.Dscierrecaja1.Moneda.Rows(I).Item("CodMoneda"), Me.Dscierrecaja1.Moneda.Rows(I).Item("ValorCompra"), Me.Dscierrecaja1.Moneda.Rows(I).Item("MonedaNombre"))
                tabla.Rows.Add(Dr)
                tablacierre.Rows.Add(Dr2)
            Next

            'Llenar las Operaciones
            For I = 0 To (Me.Dscierrecaja1.OpcionesDePago.Count - 1)
                Dr1 = Me.tablaresumen.NewRow
                Dr1(0) = Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("Documento")
                Dr1(1) = Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("TipoDocumento")
                Dr1(2) = Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("Nombremoneda")
                Dr1(3) = Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                'Si es una devoluci�n
                If Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "DVE" Then
                    Dr1(4) = "DVE"
                Else 'Si no es una devoluci�n
                    Select Case Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("FormaPago")
                        Case "EFE"
                            Dr1(4) = "EFE"
                        Case "MCE"
                            Dr1(4) = "MCE"
                        Case "MCS"
                            Dr1(4) = "MCS"
                        Case "TAR"
                            Dr1(4) = "TAR"
                        Case "CHE"
                            Dr1(4) = "CHE"
                        Case "TRA"
                            Dr1(4) = "TRA"
                    End Select
                End If
                Dr1(5) = (Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("Denominacion") * Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("TipoCambio"))
                tablaresumen.Rows.Add(Dr1)
            Next
            DescontarVuelto()
            Me.dgResumen.DataSource = tablaresumen
            Me.dgOperaciones.DataSource = tabla
            Me.dgCierre.DataSource = Me.tablacierre
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").Current("Subtotal") = (SubTotal + Devolucion)
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").Current("Devoluciones") = Devolucion
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").Current("TotalSistema") = SubTotal
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").EndCurrentEdit()
            ' CargarDetalleCierreCaja()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Function

    Function CargarCierreConsulta()
        Dim I As Integer
        Dim Dr As System.Data.DataRow
        Dim Dr1 As System.Data.DataRow
        Dim Dr2 As System.Data.DataRow
        'Valores Vacios a los grid
        Me.dgResumen.DataSource = Nothing
        Me.dgOperaciones.DataSource = Nothing
        Me.dgCierre.DataSource = Nothing

        Me.txtcodcajero.Text = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Cedula")
        Me.txtnomcajero.Text = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Nombre")

        SubTotal = 0
        Devolucion = 0
        Vuelto = 0

        'LLenar Los Totales Por monedas
        For I = 0 To (Me.Dscierrecaja1.Moneda.Count - 1)
            Dr2 = tablacierre.NewRow
            Dr = tabla.NewRow
            Dr(0) = Me.Dscierrecaja1.Moneda.Rows(I).Item("MonedaNombre")
            TotalesPorMonedas(Dr, Dr2, Me.Dscierrecaja1.Moneda.Rows(I).Item("CodMoneda"), Me.Dscierrecaja1.Moneda.Rows(I).Item("ValorCompra"), Me.Dscierrecaja1.Moneda.Rows(I).Item("MonedaNombre"))
            tabla.Rows.Add(Dr)
            tablacierre.Rows.Add(Dr2)
        Next

        'Llenar las Operaciones
        For I = 0 To (Me.Dscierrecaja1.OpcionesDePago.Count - 1)
            Dr1 = Me.tablaresumen.NewRow
            Dr1(0) = Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("Documento")
            Dr1(1) = 0
            Dr1(2) = Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("Nombremoneda")
            Dr1(3) = Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("MontoPago")
            'Si es una devoluci�n
            If Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "DVE" Then
                Dr1(4) = "DVE"
            Else 'Si no es una devoluci�n
                Select Case Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("FormaPago")
                    Case "EFE"
                        Dr1(4) = "EFE"
                    Case "TAR"
                        Dr1(4) = "TAR"
                    Case "CHE"
                        Dr1(4) = "CHE"
                    Case "TRA"
                        Dr1(4) = "TRA"
                End Select
            End If
            Dr1(5) = (Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("Denominacion") * Me.Dscierrecaja1.OpcionesDePago.Rows(I).Item("TipoCambio"))
            tablaresumen.Rows.Add(Dr1)
        Next
        DescontarVuelto()
        Me.dgResumen.DataSource = tablaresumen
        Me.dgOperaciones.DataSource = tabla
        Me.dgCierre.DataSource = Me.tablacierre

    End Function


    Function DescontarVuelto()
        Dim i As Integer
        'Efectivo Colones se le resta Los Vueltos
        ' tabla.Rows(0).Item(1) = (tabla.Rows(0).Item(1) - Vuelto)
        'Total Colones se le resta Los vueltos
        'tabla.Rows(0).Item(6) = (tabla.Rows(0).Item(6) - Vuelto)
        'tablacierre.Rows(0).Item(0) = tabla.Rows(0).Item(6)
        'tablacierre.Rows(0).Item(2) = tabla.Rows(0).Item(6)
        For i = 0 To (tabla.Rows.Count - 1)
            SubTotal += tabla.Rows(i).Item(6)
        Next
    End Function


#End Region

#Region "Cargar Detalle Cierre Caja"
    Function CargarDetalleCierreCaja()
        Dim I As Integer
        Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").EndCurrentEdit()
        Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").AddNew()
        Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").CancelCurrentEdit()

        For I = 0 To (Me.tabla.Rows.Count - 1)
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").EndCurrentEdit()
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").AddNew()
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").Current("Moneda") = tabla.Rows(I).Item("Moneda")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").Current("Efectivo") = tabla.Rows(I).Item("Efectivo")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").Current("Tarjeta") = tabla.Rows(I).Item("Tarjeta")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").Current("Cheque") = tabla.Rows(I).Item("Cheque")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").Current("Transferencia") = tabla.Rows(I).Item("Transferencia")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").Current("Total") = tabla.Rows(I).Item("Total")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").Current("Devoluciones") = tabla.Rows(I).Item("Devoluciones")
            Me.BindingContext(Me.Dscierrecaja1, "cierrecaja.cierrecajaDetalle_Cierrecaja").EndCurrentEdit()
        Next
    End Function

#End Region

#Region "Cargar Totales Por monedas"

    Function TotalesPorMonedas(ByRef Dr As DataRow, ByRef Dr1 As DataRow, ByVal CodigoMoneda As String, ByVal ValorCompra As Double, ByVal NombreMoneda As String) As DataRow

        Try
            Dim OperacionesMoneda() As DataRow
            Dim DenominacionesApertura() As DataRow
            Dim Operacion As DataRow
            Dim Denominacion As DataRow
            Dim J As Integer = 0
            Dim Vueltos As Double
            Dim Efectivo, Targeta, Cheque, Transferencia, devoluciones As Double
            Dim Denominaciones As Double
            OperacionesMoneda = Me.Dscierrecaja1.OpcionesDePago.Select("CodMoneda = '" & CodigoMoneda & "'")
            DenominacionesApertura = Me.Dscierrecaja1.DenominacionesAperturas.Select("CodMoneda = '" & CodigoMoneda & "'")

            'Cargar opciones de pago

            While J < OperacionesMoneda.Length
                Operacion = OperacionesMoneda(J)
                If Operacion("FormaPago") = "DVE" Then
                    devoluciones += Operacion("MontoPago")
                    Devolucion += (Operacion("MontoPago") * ValorCompra)
                Else

                    Vuelto += (Operacion("Denominacion") * Operacion("TipoCambio")) - (Operacion("MontoPago"))

                    Select Case Operacion("FormaPago")
                        Case "EFE", "MCE"
                            Efectivo += Operacion("MontoPago")
                        Case "MCS"
                            Efectivo -= Operacion("MontoPago")
                        Case "TAR"
                            Targeta += Operacion("MontoPago")
                        Case "CHE"
                            Cheque += Operacion("MontoPago")
                        Case "TRA"
                            Transferencia += Operacion("MontoPago")
                    End Select
                End If
                J += 1
            End While

            J = 0

            'Cargar Denominaciones apertura
            While J < DenominacionesApertura.Length
                Denominacion = DenominacionesApertura(J)
                Efectivo += Denominacion("Monto_Total")
                J += 1
            End While

            Dr(1) = (Efectivo)
            Dr(2) = Targeta
            Dr(3) = Cheque
            Dr(4) = Transferencia
            Dr(5) = devoluciones
            Dr(6) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones) * ValorCompra
            Dr1(0) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones)
            Dr1(1) = NombreMoneda
            Dr1(2) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones) * ValorCompra

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try

    End Function
#End Region

#Region "Consultar Cliente"
    Private Sub txtcodcajero_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtcodcajero.KeyDown
        If e.KeyCode = Keys.F1 Then
            consultarcliente()
        End If
    End Sub

#End Region

#Region "Valores Por Defecto"
    Private Sub defaultvalue()
        Me.Dscierrecaja1.cierrecaja.AperturaColumn.DefaultValue = 0
        Me.Dscierrecaja1.cierrecaja.AnuladoColumn.DefaultValue = 0
        Me.Dscierrecaja1.cierrecaja.FechaColumn.DefaultValue = Now
        Me.Dscierrecaja1.cierrecaja.DevolucionesColumn.DefaultValue = 0
        Me.Dscierrecaja1.cierrecaja.SubtotalColumn.DefaultValue = 0
        Me.Dscierrecaja1.cierrecaja.DevolucionesColumn.DefaultValue = 0
        Me.Dscierrecaja1.cierrecaja.TotalSistemaColumn.DefaultValue = 0
        Me.Dscierrecaja1.cierrecaja.EquivalenciaColumn.DefaultValue = 0
    End Sub
#End Region

#Region "Guardar"
    Public Function Guardar()
        CargarDetalleCierreCaja()
        CambiarEstadoApertura()
        If Registar() Then
            Me.Dscierrecaja1.AcceptChanges()
            If MsgBox("Desea imprimir el cierre de Caja", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Me.Imprimir()
            End If
            Me.tabla.Clear()
            Me.tablacierre.Clear()
            Me.tablaresumen.Clear()
            Me.Dscierrecaja1.aperturacaja.Clear()
            Me.Dscierrecaja1.Detalle_Cierrecaja.Clear()
            Me.Dscierrecaja1.cierrecaja.Clear()
            Me.Dscierrecaja1.DenominacionesAperturas.Clear()
            Me.txtcodcajero.Enabled = False
            MessageBox.Show("El cierre de caja ha sido registrado", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.ToolBar1.Buttons(0).Text = "Nuevo"
            Me.ToolBar1.Buttons(0).ImageIndex = 0
            Me.ToolBar1.Buttons(0).Enabled = True
            Me.ToolBar1.Buttons(1).Enabled = True
            Me.ToolBar1.Buttons(2).Enabled = False
            Me.ToolBar1.Buttons(3).Enabled = False
            Me.ToolBar1.Buttons(4).Enabled = False
            Me.ToolBar1.Buttons(5).Enabled = True
            txtfechaapertura.Text = ""
        Else
            MessageBox.Show("Problemas al tratar de registrar el  Cierre, Intente de nuevo, si el problema persiste pongase en contacto con el adminstrador del sistema", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Function

    Function Registar() As Boolean
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            'Apertura
            Me.SqlUpdateCommand7.Transaction = Trans
            Me.SqlUpdateCommand7.Transaction = Trans
            'Cierre Caja
            Me.SqlInsertCommand8.Transaction = Trans
            Me.SqlUpdateCommand13.Transaction = Trans
            'Detalle Cierre Caja
            Me.SqlInsertCommand9.Transaction = Trans
            Me.SqlUpdateCommand14.Transaction = Trans

            Me.daAperturacaja.Update(Me.Dscierrecaja1.aperturacaja)
            Me.daCierrecaja.Update(Me.Dscierrecaja1.cierrecaja)
            Me.daDetalle.Update(Me.Dscierrecaja1.Detalle_Cierrecaja)
            Trans.Commit()
            Return True
        Catch eEndEdit As System.Exception
            Trans.Rollback()
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            Return False
        End Try
    End Function



    Function CambiarEstadoApertura()
        Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Estado") = "C"
    End Function
#End Region

#Region "Anular"
    Private Sub anular()

        If MsgBox("Desea iniciar el proceso de anulaci�n de Cierre de Caja " & vbCrLf & "Esta acci�n reactiva la apertura en cuesti�n..", MsgBoxStyle.YesNo, "Atenci�n..") = MsgBoxResult.Yes Then
            Try
                Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").Current("anulado") = True
                Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").EndCurrentEdit()
                Me.BindingContext(Me.Dscierrecaja1, "aperturacaja").Current("Estado") = "A"
                Me.BindingContext(Me.Dscierrecaja1, "aperturacaja").EndCurrentEdit()
                If Registar() Then
                    MsgBox("Cierre anulado satisfactoriamente...")
                Else
                    MsgBox("Error al anular cierre, Intente de nuevo, si el problema persiste pongase en contacto con el administrador del sistema")
                End If

            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End If

    End Sub

#End Region

#Region "Nuevo"
    Private Sub nuevos()
        Try
            If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then
                'Restricci�n botones
                Me.lblanulado.Visible = False
                Me.ToolBar1.Buttons(0).Text = "Cancelar"
                Me.ToolBar1.Buttons(0).ImageIndex = 8
                Me.ToolBar1.Buttons(1).Enabled = False
                Me.ToolBar1.Buttons(2).Enabled = True
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = False
                Me.txtcodcajero.Enabled = True
                Me.txtcodcajero.Focus()
                'Limpia los campos
                'Me.gcMontoApertura.DataSource = Nothing
                Me.dgCierre.DataSource = Nothing
                Me.dgOperaciones.DataSource = Nothing
                Me.dgResumen.DataSource = Nothing
                Me.tabla.Clear()
                Me.tablacierre.Clear()
                Me.tablaresumen.Clear()
                Me.Dscierrecaja1.aperturacaja.Clear()
                Me.Dscierrecaja1.Detalle_Cierrecaja.Clear()
                Me.Dscierrecaja1.cierrecaja.Clear()
                Me.Dscierrecaja1.DenominacionesAperturas.Clear()
                Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").EndCurrentEdit()
                Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").AddNew()
            Else
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                Me.ToolBar1.Buttons(0).Enabled = True
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBar1.Buttons(2).Enabled = False
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = False
                Me.ToolBar1.Buttons(5).Enabled = True
                Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").CancelCurrentEdit()
                Me.tabla.Clear()
                Me.tablacierre.Clear()
                Me.tablaresumen.Clear()
                Me.Dscierrecaja1.aperturacaja.Clear()
                Me.Dscierrecaja1.Detalle_Cierrecaja.Clear()
                Me.Dscierrecaja1.cierrecaja.Clear()
                Me.Dscierrecaja1.DenominacionesAperturas.Clear()
                Me.txtcodcajero.Enabled = False
                txtfechaapertura.Text = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Toolbar"

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : nuevos()
            Case 2 : If PMU.Find Then Me.Consultar_Cierre() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Update Then Guardar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Delete Then anular() Else MsgBox("No tiene permiso para eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : If MessageBox.Show("�Desea Cerrar el m�dulo " & Me.Text & "..?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then Me.Close()
        End Select
    End Sub
#End Region

#Region "Imprimir"

    Function Imprimir()
        Try
            Dim rptCierreCaja As New ReporteCierreCaja
            rptCierreCaja.SetParameterValue(0, Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").Current("NumeroCierre"))
            CrystalReportsConexion.LoadShow(rptCierreCaja, MdiParent)
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function
#End Region

    Function Consultar_Cierre()

        '  Dim BuscarCierreCajas As New BuscarCierreCaja
        Me.dgCierre.DataSource = Nothing
        Me.dgOperaciones.DataSource = Nothing
        Me.dgResumen.DataSource = Nothing

        Me.tabla.Clear()
        Me.tablacierre.Clear()
        Me.tablaresumen.Clear()
        Me.Dscierrecaja1.DenominacionesAperturas.Clear()
        Me.Dscierrecaja1.aperturacaja.Clear()
        'Me.Dscierrecaja1.Apertura_Total_Tope.Clear() '1
        Me.Dscierrecaja1.OpcionesDePago.Clear() '2
        Me.Dscierrecaja1.Detalle_Cierrecaja.Clear()
        Me.Dscierrecaja1.cierrecaja.Clear()

        Me.tabla.AcceptChanges()

        Me.tablacierre.AcceptChanges()
        Me.tablaresumen.AcceptChanges()
        Me.Dscierrecaja1.DenominacionesAperturas.AcceptChanges()
        Me.Dscierrecaja1.aperturacaja.AcceptChanges()
        Me.Dscierrecaja1.Detalle_Cierrecaja.AcceptChanges()
        Me.Dscierrecaja1.cierrecaja.AcceptChanges()

        tabla.Clear()
        tablaresumen.Clear()
        tablacierre.Clear()
        SubTotal = 0
        Total = 0
        Devolucion = 0
        Vuelto = 0
        dgCierre.DataSource = Nothing
        Me.dgOperaciones.DataSource = Nothing

        Dim Fx As New cFunciones
        Dim Apertura As String
        Apertura = Fx.Buscar_X_Descripcion_Fecha("SELECT NumeroCierre AS Cierre, Nombre, Fecha FROM cierrecaja", "Nombre", "Fecha", "Buscar Cierre de Caja", Me.SqlConnection1.ConnectionString)
        ' If BuscarCierreCajas.ShowDialog = DialogResult.OK Then
        If Apertura <> "" Then
            '  CargarCierre(BuscarCierreCajas.Numapertura.Text)
            CargarCierre(Apertura)
            CargarAperturaConsulta(Me.Dscierrecaja1.cierrecaja.Rows(0).Item("Apertura"))
            CargarOpcionesPago(Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura"))
            CargarDenominacionesApertura(Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura"))
            txtcodaperturacajero.Text = Me.Dscierrecaja1.aperturacaja.Rows(0).Item("NApertura").ToString
            txtfechaapertura.Text = CDate(Me.Dscierrecaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
            CargarCierreConsulta()
            Me.ToolBar1.Buttons(3).Enabled = True
            Me.ToolBar1.Buttons(4).Enabled = True
            If Me.Dscierrecaja1.cierrecaja.Rows(0).Item("Anulado") = True Then
                Me.lblanulado.Visible = True
            Else
                Me.lblanulado.Visible = False
            End If
            'JCGA 10 AGOSTO 2007
            'Me.ToolBarRegistrar.Enabled = True
        Else
            MsgBox("No se optuvieron resultados", MsgBoxStyle.Information)
        End If
    End Function

    Function CargarCierre(ByVal Id As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSOFT", "Seepos", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM  cierrecaja where NumeroCierre = '" & Id & "'"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.Dscierrecaja1.cierrecaja)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Function
#Region "Logearse"
    Private Sub TextBox6_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox6.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                Dim clave As String
                Dim usuarios() As System.Data.DataRow 'almacena temporalmente los datos de un art�culo
                Dim usuario As System.Data.DataRow 'almacena temporalmente los datos de un art�culo

                clave = Me.TextBox6.Text
                usuarios = Me.Dscierrecaja1.Usuarios.Select("clave_interna= '" & CStr(clave) & "'")

                If usuarios.Length <> 0 Then
                    usuario = usuarios(0) 'se almacena la info del usuario
                    If usuario!nombre <> "" Then
                        PMU = VSM(usuario!Cedula, Me.Name) 'Carga los privilegios del usuario con el modulo 
                        If Not PMU.Execute Then MsgBox("No tiene permiso de acceso al m�dulo " & Me.Text, MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                        Me.Dscierrecaja1.cierrecaja.UsuarioColumn.DefaultValue = usuario!cedula
                        Me.Dscierrecaja1.cierrecaja.NombreUsuarioColumn.DefaultValue = usuario!nombre
                        Me.txtNombreUsuario.Text = usuario!nombre
                        Me.Usuario = usuario!nombre
                        Me.Cedula_Usuario = usuario!Cedula
                        Me.txtcodcajero.Enabled = True
                        Me.ToolBar1.Buttons(1).Enabled = True
                        IniciarEdicion()
                        Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").EndCurrentEdit()
                        Me.BindingContext(Me.Dscierrecaja1, "cierrecaja").AddNew()
                        Me.txtcodcajero.Focus()
                    Else
                        MsgBox("El Usuario no Existe", MsgBoxStyle.Information)
                        Me.TextBox6.Text = ""
                        Me.TextBox6.Focus()
                    End If
                Else
                    MsgBox("El Usuario no Existe", MsgBoxStyle.Information)
                    Me.TextBox6.Text = ""
                    Me.TextBox6.Focus()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End If
    End Sub
#End Region

    Private Sub TextBox6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox6.TextChanged

    End Sub

    Private Sub txtcodcajero_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcodcajero.TextChanged

    End Sub
End Class
