Imports DevExpress.Utils
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Data
Imports System.Windows.Forms

Public Class FrmEquivalencias
    Inherits System.Windows.Forms.Form
    Public codigo As String
    Public CodArticulo As String
#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridEquivalencias As DevExpress.XtraGrid.GridControl
    Friend WithEvents AdapterEquivalencias As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DataSetEquivalencias As LcPymes_5._2.DataSetEquivalencias
    Friend WithEvents colBarras_Equi As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ButtonAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BtnCancelar As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GridEquivalencias = New DevExpress.XtraGrid.GridControl
        Me.DataSetEquivalencias = New LcPymes_5._2.DataSetEquivalencias
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colBarras_Equi = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.AdapterEquivalencias = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.ButtonAceptar = New DevExpress.XtraEditors.SimpleButton
        Me.BtnCancelar = New DevExpress.XtraEditors.SimpleButton
        CType(Me.GridEquivalencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEquivalencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridEquivalencias
        '
        Me.GridEquivalencias.DataMember = "Inventario"
        Me.GridEquivalencias.DataSource = Me.DataSetEquivalencias
        '
        'GridEquivalencias.EmbeddedNavigator
        '
        Me.GridEquivalencias.EmbeddedNavigator.Name = ""
        Me.GridEquivalencias.Location = New System.Drawing.Point(8, 8)
        Me.GridEquivalencias.MainView = Me.GridView1
        Me.GridEquivalencias.Name = "GridEquivalencias"
        Me.GridEquivalencias.Size = New System.Drawing.Size(528, 232)
        Me.GridEquivalencias.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridEquivalencias.TabIndex = 0
        Me.GridEquivalencias.Text = "GridControl1"
        '
        'DataSetEquivalencias
        '
        Me.DataSetEquivalencias.DataSetName = "DataSetEquivalencias"
        Me.DataSetEquivalencias.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBarras_Equi, Me.colDescripcion})
        Me.GridView1.GroupPanelText = ""
        Me.GridView1.Name = "GridView1"
        '
        'colBarras_Equi
        '
        Me.colBarras_Equi.Caption = "Barras"
        Me.colBarras_Equi.FieldName = "Barras_Equi"
        Me.colBarras_Equi.Name = "colBarras_Equi"
        Me.colBarras_Equi.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colBarras_Equi.VisibleIndex = 0
        Me.colBarras_Equi.Width = 103
        '
        'colDescripcion
        '
        Me.colDescripcion.Caption = "Descripcion"
        Me.colDescripcion.FieldName = "Descripcion"
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDescripcion.VisibleIndex = 1
        Me.colDescripcion.Width = 311
        '
        'AdapterEquivalencias
        '
        Me.AdapterEquivalencias.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterEquivalencias.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Inventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Barras", "Barras"), New System.Data.Common.DataColumnMapping("Barras_Equi", "Barras_Equi"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Equivalente", "Equivalente"), New System.Data.Common.DataColumnMapping("Expr1", "Expr1")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Equivalencias.Id, Equivalencias.Codigo, Equivalencias.Barras, Equivalencia" & _
        "s.Barras_Equi, Inventario.Descripcion, Equivalencias.Equivalente, Inventario.Cod" & _
        "igo AS Expr1 FROM Inventario INNER JOIN Equivalencias ON Inventario.Codigo = Equ" & _
        "ivalencias.Equivalente"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""ERMIS-PC"";packet size=4096;integrated security=SSPI;data source=""" & _
        "ERMIS-PC\sqlmhpartes"";persist security info=False;initial catalog=Seepos"
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.Location = New System.Drawing.Point(144, 240)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(120, 28)
        Me.ButtonAceptar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        Me.ButtonAceptar.TabIndex = 5
        Me.ButtonAceptar.Text = "Aceptar"
        '
        'BtnCancelar
        '
        Me.BtnCancelar.Location = New System.Drawing.Point(272, 240)
        Me.BtnCancelar.Name = "BtnCancelar"
        Me.BtnCancelar.Size = New System.Drawing.Size(120, 28)
        Me.BtnCancelar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        Me.BtnCancelar.TabIndex = 6
        Me.BtnCancelar.Text = "Cancelar"
        '
        'FrmEquivalencias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(544, 274)
        Me.Controls.Add(Me.BtnCancelar)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.GridEquivalencias)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmEquivalencias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Equivalencias"
        CType(Me.GridEquivalencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEquivalencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub FrmEquivalencias_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cFunciones.Llenar_Tabla_Generico("SELECT dbo.Equivalencias.Id, dbo.Equivalencias.Codigo, dbo.Equivalencias.Barras, dbo.Equivalencias.Barras_Equi, dbo.Inventario.Descripcion,dbo.Equivalencias.Equivalente FROM dbo.Inventario INNER JOIN  dbo.Equivalencias ON dbo.Inventario.Codigo = dbo.Equivalencias.Equivalente WHERE dbo.Equivalencias.barras = '" & codigo & "'", Me.DataSetEquivalencias.Inventario)
    End Sub

    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        CodArticulo = BindingContext(DataSetEquivalencias, "Inventario").Current("Barras_Equi")
        Me.Close()
    End Sub

    Private Sub BtnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Me.Close()
    End Sub
End Class
