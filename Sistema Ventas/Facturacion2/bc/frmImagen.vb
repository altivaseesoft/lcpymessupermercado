Imports System.Data
Imports System.Windows.Forms

Public Class frmImagen
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Imagen_dts1 As LcPymes_5._2.imagen_dts
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Imagen_dts1 = New LcPymes_5._2.imagen_dts
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit
        CType(Me.Imagen_dts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Imagen_dts1
        '
        Me.Imagen_dts1.DataSetName = "imagen_dts"
        Me.Imagen_dts1.Locale = New System.Globalization.CultureInfo("es-ES")
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureEdit1.Location = New System.Drawing.Point(0, 0)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Size = New System.Drawing.Size(392, 254)
        Me.PictureEdit1.TabIndex = 73
        '
        'frmImagen
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(392, 254)
        Me.Controls.Add(Me.PictureEdit1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImagen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Imagen"
        CType(Me.Imagen_dts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public codigo As String
    Public conexion As String = GetSetting("seesoft", "seepos", "conexion")
    Public Sub cargaimagen(ByVal _codigo As String)
        cFunciones.Llenar_Tabla_Generico("select codigo, imagen from inventario where barras = '" & _codigo & "'", Me.Imagen_dts1.Inventario, conexion)
    End Sub

    Private Sub frmImagen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            PictureEdit1.DataBindings.Add(New Binding("EditValue", Me.Imagen_dts1, "Inventario.Imagen"))
            cargaimagen(Me.codigo)
        Catch ex As Exception
        End Try
    End Sub
End Class
