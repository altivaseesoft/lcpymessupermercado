﻿Public Class frmDocExoneracion
    Private Sub btAceptar_Click(sender As Object, e As EventArgs) Handles btAceptar.Click
        If DatosValidos() Then
            DialogResult = Windows.Forms.DialogResult.OK
            Close()
        End If


    End Sub
    Private Function DatosValidos() As Boolean
        If dtpFecha.Value > Now Then
            MsgBox("Advertencia la fecha del documento el mayor que la fecha actual.")
        End If
        If txtNumDoc.Text.Equals("") Then
            MsgBox("El documento no puede queda en blanco.")
            Return False
        End If

        Return True

    End Function
End Class