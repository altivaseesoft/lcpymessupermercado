﻿Imports System.Data.SqlClient

Public Class FormImportarExcel
    Private Tabla_Datos As New DataTable
    Private Sub _Formato_Columnas(ByVal miTabla As DataTable)
        'Creamos las columnas: 
        miTabla.Columns.Add(New DataColumn("Cod_Barras", GetType(String)))
        miTabla.Columns.Add(New DataColumn("Descripcion", GetType(String)))
        miTabla.Columns.Add(New DataColumn("Cantidad", GetType(Double)))
        miTabla.Columns.Add(New DataColumn("Precio_Costo", GetType(Double)))
        miTabla.Columns.Add(New DataColumn("Precio_Venta", GetType(Double)))
        miTabla.Columns.Add(New DataColumn("Cod_Marca", GetType(Integer)))
        miTabla.Columns.Add(New DataColumn("SubFamilia", GetType(String)))
        miTabla.Columns.Add(New DataColumn("Max_Descuento", GetType(Double)))
    End Sub
    Private Sub _leer()
        OpenFileDialog1.ShowDialog()
        'Hay que referenciar la libreria de excel que esta en COM
        'Variable de tipo Aplicación de Excel
        Dim objExcel As Microsoft.Office.Interop.Excel.Application
        'Una variable de tipo Libro de Excel
        Dim xLibro As Microsoft.Office.Interop.Excel.Workbook
        'Una variable de tipo Hoja de Excel
        Dim xHoja As Microsoft.Office.Interop.Excel.Worksheet
        'Una variable Col para las columnas y Fila para las filas
        Dim Ruta As String = ""
        Dim Col As Integer
        Dim Fila As Integer

        'creamos un nuevo objeto excel
        objExcel = New Microsoft.Office.Interop.Excel.Application

        'Usamos el método open para abrir el archivo que está en el directorio del programa llamado archivo.xls
        'xLibro = objExcel.Workbooks.Open(My.Application.Info.DirectoryPath & "\archivo.xlsx")
        'xLibro = objExcel.Workbooks.Open("c:\...\archivo.xlsx")
        'Ó podemos ponerlo con el diálogo de abrir un archivo como hice en este caso
        Ruta = OpenFileDialog1.FileName.ToString
        If Ruta = "OpenFileDialog1" Or Ruta = "" Then
            Exit Sub
        End If
        Me.txtRuta.Text = Ruta

        xLibro = objExcel.Workbooks.Open(Ruta)
        'Se debe tomar el nombre de la hoja que se desea leer
        xHoja = objExcel.Worksheets("Hoja1")

        'Creamos el DataTable
        Dim miTabla As New DataTable
        Me._Formato_Columnas(miTabla)

        'Se puede hacer el Excel Visible con:
        'objExcel.Visible = True
        '    Hacemos referencia al libro
        With xLibro
            '    Hacemos referencia a la Hoja
            With xHoja

                'Recorremos la fila 1 hasta la x
                For Fila = 2 To 200000
                    Dim dr As DataRow
                    dr = miTabla.NewRow
                    Dim miarray As New ArrayList
                    'Recorremos la columna 1 hasta la 4
                    For Col = 1 To 8
                        'Agregamos el valor de la fila que corresponde al valor de la columna a nuestro 
                        Dim temp As String = .Cells(Fila, Col).Value
                        If temp <> "" Then
                            miarray.Add(temp)
                            ' si codigo de barras esta vacio = columna 1, se agrega del codigo barras vacio
                        ElseIf Col = 1 Then
                            If temp = Nothing Then
                                temp = ""
                            End If
                            miarray.Add(temp)
                            ' si descripcion esta vacio = columna 2, deja de contar las filas
                        ElseIf Col = 2 Then
                            'MsgBox("Archivo leido, revisa los datos en el Data Grid")
                            'Se cierra el libro de excel
                            xLibro.Close(SaveChanges:=False)
                            objExcel.Quit()
                            'Eliminamos los objetos si ya no los usamos
                            objExcel = Nothing
                            xLibro = Nothing
                            xHoja = Nothing
                            'Asignar el datagrid al datatable: 
                            Me.Dgv.DataSource = Nothing
                            Me.Tabla_Datos = miTabla
                            Me.Dgv.DataSource = miTabla
                            Exit Sub
                            ' si cantidad esta vacio = columna 3,agrega cantidad 0
                        ElseIf Col = 3 Then
                            miarray.Add(0)
                            ' si costo esta vacio = columna 4,agrega costo 1
                        ElseIf Col = 4 Then
                            miarray.Add(1)
                            ' si precio venta + ivi esta vacio = columna 5,agrega  precio venta + ivi 1
                        ElseIf Col = 5 Then
                            miarray.Add(1)
                            ' si COD MARCA esta vacio = columna 5,agrega  precio venta + ivi 1
                        ElseIf Col = 6 Then
                            miarray.Add(1)
                            ' si SUBFAMILIA esta vacio = columna 5,agrega  precio venta + ivi 1
                        ElseIf Col = 7 Then
                            miarray.Add("1-0")
                            ' si MAX_DESCUENTO esta vacio = columna 5,agrega  precio venta + ivi 1
                        ElseIf Col = 8 Then
                            miarray.Add(100)

                        End If
                    Next
                    dr("Cod_Barras") = Me._Eliminar_Comillas(Trim(miarray.Item(0).ToString.ToUpper))
                    dr("Descripcion") = Me._Eliminar_Comillas(Trim(miarray.Item(1).ToString.ToUpper))
                    dr("Cantidad") = Me._Eliminar_Letras(CStr(miarray.Item(2)))
                    dr("Precio_Costo") = Me._Eliminar_Letras(CStr(miarray.Item(3)))
                    dr("Precio_Venta") = Me._Eliminar_Letras(CStr(miarray.Item(4)))
                    dr("Cod_Marca") = Me._Eliminar_Letras(CStr(miarray.Item(5)))
                    dr("SubFamilia") = Me._Eliminar_Comillas(CStr(miarray.Item(6)))
                    dr("Max_Descuento") = Me._Eliminar_Letras(CStr(miarray.Item(7)))

                    miTabla.Rows.Add(dr)
                    If Fila > 2000000 Then
                        MessageBox.Show("Error: la cantidad de items agregados no puede ser mayor que 1000", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    End If
                Next
            End With
        End With
        'MsgBox("Archivo leido, revisa los datos en el Data Grid")
        'Se cierra el libro de excel
        xLibro.Close(SaveChanges:=False)
        objExcel.Quit()

        'Eliminamos los objetos si ya no los usamos
        objExcel = Nothing
        xLibro = Nothing
        xHoja = Nothing
        'Asignar el datagrid al datatable: 
        Me.Dgv.DataSource = Nothing
        Me.Tabla_Datos = miTabla
        Me.Dgv.DataSource = miTabla
    End Sub
    Private Sub _Importar()
        _leer()
        Me.ToolBarRegistrar.Enabled = True
      
    End Sub
    Private Sub _Registrar()
        For f As Integer = 0 To Me.Tabla_Datos.Rows.Count - 1
            If Me.Tabla_Datos.Rows(f).Item(0) = "" Or Me.Tabla_Datos.Rows(f).Item(1) = "" Then
                MessageBox.Show("Revise la fila del archivo excel num : " & CStr(f + 2) & "; Le faltan datos. ", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If
        Next

        'If MessageBox.Show("¿Esta seguro que desea importar de Excel?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
        For f As Integer = 0 To Me.Tabla_Datos.Rows.Count - 1

            Dim Codigo As Integer = 0
            Codigo = CInt(Me._SlqExecuteScalar("SELECT isnull(MAX(Codigo),0) FROM Inventario") + 1)
            Dim Barras As String = ""
            Barras = Me.Tabla_Datos.Rows(f).Item(0)
            Dim Descripcion As String = ""
            Descripcion = Me.Tabla_Datos.Rows(f).Item(1)
            Dim PresentaCant As Integer = 1
            ' ESTA VARIABLE ES CAMBIANTE
            Dim CodPresentacion As Integer = 76
            ' ESTA VARIABLE ES CAMBIANTE
            Dim CodMarca As Integer = 1
            CodMarca = Me.Tabla_Datos.Rows(f).Item(5)


            ' ESTA VARIABLE ES CAMBIANTE
            Dim SubFamilia As String = "1-0"
            SubFamilia = Me.Tabla_Datos.Rows(f).Item(6)

            Dim Minima As Double = 0
            Dim PuntoMedio As Double = 0
            Dim Maxima As Double = 0
            Dim Existencia As Integer = 0
            Existencia = Me.Tabla_Datos.Rows(f).Item(2)
            ' ESTA VARIABLE ES CAMBIANTE
            Dim SubUbicacion As String = "2-1"
            Dim Observaciones As String = "Importacion Excel"
            Dim MonedaCosto As Integer = _Obtener_MonedaCosto()

            Dim PrecioBase As Double = 0
            PrecioBase = Me.Tabla_Datos.Rows(f).Item(3)
            Dim Fletes As Double = 0
            Dim OtrosCargos As Double = 0
            Dim Costo As Double = 0
            Costo = Me.Tabla_Datos.Rows(f).Item(3)
            Dim MonedaVenta As Integer = _Obtener_MonedaVenta()
            Dim IVenta As Integer = 13

            If Me.ckExento.Checked = True Then
                IVenta = 0
            End If
            Dim Precio_A As Double = 0
            Precio_A = _Obtener_PrecioVenta(Me.Tabla_Datos.Rows(f).Item(4), IVenta)
            Dim Precio_B As Double = 0
            Dim Precio_C As Double = 0
            Dim Precio_D As Double = 0
            Dim Precio_Promo As Double = 0

            Dim Promo_Activa As Integer = 0
            Dim Promo_Inicio As Date = Now.Date
            Dim Promo_Finaliza As Date = Now.Date
            Dim Max_Comision As Integer = 0
            Dim Max_Descuento As Integer = 100
            Max_Descuento = Me.Tabla_Datos.Rows(f).Item(7)

            '  Dim Imagen
            Dim FechaIngreso As Date = Now.Date
            Dim Servicio As Integer = 0
            Dim Inhabilitado As Integer = 0
            ' ESTA VARIABLE ES CAMBIANTE
            'Dim Proveedor As Integer = 11
            Dim Precio_Sugerido As Double = 0
            Dim SugeridoIV As Double = 0
            Dim PreguntaPrecio As Integer = 0
            Dim Lote As Integer = 0
            Dim Consignacion As Integer = 0
            Dim Id_Bodega As Integer = 0
            Dim ExistenciaBodega As Integer = 0
            ExistenciaBodega = Me.Tabla_Datos.Rows(f).Item(2)
            Dim Apartado As Integer = 0

            ''------------------------------------------------------------------
            ''verifica si existe en la base de datos
            Dim consulta_busca_articulo As String = "SELECT  isnull(Codigo,0) FROM inventario WHERE barras = '" & Barras & "'"
            Dim existe As Integer = 0
            existe = CInt(_SlqExecuteScalar(consulta_busca_articulo))
            If existe = 0 Then
                If MessageBox.Show("¿Desea insertar un nuevo articulo : Cod. Barras: " & Barras & " - Descripcion: " & Descripcion & "- Precio_Sugerido: " & Precio_Sugerido & " ?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                    Dim consulta_insert As String = " INSERT INTO Inventario " & _
                                   " (Codigo,Barras,Descripcion,PresentaCant,CodPresentacion,CodMarca,SubFamilia,Minima,PuntoMedio,Maxima,Existencia,ExistenciaBodega," & _
                                   "BodegaConsignacion,SubUbicacion,Observaciones,MonedaCosto,PrecioBase,Fletes,OtrosCargos,Costo," & _
                                   "MonedaVenta,IVenta,Precio_A,Precio_B,Precio_C,Precio_D,Precio_Promo,Promo_Activa,Promo_Inicio,Promo_Finaliza,Max_Comision,Max_Descuento,FechaIngreso,Servicio,Inhabilitado ) " & _
                               " VALUES " & _
                                " (" & Codigo & ",'" & Barras & "' , '" & Descripcion & "' , " & PresentaCant & " , " & CodPresentacion & " , " & CodMarca & " , '" & SubFamilia & "' , " & Minima & " , " & PuntoMedio & " , " & Maxima & " ," & Existencia & " ," & ExistenciaBodega & "," & _
                                    Consignacion & " , '" & SubUbicacion & "','" & Observaciones & "' , " & MonedaCosto & " , " & PrecioBase & ", " & Fletes & " , " & OtrosCargos & " , " & Costo & ", " & MonedaVenta & " , " & IVenta & " , " & Precio_A & ", " & Precio_B & " , " & Precio_C & " , " & _
                                    Precio_D & " , " & Precio_Promo & " , " & Promo_Activa & " , '" & Promo_Inicio & "' , '" & Promo_Finaliza & "' , " & Max_Comision & " , " & Max_Descuento & " , '" & FechaIngreso & "',  " & _
                                     Servicio & " , " & Inhabilitado & " ) "
                    If Me._SlqExecute(consulta_insert) = 0 Then
                        MessageBox.Show("No se registro el : cod: " & Barras & " - Descripcion: " & Descripcion & "- Precio_Sugerido: " & Precio_Sugerido, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        'Else
                        ' Dim consulta_update As String = " UPDATE ArticulosXBodega " & _
                        '    " SET Existencia = " & ExistenciaBodega & _
                        '   " WHERE (Codigo = " & Codigo & ")"
                        ' Me._SlqExecute(consulta_update)
                        'MessageBox.Show("Se registro el : Cod. Barras: " & Barras & " - Descripcion: " & Descripcion & "- Precio_Sugerido: " & Precio_Sugerido, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

                    End If

                End If
                'Dim Tipo_Cambio As Double = Me._SlqExecuteScalar("select ValorCompra from dbo.Moneda where CodMoneda = " & MonedaCosto)
                'Dim Registra_Kardex As String = ""
                'Registra_Kardex = " INSERT INTO Kardex " & _
                '                " ( Codigo, Descripcion, Documento, Tipo, Fecha, Exist_Ant, Cantidad, Exist_Act, Costo_Unit, Costo_Mov, Cod_Proveedor, Cod_Cliente," & _
                '                " Cod_Moneda, IdBodegaOrigen, IdBodegaDestino, Exist_AntBod, Exist_ActBod, Tipo_Cambio, Observaciones) " & _
                '                " VALUES " & _
                '                " ( " & Codigo & " , '" & Descripcion & "', " & Codigo & ", 'IREXCEL', getdate(), 0, " & Existencia & ", " & Existencia & ", " & Costo & "," & Existencia * Costo & ", 0, 0," & _
                '                 MonedaCosto & ", 0, 0, 0, 0, Tipo_Cambio, 'REGISTRADO DE EXCEL') " & _
                ' Me._SlqExecute(Registra_Kardex)
            Else

                If CInt(_SlqExecuteScalar("SELECT COUNT(Codigo)  FROM Inventario  WHERE (Barras = '" & Barras & "')")) = 1 Then


                    Me.ck_Act_Sol_Exist.Checked = True
                    Dim dr As SqlDataReader
                    dr = _GetRecorset(_Conectar("SeePos"), "SELECT Codigo, Barras, Descripcion  FROM  Inventario WHERE (Codigo = " & CStr(existe) & ")")
                    If dr.Read Then
                        If existe = dr("Codigo") Then
                            If MessageBox.Show("¿El articulo Cod. Barras: " & dr("Barras") & "-Descripcion: " & dr("Descripcion") & "; desea actualizarlo ?", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then


                                Dim consulta_existencia_actual As String = "SELECT Existencia FROM Inventario WHERE (Codigo = " & existe & ")"
                                Dim existencia_actual As Integer = 0
                                existencia_actual = _SlqExecuteScalar(consulta_existencia_actual)

                                Dim consulta_actualiza_inventario As String
                                Dim consulta_update As String = ""
                                If Me.ck_sumar.Checked = True Then
                                    If Me.ck_Act_Sol_Exist.Checked = False Then
                                        consulta_actualiza_inventario = "UPDATE Inventario SET  Precio_A=" & Precio_A & ",Descripcion='" & Descripcion & "' , Existencia = " & Existencia + existencia_actual & ", PrecioBase = " & PrecioBase & ", Costo = " & Costo & ", Precio_A = " & Precio_A & ", ExistenciaBodega =" & ExistenciaBodega + existencia_actual & ", MonedaVenta = " & MonedaVenta & " ,MonedaCosto = " & MonedaCosto & " WHERE (Codigo = " & existe & ")"
                                    Else
                                        consulta_actualiza_inventario = "UPDATE Inventario SET  Precio_A=" & Precio_A & ",Descripcion='" & Descripcion & "',Existencia = " & Existencia + existencia_actual & ",  ExistenciaBodega =" & ExistenciaBodega + existencia_actual & ", MonedaVenta = " & MonedaVenta & " ,MonedaCosto = " & MonedaCosto & " WHERE (Codigo = " & existe & ")"
                                    End If
                                    ' consulta_update = " UPDATE ArticulosXBodega " & _
                                    '" SET Existencia = " & Existencia + existencia_actual & _
                                    '" WHERE (Codigo = " & existe & ")"
                                Else
                                    If Me.ck_Act_Sol_Exist.Checked = False Then
                                        consulta_actualiza_inventario = "UPDATE Inventario SET Barras = '" & Barras & "' , Descripcion = '" & Descripcion & "' , Existencia = " & Existencia & ", PrecioBase = " & PrecioBase & ", Costo = " & Costo & ", Precio_A = " & Precio_A & ", ExistenciaBodega =" & ExistenciaBodega & ", MonedaVenta = " & MonedaVenta & " ,MonedaCosto = " & MonedaCosto & " WHERE (Codigo = " & existe & ")"
                                    Else
                                        consulta_actualiza_inventario = "UPDATE Inventario SET  Precio_A=" & Precio_A & ",Descripcion='" & Descripcion & "',Existencia = " & Existencia & ",  ExistenciaBodega =" & ExistenciaBodega & ", MonedaVenta = " & MonedaVenta & " ,MonedaCosto = " & MonedaCosto & " WHERE (Codigo = " & existe & ")"
                                    End If
                                    'consulta_update = " UPDATE ArticulosXBodega " & _
                                    '  " SET Existencia = " & Existencia & _
                                    '" WHERE (Codigo = " & existe & ")"
                                End If
                                If Me._SlqExecute(consulta_actualiza_inventario) Then
                                    If Me._SlqExecute(consulta_actualiza_inventario) Then
                                        Dim Tipo_Cambio As Double = Me._SlqExecuteScalar("select ValorCompra from dbo.Moneda where CodMoneda = " & MonedaCosto)
                                        Dim Actualiza_Kardex As String = " INSERT INTO Kardex " & _
                                                         " ( Codigo, Descripcion, Documento, Tipo, Fecha, Exist_Ant, Cantidad, Exist_Act, Costo_Unit, Costo_Mov, Cod_Proveedor, Cod_Cliente," & _
                                                         " Cod_Moneda,Tipo_Cambio, Observaciones) " & _
                                                         " VALUES " & _
                                                         " ( " & existe & " , '" & Descripcion & "', " & existe & ", 'AIE', getdate(), 0, " & Existencia & ", " & Existencia & ", " & Costo & "," & Existencia * Costo & ", 0, 0," & _
                                                          MonedaCosto & ", " & Tipo_Cambio & " , 'Actualizacion Importada de Excel, Usuario: " & Me.txtNombreUsuario.Text & "') "
                                        Me._SlqExecute(Actualiza_Kardex)
                                    Else
                                        MessageBox.Show("La Consulta: ' " & consulta_update & " '; No se pudo ejecutar", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

                                    End If
                                Else
                                    MessageBox.Show("La Consulta: ' " & consulta_actualiza_inventario & " '; No se pudo ejecutar", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

                                End If

                            End If
                        Else
                            ' poner funcion eliminar uno de los repetidos
                            MessageBox.Show("Los Codigos Internos son diferentes, " & existe & " <> " & dr("Codigo") & "; Puede ser que existan mas de un articulo registrado con el mismo codigo de barras (" & dr("Barras") & " )", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        End If
                    End If
                Else
                    MessageBox.Show("Puede ser que existan mas de un articulo registrado con el mismo codigo de barras (" & Barras & " )", "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

                End If


            End If
        Next
        'LIMPIO OBJETOS 
        Me.Tabla_Datos.Clear()
        Me.Dgv.DataSource = Nothing
        Me.txtRuta.Text = ""
        Me.ToolBarRegistrar.Enabled = False
        Me.txtNombreUsuario.Text = ""
        Me.txtUsuario.Text = ""
        Me.Panel1.Enabled = False
        Me.ToolBar1.Enabled = False
        MsgBox("Proceso terminado")
        ' End If

    End Sub
    Private Function _Eliminar_Comillas(ByVal palabra As String) As String
        Return palabra.Replace("'", " ")
    End Function
    Private Function _SlqExecute(ByVal Consulta As String) As Integer
        Dim t As Integer = 0

        Dim con As New SqlConnection(GetSetting("SeeSoft", "SeePos", "Conexion"))
        Using con
            Dim cmd As New SqlCommand(Consulta, con)
            Try
                Me._Conectar(con)
                t = cmd.ExecuteNonQuery()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                Return t
            End Try
            Me._Desconectar(con)
        End Using
        Return t
    End Function
    Private Function _SlqExecuteScalar(ByVal Consulta As String) As String
        Dim Dato As String = ""
        Dim con As New SqlConnection(GetSetting("SeeSoft", "SeePos", "Conexion"))

        Using con
            Dim cmd As New SqlCommand(Consulta, con)
            Try
                Me._Conectar(con)
                Dato = CStr(cmd.ExecuteScalar())

            Catch ex As Exception
                MessageBox.Show(ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Finally
                Me._Desconectar(con)
            End Try

        End Using
        Return Dato
    End Function
    Private Function _Obtener_PrecioVenta(ByVal PrecioVenta_IVI As Double, ByVal IVenta As Double) As Double
        Return PrecioVenta_IVI / (1 + (IVenta / 100))
    End Function
    Private Sub _Conectar(ByVal Con As SqlConnection)
        If Con.State <> ConnectionState.Open Then Con.Open()
    End Sub
    Private Sub _Desconectar(ByVal Con As SqlConnection)
        If con.State <> ConnectionState.Closed Then con.Close()
    End Sub
    Private Function _Eliminar_Letras(ByVal numero As String) As Double
        Dim nuevo As String = ""
        Try
            For i As Integer = 0 To numero.Length - 1
                If Char.IsLetter(numero(i)) Then
                    Dim Letra As Char = numero(i)
                    nuevo = numero.Replace(Letra, "0")
                    numero = nuevo
                End If
            Next
            Return CDbl(numero)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Atención...", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Function

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : _Importar()
            Case 2 : _Registrar()
            Case 3 : If MessageBox.Show("¿Desea Cerrar el módulo de Importar Datos", "Atención...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then Me.Dispose(True) : Me.Close()
        End Select
    End Sub
    Private Function _Obtener_MonedaVenta() As Integer

        '-----------------------------------------------------------------------
        Dim mi_moneda As Integer = 1
        Try
            mi_moneda = Me._SlqExecuteScalar("select CodMoneda from Moneda where MonedaNombre = '" & Me.ComboBoxTipoMonedaVenta.Text & "'")
        Catch ex As Exception
            SaveSetting("SeeSOFT", "SeePos", "Moneda", "1")
            mi_moneda = CInt(GetSetting("SeeSOFT", "SeePos", "Moneda"))

        End Try
        Return mi_moneda
        '-----------------------------------------------------------------------

    End Function
    Private Function _Obtener_MonedaCosto() As Integer

        '-----------------------------------------------------------------------
        Dim mi_moneda As Integer = 1
        Try
            mi_moneda = Me._SlqExecuteScalar("select CodMoneda from Moneda where MonedaNombre = '" & Me.ComboBoxTipoMonedaCosto.Text & "'")
        Catch ex As Exception
            SaveSetting("SeeSOFT", "SeePos", "Moneda", "1")
            mi_moneda = CInt(GetSetting("SeeSOFT", "SeePos", "Moneda"))

        End Try
        Return mi_moneda
        '-----------------------------------------------------------------------

    End Function


    Private Sub FormImportarExcel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Seguridad", "CONEXION")
        Catch ex As SystemException
            MsgBox(ex.ToString)
        End Try
        Adapter_Moneda.Fill(Me.DataSetImportarExcel1, "Moneda")

    End Sub

    Private Sub txtUsuario_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyCode = Keys.Enter Then
            _Loggin_Usuario(Me.txtUsuario.Text)
        End If

    End Sub
    Public Function _GetRecorset(ByRef conexion As SqlConnection, ByVal StrQuery As String) As SqlDataReader
        Dim Command As SqlCommand
        Dim SqlDatos As SqlDataReader
        Dim Mensaje As String
        Try
            Command = New SqlCommand(StrQuery, conexion)
            SqlDatos = Command.ExecuteReader
        Catch ex As Exception
            Mensaje = ex.Message
            MsgBox("Favor Comunicar el siguiente Error a su Empresa Proveedora de Software.:" & vbCrLf & Mensaje, MsgBoxStyle.Critical, "Alerta...")
        Finally
            Command.Dispose()
            Command = Nothing
        End Try
        Return SqlDatos
    End Function
    Public Function _Conectar(Optional ByVal Modulo As String = "SeePOS") As SqlConnection
        Dim sQlconexion As New SqlConnection
        Dim SQLStringConexion As String
        If sQlconexion.State <> ConnectionState.Open Then
            SQLStringConexion = GetSetting("SeeSOFT", Modulo, "Conexion")
            sQlconexion.ConnectionString = SQLStringConexion
            If sQlconexion.ConnectionString <> "" Then sQlconexion.Open()
        Else
        End If
        Return sQlconexion
    End Function
    Private Function _Loggin_Usuario(ByVal clave_interna As String) As Boolean
        Try

            If clave_interna <> "" Then
                Dim Consulta As String = " SELECT     Usuarios.Id_Usuario, Usuarios.Nombre, Perfil_x_Modulo.Accion_Ejecucion " & _
" FROM         Modulos INNER JOIN " & _
                      " Perfil_x_Modulo ON Modulos.Id_modulo = Perfil_x_Modulo.Id_Modulo INNER JOIN " & _
                      " Perfil ON Perfil_x_Modulo.Id_Perfil = Perfil.Id_Perfil INNER JOIN " & _
                      " Perfil_x_Usuario ON Perfil.Id_Perfil = Perfil_x_Usuario.Id_Perfil INNER JOIN " & _
                      " Usuarios ON Perfil_x_Usuario.Id_Usuario = Usuarios.Id_Usuario " & _
" WHERE     (Modulos.Modulo_Nombre_Interno = 'Importar_Excel') AND (Usuarios.Clave_Interna = '" & clave_interna & "')"
                Dim dr As SqlDataReader
                dr = Me._GetRecorset(Me._Conectar("Seguridad"), Consulta)
                If dr.Read Then
                    If dr("Accion_Ejecucion") Then
                        Me.txtNombreUsuario.Text = dr("Nombre")
                        Me.Panel1.Enabled = True
                        Me.ToolBar1.Enabled = True
                    Else
                        Me.txtNombreUsuario.Text = ""
                        Me.txtUsuario.Text = ""
                        Me.txtUsuario.Focus()
                        MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atención...")
                        Exit Function
                    End If
                Else
                    Me.txtNombreUsuario.Text = ""
                    Me.txtUsuario.Text = ""
                    Me.txtUsuario.Focus()
                    MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atención...")
                    Exit Function
                End If
            Else
                MsgBox("Digite Clave de Usuario")
                txtUsuario.Text = ""
                Me.txtUsuario.Focus()
                Exit Function
            End If
        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atención...")
        End Try
    End Function

   
End Class
