Imports System.Data.SqlClient
Imports System.Data
Imports System.Windows.Forms

Public Class GenerarConsecutivoHA
    Private _suc As String
    Private _ptoVenta As String
    Private _tipoDoc As String
    Private _consecutivoHA As String
    Private _numConsecutiva As String
    Dim strQuery As String
    Dim CConexion As New Conexion
    Dim sqlConexion As New SqlConnection


    Public Function crearNumeroConsecutivo(ByVal _CodCliente As String, ByVal idFactura As Integer, ByVal _numFactura As Integer, Optional ByVal _esAnulacion As Boolean = False)
        Dim rs As SqlDataReader

        sqlConexion = CConexion.Conectar

        rs = CConexion.GetRecorset(sqlConexion, "Select Consecutivo_HA_FE,Consecutivo_HA_TIQE,Consecutivo_HA_NCE,No_Sucursal,No_Caja  From dbo.TB_CE_Cofiguracion")

        While rs.Read
            Try
                If _CodCliente.Equals("0") Then
                    _tipoDoc = "04"
                    _consecutivoHA = rs("Consecutivo_HA_TIQE")
                ElseIf _CodCliente <> "0" Then
                    _tipoDoc = "01"
                    _consecutivoHA = rs("Consecutivo_HA_FE")
                End If

                If _esAnulacion = True Then
                    _tipoDoc = "03"
                    _consecutivoHA = rs("Consecutivo_HA_NCE")
                End If
                _suc = rs("No_Sucursal")
                _ptoVenta = rs("No_Caja")

            Catch ex As Exception
                MessageBox.Show(ex.Message)
                CConexion.DesConectar(CConexion.sQlconexion)
            End Try
        End While
        rs.Close()

        _numConsecutiva = _suc + _ptoVenta + _tipoDoc + _consecutivoHA

        strQuery = "INSERT INTO Seepos.dbo.TB_CE_ConsecutivoProvisionalHA (Id_Factura,Num_Factura,Consecutivo_Hacienda,TipoDocElectronico)  VALUES ('" & idFactura & "','" & _numFactura & "','" & _numConsecutiva & "','" & _tipoDoc & "')"

        CConexion.SlqExecute(sqlConexion, strQuery)
        CConexion.DesConectar(CConexion.sQlconexion)
    End Function

End Class
