Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data
Imports System.Windows.Forms
Public Class cFunciones
    Public Shared Descripcion As String
    Public Shared Sub spEjecutar(ByVal _sqlcommand As SqlClient.SqlCommand, ByVal _conexion As String)
        Try
            Try

                _sqlcommand.Connection = New SqlClient.SqlConnection(_conexion)
                _sqlcommand.Connection.Open()
                _sqlcommand.ExecuteNonQuery()
            Catch ex As SqlClient.SqlException
                MsgBox(ex.Message)
            End Try

        Catch ex As Exception

        End Try
    End Sub
    Public Function BuscarDatos(ByVal strConsulta As String, ByVal Campo As String, Optional ByVal nombre As String = "Buscar...", Optional ByVal NuevaConexionStr As String = "") As String
        Dim frmBuscar As New Buscar
        Dim codigo As String
        frmBuscar.sqlstring = strConsulta
        frmBuscar.Text = nombre
        frmBuscar.campo = Campo
        frmBuscar.NuevaConexion = NuevaConexionStr
        frmBuscar.ShowDialog()
        codigo = frmBuscar.codigo
        Descripcion = frmBuscar.descrip
        Return codigo
    End Function
    Public Function Buscar_X_Descripcion_Fecha(ByVal SQLString As String, ByVal CampoFiltro As String, ByVal CampoFechaFiltro As String, Optional ByVal NombreBuscador As String = "Buscar...", Optional ByVal NuevaConexion As String = "") As String
        Dim frmBuscar As New FrmBuscador
        Dim codigo As String
        frmBuscar.SQLString = SQLString
        frmBuscar.Text = NombreBuscador
        frmBuscar.CampoFiltro = CampoFiltro
        frmBuscar.CampoFecha = CampoFechaFiltro
        frmBuscar.NuevaConexion = NuevaConexion
        frmBuscar.ShowDialog()
        If frmBuscar.Cancelado Then
            Return Nothing
        Else
            Return frmBuscar.Codigo
        End If
    End Function
    Public Function Buscar_X_Descripcion_Fecha5C(ByVal SQLString As String, ByVal CampoFiltro As String, ByVal CampoFechaFiltro As String, Optional ByVal NombreBuscador As String = "Buscar...") As String
        'BUSCADOR DISE�ADO PARA CINCO COLUMNAS
        Dim frmBuscar As New FrmBuscador5C
        Dim codigo As String
        frmBuscar.SQLString = SQLString
        frmBuscar.Text = NombreBuscador
        frmBuscar.CampoFiltro = CampoFiltro
        frmBuscar.CampoFecha = CampoFechaFiltro
        frmBuscar.ShowDialog()
        If frmBuscar.Cancelado Then
            Return Nothing
        Else
            Return frmBuscar.Codigo
        End If
    End Function

    'Esta Funci�n Calcula el saldo de la factura
    Public Function Saldo_de_Factura(ByVal FacturaNo As Long, ByVal MontoFactura As Double, ByVal TipoCambFact As Double, ByVal TipoCambRecibo As Double, Optional ByVal strModulo As String = "SeePOS") As Double
        Dim cConexion As New Conexion
        Dim sqlConexion As New SqlConnection
        sqlConexion = cConexion.Conectar(strModulo)
        Saldo_de_Factura = cConexion.SlqExecuteScalar(sqlConexion, "Select  dbo.SaldoFactura(GETDATE(), " & FacturaNo & ")") * (TipoCambFact / TipoCambRecibo)
        cConexion.DesConectar(sqlConexion)
    End Function

    Public Shared Function Saldo_de_Factura_Proveedor(ByVal FacturaNo As Double, ByVal MontoFactura As Double, ByVal TipoCambFact As Double, ByVal TipoCambRecibo As Double, ByVal Proveedor As Double, Optional ByVal strModulo As String = "") As Double
        Dim cConexion As New Conexion
        If FacturaNo = 0 Then Exit Function
        Saldo_de_Factura_Proveedor = (cConexion.SlqExecuteScalar(cConexion.Conectar(strModulo), "Select dbo.SaldoFacturaCompra(GETDATE(), " & FacturaNo & "," & Proveedor & ")") * TipoCambFact / TipoCambRecibo)
        cConexion.DesConectar(cConexion.sQlconexion)

    End Function
    Public Shared Function BuscarFacturas(ByVal CodigoCliente As Integer) As DataTable

        Dim dt As New DataTable
        '' Dentro de un Try/Catch por si se produce un error
        Try
            Llenar_Tabla_Generico("SELECT Num_Factura as Factura, Fecha, Total, Cod_Moneda from Ventas WHERE Tipo = 'CRE'  and Anulado = 0 and Cod_Cliente = " & CodigoCliente & " and (dbo.SaldoFactura(GETDATE(), Num_Factura) > 0)", dt)
        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
            Return Nothing
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            'If Not cnn Is Nothing Then
            'cnn.Close()
            'End If
        End Try
        ' Devolvemos el objeto DataTable con los datos de la consulta
        Return dt
    End Function

    Public Function ValidarPeriodo(ByVal Fecha As DateTime) As Boolean  'VALIDA SI ESTA EN EL MISMO PERIODO CONTABLE
        Dim cConexion As New Conexion                                   'O SI EL EL PERIODO ESTA CERRADO PARA PERMITIR O NO LA TRANSACCION
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader

        Try
            ValidarPeriodo = False
            'BUSCA EL MES Y EL A�O DEL PERIODO QUE SE ENCUENTRA ACTIVO
            rs = cConexion.GetRecorset(cConexion.Conectar("Contabilidad"), "SELECT Mes, Anno, Activo FROM Periodo WHERE Activo = 1")

            While rs.Read
                If rs("Activo") = False Then    'VERIFICA SI EL PERIODO ESTA CERRADO
                    If Fecha.Year = rs("Anno") Then     'VERIFICA SI ESTA EN EL MISMO A�O
                        If Fecha.Month = rs("Mes") Then 'VERIFICA SI ESTA EN EL MISMO MES
                            ValidarPeriodo = True       'EN CASO DE QUE SEA EL MISMO PERIODO
                        End If
                    End If
                End If
            End While
            rs.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
    End Function

    Public Shared Function BuscarFacturas_Proveedor(ByVal CodigoProv As Integer, Optional ByVal Conexion As String = "") As DataTable
        Dim cnn As SqlConnection = Nothing
        Dim dt As New DataTable
        ' Dentro de un Try/Catch por si se produce un error
        Try
            ' Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = IIf(Conexion = "", GetSetting("SeeSOFT", "SEEPOS", "CONEXION"), Conexion)
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String =
            "SELECT Id_Compra, Factura, Fecha, TotalFactura, Cod_MonedaCompra FROM compras WHERE (FacturaCancelado = 0) AND (TipoCompra = 'CRE') AND (CodigoProv = " & CodigoProv & ") AND (dbo.SaldoFacturaCompra(GETDATE(), Id_Compra, CodigoProv) <> 0)"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmd.Parameters.Add(New SqlParameter("@Codigo", SqlDbType.Int))
            cmd.Parameters("@Codigo").Value = CodigoProv
            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            ' Llenamos la tabla

            da.Fill(dt)

        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
            Return Nothing
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try

        Return dt ' Devolvemos el objeto DataTable con los datos de la consulta
    End Function

    Public Shared Function Cargar_Tabla_Generico(ByRef DataAdapter As SqlDataAdapter, ByVal SQLCommand As String, Optional ByVal NuevaConexionStr As String = "") As DataTable
        Dim StringConexion As String = IIf(NuevaConexionStr = "", GetSetting("SeeSOFT", "SEEPOS", "CONEXION"), NuevaConexionStr) 'JCGA 19032007
        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Dim Tabla As New DataTable
        Dim Comando As SqlCommand = New SqlCommand
        Try
            ConexionX.Open()
            Comando.CommandText = SQLCommand
            Comando.Connection = ConexionX
            Comando.CommandType = CommandType.Text
            Comando.CommandTimeout = 90
            DataAdapter.SelectCommand = Comando
            DataAdapter.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.ToString) ' Si hay error, devolvemos un valor nulo.
            Return Nothing
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
        Return Tabla ' Devolvemos el objeto DataTable con los datos de la consulta
    End Function

    Public Shared Function Llenar_Tabla_Generico(ByVal SQLCommand As String, ByRef Tabla As DataTable, Optional ByVal NuevaConexionStr As String = "")
        Dim StringConexion As String
        StringConexion = IIf(NuevaConexionStr = "", GetSetting("SeeSOFT", "SEEPOS", "CONEXION"), NuevaConexionStr)

        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Dim Comando As SqlCommand = New SqlCommand
        Try
            ConexionX.Open()
            Comando.CommandText = SQLCommand
            Comando.Connection = ConexionX
            Comando.CommandType = CommandType.Text
            Comando.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = Comando
            Tabla.Clear()
            da.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Alerta..") ' Si hay error, devolvemos un valor nulo.
            Return Nothing
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
    End Function
    Public Shared Sub Llenar_Tabla_Generico(ByVal SQLCommand As SqlCommand, ByRef Tabla As DataTable, Optional ByVal NuevaConexionStr As String = "")
        Dim StringConexion As String
        StringConexion = IIf(NuevaConexionStr = "", GetSetting("SEESOFT", "SEEPOS", "CONEXION"), NuevaConexionStr)
        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Try
            ConexionX.Open()
            SQLCommand.Connection = ConexionX
            Dim da As New SqlDataAdapter
            da.SelectCommand = SQLCommand
            Tabla.Clear()
            da.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Alerta..")
            Exit Sub
        Finally
            If Not ConexionX Is Nothing Then
                ConexionX.Close()
            End If
        End Try
    End Sub
    Public Shared Function BuscaString(ByVal strQuery As String, ByVal Conected As String, Optional ByVal strCampo As String = "") As String 'JCGA 12042007
        Dim strDatos As String = ""

        Try
            'Creo el Objeto de Conexion para realizar la consulta
            Dim myConnection As New SqlConnection(Conected)
            'Creo el Objeto Command con el que realizare la consulta
            Dim myCommand As New SqlCommand(strQuery, myConnection)

            myConnection.Open()

            'Objeto reader que utilizare en para la consulta
            Dim myReader As SqlDataReader

            'Asigno los datos de la consulta
            myReader = myCommand.ExecuteReader()
            If strCampo = "" Then

                If myReader.Read Then
                    strDatos = myReader.GetString(0)
                End If
            End If

            If strCampo <> "" Then
                If myReader.Read Then
                    strDatos = myReader(strCampo)
                End If
            End If

            myReader.Close()

            'Cierro la conexion
            myConnection.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n ...")
        End Try
        Return strDatos
    End Function
    Public Function VerificarBaseDatos(ByVal BaseDatos As String) As Boolean
        Dim Cx As New Conexion
        Dim Resultado As String
        Try
            Resultado = Cx.SlqExecuteScalar(Cx.Conectar("Seguridad"), "SELECT Name FROM MASTER.DBO.sysdatabases WHERE name = N'" & BaseDatos & "'")
            Cx.DesConectar(Cx.sQlconexion)
            If Resultado = Nothing Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception

            MessageBox.Show(ex.Message, "Error al mostrar datos",
                                                MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Function

    Public Function BuscaNumeroAsiento(ByVal InicioAsiento As String) As String
        Dim cConexion As New Conexion
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader
        Dim Numero As String
        Dim Max As String = "0"
        Dim Ceros, Length As Integer

        Try
            'BUSCA LOS NUMEROS DE ASIENTOS EXISTENTES PARA EL A�O Y MES ESTABLECIDOS
            rs = cConexion.GetRecorset(cConexion.Conectar("Contabilidad"), "SELECT NumAsiento from AsientosContables Where NumAsiento Like '" & InicioAsiento & "%'")

            While rs.Read

                Numero = rs("NumAsiento").Substring(9)  'SELECCIONA SOLO EL NUMERO DE CONSECUTIVO DEL ASIENTO SIN EL A�O Y MES
                If CInt(Max) < CInt(Numero) Then        'VERIFICA SI EL NUMERO QUE ESTA LEYENDO ES EL MAYOR
                    Max = Numero                        'DE SER MAYOR SE LO ASIGNA AL NUMERO MAX
                End If
            End While
            rs.Close()

            If Max = 0 Then
                BuscaNumeroAsiento = InicioAsiento & "0001"  'ENVIA EL SIGUIENTE NUMERO DE ASIENTO
            Else
                '-----------------------------------------------------------
                'PARA SABER LA CANTIDAD DE CEROS QUE DEBE HABER EN EL CONSECUTIVO DEL ASIENTO
                Ceros = Max.TrimStart("0").Length
                Max = CInt(Max)
                Length = Max.Length
                Max += 1
                If Max.Length <> Length Then
                    Ceros += 1
                End If
                For i As Integer = 0 To (3 - Ceros)
                    InicioAsiento = InicioAsiento & "0"
                Next
                '-----------------------------------------------------------
                BuscaNumeroAsiento = InicioAsiento & Max  'ENVIA EL SIGUIENTE NUMERO DE ASIENTO
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
    End Function

    Public Function BuscaPeriodo(ByVal Fecha As DateTime) As String
        Dim cConexion As New Conexion                   'BUSCA EL PERIODO DE LA TRANSACCI�N DE ACUERDO AL PERIODO FISCAL
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader
        Dim Inicio, Final As DateTime

        Try
			'BUSCA LOS PERIODOS FISCALES ABIERTOS
			rs = cConexion.GetRecorset(cConexion.Conectar("Contabilidad"), "SELECT dbo.DateOnlyInicio(FechaInicio) AS FechaInicio, dbo.DateOnlyFinal(FechaFinal) AS FechaFinal FROM PeriodoFiscal WHERE '" & Fecha.Date & "' BETWEEN FechaInicio AND FechaFinal")

			While rs.Read
				If Fecha >= rs("FechaInicio") Then
					If Fecha <= rs("FechaFinal") Then
						Inicio = rs("FechaInicio")
						Final = rs("FechaFinal")
						Dim meses As Integer = DateDiff(DateInterval.Month, Inicio, Final) + 1
						Fecha = CDate("01" & "/" & Fecha.Month & "/" & Fecha.Year)
						Inicio = CDate("01" & "/" & Inicio.Month & "/" & Inicio.Year)
						For i As Integer = 1 To meses
							If Inicio < Fecha Then
								Inicio = Inicio.AddMonths(1)
							Else
								BuscaPeriodo = i & "/" & Final.Year
								Exit For
							End If
						Next
					End If
				End If
			End While
			rs.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
    End Function

    Public Shared Function fnEjecutar(ByVal _sqlcommand As SqlClient.SqlCommand, ByVal _strConexion As String) As Boolean
        Try
            Try
                _sqlcommand.Connection = New SqlClient.SqlConnection(_strConexion)
                _sqlcommand.Connection.Open()
                _sqlcommand.ExecuteNonQuery()
            Catch ex As SqlClient.SqlException
                If ex.LineNumber = 65536 Or ex.LineNumber = 0 Then
                    MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
                Else
                    MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
                End If
            End Try

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
            Return False
        End Try
        Return True
    End Function
    Public Shared Sub CargarTablaSqlCommand(ByVal SQLCommand As SqlCommand, ByRef Tabla As DataTable, Optional ByVal NuevaConexionStr As String = "")
        Dim StringConexion As String
        StringConexion = IIf(NuevaConexionStr = "", GetSetting("SeeSOFT", "SEGURIDAD", "CONEXION"), NuevaConexionStr)

        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Try
            ConexionX.Open()

            SQLCommand.Connection = ConexionX
            SQLCommand.CommandType = CommandType.Text
            SQLCommand.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = SQLCommand
            Tabla.Clear()
            da.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Alerta..") ' Si hay error, devolvemos un valor nulo.
            Exit Sub
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
    End Sub
    Public Shared Sub TraerDatosDB(ByVal SQLCommand As String, ByRef Tabla As DataTable, Optional ByVal NuevaConexionStr As String = "")
        Dim StringConexion As String
        StringConexion = IIf(NuevaConexionStr = "", GetSetting("SeeSOFT", "SEGURIDAD", "CONEXION"), NuevaConexionStr)

        Dim ConexionX As SqlConnection = New SqlConnection(StringConexion)
        Dim Comando As SqlCommand = New SqlCommand
        Try
            ConexionX.Open()
            Comando.CommandText = SQLCommand
            Comando.Connection = ConexionX
            Comando.CommandType = CommandType.Text
            Comando.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = Comando
            Tabla.Clear()
            da.Fill(Tabla)
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Alerta..") ' Si hay error, devolvemos un valor nulo.
            Exit Sub
        Finally
            If Not ConexionX Is Nothing Then ' Por si se produce un error comprobamos si en realidad el objeto Connection est� iniciado de ser as�, lo cerramos.
                ConexionX.Close()
            End If
        End Try
    End Sub

End Class

