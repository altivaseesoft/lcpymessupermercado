Module Gobales

    Public Factura_Generica As New CrystalDecisions.CrystalReports.Engine.ReportDocument 'Factura_Generica
    Public FacturaPVE As New CrystalDecisions.CrystalReports.Engine.ReportDocument 'Reporte_FacturaPVEs
    Public glodtsGlobal As New dtsGlobal
    Public Sub Inicialza_reporteFactura()
        Try

            FacturaPVE.Load(GetSetting("SeeSoft", "SeePOs Report", "Reporte_FacturaPVEs"))
            Factura_Generica.Load(GetSetting("SeeSoft", "SeePOs Report", "Factura_Generica"))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Problemas cargando informes")


        End Try
    End Sub

End Module
