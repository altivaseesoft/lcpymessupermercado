Imports System.Data
Imports System.Drawing
Imports System.Windows.Forms

Public Class FrmBuscarArticuloPorSucursal
    Inherits System.Windows.Forms.Form

#Region "Variables"
    '    Dim DV As DataView
    Public Codigo As String
    Public Barras As String
    Public Cancelado As Boolean
    Dim CadenaWhere As String
    Public NuevaConexion As String
    Public Id_Bodega As Integer = 0
    Friend WithEvents colExistenciaNicoya As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents Label5 As Label
    Public Servicios As Boolean
    Friend WithEvents btnNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditar As DevExpress.XtraEditors.SimpleButton
    Public WithEvents btnKardex As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colFamilia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents lbCargando As Label
    Friend WithEvents bwCargarInventario As System.ComponentModel.BackgroundWorker
    Friend WithEvents btRefrescar As DevExpress.XtraEditors.SimpleButton


#End Region

    Dim usua As Usuario_Logeado
    Friend WithEvents panelVerde As Panel
    Public esF1 As Boolean = True


#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()
        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        usua = Usuario_Parametro

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents datos As DataSetCatalogoInventario
    Friend WithEvents TextBoxBuscar As System.Windows.Forms.TextBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents TxtCodigo As System.Windows.Forms.TextBox
    Public WithEvents ButtonAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButtonCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Filtro_Inicio_del_Campo As System.Windows.Forms.RadioButton
    Friend WithEvents Filtro_Cualquier_Parte_del_Campo As System.Windows.Forms.RadioButton
    Public WithEvents CheckBoxInHabilitados As System.Windows.Forms.CheckBox
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents RB_Medidas As System.Windows.Forms.RadioButton
    Friend WithEvents RB_Alterno As System.Windows.Forms.RadioButton
    'Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents colCodigo As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colDescripcion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colMarca As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colPrecioFinal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colExistenciaLiberia As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colUbicacion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colAlterno As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colMedidas As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents BtnImagen As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BtnEquivalencia As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TxtBarras As System.Windows.Forms.TextBox
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmBuscarArticuloPorSucursal))
        Dim ColumnFilterInfo1 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo2 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo3 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo4 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo5 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo6 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo7 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo8 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo9 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Dim ColumnFilterInfo10 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection()
        Me.datos = New LcPymes_5._2.DataSetCatalogoInventario()
        Me.TextBoxBuscar = New System.Windows.Forms.TextBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RB_Alterno = New System.Windows.Forms.RadioButton()
        Me.RB_Medidas = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Filtro_Inicio_del_Campo = New System.Windows.Forms.RadioButton()
        Me.Filtro_Cualquier_Parte_del_Campo = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ButtonAceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.ButtonCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.TxtCodigo = New System.Windows.Forms.TextBox()
        Me.CheckBoxInHabilitados = New System.Windows.Forms.CheckBox()
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.GridControl = New DevExpress.XtraGrid.GridControl()
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView()
        Me.GridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.colCodigo = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colAlterno = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colDescripcion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colMarca = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colFamilia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colExistenciaLiberia = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colExistenciaNicoya = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colPrecioFinal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colUbicacion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colMedidas = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.BtnImagen = New DevExpress.XtraEditors.SimpleButton()
        Me.BtnEquivalencia = New DevExpress.XtraEditors.SimpleButton()
        Me.TxtBarras = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnNuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEditar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnKardex = New DevExpress.XtraEditors.SimpleButton()
        Me.lbCargando = New System.Windows.Forms.Label()
        Me.bwCargarInventario = New System.ComponentModel.BackgroundWorker()
        Me.btRefrescar = New DevExpress.XtraEditors.SimpleButton()
        Me.panelVerde = New System.Windows.Forms.Panel()
        CType(Me.datos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.GridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=""ERMIS-PC"";packet size=4096;integrated security=SSPI;data source=""" &
    "ERMIS-PC\SQLMHPARTES"";persist security info=False;initial catalog=Seepos"
        Me.SqlConnection.FireInfoMessageEventOnUserErrors = False
        '
        'datos
        '
        Me.datos.DataSetName = "DataSetCatalogoInventario"
        Me.datos.Locale = New System.Globalization.CultureInfo("es-CR")
        Me.datos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBoxBuscar
        '
        Me.TextBoxBuscar.BackColor = System.Drawing.Color.Yellow
        Me.TextBoxBuscar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxBuscar.Location = New System.Drawing.Point(82, 18)
        Me.TextBoxBuscar.Name = "TextBoxBuscar"
        Me.TextBoxBuscar.Size = New System.Drawing.Size(586, 29)
        Me.TextBoxBuscar.TabIndex = 0
        '
        'RadioButton1
        '
        Me.RadioButton1.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.Black
        Me.RadioButton1.Location = New System.Drawing.Point(128, 6)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(97, 16)
        Me.RadioButton1.TabIndex = 1
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Descripci�n"
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.Black
        Me.RadioButton2.Location = New System.Drawing.Point(231, 6)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(100, 16)
        Me.RadioButton2.TabIndex = 2
        Me.RadioButton2.Text = "Ubicaci�n"
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton3
        '
        Me.RadioButton3.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.ForeColor = System.Drawing.Color.Black
        Me.RadioButton3.Location = New System.Drawing.Point(324, 6)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(78, 16)
        Me.RadioButton3.TabIndex = 3
        Me.RadioButton3.Text = "C�digo"
        Me.RadioButton3.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(-1, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Busqueda en -->"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.RB_Alterno)
        Me.Panel1.Controls.Add(Me.RB_Medidas)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.RadioButton3)
        Me.Panel1.Controls.Add(Me.RadioButton1)
        Me.Panel1.Controls.Add(Me.RadioButton2)
        Me.Panel1.Location = New System.Drawing.Point(8, 465)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(579, 31)
        Me.Panel1.TabIndex = 2
        '
        'RB_Alterno
        '
        Me.RB_Alterno.BackColor = System.Drawing.Color.Transparent
        Me.RB_Alterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RB_Alterno.ForeColor = System.Drawing.Color.Black
        Me.RB_Alterno.Location = New System.Drawing.Point(494, 6)
        Me.RB_Alterno.Name = "RB_Alterno"
        Me.RB_Alterno.Size = New System.Drawing.Size(78, 16)
        Me.RB_Alterno.TabIndex = 5
        Me.RB_Alterno.Text = "Cod. Alt."
        Me.RB_Alterno.UseVisualStyleBackColor = False
        '
        'RB_Medidas
        '
        Me.RB_Medidas.BackColor = System.Drawing.Color.Transparent
        Me.RB_Medidas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RB_Medidas.ForeColor = System.Drawing.Color.Black
        Me.RB_Medidas.Location = New System.Drawing.Point(408, 6)
        Me.RB_Medidas.Name = "RB_Medidas"
        Me.RB_Medidas.Size = New System.Drawing.Size(78, 16)
        Me.RB_Medidas.TabIndex = 4
        Me.RB_Medidas.Text = "Medidas"
        Me.RB_Medidas.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Filtro_Inicio_del_Campo)
        Me.Panel2.Controls.Add(Me.Filtro_Cualquier_Parte_del_Campo)
        Me.Panel2.Location = New System.Drawing.Point(9, 427)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(416, 24)
        Me.Panel2.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(0, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(128, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Coincidir  -->"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Filtro_Inicio_del_Campo
        '
        Me.Filtro_Inicio_del_Campo.BackColor = System.Drawing.Color.Transparent
        Me.Filtro_Inicio_del_Campo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Filtro_Inicio_del_Campo.ForeColor = System.Drawing.Color.Black
        Me.Filtro_Inicio_del_Campo.Location = New System.Drawing.Point(128, 3)
        Me.Filtro_Inicio_del_Campo.Name = "Filtro_Inicio_del_Campo"
        Me.Filtro_Inicio_del_Campo.Size = New System.Drawing.Size(131, 17)
        Me.Filtro_Inicio_del_Campo.TabIndex = 0
        Me.Filtro_Inicio_del_Campo.Text = "Inicio del Campo"
        Me.Filtro_Inicio_del_Campo.UseVisualStyleBackColor = False
        '
        'Filtro_Cualquier_Parte_del_Campo
        '
        Me.Filtro_Cualquier_Parte_del_Campo.BackColor = System.Drawing.Color.Transparent
        Me.Filtro_Cualquier_Parte_del_Campo.Checked = True
        Me.Filtro_Cualquier_Parte_del_Campo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Filtro_Cualquier_Parte_del_Campo.ForeColor = System.Drawing.Color.Black
        Me.Filtro_Cualquier_Parte_del_Campo.Location = New System.Drawing.Point(275, 4)
        Me.Filtro_Cualquier_Parte_del_Campo.Name = "Filtro_Cualquier_Parte_del_Campo"
        Me.Filtro_Cualquier_Parte_del_Campo.Size = New System.Drawing.Size(127, 17)
        Me.Filtro_Cualquier_Parte_del_Campo.TabIndex = 1
        Me.Filtro_Cualquier_Parte_del_Campo.TabStop = True
        Me.Filtro_Cualquier_Parte_del_Campo.Text = "Cualquier Parte"
        Me.Filtro_Cualquier_Parte_del_Campo.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Gray
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(603, 462)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 18)
        Me.Label3.TabIndex = 77
        Me.Label3.Text = "C�d."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonAceptar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.ButtonAceptar.Image = Global.LcPymes_5._2.My.Resources.Resources.Marcar_sincolor
        Me.ButtonAceptar.Location = New System.Drawing.Point(945, 449)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(138, 47)
        Me.ButtonAceptar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.LightSeaGreen, System.Drawing.Color.White)
        Me.ButtonAceptar.TabIndex = 4
        Me.ButtonAceptar.Text = "Aceptar"
        '
        'ButtonCancelar
        '
        Me.ButtonCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonCancelar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.ButtonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonCancelar.Image = Global.LcPymes_5._2.My.Resources.Resources.Cerrar_sincolor
        Me.ButtonCancelar.Location = New System.Drawing.Point(1089, 449)
        Me.ButtonCancelar.Name = "ButtonCancelar"
        Me.ButtonCancelar.Size = New System.Drawing.Size(132, 47)
        Me.ButtonCancelar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Tomato, System.Drawing.Color.White)
        Me.ButtonCancelar.TabIndex = 5
        Me.ButtonCancelar.Text = "Cancelar"
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.Width = 575
        '
        'TxtCodigo
        '
        Me.TxtCodigo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TxtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCodigo.ForeColor = System.Drawing.Color.Blue
        Me.TxtCodigo.Location = New System.Drawing.Point(603, 483)
        Me.TxtCodigo.Name = "TxtCodigo"
        Me.TxtCodigo.Size = New System.Drawing.Size(112, 13)
        Me.TxtCodigo.TabIndex = 3
        Me.TxtCodigo.Text = "0.00"
        Me.TxtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CheckBoxInHabilitados
        '
        Me.CheckBoxInHabilitados.Enabled = False
        Me.CheckBoxInHabilitados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxInHabilitados.ForeColor = System.Drawing.Color.Black
        Me.CheckBoxInHabilitados.Location = New System.Drawing.Point(82, 50)
        Me.CheckBoxInHabilitados.Name = "CheckBoxInHabilitados"
        Me.CheckBoxInHabilitados.Size = New System.Drawing.Size(171, 17)
        Me.CheckBoxInHabilitados.TabIndex = 78
        Me.CheckBoxInHabilitados.Text = "Ver Inhabilitados"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "CatalogoInventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Barras", "Barras"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Marca", "Marca"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo"), New System.Data.Common.DataColumnMapping("Precio_A", "Precio_A"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("PrecioFinal", "PrecioFinal"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("Ubicacion", "Ubicacion"), New System.Data.Common.DataColumnMapping("Inhabilitado", "Inhabilitado"), New System.Data.Common.DataColumnMapping("Bodega", "Bodega"), New System.Data.Common.DataColumnMapping("Alterno", "Alterno"), New System.Data.Common.DataColumnMapping("Medidas", "Medidas"), New System.Data.Common.DataColumnMapping("Servicio", "Servicio"), New System.Data.Common.DataColumnMapping("Equivalencia", "Equivalencia")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"), New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 341, "Descripcion"), New System.Data.SqlClient.SqlParameter("@Marca", System.Data.SqlDbType.VarChar, 50, "Marca"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"), New System.Data.SqlClient.SqlParameter("@Precio_A", System.Data.SqlDbType.Float, 8, "Precio_A"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@PrecioFinal", System.Data.SqlDbType.Float, 8, "PrecioFinal"), New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"), New System.Data.SqlClient.SqlParameter("@Ubicacion", System.Data.SqlDbType.VarChar, 150, "Ubicacion"), New System.Data.SqlClient.SqlParameter("@Inhabilitado", System.Data.SqlDbType.Bit, 1, "Inhabilitado"), New System.Data.SqlClient.SqlParameter("@Bodega", System.Data.SqlDbType.Float, 8, "Bodega"), New System.Data.SqlClient.SqlParameter("@Alterno", System.Data.SqlDbType.VarChar, 255, "Alterno"), New System.Data.SqlClient.SqlParameter("@Medidas", System.Data.SqlDbType.VarChar, 50, "Medidas"), New System.Data.SqlClient.SqlParameter("@Servicio", System.Data.SqlDbType.Bit, 1, "Servicio"), New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.VarChar, 300, "Equivalencia")})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = resources.GetString("SqlSelectCommand1.CommandText")
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        '
        'GridControl
        '
        Me.GridControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl.DataSource = Me.datos
        '
        '
        '
        Me.GridControl.EmbeddedNavigator.Name = ""
        Me.GridControl.Location = New System.Drawing.Point(9, 73)
        Me.GridControl.MainView = Me.AdvBandedGridView1
        Me.GridControl.Name = "GridControl"
        Me.GridControl.Size = New System.Drawing.Size(1212, 348)
        Me.GridControl.TabIndex = 80
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand2})
        Me.AdvBandedGridView1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.colCodigo, Me.colAlterno, Me.colDescripcion, Me.colMarca, Me.colFamilia, Me.colMedidas, Me.colExistenciaLiberia, Me.colExistenciaNicoya, Me.colPrecioFinal, Me.colUbicacion})
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        Me.AdvBandedGridView1.OptionsView.ShowGroupPanel = False
        '
        'GridBand2
        '
        Me.GridBand2.Columns.Add(Me.colCodigo)
        Me.GridBand2.Columns.Add(Me.colAlterno)
        Me.GridBand2.Columns.Add(Me.colDescripcion)
        Me.GridBand2.Columns.Add(Me.colMarca)
        Me.GridBand2.Columns.Add(Me.colFamilia)
        Me.GridBand2.Columns.Add(Me.colExistenciaLiberia)
        Me.GridBand2.Columns.Add(Me.colExistenciaNicoya)
        Me.GridBand2.Columns.Add(Me.colPrecioFinal)
        Me.GridBand2.Columns.Add(Me.colUbicacion)
        Me.GridBand2.Columns.Add(Me.colMedidas)
        Me.GridBand2.Name = "GridBand2"
        Me.GridBand2.Width = 1249
        '
        'colCodigo
        '
        Me.colCodigo.Caption = "C�digo"
        Me.colCodigo.FieldName = "Barras"
        Me.colCodigo.FilterInfo = ColumnFilterInfo1
        Me.colCodigo.Name = "colCodigo"
        Me.colCodigo.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCodigo.Visible = True
        Me.colCodigo.Width = 115
        '
        'colAlterno
        '
        Me.colAlterno.Caption = "Alterno"
        Me.colAlterno.FieldName = "Alterno"
        Me.colAlterno.FilterInfo = ColumnFilterInfo2
        Me.colAlterno.Name = "colAlterno"
        Me.colAlterno.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colAlterno.Visible = True
        Me.colAlterno.Width = 106
        '
        'colDescripcion
        '
        Me.colDescripcion.Caption = "Descripcion"
        Me.colDescripcion.FieldName = "Descripcion"
        Me.colDescripcion.FilterInfo = ColumnFilterInfo3
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDescripcion.SortIndex = 0
        Me.colDescripcion.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.colDescripcion.Visible = True
        Me.colDescripcion.Width = 538
        '
        'colMarca
        '
        Me.colMarca.Caption = "Equivalencia"
        Me.colMarca.FieldName = "Equivalencia"
        Me.colMarca.FilterInfo = ColumnFilterInfo4
        Me.colMarca.Name = "colMarca"
        Me.colMarca.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMarca.Visible = True
        Me.colMarca.Width = 125
        '
        'colFamilia
        '
        Me.colFamilia.Caption = "Familia"
        Me.colFamilia.FieldName = "Familia"
        Me.colFamilia.FilterInfo = ColumnFilterInfo5
        Me.colFamilia.Name = "colFamilia"
        Me.colFamilia.Visible = True
        Me.colFamilia.Width = 125
        '
        'colExistenciaLiberia
        '
        Me.colExistenciaLiberia.Caption = "Ex. Liberia"
        Me.colExistenciaLiberia.FieldName = "BodegaLiberia"
        Me.colExistenciaLiberia.FilterInfo = ColumnFilterInfo6
        Me.colExistenciaLiberia.Name = "colExistenciaLiberia"
        Me.colExistenciaLiberia.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colExistenciaLiberia.Visible = True
        Me.colExistenciaLiberia.Width = 64
        '
        'colExistenciaNicoya
        '
        Me.colExistenciaNicoya.Caption = "Ex. Nicoya"
        Me.colExistenciaNicoya.FieldName = "BodegaNicoya"
        Me.colExistenciaNicoya.FilterInfo = ColumnFilterInfo7
        Me.colExistenciaNicoya.Name = "colExistenciaNicoya"
        Me.colExistenciaNicoya.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colExistenciaNicoya.Visible = True
        Me.colExistenciaNicoya.Width = 64
        '
        'colPrecioFinal
        '
        Me.colPrecioFinal.Caption = "PrecioFinal"
        Me.colPrecioFinal.DisplayFormat.FormatString = "c2"
        Me.colPrecioFinal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colPrecioFinal.FieldName = "PrecioFinal"
        Me.colPrecioFinal.FilterInfo = ColumnFilterInfo8
        Me.colPrecioFinal.Name = "colPrecioFinal"
        Me.colPrecioFinal.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colPrecioFinal.Visible = True
        Me.colPrecioFinal.Width = 112
        '
        'colUbicacion
        '
        Me.colUbicacion.Caption = "Ubicacion"
        Me.colUbicacion.FieldName = "Ubicacion"
        Me.colUbicacion.FilterInfo = ColumnFilterInfo9
        Me.colUbicacion.Name = "colUbicacion"
        Me.colUbicacion.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colUbicacion.Width = 70
        '
        'colMedidas
        '
        Me.colMedidas.Caption = "Medidas"
        Me.colMedidas.FieldName = "Medidas"
        Me.colMedidas.FilterInfo = ColumnFilterInfo10
        Me.colMedidas.Name = "colMedidas"
        Me.colMedidas.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMedidas.Width = 62
        '
        'BtnImagen
        '
        Me.BtnImagen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnImagen.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat
        Me.BtnImagen.Image = Global.LcPymes_5._2.My.Resources.Resources.VerImagen_sincolor
        Me.BtnImagen.Location = New System.Drawing.Point(469, 424)
        Me.BtnImagen.Name = "BtnImagen"
        Me.BtnImagen.Size = New System.Drawing.Size(118, 35)
        Me.BtnImagen.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer)), System.Drawing.Color.White)
        Me.BtnImagen.TabIndex = 81
        Me.BtnImagen.Text = "Ver Imagen"
        '
        'BtnEquivalencia
        '
        Me.BtnEquivalencia.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnEquivalencia.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.BtnEquivalencia.Image = Global.LcPymes_5._2.My.Resources.Resources.Equivalencia_sincolor
        Me.BtnEquivalencia.Location = New System.Drawing.Point(603, 424)
        Me.BtnEquivalencia.Name = "BtnEquivalencia"
        Me.BtnEquivalencia.Size = New System.Drawing.Size(122, 35)
        Me.BtnEquivalencia.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer)), System.Drawing.Color.Black)
        Me.BtnEquivalencia.TabIndex = 82
        Me.BtnEquivalencia.Text = "Equivalencia"
        '
        'TxtBarras
        '
        Me.TxtBarras.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TxtBarras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtBarras.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBarras.ForeColor = System.Drawing.Color.Black
        Me.TxtBarras.Location = New System.Drawing.Point(603, 476)
        Me.TxtBarras.Name = "TxtBarras"
        Me.TxtBarras.Size = New System.Drawing.Size(122, 20)
        Me.TxtBarras.TabIndex = 83
        Me.TxtBarras.Text = "0.00"
        Me.TxtBarras.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(5, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 20)
        Me.Label5.TabIndex = 85
        Me.Label5.Text = "Buscar:"
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.btnNuevo.Image = Global.LcPymes_5._2.My.Resources.Resources.Nuevo_sincolor
        Me.btnNuevo.Location = New System.Drawing.Point(871, 15)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(110, 38)
        Me.btnNuevo.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(83, Byte), Integer)), System.Drawing.Color.White)
        Me.btnNuevo.TabIndex = 86
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.ToolTip = "Crear nuevo art�culo"
        '
        'btnEditar
        '
        Me.btnEditar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.btnEditar.Image = Global.LcPymes_5._2.My.Resources.Resources.Editar_sincolor
        Me.btnEditar.Location = New System.Drawing.Point(987, 15)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnEditar.Size = New System.Drawing.Size(110, 38)
        Me.btnEditar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Gray, System.Drawing.Color.White)
        Me.btnEditar.TabIndex = 87
        Me.btnEditar.Text = "Editar"
        Me.btnEditar.ToolTip = "Editar el art�culo"
        '
        'btnKardex
        '
        Me.btnKardex.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnKardex.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.btnKardex.Image = Global.LcPymes_5._2.My.Resources.Resources.Kardex_sincolor
        Me.btnKardex.Location = New System.Drawing.Point(1103, 15)
        Me.btnKardex.Name = "btnKardex"
        Me.btnKardex.Size = New System.Drawing.Size(118, 38)
        Me.btnKardex.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Gray, System.Drawing.Color.White)
        Me.btnKardex.TabIndex = 88
        Me.btnKardex.Text = "Kardex"
        Me.btnKardex.ToolTip = "Movimientos del art�culo"
        Me.btnKardex.Visible = False
        '
        'lbCargando
        '
        Me.lbCargando.AutoSize = True
        Me.lbCargando.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCargando.ForeColor = System.Drawing.Color.Black
        Me.lbCargando.Location = New System.Drawing.Point(534, 50)
        Me.lbCargando.Name = "lbCargando"
        Me.lbCargando.Size = New System.Drawing.Size(134, 13)
        Me.lbCargando.TabIndex = 89
        Me.lbCargando.Text = "Cargando Inventario..."
        Me.lbCargando.Visible = False
        '
        'bwCargarInventario
        '
        '
        'btRefrescar
        '
        Me.btRefrescar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btRefrescar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btRefrescar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.btRefrescar.Image = Global.LcPymes_5._2.My.Resources.Resources.Actualizar_sincolor
        Me.btRefrescar.Location = New System.Drawing.Point(731, 15)
        Me.btRefrescar.Name = "btRefrescar"
        Me.btRefrescar.Size = New System.Drawing.Size(134, 38)
        Me.btRefrescar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(157, Byte), Integer)), System.Drawing.Color.White)
        Me.btRefrescar.TabIndex = 90
        Me.btRefrescar.Text = "Refrescar"
        Me.btRefrescar.ToolTip = "Crear nuevo art�culo"
        '
        'panelVerde
        '
        Me.panelVerde.BackColor = System.Drawing.Color.FromArgb(CType(CType(84, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.panelVerde.Location = New System.Drawing.Point(-1, -1)
        Me.panelVerde.Name = "panelVerde"
        Me.panelVerde.Size = New System.Drawing.Size(1227, 10)
        Me.panelVerde.TabIndex = 91
        '
        'FrmBuscarArticuloPorSucursal
        '
        Me.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CancelButton = Me.ButtonCancelar
        Me.ClientSize = New System.Drawing.Size(1227, 498)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.panelVerde)
        Me.Controls.Add(Me.btRefrescar)
        Me.Controls.Add(Me.lbCargando)
        Me.Controls.Add(Me.btnKardex)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TxtBarras)
        Me.Controls.Add(Me.TxtCodigo)
        Me.Controls.Add(Me.TextBoxBuscar)
        Me.Controls.Add(Me.BtnEquivalencia)
        Me.Controls.Add(Me.BtnImagen)
        Me.Controls.Add(Me.CheckBoxInHabilitados)
        Me.Controls.Add(Me.ButtonCancelar)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GridControl)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmBuscarArticuloPorSucursal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Buscar Art�culo"
        CType(Me.datos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.GridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region "Load"
    Private Sub FrmBuscarArticulo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.SqlConnection.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)

            Me.datos.CatalogoInventarioPorSucursal.DefaultView.AllowDelete = False
            Me.datos.CatalogoInventarioPorSucursal.DefaultView.AllowEdit = False
            Me.datos.CatalogoInventarioPorSucursal.DefaultView.AllowNew = False
            Me.GridControl.DataSource = Me.datos.CatalogoInventarioPorSucursal.DefaultView

            Me.TxtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.datos.CatalogoInventarioPorSucursal.DefaultView, "Barras"))
            Me.TxtBarras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.datos.CatalogoInventarioPorSucursal.DefaultView, "codigo"))
            If Me.BindingContext(Me.datos.CatalogoInventarioPorSucursal.DefaultView).Count Then
                Me.BindingContext(Me.datos.CatalogoInventarioPorSucursal.DefaultView).Position = 0
            End If

            If glodtsGlobal.configuraciones(0).UtilizaSucursales = "NO" Then
                colExistenciaNicoya.Visible = False
                colExistenciaLiberia.Caption = "Existencia"
            End If

            TextBoxBuscar.Enabled = True
            TextBoxBuscar.Focus()
            ButtonAceptar.Visible = esF1
            btnEditar.Enabled = False
            btnKardex.Enabled = False
            spRegrescarListado()
            If ButtonAceptar.Visible = False Then
                ButtonCancelar.Text = "Cerrar"
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub
#End Region

    Dim dts As New DataSetCatalogoInventario
    Private Sub spCargarDataSet()
        Try
            clsCargarCatalogo.spCargarDataSet(Id_Bodega)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)

        End Try

    End Sub

#Region "Funciones"
    Private Sub BuscarDatos(ByVal Descripcion As String, ByVal CampoFiltro As String)

        Dim filtroDefault As String = ""
        If Me.Filtro_Inicio_del_Campo.Checked = True Then

            If CampoFiltro.Contains("Descripcion") Or CampoFiltro.Contains("Barras") Then
                filtroDefault = "(Descripcion lIKE '" & Descripcion & "%' or Barras lIKE '" & Descripcion & "%')"
            Else
                filtroDefault = CampoFiltro & " lIKE '" & Descripcion & "%'"
            End If
            CadenaWhere = " WHERE " & filtroDefault & IIf(Me.CheckBoxInHabilitados.Checked = True, " AND IdBodega = " & Id_Bodega & " AND Inhabilitado = 1", " AND IdBodega = " & Id_Bodega & " AND Inhabilitado = 0") & IIf(Servicios = True, " and Servicio = 1", "")

        ElseIf Me.Filtro_Cualquier_Parte_del_Campo.Checked = True Then

            If CampoFiltro.Contains("Descripcion") Or CampoFiltro.Contains("Barras") Then
                filtroDefault = "(Descripcion lIKE '%" & Descripcion & "%' or Barras lIKE '%" & Descripcion & "%')"
            Else
                filtroDefault = CampoFiltro & " lIKE '%" & Descripcion & "%'"
            End If

            CadenaWhere = " WHERE " & filtroDefault & IIf(Me.CheckBoxInHabilitados.Checked = True, " AND IdBodega = " & Id_Bodega & " AND Inhabilitado = 1", " AND IdBodega = " & Id_Bodega & " AND Inhabilitado = 0") & IIf(Servicios = True, " and Servicio = 1", "")
        Else
            CadenaWhere = "" & IIf(Me.CheckBoxInHabilitados.Checked = True, " Inhabilitado = 1", " Inhabilitado = 0") & IIf(Servicios = True, " and Servicio = 1", "")
        End If
        Try
            Me.datos.Clear()

            If Id_Bodega <> 0 Then
                Me.SqlDataAdapter.SelectCommand.CommandText = "Select [dbo].[CatalogoInventarioPorSucursal].* FROM [dbo].[CatalogoInventarioPorSucursal] inner join dbo.ArticulosXBodega as AB " &
                                                              " on [dbo].[CatalogoInventarioPorSucursal].Codigo = AB.COdigo " & CadenaWhere.Replace("IdBodega", "AB.IdBodega") & " order by descripcion "

            Else
                Me.SqlDataAdapter.SelectCommand.CommandText = "SELECT * FROM CatalogoInventarioPorSucursal " & CadenaWhere & " order by descripcion "

            End If

            Me.SqlDataAdapter.Fill(Me.datos, "CatalogoInventarioPorSucursal")
            Me.GridControl.DataSource = Me.datos.CatalogoInventarioPorSucursal.DefaultView
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub spBuscarDatos(ByVal Descripcion As String, ByVal CampoFiltro As String)

        Dim filtroDefault As String = ""
        If Me.Filtro_Inicio_del_Campo.Checked = True Then

            If CampoFiltro.Contains("Descripcion") Then
                filtroDefault = fnCrearLike("(Descripcion lIKE '@Parametro%' or Barras lIKE '@Parametro%')", Descripcion)
            Else
                filtroDefault = fnCrearLike(CampoFiltro & " lIKE '@Parametro%'", Descripcion)
            End If
            If filtroDefault.Equals("") Then
                CadenaWhere = IIf(Me.CheckBoxInHabilitados.Checked = True, " IdBodega = " & Id_Bodega & " AND Inhabilitado = 1", " IdBodega = " & Id_Bodega & " AND Inhabilitado = 0") & IIf(Servicios = True, " and Servicio = 1", "")
            Else
                CadenaWhere = filtroDefault & IIf(Me.CheckBoxInHabilitados.Checked = True, " AND IdBodega = " & Id_Bodega & " AND Inhabilitado = 1", " AND IdBodega = " & Id_Bodega & " AND Inhabilitado = 0") & IIf(Servicios = True, " and Servicio = 1", "")
            End If


        ElseIf Me.Filtro_Cualquier_Parte_del_Campo.Checked = True Then

            If CampoFiltro.Contains("Descripcion") Then
                filtroDefault = fnCrearLike("(Descripcion lIKE '%@Parametro%' or Barras lIKE '%@Parametro%')", Descripcion)
            Else
                filtroDefault = fnCrearLike(CampoFiltro & " lIKE '%@Parametro%'", Descripcion)
            End If
            If filtroDefault.Equals("") Then
                CadenaWhere = IIf(Me.CheckBoxInHabilitados.Checked = True, " IdBodega = " & Id_Bodega & " AND Inhabilitado = 1", " IdBodega = " & Id_Bodega & " AND Inhabilitado = 0") & IIf(Servicios = True, " and Servicio = 1", "")
            Else
                CadenaWhere = filtroDefault & IIf(Me.CheckBoxInHabilitados.Checked = True, " AND IdBodega = " & Id_Bodega & " AND Inhabilitado = 1", " AND IdBodega = " & Id_Bodega & " AND Inhabilitado = 0") & IIf(Servicios = True, " and Servicio = 1", "")
            End If

        Else
            CadenaWhere = "" & IIf(Me.CheckBoxInHabilitados.Checked = True, " Inhabilitado = 1", " Inhabilitado = 0") & IIf(Servicios = True, " and Servicio = 1", "")
        End If
        Try
            Me.datos.Clear()
            Dim row As DataRow()
            row = dts.CatalogoInventarioPorSucursal.Select(CadenaWhere.Replace("%", "*"))
            If row.Length > 0 Then
                datos.CatalogoInventarioPorSucursal.Merge(row.CopyToDataTable())
            End If

            Me.GridControl.DataSource = Me.datos.CatalogoInventarioPorSucursal.DefaultView
        Catch ex As SystemException
            MsgBox(ex.Message _
                   , MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Private Function fnCrearLike(ByVal sql As String, ByVal cadena As String) As String
        Try
            Dim arreglo As Array
            Dim retorno As String = ""

            arreglo = cadena.Split("%")
            For i As Integer = 0 To arreglo.Length - 1

                If arreglo(i).ToString() <> "" Then
                    If retorno = "" Then
                        retorno += sql.Replace("@Parametro", arreglo(i).ToString())
                    Else
                        retorno += " and " & sql.Replace("@Parametro", arreglo(i).ToString())
                    End If

                End If
            Next

            Return retorno
        Catch ex As Exception

        End Try
    End Function




    Sub SeleccionarCodigo()
        Dim hi As DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo = AdvBandedGridView1.CalcHitInfo(CType(GridControl, Control).PointToClient(Control.MousePosition))
        If hi.RowHandle >= 0 Then 'BuscarById(AdvBandedGridView1.GetDataRow(hi.RowHandle))
            TxtCodigo.Text = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle).ItemArray(0)
            TxtBarras.Text = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle).ItemArray(11)
        ElseIf AdvBandedGridView1.FocusedRowHandle >= 0 Then
            TxtCodigo.Text = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle).ItemArray(0)
            TxtBarras.Text = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle).ItemArray(11)


        Else
            TxtCodigo.Text = 0
            TxtBarras.Text = 0
        End If
    End Sub
#End Region

#Region "Funciones Controles"
    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        spAceptar()
    End Sub
    Sub spAceptar()
        Cancelado = False
        SeleccionarCodigo()
        Codigo = TxtCodigo.Text
        Barras = TxtBarras.Text
        Close()
    End Sub

    Private Sub SimpleButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelar.Click
        Cancelado = True
        Codigo = 0
        Close()
    End Sub

    Private Sub TextBoxBuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxBuscar.TextChanged
        If TextBoxBuscar.Text.Length >= 2 Then

            spRegrescarListado()

        End If
    End Sub



    Private Sub TextBoxBuscar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBuscar.KeyPress
        If Asc(e.KeyChar) = Keys.Enter Then
            GridControl.Focus()
        End If
    End Sub
    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        spAplicarFiltro()
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        spAplicarFiltro()
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        spAplicarFiltro()
    End Sub

    Private Sub RB_Medidas_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Medidas.CheckedChanged
        spAplicarFiltro()
    End Sub

    Private Sub RB_Alterno_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Alterno.CheckedChanged
        spAplicarFiltro()
    End Sub

    Private Sub Filtro_Inicio_del_Campo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Filtro_Inicio_del_Campo.CheckedChanged, Filtro_Cualquier_Parte_del_Campo.CheckedChanged
        spAplicarFiltro()
    End Sub

    Private Sub CheckBoxInHabilitados_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxInHabilitados.CheckedChanged
        spAplicarFiltro()
    End Sub
#End Region

    Private Sub BtnImagen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnImagen.Click
        Try
            Dim frm As New frmImagen
            frm.codigo = TxtCodigo.Text
            frm.ShowDialog()
        Catch ex As Exception
        End Try
    End Sub


    Private Sub BtnEquivalencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEquivalencia.Click
        Try
            Dim frm As New FrmEquivalencias
            frm.codigo = TxtCodigo.Text
            If frm.ShowDialog() = DialogResult.OK Then
                RadioButton3.Checked = True
                TextBoxBuscar.Text = frm.CodArticulo
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Private Sub GridControl_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridControl.KeyPress
        If Asc(e.KeyChar) = Keys.Enter Then
            If ButtonAceptar.Visible Then
                SeleccionarCodigo()
                Cancelado = False
                Codigo = TxtCodigo.Text
                Barras = TxtBarras.Text
                Close()
            Else
                editar()
            End If

        End If
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Try
            Dim frm As New FrmInventario(usua)
            frm.Show()
            frm.NuevosDatos()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        editar()
    End Sub

    Sub editar()
        Try
            Dim frm As New FrmInventario(usua)
            frm.Show()
            frm.spBuscarRegistros(CDbl(TxtBarras.Text))
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnKardex_Click(sender As Object, e As EventArgs) Handles btnKardex.Click
        abrirKardex()
    End Sub
    Private Sub abrirKardex()
        Dim IdInventario As Integer
        IdInventario = IIf(Integer.TryParse(TxtBarras.Text, IdInventario), TxtBarras.Text, 0)
        Dim frm As Form = LcPymesKardex.Navegador.formularioInventario(IdInventario, True)
        If frm.ShowDialog = DialogResult.OK Then

        End If
    End Sub

    Private Sub GridControl_Click(ByVal sender As Object, ByVal e As DevExpress.Utils.DXMouseEventArgs) Handles GridControl.Click
        If AdvBandedGridView1.SelectedRowsCount > 0 Then
            btnEditar.Enabled = True
            btnKardex.Enabled = True
        Else
            btnEditar.Enabled = False
            btnKardex.Enabled = False
        End If
    End Sub

    Private Sub bwCargarInventario_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwCargarInventario.DoWork
        spCargarDataSet()
    End Sub
    Sub spAplicarFiltro()
        If Me.RadioButton1.Checked = True Then
            spBuscarDatos(Me.TextBoxBuscar.Text, "Descripcion")
        ElseIf Me.RadioButton2.Checked = True Then
            spBuscarDatos(Me.TextBoxBuscar.Text, "Ubicacion")
        ElseIf Me.RB_Medidas.Checked = True Then
            spBuscarDatos(Me.TextBoxBuscar.Text, "Medidas")
        ElseIf Me.RB_Alterno.Checked = True Then
            spBuscarDatos(Me.TextBoxBuscar.Text, "Alterno")
        Else
            spBuscarDatos(Me.TextBoxBuscar.Text, "Barras")
        End If
    End Sub

    Private Sub bwCargarInventario_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwCargarInventario.RunWorkerCompleted
        lbCargando.Visible = False
        dts = clsCargarCatalogo.listadoInventario.Copy()
        spAplicarFiltro()
        TextBoxBuscar.Enabled = True
        TextBoxBuscar.Focus()
        btRefrescar.Enabled = True
        If AdvBandedGridView1.RowCount <= 0 Then
            btnEditar.Enabled = False
            btnKardex.Enabled = False
        End If
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub btRefrescar_Click(sender As Object, e As EventArgs) Handles btRefrescar.Click
        spRegrescarListado()
    End Sub
    Sub spRegrescarListado()
        If bwCargarInventario.IsBusy = False Then
            lbCargando.Visible = True
            btRefrescar.Enabled = False
            bwCargarInventario.RunWorkerAsync()

        End If

    End Sub

    Private Sub TextBoxBuscar_Enter(sender As Object, e As EventArgs) Handles TextBoxBuscar.Enter
        Dim tb As TextBox = DirectCast(sender, TextBox)
        tb.BackColor = Color.FromArgb(251, 227, 55)
        DibujarBorde(tb, Color.White)
    End Sub

    Private Sub DibujarBorde(tb As TextBox, clr As Color)

        If (tb Is Nothing) Then Return

        Dim d As Integer = 1

        Dim pt As New Point(tb.Location.X - d, tb.Location.Y - d)
        Dim sz As New Size(tb.Width + (2 * d), tb.Height + (2 * d))

        Using g As Graphics = Me.CreateGraphics()
            Dim rect As New Rectangle(pt, sz)
            Dim p As New Pen(clr, 2)
            g.DrawRectangle(p, rect)
            p.Dispose()
        End Using

    End Sub

    Private Sub TextBoxBuscar_Leave(sender As Object, e As EventArgs) Handles TextBoxBuscar.Leave
        Dim tb As TextBox = DirectCast(sender, TextBox)
        tb.BackColor = Color.White
        DibujarBorde(tb, Me.BackColor)
    End Sub



    Private Sub GridControl_DoubleClick(sender As Object, e As EventArgs) Handles GridControl.DoubleClick
        AbrirLinea()
    End Sub
    Sub AbrirLinea()
        If ButtonAceptar.Visible Then
            spAceptar()
        Else
            editar()
        End If

    End Sub
End Class