Imports System.Data
Imports System.Data.SqlClient

Public Class GestioDatos
    Private conexion As New SqlClient.SqlConnection(GetSetting("Seesoft", "SeePOS", "Conexion"))
    Private adaptador As SqlClient.SqlDataAdapter
    Public Function Ejecuta(ByVal sql As String) As DataTable
        Try
            Dim dts As New DataTable
            conexion.Open()
            adaptador = New SqlClient.SqlDataAdapter(sql, conexion)
            adaptador.SelectCommand.CommandType = CommandType.Text
            adaptador.Fill(dts)
            conexion.Close()
            Return dts
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function
End Class

Public Class CifraDecifra
    Public Shared PalabraBase As String = "FLORECITAS"
    Public Shared Function Cifra(ByVal _val As String) As String
        Dim RESULTADO As String = ""
        For Each FILA As Char In _val
            If IsNumeric(FILA) Then
                RESULTADO += PalabraBase.Substring(CInt(FILA.ToString), 1)
            Else
                RESULTADO += FILA
            End If
        Next
        Return RESULTADO
    End Function
    Public Shared Function Decifra(ByVal _val As String) As String
        Dim RESULTADO As String = "" : Dim ENCONTRADO As Boolean = False
        For Each FILA As Char In _val
            ENCONTRADO = False
            For i As Integer = 0 To PalabraBase.Length - 1
                If FILA = PalabraBase.Substring(i, 1) Then
                    RESULTADO += i.ToString : ENCONTRADO = True : Exit For
                End If
            Next
            If ENCONTRADO = False Then RESULTADO += FILA
        Next
        Return RESULTADO
    End Function

End Class
