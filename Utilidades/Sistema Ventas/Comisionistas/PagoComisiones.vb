Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing.Printing

Public Class PagoComisiones
    Inherits FrmPlantilla

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        usua = Usuario_Parametro

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtCedulaUsuario As System.Windows.Forms.TextBox
    Friend WithEvents AdapterPagoComision As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents DsPagoComision1 As LcPymes_5._2.DsPagoComision
    Friend WithEvents AdapterUsuarios As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtNumPago As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LFecha As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lbAnulado As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtCod_Art As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GridFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupBox_Datos_Comisionista As System.Windows.Forms.GroupBox
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AdapterFacturas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colComision As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents B_Desmarcar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents B_Marca As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colPagar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtCedula As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(PagoComisiones))
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.Label48 = New System.Windows.Forms.Label
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.txtCedulaUsuario = New System.Windows.Forms.TextBox
        Me.DsPagoComision1 = New LcPymes_5._2.DsPagoComision
        Me.AdapterPagoComision = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.AdapterUsuarios = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.txtNumPago = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.LFecha = New System.Windows.Forms.Label
        Me.GroupBox_Datos_Comisionista = New System.Windows.Forms.GroupBox
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtCedula = New DevExpress.XtraEditors.TextEdit
        Me.lbAnulado = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtCod_Art = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.GridFacturas = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colComision = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colPagar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtTotal = New DevExpress.XtraEditors.TextEdit
        Me.AdapterFacturas = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.B_Desmarcar = New DevExpress.XtraEditors.SimpleButton
        Me.B_Marca = New DevExpress.XtraEditors.SimpleButton
        CType(Me.DsPagoComision1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_Datos_Comisionista.SuspendLayout()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCedula.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtCod_Art.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(664, 330)
        Me.DataNavigator.Name = "DataNavigator"
        Me.DataNavigator.Visible = False
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(650, 32)
        Me.TituloModulo.Text = "Pago de Comisiones"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.Text = "Anular"
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 300)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(650, 52)
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'txtUsuario
        '
        Me.txtUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(552, 336)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(96, 13)
        Me.txtUsuario.TabIndex = 0
        Me.txtUsuario.Text = ""
        '
        'Label48
        '
        Me.Label48.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label48.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.White
        Me.Label48.Location = New System.Drawing.Point(488, 336)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(60, 13)
        Me.Label48.TabIndex = 204
        Me.Label48.Text = "Usuario->"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(488, 320)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(160, 13)
        Me.txtNombreUsuario.TabIndex = 205
        Me.txtNombreUsuario.Text = ""
        '
        'txtCedulaUsuario
        '
        Me.txtCedulaUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCedulaUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.txtCedulaUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCedulaUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsPagoComision1, "PagoComision.Usuario"))
        Me.txtCedulaUsuario.Enabled = False
        Me.txtCedulaUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtCedulaUsuario.Location = New System.Drawing.Point(672, 304)
        Me.txtCedulaUsuario.Name = "txtCedulaUsuario"
        Me.txtCedulaUsuario.ReadOnly = True
        Me.txtCedulaUsuario.Size = New System.Drawing.Size(48, 13)
        Me.txtCedulaUsuario.TabIndex = 206
        Me.txtCedulaUsuario.Text = ""
        Me.txtCedulaUsuario.Visible = False
        '
        'DsPagoComision1
        '
        Me.DsPagoComision1.DataSetName = "DsPagoComision"
        Me.DsPagoComision1.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'AdapterPagoComision
        '
        Me.AdapterPagoComision.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterPagoComision.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterPagoComision.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterPagoComision.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "PagoComision", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumPago", "NumPago"), New System.Data.Common.DataColumnMapping("Nombre_Comisionista", "Nombre_Comisionista"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Cod_Comisionista", "Cod_Comisionista")})})
        Me.AdapterPagoComision.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM PagoComision WHERE (NumPago = @Original_NumPago) AND (Anulado = @Orig" & _
        "inal_Anulado) AND (Cod_Comisionista = @Original_Cod_Comisionista) AND (Fecha = @" & _
        "Original_Fecha) AND (Nombre_Comisionista = @Original_Nombre_Comisionista) AND (T" & _
        "otal = @Original_Total) AND (Usuario = @Original_Usuario)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumPago", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumPago", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Comisionista", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Comisionista", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Comisionista", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Comisionista", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO PagoComision(Nombre_Comisionista, Total, Fecha, Usuario, Anulado, Cod" & _
        "_Comisionista) VALUES (@Nombre_Comisionista, @Total, @Fecha, @Usuario, @Anulado," & _
        " @Cod_Comisionista); SELECT NumPago, Nombre_Comisionista, Total, Fecha, Usuario," & _
        " Anulado, Cod_Comisionista FROM PagoComision WHERE (NumPago = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Comisionista", System.Data.SqlDbType.VarChar, 150, "Nombre_Comisionista"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 50, "Usuario"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Comisionista", System.Data.SqlDbType.BigInt, 8, "Cod_Comisionista"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT NumPago, Nombre_Comisionista, Total, Fecha, Usuario, Anulado, Cod_Comision" & _
        "ista FROM PagoComision"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE PagoComision SET Nombre_Comisionista = @Nombre_Comisionista, Total = @Tota" & _
        "l, Fecha = @Fecha, Usuario = @Usuario, Anulado = @Anulado, Cod_Comisionista = @C" & _
        "od_Comisionista WHERE (NumPago = @Original_NumPago) AND (Anulado = @Original_Anu" & _
        "lado) AND (Cod_Comisionista = @Original_Cod_Comisionista) AND (Fecha = @Original" & _
        "_Fecha) AND (Nombre_Comisionista = @Original_Nombre_Comisionista) AND (Total = @" & _
        "Original_Total) AND (Usuario = @Original_Usuario); SELECT NumPago, Nombre_Comisi" & _
        "onista, Total, Fecha, Usuario, Anulado, Cod_Comisionista FROM PagoComision WHERE" & _
        " (NumPago = @NumPago)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Comisionista", System.Data.SqlDbType.VarChar, 150, "Nombre_Comisionista"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 50, "Usuario"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Comisionista", System.Data.SqlDbType.BigInt, 8, "Cod_Comisionista"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumPago", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumPago", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Comisionista", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Comisionista", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Comisionista", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Comisionista", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumPago", System.Data.SqlDbType.BigInt, 8, "NumPago"))
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM PagoComision WHERE (NumPago = @Original_NumPago) AND (Anulado = @Orig" & _
        "inal_Anulado) AND (Ced_Comisionista = @Original_Ced_Comisionista) AND (Fecha = @" & _
        "Original_Fecha) AND (Nombre_Comisionista = @Original_Nombre_Comisionista) AND (T" & _
        "otal = @Original_Total) AND (Usuario = @Original_Usuario)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumPago", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumPago", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ced_Comisionista", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ced_Comisionista", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Comisionista", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Comisionista", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO PagoComision(Ced_Comisionista, Nombre_Comisionista, Total, Fecha, Usu" & _
        "ario, Anulado) VALUES (@Ced_Comisionista, @Nombre_Comisionista, @Total, @Fecha, " & _
        "@Usuario, @Anulado); SELECT NumPago, Ced_Comisionista, Nombre_Comisionista, Tota" & _
        "l, Fecha, Usuario, Anulado FROM PagoComision WHERE (NumPago = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ced_Comisionista", System.Data.SqlDbType.VarChar, 15, "Ced_Comisionista"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Comisionista", System.Data.SqlDbType.VarChar, 150, "Nombre_Comisionista"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 50, "Usuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT NumPago, Ced_Comisionista, Nombre_Comisionista, Total, Fecha, Usuario, Anu" & _
        "lado FROM PagoComision"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE PagoComision SET Ced_Comisionista = @Ced_Comisionista, Nombre_Comisionista" & _
        " = @Nombre_Comisionista, Total = @Total, Fecha = @Fecha, Usuario = @Usuario, Anu" & _
        "lado = @Anulado WHERE (NumPago = @Original_NumPago) AND (Anulado = @Original_Anu" & _
        "lado) AND (Ced_Comisionista = @Original_Ced_Comisionista) AND (Fecha = @Original" & _
        "_Fecha) AND (Nombre_Comisionista = @Original_Nombre_Comisionista) AND (Total = @" & _
        "Original_Total) AND (Usuario = @Original_Usuario); SELECT NumPago, Ced_Comisioni" & _
        "sta, Nombre_Comisionista, Total, Fecha, Usuario, Anulado FROM PagoComision WHERE" & _
        " (NumPago = @NumPago)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ced_Comisionista", System.Data.SqlDbType.VarChar, 15, "Ced_Comisionista"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Comisionista", System.Data.SqlDbType.VarChar, 150, "Nombre_Comisionista"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 50, "Usuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumPago", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumPago", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ced_Comisionista", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ced_Comisionista", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Comisionista", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Comisionista", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumPago", System.Data.SqlDbType.BigInt, 8, "NumPago"))
        '
        'AdapterUsuarios
        '
        Me.AdapterUsuarios.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterUsuarios.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Id_Usuario", "Id_Usuario"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Clave_Entrada", "Clave_Entrada"), New System.Data.Common.DataColumnMapping("Clave_Interna", "Clave_Interna")})})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT Cedula, Id_Usuario, Nombre, Clave_Entrada, Clave_Interna FROM Usuarios"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'txtNumPago
        '
        Me.txtNumPago.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNumPago.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsPagoComision1, "PagoComision.NumPago"))
        Me.txtNumPago.Enabled = False
        Me.txtNumPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumPago.Location = New System.Drawing.Point(104, 16)
        Me.txtNumPago.Name = "txtNumPago"
        Me.txtNumPago.Size = New System.Drawing.Size(72, 13)
        Me.txtNumPago.TabIndex = 208
        Me.txtNumPago.Text = ""
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 12)
        Me.Label1.TabIndex = 207
        Me.Label1.Text = "Pago N�"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LFecha
        '
        Me.LFecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LFecha.BackColor = System.Drawing.Color.DarkBlue
        Me.LFecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsPagoComision1, "PagoComision.Fecha"))
        Me.LFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LFecha.ForeColor = System.Drawing.Color.White
        Me.LFecha.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.LFecha.Location = New System.Drawing.Point(496, 8)
        Me.LFecha.Name = "LFecha"
        Me.LFecha.Size = New System.Drawing.Size(152, 16)
        Me.LFecha.TabIndex = 209
        Me.LFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox_Datos_Comisionista
        '
        Me.GroupBox_Datos_Comisionista.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox_Datos_Comisionista.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox_Datos_Comisionista.Controls.Add(Me.txtNombre)
        Me.GroupBox_Datos_Comisionista.Controls.Add(Me.Label37)
        Me.GroupBox_Datos_Comisionista.Controls.Add(Me.Label5)
        Me.GroupBox_Datos_Comisionista.Controls.Add(Me.Label20)
        Me.GroupBox_Datos_Comisionista.Controls.Add(Me.txtCedula)
        Me.GroupBox_Datos_Comisionista.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox_Datos_Comisionista.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox_Datos_Comisionista.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox_Datos_Comisionista.Name = "GroupBox_Datos_Comisionista"
        Me.GroupBox_Datos_Comisionista.Size = New System.Drawing.Size(632, 64)
        Me.GroupBox_Datos_Comisionista.TabIndex = 210
        Me.GroupBox_Datos_Comisionista.TabStop = False
        '
        'txtNombre
        '
        Me.txtNombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNombre.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsPagoComision1, "PagoComision.Nombre_Comisionista"))
        Me.txtNombre.EditValue = ""
        Me.txtNombre.Location = New System.Drawing.Point(128, 40)
        Me.txtNombre.Name = "txtNombre"
        '
        'txtNombre.Properties
        '
        Me.txtNombre.Properties.ReadOnly = True
        Me.txtNombre.Size = New System.Drawing.Size(496, 19)
        Me.txtNombre.TabIndex = 197
        '
        'Label37
        '
        Me.Label37.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label37.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(24, 0)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(568, 16)
        Me.Label37.TabIndex = 157
        Me.Label37.Text = "Datos del Comisionista"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(128, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(496, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Nombre"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label20.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(8, 24)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(112, 16)
        Me.Label20.TabIndex = 158
        Me.Label20.Text = "Codigo"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCedula
        '
        Me.txtCedula.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsPagoComision1, "PagoComision.Cod_Comisionista"))
        Me.txtCedula.EditValue = ""
        Me.txtCedula.Location = New System.Drawing.Point(8, 40)
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.Size = New System.Drawing.Size(112, 19)
        Me.txtCedula.TabIndex = 0
        '
        'lbAnulado
        '
        Me.lbAnulado.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lbAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbAnulado.ForeColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(0, Byte), CType(0, Byte))
        Me.lbAnulado.Location = New System.Drawing.Point(224, 72)
        Me.lbAnulado.Name = "lbAnulado"
        Me.lbAnulado.Size = New System.Drawing.Size(160, 48)
        Me.lbAnulado.TabIndex = 211
        Me.lbAnulado.Text = "ANULADO"
        Me.lbAnulado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbAnulado.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.lbAnulado)
        Me.Panel1.Controls.Add(Me.txtCod_Art)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.GridFacturas)
        Me.Panel1.Location = New System.Drawing.Point(8, 112)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(632, 152)
        Me.Panel1.TabIndex = 212
        '
        'txtCod_Art
        '
        Me.txtCod_Art.EditValue = ""
        Me.txtCod_Art.Location = New System.Drawing.Point(-96, 40)
        Me.txtCod_Art.Name = "txtCod_Art"
        '
        'txtCod_Art.Properties
        '
        Me.txtCod_Art.Properties.Enabled = False
        Me.txtCod_Art.Properties.ReadOnly = True
        Me.txtCod_Art.Size = New System.Drawing.Size(96, 19)
        Me.txtCod_Art.TabIndex = 196
        '
        'Label16
        '
        Me.Label16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(24, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(570, 16)
        Me.Label16.TabIndex = 191
        Me.Label16.Text = "Facturas"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GridFacturas
        '
        Me.GridFacturas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridFacturas.DataMember = "ComisionesPendientes"
        Me.GridFacturas.DataSource = Me.DsPagoComision1
        '
        'GridFacturas.EmbeddedNavigator
        '
        Me.GridFacturas.EmbeddedNavigator.Name = ""
        Me.GridFacturas.Location = New System.Drawing.Point(8, 24)
        Me.GridFacturas.MainView = Me.GridView2
        Me.GridFacturas.Name = "GridFacturas"
        Me.GridFacturas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.GridFacturas.Size = New System.Drawing.Size(616, 120)
        Me.GridFacturas.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridFacturas.TabIndex = 0
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFactura, Me.colFecha, Me.colTotal, Me.colComision, Me.colPagar})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView2.OptionsView.ShowFilterPanel = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colFactura
        '
        Me.colFactura.Caption = "Factura"
        Me.colFactura.FieldName = "Num_Factura"
        Me.colFactura.Name = "colFactura"
        Me.colFactura.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFactura.VisibleIndex = 0
        Me.colFactura.Width = 149
        '
        'colFecha
        '
        Me.colFecha.Caption = "Fecha"
        Me.colFecha.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.colFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colFecha.FieldName = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFecha.VisibleIndex = 1
        Me.colFecha.Width = 165
        '
        'colTotal
        '
        Me.colTotal.Caption = "Total"
        Me.colTotal.DisplayFormat.FormatString = "#,#0.00"
        Me.colTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colTotal.FieldName = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTotal.VisibleIndex = 2
        Me.colTotal.Width = 149
        '
        'colComision
        '
        Me.colComision.Caption = "Comision"
        Me.colComision.DisplayFormat.FormatString = "#,#0.00"
        Me.colComision.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colComision.FieldName = "Comision"
        Me.colComision.Name = "colComision"
        Me.colComision.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colComision.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colComision.VisibleIndex = 3
        Me.colComision.Width = 100
        '
        'colPagar
        '
        Me.colPagar.Caption = "Pagar"
        Me.colPagar.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colPagar.FieldName = "Pagar"
        Me.colPagar.Name = "colPagar"
        Me.colPagar.VisibleIndex = 4
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        Me.RepositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(392, 272)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(144, 16)
        Me.Label8.TabIndex = 214
        Me.Label8.Text = "Total Comisi�n : "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotal
        '
        Me.txtTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsPagoComision1, "PagoComision.Total"))
        Me.txtTotal.EditValue = "0.0"
        Me.txtTotal.Location = New System.Drawing.Point(536, 272)
        Me.txtTotal.Name = "txtTotal"
        '
        'txtTotal.Properties
        '
        Me.txtTotal.Properties.DisplayFormat.FormatString = "#,#0.0"
        Me.txtTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotal.Properties.EditFormat.FormatString = "#,#0.0"
        Me.txtTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotal.Properties.ReadOnly = True
        Me.txtTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtTotal.Size = New System.Drawing.Size(104, 22)
        Me.txtTotal.TabIndex = 213
        '
        'AdapterFacturas
        '
        Me.AdapterFacturas.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterFacturas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ComisionesPendientes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("CodComisionista", "CodComisionista"), New System.Data.Common.DataColumnMapping("Comision", "Comision"), New System.Data.Common.DataColumnMapping("PagoComision", "PagoComision")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Id, Num_Factura, Total, Fecha, CodComisionista, Comision, PagoComision FRO" & _
        "M ComisionesPendientes"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Id, Num_Factura, Total, Fecha, CodComisionista, Comision FROM ComisionesPe" & _
        "ndientes"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'B_Desmarcar
        '
        Me.B_Desmarcar.Enabled = False
        Me.B_Desmarcar.Location = New System.Drawing.Point(144, 272)
        Me.B_Desmarcar.Name = "B_Desmarcar"
        Me.B_Desmarcar.Size = New System.Drawing.Size(136, 23)
        Me.B_Desmarcar.TabIndex = 216
        Me.B_Desmarcar.Text = "Deseleccionar Todas"
        '
        'B_Marca
        '
        Me.B_Marca.Enabled = False
        Me.B_Marca.Location = New System.Drawing.Point(8, 272)
        Me.B_Marca.Name = "B_Marca"
        Me.B_Marca.Size = New System.Drawing.Size(120, 23)
        Me.B_Marca.TabIndex = 215
        Me.B_Marca.Text = "Seleccionar Todas"
        '
        'PagoComisiones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(650, 352)
        Me.Controls.Add(Me.B_Desmarcar)
        Me.Controls.Add(Me.B_Marca)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox_Datos_Comisionista)
        Me.Controls.Add(Me.LFecha)
        Me.Controls.Add(Me.txtNumPago)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCedulaUsuario)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.Label48)
        Me.Name = "PagoComisiones"
        Me.Text = "Pago de Comisiones"
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.Label48, 0)
        Me.Controls.SetChildIndex(Me.txtUsuario, 0)
        Me.Controls.SetChildIndex(Me.txtNombreUsuario, 0)
        Me.Controls.SetChildIndex(Me.txtCedulaUsuario, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.txtNumPago, 0)
        Me.Controls.SetChildIndex(Me.LFecha, 0)
        Me.Controls.SetChildIndex(Me.GroupBox_Datos_Comisionista, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.txtTotal, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.B_Marca, 0)
        Me.Controls.SetChildIndex(Me.B_Desmarcar, 0)
        CType(Me.DsPagoComision1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_Datos_Comisionista.ResumeLayout(False)
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCedula.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtCod_Art.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Variables"
    Dim usua
    Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
    Private sqlConexion As SqlConnection
    Dim CConexion As New Conexion
#End Region

#Region "Load"
    Private Sub PagoComisiones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = GetSetting("Seesoft", "SeePos", "Conexion")
        AdapterUsuarios.Fill(DsPagoComision1, "Usuarios")
        ValoresDefecto()
    End Sub

    Private Sub ValoresDefecto()
        'PAGO COMISIONES
        DsPagoComision1.PagoComision.NumPagoColumn.AutoIncrement = True
        DsPagoComision1.PagoComision.NumPagoColumn.AutoIncrementSeed = -1
        DsPagoComision1.PagoComision.NumPagoColumn.AutoIncrementStep = -1

        DsPagoComision1.PagoComision.Cod_ComisionistaColumn.DefaultValue = 0
        DsPagoComision1.PagoComision.Nombre_ComisionistaColumn.DefaultValue = ""
        DsPagoComision1.PagoComision.TotalColumn.DefaultValue = 0.0
        DsPagoComision1.PagoComision.FechaColumn.DefaultValue = Now
        DsPagoComision1.PagoComision.UsuarioColumn.DefaultValue = ""
        DsPagoComision1.PagoComision.AnuladoColumn.DefaultValue = 0

        DsPagoComision1.ComisionesPendientes.PagarColumn.DefaultValue = False
    End Sub
#End Region

#Region "Toolbar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Nuevo()

            Case 2 : If PMU.Find Then Buscar() Else MsgBox("No tiene permiso para Buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 3 : If PMU.Update Then Guardar() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 4 : If PMU.Delete Then Anular() Else MsgBox("No tiene permiso para eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 5 : If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 7 : Close()
        End Select
    End Sub
#End Region

#Region "Nuevo"
    Private Sub Nuevo()
        lbAnulado.Visible = False
        DsPagoComision1.PagoComision.Clear()
        DsPagoComision1.ComisionesPendientes.Clear()

        If ToolBar1.Buttons(0).Text = "Nuevo" Then
            ToolBar1.Buttons(0).Text = "Cancelar"
            ToolBar1.Buttons(0).ImageIndex = 8

            Try 'inicia la edicion
                BindingContext(DsPagoComision1, "PagoComision").CancelCurrentEdit()
                BindingContext(DsPagoComision1, "PagoComision").EndCurrentEdit()
                BindingContext(DsPagoComision1, "PagoComision").AddNew()
                Habilitar_InHabilitar(True)
                ToolBarBuscar.Enabled = False
                ToolBarNuevo.Enabled = True
                ToolBarRegistrar.Enabled = True
                ToolBarImprimir.Enabled = False
                ToolBarEliminar.Enabled = False
                txtCedula.Focus()

            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        Else
            Try
                BindingContext(DsPagoComision1, "PagoComision").CancelCurrentEdit()
                BindingContext(DsPagoComision1, "PagoComision").EndCurrentEdit()
                Habilitar_InHabilitar(False)
                ToolBar1.Buttons(0).Text = "Nuevo"
                ToolBar1.Buttons(0).ImageIndex = 0
                ToolBarBuscar.Enabled = True
                ToolBarNuevo.Enabled = True
                ToolBarRegistrar.Enabled = False
                ToolBarImprimir.Enabled = False
                ToolBarEliminar.Enabled = False

            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        End If
    End Sub
#End Region

#Region "Buscar"
    Private Sub Buscar()
        Try
            Dim frmBuscar As New FrmBuscador

            DsPagoComision1.PagoComision.Clear()
            DsPagoComision1.ComisionesPendientes.Clear()
            
            lbAnulado.Visible = False
            ToolBarEliminar.Enabled = False
            ToolBarImprimir.Enabled = False
            ToolBarNuevo.Enabled = True

            frmBuscar.SQLString = "SELECT NumPago AS Pago, Fecha, Nombre_Comisionista AS Comisionista, Total FROM PagoComision ORDER BY NumPago DESC"
            frmBuscar.Text = "Buscar Pago de Comisiones"
            frmBuscar.CampoFiltro = "Comisionista"
            frmBuscar.CampoFecha = "Fecha"
            frmBuscar.NuevaConexion = SqlConnection1.ConnectionString
            frmBuscar.ShowDialog()

            If frmBuscar.Cancelado Then
                Exit Sub
            Else
                If IsNothing(frmBuscar.Codigo) Then
                    Exit Sub
                End If
                LlenarPago(frmBuscar.Codigo)
                ToolBarImprimir.Enabled = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub LlenarPago(ByVal Id As Double)
        Dim cnnv As SqlConnection = Nothing
        Dim dt As New DataTable
        '
        ' Dentro de un Try/Catch por si se produce un error
        Try
            '''''''''LLENAR ORDEN''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            cnnv = New SqlConnection(sConn)
            cnnv.Open()
            ' Creamos el comando para la consulta
            Dim cmdv As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM PagoComision WHERE (NumPago = @Id_Factura) "
            cmdv.CommandText = sel
            cmdv.Connection = cnnv
            cmdv.CommandType = CommandType.Text
            cmdv.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmdv.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.BigInt))
            cmdv.Parameters("@Id_Factura").Value = Id

            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim dv As New SqlDataAdapter
            dv.SelectCommand = cmdv
            ' Llenamos la tabla
            dv.Fill(DsPagoComision1, "PagoComision")

            If BindingContext(DsPagoComision1, "PagoComision").Current("Anulado") Then
                lbAnulado.Visible = True
            Else
                ToolBarEliminar.Enabled = True
            End If

            '''''''''LLENAR ORDEN DETALLES''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            sel = "SELECT * FROM ComisionesPendientes WHERE (PagoComision = @Id_Factura) "

            cmd.CommandText = sel
            cmd.Connection = cnnv
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmd.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.BigInt))
            cmd.Parameters("@Id_Factura").Value = Id

            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            ' Llenamos la tabla
            da.Fill(DsPagoComision1, "ComisionesPendientes")

        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Sub
#End Region

#Region "Guardar"
    Private Sub Guardar()
        Try
            If BindingContext(DsPagoComision1, "ComisionesPendientes").Count = 0 Then 'Si el cierre no tiene detalle
                MsgBox("No se puede guardar un pago si no contiene facturas!", MsgBoxStyle.Critical)
                ToolBar1.Buttons(3).Enabled = True
                Exit Sub
            End If

            If MessageBox.Show("�Desea guardar el pago?", "SeeSoft", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                CalcularTotal()

                If txtTotal.EditValue <= 0 Then
                    MsgBox("No se puede guardar un pago si no contiene facturas a pagar!", MsgBoxStyle.Critical)
                    ToolBar1.Buttons(3).Enabled = True
                    Exit Sub
                End If

                BindingContext(DsPagoComision1, "PagoComision").EndCurrentEdit()

                If Registrar_Pago() And Actualizar_Ventas(BindingContext(DsPagoComision1, "PagoComision").Current("NumPago")) Then
                    ToolBar1.Buttons(4).Enabled = False
                    ToolBar1.Buttons(1).Enabled = False
                    ToolBar1.Buttons(0).Text = "Nuevo"
                    ToolBar1.Buttons(0).ImageIndex = 0
                    ToolBarNuevo.Enabled = False
                    ToolBar1.Buttons(3).Enabled = False
                    ToolBar1.Buttons(2).Enabled = False

                    MsgBox("El pago se guardo Satisfactoriamente!!", MsgBoxStyle.Information)
                    If (MsgBox("Desea Imprimir el pago?", MsgBoxStyle.YesNo)) = MsgBoxResult.Yes Then
                        imprimir()
                    End If

                    DsPagoComision1.PagoComision.Clear()
                    DsPagoComision1.ComisionesPendientes.Clear()
                    Habilitar_InHabilitar(False)
                    txtUsuario.Text = ""
                    txtCedulaUsuario.Text = ""
                    txtNombreUsuario.Text = ""
                    txtUsuario.Focus()
                Else
                    MsgBox("Error al Guardar el pago!!!", MsgBoxStyle.Critical)
                End If
            Else
                Exit Sub
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Function Registrar_Pago() As Boolean
        If Me.SqlConnection1.State <> SqlConnection1.State.Open Then SqlConnection1.Open()
        Dim Trans As SqlTransaction = SqlConnection1.BeginTransaction
        Try
            AdapterPagoComision.InsertCommand.Transaction = Trans
            AdapterPagoComision.DeleteCommand.Transaction = Trans
            AdapterPagoComision.UpdateCommand.Transaction = Trans
            AdapterPagoComision.SelectCommand.Transaction = Trans

            AdapterPagoComision.Update(DsPagoComision1, "PagoComision")

            Trans.Commit()
            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            Me.ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try
    End Function

    Function Actualizar_Ventas(ByVal NumPago As Integer) As Boolean
        Dim Cx As New Conexion
        Try
            For i As Integer = 0 To DsPagoComision1.ComisionesPendientes.Count - 1
                If DsPagoComision1.ComisionesPendientes(i).Pagar Then
                    Cx.UpdateRecords("Ventas", "PagoComision = " & NumPago, "Id = " & DsPagoComision1.ComisionesPendientes(i).Id)
                End If
            Next

            Return True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            Me.ToolBar1.Buttons(3).Enabled = True
            Return False
        End Try
    End Function
#End Region

#Region "Anular"
    Private Sub Anular()
        Dim cConexion As New Conexion
        Dim Restringida As Boolean

        Try
            If Me.BindingContext(DsPagoComision1, "PagoComision").Current("Anulado") = True Then
                MsgBox("El pago ya esta anulado!!!", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            Dim resp As Integer
            If Me.BindingContext(DsPagoComision1, "PagoComision").Count > 0 Then
                resp = MessageBox.Show("�Desea Anular este pago de comisi�n?", "SeeSoft", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    If Insertar_Bitacora() And Registrar_Anulacion_Pago() Then
                        MsgBox("El pago ha sido anulado correctamente!", MsgBoxStyle.Information)
                        ToolBar1.Buttons(4).Enabled = False
                        ToolBar1.Buttons(1).Enabled = False
                        ToolBar1.Buttons(0).Text = "Nuevo"
                        ToolBar1.Buttons(0).ImageIndex = 0
                        ToolBarNuevo.Enabled = False
                        ToolBarImprimir.Enabled = False
                        ToolBar1.Buttons(3).Enabled = False
                        ToolBar1.Buttons(2).Enabled = False
                        DsPagoComision1.PagoComision.Clear()
                        DsPagoComision1.ComisionesPendientes.Clear()
                        txtUsuario.Text = ""
                        txtCedulaUsuario.Text = ""
                        txtNombreUsuario.Text = ""
                        txtUsuario.Focus()
                    End If
                Else
                    Exit Sub
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Function Registrar_Anulacion_Pago() As Boolean
        Dim Cx As New Conexion
        Try
            Cx.UpdateRecords("PagoComision", "Anulado = 1", "NumPago = " & BindingContext(DsPagoComision1, "PagoComision").Current("NumPago"))
            Return True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            Me.ToolBar1.Buttons(3).Enabled = True
            Return False
        End Try
    End Function

    Function Insertar_Bitacora() As Boolean
        Try
            Dim funciones As New Conexion
            Dim datos As String
            datos = "'PAGO COMISION','" & txtNumPago.Text & "', 'COMISIONISTA " & txtNombre.Text & "','PAGO DE COMISI�N ANULADO','" & txtNombreUsuario.Text & "'," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0
            If funciones.AddNewRecord("Bitacora", "Tabla,Campo_Clave,DescripcionCampo,Accion,Usuario,Costo,VentaA,VentaB,VentaC,VentaD", datos) <> "" Then
                MsgBox("Problemas al Anular el pago!", MsgBoxStyle.Critical)
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            Return False
        End Try
    End Function
#End Region

#Region "Imprimir"
    Function imprimir()
        Try
            ToolBar1.Buttons(4).Enabled = False

            Dim Reporte As New PagoComision_PVE
            CrystalReportsConexion.LoadReportViewer(Nothing, Reporte, True)
            Reporte.PrintOptions.PrinterName = Automatic_Printer_Dialog(0) 'PV
            Reporte.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            Reporte.SetParameterValue(0, CInt(txtNumPago.Text))
            Reporte.PrintToPrinter(1, True, 0, 0)

            ToolBar1.Buttons(4).Enabled = True

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function

    Private Function Automatic_Printer_Dialog(ByVal PrinterToSelect As Byte) As String 'SAJ 01092006 
        Dim PrintDocument1 As New PrintDocument
        Dim DefaultPrinter As String = PrintDocument1.PrinterSettings.PrinterName
        Dim PrinterInstalled As String
        'BUSCA LA IMPRESORA PREDETERMINADA PARA EL SISTEMA
        For Each PrinterInstalled In PrinterSettings.InstalledPrinters
            Select Case Split(PrinterInstalled.ToUpper, "\").GetValue(Split(PrinterInstalled.ToUpper, "\").GetLength(0) - 1)
                Case "FACTURACION"
                    If PrinterToSelect = 0 Then
                        Return PrinterInstalled.ToString
                        Exit Function
                    End If
                Case "PUNTOVENTA"
                    If PrinterToSelect = 3 Then
                        Return PrinterInstalled.ToString
                        Exit Function
                    End If
            End Select
        Next
        If MsgBox("No se ha encontrado las impresoras predeterminadas para el sistema..." & vbCrLf & "Desea proceder a selecionar una impresora....", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Atenci�n...") = MsgBoxResult.Yes Then
            Dim PrinterDialog As New PrintDialog
            Dim DocPrint As New PrintDocument
            PrinterDialog.Document = DocPrint
            PrinterDialog.ShowDialog()
            If PrinterDialog.ShowDialog.Yes Then
                Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
            Else
                Return DefaultPrinter 'NO SE SELECCIONO IMPRESORA ALGUNA
            End If
        End If
    End Function
#End Region

#Region "Controles Funciones"
    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyCode = Keys.Enter Then
            Loggin_Usuario()
        End If
    End Sub

    Private Sub txtCedula_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCedula.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim funcion As New cFunciones
            Dim Id As String
            Dim con As String = GetSetting("SeeSoft", "SeePos", "Conexion")
            Try
                Id = funcion.BuscarDatos("Select Codigo, Nombre from Comisionista", "Nombre", "Buscar por nombre del comisionista ...", con)
                If Id = Nothing Then ' si se dio en el boton de cancelar
                    Exit Sub
                End If

                If Id <> "" And Id <> "0" Then
                    txtCedula.Text = Id
                Else
                    Exit Sub
                End If

                If txtCedula.Text <> "" And txtCedula.Text <> "0" Then
                    sqlConexion = CConexion.Conectar("SeePOS")
                    txtNombre.Text = CConexion.SlqExecuteScalar(sqlConexion, "Select Nombre from Comisionista where Cedula ='" & txtCedula.Text & "'")
                    CConexion.DesConectar(sqlConexion)
                Else
                    txtNombre.Text = ""
                    Exit Sub
                End If

                LlenarGridFacturas()
                GridFacturas.Focus()

            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        End If

        If e.KeyCode = Keys.Enter Then
            Try
                If txtCedula.Text <> "" And txtCedula.Text <> "0" Then
                    sqlConexion = CConexion.Conectar("SeePOS")
                    txtNombre.Text = CConexion.SlqExecuteScalar(sqlConexion, "Select Nombre from Comisionista where Codigo ='" & txtCedula.Text & "'")
                    CConexion.DesConectar(sqlConexion)
                Else
                    txtNombre.Text = ""
                    Exit Sub
                End If
                LlenarGridFacturas()
                GridFacturas.Focus()

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub


    Private Sub RepositoryItemCheckEdit1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RepositoryItemCheckEdit1.CheckedChanged
        Try
            CalcularTotal()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub B_Marca_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Marca.Click
        Marca_Desmarca(True)
    End Sub


    Private Sub B_Desmarcar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Desmarcar.Click
        Marca_Desmarca(False)
    End Sub


    Private Sub txtCedula_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCedula.KeyPress
        If (Not Char.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub txtCedula_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCedula.GotFocus
        txtCedula.SelectAll()
    End Sub
#End Region

#Region "Funciones"
    Private Sub Habilitar_InHabilitar(ByVal Estado As Boolean)
        GroupBox_Datos_Comisionista.Enabled = Estado
        GridFacturas.Enabled = Estado
        B_Marca.Enabled = Estado
        B_Desmarcar.Enabled = Estado
    End Sub

    Private Sub Loggin_Usuario()
        Try
            If BindingContext(DsPagoComision1.Usuarios).Count > 0 Then
                Dim Usuario_autorizadores() As System.Data.DataRow
                Dim Usua As System.Data.DataRow

                Usuario_autorizadores = DsPagoComision1.Usuarios.Select("Clave_Interna ='" & txtUsuario.Text & "'")
                If Usuario_autorizadores.Length <> 0 Then
                    Usua = Usuario_autorizadores(0)
                    PMU = VSM(Usua("Cedula"), Name) 'Carga los privilegios del usuario con el modulo 
                    If Not PMU.Execute Then
                        MsgBox("Usted no tiene permisos para realizar pagos..", MsgBoxStyle.Exclamation)
                        txtUsuario.Text = ""
                        txtUsuario.Focus()
                        Exit Sub
                    End If
                    DsPagoComision1.PagoComision.UsuarioColumn.DefaultValue = Usua("Cedula")
                    ToolBarNuevo.Enabled = True
                    ToolBarBuscar.Enabled = True
                    Nuevo()
                    txtNombreUsuario.Text = Usua("Nombre")
                    txtCedulaUsuario.Text = Usua("Cedula")

                Else ' si no existe una contrase�a como esta
                    MsgBox("Contrase�a interna incorrecta", MsgBoxStyle.Exclamation)
                    txtUsuario.Text = ""
                    txtNombreUsuario.Text = ""
                    txtCedulaUsuario.Text = ""
                    ToolBarNuevo.Enabled = False
                    ToolBarBuscar.Enabled = False
                End If
            Else
                MsgBox("No Existen Usuarios, ingrese datos")
            End If

        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub LlenarGridFacturas()
        Try
            Dim sqlCommand As New System.Data.SqlClient.SqlCommand
            Dim funcion As New cFunciones

            If txtCedula.Text <> "" And txtNombre.Text <> "" Then
                DsPagoComision1.ComisionesPendientes.Clear()
                sqlCommand.Connection = SqlConnection1
                sqlCommand.CommandText = "SELECT * FROM dbo.ComisionesPendientes WHERE PagoComision = 0 AND CodComisionista = " & txtCedula.Text
                AdapterFacturas.SelectCommand = sqlCommand
                AdapterFacturas.Fill(DsPagoComision1, "ComisionesPendientes")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Marca_Desmarca(ByVal Estado As Boolean)
        For i As Integer = 0 To DsPagoComision1.ComisionesPendientes.Count - 1
            DsPagoComision1.ComisionesPendientes(i).Pagar = Estado
        Next
        CalcularTotal()
    End Sub

    Private Sub CalcularTotal()
        Try
            txtTotal.EditValue = 0
            GridView2.CloseEditor()
            GridView2.UpdateCurrentRow()

            For i As Integer = 0 To DsPagoComision1.ComisionesPendientes.Count - 1
                If DsPagoComision1.ComisionesPendientes(i).Pagar = True Then
                    txtTotal.EditValue += DsPagoComision1.ComisionesPendientes(i).Comision
                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

End Class
