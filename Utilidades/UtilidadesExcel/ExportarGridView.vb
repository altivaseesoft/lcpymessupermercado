﻿Imports System.Data
Imports System.Windows.Forms
Imports Microsoft.Office.Interop

Public Class ExportarGridView
    Public Shared Sub Exportar(datos As DataGridView)
        Dim file As New SaveFileDialog
        Dim ubicacionFisica As String = ""
        file.Filter = "Excel (*.xls)|*.xls|All files (*.*)|*.*"
        If file.ShowDialog() = DialogResult.OK Then
            ubicacionFisica = file.FileName

        End If
        If ubicacionFisica.Equals("") Then
            Exit Sub
        End If

        Try
            If ((datos.Columns.Count = 0) Or (datos.Rows.Count = 0)) Then
                Exit Sub
            End If

            'Creando Dataset para Exportar
            Dim dset As New DataSet
            'Agregar tabla al Dataset
            dset.Tables.Add()
            'AGregar Columna a la tabla
            For i As Integer = 0 To datos.ColumnCount - 1
                dset.Tables(0).Columns.Add(datos.Columns(i).HeaderText)
            Next
            'Agregar filas a la tabla
            Dim dr1 As DataRow
            For i As Integer = 0 To datos.RowCount - 1
                dr1 = dset.Tables(0).NewRow
                For j As Integer = 0 To datos.Columns.Count - 1
                    dr1(j) = datos.Rows(i).Cells(j).Value
                Next
                dset.Tables(0).Rows.Add(dr1)
            Next

            Dim aplicacion As New Excel.Application
            Dim wBook As Microsoft.Office.Interop.Excel.Workbook
            Dim wSheet As Microsoft.Office.Interop.Excel.Worksheet

            wBook = aplicacion.Workbooks.Add()
            wSheet = wBook.ActiveSheet()

            Dim dt As System.Data.DataTable = dset.Tables(0)
            Dim dc As System.Data.DataColumn
            Dim dr As System.Data.DataRow
            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0

            For Each dc In dt.Columns
                colIndex = colIndex + 1
                aplicacion.Cells(1, colIndex) = dc.ColumnName
            Next

            For Each dr In dt.Rows
                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In dt.Columns
                    colIndex = colIndex + 1
                    aplicacion.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)

                Next
            Next
            'Configurar la orientacion de la  hoja y el tamaño
            Try
                wSheet.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape
                wSheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperLegal
            Catch ex As Exception

            End Try

            'Configurar con negrilla la cabecera y tenga autofit
            wSheet.Rows.Item(1).Font.Bold = 1
            wSheet.Columns.AutoFit()


            Dim strFileName As String = ubicacionFisica
            Dim blnFileOpen As Boolean = False
            Try
                Dim fileTemp As System.IO.FileStream = System.IO.File.OpenWrite(strFileName)
                fileTemp.Close()
            Catch ex As Exception
                blnFileOpen = False
            End Try

            If System.IO.File.Exists(strFileName) Then
                System.IO.File.Delete(strFileName)
            End If
            MessageBox.Show("El documento fue exportado correctamente.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
            wBook.SaveAs(strFileName)
            aplicacion.Workbooks.Open(strFileName)
            aplicacion.Visible = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "SISTEMA INTEGRADO DE ADMINISTRACION DE PERSONAL", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub
End Class
