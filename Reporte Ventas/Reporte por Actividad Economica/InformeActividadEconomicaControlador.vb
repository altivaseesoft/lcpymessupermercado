﻿Imports System.Data
Imports System.Data.SqlClient


Public Class InformeActividadEconomicaControlador : Inherits dstActividadEconomica
    Private Sub CargarCliente()
        Dim sql As New SqlCommand
        sql.CommandText = "Select * From Clientes"
        cFunciones.Llenar_Tabla_Generico(sql, Me.Clientes)
    End Sub


    Public Shared Sub cargarComboActivdadEconomica(ByRef tablaTB_CE_ActividadEconomica As DataTable)
        InformeActividadEconomicaDatos.cargarDatosActividadEconomica(tablaTB_CE_ActividadEconomica)
    End Sub
    Public Shared Sub AbrirInforme(ByRef datos As dstActividadEconomica, nombreUsuario As String, nombreActividad As String, nombreMes As String)
        Dim frm As New frmVisorReportes
        Dim rpt As New rtpInformeIVAActividadEconomica
        rpt.Subreports(0).SetDataSource(datos)
        rpt.Subreports(1).SetDataSource(datos)
        rpt.SetParameterValue("UsuarioImpresion", nombreUsuario)
        rpt.SetParameterValue("ActividadEconomica", nombreActividad)
        rpt.SetParameterValue("Periodo", nombreMes)
        frm.rptViewer.ReportSource = rpt
        frm.Show()

    End Sub
    Public Shared Sub cargarComboPeriodo(ByRef tabla As DataTable)
        InformeActividadEconomicaDatos.cargarDatosPeriodo(tabla)
    End Sub

    Public Shared Sub cargarDgvDetalleVentas(ByRef tabla As DataTable, ByVal IdActividadEconomica As Integer, IdPeriodo As Integer, Tipo As Integer)
        InformeActividadEconomicaDatos.cargarDatosDgvDetallesVentas(tabla, IdActividadEconomica, diaInicialDelPeriodo(IdPeriodo), diaFinalDelPeriodo(IdPeriodo), Tipo)
    End Sub
    Public Shared Sub cargarDgvDetalleVentasMostrar(ByRef tabla As DataTable, ByVal IdActividadEconomica As Integer, IdPeriodo As Integer, Tipo As Integer)
        InformeActividadEconomicaDatos.cargarDatosDgvDetallesVentasMostrar(tabla, IdActividadEconomica, diaInicialDelPeriodo(IdPeriodo), diaFinalDelPeriodo(IdPeriodo), Tipo)
    End Sub
    Public Shared Sub cargarDgvDetalleCompras(ByRef tabla As DataTable, ByVal IdActividadEconomica As Integer, IdPeriodo As Integer)
        InformeActividadEconomicaDatos.cargarDatosDgvDetallesCompras(tabla, IdActividadEconomica, diaInicialDelPeriodo(IdPeriodo), diaFinalDelPeriodo(IdPeriodo))
    End Sub
    Public Shared Sub cargarDgvDetalleComprasMostrar(ByRef tabla As DataTable, ByVal IdActividadEconomica As Integer, IdPeriodo As Integer)
        InformeActividadEconomicaDatos.cargarDatosDgvDetallesComprasMostrar(tabla, IdActividadEconomica, diaInicialDelPeriodo(IdPeriodo), diaFinalDelPeriodo(IdPeriodo))
    End Sub
    Public Shared Sub cargarDgvResumenCompras(ByRef tabla As DataTable, ByVal IdActividadEconomica As Integer, IdPeriodo As Integer)
        InformeActividadEconomicaDatos.cargarDatosDgvResumenCompras(tabla, IdActividadEconomica, diaInicialDelPeriodo(IdPeriodo), diaFinalDelPeriodo(IdPeriodo))
    End Sub

    Public Shared Sub cargarDgvResumenVentas(ByRef tabla As DataTable, ByVal IdActividadEconomica As Integer, IdPeriodo As Integer, Tipo As Integer)
        InformeActividadEconomicaDatos.cargarDatosDgvResumenVentas(tabla, IdActividadEconomica, diaInicialDelPeriodo(IdPeriodo), diaFinalDelPeriodo(IdPeriodo), Tipo)
    End Sub

    Public Shared Function diaInicialDelPeriodo(IdPeriodo As Integer) As Date
        Dim diaInicio As Date
        diaInicio = "01/" & InformeActividadEconomicaDatos.obtenerMesPeriodo(IdPeriodo).ToString & "/" & InformeActividadEconomicaDatos.obtenerAnnoPeriodo(IdPeriodo).ToString & ""
        Return diaInicio
    End Function

    Public Shared Function diaFinalDelPeriodo(IdPeriodo As Integer) As Date
        Dim diaFinal As Date
        diaFinal = DateSerial(Year(diaInicialDelPeriodo(IdPeriodo)), Month(diaInicialDelPeriodo(IdPeriodo)) + 1, 0)
        Return diaFinal
    End Function
End Class
