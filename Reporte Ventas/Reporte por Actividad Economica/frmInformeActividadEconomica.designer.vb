﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmInformeActividadEconomica
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInformeActividadEconomica))
        Me.cmbMes = New System.Windows.Forms.ComboBox()
        Me.enlaceNumeroPeriodo = New System.Windows.Forms.BindingSource(Me.components)
        Me.datos = New LcPymes_5._2.dstActividadEconomica()
        Me.cmbActividadEconomica = New System.Windows.Forms.ComboBox()
        Me.enlaceActividadEconomica = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.dgvResumen = New System.Windows.Forms.DataGridView()
        Me.PorcentajeImpuestoResumenCompras = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotalDataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImpuestoDataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ResumenComprasBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblActividadEconimica = New System.Windows.Forms.Label()
        Me.dgvDetalleCompras = New System.Windows.Forms.DataGridView()
        Me.DocumentoDataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoDataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProveedorDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotalDataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImpuestoDataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvDetalleBSCompras = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvDetalleBSVentas = New System.Windows.Forms.BindingSource(Me.components)
        Me.Documento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotal1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Subtotal2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Subtotal4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Subtotal8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotal13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocumentoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClienteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImpuestoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocumentoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClienteDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImpuestoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotalDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LblSubTotalResumenCompras = New System.Windows.Forms.Label()
        Me.LblImpuestoResumenCompras = New System.Windows.Forms.Label()
        Me.dgvResumenVentas = New System.Windows.Forms.DataGridView()
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotalDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImpuestoDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ResumenVentasBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblSubTotalResumenVentas = New System.Windows.Forms.Label()
        Me.LblImpuestoResumenVentas = New System.Windows.Forms.Label()
        Me.DocumentoDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClienteDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.bwDatos = New System.ComponentModel.BackgroundWorker()
        Me.TabControlVC = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btExportarCompras = New System.Windows.Forms.Button()
        Me.panelComprasTotales = New System.Windows.Forms.FlowLayoutPanel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btExportarVentas = New System.Windows.Forms.Button()
        Me.panelVentasTotales = New System.Windows.Forms.FlowLayoutPanel()
        Me.dgvDetalleVentas = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gbAnalisisVenta = New System.Windows.Forms.GroupBox()
        Me.rbGeneral = New System.Windows.Forms.RadioButton()
        Me.rbNoTributable = New System.Windows.Forms.RadioButton()
        Me.rbTributable = New System.Windows.Forms.RadioButton()
        CType(Me.enlaceNumeroPeriodo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.datos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.enlaceActividadEconomica, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvResumen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResumenComprasBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetalleCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetalleBSCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetalleBSVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvResumenVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResumenVentasBS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlVC.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvDetalleVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbAnalisisVenta.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbMes
        '
        Me.cmbMes.DataSource = Me.enlaceNumeroPeriodo
        Me.cmbMes.DisplayMember = "AnnoMes"
        Me.cmbMes.FormattingEnabled = True
        Me.cmbMes.Location = New System.Drawing.Point(12, 28)
        Me.cmbMes.Name = "cmbMes"
        Me.cmbMes.Size = New System.Drawing.Size(223, 21)
        Me.cmbMes.TabIndex = 0
        Me.cmbMes.ValueMember = "Id_Periodo"
        '
        'enlaceNumeroPeriodo
        '
        Me.enlaceNumeroPeriodo.DataMember = "vs_NumeroDePeriodoANombre"
        Me.enlaceNumeroPeriodo.DataSource = Me.datos
        '
        'datos
        '
        Me.datos.DataSetName = "dstActividadEconomica"
        Me.datos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'cmbActividadEconomica
        '
        Me.cmbActividadEconomica.DataSource = Me.enlaceActividadEconomica
        Me.cmbActividadEconomica.DisplayMember = "Descripcion"
        Me.cmbActividadEconomica.FormattingEnabled = True
        Me.cmbActividadEconomica.Location = New System.Drawing.Point(12, 79)
        Me.cmbActividadEconomica.Name = "cmbActividadEconomica"
        Me.cmbActividadEconomica.Size = New System.Drawing.Size(223, 21)
        Me.cmbActividadEconomica.TabIndex = 1
        Me.cmbActividadEconomica.ValueMember = "Id"
        '
        'enlaceActividadEconomica
        '
        Me.enlaceActividadEconomica.DataMember = "TB_CE_ActividadEconomica"
        Me.enlaceActividadEconomica.DataSource = Me.datos
        '
        'btnGenerar
        '
        Me.btnGenerar.Location = New System.Drawing.Point(12, 179)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(107, 36)
        Me.btnGenerar.TabIndex = 2
        Me.btnGenerar.Text = "Generar"
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(125, 179)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(110, 36)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'dgvResumen
        '
        Me.dgvResumen.AllowUserToAddRows = False
        Me.dgvResumen.AllowUserToDeleteRows = False
        Me.dgvResumen.AutoGenerateColumns = False
        Me.dgvResumen.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvResumen.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvResumen.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvResumen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResumen.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PorcentajeImpuestoResumenCompras, Me.SubTotalDataGridViewTextBoxColumn3, Me.ImpuestoDataGridViewTextBoxColumn3})
        Me.dgvResumen.DataSource = Me.ResumenComprasBS
        Me.dgvResumen.Location = New System.Drawing.Point(353, 44)
        Me.dgvResumen.Name = "dgvResumen"
        Me.dgvResumen.ReadOnly = True
        Me.dgvResumen.RowHeadersVisible = False
        Me.dgvResumen.Size = New System.Drawing.Size(374, 150)
        Me.dgvResumen.TabIndex = 5
        '
        'PorcentajeImpuestoResumenCompras
        '
        Me.PorcentajeImpuestoResumenCompras.DataPropertyName = "PorcentajeImpuesto"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.PorcentajeImpuestoResumenCompras.DefaultCellStyle = DataGridViewCellStyle2
        Me.PorcentajeImpuestoResumenCompras.HeaderText = "Porcentaje Impuesto"
        Me.PorcentajeImpuestoResumenCompras.Name = "PorcentajeImpuestoResumenCompras"
        Me.PorcentajeImpuestoResumenCompras.ReadOnly = True
        Me.PorcentajeImpuestoResumenCompras.Width = 135
        '
        'SubTotalDataGridViewTextBoxColumn3
        '
        Me.SubTotalDataGridViewTextBoxColumn3.DataPropertyName = "SubTotal"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.SubTotalDataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle3
        Me.SubTotalDataGridViewTextBoxColumn3.HeaderText = "SubTotal"
        Me.SubTotalDataGridViewTextBoxColumn3.Name = "SubTotalDataGridViewTextBoxColumn3"
        Me.SubTotalDataGridViewTextBoxColumn3.ReadOnly = True
        Me.SubTotalDataGridViewTextBoxColumn3.Width = 83
        '
        'ImpuestoDataGridViewTextBoxColumn3
        '
        Me.ImpuestoDataGridViewTextBoxColumn3.DataPropertyName = "Impuesto"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.ImpuestoDataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle4
        Me.ImpuestoDataGridViewTextBoxColumn3.HeaderText = "Impuesto"
        Me.ImpuestoDataGridViewTextBoxColumn3.Name = "ImpuestoDataGridViewTextBoxColumn3"
        Me.ImpuestoDataGridViewTextBoxColumn3.ReadOnly = True
        Me.ImpuestoDataGridViewTextBoxColumn3.Width = 83
        '
        'ResumenComprasBS
        '
        Me.ResumenComprasBS.DataMember = "ResumenCompras"
        Me.ResumenComprasBS.DataSource = Me.datos
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo.Location = New System.Drawing.Point(12, 4)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(89, 24)
        Me.lblPeriodo.TabIndex = 6
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblActividadEconimica
        '
        Me.lblActividadEconimica.AutoSize = True
        Me.lblActividadEconimica.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActividadEconimica.Location = New System.Drawing.Point(12, 53)
        Me.lblActividadEconimica.Name = "lblActividadEconimica"
        Me.lblActividadEconimica.Size = New System.Drawing.Size(212, 24)
        Me.lblActividadEconimica.TabIndex = 7
        Me.lblActividadEconimica.Text = "Actividad Económica:"
        '
        'dgvDetalleCompras
        '
        Me.dgvDetalleCompras.AllowUserToAddRows = False
        Me.dgvDetalleCompras.AllowUserToDeleteRows = False
        Me.dgvDetalleCompras.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDetalleCompras.AutoGenerateColumns = False
        Me.dgvDetalleCompras.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvDetalleCompras.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleCompras.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvDetalleCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleCompras.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DocumentoDataGridViewTextBoxColumn3, Me.TipoDataGridViewTextBoxColumn3, Me.FechaDataGridViewTextBoxColumn3, Me.ProveedorDataGridViewTextBoxColumn, Me.SubTotalDataGridViewTextBoxColumn4, Me.ImpuestoDataGridViewTextBoxColumn4, Me.TotalDataGridViewTextBoxColumn3})
        Me.dgvDetalleCompras.DataSource = Me.dgvDetalleBSCompras
        Me.dgvDetalleCompras.Location = New System.Drawing.Point(0, 0)
        Me.dgvDetalleCompras.Name = "dgvDetalleCompras"
        Me.dgvDetalleCompras.ReadOnly = True
        Me.dgvDetalleCompras.RowHeadersVisible = False
        Me.dgvDetalleCompras.Size = New System.Drawing.Size(987, 272)
        Me.dgvDetalleCompras.TabIndex = 4
        '
        'DocumentoDataGridViewTextBoxColumn3
        '
        Me.DocumentoDataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DocumentoDataGridViewTextBoxColumn3.DataPropertyName = "Documento"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.DocumentoDataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle6
        Me.DocumentoDataGridViewTextBoxColumn3.HeaderText = "Documento"
        Me.DocumentoDataGridViewTextBoxColumn3.Name = "DocumentoDataGridViewTextBoxColumn3"
        Me.DocumentoDataGridViewTextBoxColumn3.ReadOnly = True
        Me.DocumentoDataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DocumentoDataGridViewTextBoxColumn3.Width = 77
        '
        'TipoDataGridViewTextBoxColumn3
        '
        Me.TipoDataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.TipoDataGridViewTextBoxColumn3.DataPropertyName = "Tipo"
        Me.TipoDataGridViewTextBoxColumn3.HeaderText = "Tipo"
        Me.TipoDataGridViewTextBoxColumn3.Name = "TipoDataGridViewTextBoxColumn3"
        Me.TipoDataGridViewTextBoxColumn3.ReadOnly = True
        Me.TipoDataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TipoDataGridViewTextBoxColumn3.Width = 38
        '
        'FechaDataGridViewTextBoxColumn3
        '
        Me.FechaDataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.FechaDataGridViewTextBoxColumn3.DataPropertyName = "Fecha"
        Me.FechaDataGridViewTextBoxColumn3.HeaderText = "Fecha"
        Me.FechaDataGridViewTextBoxColumn3.Name = "FechaDataGridViewTextBoxColumn3"
        Me.FechaDataGridViewTextBoxColumn3.ReadOnly = True
        Me.FechaDataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FechaDataGridViewTextBoxColumn3.Width = 48
        '
        'ProveedorDataGridViewTextBoxColumn
        '
        Me.ProveedorDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.ProveedorDataGridViewTextBoxColumn.DataPropertyName = "Proveedor"
        Me.ProveedorDataGridViewTextBoxColumn.HeaderText = "Proveedor"
        Me.ProveedorDataGridViewTextBoxColumn.Name = "ProveedorDataGridViewTextBoxColumn"
        Me.ProveedorDataGridViewTextBoxColumn.ReadOnly = True
        Me.ProveedorDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ProveedorDataGridViewTextBoxColumn.Width = 71
        '
        'SubTotalDataGridViewTextBoxColumn4
        '
        Me.SubTotalDataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.SubTotalDataGridViewTextBoxColumn4.DataPropertyName = "SubTotal"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.SubTotalDataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle7
        Me.SubTotalDataGridViewTextBoxColumn4.HeaderText = "SubTotal"
        Me.SubTotalDataGridViewTextBoxColumn4.Name = "SubTotalDataGridViewTextBoxColumn4"
        Me.SubTotalDataGridViewTextBoxColumn4.ReadOnly = True
        Me.SubTotalDataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.SubTotalDataGridViewTextBoxColumn4.Width = 64
        '
        'ImpuestoDataGridViewTextBoxColumn4
        '
        Me.ImpuestoDataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.ImpuestoDataGridViewTextBoxColumn4.DataPropertyName = "Impuesto"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        Me.ImpuestoDataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle8
        Me.ImpuestoDataGridViewTextBoxColumn4.HeaderText = "Impuesto"
        Me.ImpuestoDataGridViewTextBoxColumn4.Name = "ImpuestoDataGridViewTextBoxColumn4"
        Me.ImpuestoDataGridViewTextBoxColumn4.ReadOnly = True
        Me.ImpuestoDataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ImpuestoDataGridViewTextBoxColumn4.Width = 64
        '
        'TotalDataGridViewTextBoxColumn3
        '
        Me.TotalDataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.TotalDataGridViewTextBoxColumn3.DataPropertyName = "Total"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.TotalDataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle9
        Me.TotalDataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.TotalDataGridViewTextBoxColumn3.Name = "TotalDataGridViewTextBoxColumn3"
        Me.TotalDataGridViewTextBoxColumn3.ReadOnly = True
        Me.TotalDataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TotalDataGridViewTextBoxColumn3.Width = 42
        '
        'dgvDetalleBSCompras
        '
        Me.dgvDetalleBSCompras.DataMember = "vs_detalle_compras"
        Me.dgvDetalleBSCompras.DataSource = Me.datos
        '
        'dgvDetalleBSVentas
        '
        Me.dgvDetalleBSVentas.DataMember = "vs_detalle_ventas"
        Me.dgvDetalleBSVentas.DataSource = Me.datos
        '
        'Documento
        '
        Me.Documento.DataPropertyName = "Documento"
        Me.Documento.HeaderText = "Documento"
        Me.Documento.Name = "Documento"
        Me.Documento.ReadOnly = True
        '
        'Tipo
        '
        Me.Tipo.DataPropertyName = "Tipo"
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'Cliente
        '
        Me.Cliente.DataPropertyName = "Cliente"
        Me.Cliente.HeaderText = "Cliente"
        Me.Cliente.Name = "Cliente"
        Me.Cliente.ReadOnly = True
        '
        'SubTotal
        '
        Me.SubTotal.HeaderText = "SubTotal 0%"
        Me.SubTotal.Name = "SubTotal"
        Me.SubTotal.ReadOnly = True
        '
        'Impuesto
        '
        Me.Impuesto.HeaderText = "Impuesto 0%"
        Me.Impuesto.Name = "Impuesto"
        Me.Impuesto.ReadOnly = True
        '
        'SubTotal1
        '
        Me.SubTotal1.HeaderText = "SubTotal 1%"
        Me.SubTotal1.Name = "SubTotal1"
        Me.SubTotal1.ReadOnly = True
        '
        'Impuesto1
        '
        Me.Impuesto1.HeaderText = "Impuesto 1%"
        Me.Impuesto1.Name = "Impuesto1"
        Me.Impuesto1.ReadOnly = True
        '
        'Subtotal2
        '
        Me.Subtotal2.HeaderText = "Subtotal 2%"
        Me.Subtotal2.Name = "Subtotal2"
        Me.Subtotal2.ReadOnly = True
        '
        'Impuesto2
        '
        Me.Impuesto2.HeaderText = "Impuesto 2%"
        Me.Impuesto2.Name = "Impuesto2"
        Me.Impuesto2.ReadOnly = True
        '
        'Subtotal4
        '
        Me.Subtotal4.HeaderText = "Subtotal 4%"
        Me.Subtotal4.Name = "Subtotal4"
        Me.Subtotal4.ReadOnly = True
        '
        'Impuesto4
        '
        Me.Impuesto4.HeaderText = "Impuesto 4%"
        Me.Impuesto4.Name = "Impuesto4"
        Me.Impuesto4.ReadOnly = True
        '
        'Subtotal8
        '
        Me.Subtotal8.HeaderText = "Subtotal 8%"
        Me.Subtotal8.Name = "Subtotal8"
        Me.Subtotal8.ReadOnly = True
        '
        'Impuesto8
        '
        Me.Impuesto8.HeaderText = "Impuesto 8%"
        Me.Impuesto8.Name = "Impuesto8"
        Me.Impuesto8.ReadOnly = True
        '
        'SubTotal13
        '
        Me.SubTotal13.DataPropertyName = "SubTotal"
        Me.SubTotal13.HeaderText = "SubTotal 13%"
        Me.SubTotal13.Name = "SubTotal13"
        Me.SubTotal13.ReadOnly = True
        '
        'Impuesto13
        '
        Me.Impuesto13.DataPropertyName = "Impuesto"
        Me.Impuesto13.HeaderText = "Impuesto 13%"
        Me.Impuesto13.Name = "Impuesto13"
        Me.Impuesto13.ReadOnly = True
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        '
        'DocumentoDataGridViewTextBoxColumn
        '
        Me.DocumentoDataGridViewTextBoxColumn.DataPropertyName = "Documento"
        Me.DocumentoDataGridViewTextBoxColumn.HeaderText = "Documento"
        Me.DocumentoDataGridViewTextBoxColumn.Name = "DocumentoDataGridViewTextBoxColumn"
        Me.DocumentoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TipoDataGridViewTextBoxColumn
        '
        Me.TipoDataGridViewTextBoxColumn.DataPropertyName = "Tipo"
        Me.TipoDataGridViewTextBoxColumn.HeaderText = "Tipo"
        Me.TipoDataGridViewTextBoxColumn.Name = "TipoDataGridViewTextBoxColumn"
        Me.TipoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FechaDataGridViewTextBoxColumn
        '
        Me.FechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha"
        Me.FechaDataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FechaDataGridViewTextBoxColumn.Name = "FechaDataGridViewTextBoxColumn"
        Me.FechaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ClienteDataGridViewTextBoxColumn
        '
        Me.ClienteDataGridViewTextBoxColumn.DataPropertyName = "Cliente"
        Me.ClienteDataGridViewTextBoxColumn.HeaderText = "Cliente"
        Me.ClienteDataGridViewTextBoxColumn.Name = "ClienteDataGridViewTextBoxColumn"
        Me.ClienteDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ImpuestoDataGridViewTextBoxColumn
        '
        Me.ImpuestoDataGridViewTextBoxColumn.DataPropertyName = "Impuesto"
        Me.ImpuestoDataGridViewTextBoxColumn.HeaderText = "Impuesto"
        Me.ImpuestoDataGridViewTextBoxColumn.Name = "ImpuestoDataGridViewTextBoxColumn"
        Me.ImpuestoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SubTotalDataGridViewTextBoxColumn
        '
        Me.SubTotalDataGridViewTextBoxColumn.DataPropertyName = "SubTotal"
        Me.SubTotalDataGridViewTextBoxColumn.HeaderText = "SubTotal"
        Me.SubTotalDataGridViewTextBoxColumn.Name = "SubTotalDataGridViewTextBoxColumn"
        Me.SubTotalDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TotalDataGridViewTextBoxColumn
        '
        Me.TotalDataGridViewTextBoxColumn.DataPropertyName = "Total"
        Me.TotalDataGridViewTextBoxColumn.HeaderText = "Total"
        Me.TotalDataGridViewTextBoxColumn.Name = "TotalDataGridViewTextBoxColumn"
        Me.TotalDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PorcentajeImpuestoDataGridViewTextBoxColumn
        '
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn.DataPropertyName = "PorcentajeImpuesto"
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn.HeaderText = "PorcentajeImpuesto"
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn.Name = "PorcentajeImpuestoDataGridViewTextBoxColumn"
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DocumentoDataGridViewTextBoxColumn1
        '
        Me.DocumentoDataGridViewTextBoxColumn1.Name = "DocumentoDataGridViewTextBoxColumn1"
        '
        'TipoDataGridViewTextBoxColumn1
        '
        Me.TipoDataGridViewTextBoxColumn1.Name = "TipoDataGridViewTextBoxColumn1"
        '
        'FechaDataGridViewTextBoxColumn1
        '
        Me.FechaDataGridViewTextBoxColumn1.Name = "FechaDataGridViewTextBoxColumn1"
        '
        'ClienteDataGridViewTextBoxColumn1
        '
        Me.ClienteDataGridViewTextBoxColumn1.Name = "ClienteDataGridViewTextBoxColumn1"
        '
        'ImpuestoDataGridViewTextBoxColumn1
        '
        Me.ImpuestoDataGridViewTextBoxColumn1.Name = "ImpuestoDataGridViewTextBoxColumn1"
        '
        'SubTotalDataGridViewTextBoxColumn1
        '
        Me.SubTotalDataGridViewTextBoxColumn1.Name = "SubTotalDataGridViewTextBoxColumn1"
        '
        'TotalDataGridViewTextBoxColumn1
        '
        Me.TotalDataGridViewTextBoxColumn1.Name = "TotalDataGridViewTextBoxColumn1"
        '
        'PorcentajeImpuestoDataGridViewTextBoxColumn1
        '
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn1.Name = "PorcentajeImpuestoDataGridViewTextBoxColumn1"
        '
        'LblSubTotalResumenCompras
        '
        Me.LblSubTotalResumenCompras.AutoSize = True
        Me.LblSubTotalResumenCompras.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSubTotalResumenCompras.Location = New System.Drawing.Point(445, 198)
        Me.LblSubTotalResumenCompras.Name = "LblSubTotalResumenCompras"
        Me.LblSubTotalResumenCompras.Size = New System.Drawing.Size(25, 13)
        Me.LblSubTotalResumenCompras.TabIndex = 12
        Me.LblSubTotalResumenCompras.Text = "0.0"
        Me.LblSubTotalResumenCompras.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LblImpuestoResumenCompras
        '
        Me.LblImpuestoResumenCompras.AutoSize = True
        Me.LblImpuestoResumenCompras.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImpuestoResumenCompras.Location = New System.Drawing.Point(591, 198)
        Me.LblImpuestoResumenCompras.Name = "LblImpuestoResumenCompras"
        Me.LblImpuestoResumenCompras.Size = New System.Drawing.Size(25, 13)
        Me.LblImpuestoResumenCompras.TabIndex = 13
        Me.LblImpuestoResumenCompras.Text = "0.0"
        '
        'dgvResumenVentas
        '
        Me.dgvResumenVentas.AllowUserToAddRows = False
        Me.dgvResumenVentas.AllowUserToDeleteRows = False
        Me.dgvResumenVentas.AutoGenerateColumns = False
        Me.dgvResumenVentas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvResumenVentas.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvResumenVentas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvResumenVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResumenVentas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PorcentajeImpuestoDataGridViewTextBoxColumn2, Me.SubTotalDataGridViewTextBoxColumn2, Me.ImpuestoDataGridViewTextBoxColumn2})
        Me.dgvResumenVentas.DataSource = Me.ResumenVentasBS
        Me.dgvResumenVentas.Location = New System.Drawing.Point(788, 44)
        Me.dgvResumenVentas.Name = "dgvResumenVentas"
        Me.dgvResumenVentas.ReadOnly = True
        Me.dgvResumenVentas.RowHeadersVisible = False
        Me.dgvResumenVentas.Size = New System.Drawing.Size(375, 150)
        Me.dgvResumenVentas.TabIndex = 14
        '
        'PorcentajeImpuestoDataGridViewTextBoxColumn2
        '
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn2.DataPropertyName = "PorcentajeImpuesto"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle11
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn2.HeaderText = "Porcentaje Impuesto"
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn2.Name = "PorcentajeImpuestoDataGridViewTextBoxColumn2"
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn2.ReadOnly = True
        Me.PorcentajeImpuestoDataGridViewTextBoxColumn2.Width = 135
        '
        'SubTotalDataGridViewTextBoxColumn2
        '
        Me.SubTotalDataGridViewTextBoxColumn2.DataPropertyName = "SubTotal"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.SubTotalDataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle12
        Me.SubTotalDataGridViewTextBoxColumn2.HeaderText = "SubTotal"
        Me.SubTotalDataGridViewTextBoxColumn2.Name = "SubTotalDataGridViewTextBoxColumn2"
        Me.SubTotalDataGridViewTextBoxColumn2.ReadOnly = True
        Me.SubTotalDataGridViewTextBoxColumn2.Width = 83
        '
        'ImpuestoDataGridViewTextBoxColumn2
        '
        Me.ImpuestoDataGridViewTextBoxColumn2.DataPropertyName = "Impuesto"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.ImpuestoDataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle13
        Me.ImpuestoDataGridViewTextBoxColumn2.HeaderText = "Impuesto"
        Me.ImpuestoDataGridViewTextBoxColumn2.Name = "ImpuestoDataGridViewTextBoxColumn2"
        Me.ImpuestoDataGridViewTextBoxColumn2.ReadOnly = True
        Me.ImpuestoDataGridViewTextBoxColumn2.Width = 83
        '
        'ResumenVentasBS
        '
        Me.ResumenVentasBS.DataMember = "ResumenVentas"
        Me.ResumenVentasBS.DataSource = Me.datos
        '
        'LblSubTotalResumenVentas
        '
        Me.LblSubTotalResumenVentas.AutoSize = True
        Me.LblSubTotalResumenVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSubTotalResumenVentas.Location = New System.Drawing.Point(908, 197)
        Me.LblSubTotalResumenVentas.Name = "LblSubTotalResumenVentas"
        Me.LblSubTotalResumenVentas.Size = New System.Drawing.Size(25, 13)
        Me.LblSubTotalResumenVentas.TabIndex = 15
        Me.LblSubTotalResumenVentas.Text = "0.0"
        '
        'LblImpuestoResumenVentas
        '
        Me.LblImpuestoResumenVentas.AutoSize = True
        Me.LblImpuestoResumenVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImpuestoResumenVentas.Location = New System.Drawing.Point(1055, 197)
        Me.LblImpuestoResumenVentas.Name = "LblImpuestoResumenVentas"
        Me.LblImpuestoResumenVentas.Size = New System.Drawing.Size(25, 13)
        Me.LblImpuestoResumenVentas.TabIndex = 16
        Me.LblImpuestoResumenVentas.Text = "0.0"
        '
        'DocumentoDataGridViewTextBoxColumn2
        '
        Me.DocumentoDataGridViewTextBoxColumn2.DataPropertyName = "Documento"
        Me.DocumentoDataGridViewTextBoxColumn2.HeaderText = "Documento"
        Me.DocumentoDataGridViewTextBoxColumn2.Name = "DocumentoDataGridViewTextBoxColumn2"
        Me.DocumentoDataGridViewTextBoxColumn2.ReadOnly = True
        '
        'TipoDataGridViewTextBoxColumn2
        '
        Me.TipoDataGridViewTextBoxColumn2.DataPropertyName = "Tipo"
        Me.TipoDataGridViewTextBoxColumn2.HeaderText = "Tipo"
        Me.TipoDataGridViewTextBoxColumn2.Name = "TipoDataGridViewTextBoxColumn2"
        Me.TipoDataGridViewTextBoxColumn2.ReadOnly = True
        '
        'FechaDataGridViewTextBoxColumn2
        '
        Me.FechaDataGridViewTextBoxColumn2.DataPropertyName = "Fecha"
        Me.FechaDataGridViewTextBoxColumn2.HeaderText = "Fecha"
        Me.FechaDataGridViewTextBoxColumn2.Name = "FechaDataGridViewTextBoxColumn2"
        Me.FechaDataGridViewTextBoxColumn2.ReadOnly = True
        '
        'ClienteDataGridViewTextBoxColumn2
        '
        Me.ClienteDataGridViewTextBoxColumn2.DataPropertyName = "Cliente"
        Me.ClienteDataGridViewTextBoxColumn2.HeaderText = "Cliente"
        Me.ClienteDataGridViewTextBoxColumn2.Name = "ClienteDataGridViewTextBoxColumn2"
        Me.ClienteDataGridViewTextBoxColumn2.ReadOnly = True
        '
        'TotalDataGridViewTextBoxColumn2
        '
        Me.TotalDataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.TotalDataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.TotalDataGridViewTextBoxColumn2.Name = "TotalDataGridViewTextBoxColumn2"
        Me.TotalDataGridViewTextBoxColumn2.ReadOnly = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(482, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 24)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Compras"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(964, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 24)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Ventas"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(350, 197)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Totales:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(806, 198)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Totales:"
        '
        'bwDatos
        '
        Me.bwDatos.WorkerReportsProgress = True
        '
        'TabControlVC
        '
        Me.TabControlVC.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControlVC.Controls.Add(Me.TabPage1)
        Me.TabControlVC.Controls.Add(Me.TabPage2)
        Me.TabControlVC.Location = New System.Drawing.Point(12, 226)
        Me.TabControlVC.Name = "TabControlVC"
        Me.TabControlVC.SelectedIndex = 0
        Me.TabControlVC.Size = New System.Drawing.Size(1199, 298)
        Me.TabControlVC.TabIndex = 23
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btExportarCompras)
        Me.TabPage1.Controls.Add(Me.panelComprasTotales)
        Me.TabPage1.Controls.Add(Me.dgvDetalleCompras)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1191, 272)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Compras"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btExportarCompras
        '
        Me.btExportarCompras.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btExportarCompras.Location = New System.Drawing.Point(1002, 3)
        Me.btExportarCompras.Name = "btExportarCompras"
        Me.btExportarCompras.Size = New System.Drawing.Size(110, 36)
        Me.btExportarCompras.TabIndex = 25
        Me.btExportarCompras.Text = "Excel"
        Me.btExportarCompras.UseVisualStyleBackColor = True
        '
        'panelComprasTotales
        '
        Me.panelComprasTotales.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelComprasTotales.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.panelComprasTotales.Location = New System.Drawing.Point(993, 45)
        Me.panelComprasTotales.Name = "panelComprasTotales"
        Me.panelComprasTotales.Size = New System.Drawing.Size(187, 220)
        Me.panelComprasTotales.TabIndex = 24
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btExportarVentas)
        Me.TabPage2.Controls.Add(Me.panelVentasTotales)
        Me.TabPage2.Controls.Add(Me.dgvDetalleVentas)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1191, 272)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Ventas"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btExportarVentas
        '
        Me.btExportarVentas.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btExportarVentas.Location = New System.Drawing.Point(1007, 3)
        Me.btExportarVentas.Name = "btExportarVentas"
        Me.btExportarVentas.Size = New System.Drawing.Size(110, 36)
        Me.btExportarVentas.TabIndex = 26
        Me.btExportarVentas.Text = "Excel"
        Me.btExportarVentas.UseVisualStyleBackColor = True
        '
        'panelVentasTotales
        '
        Me.panelVentasTotales.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelVentasTotales.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.panelVentasTotales.Location = New System.Drawing.Point(994, 45)
        Me.panelVentasTotales.Name = "panelVentasTotales"
        Me.panelVentasTotales.Size = New System.Drawing.Size(191, 227)
        Me.panelVentasTotales.TabIndex = 25
        '
        'dgvDetalleVentas
        '
        Me.dgvDetalleVentas.AllowUserToAddRows = False
        Me.dgvDetalleVentas.AllowUserToDeleteRows = False
        Me.dgvDetalleVentas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDetalleVentas.AutoGenerateColumns = False
        Me.dgvDetalleVentas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvDetalleVentas.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleVentas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvDetalleVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleVentas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.dgvDetalleVentas.DataSource = Me.dgvDetalleBSVentas
        Me.dgvDetalleVentas.Location = New System.Drawing.Point(0, 0)
        Me.dgvDetalleVentas.Name = "dgvDetalleVentas"
        Me.dgvDetalleVentas.ReadOnly = True
        Me.dgvDetalleVentas.RowHeadersVisible = False
        Me.dgvDetalleVentas.Size = New System.Drawing.Size(988, 272)
        Me.dgvDetalleVentas.TabIndex = 5
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Documento"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Documento"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 77
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Tipo"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Tipo"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 38
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Fecha"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 48
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Cliente"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Cliente"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 52
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "SubTotal"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.Format = "N2"
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn6.HeaderText = "SubTotal"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 64
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Impuesto"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Format = "N2"
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridViewTextBoxColumn7.HeaderText = "Impuesto"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 64
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Total"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle17.Format = "N2"
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle17
        Me.DataGridViewTextBoxColumn8.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 42
        '
        'gbAnalisisVenta
        '
        Me.gbAnalisisVenta.Controls.Add(Me.rbGeneral)
        Me.gbAnalisisVenta.Controls.Add(Me.rbNoTributable)
        Me.gbAnalisisVenta.Controls.Add(Me.rbTributable)
        Me.gbAnalisisVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAnalisisVenta.Location = New System.Drawing.Point(16, 117)
        Me.gbAnalisisVenta.Name = "gbAnalisisVenta"
        Me.gbAnalisisVenta.Size = New System.Drawing.Size(299, 47)
        Me.gbAnalisisVenta.TabIndex = 25
        Me.gbAnalisisVenta.TabStop = False
        Me.gbAnalisisVenta.Text = "Analisis de venta"
        Me.gbAnalisisVenta.Visible = False
        '
        'rbGeneral
        '
        Me.rbGeneral.AutoSize = True
        Me.rbGeneral.Checked = True
        Me.rbGeneral.Location = New System.Drawing.Point(220, 20)
        Me.rbGeneral.Name = "rbGeneral"
        Me.rbGeneral.Size = New System.Drawing.Size(73, 17)
        Me.rbGeneral.TabIndex = 2
        Me.rbGeneral.TabStop = True
        Me.rbGeneral.Text = "General."
        Me.rbGeneral.UseVisualStyleBackColor = True
        '
        'rbNoTributable
        '
        Me.rbNoTributable.AutoSize = True
        Me.rbNoTributable.Location = New System.Drawing.Point(112, 20)
        Me.rbNoTributable.Name = "rbNoTributable"
        Me.rbNoTributable.Size = New System.Drawing.Size(102, 17)
        Me.rbNoTributable.TabIndex = 1
        Me.rbNoTributable.Text = "No tributable."
        Me.rbNoTributable.UseVisualStyleBackColor = True
        '
        'rbTributable
        '
        Me.rbTributable.AutoSize = True
        Me.rbTributable.Location = New System.Drawing.Point(13, 20)
        Me.rbTributable.Name = "rbTributable"
        Me.rbTributable.Size = New System.Drawing.Size(86, 17)
        Me.rbTributable.TabIndex = 0
        Me.rbTributable.Text = "Tributable."
        Me.rbTributable.UseVisualStyleBackColor = True
        '
        'frmInformeActividadEconomica
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1223, 525)
        Me.Controls.Add(Me.gbAnalisisVenta)
        Me.Controls.Add(Me.TabControlVC)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LblImpuestoResumenVentas)
        Me.Controls.Add(Me.LblSubTotalResumenVentas)
        Me.Controls.Add(Me.dgvResumenVentas)
        Me.Controls.Add(Me.LblImpuestoResumenCompras)
        Me.Controls.Add(Me.LblSubTotalResumenCompras)
        Me.Controls.Add(Me.lblActividadEconimica)
        Me.Controls.Add(Me.lblPeriodo)
        Me.Controls.Add(Me.dgvResumen)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnGenerar)
        Me.Controls.Add(Me.cmbActividadEconomica)
        Me.Controls.Add(Me.cmbMes)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmInformeActividadEconomica"
        Me.Text = "Reporte para D104 - IVA por Actividad Económica."
        CType(Me.enlaceNumeroPeriodo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.datos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.enlaceActividadEconomica, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvResumen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResumenComprasBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetalleCompras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetalleBSCompras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetalleBSVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvResumenVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResumenVentasBS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlVC.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvDetalleVentas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbAnalisisVenta.ResumeLayout(False)
        Me.gbAnalisisVenta.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmbMes As Windows.Forms.ComboBox
    Friend WithEvents cmbActividadEconomica As Windows.Forms.ComboBox
    Friend WithEvents btnGenerar As Windows.Forms.Button
    Friend WithEvents btnImprimir As Windows.Forms.Button
    Friend WithEvents dgvResumen As Windows.Forms.DataGridView
    Friend WithEvents enlaceActividadEconomica As Windows.Forms.BindingSource
    Friend WithEvents lblPeriodo As Windows.Forms.Label
    Friend WithEvents lblActividadEconimica As Windows.Forms.Label
    Friend WithEvents datos As dstActividadEconomica
    Friend WithEvents enlaceNumeroPeriodo As Windows.Forms.BindingSource
    Friend WithEvents dgvDetalleCompras As Windows.Forms.DataGridView
    Friend WithEvents dgvDetalleBSVentas As Windows.Forms.BindingSource
    Friend WithEvents DocumentoDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClienteDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpuestoDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotalDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajeImpuestoDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocumentoDataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoDataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClienteDataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpuestoDataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotalDataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajeImpuestoDataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Documento As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cliente As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotal As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotal1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Subtotal2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Subtotal4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Subtotal8 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto8 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotal13 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto13 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocumentoDataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoDataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClienteDataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ResumenComprasBS As Windows.Forms.BindingSource
    Friend WithEvents LblSubTotalResumenCompras As Windows.Forms.Label
    Friend WithEvents LblImpuestoResumenCompras As Windows.Forms.Label
    Friend WithEvents dgvResumenVentas As Windows.Forms.DataGridView
    Friend WithEvents ResumenVentasBS As Windows.Forms.BindingSource
    Friend WithEvents LblSubTotalResumenVentas As Windows.Forms.Label
    Friend WithEvents LblImpuestoResumenVentas As Windows.Forms.Label
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents Label3 As Windows.Forms.Label
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents bwDatos As ComponentModel.BackgroundWorker
    Friend WithEvents TabControlVC As Windows.Forms.TabControl
    Friend WithEvents TabPage1 As Windows.Forms.TabPage
    Friend WithEvents panelComprasTotales As Windows.Forms.FlowLayoutPanel
    Friend WithEvents TabPage2 As Windows.Forms.TabPage
    Friend WithEvents dgvDetalleVentas As Windows.Forms.DataGridView
    Friend WithEvents panelVentasTotales As Windows.Forms.FlowLayoutPanel
    Friend WithEvents dgvDetalleBSCompras As Windows.Forms.BindingSource
    Friend WithEvents DocumentoDataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoDataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProveedorDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotalDataGridViewTextBoxColumn4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpuestoDataGridViewTextBoxColumn4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajeImpuestoResumenCompras As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotalDataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpuestoDataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajeImpuestoDataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotalDataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpuestoDataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btExportarCompras As Windows.Forms.Button
    Friend WithEvents btExportarVentas As Windows.Forms.Button
    Friend WithEvents gbAnalisisVenta As Windows.Forms.GroupBox
    Friend WithEvents rbGeneral As Windows.Forms.RadioButton
    Friend WithEvents rbNoTributable As Windows.Forms.RadioButton
    Friend WithEvents rbTributable As Windows.Forms.RadioButton
End Class
