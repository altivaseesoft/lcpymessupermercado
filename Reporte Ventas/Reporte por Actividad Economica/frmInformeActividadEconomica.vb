﻿
Imports System.Data
Imports System.Windows.Forms
Imports System.Drawing

Public Class frmInformeActividadEconomica
    Friend WithEvents dstActividadEconomica As dstActividadEconomica


    Private Sub frmInformeActividadEconomica_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarCombx()
        ObtenerPermisos()
        cmbActividadEconomica.SelectedIndex = 0
        cmbMes.SelectedIndex = 0
    End Sub

    Function ObtenerPermisos() As Boolean
        Try
            Dim dtPermisos As New DataTable
            Dim UsaAnalisis As Boolean = False
            Dim TipoUsuario As Boolean = False
            cFunciones.Llenar_Tabla_Generico("Select top 1 UsaAnalisisVenta from Configuraciones", dtPermisos, GetSetting("SeeSOFT", "SEEPOS", "CONEXION"))

            If dtPermisos.Rows.Count > 0 Then
                UsaAnalisis = dtPermisos.Rows(0).ItemArray(0)
            End If

            dtPermisos.Clear()
            dtPermisos.Columns.Clear()
            dtPermisos.Rows.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Gerencial from Usuarios where Id_Usuario='" & _IdUsuario & "'", dtPermisos, GetSetting("SeeSOFT", "Seguridad", "CONEXION"))
            If dtPermisos.Rows.Count > 0 Then
                TipoUsuario = dtPermisos.Rows(0).ItemArray(0)
            End If

            If UsaAnalisis = True And TipoUsuario = True Then
                gbAnalisisVenta.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Sub CargarCombx()
        InformeActividadEconomicaControlador.cargarComboActivdadEconomica(Me.datos.TB_CE_ActividadEconomica)
        InformeActividadEconomicaControlador.cargarComboPeriodo(Me.datos.vs_NumeroDePeriodoANombre)
    End Sub

    Private Sub LoadDataGrids(sender As Object, e As EventArgs) Handles btnGenerar.Click
        GenerarClic()
    End Sub


    Private ActividadSeleccionada As Integer = 0
    Private PeriodoSeleccionado As Integer = 0
    Private nombreUsuario As String
    Dim _IdUsuario As String
    Public Sub New(_nombreUsuario As String, IdUsuario As String)
        InitializeComponent()
        nombreUsuario = _nombreUsuario
        _IdUsuario = IdUsuario
    End Sub

    Private Sub GenerarClic()
        Dim vacio As New dstActividadEconomica
        ActividadSeleccionada = Me.cmbActividadEconomica.SelectedValue
        PeriodoSeleccionado = Me.cmbMes.SelectedValue
        dgvDetalleBSVentas.DataSource = vacio
        dgvDetalleBSVentas.DataMember = vacio.vs_detalle_mostrar.TableName

        dgvDetalleBSCompras.DataSource = vacio
        dgvDetalleBSCompras.DataMember = vacio.vs_detalle_mostrarCompras.TableName

        ResumenComprasBS.DataSource = vacio
        ResumenComprasBS.DataMember = vacio.ResumenCompras.TableName

        ResumenVentasBS.DataSource = vacio
        ResumenVentasBS.DataMember = vacio.ResumenVentas.TableName
        btnGenerar.Enabled = False
        btnGenerar.Text = "Cargando..."
        If Not bwDatos.IsBusy Then
            bwDatos.RunWorkerAsync()
        End If
    End Sub


    Private Sub bwDatos_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwDatos.DoWork
        IniciarCargar()
    End Sub
    Private Sub IniciarCargar()
        Generar()
    End Sub

    Private Sub Generar()
        InformeActividadEconomicaControlador.cargarDgvResumenCompras(Me.datos.ResumenCompras, ActividadSeleccionada, PeriodoSeleccionado)

        If gbAnalisisVenta.Visible = True Then
            If rbTributable.Checked Then
                InformeActividadEconomicaControlador.cargarDgvResumenVentas(Me.datos.ResumenVentas, ActividadSeleccionada, PeriodoSeleccionado, 1)
                InformeActividadEconomicaControlador.cargarDgvDetalleVentas(Me.datos.vs_detalle_ventas, ActividadSeleccionada, PeriodoSeleccionado, 1)
                InformeActividadEconomicaControlador.cargarDgvDetalleVentasMostrar(Me.datos.vs_detalle_mostrar, ActividadSeleccionada, PeriodoSeleccionado, 1)
            ElseIf rbNoTributable.Checked Then
                InformeActividadEconomicaControlador.cargarDgvResumenVentas(Me.datos.ResumenVentas, ActividadSeleccionada, PeriodoSeleccionado, 0)
                InformeActividadEconomicaControlador.cargarDgvDetalleVentas(Me.datos.vs_detalle_ventas, ActividadSeleccionada, PeriodoSeleccionado, 0)
                InformeActividadEconomicaControlador.cargarDgvDetalleVentasMostrar(Me.datos.vs_detalle_mostrar, ActividadSeleccionada, PeriodoSeleccionado, 0)
            Else
                InformeActividadEconomicaControlador.cargarDgvResumenVentas(Me.datos.ResumenVentas, ActividadSeleccionada, PeriodoSeleccionado, -1)
                InformeActividadEconomicaControlador.cargarDgvDetalleVentas(Me.datos.vs_detalle_ventas, ActividadSeleccionada, PeriodoSeleccionado, -1)
                InformeActividadEconomicaControlador.cargarDgvDetalleVentasMostrar(Me.datos.vs_detalle_mostrar, ActividadSeleccionada, PeriodoSeleccionado, -1)
            End If
        Else
            InformeActividadEconomicaControlador.cargarDgvResumenVentas(Me.datos.ResumenVentas, ActividadSeleccionada, PeriodoSeleccionado, -1)
            InformeActividadEconomicaControlador.cargarDgvDetalleVentas(Me.datos.vs_detalle_ventas, ActividadSeleccionada, PeriodoSeleccionado, -1)
            InformeActividadEconomicaControlador.cargarDgvDetalleVentasMostrar(Me.datos.vs_detalle_mostrar, ActividadSeleccionada, PeriodoSeleccionado, -1)
        End If

        InformeActividadEconomicaControlador.cargarDgvDetalleCompras(Me.datos.vs_detalle_compras, ActividadSeleccionada, PeriodoSeleccionado)
        InformeActividadEconomicaControlador.cargarDgvDetalleComprasMostrar(Me.datos.vs_detalle_mostrarCompras, ActividadSeleccionada, PeriodoSeleccionado)
    End Sub

    Private Sub bwDatos_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwDatos.ProgressChanged
        InformarProgreso()
    End Sub
    Private Sub InformarProgreso()

    End Sub

    Private Sub bwDatos_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwDatos.RunWorkerCompleted
        TerminarProceso()
    End Sub
    Private Sub CrearTotalesCompras(porcNombre As String)
        Dim etiqueta As Label
        etiqueta = New Label()
        etiqueta.Name = "EtiquetaSubTotal" & porcNombre & ""

        etiqueta.AutoSize = True
        etiqueta.TabIndex = 22
        etiqueta.Text = "SubTotal " & porcNombre & "%"
        etiqueta.Font = New Font("Arial", 11, FontStyle.Bold)
        panelComprasTotales.Controls.Add(etiqueta)

        Dim total As TextBox
        total = New TextBox()
        total.Name = "txtSubTotal" & porcNombre & ""
        total.Tag = "SubTotal;" & porcNombre & ""
        total.Size = New System.Drawing.Size(100, 13)
        total.TabIndex = 23
        total.TextAlign = ToolBarTextAlign.Right
        total.Text = 0
        panelComprasTotales.Controls.Add(total)


        etiqueta = New Label()
        etiqueta.Name = "EtiquetaImpuesto" & porcNombre & ""
        etiqueta.AutoSize = True
        etiqueta.TabIndex = 24
        etiqueta.Text = "Impuesto" & porcNombre & "%"
        etiqueta.Font = New Font("Arial", 11, FontStyle.Bold)
        panelComprasTotales.Controls.Add(etiqueta)

        total = New TextBox()
        total.Name = "txtImpuesto" & porcNombre & ""
        total.Tag = "Impuesto;" & porcNombre & ""
        total.Size = New System.Drawing.Size(100, 13)
        total.TabIndex = 25
        total.Text = 0
        total.TextAlign = ToolBarTextAlign.Right
        panelComprasTotales.Controls.Add(total)
    End Sub

    Private Sub CrearTotalesVentas(porcNombre As String)
        Dim etiqueta As Label
        etiqueta = New Label()
        etiqueta.Name = "EtiquetaSubTotalV" & porcNombre & ""
        etiqueta.AutoSize = True
        etiqueta.TabIndex = 22
        etiqueta.Text = "SubTotal " & porcNombre & "%"
        etiqueta.Font = New Font("Arial", 11, FontStyle.Bold)
        panelVentasTotales.Controls.Add(etiqueta)

        Dim total As TextBox
        total = New TextBox()
        total.Name = "txtSubTotalV" & porcNombre & ""
        total.Tag = "SubTotal;" & porcNombre & ""
        total.Size = New System.Drawing.Size(100, 13)
        total.TabIndex = 23
        total.TextAlign = ToolBarTextAlign.Right
        total.Text = 0
        panelVentasTotales.Controls.Add(total)


        etiqueta = New Label()
        etiqueta.Name = "EtiquetaImpuesto" & porcNombre & ""
        etiqueta.AutoSize = True
        etiqueta.TabIndex = 24
        etiqueta.Text = "Impuesto " & porcNombre & "%"
        etiqueta.Font = New Font("Arial", 11, FontStyle.Bold)
        panelVentasTotales.Controls.Add(etiqueta)

        total = New TextBox()
        total.Name = "txtImpuestoV" & porcNombre & ""
        total.Tag = "Impuesto;" & porcNombre & ""
        total.Size = New System.Drawing.Size(100, 13)
        total.TabIndex = 25
        total.Text = 0
        total.TextAlign = ToolBarTextAlign.Right
        panelVentasTotales.Controls.Add(total)
    End Sub

    Private Sub TerminarProceso()
        Dim ImpuestoV As Double
        Dim SubTotalV As Double
        Dim filaV As String
        Dim ImpuestoC As Double
        Dim SubTotalC As Double
        Dim filaC As String
        dgvDetalleBSVentas.DataSource = datos
        dgvDetalleBSVentas.DataMember = datos.vs_detalle_mostrar.TableName
        ResumenVentasBS.DataSource = datos
        ResumenVentasBS.DataMember = datos.ResumenVentas.TableName
        dgvDetalleBSCompras.DataSource = datos
        dgvDetalleBSCompras.DataMember = datos.vs_detalle_mostrarCompras.TableName
        ResumenComprasBS.DataSource = datos
        ResumenComprasBS.DataMember = datos.ResumenCompras.TableName
        panelComprasTotales.Controls.Clear()
        panelVentasTotales.Controls.Clear()

        For Each linea As dstActividadEconomica.ResumenVentasRow In datos.ResumenVentas
            Dim porcNombre As String = linea.PorcentajeImpuesto
            If dgvDetalleVentas.Columns.Contains("SubTotalV" & porcNombre) Then
                dgvDetalleVentas.Columns.Remove("SubTotalV" & porcNombre)
            End If
            Dim estilo As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
            estilo.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
            estilo.Format = "N2"
            Dim columna As Windows.Forms.DataGridViewTextBoxColumn
            columna = New System.Windows.Forms.DataGridViewTextBoxColumn()
            columna.Name = "SubTotalV" & porcNombre
            columna.DefaultCellStyle = estilo
            columna.HeaderText = "SubTotal" & porcNombre & "%"
            columna.ReadOnly = True
            columna.Width = 95
            dgvDetalleVentas.Columns.Add(columna)

            If dgvDetalleVentas.Columns.Contains("ImpuestoV" & porcNombre) Then
                dgvDetalleVentas.Columns.Remove("ImpuestoV" & porcNombre)
            End If
            columna = New System.Windows.Forms.DataGridViewTextBoxColumn()
            columna.Name = "ImpuestoV" & porcNombre
            columna.DefaultCellStyle = estilo
            columna.HeaderText = "Impuesto" & porcNombre & "%"
            columna.ReadOnly = True
            columna.Width = 95
            dgvDetalleVentas.Columns.Add(columna)

            CrearTotalesVentas(porcNombre)
        Next

        For Each linea As dstActividadEconomica.ResumenComprasRow In datos.ResumenCompras
            Dim porcNombre As String = linea.PorcentajeImpuesto

            If dgvDetalleCompras.Columns.Contains("SubTotal" & porcNombre) Then
                dgvDetalleCompras.Columns.Remove("SubTotal" & porcNombre)
            End If
            Dim estilo As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
            estilo.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
            estilo.Format = "N2"
            Dim columna As Windows.Forms.DataGridViewTextBoxColumn
            columna = New System.Windows.Forms.DataGridViewTextBoxColumn()
            columna.Name = "SubTotal" & porcNombre
            columna.DefaultCellStyle = estilo
            columna.HeaderText = "SubTotal " & porcNombre & "%"
            columna.ReadOnly = True
            columna.Width = 95
            dgvDetalleCompras.Columns.Add(columna)

            If dgvDetalleCompras.Columns.Contains("Impuesto" & porcNombre) Then
                dgvDetalleCompras.Columns.Remove("Impuesto" & porcNombre)
            End If
            columna = New System.Windows.Forms.DataGridViewTextBoxColumn()
            columna.Name = "Impuesto" & porcNombre
            columna.DefaultCellStyle = estilo
            columna.HeaderText = "Impuesto " & porcNombre & "%"
            columna.ReadOnly = True
            columna.Width = 95
            dgvDetalleCompras.Columns.Add(columna)
            CrearTotalesCompras(porcNombre)

        Next
        TabControlVC.SelectedIndex = 0
        TabControlVC.SelectedIndex = 1
        For i As Integer = 0 To Me.datos.Tables("vs_detalle_ventas").Rows().Count - 1

            For fi As Integer = 0 To Me.datos.vs_detalle_mostrar.Count - 1
                If datos.vs_detalle_mostrar(fi).Documento.Equals(datos.vs_detalle_ventas(i).Documento) And datos.vs_detalle_mostrar(fi).Tipo.Equals(datos.vs_detalle_ventas(i).Tipo) Then
                    filaV = Me.datos.Tables("vs_detalle_ventas").Rows(i).Item("PorcentajeImpuesto").ToString
                    ImpuestoV = Me.datos.Tables("vs_detalle_ventas").Rows(i).Item("Impuesto")
                    SubTotalV = Me.datos.Tables("vs_detalle_ventas").Rows(i).Item("SubTotal")
                    dgvDetalleVentas.Rows(fi).Cells("SubTotalV" & filaV).Value += Math.Abs(SubTotalV)
                    dgvDetalleVentas.Rows(fi).Cells("ImpuestoV" & filaV).Value += Math.Abs(ImpuestoV)
                    Dim txtS As TextBox = ControlTotalVentas("txtSubTotalV" & filaV)
                    txtS.Text += SubTotalV

                    Dim txtI As TextBox = ControlTotalVentas("txtImpuestoV" & filaV)
                    txtI.Text += ImpuestoV

                End If

            Next
        Next
        dgvDetalleVentas.Refresh()
        TabControlVC.SelectedIndex = 0

        For i As Integer = 0 To Me.datos.Tables("vs_detalle_compras").Rows().Count - 1

            For fi As Integer = 0 To Me.datos.vs_detalle_mostrarCompras.Count - 1
                If datos.vs_detalle_mostrarCompras(fi).Documento.Equals(datos.vs_detalle_compras(i).Documento) And datos.vs_detalle_mostrarCompras(fi).Tipo.Equals(datos.vs_detalle_compras(i).Tipo) Then
                    filaC = Me.datos.Tables("vs_detalle_compras").Rows(i).Item("PorcentajeImpuesto").ToString
                    ImpuestoC = Me.datos.Tables("vs_detalle_compras").Rows(i).Item("Impuesto")
                    SubTotalC = Me.datos.Tables("vs_detalle_compras").Rows(i).Item("SubTotal")
                    dgvDetalleCompras.Rows(fi).Cells("SubTotal" & filaC).Value += Math.Abs(SubTotalC)
                    dgvDetalleCompras.Rows(fi).Cells("Impuesto" & filaC).Value += Math.Abs(ImpuestoC)
                    Dim txtS As TextBox = ControlTotalCompras("txtSubTotal" & filaC)
                    txtS.Text += SubTotalC

                    Dim txtI As TextBox = ControlTotalCompras("txtImpuesto" & filaC)
                    txtI.Text += ImpuestoC

                    For e As Integer = 0 To Me.datos.Tables("ResumenCompras").Rows().Count - 1
                        If dgvResumen.Rows(e).Cells("PorcentajeImpuestoResumenCompras").Value.Equals(filaC) Then
                            dgvResumen.Rows(e).Cells("SubTotalResumenCompras").Value = ControlTotalCompras("txtSubTotal" & filaC).Text
                        End If
                    Next
                End If

            Next
        Next
        dgvDetalleCompras.Refresh()
        ControlFormatoTotalCompras()
        ControlFormatoTotalVentas()
        EnviarValorResumenVentas()
        EnviarValorResumenCompras()
        btnGenerar.Enabled = True
        btnGenerar.Text = "Generar"

    End Sub
    Sub EnviarValorResumenVentas()
        For Each ctrl As Control In panelVentasTotales.Controls
            If TypeOf ctrl Is TextBox Then
                Dim eti As String = ctrl.Tag
                Dim datosEti As String() = eti.Split(";")
                RecibirValorResumenVentas(datosEti(1), datosEti(0), ctrl.Text)
            End If
        Next

        Dim totalSubTotal As Double = 0
        Dim totalImpuesto As Double = 0
        For Each linea As dstActividadEconomica.ResumenVentasRow In datos.ResumenVentas
            totalSubTotal += linea.SubTotal
            totalImpuesto += linea.Impuesto
        Next

        LblSubTotalResumenVentas.Text = Format(totalSubTotal, "#,##0.00")
        LblImpuestoResumenVentas.Text = Format(totalImpuesto, "#,##0.00")
    End Sub
    Sub RecibirValorResumenVentas(porcentaje As String, columna As String, valor As String)
        For Each linea As dstActividadEconomica.ResumenVentasRow In datos.ResumenVentas
            If linea.PorcentajeImpuesto.ToString.Equals(porcentaje) Then
                linea.Item(columna) = valor
            End If
        Next
    End Sub
    Sub EnviarValorResumenCompras()
        For Each ctrl As Control In panelComprasTotales.Controls
            If TypeOf ctrl Is TextBox Then
                Dim eti As String = ctrl.Tag
                Dim datosEti As String() = eti.Split(";")
                RecibirValorResumenCompras(datosEti(1), datosEti(0), ctrl.Text)
            End If
        Next

        Dim totalSubTotal As Double = 0
        Dim totalImpuesto As Double = 0
        For Each linea As dstActividadEconomica.ResumenComprasRow In datos.ResumenCompras
            totalSubTotal += linea.SubTotal
            totalImpuesto += linea.Impuesto
        Next

        LblSubTotalResumenCompras.Text = Format(totalSubTotal, "#,##0.00")
        LblImpuestoResumenCompras.Text = Format(totalImpuesto, "#,##0.00")
    End Sub
    Sub RecibirValorResumenCompras(porcentaje As String, columna As String, valor As String)
        For Each linea As dstActividadEconomica.ResumenComprasRow In datos.ResumenCompras
            If linea.PorcentajeImpuesto.ToString.Equals(porcentaje) Then
                linea.Item(columna) = valor
            End If
        Next
    End Sub

    Function ControlTotalCompras(nombre As String) As TextBox
        For Each ctrl As Control In panelComprasTotales.Controls
            If ctrl.Name = nombre Then
                Return ctrl
            End If
        Next
        Return New Control()
    End Function
    Sub ControlFormatoTotalCompras()
        For Each ctrl As Control In panelComprasTotales.Controls
            If ctrl.Name.StartsWith("txt") Then
                Dim total As Double = ctrl.Text
                ctrl.Text = Format(total, "#,##0.00")
            End If
        Next

    End Sub
    Function ControlTotalVentas(nombre As String) As TextBox
        For Each ctrl As Control In panelVentasTotales.Controls
            If ctrl.Name = nombre Then
                Return ctrl
            End If
        Next
        Return New Control()
    End Function
    Sub ControlFormatoTotalVentas()
        For Each ctrl As Control In panelVentasTotales.Controls
            If ctrl.Name.StartsWith("txt") Then
                Dim total As Double = ctrl.Text
                ctrl.Text = Format(total, "#,##0.00")
            End If
        Next

    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub
    Private Sub Imprimir()
        btnImprimir.Enabled = False
        btnImprimir.Text = "Cargando..."
        InformeActividadEconomicaControlador.AbrirInforme(datos, nombreUsuario, cmbActividadEconomica.Text, cmbMes.Text)
        btnImprimir.Enabled = True
        btnImprimir.Text = "Imprimir"
    End Sub

    Private Sub btExportarVentas_Click(sender As Object, e As EventArgs) Handles btExportarVentas.Click
        Exportar(dgvDetalleVentas)
    End Sub
    Private Sub Exportar(dg As DataGridView)
        ExportarGridView.Exportar(dg)
    End Sub

    Private Sub btExportarCompras_Click(sender As Object, e As EventArgs) Handles btExportarCompras.Click
        Exportar(dgvDetalleCompras)
    End Sub
End Class

