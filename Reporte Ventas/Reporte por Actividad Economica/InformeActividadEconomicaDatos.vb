﻿Imports System.Data
Imports System.Data.SqlClient

Public Class InformeActividadEconomicaDatos
    'y
    '    End Sub

    Public Shared Sub cargarDatosPeriodo(ByRef tabla As DataTable)
        Try
            Dim sql As New SqlClient.SqlCommand
            sql.CommandText = "SELECT Id_Periodo, AnnoMes from dbo.vs_NumeroDePeriodoANombre"
            'cFunciones.Llenar_Tabla_Generico(sql, tabla, Configuracion.Claves.Conexion("Contabilidad"))
            cFunciones.Llenar_Tabla_Generico(sql, tabla, GetSetting("SeeSOFT", "Contabilidad", "Conexion"))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Public Shared Sub cargarDatosActividadEconomica(ByRef tabla As DataTable)
        Try
            Dim sql As New SqlClient.SqlCommand
            sql.CommandType = CommandType.Text
            sql.CommandText = "SELECT Id, Descripcion from  dbo.TB_CE_ActividadEconomica"
            'cFunciones.Llenar_Tabla_Generico(sql, tabla, Configuracion.Claves.Conexion("SEEPOS"))
            cFunciones.Llenar_Tabla_Generico(sql, tabla, GetSetting("SeeSOFT", "SeePos", "Conexion"))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Public Shared Sub cargarDatosDgvDetallesVentas(ByRef tabla As DataTable, IdActividadEconomica As Integer, diaInicio As Date, diaFinal As Date, Tipo As Integer)
        Try
            Dim sql As New SqlClient.SqlCommand
            sql.CommandType = CommandType.Text

            Select Case Tipo
                Case 0
                    sql.CommandText = "SELECT Documento,Tipo,Fecha,Cliente,Impuesto,SubTotal,Total,PorcentajeImpuesto FROM vs_detalle_ventas
                            where Tributable=0 and Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' 
                            and CodActividadEconomica=" & IdActividadEconomica & ""
                Case 1
                    sql.CommandText = "SELECT Documento,Tipo,Fecha,Cliente,Impuesto,SubTotal,Total,PorcentajeImpuesto FROM vs_detalle_ventas
                            where Tributable=1 and Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' 
                            and CodActividadEconomica=" & IdActividadEconomica & ""
                Case -1
                    sql.CommandText = "SELECT Documento,Tipo,Fecha,Cliente,Impuesto,SubTotal,Total,PorcentajeImpuesto FROM vs_detalle_ventas
                            where Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' 
                            and CodActividadEconomica=" & IdActividadEconomica & ""
            End Select


            cFunciones.Llenar_Tabla_Generico(sql, tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Public Shared Sub cargarDatosDgvDetallesVentasMostrar(ByRef tabla As DataTable, IdActividadEconomica As Integer, diaInicio As Date, diaFinal As Date, Tipo As Integer)
        Try
            Dim sql As New SqlClient.SqlCommand
            sql.CommandType = CommandType.Text
            Select Case Tipo
                Case 0
                    sql.CommandText = "SELECT Documento,Tipo,Fecha,Cliente,Impuesto,SubTotal,Total,0 AS PorcentajeImpuesto FROM vs_detalle_mostrar
                            where Tributable=0 and Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' 
                            and CodActividadEconomica=" & IdActividadEconomica & ""
                Case 1
                    sql.CommandText = "SELECT Documento,Tipo,Fecha,Cliente,Impuesto,SubTotal,Total,0 AS PorcentajeImpuesto FROM vs_detalle_mostrar
                            where Tributable=1 and Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' 
                            and CodActividadEconomica=" & IdActividadEconomica & ""
                Case -1
                    sql.CommandText = "SELECT Documento,Tipo,Fecha,Cliente,Impuesto,SubTotal,Total,0 AS PorcentajeImpuesto FROM vs_detalle_mostrar
                            where Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' 
                            and CodActividadEconomica=" & IdActividadEconomica & ""
            End Select
            cFunciones.Llenar_Tabla_Generico(sql, tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub
    Public Shared Sub cargarDatosDgvDetallesCompras(ByRef tabla As DataTable, IdActividadEconomica As Integer, diaInicio As Date, diaFinal As Date)
        Try
            Dim sql As New SqlClient.SqlCommand
            sql.CommandType = CommandType.Text
            sql.CommandText = "SELECT Documento,Tipo,Fecha,Cliente as Proveedor,Impuesto,SubTotal,Total,PorcentajeImpuesto FROM vs_detalle_compras
                            where Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' 
                            and CodActividadEconomica=" & IdActividadEconomica & ""
            'cFunciones.Llenar_Tabla_Generico(sql, tabla, Configuracion.Claves.Conexion("SEEPOS"))
            cFunciones.Llenar_Tabla_Generico(sql, tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Public Shared Sub cargarDatosDgvDetallesComprasMostrar(ByRef tabla As DataTable, IdActividadEconomica As Integer, diaInicio As Date, diaFinal As Date)
        Try
            Dim sql As New SqlClient.SqlCommand
            sql.CommandType = CommandType.Text
            sql.CommandText = "SELECT Documento,Tipo,Fecha,Cliente as Proveedor,Impuesto,SubTotal,Total,0 AS PorcentajeImpuesto FROM vs_detalleCompras_mostrar
                            where Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' 
                            and CodActividadEconomica=" & IdActividadEconomica & ""
            'cFunciones.Llenar_Tabla_Generico(sql, tabla, Configuracion.Claves.Conexion("SEEPOS"))
            cFunciones.Llenar_Tabla_Generico(sql, tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Public Shared Sub cargarDatosDgvResumenCompras(ByRef tabla As DataTable, IdActividadEconomica As Integer, diaInicio As Date, diaFinal As Date)
        Try
            Dim sql As New SqlClient.SqlCommand
            sql.CommandType = CommandType.Text
            sql.CommandText = "select PorcentajeImpuesto, sum(SubTotal) as SubTotal,sum(Impuesto) as Impuesto from(
	                        select PorcentajeImpuesto,SubTotal,Impuesto from vs_resumenCompras_compras
	                        where Fecha >='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
	                        union
	                        select PorcentajeImpuesto,SubTotal,Impuesto from vs_resumenCompras_devolucionesCompras
	                        where Fecha >='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
	                        union
	                        select PorcentajeImpuesto,SubTotal,Impuesto from vs_resumenCompras_ajustecpagar
	                         where Fecha >='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
                             and Tipo='CRE'
                            union
	                        select PorcentajeImpuesto,SubTotal,Impuesto from vs_resumenCompras_ajustecpagar
	                        where Fecha >='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
                            and Tipo ='DEB'
                        ) Compras group by PorcentajeImpuesto order by PorcentajeImpuesto"
            'cFunciones.Llenar_Tabla_Generico(sql, tabla, Configuracion.Claves.Conexion("SEEPOS"))
            cFunciones.Llenar_Tabla_Generico(sql, tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Public Shared Sub cargarDatosDgvResumenVentas(ByRef tabla As DataTable, IdActividadEconomica As Integer, diaInicio As Date, diaFinal As Date, Tipo As Integer)
        Try
            Dim sql As New SqlClient.SqlCommand
            sql.CommandType = CommandType.Text
            Select Case Tipo
                Case 0
                    Sql.CommandText = "select PorcentajeImpuesto, sum(SubTotal) as SubTotal,sum(Impuesto) as Impuesto from(
	                        select PorcentajeImpuesto,SubTotal,Impuesto, Id,  Tipo from vs_resumenVentas_Ventas
	                        where Tributable=0 and Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
	                        union 
	                        select PorcentajeImpuesto,SubTotal,Impuesto, Devolucion As Id, Tipo from vs_resumenVentas_devolucionesVentas
	                        where Tributable=0 and Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
	                        ) Ventas group by PorcentajeImpuesto order by PorcentajeImpuesto"
                Case 1
                    Sql.CommandText = "select PorcentajeImpuesto, sum(SubTotal) as SubTotal,sum(Impuesto) as Impuesto from(
	                        select PorcentajeImpuesto,SubTotal,Impuesto, Id,  Tipo from vs_resumenVentas_Ventas
	                        where Tributable=1 and Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
	                        union 
	                        select PorcentajeImpuesto,SubTotal,Impuesto, Devolucion As Id, Tipo from vs_resumenVentas_devolucionesVentas
	                        where Tributable=1 and Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
	                        ) Ventas group by PorcentajeImpuesto order by PorcentajeImpuesto"
                Case -1
                    Sql.CommandText = "select PorcentajeImpuesto, sum(SubTotal) as SubTotal,sum(Impuesto) as Impuesto from(
	                        select PorcentajeImpuesto,SubTotal,Impuesto, Id,  Tipo from vs_resumenVentas_Ventas
	                        where Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
	                        union 
	                        select PorcentajeImpuesto,SubTotal,Impuesto, Devolucion As Id, Tipo from vs_resumenVentas_devolucionesVentas
	                        where Fecha>='" & diaInicio & "' and Fecha<='" & diaFinal & " 23:59:59' and CodActividadEconomica=" & IdActividadEconomica & "
	                        ) Ventas group by PorcentajeImpuesto order by PorcentajeImpuesto"
            End Select
            cFunciones.Llenar_Tabla_Generico(Sql, tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Public Shared Function obtenerMesPeriodo(IdPeriodo As Integer) As Integer
        Dim cConexion As New Conexion
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader
        Dim mes As Integer = 0
        Try
            rs = cConexion.GetRecorset(cConexion.Conectar("Contabilidad"), "SELECT Mes from Periodo where Id_Periodo=" & IdPeriodo & "")

            mes = "0"
            While rs.Read
                mes = rs("Mes")
            End While
            rs.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
        Return mes
    End Function

    Public Shared Function obtenerAnnoPeriodo(IdPeriodo As Integer) As Integer
        Dim cConexion As New Conexion
        Dim sqlConexion As New SqlConnection
        Dim rs As SqlDataReader
        Dim mes As Integer = 0
        Try
            rs = cConexion.GetRecorset(cConexion.Conectar("Contabilidad"), "SELECT Anno from Periodo where Id_Periodo=" & IdPeriodo & "")

            mes = "0"
            While rs.Read
                mes = rs("Anno")
            End While
            rs.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Comunique el siguiente error a su Empresa Proveedora de Software")
        Finally
            cConexion.DesConectar(sqlConexion)
        End Try
        Return mes
    End Function
End Class
