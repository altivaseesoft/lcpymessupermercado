﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports System.Windows.Forms
Imports System.Data

Public Class frmReporteVentas
    Inherits System.Windows.Forms.Form

#Region " Código generado por el Diseñador de Windows Forms "
    Dim IdUsuario As String
    Public Sub New(cedula As String)
        MyBase.New()

        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()
        IdUsuario = cedula
        'Agregar cualquier inicialización después de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código.

    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents FechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents ButtonMostrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbxCliente As System.Windows.Forms.ComboBox
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents daClientes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents cboMonedas As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoCambio As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As ValidText.ValidText
    Friend WithEvents cbxEmpleado As System.Windows.Forms.ComboBox
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblEmpleado As System.Windows.Forms.Label
    Friend WithEvents cbxCategoría As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategoría As System.Windows.Forms.Label

    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsReporteVentas As DsReporteVentas
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Private WithEvents TreeList1 As DevExpress.XtraTreeList.TreeList
    Private WithEvents TreeList As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents CBTipo As System.Windows.Forms.ComboBox
    Friend WithEvents cbAnalisisVenta As ComboBox
    Friend WithEvents Ck_VerArticulos As System.Windows.Forms.CheckBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReporteVentas))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Ck_VerArticulos = New System.Windows.Forms.CheckBox()
        Me.CBTipo = New System.Windows.Forms.ComboBox()
        Me.ButtonMostrar = New DevExpress.XtraEditors.SimpleButton()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.cbxCliente = New System.Windows.Forms.ComboBox()
        Me.cbxCategoría = New System.Windows.Forms.ComboBox()
        Me.cbxEmpleado = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtMonto = New ValidText.ValidText()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboMonedas = New System.Windows.Forms.ComboBox()
        Me.DsReporteVentas = New LcPymes_5._2.DsReporteVentas()
        Me.FechaFinal = New System.Windows.Forms.DateTimePicker()
        Me.FechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.lblEmpleado = New System.Windows.Forms.Label()
        Me.lblTipoCambio = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.lblCategoría = New System.Windows.Forms.Label()
        Me.TreeList1 = New DevExpress.XtraTreeList.TreeList()
        Me.TreeList = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.daClientes = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.daMoneda = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.cbAnalisisVenta = New System.Windows.Forms.ComboBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DsReporteVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TreeList1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.GroupBox1.Controls.Add(Me.cbAnalisisVenta)
        Me.GroupBox1.Controls.Add(Me.Ck_VerArticulos)
        Me.GroupBox1.Controls.Add(Me.CBTipo)
        Me.GroupBox1.Controls.Add(Me.ButtonMostrar)
        Me.GroupBox1.Controls.Add(Me.cbxCliente)
        Me.GroupBox1.Controls.Add(Me.cbxCategoría)
        Me.GroupBox1.Controls.Add(Me.cbxEmpleado)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtMonto)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cboMonedas)
        Me.GroupBox1.Controls.Add(Me.FechaFinal)
        Me.GroupBox1.Controls.Add(Me.FechaInicio)
        Me.GroupBox1.Controls.Add(Me.lblEmpleado)
        Me.GroupBox1.Controls.Add(Me.lblTipoCambio)
        Me.GroupBox1.Controls.Add(Me.lblCliente)
        Me.GroupBox1.Controls.Add(Me.lblCategoría)
        Me.GroupBox1.Controls.Add(Me.TreeList1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(212, 513)
        Me.GroupBox1.TabIndex = 77
        Me.GroupBox1.TabStop = False
        '
        'Ck_VerArticulos
        '
        Me.Ck_VerArticulos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Ck_VerArticulos.Location = New System.Drawing.Point(16, 481)
        Me.Ck_VerArticulos.Name = "Ck_VerArticulos"
        Me.Ck_VerArticulos.Size = New System.Drawing.Size(104, 24)
        Me.Ck_VerArticulos.TabIndex = 67
        Me.Ck_VerArticulos.Text = "Ver Articulos"
        '
        'CBTipo
        '
        Me.CBTipo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CBTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBTipo.ForeColor = System.Drawing.Color.Blue
        Me.CBTipo.Items.AddRange(New Object() {"TODAS", "CON", "CRE", "PVE"})
        Me.CBTipo.Location = New System.Drawing.Point(8, 288)
        Me.CBTipo.Name = "CBTipo"
        Me.CBTipo.Size = New System.Drawing.Size(184, 21)
        Me.CBTipo.TabIndex = 66
        Me.CBTipo.Visible = False
        '
        'ButtonMostrar
        '
        Me.ButtonMostrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonMostrar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat
        Me.ButtonMostrar.ImageIndex = 0
        Me.ButtonMostrar.ImageList = Me.ImageList1
        Me.ButtonMostrar.Location = New System.Drawing.Point(8, 431)
        Me.ButtonMostrar.Name = "ButtonMostrar"
        Me.ButtonMostrar.Size = New System.Drawing.Size(184, 48)
        Me.ButtonMostrar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.RoyalBlue)
        Me.ButtonMostrar.TabIndex = 20
        Me.ButtonMostrar.Text = "Mostrar Reporte"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        '
        'cbxCliente
        '
        Me.cbxCliente.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbxCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.ForeColor = System.Drawing.Color.Blue
        Me.cbxCliente.Location = New System.Drawing.Point(8, 287)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.Size = New System.Drawing.Size(184, 21)
        Me.cbxCliente.TabIndex = 40
        '
        'cbxCategoría
        '
        Me.cbxCategoría.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbxCategoría.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCategoría.Enabled = False
        Me.cbxCategoría.ForeColor = System.Drawing.Color.Blue
        Me.cbxCategoría.Location = New System.Drawing.Point(8, 287)
        Me.cbxCategoría.Name = "cbxCategoría"
        Me.cbxCategoría.Size = New System.Drawing.Size(182, 21)
        Me.cbxCategoría.TabIndex = 63
        '
        'cbxEmpleado
        '
        Me.cbxEmpleado.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbxEmpleado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxEmpleado.Enabled = False
        Me.cbxEmpleado.ForeColor = System.Drawing.Color.Blue
        Me.cbxEmpleado.Location = New System.Drawing.Point(8, 287)
        Me.cbxEmpleado.Name = "cbxEmpleado"
        Me.cbxEmpleado.Size = New System.Drawing.Size(182, 21)
        Me.cbxEmpleado.TabIndex = 61
        Me.cbxEmpleado.Visible = False
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(8, 383)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 20)
        Me.Label2.TabIndex = 60
        Me.Label2.Text = "Monto >="
        '
        'txtMonto
        '
        Me.txtMonto.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtMonto.Enabled = False
        Me.txtMonto.FieldReference = Nothing
        Me.txtMonto.Location = New System.Drawing.Point(96, 383)
        Me.txtMonto.MaskEdit = ""
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.RegExPattern = ValidText.ValidText.RegularExpressionModes.Custom
        Me.txtMonto.Required = False
        Me.txtMonto.ShowErrorIcon = False
        Me.txtMonto.Size = New System.Drawing.Size(96, 20)
        Me.txtMonto.TabIndex = 59
        Me.txtMonto.ValidationMode = ValidText.ValidText.ValidationModes.None
        Me.txtMonto.ValidText = Nothing
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.Label4.Location = New System.Drawing.Point(8, 335)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 20)
        Me.Label4.TabIndex = 57
        Me.Label4.Text = "Hasta"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.Label3.Location = New System.Drawing.Point(8, 311)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 20)
        Me.Label3.TabIndex = 56
        Me.Label3.Text = "Desde"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.Location = New System.Drawing.Point(8, 359)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 20)
        Me.Label13.TabIndex = 53
        Me.Label13.Text = "Moneda"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboMonedas
        '
        Me.cboMonedas.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboMonedas.DataSource = Me.DsReporteVentas
        Me.cboMonedas.DisplayMember = "Moneda.MonedaNombre"
        Me.cboMonedas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMonedas.Location = New System.Drawing.Point(96, 359)
        Me.cboMonedas.Name = "cboMonedas"
        Me.cboMonedas.Size = New System.Drawing.Size(96, 21)
        Me.cboMonedas.TabIndex = 41
        Me.cboMonedas.ValueMember = "ValorCompra"
        '
        'DsReporteVentas
        '
        Me.DsReporteVentas.DataSetName = "DsReporteVentas"
        Me.DsReporteVentas.Locale = New System.Globalization.CultureInfo("es-CR")
        Me.DsReporteVentas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FechaFinal
        '
        Me.FechaFinal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaFinal.Location = New System.Drawing.Point(96, 335)
        Me.FechaFinal.Name = "FechaFinal"
        Me.FechaFinal.Size = New System.Drawing.Size(96, 20)
        Me.FechaFinal.TabIndex = 37
        Me.FechaFinal.Value = New Date(2006, 4, 19, 0, 0, 0, 0)
        '
        'FechaInicio
        '
        Me.FechaInicio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaInicio.Location = New System.Drawing.Point(96, 311)
        Me.FechaInicio.Name = "FechaInicio"
        Me.FechaInicio.Size = New System.Drawing.Size(96, 20)
        Me.FechaInicio.TabIndex = 36
        Me.FechaInicio.Value = New Date(2006, 4, 10, 0, 0, 0, 0)
        '
        'lblEmpleado
        '
        Me.lblEmpleado.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblEmpleado.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lblEmpleado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpleado.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblEmpleado.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.lblEmpleado.Location = New System.Drawing.Point(8, 271)
        Me.lblEmpleado.Name = "lblEmpleado"
        Me.lblEmpleado.Size = New System.Drawing.Size(184, 15)
        Me.lblEmpleado.TabIndex = 62
        Me.lblEmpleado.Text = "Empleado"
        Me.lblEmpleado.Visible = False
        '
        'lblTipoCambio
        '
        Me.lblTipoCambio.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsReporteVentas, "Moneda.ValorCompra", True))
        Me.lblTipoCambio.Location = New System.Drawing.Point(624, 88)
        Me.lblTipoCambio.Name = "lblTipoCambio"
        Me.lblTipoCambio.Size = New System.Drawing.Size(100, 8)
        Me.lblTipoCambio.TabIndex = 42
        '
        'lblCliente
        '
        Me.lblCliente.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCliente.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblCliente.Location = New System.Drawing.Point(8, 271)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(184, 15)
        Me.lblCliente.TabIndex = 58
        Me.lblCliente.Text = "Cliente"
        '
        'lblCategoría
        '
        Me.lblCategoría.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCategoría.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lblCategoría.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCategoría.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategoría.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblCategoría.Location = New System.Drawing.Point(8, 271)
        Me.lblCategoría.Name = "lblCategoría"
        Me.lblCategoría.Size = New System.Drawing.Size(186, 15)
        Me.lblCategoría.TabIndex = 64
        Me.lblCategoría.Text = "Categoría"
        Me.lblCategoría.Visible = False
        '
        'TreeList1
        '
        Me.TreeList1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TreeList1.Columns.AddRange(New DevExpress.XtraTreeList.Columns.TreeListColumn() {Me.TreeList})
        Me.TreeList1.Location = New System.Drawing.Point(7, 10)
        Me.TreeList1.Name = "TreeList1"
        Me.TreeList1.BeginUnboundLoad()
        Me.TreeList1.AppendNode(New Object() {"Ventas"}, -1, 2, 3, -1)
        Me.TreeList1.AppendNode(New Object() {"Detalladas"}, 0, 1, 0, 0)
        Me.TreeList1.AppendNode(New Object() {"Detalladas x Cliente"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Diarias"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Reales"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Empleado"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Familia"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Periodo Fiscal"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Tipo"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Anuladas"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"SubFamilia"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Comisiones"}, -1, 2, 3, -1)
        Me.TreeList1.AppendNode(New Object() {"Contado y Recuperaci�n"}, 11, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Venta Contado y Cr�dito"}, 11, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Devoluciones"}, -1, 2, 3, -1)
        Me.TreeList1.AppendNode(New Object() {"General x Empleados"}, 14, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Venta General"}, 14, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Detallada x Empleado"}, 14, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Ganancias"}, -1, 2, 3, -1)
        Me.TreeList1.AppendNode(New Object() {"Ventas"}, 18, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Seguimiento"}, -1, 2, 3, -1)
        Me.TreeList1.AppendNode(New Object() {"Empleado"}, 20, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Familia"}, 20, 1, 0, -1)
        Me.TreeList1.EndUnboundLoad()
        Me.TreeList1.PrintOptions = CType((((((DevExpress.XtraTreeList.PrintOptionsFlags.PrintPageHeader Or DevExpress.XtraTreeList.PrintOptionsFlags.PrintTree) _
            Or DevExpress.XtraTreeList.PrintOptionsFlags.PrintTreeButtons) _
            Or DevExpress.XtraTreeList.PrintOptionsFlags.PrintImages) _
            Or DevExpress.XtraTreeList.PrintOptionsFlags.AutoWidth) _
            Or DevExpress.XtraTreeList.PrintOptionsFlags.AutoRowHeight), DevExpress.XtraTreeList.PrintOptionsFlags)
        Me.TreeList1.RootValue = "0"
        Me.TreeList1.SelectImageList = Me.ImageList2
        Me.TreeList1.Size = New System.Drawing.Size(195, 255)
        Me.TreeList1.TabIndex = 65
        Me.TreeList1.TreeLineStyle = DevExpress.XtraTreeList.LineStyle.None
        '
        'TreeList
        '
        Me.TreeList.Caption = "Tipos Reporte"
        Me.TreeList.FieldName = "TreeListColumn1"
        Me.TreeList.Name = "TreeList"
        Me.TreeList.Options = CType(((((((DevExpress.XtraTreeList.Columns.ColumnOptions.CanMoved Or DevExpress.XtraTreeList.Columns.ColumnOptions.CanResized) _
            Or DevExpress.XtraTreeList.Columns.ColumnOptions.CanSorted) _
            Or DevExpress.XtraTreeList.Columns.ColumnOptions.[ReadOnly]) _
            Or DevExpress.XtraTreeList.Columns.ColumnOptions.CanFocused) _
            Or DevExpress.XtraTreeList.Columns.ColumnOptions.ShowInCustomizationForm) _
            Or DevExpress.XtraTreeList.Columns.ColumnOptions.CanMovedToCustomizationForm), DevExpress.XtraTreeList.Columns.ColumnOptions)
        Me.TreeList.VisibleIndex = 0
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "")
        Me.ImageList2.Images.SetKeyName(1, "")
        Me.ImageList2.Images.SetKeyName(2, "")
        Me.ImageList2.Images.SetKeyName(3, "")
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=DIEGO;packet size=4096;integrated security=SSPI;data source=""."";pe" &
    "rsist security info=False;initial catalog=SeePos"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'daClientes
        '
        Me.daClientes.DeleteCommand = Me.SqlDeleteCommand1
        Me.daClientes.InsertCommand = Me.SqlInsertCommand1
        Me.daClientes.SelectCommand = Me.SqlSelectCommand1
        Me.daClientes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Clientes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("identificacion", "identificacion"), New System.Data.Common.DataColumnMapping("cedula", "cedula"), New System.Data.Common.DataColumnMapping("nombre", "nombre"), New System.Data.Common.DataColumnMapping("observaciones", "observaciones"), New System.Data.Common.DataColumnMapping("Telefono_01", "Telefono_01"), New System.Data.Common.DataColumnMapping("telefono_02", "telefono_02"), New System.Data.Common.DataColumnMapping("fax_01", "fax_01"), New System.Data.Common.DataColumnMapping("fax_02", "fax_02"), New System.Data.Common.DataColumnMapping("e_mail", "e_mail"), New System.Data.Common.DataColumnMapping("abierto", "abierto"), New System.Data.Common.DataColumnMapping("direccion", "direccion"), New System.Data.Common.DataColumnMapping("impuesto", "impuesto"), New System.Data.Common.DataColumnMapping("max_credito", "max_credito"), New System.Data.Common.DataColumnMapping("Plazo_credito", "Plazo_credito"), New System.Data.Common.DataColumnMapping("descuento", "descuento"), New System.Data.Common.DataColumnMapping("empresa", "empresa"), New System.Data.Common.DataColumnMapping("tipoprecio", "tipoprecio"), New System.Data.Common.DataColumnMapping("sinrestriccion", "sinrestriccion"), New System.Data.Common.DataColumnMapping("usuario", "usuario"), New System.Data.Common.DataColumnMapping("nombreusuario", "nombreusuario"), New System.Data.Common.DataColumnMapping("agente", "agente"), New System.Data.Common.DataColumnMapping("CodMonedaCredito", "CodMonedaCredito")})})
        Me.daClientes.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_identificacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "identificacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMonedaCredito", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMonedaCredito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Plazo_credito", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo_credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_01", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_abierto", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "abierto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_agente", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "agente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_cedula", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_direccion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "direccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_e_mail", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "e_mail", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_empresa", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "empresa", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_fax_01", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fax_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_fax_02", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fax_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_impuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "impuesto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_max_credito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "max_credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_nombreusuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "nombreusuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_sinrestriccion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "sinrestriccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_telefono_02", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "telefono_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_tipoprecio", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "tipoprecio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "usuario", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@identificacion", System.Data.SqlDbType.Int, 4, "identificacion"), New System.Data.SqlClient.SqlParameter("@cedula", System.Data.SqlDbType.VarChar, 30, "cedula"), New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 255, "nombre"), New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 255, "observaciones"), New System.Data.SqlClient.SqlParameter("@Telefono_01", System.Data.SqlDbType.VarChar, 8, "Telefono_01"), New System.Data.SqlClient.SqlParameter("@telefono_02", System.Data.SqlDbType.VarChar, 8, "telefono_02"), New System.Data.SqlClient.SqlParameter("@fax_01", System.Data.SqlDbType.VarChar, 8, "fax_01"), New System.Data.SqlClient.SqlParameter("@fax_02", System.Data.SqlDbType.VarChar, 8, "fax_02"), New System.Data.SqlClient.SqlParameter("@e_mail", System.Data.SqlDbType.VarChar, 255, "e_mail"), New System.Data.SqlClient.SqlParameter("@abierto", System.Data.SqlDbType.VarChar, 2, "abierto"), New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 255, "direccion"), New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Float, 8, "impuesto"), New System.Data.SqlClient.SqlParameter("@max_credito", System.Data.SqlDbType.Float, 8, "max_credito"), New System.Data.SqlClient.SqlParameter("@Plazo_credito", System.Data.SqlDbType.Int, 4, "Plazo_credito"), New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Float, 8, "descuento"), New System.Data.SqlClient.SqlParameter("@empresa", System.Data.SqlDbType.VarChar, 2, "empresa"), New System.Data.SqlClient.SqlParameter("@tipoprecio", System.Data.SqlDbType.SmallInt, 2, "tipoprecio"), New System.Data.SqlClient.SqlParameter("@sinrestriccion", System.Data.SqlDbType.VarChar, 2, "sinrestriccion"), New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 50, "usuario"), New System.Data.SqlClient.SqlParameter("@nombreusuario", System.Data.SqlDbType.VarChar, 50, "nombreusuario"), New System.Data.SqlClient.SqlParameter("@agente", System.Data.SqlDbType.VarChar, 50, "agente"), New System.Data.SqlClient.SqlParameter("@CodMonedaCredito", System.Data.SqlDbType.Int, 4, "CodMonedaCredito")})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = resources.GetString("SqlSelectCommand1.CommandText")
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@identificacion", System.Data.SqlDbType.Int, 4, "identificacion"), New System.Data.SqlClient.SqlParameter("@cedula", System.Data.SqlDbType.VarChar, 30, "cedula"), New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 255, "nombre"), New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 255, "observaciones"), New System.Data.SqlClient.SqlParameter("@Telefono_01", System.Data.SqlDbType.VarChar, 8, "Telefono_01"), New System.Data.SqlClient.SqlParameter("@telefono_02", System.Data.SqlDbType.VarChar, 8, "telefono_02"), New System.Data.SqlClient.SqlParameter("@fax_01", System.Data.SqlDbType.VarChar, 8, "fax_01"), New System.Data.SqlClient.SqlParameter("@fax_02", System.Data.SqlDbType.VarChar, 8, "fax_02"), New System.Data.SqlClient.SqlParameter("@e_mail", System.Data.SqlDbType.VarChar, 255, "e_mail"), New System.Data.SqlClient.SqlParameter("@abierto", System.Data.SqlDbType.VarChar, 2, "abierto"), New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 255, "direccion"), New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Float, 8, "impuesto"), New System.Data.SqlClient.SqlParameter("@max_credito", System.Data.SqlDbType.Float, 8, "max_credito"), New System.Data.SqlClient.SqlParameter("@Plazo_credito", System.Data.SqlDbType.Int, 4, "Plazo_credito"), New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Float, 8, "descuento"), New System.Data.SqlClient.SqlParameter("@empresa", System.Data.SqlDbType.VarChar, 2, "empresa"), New System.Data.SqlClient.SqlParameter("@tipoprecio", System.Data.SqlDbType.SmallInt, 2, "tipoprecio"), New System.Data.SqlClient.SqlParameter("@sinrestriccion", System.Data.SqlDbType.VarChar, 2, "sinrestriccion"), New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 50, "usuario"), New System.Data.SqlClient.SqlParameter("@nombreusuario", System.Data.SqlDbType.VarChar, 50, "nombreusuario"), New System.Data.SqlClient.SqlParameter("@agente", System.Data.SqlDbType.VarChar, 50, "agente"), New System.Data.SqlClient.SqlParameter("@CodMonedaCredito", System.Data.SqlDbType.Int, 4, "CodMonedaCredito"), New System.Data.SqlClient.SqlParameter("@Original_identificacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "identificacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMonedaCredito", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMonedaCredito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Plazo_credito", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo_credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_01", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_abierto", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "abierto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_agente", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "agente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_cedula", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_direccion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "direccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_e_mail", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "e_mail", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_empresa", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "empresa", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_fax_01", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fax_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_fax_02", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fax_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_impuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "impuesto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_max_credito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "max_credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_nombreusuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "nombreusuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_sinrestriccion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "sinrestriccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_telefono_02", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "telefono_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_tipoprecio", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "tipoprecio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "usuario", System.Data.DataRowVersion.Original, Nothing)})
        '
        'daMoneda
        '
        Me.daMoneda.DeleteCommand = Me.SqlDeleteCommand2
        Me.daMoneda.InsertCommand = Me.SqlInsertCommand2
        Me.daMoneda.SelectCommand = Me.SqlSelectCommand2
        Me.daMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        Me.daMoneda.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo")})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(200, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(488, 513)
        Me.CrystalReportViewer1.TabIndex = 78
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'cbAnalisisVenta
        '
        Me.cbAnalisisVenta.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbAnalisisVenta.FormattingEnabled = True
        Me.cbAnalisisVenta.Items.AddRange(New Object() {"General", "Tributable", "No Tributable"})
        Me.cbAnalisisVenta.Location = New System.Drawing.Point(8, 405)
        Me.cbAnalisisVenta.Name = "cbAnalisisVenta"
        Me.cbAnalisisVenta.Size = New System.Drawing.Size(184, 21)
        Me.cbAnalisisVenta.TabIndex = 68
        Me.cbAnalisisVenta.Visible = False
        '
        'frmReporteVentas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 513)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "frmReporteVentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte Ventas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DsReporteVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TreeList1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "variables"

    Private cConexion As Conexion
    Private sqlConexion As SqlConnection
    Dim Reporte_ID As Byte
#End Region

    Private Sub ButtonMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMostrar.Click
        '0	-	1	Ventas Detalladas
        '1	-	2	Ventas Detalladas x Cliente
        '2	-	7	Ventas x Período Fiscal
        '3	-	3	Ventas Diarias
        '4	-	6	Ventas x Familia
        '5	-	5	Ventas x Empleado
        '6	-	21	Seguimiento x Familia
        '7	-	20	Seguimiento x Empleado
        '8	-	8	Ventas x Tipo
        '9	-	4	Ventas Reales
        '10	-	12	Comisiones de Venta
        '11	-	15	Devoluciones de Venta
        '12	-	14	Devoluciones de Empleados
        '13	-	16	Devoluciones x Empleado
        '14	-	17	Ganancias de Venta
        '15	-	11	Comisión Contado y Recuperación
        '16	-	9	Ventas Anuladas

        Dim SQLConexion As New Conexion
        Try
            SQLConexion.SQLStringConexion = Me.SqlConnection1.ConnectionString  '   GetSetting("SeeSOFT", "SeePos", "Conexion")
            SQLConexion.Conectar()

            Select Case Reporte_ID 'Me.cbxOpcionesdeVenta.SelectedIndex
                Case 1 '0
                    If CBTipo.SelectedIndex = 0 Then
                        If Me.Ck_VerArticulos.Checked = True Then
                            Dim Reporte As New Ventas_Detalladas_General2
                            Reporte.SetParameterValue(0, "REPORTE DE VENTAS BRUTAS DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                            Reporte.SetParameterValue(1, CDbl(lblTipoCambio.Text))
                            Reporte.SetParameterValue(2, cboMonedas.Text)
                            Reporte.SetParameterValue(3, CDate(Me.FechaInicio.Value))
                            Reporte.SetParameterValue(4, CDate(Me.FechaFinal.Value))
                            'Reporte.SetParameterValue(5, CDbl(lblTipoCambio.Text))
                            'Reporte.SetParameterValue(6, CDate(Me.FechaInicio.Value))
                            'Reporte.SetParameterValue(7, CDate(Me.FechaFinal.Value))
                            CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Reporte)

                        Else
                            Dim Reporte As New Ventas_Detalladas_General
                            Dim TipoAnalisis As Integer = 0
                            Dim Titulo As String = ""

                            If cbAnalisisVenta.Visible = True Then
                                If cbAnalisisVenta.SelectedIndex = 0 Then
                                    TipoAnalisis = -1
                                    Titulo = "REPORTE DE VENTAS GENERAL DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'"
                                ElseIf cbAnalisisVenta.SelectedIndex = 1 Then
                                    TipoAnalisis = 1
                                    Titulo = "REPORTE DE VENTAS BRUTAS DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'"
                                ElseIf cbAnalisisVenta.SelectedIndex = 2 Then
                                    TipoAnalisis = 0
                                    Titulo = "REPORTE DE PRE-VENTA DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'"
                                End If
                            Else
                                TipoAnalisis = -1
                                Titulo = "REPORTE DE VENTAS GENERAL DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'"
                            End If

                            Reporte.SetParameterValue(0, Titulo)
                            Reporte.SetParameterValue(1, CDbl(lblTipoCambio.Text))
                            Reporte.SetParameterValue(2, cboMonedas.Text)
                            Reporte.SetParameterValue(3, CDate(Me.FechaInicio.Value))
                            Reporte.SetParameterValue(4, CDate(Me.FechaFinal.Value))
                            Reporte.SetParameterValue(5, TipoAnalisis)
                            Reporte.SetParameterValue(6, CDbl(lblTipoCambio.Text))
                            Reporte.SetParameterValue(7, CDate(Me.FechaInicio.Value))
                            Reporte.SetParameterValue(8, CDate(Me.FechaFinal.Value))
                            Reporte.SetParameterValue(9, TipoAnalisis)
                            CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Reporte)

                        End If

                    Else
                        Dim Reporte As New Ventas_Detalladas_General_Tipo
                        Reporte.SetParameterValue(0, "REPORTE DE VENTAS BRUTAS DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                        Reporte.SetParameterValue(1, CDbl(lblTipoCambio.Text))
                        Reporte.SetParameterValue(2, cboMonedas.Text)
                        Reporte.SetParameterValue(3, CDate(Me.FechaInicio.Value))
                        Reporte.SetParameterValue(4, CDate(Me.FechaFinal.Value))
                        Reporte.SetParameterValue(5, CBTipo.Text)
                        Reporte.SetParameterValue(6, CDbl(lblTipoCambio.Text))
                        Reporte.SetParameterValue(7, CDate(Me.FechaInicio.Value))
                        Reporte.SetParameterValue(8, CDate(Me.FechaFinal.Value))

                        CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Reporte)
                    End If


                Case 2 '1 'REPORTE DE VENTAS X CLIENTE
                    Dim ReportexCliente As New VentasxCliente
                    ReportexCliente.SetParameterValue(0, "REPORTE DE VENTAS DE '" & Me.cbxCliente.Text & "' DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                    ReportexCliente.SetParameterValue(1, CDbl(lblTipoCambio.Text))
                    ReportexCliente.SetParameterValue(2, cboMonedas.Text)
                    ReportexCliente.SetParameterValue(3, CDate(Me.FechaInicio.Value))
                    ReportexCliente.SetParameterValue(4, CDate(Me.FechaFinal.Value))
                    ReportexCliente.SetParameterValue(5, Me.cbxCliente.Text)
                    ReportexCliente.SetParameterValue(6, CDbl(lblTipoCambio.Text))
                    ReportexCliente.SetParameterValue(7, CDate(Me.FechaInicio.Value))
                    ReportexCliente.SetParameterValue(8, CDate(Me.FechaFinal.Value))
                    ReportexCliente.SetParameterValue(9, Me.cbxCliente.Text)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, ReportexCliente)

                Case 7 '2 'REPORTE DE VENTAS FISCAL
                    If txtMonto.Text = "" Then
                        MsgBox("Debe ingresar un monto!!", MsgBoxStyle.Exclamation, "Atención...")
                        Exit Try
                    End If
                    Dim ReporteFiscal As New VentasFiscales
                    ReporteFiscal.SetParameterValue(0, CDbl(lblTipoCambio.Text))
                    ReporteFiscal.SetParameterValue(1, CDate(Me.FechaInicio.Value))
                    ReporteFiscal.SetParameterValue(2, CDate(Me.FechaFinal.Value))
                    ReporteFiscal.SetParameterValue(3, cboMonedas.Text)
                    ReporteFiscal.SetParameterValue(4, CDbl(Me.txtMonto.Text))
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, ReporteFiscal)

                Case 3 'REPORTE DE VENTAS DIARIAS
                    Dim RptVentaxdia As New VentasXDia2
                    RptVentaxdia.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    RptVentaxdia.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    RptVentaxdia.SetParameterValue(2, CDbl(lblTipoCambio.Text))
                    RptVentaxdia.SetParameterValue(3, CStr(cboMonedas.Text))
                    RptVentaxdia.SetParameterValue(4, "REPORTE DE VENTAS DIARIAS DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptVentaxdia)

                Case 6 '4 'REPORTE DE VENTAS X CATEGORIA
                    Dim RptVentaxCategoria As New VentaxCategoria2
                    RptVentaxCategoria.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    RptVentaxCategoria.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    RptVentaxCategoria.SetParameterValue(2, CDbl(lblTipoCambio.Text))
                    RptVentaxCategoria.SetParameterValue(3, cboMonedas.Text)
                    RptVentaxCategoria.SetParameterValue(4, "REPORTE DE VENTAS X CATEGORIA DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptVentaxCategoria)

                Case 5 'REPORTE DE VENTAS X EMPLEADO
                    Dim RptVentaxEmpleado As New VentasxEmpleado2
                    RptVentaxEmpleado.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    RptVentaxEmpleado.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    RptVentaxEmpleado.SetParameterValue(2, "REPORTE DE VENTAS X EMPLEADO DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                    RptVentaxEmpleado.SetParameterValue(3, cboMonedas.Text)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptVentaxEmpleado)

                Case 10 '4 'REPORTE DE VENTAS X SUBFAMILIA
                    Dim ReporteVentasXSubFamilia As New ReporteVentasXSubFamilia
                    ReporteVentasXSubFamilia.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    ReporteVentasXSubFamilia.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    ReporteVentasXSubFamilia.SetParameterValue(2, CDbl(lblTipoCambio.Text))
                    ReporteVentasXSubFamilia.SetParameterValue(3, cboMonedas.Text)
                    ReporteVentasXSubFamilia.SetParameterValue(4, "REPORTE DE VENTAS X SUBFAMILIA DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, ReporteVentasXSubFamilia)

                Case 22 '6 'REPORTE DE VENTAS SEGUIMIENTO X CATEGORIA
                    Dim RptSeguimientoxCategoria As New SeguimientoxCategoria
                    'RptSeguimientoxCategoria.SetParameterValue(0, "REPORTE DE VENTAS X SEGUIMIENTO DE CATEGORIA DESDE EL '" & Me.cbxCategoría.Text & "' desde el '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                    'RptSeguimientoxCategoria.SetParameterValue(1, CDbl(lblTipoCambio.Text))
                    RptSeguimientoxCategoria.SetParameterValue("Moneda", cboMonedas.Text)
                    RptSeguimientoxCategoria.SetParameterValue("Fechainicial", CDate(Me.FechaInicio.Value))
                    RptSeguimientoxCategoria.SetParameterValue("Fechafinal", CDate(Me.FechaFinal.Value))
                    RptSeguimientoxCategoria.SetParameterValue("Categoria", Me.cbxCategoría.Text)
                    RptSeguimientoxCategoria.SetParameterValue("TipoCambio", CDbl(lblTipoCambio.Text))
                    If MsgBox("¿Desea ver el gráfico?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        RptSeguimientoxCategoria.SetParameterValue("HayGrafico", 1)
                    Else
                        RptSeguimientoxCategoria.SetParameterValue("HayGrafico", 0)
                    End If

                    'RptSeguimientoxCategoria.SetParameterValue(7, CDate(Me.FechaInicio.Value))
                    'RptSeguimientoxCategoria.SetParameterValue(8, CDate(Me.FechaFinal.Value))
                    'RptSeguimientoxCategoria.SetParameterValue(9, Me.cbxCategoría.Text)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptSeguimientoxCategoria)

                Case 21 '7 'REPORTE DE VENTAS X SEGUIMIENTO DE EMPLEADO
                    Dim RptSeguimientoxEmpleado As New SeguimientoxEmpleado
                    'JCGA 27 DE JULIO 2007
                    'RptSeguimientoxEmpleado.SetParameterValue(0, "REPORTE DE VENTAS X SEGUIMIENTO DE EMPLEADO DESDE EL " & Me.cbxEmpleado.Text & " desde el '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                    RptSeguimientoxEmpleado.SetParameterValue("Fechainicial", CDate(Me.FechaInicio.Value))
                    RptSeguimientoxEmpleado.SetParameterValue("Fechafinal", CDate(Me.FechaFinal.Value))
                    RptSeguimientoxEmpleado.SetParameterValue("Empleado", Me.cbxEmpleado.Text)
                    RptSeguimientoxEmpleado.SetParameterValue("TipoCambio", CDbl(lblTipoCambio.Text))
                    RptSeguimientoxEmpleado.SetParameterValue("Moneda", cboMonedas.Text)

                    If MsgBox("¿Desea ver el gráfico?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        RptSeguimientoxEmpleado.SetParameterValue("HayGrafico", 1)

                    Else
                        RptSeguimientoxEmpleado.SetParameterValue("HayGrafico", 0)

                    End If

                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptSeguimientoxEmpleado)

                Case 8 'REPORTE DE VENTAS X TIPO
                    Dim RptVentasxTipo As New VentasxTipo2
                    RptVentasxTipo.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    RptVentasxTipo.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    RptVentasxTipo.SetParameterValue(2, cboMonedas.Text)
                    RptVentasxTipo.SetParameterValue(3, "REPORTE DE VENTAS X TIPO DESDE EL '" & Me.FechaInicio.Text & "' HASTA EL '" & Me.FechaFinal.Text & "'")
                    RptVentasxTipo.SetParameterValue(4, CDbl(Me.lblTipoCambio.Text))

                    If MsgBox("¿Desea ver el gráfico?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        RptVentasxTipo.SetParameterValue("HayGrafico", 1)

                    Else
                        RptVentasxTipo.SetParameterValue("HayGrafico", 0)

                    End If

                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptVentasxTipo)

                Case 4 '9 'REPORTE DE VENTAS REALES

                    Dim RptVentasReales As New ReporteVentasReales
                    Dim TipoAnalisis As Integer = 0
                    Dim Titulo As String = ""

                    If cbAnalisisVenta.Visible = True Then
                        If cbAnalisisVenta.SelectedIndex = 0 Then
                            TipoAnalisis = -1
                            Titulo = "REPORTE DE VENTAS REALES GENERAL"
                        ElseIf cbAnalisisVenta.SelectedIndex = 1 Then
                            TipoAnalisis = 1
                            Titulo = "REPORTE DE VENTAS REALES"
                        ElseIf cbAnalisisVenta.SelectedIndex = 2 Then
                            TipoAnalisis = 0
                            Titulo = "REPORTE DE PRE-VENTA REALES"
                        End If
                    Else
                        TipoAnalisis = -1
                        Titulo = "REPORTE DE VENTAS REALES GENERAL"
                    End If

                    RptVentasReales.SetParameterValue(0, Me.FechaInicio.Value)
                    RptVentasReales.SetParameterValue(1, Me.FechaFinal.Value)
                    RptVentasReales.SetParameterValue(2, CDbl(lblTipoCambio.Text))
                    RptVentasReales.SetParameterValue(3, cboMonedas.Text)
                    RptVentasReales.SetParameterValue("Titulo", Titulo)
                    RptVentasReales.SetParameterValue("TipoAnalisis", TipoAnalisis)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptVentasReales)

                Case 13 '10 'REPORTE  DE COMISIONES GENERAL
                    Dim Comisionantes As New ReporteComisionesFinal
                    Comisionantes.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    Comisionantes.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Comisionantes)

                Case 16 '11 'DEVOLUCIONES
                    Dim RptDevoluciones As New Devoluciones
                    RptDevoluciones.SetParameterValue(0, CDbl(lblTipoCambio.Text))
                    RptDevoluciones.SetParameterValue(1, CDate(Me.FechaInicio.Value))
                    RptDevoluciones.SetParameterValue(2, CDate(Me.FechaFinal.Value))
                    RptDevoluciones.SetParameterValue(3, cboMonedas.Text)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptDevoluciones)

                Case 15 ' 12 'DEVOLUCIONES GENERALES DE EMPLEADOS
                    Dim RptDevoluciones_Generales As New DevolucionesGeneralEmpleados
                    RptDevoluciones_Generales.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    RptDevoluciones_Generales.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    RptDevoluciones_Generales.SetParameterValue(2, CDbl(lblTipoCambio.Text))
                    RptDevoluciones_Generales.SetParameterValue(3, cboMonedas.Text)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptDevoluciones_Generales)

                Case 17 '13 'DEVOLUCIONES X EMPLEADO
                    Dim RptDevolucionesxEmpleado As New DevolucionesxEmpleado
                    RptDevolucionesxEmpleado.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    RptDevolucionesxEmpleado.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    RptDevolucionesxEmpleado.SetParameterValue(2, CDbl(lblTipoCambio.Text))
                    RptDevolucionesxEmpleado.SetParameterValue(3, cboMonedas.Text)
                    RptDevolucionesxEmpleado.SetParameterValue(4, Me.cbxEmpleado.Text)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptDevolucionesxEmpleado)

                Case 19 '14 'GANANCIAS DE VENTA
                    Dim RptGanancias As New Ganancias_Ventas2
                    RptGanancias.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    RptGanancias.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    RptGanancias.SetParameterValue(2, CDbl(lblTipoCambio.Text))
                    RptGanancias.SetParameterValue(3, cboMonedas.Text)
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, RptGanancias)
                Case 12 '15
                    Dim ReporteComision_Ventas_Contado_Recuperacion As New ReporteComisiones_X_Contado_Recuperacion
                    ReporteComision_Ventas_Contado_Recuperacion.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    ReporteComision_Ventas_Contado_Recuperacion.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, ReporteComision_Ventas_Contado_Recuperacion)
                Case 9
                    Dim Reporte_Facturas_Ventas_Anuladas As New Reporte_Ventas_Anuladas
                    Reporte_Facturas_Ventas_Anuladas.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                    Reporte_Facturas_Ventas_Anuladas.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                    CrystalReportsConexion.LoadReportViewer(Me.CrystalReportViewer1, Reporte_Facturas_Ventas_Anuladas)
            End Select
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try

    End Sub

    '    Private Sub cbxOpcionesdeVenta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOpcionesdeVenta.SelectedIndexChanged
    Private Sub Reporte_Seleccionado()
        '0	-	1	Ventas Detalladas
        '1	-	2	Ventas Detalladas x Cliente
        '2	-	7	Ventas x Período Fiscal
        '3	-	3	Ventas Diarias
        '4	-	6	Ventas x Familia
        '5	-	5	Ventas x Empleado
        '6	-	21	Seguimiento x Familia
        '7	-	20	Seguimiento x Empleado
        '8	-	8	Ventas x Tipo
        '9	-	4	Ventas Reales
        '10	-	12	Comisiones de Venta
        '11	-	15	Devoluciones de Venta
        '12	-	14	Devoluciones de Empleados
        '13	-	16	Devoluciones x Empleado
        '14	-	17	Ganancias de Venta
        '15	-	11	Comisión Contado y Recuperación
        '16	-	9	Ventas Anuladas

        CBTipo.Visible = False
        lblEmpleado.Visible = False
        lblEmpleado.Text = "Empleado"
        Me.Ck_VerArticulos.Visible = False

        Select Case Reporte_ID 'Me.cbxOpcionesdeVenta.SelectedIndex
            Case 1 '0 'Ventas Detalladas
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Visible = False
                Me.cbxEmpleado.Visible = False
                lblEmpleado.Visible = True
                CBTipo.Visible = True
                lblEmpleado.Text = "Tipo"
                Me.FechaInicio.Focus()
                Me.Ck_VerArticulos.Checked = False
                Me.Ck_VerArticulos.Visible = True

            Case 2 '1 'Ventas Detalladas x Cliente
                Dim rs As SqlDataReader
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Visible = True
                Me.cbxCliente.Enabled = True
                Me.cbxCliente.Focus()
                Me.cbxEmpleado.Enabled = False
                Me.cbxEmpleado.Visible = False

                rs = cConexion.GetRecorset(cConexion.sQlconexion, "SELECT nombre from clientes order by nombre")
                While rs.Read
                    Try
                        Me.cbxCliente.Items.Add(rs!nombre)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()

            Case 7 '2 'Ventas x Período Fiscal
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = True
                Me.txtMonto.Focus()
                Me.cbxEmpleado.Enabled = False
                Me.cbxCliente.Enabled = False

            Case 3 'Ventas Diarias
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = False

            Case 6 '4 'Ventas x Familia
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = False
            Case 5 'Ventas x Empleado
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.lblCliente.Visible = False
                Me.cbxCliente.Enabled = False
                Me.cbxCliente.Visible = False
                Me.lblEmpleado.Visible = False
                Me.cbxEmpleado.Enabled = False
                Me.cbxEmpleado.Visible = False
            Case 22 '6 'Seguimiento x Familia
                Dim rs As SqlDataReader
                rs = cConexion.GetRecorset(cConexion.sQlconexion, "SELECT * from Familia order by Descripcion")
                While rs.Read
                    Try
                        Me.cbxCategoría.Items.Add(rs!Descripcion)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()
                Me.lblCategoría.Visible = True
                Me.cbxCategoría.Enabled = True
                Me.cbxCategoría.Visible = True
                Me.txtMonto.Enabled = False
                Me.lblCliente.Visible = False
                Me.cbxCliente.Visible = False
                Me.lblEmpleado.Visible = False
                Me.cbxEmpleado.Visible = False
            Case 21 '7 'Seguimiento x Empleado
                Dim rs As SqlDataReader
                Me.cbxEmpleado.Items.Add("---------------")
                rs = cConexion.GetRecorset(cConexion.sQlconexion, "SELECT * from usuarios order by nombre")
                While rs.Read
                    Try
                        Me.cbxEmpleado.Items.Add(rs!nombre)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.lblCliente.Visible = False
                Me.cbxCliente.Visible = False
                Me.lblEmpleado.Visible = True
                Me.cbxEmpleado.Enabled = True
                Me.cbxEmpleado.Visible = True
            Case 8 'Ventas x Tipo
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = False
            Case 4, 12 '9, 15 '     Case 4, 11 '9, 15
                'Ventas(Reales)
                'Comisión Contado y Recuperación



                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = False
                Me.FechaInicio.Focus()
            Case 13 '10 'REPORTE DE COMISIONES de Venta Case 12 '10 'REPORTE DE COMISIONES de Venta

                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = False
                Me.FechaInicio.Focus()
            Case 16 '11 'Devoluciones de Venta
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = False
                Me.FechaInicio.Focus()
            Case 15 '12 'Devoluciones de Empleados

                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = False
                Me.FechaInicio.Focus()
            Case 17 '13 'Devoluciones x Empleado

                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = True
                Me.cbxEmpleado.Visible = True
                Me.lblEmpleado.Visible = True
                Me.FechaInicio.Focus()
                Dim rs As SqlDataReader
                rs = cConexion.GetRecorset(cConexion.sQlconexion, "SELECT distinct(nombre) from usuarios order by nombre")
                While rs.Read
                    Try
                        Me.cbxEmpleado.Items.Add(rs!nombre)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                'JCGA 31 DE JULIO 2007
                If Me.cbxEmpleado.Enabled = False Then
                    Me.cbxEmpleado.Enabled = True
                End If
                Me.cbxCliente.Visible = False
                rs.Close()
            Case 18 '14 'Ganancias de Venta
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.cbxCliente.Enabled = False
                Me.cbxEmpleado.Enabled = False
                Me.cbxEmpleado.Visible = False
                Me.lblEmpleado.Visible = True
                Me.FechaInicio.Focus()

            Case 19 'Ventas 
                Me.lblCategoría.Visible = False
                Me.cbxCategoría.Visible = False
                Me.txtMonto.Enabled = False
                Me.lblCliente.Visible = False
                Me.cbxCliente.Enabled = False
                Me.cbxCliente.Visible = False
                Me.lblEmpleado.Visible = False
                Me.cbxEmpleado.Enabled = False
                Me.cbxEmpleado.Visible = False
        End Select
    End Sub

    Private Sub frmReporteVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            cConexion = New Conexion
            sqlConexion = cConexion.Conectar

            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            Me.daClientes.Fill(Me.DsReporteVentas, "clientes")
            Me.daMoneda.Fill(Me.DsReporteVentas, "moneda")
            Me.FechaInicio.Text = Date.Today
            Me.FechaFinal.Text = Date.Today
            Me.TreeList1.FullExpand()
            CBTipo.SelectedIndex = 0
            ObtenerPermisos()
            cbAnalisisVenta.SelectedIndex = 0
            WindowState = FormWindowState.Maximized
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Function ObtenerPermisos() As Boolean
        Try
            Dim dtPermisos As New DataTable
            Dim UsaAnalisis As Boolean = False
            Dim TipoUsuario As Boolean = False
            cFunciones.Llenar_Tabla_Generico("Select top 1 UsaAnalisisVenta from Configuraciones", dtPermisos, GetSetting("SeeSOFT", "SEEPOS", "CONEXION"))

            If dtPermisos.Rows.Count > 0 Then
                UsaAnalisis = dtPermisos.Rows(0).ItemArray(0)
            End If

            dtPermisos.Clear()
            dtPermisos.Columns.Clear()
            dtPermisos.Rows.Clear()
            cFunciones.Llenar_Tabla_Generico("Select Gerencial from Usuarios where Id_Usuario='" & IdUsuario & "'", dtPermisos, GetSetting("SeeSOFT", "Seguridad", "CONEXION"))
            If dtPermisos.Rows.Count > 0 Then
                TipoUsuario = dtPermisos.Rows(0).ItemArray(0)
            End If

            If UsaAnalisis = True And TipoUsuario = True Then
                cbAnalisisVenta.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Sub txtMonto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMonto.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub TreeList1_FocusedNodeChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraTreeList.FocusedNodeChangedEventArgs) Handles TreeList1.FocusedNodeChanged
        Reporte_ID = e.Node.Id
        Reporte_Seleccionado()
    End Sub
End Class
