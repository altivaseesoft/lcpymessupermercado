Imports System.data.SqlClient
Imports System.Windows.Forms

Public Class Comisionista
    Inherits System.Windows.Forms.Form
    Dim PMU As New PerfilModulo_Class
    Dim buscando As Boolean


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents adComisionistas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents PanelDatosBasicos As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Txtobserv As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Txtnombre As System.Windows.Forms.TextBox
    Friend WithEvents Txtcedula As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents DsComisionistas1 As LcPymes_5._2.dsComisionistas
    Friend WithEvents txtCelular As System.Windows.Forms.TextBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEditar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton5 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton6 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Comisionista))
        Me.Label15 = New System.Windows.Forms.Label
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adComisionistas = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Label9 = New System.Windows.Forms.Label
        Me.PanelDatosBasicos = New System.Windows.Forms.Panel
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtCelular = New System.Windows.Forms.TextBox
        Me.DsComisionistas1 = New LcPymes_5._2.dsComisionistas
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtTelefono = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtEmpresa = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Txtobserv = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Txtnombre = New System.Windows.Forms.TextBox
        Me.Txtcedula = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEditar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton5 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton6 = New System.Windows.Forms.ToolBarButton
        Me.PanelDatosBasicos.SuspendLayout()
        CType(Me.DsComisionistas1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Image = CType(resources.GetObject("Label15.Image"), System.Drawing.Image)
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(-184, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(998, 40)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Formulario de Comisionistas"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'adComisionistas
        '
        Me.adComisionistas.DeleteCommand = Me.SqlDeleteCommand1
        Me.adComisionistas.InsertCommand = Me.SqlInsertCommand1
        Me.adComisionistas.SelectCommand = Me.SqlSelectCommand1
        Me.adComisionistas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Comisionista", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Empresa", "Empresa"), New System.Data.Common.DataColumnMapping("Telefono", "Telefono"), New System.Data.Common.DataColumnMapping("Correo", "Correo"), New System.Data.Common.DataColumnMapping("Celular", "Celular"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo")})})
        Me.adComisionistas.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Comisionista WHERE (Codigo = @Original_Codigo) AND (Cedula = @Origina" & _
        "l_Cedula) AND (Celular = @Original_Celular) AND (Correo = @Original_Correo) AND " & _
        "(Empresa = @Original_Empresa) AND (Nombre = @Original_Nombre) AND (Observaciones" & _
        " = @Original_Observaciones) AND (Telefono = @Original_Telefono)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Celular", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Celular", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Correo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Correo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Empresa", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Empresa", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Comisionista(Nombre, Cedula, Empresa, Telefono, Correo, Celular, Obse" & _
        "rvaciones) VALUES (@Nombre, @Cedula, @Empresa, @Telefono, @Correo, @Celular, @Ob" & _
        "servaciones); SELECT Nombre, Cedula, Empresa, Telefono, Correo, Celular, Observa" & _
        "ciones, Codigo FROM Comisionista WHERE (Codigo = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 150, "Nombre"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 15, "Cedula"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 150, "Empresa"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono", System.Data.SqlDbType.VarChar, 15, "Telefono"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Correo", System.Data.SqlDbType.VarChar, 50, "Correo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Celular", System.Data.SqlDbType.VarChar, 50, "Celular"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Nombre, Cedula, Empresa, Telefono, Correo, Celular, Observaciones, Codigo " & _
        "FROM Comisionista"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Comisionista SET Nombre = @Nombre, Cedula = @Cedula, Empresa = @Empresa, T" & _
        "elefono = @Telefono, Correo = @Correo, Celular = @Celular, Observaciones = @Obse" & _
        "rvaciones WHERE (Codigo = @Original_Codigo) AND (Cedula = @Original_Cedula) AND " & _
        "(Celular = @Original_Celular) AND (Correo = @Original_Correo) AND (Empresa = @Or" & _
        "iginal_Empresa) AND (Nombre = @Original_Nombre) AND (Observaciones = @Original_O" & _
        "bservaciones) AND (Telefono = @Original_Telefono); SELECT Nombre, Cedula, Empres" & _
        "a, Telefono, Correo, Celular, Observaciones, Codigo FROM Comisionista WHERE (Cod" & _
        "igo = @Codigo)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 150, "Nombre"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 15, "Cedula"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Empresa", System.Data.SqlDbType.VarChar, 150, "Empresa"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono", System.Data.SqlDbType.VarChar, 15, "Telefono"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Correo", System.Data.SqlDbType.VarChar, 50, "Correo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Celular", System.Data.SqlDbType.VarChar, 50, "Celular"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Celular", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Celular", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Correo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Correo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Empresa", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Empresa", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.White
        Me.Label9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsComisionistas1, "Comisionista.Codigo"))
        Me.Label9.ForeColor = System.Drawing.Color.Red
        Me.Label9.Location = New System.Drawing.Point(32, 21)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 16)
        Me.Label9.TabIndex = 74
        Me.Label9.Text = "00000000"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PanelDatosBasicos
        '
        Me.PanelDatosBasicos.BackColor = System.Drawing.Color.Silver
        Me.PanelDatosBasicos.Controls.Add(Me.Label7)
        Me.PanelDatosBasicos.Controls.Add(Me.txtCelular)
        Me.PanelDatosBasicos.Controls.Add(Me.Label6)
        Me.PanelDatosBasicos.Controls.Add(Me.txtEmail)
        Me.PanelDatosBasicos.Controls.Add(Me.Label5)
        Me.PanelDatosBasicos.Controls.Add(Me.txtTelefono)
        Me.PanelDatosBasicos.Controls.Add(Me.Label1)
        Me.PanelDatosBasicos.Controls.Add(Me.txtEmpresa)
        Me.PanelDatosBasicos.Controls.Add(Me.Label3)
        Me.PanelDatosBasicos.Controls.Add(Me.Txtobserv)
        Me.PanelDatosBasicos.Controls.Add(Me.Label4)
        Me.PanelDatosBasicos.Controls.Add(Me.Txtnombre)
        Me.PanelDatosBasicos.Controls.Add(Me.Txtcedula)
        Me.PanelDatosBasicos.Controls.Add(Me.Label2)
        Me.PanelDatosBasicos.Location = New System.Drawing.Point(0, 40)
        Me.PanelDatosBasicos.Name = "PanelDatosBasicos"
        Me.PanelDatosBasicos.Size = New System.Drawing.Size(624, 144)
        Me.PanelDatosBasicos.TabIndex = 75
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Location = New System.Drawing.Point(504, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(112, 15)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Celular"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtCelular
        '
        Me.txtCelular.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCelular.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsComisionistas1, "Comisionista.Celular"))
        Me.txtCelular.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCelular.ForeColor = System.Drawing.Color.Blue
        Me.txtCelular.Location = New System.Drawing.Point(504, 64)
        Me.txtCelular.Name = "txtCelular"
        Me.txtCelular.Size = New System.Drawing.Size(112, 13)
        Me.txtCelular.TabIndex = 12
        Me.txtCelular.Text = ""
        '
        'DsComisionistas1
        '
        Me.DsComisionistas1.DataSetName = "dsComisionistas"
        Me.DsComisionistas1.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(386, 80)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(230, 15)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "E-mail"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtEmail
        '
        Me.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtEmail.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsComisionistas1, "Comisionista.Correo"))
        Me.txtEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.ForeColor = System.Drawing.Color.Blue
        Me.txtEmail.Location = New System.Drawing.Point(386, 96)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(230, 13)
        Me.txtEmail.TabIndex = 10
        Me.txtEmail.Text = ""
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(384, 48)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 15)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Tel�fono"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtTelefono
        '
        Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTelefono.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsComisionistas1, "Comisionista.Telefono"))
        Me.txtTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelefono.ForeColor = System.Drawing.Color.Blue
        Me.txtTelefono.Location = New System.Drawing.Point(384, 64)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(112, 13)
        Me.txtTelefono.TabIndex = 8
        Me.txtTelefono.Text = ""
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(5, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(371, 15)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Empresa"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtEmpresa
        '
        Me.txtEmpresa.AccessibleDescription = ""
        Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmpresa.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsComisionistas1, "Comisionista.Empresa"))
        Me.txtEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpresa.ForeColor = System.Drawing.Color.Blue
        Me.txtEmpresa.Location = New System.Drawing.Point(5, 64)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(371, 13)
        Me.txtEmpresa.TabIndex = 6
        Me.txtEmpresa.Text = ""
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(5, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(371, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Nombre"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Txtobserv
        '
        Me.Txtobserv.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Txtobserv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txtobserv.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsComisionistas1, "Comisionista.Observaciones"))
        Me.Txtobserv.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtobserv.ForeColor = System.Drawing.Color.Blue
        Me.Txtobserv.Location = New System.Drawing.Point(6, 96)
        Me.Txtobserv.Multiline = True
        Me.Txtobserv.Name = "Txtobserv"
        Me.Txtobserv.Size = New System.Drawing.Size(370, 40)
        Me.Txtobserv.TabIndex = 5
        Me.Txtobserv.Text = ""
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(384, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(232, 15)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "C�dula"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Txtnombre
        '
        Me.Txtnombre.AccessibleDescription = ""
        Me.Txtnombre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Txtnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txtnombre.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsComisionistas1, "Comisionista.Nombre"))
        Me.Txtnombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtnombre.ForeColor = System.Drawing.Color.Blue
        Me.Txtnombre.Location = New System.Drawing.Point(5, 25)
        Me.Txtnombre.Name = "Txtnombre"
        Me.Txtnombre.Size = New System.Drawing.Size(371, 13)
        Me.Txtnombre.TabIndex = 0
        Me.Txtnombre.Text = ""
        '
        'Txtcedula
        '
        Me.Txtcedula.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Txtcedula.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsComisionistas1, "Comisionista.Cedula"))
        Me.Txtcedula.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtcedula.ForeColor = System.Drawing.Color.Blue
        Me.Txtcedula.Location = New System.Drawing.Point(384, 25)
        Me.Txtcedula.Name = "Txtcedula"
        Me.Txtcedula.Size = New System.Drawing.Size(232, 13)
        Me.Txtcedula.TabIndex = 1
        Me.Txtcedula.Text = ""
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(6, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(370, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Observaciones"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarEditar, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton5, Me.ToolBarButton6})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 188)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(632, 58)
        Me.ToolBar1.TabIndex = 77
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Text = "Nuevo"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Text = "Buscar"
        '
        'ToolBarEditar
        '
        Me.ToolBarEditar.Enabled = False
        Me.ToolBarEditar.ImageIndex = 5
        Me.ToolBarEditar.Text = "Editar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.Enabled = False
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Text = "Registrar"
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.Enabled = False
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Text = "Eliminar"
        '
        'ToolBarButton5
        '
        Me.ToolBarButton5.ImageIndex = 7
        Me.ToolBarButton5.Text = "Imprimir"
        '
        'ToolBarButton6
        '
        Me.ToolBarButton6.ImageIndex = 6
        Me.ToolBarButton6.Text = "Cerrar"
        '
        'Comisionista
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(632, 246)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.PanelDatosBasicos)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label15)
        Me.Name = "Comisionista"
        Me.Text = "Comisionista"
        Me.PanelDatosBasicos.ResumeLayout(False)
        CType(Me.DsComisionistas1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Comisionista_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Establece la conexi�n a la base de datos al cargar el formulario
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            PanelDatosBasicos.Enabled = False
            'Valores Predeterminados
            DsComisionistas1.Comisionista.CodigoColumn.AutoIncrement = True
            DsComisionistas1.Comisionista.CodigoColumn.AutoIncrementSeed = -1
            DsComisionistas1.Comisionista.CodigoColumn.AutoIncrementStep = -1

            DsComisionistas1.Comisionista.NombreColumn.DefaultValue = ""
            DsComisionistas1.Comisionista.CedulaColumn.DefaultValue = ""
            DsComisionistas1.Comisionista.EmpresaColumn.DefaultValue = ""
            DsComisionistas1.Comisionista.TelefonoColumn.DefaultValue = ""
            DsComisionistas1.Comisionista.CorreoColumn.DefaultValue = ""
            DsComisionistas1.Comisionista.CelularColumn.DefaultValue = ""
            DsComisionistas1.Comisionista.ObservacionesColumn.DefaultValue = ""

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Nuevo()
        Try
            If ToolBar1.Buttons(0).Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
                ToolBar1.Buttons(0).Text = "Cancelar"
                ToolBar1.Buttons(0).ImageIndex = 8
                ToolBar1.Buttons(3).Enabled = True
                PanelDatosBasicos.Enabled = True
                BindingContext(DsComisionistas1, "Comisionista").CancelCurrentEdit()
                BindingContext(DsComisionistas1, "Comisionista").AddNew()
                Txtnombre.Focus()
            Else
                BindingContext(DsComisionistas1, "Comisionista").CancelCurrentEdit()
                DsComisionistas1.Comisionista.Clear()
                PanelDatosBasicos.Enabled = False
                ToolBar1.Buttons(0).Text = "Nuevo"
                ToolBar1.Buttons(0).ImageIndex = 0
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub


    Function LlenarCominisionista(ByVal Id As Integer)
        Dim cnnv As SqlConnection = Nothing
        Dim dt As New System.Data.DataTable
        Dim cConexion As New Conexion
        'Dentro de un Try/Catch por si se produce un error
        Try
            '''''''''LLENAR COMISIONISTAS''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = GetSetting("SeeSoft", "SeePos", "CONEXION")
            cnnv = New SqlConnection(sConn)
            cnnv.Open()
            'Creamos el comando para la consulta
            Dim cmdv As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM Comisionista WHERE (Codigo = @Id_Codigo)"
            cmdv.CommandText = sel
            cmdv.Connection = cnnv
            cmdv.CommandType = System.Data.CommandType.Text
            cmdv.CommandTimeout = 90
            'Los par�metros usados en la cadena de la consulta 
            cmdv.Parameters.Add(New SqlParameter("@Id_Codigo", System.Data.SqlDbType.BigInt))
            cmdv.Parameters("@Id_Codigo").Value = Id
            'Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim dv As New SqlDataAdapter
            dv.SelectCommand = cmdv
            ' Llenamos la tabla
            dv.Fill(DsComisionistas1, "Comisionista")
        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Function

    Private Function Buscar()
        Dim funcion As New cFunciones
        Dim Id As Integer
        Dim con As String = GetSetting("SeeSoft", "SeePos", "Conexion")
        Try
            DsComisionistas1.Comisionista.Clear()
            Id = funcion.BuscarDatos("Select Codigo, Nombre from Comisionista", "Nombre", "Buscar por nombre del comisionista ...", con)
            buscando = True
            If Id = Nothing Then ' si se dio en el boton de cancelar
                buscando = False
                Exit Function
            End If

            LlenarCominisionista(Id)
            'nuevo
            ToolBar1.Buttons(0).Enabled = True
            'buscar
            ToolBar1.Buttons(1).Enabled = True
            'editar 
            ToolBar1.Buttons(2).Enabled = True
            'registrar
            ToolBar1.Buttons(3).Enabled = False
            'eliminar
            ToolBar1.Buttons(4).Enabled = True
            'Imprimir
            ToolBar1.Buttons(5).Enabled = True
            'Cerrar
            ToolBar1.Buttons(6).Enabled = True

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function

    Private Function Guardar()
        If Txtnombre.Text = "" Then
            MsgBox("Debe incluir el nombre del comisionista", MsgBoxStyle.Information, "Atenci�n...")
            Exit Function
        End If

        'If Txtcedula.Text = "" Then
        '    MsgBox("Debe ingresar el n�mero de c�dula del comisionista", MsgBoxStyle.Information, "Atenci�n...")
        '    Exit Function
        'End If

        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            'finaliza la edici�n
            adComisionistas.UpdateCommand.Transaction = Trans
            adComisionistas.InsertCommand.Transaction = Trans
            adComisionistas.DeleteCommand.Transaction = Trans
            BindingContext(DsComisionistas1, "Comisionista").EndCurrentEdit()
            adComisionistas.Update(DsComisionistas1, "Comisionista")
            Trans.Commit()
            DsComisionistas1.AcceptChanges()
            MsgBox("Datos Guardados Satisfactoriamente", MsgBoxStyle.Information)
            ToolBar1.Buttons(0).Text = "Nuevo"
            ToolBar1.Buttons(0).ImageIndex = 0
            PanelDatosBasicos.Enabled = False
            'nuevo
            ToolBar1.Buttons(0).Enabled = True
            'buscar
            ToolBar1.Buttons(1).Enabled = True
            'Editar
            ToolBar1.Buttons(2).Enabled = False
            'registrar
            ToolBar1.Buttons(3).Enabled = False
            'eliminar
            ToolBar1.Buttons(4).Enabled = False
            'Imprimir
            ToolBar1.Buttons(5).Enabled = True
            'cerrar
            ToolBar1.Buttons(6).Enabled = True
            DsComisionistas1.Comisionista.Clear()
        Catch ex As Exception
            MsgBox(ex.ToString)
            Trans.Rollback()
        End Try
    End Function

    Private Sub Txtnombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtnombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtcedula.Focus()
        End If
    End Sub

    Private Sub Txtcedula_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtcedula.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtEmpresa.Focus()
        End If
    End Sub

    Private Sub txtEmpresa_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtEmpresa.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtTelefono.Focus()
        End If
    End Sub

    Private Sub txtTelefono_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTelefono.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtCelular.Focus()
        End If
    End Sub

    Private Sub txtCelular_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCelular.KeyDown
        If e.KeyCode = Keys.Enter Then
            Txtobserv.Focus()
        End If
    End Sub

    Private Sub Txtobserv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtobserv.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtEmail.Focus()
        End If
    End Sub

    Private Sub txtTelefono_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "-"c) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Function Editar()
        PanelDatosBasicos.Enabled = True
        'nuevo
        ToolBar1.Buttons(0).Enabled = True
        'buscar
        ToolBar1.Buttons(1).Enabled = False
        'editar
        ToolBar1.Buttons(2).Enabled = False
        'registrar
        ToolBar1.Buttons(3).Enabled = True
        'eliminar
        ToolBar1.Buttons(4).Enabled = True
        'Imprimir
        ToolBar1.Buttons(5).Enabled = False
        'Cerrar
        ToolBar1.Buttons(6).Enabled = True
    End Function

    Private Sub eliminar()
        Try
            If Me.BindingContext(DsComisionistas1, "Comisionista").Count > 0 Then
                Dim Cconexion As New Conexion
                Dim strDel As Integer = BindingContext(DsComisionistas1, "Comisionista").Current("Codigo")
                Dim rs As SqlDataReader
                Dim Resultado As String
                If Txtnombre.Text <> "" Then
                    If MessageBox.Show("�Desea eliminar este comisionista?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.No Then Exit Sub
                    Resultado = Cconexion.SlqExecute(Cconexion.Conectar("SeePos"), "Delete from Comisionista where Codigo = " & strDel)
                    If Resultado = vbNullString Then
                        MessageBox.Show("El comisionista fue eliminado", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        DsComisionistas1.Comisionista.Clear()
                        PanelDatosBasicos.Enabled = False
                        'nuevo
                        ToolBar1.Buttons(0).Enabled = True
                        'buscar
                        ToolBar1.Buttons(1).Enabled = True
                        'editar
                        ToolBar1.Buttons(2).Enabled = False
                        'registrar
                        ToolBar1.Buttons(3).Enabled = False
                        'eliminar
                        ToolBar1.Buttons(4).Enabled = False
                        'imprimir
                        ToolBar1.Buttons(5).Enabled = True
                        'Cerrar
                        ToolBar1.Buttons(6).Enabled = True
                    Else
                        MessageBox.Show(Resultado)
                        Exit Sub
                    End If
                Else
                    MessageBox.Show("No hay comisionistas a eliminar ", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                MessageBox.Show("No hay comisionistas a eliminar ", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error...")
        End Try
    End Sub

    Private Function imprimir()
        Try
            Dim Imprimir_Comisionista As New ListadoComisionistas
            CrystalReportsConexion.LoadShow(Imprimir_Comisionista, MdiParent, SqlConnection1.ConnectionString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Atenci�n...")
        End Try
    End Function

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Nuevo()
            Case 2 : Buscar()
            Case 3 : Editar()
            Case 4 : Guardar()
            Case 5 : eliminar()
            Case 6 : imprimir()
            Case 7 : Close()
        End Select
    End Sub
End Class
