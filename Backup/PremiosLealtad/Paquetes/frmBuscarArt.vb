Imports System.Windows.Forms
Imports System.Data

Public Class frmBuscarArt
    Inherits System.Windows.Forms.Form
    Dim CodigoArticulo As String
    Dim Sep As Char

    Public Cantidad As Double
    Public Codigo As Integer
    Public Descripcion As String

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btAceptar As System.Windows.Forms.Button
    Friend WithEvents txCodigo As System.Windows.Forms.TextBox
    Friend WithEvents btF1 As System.Windows.Forms.Button
    Friend WithEvents lbMensaje As System.Windows.Forms.Label
    Friend WithEvents DtsPaquetes1 As LcPymes_5._2.dtsPaquetes
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.txCantidad = New System.Windows.Forms.TextBox
        Me.btAceptar = New System.Windows.Forms.Button
        Me.txCodigo = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btF1 = New System.Windows.Forms.Button
        Me.lbMensaje = New System.Windows.Forms.Label
        Me.DtsPaquetes1 = New LcPymes_5._2.dtsPaquetes
        CType(Me.DtsPaquetes1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(48, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cantidad : "
        '
        'txCantidad
        '
        Me.txCantidad.Location = New System.Drawing.Point(104, 16)
        Me.txCantidad.Name = "txCantidad"
        Me.txCantidad.Size = New System.Drawing.Size(184, 20)
        Me.txCantidad.TabIndex = 1
        Me.txCantidad.Text = "0"
        Me.txCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btAceptar
        '
        Me.btAceptar.Location = New System.Drawing.Point(144, 120)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.TabIndex = 3
        Me.btAceptar.Text = "Aceptar"
        '
        'txCodigo
        '
        Me.txCodigo.Location = New System.Drawing.Point(104, 64)
        Me.txCodigo.Name = "txCodigo"
        Me.txCodigo.Size = New System.Drawing.Size(184, 20)
        Me.txCodigo.TabIndex = 7
        Me.txCodigo.Text = ""
        Me.txCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "C�digo Producto :"
        '
        'btF1
        '
        Me.btF1.Location = New System.Drawing.Point(304, 64)
        Me.btF1.Name = "btF1"
        Me.btF1.Size = New System.Drawing.Size(32, 24)
        Me.btF1.TabIndex = 8
        Me.btF1.Text = "F1"
        '
        'lbMensaje
        '
        Me.lbMensaje.ForeColor = System.Drawing.Color.Red
        Me.lbMensaje.Location = New System.Drawing.Point(128, 96)
        Me.lbMensaje.Name = "lbMensaje"
        Me.lbMensaje.Size = New System.Drawing.Size(192, 23)
        Me.lbMensaje.TabIndex = 9
        Me.lbMensaje.Text = "El art�culo no existe."
        Me.lbMensaje.Visible = False
        '
        'DtsPaquetes1
        '
        Me.DtsPaquetes1.DataSetName = "dtsPaquetes"
        Me.DtsPaquetes1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'frmBuscarArt
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(360, 154)
        Me.Controls.Add(Me.lbMensaje)
        Me.Controls.Add(Me.btF1)
        Me.Controls.Add(Me.txCodigo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btAceptar)
        Me.Controls.Add(Me.txCantidad)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(376, 192)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(376, 192)
        Me.Name = "frmBuscarArt"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Buscar Art�culo"
        CType(Me.DtsPaquetes1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub frmBuscarArt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Sep = Application.CurrentCulture.NumberFormat.NumberDecimalSeparator
    End Sub

    Private Sub btF1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btF1.Click
        Me.spAgregarArticulo()
    End Sub
    Private Sub spAgregarArticulo()
        Dim BuscarArticulo As New FrmBuscarArticulo
        BuscarArticulo.ShowDialog()
        If BuscarArticulo.Cancelado Then
            Me.txCodigo.Focus()
        Else
            Me.txCodigo.BackColor = BackColor.White
            Me.lbMensaje.Visible = False
            Me.fnValidarCodigo(BuscarArticulo.Codigo)
        End If
    End Sub
    Function fnCantidadPuntos(ByVal cadena As String, ByVal caracter As String) As Integer
        Dim i As Integer, num As Integer
        For i = 1 To Len(cadena)
            If Mid(cadena, i, 1) = caracter Then num = num + 1
        Next
        fnCantidadPuntos = num
    End Function
    Private Sub txCantidad_KeyPress(ByVal sender As System.Object, ByVal e As KeyPressEventArgs) Handles txCantidad.KeyPress
        Dim texto As String = Me.txCantidad.Text
        ' If texto.Equals("0") Then texto = ""
        Dim cantidad As Integer
        cantidad = fnCantidadPuntos(texto + e.KeyChar, Sep)
        If cantidad <= 1 And Not Mid(texto + e.KeyChar, 1).Equals(Sep) Then
            If Not (Char.IsNumber(e.KeyChar) Or e.KeyChar.Equals(Sep) Or Char.IsControl(e.KeyChar)) Then
                e.Handled = True
            End If
        Else
            e.Handled = True
        End If
    End Sub
    Private Sub txCantidad_KeyUp(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles txCantidad.KeyUp
        If e.KeyCode = Keys.Enter Then
            If Me.fnValidarCantidad Then
                Me.txCodigo.Focus()
            End If

        End If
    End Sub
    Private Sub txCodigo_KeyPress(ByVal sender As System.Object, ByVal e As KeyPressEventArgs) Handles txCodigo.KeyPress
        If IsNumeric(e.KeyChar) Then
            e.Handled = False
        End If
    End Sub
    Private Sub txCodigo_KeyPress(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles txCodigo.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.spAceptar()
        End If
        If e.KeyCode = Keys.F1 Then
            Me.spAgregarArticulo()
        End If
    End Sub
    Private Function fnValidarCodigo(ByVal _Codigo As Integer) As Boolean
        Dim sql As String
        Dim dt As New DataTable
        Dim Fx As New cFunciones

        sql = " SELECT [Codigo] ,[Descripcion] FROM [Seepos].[dbo].[Inventario] where [Codigo] = " & _Codigo
        Fx.Llenar_Tabla_Generico(sql, dt, GetSetting("SeeSOFT", "SeePos", "Conexion"))

        If dt.Rows.Count > 0 Then
            Me.txCodigo.Text = dt.Rows(0).Item("Codigo")
            Me.Codigo = dt.Rows(0).Item("Codigo")
            Me.Descripcion = dt.Rows(0).Item("Descripcion")
            Me.Cantidad = Me.txCantidad.Text
            Return True
        Else
            Return False
        End If

    End Function
    Private Function fnValidarCantidad() As Boolean
        If CDbl(Me.txCantidad.Text) <= 0 Then
            Me.txCantidad.Focus()
            Me.txCantidad.BackColor = BackColor.Bisque
            Me.lbMensaje.Text = "La cantidad no puede ser 0."
            Me.lbMensaje.Visible = True
            Return False
        Else
            Me.txCantidad.BackColor = BackColor.White
            Return True
        End If

    End Function


    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        Me.spAceptar()
    End Sub
    Private Sub spAceptar()
        If fnValidarCantidad() Then
            If Not Me.txCodigo.Text = "" Then
                If Me.fnValidarCodigo(Me.txCodigo.Text) Then
                    If Not Me.fnValidarExistencia Then
                        Me.DialogResult = DialogResult.OK
                    Else
                        Me.lbMensaje.Text = "El art�culo ya existe en el Paquete."
                        Me.lbMensaje.Visible = True
                        Me.txCodigo.BackColor = BackColor.Bisque
                    End If
                Else
                    Me.lbMensaje.Text = "El art�culo no existe."
                    Me.lbMensaje.Visible = True
                    Me.txCodigo.BackColor = BackColor.Bisque
                End If
            Else
                Me.lbMensaje.Text = "El art�culo no existe."
                Me.lbMensaje.Visible = True
                Me.txCodigo.BackColor = BackColor.Bisque
            End If
        End If
    End Sub

    Private Function fnValidarExistencia() As Boolean
        If DtsPaquetes1.PaqueteDetalle.Rows.Count > 0 Then

            For i As Integer = 0 To DtsPaquetes1.PaqueteDetalle.Rows.Count - 1
                If Not DtsPaquetes1.PaqueteDetalle(i).RowState = DataRowState.Deleted Then
                    If DtsPaquetes1.PaqueteDetalle(i).Codigo = Me.txCodigo.Text Then
                        Return True
                    End If
                End If
            Next
        End If
        Return False
    End Function


    Private Sub txCantidad_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txCantidad.TextChanged
        Dim str As String = "1234567890" & Sep
        For Each c As Char In Me.txCantidad.Text.ToCharArray()
            If InStr(1, str, c) = 0 Then
                Me.txCantidad.Text = Replace(Me.txCantidad.Text, c, "")
            End If
        Next
    End Sub
End Class
