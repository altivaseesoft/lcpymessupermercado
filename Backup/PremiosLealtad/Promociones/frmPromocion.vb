
Imports System.Data
Imports System.Windows.Forms

Public Class frmPromocion
    Inherits System.Windows.Forms.Form

    Dim usua As Usuario_Logeado
    Dim PMU As New PerfilModulo_Class
    Dim NuevaConexion As String
    Public Nuevo As Boolean
    Public pubIdPaquete As Integer
    Public pubDescripcion As String
    Public pubAcumulado As Double
    Public pubId As Integer
    Public pubUsuario As String
    Public pubEstado As Boolean
    Dim priCambio As Boolean

    Dim strConexion As String

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbPaquetes As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btGuardar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbID As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lbNombreUsuario As System.Windows.Forms.Label
    Friend WithEvents DtsPaquetes1 As LcPymes_5._2.dtsPaquetes
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents adPaquetes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents nuAcumulado As System.Windows.Forms.NumericUpDown
    Friend WithEvents txNombrePromocion As System.Windows.Forms.TextBox
    Friend WithEvents cbEstado As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.txNombrePromocion = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cbPaquetes = New System.Windows.Forms.ComboBox
        Me.DtsPaquetes1 = New LcPymes_5._2.dtsPaquetes
        Me.Label3 = New System.Windows.Forms.Label
        Me.nuAcumulado = New System.Windows.Forms.NumericUpDown
        Me.btGuardar = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.lbID = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lbNombreUsuario = New System.Windows.Forms.Label
        Me.adPaquetes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.cbEstado = New System.Windows.Forms.CheckBox
        CType(Me.DtsPaquetes1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nuAcumulado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(56, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Descripci�n : "
        '
        'txNombrePromocion
        '
        Me.txNombrePromocion.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txNombrePromocion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txNombrePromocion.Location = New System.Drawing.Point(152, 24)
        Me.txNombrePromocion.MaxLength = 50
        Me.txNombrePromocion.Name = "txNombrePromocion"
        Me.txNombrePromocion.Size = New System.Drawing.Size(360, 23)
        Me.txNombrePromocion.TabIndex = 2
        Me.txNombrePromocion.Text = ""
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(72, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Paquete :"
        '
        'cbPaquetes
        '
        Me.cbPaquetes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbPaquetes.DataSource = Me.DtsPaquetes1
        Me.cbPaquetes.DisplayMember = "Paquetes.Nombre"
        Me.cbPaquetes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPaquetes.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPaquetes.Location = New System.Drawing.Point(152, 72)
        Me.cbPaquetes.Name = "cbPaquetes"
        Me.cbPaquetes.Size = New System.Drawing.Size(360, 26)
        Me.cbPaquetes.TabIndex = 4
        Me.cbPaquetes.ValueMember = "Paquetes.Id"
        '
        'DtsPaquetes1
        '
        Me.DtsPaquetes1.DataSetName = "dtsPaquetes"
        Me.DtsPaquetes1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 132)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(136, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Acumulado Mayor a :"
        '
        'nuAcumulado
        '
        Me.nuAcumulado.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nuAcumulado.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nuAcumulado.Location = New System.Drawing.Point(152, 128)
        Me.nuAcumulado.Maximum = New Decimal(New Integer() {1874919424, 2328306, 0, 0})
        Me.nuAcumulado.Name = "nuAcumulado"
        Me.nuAcumulado.Size = New System.Drawing.Size(360, 23)
        Me.nuAcumulado.TabIndex = 6
        Me.nuAcumulado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btGuardar
        '
        Me.btGuardar.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btGuardar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btGuardar.Location = New System.Drawing.Point(260, 184)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(120, 23)
        Me.btGuardar.TabIndex = 7
        Me.btGuardar.Text = "Guardar"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 208)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(24, 23)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "ID: "
        '
        'lbID
        '
        Me.lbID.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbID.Location = New System.Drawing.Point(32, 208)
        Me.lbID.Name = "lbID"
        Me.lbID.Size = New System.Drawing.Size(24, 23)
        Me.lbID.TabIndex = 11
        Me.lbID.Text = "0"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(456, 184)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 23)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Usuario Creador: "
        '
        'lbNombreUsuario
        '
        Me.lbNombreUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbNombreUsuario.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbNombreUsuario.Location = New System.Drawing.Point(456, 208)
        Me.lbNombreUsuario.Name = "lbNombreUsuario"
        Me.lbNombreUsuario.Size = New System.Drawing.Size(176, 23)
        Me.lbNombreUsuario.TabIndex = 13
        Me.lbNombreUsuario.Text = "Nombre"
        '
        'adPaquetes
        '
        Me.adPaquetes.SelectCommand = Me.SqlSelectCommand1
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id, Nombre FROM tb_S_Paquetes"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'cbEstado
        '
        Me.cbEstado.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbEstado.Location = New System.Drawing.Point(544, 24)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Size = New System.Drawing.Size(80, 24)
        Me.cbEstado.TabIndex = 14
        Me.cbEstado.Text = "Desactivar"
        '
        'frmPromocion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(632, 242)
        Me.Controls.Add(Me.cbEstado)
        Me.Controls.Add(Me.lbNombreUsuario)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lbID)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.nuAcumulado)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbPaquetes)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txNombrePromocion)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(648, 280)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(648, 280)
        Me.Name = "frmPromocion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promoci�n"
        CType(Me.DtsPaquetes1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nuAcumulado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmPromocion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        Me.spIniciarForm()
    End Sub
    Private Sub frmPromocion_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Me.priCambio = True Then
            If MsgBox("�Desea guardar los cambios efectuados?", MsgBoxStyle.YesNo, "Atenci�n") = MsgBoxResult.Yes Then
                If Not Me.fnGuardar() Then
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Private Sub spIniciarForm()
        Dim sql As String
        Dim Fx As cFunciones
        strConexion = SqlConnection1.ConnectionString

        Try
            ' Cargar Paquetes 
            SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
            sql = "SELECT Id, Nombre FROM tb_S_Paquetes WHERE (Estado = 'False')"
            Fx.Cargar_Tabla_Generico(Me.adPaquetes, sql, strConexion)
            Me.adPaquetes.Fill(DtsPaquetes1.Tables("Paquetes"))

            If Nuevo Then
                Me.lbNombreUsuario.Text = ""
                Me.lbID.Text = ""
                Me.cbEstado.Enabled = False
            Else
                Me.txNombrePromocion.Text = Me.pubDescripcion
                Me.cbPaquetes.SelectedValue = Me.pubIdPaquete
                Me.nuAcumulado.Value = Me.pubAcumulado
                Me.lbID.Text = Me.pubId
                Me.lbNombreUsuario.Text = Me.pubUsuario
                Me.cbEstado.Checked = Me.pubEstado

            End If
            Me.priCambio = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function fnGuardar() As Boolean
        Dim sql As New SqlClient.SqlCommand
        Dim cmd As New SqlClient.SqlCommand
        Dim SQlInsert As String
        Dim Fx As cFunciones

        If fnValidarCampos() Then
            Try
                If Not Nuevo Then
                    sql.CommandText = "UPDATE [Seepos].[dbo].[tb_S_Promociones] SET [Nombre] = '" & Me.txNombrePromocion.Text & _
                    "' ,[MontoAcumulado] = '" & Me.nuAcumulado.Value & "' ,[Estado] = '" & Me.cbEstado.Checked & _
                    "' ,[IdPaquete] = '" & Me.cbPaquetes.SelectedValue & "'  WHERE Id = " & Me.pubId
                    Fx.fnEjecutar(sql, strConexion)
                Else
                    Dim sqlPromocion As String
                    Dim dt As New DataTable

                    sqlPromocion = "INSERT INTO [Seepos].[dbo].[tb_S_Promociones] ([Nombre] ,[Fecha] ,[MontoAcumulado], [IdUsuario] ,[Estado], [IdPaquete]) " & _
                                      " VALUES ( '" & Me.txNombrePromocion.Text & "','" & Now & "','" & Me.nuAcumulado.Value & "','" & usua.Cedula & "','" & Me.cbEstado.Checked & "','" & Me.cbPaquetes.SelectedValue & "')"
                    sql.CommandText = sqlPromocion
                    Fx.fnEjecutar(sql, strConexion)

                    sqlPromocion = "select isNull(Max(Id),0) as Id from tb_S_Promociones"
                    Fx.Llenar_Tabla_Generico(sqlPromocion, dt, strConexion)
                    If dt.Rows.Count > 0 Then
                        Me.lbID.Text = dt.Rows(0).Item("Id")
                        Me.pubId = dt.Rows(0).Item("Id")
                        Me.lbNombreUsuario.Text = usua.Nombre
                    End If
                End If
                If Nuevo Then
                    Me.Text = "Editar Promoci�n"
                    Me.cbEstado.Enabled = True
                    MsgBox("La informaci�n se guard� exitosamente.", MsgBoxStyle.Information, "Atenci�n")
                Else
                    MsgBox("La informaci�n se actualiz� exitosamente.", MsgBoxStyle.Information, "Atenci�n")
                End If
                Nuevo = False
               Me.priCambio = False
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
          
        Else
            Return False
        End If

    End Function

    Private Function fnValidarCampos()
        Dim sql As String
        Dim Fx As New cFunciones
        Dim dt As New DataTable
        Try
            ' Validaci�n de Nombre --------------------------------
            If Replace(Me.txNombrePromocion.Text, " ", "") = "" Then
                Me.txNombrePromocion.BackColor = BackColor.Bisque
                Me.txNombrePromocion.Focus()
                MsgBox("Por favor, completar los campos obligatorios.", MsgBoxStyle.Information, "Atenci�n")
                Return False
            Else
                Me.txNombrePromocion.BackColor = BackColor.White
            End If

            If Nuevo Then
                sql = "Select * From tb_S_Promociones where  Nombre collate SQL_Latin1_General_Cp1_CI_AI = '" & Me.txNombrePromocion.Text & "'"
            Else
                sql = "Select * From tb_S_Promociones where Id <> " & Me.pubId & " AND Nombre collate SQL_Latin1_General_Cp1_CI_AI = '" & Me.txNombrePromocion.Text & "'"
            End If

            Fx.Llenar_Tabla_Generico(sql, dt, strConexion)

            If dt.Rows.Count > 0 Then
                Me.txNombrePromocion.BackColor = BackColor.Bisque
                Me.txNombrePromocion.Focus()
                MsgBox("Los datos ya existen.", MsgBoxStyle.Information, "Atenci�n")
                Return False
            Else
                Me.txNombrePromocion.BackColor = BackColor.White
            End If


            '-------------------------------------------------------
            'Validaci�n de acumulado
            If Me.nuAcumulado.Value <= 0 Then
                Me.nuAcumulado.BackColor = BackColor.Bisque
                Me.nuAcumulado.Focus()
                MsgBox("El monto acumulado debe ser mayor a 0.", MsgBoxStyle.Information, "Atenci�n")
                Return False
            Else
                Me.nuAcumulado.BackColor = BackColor.White
            End If

            If Nuevo Then
                sql = "Select MontoAcumulado From tb_S_Promociones where  MontoAcumulado = " & Me.nuAcumulado.Value
            Else
                sql = "Select MontoAcumulado From tb_S_Promociones where Id <> " & Me.pubId & " AND MontoAcumulado = " & Me.nuAcumulado.Value
            End If

            dt.Clear()
            Fx.Llenar_Tabla_Generico(sql, dt, strConexion)

            If dt.Rows.Count > 0 Then
                Me.nuAcumulado.BackColor = BackColor.Bisque
                Me.nuAcumulado.Focus()
                MsgBox("Los datos ya existen.", MsgBoxStyle.Information, "Atenci�n")
                Return False
            Else
                Me.nuAcumulado.BackColor = BackColor.White
            End If

            If Me.cbPaquetes.SelectedValue = Nothing Then
                Me.cbPaquetes.BackColor = BackColor.Bisque
                MsgBox("Por Favor, ingrese un paquete.", MsgBoxStyle.Information, "Atenci�n")
                Return False
            Else
                Me.cbPaquetes.BackColor = BackColor.White
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return True
    End Function

    Private Sub txNombrePromocion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txNombrePromocion.TextChanged
        Dim str As String = "01234567890abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ �����"
        For Each c As Char In Me.txNombrePromocion.Text.ToCharArray()
            If InStr(1, str, c) = 0 Then
                Me.txNombrePromocion.Text = Replace(Me.txNombrePromocion.Text, c, "")
            End If
        Next
        Me.priCambio = True
    End Sub
    Private Sub txNombrePromocion_KeyUp(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles txNombrePromocion.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.cbPaquetes.Focus()
        End If
    End Sub

    Private Sub cbPaquetes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPaquetes.SelectedIndexChanged
        Me.priCambio = True
    End Sub
    Private Sub cbPaquetes_KeyUp(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles cbPaquetes.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.nuAcumulado.Focus()
        End If
    End Sub

    Private Sub nuAcumulado_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nuAcumulado.ValueChanged
        Me.priCambio = True
    End Sub
    Private Sub nuAcumulado_KeyUp(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles nuAcumulado.KeyDown

        If e.KeyCode = Keys.Enter Then
            Me.btGuardar.Focus()
        End If
    End Sub
    Private Sub nuAcumulado_KeyUp(ByVal sender As System.Object, ByVal e As KeyPressEventArgs) Handles nuAcumulado.KeyPress
        If Char.IsDigit(e.KeyChar) And (Not e.KeyChar = "," Or Not e.KeyChar = ".") Or Char.IsControl(e.KeyChar) Then
            Me.priCambio = True
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub btGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Me.fnGuardar()
    End Sub

    Private Sub cbEstado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbEstado.CheckedChanged
        Me.priCambio = True
    End Sub

    Private Sub txNombrePromocion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txNombrePromocion.KeyPress

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            Exit Sub
        End If
        If Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
            Exit Sub
        End If
        If Not (Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub
End Class
