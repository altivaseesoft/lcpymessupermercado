'****************************************************************************
'@utor:
'       ing.rolando obando rojas.
'****************************************************************************
Imports System.Data
Namespace Valida_Asientos
    Public Class Puede
        Private Con As String = GetSetting("SeeSoft", "SeePOs", "Conexion")
        Private dt As New DataTable
        Private Mensaje_Anular As String = ""
        Private strSQL As String = ""

        Sub New()
            Me.Mensaje_Anular = "Advertencia!!!" & vbCrLf _
            & "El documento no se puede anular debido a que el Asiento {0} esta Mayorizado"
        End Sub

        Public Function Anular(ByVal _id As String, ByVal _m As Modulos)
            Select Case _m
                Case Modulos.AbonocPagar
                    Me.strSQL = "SELECT  ISNULL(AC.NumAsiento, '') AS NumAsiento, ISNULL(AC.Mayorizado,0) AS Mayorizado FROM SeePOs.dbo.AbonocPagar as A INNER JOIN Bancos.dbo.Cheques as C ON A.Documento = C.Num_Cheque and C.Tipo = A.TipoDocumento LEFT JOIN Contabilidad.dbo.AsientosContables AS AC ON C.Asiento COLLATE Traditional_Spanish_CI_AS = AC.NumAsiento WHERE A.Id_Abonocpagar = " & _id
                Case Modulos.DevolucionCompra
                    Me.strSQL = "SELECT A.NumAsiento, A.Mayorizado FROM SeePOs.dbo.devoluciones_Compras as D INNER JOIN Contabilidad.dbo.AsientosContables as A on A.Modulo = 'DEV COM' AND D.Devolucion = A.NumDoc WHERE D.Devolucion = " & _id
                Case Modulos.AjustesInventario
                    Me.strSQL = "SELECT AC.NumAsiento, AC.Mayorizado FROM SeePOs.dbo.AjusteInventario AS A INNER JOIN Contabilidad.dbo.AsientosContables as AC on A.AsientoEntrada COLLATE Traditional_Spanish_CI_AS = AC.NumAsiento or A.AsientoSalida COLLATE Traditional_Spanish_CI_AS = AC.NumAsiento WHERE A.Consecutivo = " & _id
                Case Modulos.Compras
                    Me.strSQL = "select AC.NumAsiento, AC.Mayorizado from SeePOs.dbo.Compras as C inner join Contabilidad.dbo.AsientosContables as AC on C.Asiento COLLATE Traditional_Spanish_CI_AS = AC.Numasiento where C.Id_Compra = " & _id
            End Select

            cFunciones.Llenar_Tabla_Generico(Me.strSQL, dt, Me.Con)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Mayorizado") = 1 Or dt.Rows(0).Item("Mayorizado") = True Then
                    Dim frm As New frmAsiento
                    frm.msg = String.Format(Me.Mensaje_Anular, dt.Rows(0).Item("NumAsiento"))
                    frm.ShowDialog()
                    dt.Clear()
                    Return False
                End If
            End If
            dt.Clear()
            Return True
        End Function

    End Class

    Public Enum Modulos
        AbonocPagar
        DevolucionCompra
        AjustesInventario
        Compras
    End Enum

End Namespace

