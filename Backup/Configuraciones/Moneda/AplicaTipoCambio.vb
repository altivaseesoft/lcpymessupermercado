Public Class AplicaTipoCambio
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents adTipoCambio As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsAplicaTipoCambio1 As dsAplicaTipoCambio
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Public WithEvents TituloModulo As System.Windows.Forms.Label
    Friend WithEvents dtFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(AplicaTipoCambio))
        Me.Button1 = New System.Windows.Forms.Button
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adTipoCambio = New System.Data.SqlClient.SqlDataAdapter
        Me.DsAplicaTipoCambio1 = New dsAplicaTipoCambio
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.TituloModulo = New System.Windows.Forms.Label
        Me.dtFecha = New System.Windows.Forms.DateTimePicker
        Me.Label21 = New System.Windows.Forms.Label
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.DsAplicaTipoCambio1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(16, 80)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 24)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Ventas"
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=DIEGOGAMBOA;packet size=4096;integrated security=SSPI;data source=" & _
        "DIEGOGAMBOA;persist security info=False;initial catalog=Seguridad"
        '
        'adTipoCambio
        '
        Me.adTipoCambio.DeleteCommand = Me.SqlDeleteCommand1
        Me.adTipoCambio.InsertCommand = Me.SqlInsertCommand1
        Me.adTipoCambio.SelectCommand = Me.SqlSelectCommand1
        Me.adTipoCambio.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "HistoricoMoneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("IDs", "IDs"), New System.Data.Common.DataColumnMapping("Id_Moneda", "Id_Moneda"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha")})})
        Me.adTipoCambio.UpdateCommand = Me.SqlUpdateCommand1
        '
        'DsAplicaTipoCambio1
        '
        Me.DsAplicaTipoCambio1.DataSetName = "dsAplicaTipoCambio"
        Me.DsAplicaTipoCambio1.Locale = New System.Globalization.CultureInfo("es-ES")
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(128, 72)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(96, 40)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "OpcionesPago Hotel"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(368, 80)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(96, 24)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Check Out"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(248, 72)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(96, 40)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "OpcionesPago Coati"
        '
        'TituloModulo
        '
        Me.TituloModulo.BackColor = System.Drawing.Color.FromArgb(CType(56, Byte), CType(91, Byte), CType(165, Byte))
        Me.TituloModulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.TituloModulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.TituloModulo.ForeColor = System.Drawing.Color.White
        Me.TituloModulo.Image = CType(resources.GetObject("TituloModulo.Image"), System.Drawing.Image)
        Me.TituloModulo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.TituloModulo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.TituloModulo.Location = New System.Drawing.Point(0, 0)
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(488, 32)
        Me.TituloModulo.TabIndex = 59
        Me.TituloModulo.Text = "Aplicar Tipo Cambio"
        Me.TituloModulo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dtFecha
        '
        Me.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.dtFecha.Location = New System.Drawing.Point(128, 40)
        Me.dtFecha.MaxDate = New Date(2090, 1, 1, 0, 0, 0, 0)
        Me.dtFecha.MinDate = New Date(1999, 1, 1, 0, 0, 0, 0)
        Me.dtFecha.Name = "dtFecha"
        Me.dtFecha.Size = New System.Drawing.Size(88, 20)
        Me.dtFecha.TabIndex = 74
        Me.dtFecha.Value = New Date(2007, 11, 29, 0, 0, 0, 0)
        '
        'Label21
        '
        Me.Label21.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label21.Location = New System.Drawing.Point(40, 40)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(79, 16)
        Me.Label21.TabIndex = 75
        Me.Label21.Text = "Fecha"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT IDs, Id_Moneda, ValorCompra, ValorVenta, Usuario, Fecha FROM HistoricoMone" & _
        "da ORDER BY Fecha"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO HistoricoMoneda(Id_Moneda, ValorCompra, ValorVenta, Usuario, Fecha) V" & _
        "ALUES (@Id_Moneda, @ValorCompra, @ValorVenta, @Usuario, @Fecha); SELECT IDs, Id_" & _
        "Moneda, ValorCompra, ValorVenta, Usuario, Fecha FROM HistoricoMoneda WHERE (IDs " & _
        "= @@IDENTITY) ORDER BY Fecha"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Moneda", System.Data.SqlDbType.BigInt, 8, "Id_Moneda"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE HistoricoMoneda SET Id_Moneda = @Id_Moneda, ValorCompra = @ValorCompra, Va" & _
        "lorVenta = @ValorVenta, Usuario = @Usuario, Fecha = @Fecha WHERE (IDs = @Origina" & _
        "l_IDs) AND (Fecha = @Original_Fecha) AND (Id_Moneda = @Original_Id_Moneda) AND (" & _
        "Usuario = @Original_Usuario) AND (ValorCompra = @Original_ValorCompra) AND (Valo" & _
        "rVenta = @Original_ValorVenta); SELECT IDs, Id_Moneda, ValorCompra, ValorVenta, " & _
        "Usuario, Fecha FROM HistoricoMoneda WHERE (IDs = @IDs) ORDER BY Fecha"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Moneda", System.Data.SqlDbType.BigInt, 8, "Id_Moneda"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IDs", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IDs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Moneda", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IDs", System.Data.SqlDbType.BigInt, 8, "IDs"))
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM HistoricoMoneda WHERE (IDs = @Original_IDs) AND (Fecha = @Original_Fe" & _
        "cha) AND (Id_Moneda = @Original_Id_Moneda) AND (Usuario = @Original_Usuario) AND" & _
        " (ValorCompra = @Original_ValorCompra) AND (ValorVenta = @Original_ValorVenta)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IDs", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IDs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Moneda", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'AplicaTipoCambio
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(488, 158)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.dtFecha)
        Me.Controls.Add(Me.TituloModulo)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "AplicaTipoCambio"
        Me.Text = "AplicaTipoCambio"
        CType(Me.DsAplicaTipoCambio1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub AplicaTipoCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "Seguridad", "CONEXION")
        adTipoCambio.Fill(Me.DsAplicaTipoCambio1, "HistoricoMoneda")
        dtFecha.Value = Now.Date
        dtFecha.Focus()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim i As Integer
        Dim Fx As New Conexion
        Dim FechaTipoCambio As Date
        Dim FechaTipoCambio1 As Date
        For i = 0 To Me.DsAplicaTipoCambio1.HistoricoMoneda.Count - 1
            FechaTipoCambio = DsAplicaTipoCambio1.HistoricoMoneda.Rows(i).Item(5)
            FechaTipoCambio1 = FechaTipoCambio.AddDays(1)
            If dtFecha.Value >= FechaTipoCambio And dtFecha.Value < FechaTipoCambio1 Then
                Fx.UpdateRecords("Ventas", "Tipo_Cambio =" & DsAplicaTipoCambio1.HistoricoMoneda.Rows(i).Item("ValorCompra"), "Cod_Moneda = 2 and (Fecha >= '" & FechaTipoCambio & "' and Fecha <'" & FechaTipoCambio1 & "')", "Hotel")
            End If
        Next
        MsgBox("Los Tipos de Cambio a la tabla de ventas se aplicar�n correctamente", MsgBoxStyle.Information, "Atenci�n ...")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim i As Integer
        Dim Fx As New Conexion
        Dim FechaTipoCambio As Date
        Dim FechaTipoCambio1 As Date
        For i = 0 To Me.DsAplicaTipoCambio1.HistoricoMoneda.Count - 1
            FechaTipoCambio = DsAplicaTipoCambio1.HistoricoMoneda.Rows(i).Item(5)
            FechaTipoCambio1 = FechaTipoCambio.AddDays(1)
            If dtFecha.Value >= FechaTipoCambio And dtFecha.Value < FechaTipoCambio1 Then
                Fx.UpdateRecords("OpcionesDePago", "TipoCambio =" & DsAplicaTipoCambio1.HistoricoMoneda.Rows(i).Item("ValorCompra"), "CodMoneda = 2 and (Fecha >= '" & FechaTipoCambio & "' and Fecha <'" & FechaTipoCambio1 & "')", "Hotel")
            End If
        Next
        MsgBox("Los Tipos de Cambio a la tabla Opciones de Pago de Hotel se aplicar�n correctamente", MsgBoxStyle.Information, "Atenci�n ...")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim i As Integer
        Dim Fx As New Conexion
        Dim FechaTipoCambio As Date
        Dim FechaTipoCambio1 As Date
        For i = 0 To Me.DsAplicaTipoCambio1.HistoricoMoneda.Count - 1
            FechaTipoCambio = DsAplicaTipoCambio1.HistoricoMoneda.Rows(i).Item(5)
            FechaTipoCambio1 = FechaTipoCambio.AddDays(1)
            If dtFecha.Value >= FechaTipoCambio And dtFecha.Value < FechaTipoCambio1 Then
                Fx.UpdateRecords("Check_Out", "Tipo_Cambio =" & DsAplicaTipoCambio1.HistoricoMoneda.Rows(i).Item("ValorCompra"), "Cod_Moneda = 2 and (Fecha >= '" & FechaTipoCambio & "' and Fecha <'" & FechaTipoCambio1 & "')", "Hotel")
            End If
        Next
        MsgBox("Los Tipos de Cambio a la tabla Check Out se aplicar�n correctamente", MsgBoxStyle.Information, "Atenci�n ...")
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim i As Integer
        Dim Fx As New Conexion
        Dim FechaTipoCambio As Date
        Dim FechaTipoCambio1 As Date
        For i = 0 To Me.DsAplicaTipoCambio1.HistoricoMoneda.Count - 1
            FechaTipoCambio = DsAplicaTipoCambio1.HistoricoMoneda.Rows(i).Item(5)
            FechaTipoCambio1 = FechaTipoCambio.AddDays(1)
            If dtFecha.Value >= FechaTipoCambio And dtFecha.Value < FechaTipoCambio1 Then
                Fx.UpdateRecords("OpcionesDePago", "TipoCambio =" & DsAplicaTipoCambio1.HistoricoMoneda.Rows(i).Item("ValorCompra"), "CodMoneda = 2 and (Fecha >= '" & FechaTipoCambio & "' and Fecha <'" & FechaTipoCambio1 & "')", "Restaurante")
            End If
        Next
        MsgBox("Los Tipos de cambio a la tabla Opciones de Pago de Coati se aplicar�n correctamente", MsgBoxStyle.Information, "Atenci�n ...")
    End Sub
End Class
