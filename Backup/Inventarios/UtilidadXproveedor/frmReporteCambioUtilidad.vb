Imports System.Data
Imports System.Windows.Forms
Public Class frmReporteCambioUtilidad
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents FechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents lblFechaInicio As System.Windows.Forms.Label
    Friend WithEvents ButtonMostrar As System.Windows.Forms.Button
    Friend WithEvents VisorReporte As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents cbUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents rbUsuario As System.Windows.Forms.RadioButton
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents cbProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents rbProveedor As System.Windows.Forms.RadioButton
    Friend WithEvents DtsCambioUtilidad1 As LcPymes_5._2.dtsCambioUtilidad
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.FechaFinal = New System.Windows.Forms.DateTimePicker
        Me.FechaInicio = New System.Windows.Forms.DateTimePicker
        Me.lblHasta = New System.Windows.Forms.Label
        Me.lblFechaInicio = New System.Windows.Forms.Label
        Me.ButtonMostrar = New System.Windows.Forms.Button
        Me.VisorReporte = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.cbUsuario = New System.Windows.Forms.ComboBox
        Me.rbUsuario = New System.Windows.Forms.RadioButton
        Me.cbProveedor = New System.Windows.Forms.ComboBox
        Me.rbProveedor = New System.Windows.Forms.RadioButton
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.DtsCambioUtilidad1 = New LcPymes_5._2.dtsCambioUtilidad
        Me.GroupBox1.SuspendLayout()
        CType(Me.DtsCambioUtilidad1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.FechaFinal)
        Me.GroupBox1.Controls.Add(Me.FechaInicio)
        Me.GroupBox1.Controls.Add(Me.lblHasta)
        Me.GroupBox1.Controls.Add(Me.lblFechaInicio)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox1.Location = New System.Drawing.Point(-5, 26)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(184, 104)
        Me.GroupBox1.TabIndex = 98
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtro por Fecha"
        '
        'FechaFinal
        '
        Me.FechaFinal.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.FechaFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.FechaFinal.Location = New System.Drawing.Point(76, 64)
        Me.FechaFinal.Name = "FechaFinal"
        Me.FechaFinal.Size = New System.Drawing.Size(96, 20)
        Me.FechaFinal.TabIndex = 41
        Me.FechaFinal.Value = New Date(2016, 2, 18, 10, 49, 46, 183)
        '
        'FechaInicio
        '
        Me.FechaInicio.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.FechaInicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.FechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.FechaInicio.Location = New System.Drawing.Point(76, 24)
        Me.FechaInicio.Name = "FechaInicio"
        Me.FechaInicio.Size = New System.Drawing.Size(96, 20)
        Me.FechaInicio.TabIndex = 40
        Me.FechaInicio.Value = New Date(2006, 4, 10, 0, 0, 0, 0)
        '
        'lblHasta
        '
        Me.lblHasta.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lblHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblHasta.ForeColor = System.Drawing.Color.Blue
        Me.lblHasta.Location = New System.Drawing.Point(12, 64)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(56, 16)
        Me.lblHasta.TabIndex = 39
        Me.lblHasta.Text = "Hasta : "
        '
        'lblFechaInicio
        '
        Me.lblFechaInicio.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lblFechaInicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblFechaInicio.ForeColor = System.Drawing.Color.Blue
        Me.lblFechaInicio.Location = New System.Drawing.Point(12, 24)
        Me.lblFechaInicio.Name = "lblFechaInicio"
        Me.lblFechaInicio.Size = New System.Drawing.Size(56, 16)
        Me.lblFechaInicio.TabIndex = 38
        Me.lblFechaInicio.Text = "Desde : "
        '
        'ButtonMostrar
        '
        Me.ButtonMostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ButtonMostrar.Location = New System.Drawing.Point(43, 426)
        Me.ButtonMostrar.Name = "ButtonMostrar"
        Me.ButtonMostrar.Size = New System.Drawing.Size(88, 40)
        Me.ButtonMostrar.TabIndex = 97
        Me.ButtonMostrar.Text = "Mostrar"
        '
        'VisorReporte
        '
        Me.VisorReporte.ActiveViewIndex = -1
        Me.VisorReporte.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.VisorReporte.AutoScroll = True
        Me.VisorReporte.DisplayGroupTree = False
        Me.VisorReporte.ForeColor = System.Drawing.Color.Blue
        Me.VisorReporte.Location = New System.Drawing.Point(187, 2)
        Me.VisorReporte.Name = "VisorReporte"
        Me.VisorReporte.ReportSource = Nothing
        Me.VisorReporte.ShowCloseButton = False
        Me.VisorReporte.Size = New System.Drawing.Size(608, 560)
        Me.VisorReporte.TabIndex = 96
        '
        'cbUsuario
        '
        Me.cbUsuario.DataSource = Me.DtsCambioUtilidad1.Usuario
        Me.cbUsuario.DisplayMember = "Nombre"
        Me.cbUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbUsuario.Location = New System.Drawing.Point(19, 274)
        Me.cbUsuario.Name = "cbUsuario"
        Me.cbUsuario.Size = New System.Drawing.Size(160, 21)
        Me.cbUsuario.TabIndex = 95
        Me.cbUsuario.ValueMember = "Cedula"
        Me.cbUsuario.Visible = False
        '
        'rbUsuario
        '
        Me.rbUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rbUsuario.Location = New System.Drawing.Point(3, 250)
        Me.rbUsuario.Name = "rbUsuario"
        Me.rbUsuario.Size = New System.Drawing.Size(144, 24)
        Me.rbUsuario.TabIndex = 94
        Me.rbUsuario.Text = "Por Usuario"
        '
        'cbProveedor
        '
        Me.cbProveedor.DataSource = Me.DtsCambioUtilidad1.Proveedor
        Me.cbProveedor.DisplayMember = "Nombre"
        Me.cbProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbProveedor.Location = New System.Drawing.Point(19, 210)
        Me.cbProveedor.Name = "cbProveedor"
        Me.cbProveedor.Size = New System.Drawing.Size(160, 21)
        Me.cbProveedor.TabIndex = 93
        Me.cbProveedor.ValueMember = "CodigoProv"
        Me.cbProveedor.Visible = False
        '
        'rbProveedor
        '
        Me.rbProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rbProveedor.Location = New System.Drawing.Point(3, 178)
        Me.rbProveedor.Name = "rbProveedor"
        Me.rbProveedor.TabIndex = 92
        Me.rbProveedor.Text = "Por Proveedor"
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'DtsCambioUtilidad1
        '
        Me.DtsCambioUtilidad1.DataSetName = "dtsCambioUtilidad"
        Me.DtsCambioUtilidad1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'frmReporteCambioUtilidad
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(790, 564)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ButtonMostrar)
        Me.Controls.Add(Me.VisorReporte)
        Me.Controls.Add(Me.cbUsuario)
        Me.Controls.Add(Me.rbUsuario)
        Me.Controls.Add(Me.cbProveedor)
        Me.Controls.Add(Me.rbProveedor)
        Me.ForeColor = System.Drawing.Color.Blue
        Me.Name = "frmReporteCambioUtilidad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " Reporte Cambio de Utilidad"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DtsCambioUtilidad1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim NuevaConexion As String
    Dim usua As Usuario_Logeado

    Private Sub frmReporteCambioUtilidad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        Me.spCargarComboBox()
        Me.FechaInicio.Value = Now.AddDays(-15)
        Me.FechaFinal.Value = Now
    End Sub

    Private Sub spCargarComboBox()
        Dim Fx As New cFunciones

        Dim strConexion As String = SqlConnection1.ConnectionString

        Fx.Llenar_Tabla_Generico("SELECT [Cedula] ,[Nombre] FROM [Seepos].[dbo].[Usuarios] order by Nombre ", Me.DtsCambioUtilidad1.Usuario)

        Dim Fila As DataRow
        Fila = Me.DtsCambioUtilidad1.Usuario.NewRow()
        Fila("Cedula") = 0
        Fila("Nombre") = "TODOS"
        Me.DtsCambioUtilidad1.Usuario.Rows.Add(Fila)
        Me.cbUsuario.SelectedValue = 0


        Fx.Llenar_Tabla_Generico("SELECT [CodigoProv],[Nombre] FROM [Seepos].[dbo].[Proveedores] order by Nombre ", Me.DtsCambioUtilidad1.Proveedor)

        Dim Fila2 As DataRow
        Fila2 = Me.DtsCambioUtilidad1.Proveedor.NewRow()
        Fila2("CodigoProv") = 0
        Fila2("Nombre") = "TODOS"
        Me.DtsCambioUtilidad1.Proveedor.Rows.Add(Fila2)
        Me.cbProveedor.SelectedValue = 0

    End Sub

    Private Function fnValidarFechas()
        If Me.FechaInicio.Value > Me.FechaFinal.Value Then
            Return False
        End If
        Return True
    End Function

    Private Function fnValidarRadioButton()
        If Not Me.rbProveedor.Checked And Not Me.rbUsuario.Checked Then
            Return False
        End If
        Return True
    End Function

    Private Sub spMostrarPorProveedor()
        Dim Reporte As New rptReporteCambioUtilidad_Proveedor
        Dim Codigo As Integer = 0
        If Me.cbProveedor.Text.Equals("TODOS") Then Codigo = 0

        Reporte.SetParameterValue("CodigoOperador", CDbl(Me.cbProveedor.SelectedValue))
        If Me.cbProveedor.Text.Equals("TODOS") Then
            Reporte.SetParameterValue("Filtro", False)
        Else
            Reporte.SetParameterValue("Filtro", True)
        End If
        Reporte.SetParameterValue("FechaDesde", CDate(Me.FechaInicio.Value))
        Reporte.SetParameterValue("FechaHasta", CDate(Me.FechaFinal.Value))
        CrystalReportsConexion.LoadReportViewer(VisorReporte, Reporte)
        VisorReporte.Show()
    End Sub
    Private Sub spMostrarPorUsuario()
        Dim Reporte As New rptReporteCambioUtilidad_Usuario
        Reporte.SetParameterValue("CodigoOperador", Me.cbUsuario.SelectedValue)
        If Me.cbUsuario.Text.Equals("TODOS") Then
            Reporte.SetParameterValue("Filtro", False)
        Else
            Reporte.SetParameterValue("Filtro", True)
        End If
        Reporte.SetParameterValue("FechaDesde", CDate(Me.FechaInicio.Value))
        Reporte.SetParameterValue("FechaHasta", CDate(Me.FechaFinal.Value))
        CrystalReportsConexion.LoadReportViewer(VisorReporte, Reporte)
        VisorReporte.Show()
    End Sub

    Private Sub spMostrarReporte()
        Try
            If Me.fnValidarFechas Then

                If Me.fnValidarRadioButton Then
                    Try
                        Me.ButtonMostrar.Enabled = False

                        If Me.rbProveedor.Checked Then
                            Me.spMostrarPorProveedor()
                        End If

                        If Me.rbUsuario.Checked Then
                            Me.spMostrarPorUsuario()
                        End If

                        Me.WindowState = Windows.Forms.FormWindowState.Maximized
                        Me.ButtonMostrar.Enabled = True
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        Me.ButtonMostrar.Enabled = True
                    End Try
                Else
                    MsgBox("Seleccione una de las opciones del filtro.", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("La FechaDesde no puede ser mayor a FechaHasta.", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub rbProveedor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbProveedor.CheckedChanged
        If Me.rbProveedor.Checked Then
            Me.cbProveedor.Visible = True
            Me.cbUsuario.Visible = False
            Me.cbProveedor.Focus()
        End If
    End Sub
    Private Sub rrbUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbUsuario.CheckedChanged
        If Me.rbUsuario.Checked Then
            Me.cbProveedor.Visible = False
            Me.cbUsuario.Visible = True
            Me.cbUsuario.Focus()
        End If
    End Sub
    Private Sub cbOperador_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbProveedor.KeyUp
        If e.KeyCode = Keys.Enter Then
            ButtonMostrar.Focus()
        End If
    End Sub
    Private Sub cbUsuario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbUsuario.KeyUp
        If e.KeyCode = Keys.Enter Then
            ButtonMostrar.Focus()
        End If
    End Sub

    Private Sub ButtonMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMostrar.Click
        Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
        PMU = VSM(usua.Cedula, "frmUtilidadXProveedor") 'Carga los privilegios del usuario con el modulo
        If PMU.Print Then Me.spMostrarReporte() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub


End Class
