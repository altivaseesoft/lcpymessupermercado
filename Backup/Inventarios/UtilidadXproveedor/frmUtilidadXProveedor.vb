Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports CrystalDecisions.Shared
Imports System.Drawing.Printing
Imports System
Imports System.Threading
Imports System.IO
Public Class frmUtilidadXProveedor
    Inherits _2.FrmPlantilla

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents TxtNombreProv As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCodigoProv As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txClave As System.Windows.Forms.TextBox
    Friend WithEvents nuUtilidad As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txUsuarioCreador As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUtilidadXProveedor))
        Me.nuUtilidad = New System.Windows.Forms.NumericUpDown
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtObservacion = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.TxtNombreProv = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtCodigoProv = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txUsuarioCreador = New DevExpress.XtraEditors.TextEdit
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.txClave = New System.Windows.Forms.TextBox
        Me.Label36 = New System.Windows.Forms.Label
        CType(Me.nuUtilidad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.TxtNombreProv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txUsuarioCreador.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Visible = False
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(514, 262)
        Me.DataNavigator.Name = "DataNavigator"
        Me.DataNavigator.Visible = False
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(650, 32)
        Me.TituloModulo.Text = "Utilidad por Proveedor"
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Visible = False
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 232)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(650, 52)
        '
        'nuUtilidad
        '
        Me.nuUtilidad.DecimalPlaces = 2
        Me.nuUtilidad.Location = New System.Drawing.Point(536, 32)
        Me.nuUtilidad.Name = "nuUtilidad"
        Me.nuUtilidad.Size = New System.Drawing.Size(96, 20)
        Me.nuUtilidad.TabIndex = 71
        Me.nuUtilidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nuUtilidad.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtObservacion)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.nuUtilidad)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txUsuarioCreador)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(8, 40)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(648, 192)
        Me.Panel1.TabIndex = 211
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(8, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 16)
        Me.Label4.TabIndex = 212
        Me.Label4.Text = "Usuario Creador :"
        '
        'txtObservacion
        '
        Me.txtObservacion.Location = New System.Drawing.Point(112, 88)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(520, 48)
        Me.txtObservacion.TabIndex = 94
        Me.txtObservacion.Text = ""
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(8, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 16)
        Me.Label5.TabIndex = 93
        Me.Label5.Text = "Observaci�n :"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TxtNombreProv)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txtCodigoProv)
        Me.Panel2.Location = New System.Drawing.Point(0, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(520, 48)
        Me.Panel2.TabIndex = 211
        '
        'TxtNombreProv
        '
        Me.TxtNombreProv.EditValue = ""
        Me.TxtNombreProv.Location = New System.Drawing.Point(112, 24)
        Me.TxtNombreProv.Name = "TxtNombreProv"
        '
        'TxtNombreProv.Properties
        '
        Me.TxtNombreProv.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtNombreProv.Properties.ReadOnly = True
        Me.TxtNombreProv.Size = New System.Drawing.Size(400, 19)
        Me.TxtNombreProv.TabIndex = 92
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label3.Location = New System.Drawing.Point(112, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(400, 16)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "Nombre del Proveedor"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 16)
        Me.Label2.TabIndex = 80
        Me.Label2.Text = "C�digo Proveedor"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCodigoProv
        '
        Me.txtCodigoProv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoProv.ForeColor = System.Drawing.Color.Blue
        Me.txtCodigoProv.Location = New System.Drawing.Point(8, 24)
        Me.txtCodigoProv.Name = "txtCodigoProv"
        Me.txtCodigoProv.Size = New System.Drawing.Size(96, 20)
        Me.txtCodigoProv.TabIndex = 82
        Me.txtCodigoProv.Text = ""
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label1.Location = New System.Drawing.Point(536, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 16)
        Me.Label1.TabIndex = 93
        Me.Label1.Text = "Utilidad"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txUsuarioCreador
        '
        Me.txUsuarioCreador.EditValue = ""
        Me.txUsuarioCreador.Location = New System.Drawing.Point(112, 160)
        Me.txUsuarioCreador.Name = "txUsuarioCreador"
        '
        'txUsuarioCreador.Properties
        '
        Me.txUsuarioCreador.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txUsuarioCreador.Properties.ReadOnly = True
        Me.txUsuarioCreador.Size = New System.Drawing.Size(520, 19)
        Me.txUsuarioCreador.TabIndex = 93
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=DIEGO;packet size=4096;integrated security=SSPI;data source=(local" & _
        ");persist security info=False;initial catalog=Planilla"
        '
        'txClave
        '
        Me.txClave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txClave.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txClave.ForeColor = System.Drawing.Color.Blue
        Me.txClave.Location = New System.Drawing.Point(400, 256)
        Me.txClave.Name = "txClave"
        Me.txClave.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txClave.Size = New System.Drawing.Size(56, 13)
        Me.txClave.TabIndex = 212
        Me.txClave.Text = ""
        Me.txClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label36
        '
        Me.Label36.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(320, 256)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 213
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmUtilidadXProveedor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(650, 284)
        Me.Controls.Add(Me.txClave)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmUtilidadXProveedor"
        Me.Text = "Utilidad por Proveedor"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.Label36, 0)
        Me.Controls.SetChildIndex(Me.txClave, 0)
        CType(Me.nuUtilidad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.TxtNombreProv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txUsuarioCreador.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim CodigoProv As String
    Dim Cedula_usuario As String
    Dim NombreUsuario As String

    Private Sub txtCodigoProv_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoProv.KeyDown
        If e.KeyCode = Windows.Forms.Keys.F1 Then
            Dim cFunciones As New cFunciones
            Me.txtCodigoProv.Text = cFunciones.BuscarDatos("select [CodigoProv] as [CodigoProv],nombre as Nombre from Proveedores ", "Nombre", "Buscar Proveedor ....", SqlConnection1.ConnectionString)
            FnCargarInformacionEmpleado(txtCodigoProv.Text)
        End If
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            FnCargarInformacionEmpleado(txtCodigoProv.Text)
        End If
    End Sub

#Region "Loggin usuario"
    Function Loggin_Usuario() As Boolean
        Dim cConexion As New Conexion
        Dim rs As SqlDataReader

        Try
            If txClave.Text <> "" Then
                rs = cConexion.GetRecorset(cConexion.Conectar, "SELECT Id_Usuario, Nombre from Usuarios where Clave_Interna ='" & txClave.Text & "'")
                If rs.HasRows = False Then
                    MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
                    txClave.Focus()
                    txClave.Text = ""
                    Return False
                End If
                While rs.Read
                    Try
                        NombreUsuario = rs("Nombre")
                        Cedula_usuario = rs("Id_Usuario")
                        Me.txUsuarioCreador.Text = rs("Nombre")
                        txClave.Enabled = False
                        ToolBar1.Buttons(0).Enabled = True
                        ToolBar1.Buttons(1).Enabled = True
                        Me.Panel1.Enabled = True
                        Nuevo()
                        Return True

                    Catch ex As SystemException
                        MsgBox(ex.Message)
                    End Try
                End While
                rs.Close()
                cConexion.DesConectar(cConexion.Conectar)
            Else
                MsgBox("Debe de digitar la clave de usuario", MsgBoxStyle.Exclamation)
                txClave.Focus()
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function
#End Region

    Private Sub Nuevo()

        If ToolBar1.Buttons(0).Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
            'cambia la imagen de nuevo y desabilita los botones
            ToolBar1.Buttons(0).Text = "Cancelar"
            ToolBar1.Buttons(0).ImageIndex = 8
            Try 'inicia la edicion
                Me.Panel1.Enabled = True
                ToolBar1.Buttons(2).Enabled = True
                ToolBar1.Buttons(1).Enabled = False
                ToolBar1.Buttons(3).Enabled = False
                Me.txtCodigoProv.Text = ""
                Me.TxtNombreProv.Text = ""
                Me.nuUtilidad.Value = 0
                Me.txtObservacion.Text = ""
                Me.txUsuarioCreador.Text = NombreUsuario
                Me.txtCodigoProv.Focus()
            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        Else
            Try
                'cambia la imagen a nuevo y habilita los botones del toolbar1
                Me.Panel1.Enabled = False
                ToolBar1.Buttons(4).Enabled = False
                ToolBar1.Buttons(0).Text = "Nuevo"
                ToolBar1.Buttons(0).ImageIndex = 0
                ToolBar1.Buttons(1).Enabled = True
                ToolBar1.Buttons(2).Enabled = False
                ToolBar1.Buttons(3).Enabled = False
                Me.ToolBarRegistrar.Text = "Registrar"
                Me.txtCodigoProv.Text = ""
                Me.TxtNombreProv.Text = ""
                Me.nuUtilidad.Value = 0
                Me.txtObservacion.Text = ""
                Me.txClave.Focus()

            Catch eEndEdit As System.Data.NoNullAllowedException
                System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            End Try
        End If

    End Sub

    Private Function FnValidarCampos() As Boolean
        If Not FnCargarInformacionEmpleado(txtCodigoProv.Text) Then
            MsgBox("El C�digo del Proveedor no se encuentra", MsgBoxStyle.Information, "Atenci�n...")
            Return False
        End If

        If Me.nuUtilidad.Value = 0 Then
            MsgBox("El porcentaje de utilidad debe ser mayor a 0.", MsgBoxStyle.Information, "Atenci�n...")
            Return False
        End If
        Return True
    End Function

    Private Sub spGuardar()

        If FnValidarCampos() Then
            If MsgBox("�Desea guardar los cambios de utilidad?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                Me.SqlConnection1.Close()
                If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
                Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
                Dim sqlInsert1 As SqlCommand = Me.SqlConnection1.CreateCommand
                Dim sqlInsert2 As SqlCommand = Me.SqlConnection1.CreateCommand


                Try
                    sqlInsert1.CommandText = " INSERT INTO [Seepos].[dbo].[Tb_S_CambioUtilidad] " & _
                                    " ([Fecha],[CodigoProv],[Utilidad],[Observacion],[Usuario]) VALUES " & _
                                    " ( '" & Now & "', '" & Me.txtCodigoProv.Text & "', '" & Me.nuUtilidad.Value & "', '" & Me.txtObservacion.Text & "', '" & Cedula_usuario & "')"

                    sqlInsert2.CommandText = " Update Inventario " & _
                                    " set Precio_A = (PrecioBase * " & Me.nuUtilidad.Value & "/100) + Fletes + OtrosCargos + PrecioBase, " & _
                                    " Precio_B = (PrecioBase * " & Me.nuUtilidad.Value & "/100) + Fletes + OtrosCargos + PrecioBase, " & _
                                    " Precio_C = (PrecioBase * " & Me.nuUtilidad.Value & "/100) + Fletes + OtrosCargos + PrecioBase, " & _
                                    " Precio_D = (PrecioBase * " & Me.nuUtilidad.Value & "/100) + Fletes + OtrosCargos + PrecioBase where Proveedor = '" & Me.txtCodigoProv.Text & "'"

                    sqlInsert1.Transaction = Trans
                    sqlInsert2.Transaction = Trans

                    sqlInsert1.ExecuteNonQuery()
                    sqlInsert2.ExecuteNonQuery()

                    Trans.Commit()
                    MsgBox("La informaci�n se guard� exitosamente.", MsgBoxStyle.Information)
                    Nuevo()
                Catch ex As Exception
                    Trans.Rollback()
                    MsgBox(ex.Message, MsgBoxStyle.Information)
                    Nuevo()
                End Try

            End If
        End If

    End Sub

    Private Sub spBuscar()
        Dim cFunciones As New cFunciones
        Dim IdCambio As String
        IdCambio = cFunciones.Buscar_X_Descripcion_Fecha5C("select Tb_S_CambioUtilidad.Id as Codigo ,Proveedores.Nombre,Tb_S_CambioUtilidad.Observacion ,Tb_S_CambioUtilidad.Fecha,Usuarios.Nombre as Usuario   from Tb_S_CambioUtilidad inner join Proveedores on Tb_S_CambioUtilidad.CodigoProv = Proveedores.CodigoProv inner join Usuarios on Tb_S_CambioUtilidad.Usuario = Usuarios.Cedula ", "Nombre", "Fecha")
        spCargarDatos(IdCambio)
    End Sub

    Private Sub spCargarDatos(byval _Id as String)
        Dim sql As String
        Dim dt As New DataTable
        Dim Fx As New cFunciones

        sql = "select Proveedores.CodigoProv,Proveedores.Nombre,Tb_S_CambioUtilidad.Observacion ,Tb_S_CambioUtilidad.Utilidad,Usuarios.Nombre as Usuario   from Tb_S_CambioUtilidad inner join Proveedores on Tb_S_CambioUtilidad.CodigoProv = Proveedores.CodigoProv inner join Usuarios on Tb_S_CambioUtilidad.Usuario = Usuarios.Cedula  where Tb_S_CambioUtilidad.Id = '" & _Id & "'"

        Fx.Llenar_Tabla_Generico(sql, dt)

        If dt.Rows.Count > 0 Then
            Me.txtCodigoProv.Text = dt.Rows(0).Item("CodigoProv")
            Me.TxtNombreProv.Text = dt.Rows(0).Item("Nombre")
            Me.nuUtilidad.Value = dt.Rows(0).Item("Utilidad")
            Me.txtObservacion.Text = dt.Rows(0).Item("Observacion")
            Me.txUsuarioCreador.Text = dt.Rows(0).Item("Usuario")
        End If


    End Sub

#Region "ToolBar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
        PMU = VSM(Cedula_usuario, Name) 'Carga los privilegios del usuario con el modulo
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Nuevo()
            Case 2 : If PMU.Find Then Me.spBuscar() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Update Then Me.spGuardar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : If MessageBox.Show("�Desea Cerrar el Formulario?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then Close()
        End Select
    End Sub
#End Region

    Private Function FnCargarInformacionEmpleado(ByVal codigo As String) As Boolean
        Dim cConexion As New Conexion
        Dim funciones As New cFunciones
        Dim rs As SqlDataReader
        Dim i As Integer
        Dim fila As DataRow
        Dim factura As Long
        Dim Resultado As Boolean
        If codigo <> Nothing Then
            rs = cConexion.GetRecorset(cConexion.Conectar("SeePos"), "SELECT * from [Proveedores] where [CodigoProv] ='" & codigo & "'")
            Try
                If rs.Read Then
                    TxtNombreProv.Text = rs("Nombre")
                    CodigoProv = rs("CodigoProv")
                    Me.nuUtilidad.Focus()
                    Resultado = True
                Else
                    MsgBox("El C�digo del Proveedor no se encuentra", MsgBoxStyle.Information, "Atenci�n...")
                    TxtNombreProv.Text = ""
                    txtCodigoProv.Focus()
                    rs.Close()
                    Exit Function
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information)
            End Try
            rs.Close()
            cConexion.DesConectar(cConexion.Conectar)
        End If
        Return Resultado
    End Function

    Private Sub frmUtilidadXProveedor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SqlConnection1.ConnectionString = GetSetting("SeeSoft", "SeePos", "Conexion")
        Panel1.Enabled = False
        ToolBar1.Buttons(0).Enabled = False
        ToolBar1.Buttons(1).Enabled = False
        ToolBar1.Buttons(2).Enabled = False
        ToolBar1.Buttons(4).Enabled = False
        Me.txClave.Focus()
    End Sub

    Private Sub txClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txClave.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Loggin_Usuario() Then
            End If
        End If
    End Sub

    Private Sub nuUtilidad_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles nuUtilidad.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.txtObservacion.Focus()
        End If
    End Sub
End Class
