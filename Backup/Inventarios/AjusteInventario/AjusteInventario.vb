Imports System.data.SqlClient
Imports System.Windows.Forms
Imports System.Data
Public Class AjusteInventario
    Inherits System.Windows.Forms.Form

#Region "Variables"
    Dim usua
    Dim NuevaConexion As String
    Dim strModulo As String
    Private cConexion As New Conexion
    Private sqlConexion As SqlConnection
#End Region

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Usuario_Parametro As Object, Optional ByVal Conexion As String = "")
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        NuevaConexion = Conexion
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub



    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtNumero As System.Windows.Forms.TextBox
    Friend WithEvents grpBox_Inventario As System.Windows.Forms.GroupBox
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtExistencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents op_Salida As System.Windows.Forms.RadioButton
    Friend WithEvents op_Entrada As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCostoUnit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarExcel As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtTotalEntrada As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTotalSalida As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSaldoAjuste As DevExpress.XtraEditors.TextEdit

    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtTotalEntradaD As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalSalidaD As System.Windows.Forms.TextBox
    Friend WithEvents colCod_Articulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEntrada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSalida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colobservacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotalEntrada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotalSalida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDesc_Articulo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents adAjusteInv As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsAjusteInv2 As DsAjusteInv
    Friend WithEvents adAjusteInvDetalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Check_Anulado As System.Windows.Forms.CheckBox
    Friend WithEvents txtCedulaUsuario As System.Windows.Forms.TextBox
    Friend WithEvents adBodegas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colCuentaContable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombreCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colBarras As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtBarras As System.Windows.Forms.TextBox
    Friend WithEvents btnTerminar As System.Windows.Forms.Button
    Friend WithEvents chbTerminar As System.Windows.Forms.CheckBox
    Friend WithEvents chbSaldoCXP As System.Windows.Forms.CheckBox
    Friend WithEvents txtImp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtTotalCXP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cbRazonAjuste As System.Windows.Forms.ComboBox
    Friend WithEvents adRazonAjuste As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection2 As System.Data.SqlClient.SqlConnection
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(AjusteInventario))
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtNumero = New System.Windows.Forms.TextBox
        Me.DsAjusteInv2 = New LcPymes_5._2.DsAjusteInv
        Me.grpBox_Inventario = New System.Windows.Forms.GroupBox
        Me.cbRazonAjuste = New System.Windows.Forms.ComboBox
        Me.txtBarras = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Check_Anulado = New System.Windows.Forms.CheckBox
        Me.txtTotalSalidaD = New System.Windows.Forms.TextBox
        Me.txtTotalEntradaD = New System.Windows.Forms.TextBox
        Me.txtCostoUnit = New DevExpress.XtraEditors.TextEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtObservacion = New System.Windows.Forms.TextBox
        Me.txtCantidad = New System.Windows.Forms.TextBox
        Me.txtDescripcion = New System.Windows.Forms.TextBox
        Me.txtExistencia = New DevExpress.XtraEditors.TextEdit
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.op_Salida = New System.Windows.Forms.RadioButton
        Me.op_Entrada = New System.Windows.Forms.RadioButton
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarExcel = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCod_Articulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDesc_Articulo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEntrada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSalida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colobservacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTotalEntrada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTotalSalida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCuentaContable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNombreCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colBarras = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.Label36 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtTotalEntrada = New DevExpress.XtraEditors.TextEdit
        Me.txtTotalSalida = New DevExpress.XtraEditors.TextEdit
        Me.txtSaldoAjuste = New DevExpress.XtraEditors.TextEdit
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adAjusteInv = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.adAjusteInvDetalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.txtCedulaUsuario = New System.Windows.Forms.TextBox
        Me.adBodegas = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.btnTerminar = New System.Windows.Forms.Button
        Me.chbTerminar = New System.Windows.Forms.CheckBox
        Me.chbSaldoCXP = New System.Windows.Forms.CheckBox
        Me.txtImp = New DevExpress.XtraEditors.TextEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtTotalCXP = New DevExpress.XtraEditors.TextEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.adRazonAjuste = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection2 = New System.Data.SqlClient.SqlConnection
        CType(Me.DsAjusteInv2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBox_Inventario.SuspendLayout()
        CType(Me.txtCostoUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExistencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtTotalEntrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalSalida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldoAjuste.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtImp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalCXP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label15.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Image = CType(resources.GetObject("Label15.Image"), System.Drawing.Image)
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(0, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(704, 32)
        Me.Label15.TabIndex = 104
        Me.Label15.Text = "Ajuste de Inventario"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtNumero
        '
        Me.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNumero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.Consecutivo"))
        Me.txtNumero.Enabled = False
        Me.txtNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumero.Location = New System.Drawing.Point(40, 16)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(80, 13)
        Me.txtNumero.TabIndex = 105
        Me.txtNumero.Text = ""
        Me.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DsAjusteInv2
        '
        Me.DsAjusteInv2.DataSetName = "DsAjusteInv"
        Me.DsAjusteInv2.Locale = New System.Globalization.CultureInfo("es-MX")
        '
        'grpBox_Inventario
        '
        Me.grpBox_Inventario.Controls.Add(Me.cbRazonAjuste)
        Me.grpBox_Inventario.Controls.Add(Me.txtBarras)
        Me.grpBox_Inventario.Controls.Add(Me.Label11)
        Me.grpBox_Inventario.Controls.Add(Me.TextBox1)
        Me.grpBox_Inventario.Controls.Add(Me.Label12)
        Me.grpBox_Inventario.Controls.Add(Me.Label10)
        Me.grpBox_Inventario.Controls.Add(Me.ComboBox1)
        Me.grpBox_Inventario.Controls.Add(Me.Check_Anulado)
        Me.grpBox_Inventario.Controls.Add(Me.txtTotalSalidaD)
        Me.grpBox_Inventario.Controls.Add(Me.txtTotalEntradaD)
        Me.grpBox_Inventario.Controls.Add(Me.txtCostoUnit)
        Me.grpBox_Inventario.Controls.Add(Me.Label7)
        Me.grpBox_Inventario.Controls.Add(Me.txtObservacion)
        Me.grpBox_Inventario.Controls.Add(Me.txtCantidad)
        Me.grpBox_Inventario.Controls.Add(Me.txtDescripcion)
        Me.grpBox_Inventario.Controls.Add(Me.txtExistencia)
        Me.grpBox_Inventario.Controls.Add(Me.txtCodigo)
        Me.grpBox_Inventario.Controls.Add(Me.Label6)
        Me.grpBox_Inventario.Controls.Add(Me.Label5)
        Me.grpBox_Inventario.Controls.Add(Me.GroupBox3)
        Me.grpBox_Inventario.Controls.Add(Me.Label4)
        Me.grpBox_Inventario.Controls.Add(Me.Label3)
        Me.grpBox_Inventario.Controls.Add(Me.Label1)
        Me.grpBox_Inventario.Enabled = False
        Me.grpBox_Inventario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpBox_Inventario.ForeColor = System.Drawing.SystemColors.Highlight
        Me.grpBox_Inventario.Location = New System.Drawing.Point(8, 32)
        Me.grpBox_Inventario.Name = "grpBox_Inventario"
        Me.grpBox_Inventario.Size = New System.Drawing.Size(672, 208)
        Me.grpBox_Inventario.TabIndex = 1
        Me.grpBox_Inventario.TabStop = False
        Me.grpBox_Inventario.Text = "Datos de Inventario"
        '
        'cbRazonAjuste
        '
        Me.cbRazonAjuste.DataSource = Me.DsAjusteInv2.tb_S_RazonAjuste
        Me.cbRazonAjuste.DisplayMember = "Nombre"
        Me.cbRazonAjuste.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbRazonAjuste.Location = New System.Drawing.Point(304, 112)
        Me.cbRazonAjuste.Name = "cbRazonAjuste"
        Me.cbRazonAjuste.Size = New System.Drawing.Size(264, 21)
        Me.cbRazonAjuste.TabIndex = 160
        Me.cbRazonAjuste.ValueMember = "Id"
        Me.cbRazonAjuste.Visible = False
        '
        'txtBarras
        '
        Me.txtBarras.BackColor = System.Drawing.SystemColors.Control
        Me.txtBarras.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBarras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Barras"))
        Me.txtBarras.ForeColor = System.Drawing.SystemColors.Highlight
        Me.txtBarras.Location = New System.Drawing.Point(128, 56)
        Me.txtBarras.Name = "txtBarras"
        Me.txtBarras.Size = New System.Drawing.Size(104, 13)
        Me.txtBarras.TabIndex = 159
        Me.txtBarras.Text = ""
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Nombre_Cuenta"))
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Blue
        Me.Label11.Location = New System.Drawing.Point(216, 184)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(440, 16)
        Me.Label11.TabIndex = 158
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Cuenta_Contable"))
        Me.TextBox1.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(8, 184)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(200, 13)
        Me.TextBox1.TabIndex = 156
        Me.TextBox1.Text = ""
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Blue
        Me.Label12.Location = New System.Drawing.Point(8, 168)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(648, 16)
        Me.Label12.TabIndex = 157
        Me.Label12.Text = "Cuenta Contable"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label10.Location = New System.Drawing.Point(8, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(648, 16)
        Me.Label10.TabIndex = 138
        Me.Label10.Text = "Bodega"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.DsAjusteInv2
        Me.ComboBox1.DisplayMember = "Bodega.Nombre"
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(8, 32)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(648, 21)
        Me.ComboBox1.TabIndex = 137
        Me.ComboBox1.ValueMember = "Bodega.IdBodega"
        '
        'Check_Anulado
        '
        Me.Check_Anulado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DsAjusteInv2, "AjusteInventario.Anula"))
        Me.Check_Anulado.Enabled = False
        Me.Check_Anulado.ForeColor = System.Drawing.Color.Red
        Me.Check_Anulado.Location = New System.Drawing.Point(576, 136)
        Me.Check_Anulado.Name = "Check_Anulado"
        Me.Check_Anulado.Size = New System.Drawing.Size(81, 16)
        Me.Check_Anulado.TabIndex = 15
        Me.Check_Anulado.Text = "Anulado"
        '
        'txtTotalSalidaD
        '
        Me.txtTotalSalidaD.AutoSize = False
        Me.txtTotalSalidaD.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.TotalSalida"))
        Me.txtTotalSalidaD.Enabled = False
        Me.txtTotalSalidaD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalSalidaD.Location = New System.Drawing.Point(152, 144)
        Me.txtTotalSalidaD.Name = "txtTotalSalidaD"
        Me.txtTotalSalidaD.Size = New System.Drawing.Size(104, 20)
        Me.txtTotalSalidaD.TabIndex = 14
        Me.txtTotalSalidaD.Text = ""
        Me.txtTotalSalidaD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalEntradaD
        '
        Me.txtTotalEntradaD.AutoSize = False
        Me.txtTotalEntradaD.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.TotalEntrada"))
        Me.txtTotalEntradaD.Enabled = False
        Me.txtTotalEntradaD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalEntradaD.Location = New System.Drawing.Point(16, 144)
        Me.txtTotalEntradaD.Name = "txtTotalEntradaD"
        Me.txtTotalEntradaD.Size = New System.Drawing.Size(104, 20)
        Me.txtTotalEntradaD.TabIndex = 13
        Me.txtTotalEntradaD.Text = ""
        Me.txtTotalEntradaD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCostoUnit
        '
        Me.txtCostoUnit.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.CostoUnit"))
        Me.txtCostoUnit.EditValue = ""
        Me.txtCostoUnit.Location = New System.Drawing.Point(600, 72)
        Me.txtCostoUnit.Name = "txtCostoUnit"
        '
        'txtCostoUnit.Properties
        '
        Me.txtCostoUnit.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtCostoUnit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCostoUnit.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtCostoUnit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCostoUnit.Properties.Enabled = False
        Me.txtCostoUnit.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtCostoUnit.Size = New System.Drawing.Size(60, 21)
        Me.txtCostoUnit.TabIndex = 7
        '
        'Label7
        '
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label7.Location = New System.Drawing.Point(600, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 16)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Costo Unit."
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtObservacion
        '
        Me.txtObservacion.AutoSize = False
        Me.txtObservacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.observacion"))
        Me.txtObservacion.Enabled = False
        Me.txtObservacion.Location = New System.Drawing.Point(304, 112)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(264, 40)
        Me.txtObservacion.TabIndex = 10
        Me.txtObservacion.Text = ""
        '
        'txtCantidad
        '
        Me.txtCantidad.AutoSize = False
        Me.txtCantidad.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Cantidad"))
        Me.txtCantidad.Enabled = False
        Me.txtCantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidad.Location = New System.Drawing.Point(576, 112)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(82, 20)
        Me.txtCantidad.TabIndex = 12
        Me.txtCantidad.Text = ""
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDescripcion
        '
        Me.txtDescripcion.AutoSize = False
        Me.txtDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Desc_Articulo"))
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(120, 72)
        Me.txtDescripcion.MaxLength = 255
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.ReadOnly = True
        Me.txtDescripcion.Size = New System.Drawing.Size(400, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Text = ""
        '
        'txtExistencia
        '
        Me.txtExistencia.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Existencia"))
        Me.txtExistencia.EditValue = ""
        Me.txtExistencia.Location = New System.Drawing.Point(528, 72)
        Me.txtExistencia.Name = "txtExistencia"
        '
        'txtExistencia.Properties
        '
        Me.txtExistencia.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtExistencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtExistencia.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtExistencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtExistencia.Properties.Enabled = False
        Me.txtExistencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtExistencia.Size = New System.Drawing.Size(64, 21)
        Me.txtExistencia.TabIndex = 5
        '
        'txtCodigo
        '
        Me.txtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle.Cod_Articulo"))
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigo.Location = New System.Drawing.Point(8, 72)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.Text = ""
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Location = New System.Drawing.Point(304, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(264, 16)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Observaci�n"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label5.Location = New System.Drawing.Point(576, 96)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Cantidad"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.op_Salida)
        Me.GroupBox3.Controls.Add(Me.op_Entrada)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 96)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(296, 40)
        Me.GroupBox3.TabIndex = 8
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Movimientos"
        '
        'op_Salida
        '
        Me.op_Salida.Enabled = False
        Me.op_Salida.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.op_Salida.ForeColor = System.Drawing.SystemColors.Highlight
        Me.op_Salida.Location = New System.Drawing.Point(160, 16)
        Me.op_Salida.Name = "op_Salida"
        Me.op_Salida.Size = New System.Drawing.Size(128, 16)
        Me.op_Salida.TabIndex = 1
        Me.op_Salida.Text = "Salida de Inventario"
        '
        'op_Entrada
        '
        Me.op_Entrada.Enabled = False
        Me.op_Entrada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.op_Entrada.ForeColor = System.Drawing.SystemColors.Highlight
        Me.op_Entrada.Location = New System.Drawing.Point(8, 16)
        Me.op_Entrada.Name = "op_Entrada"
        Me.op_Entrada.Size = New System.Drawing.Size(136, 15)
        Me.op_Entrada.TabIndex = 0
        Me.op_Entrada.Text = "&Entrada al Inventario"
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label4.Location = New System.Drawing.Point(120, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(400, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Descripci�n"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label3.Location = New System.Drawing.Point(528, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Existencia"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label1.Location = New System.Drawing.Point(8, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.AutoSize = False
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarExcel, Me.ToolBarCerrar})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 473)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(704, 52)
        Me.ToolBar1.TabIndex = 2
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Text = "Nuevo"
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        Me.ToolBarBuscar.ImageIndex = 1
        Me.ToolBarBuscar.Text = "Buscar"
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Text = "Registrar"
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.ImageIndex = 3
        Me.ToolBarEliminar.Text = "Anular"
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        Me.ToolBarImprimir.ImageIndex = 7
        Me.ToolBarImprimir.Text = "Imprimir"
        '
        'ToolBarExcel
        '
        Me.ToolBarExcel.Enabled = False
        Me.ToolBarExcel.ImageIndex = 5
        Me.ToolBarExcel.Text = "Exportar"
        Me.ToolBarExcel.Visible = False
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.DataMember = "AjusteInventario.AjusteInventarioAjusteInventario_Detalle"
        Me.GridControl1.DataSource = Me.DsAjusteInv2
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(8, 240)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(672, 144)
        Me.GridControl1.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 109
        Me.GridControl1.Text = "GridControl1"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCod_Articulo, Me.colCantidad, Me.colDesc_Articulo, Me.colEntrada, Me.colSalida, Me.colobservacion, Me.colTotalEntrada, Me.colTotalSalida, Me.colCuentaContable, Me.colNombreCuenta, Me.colBarras})
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsPrint.AutoWidth = False
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colCod_Articulo
        '
        Me.colCod_Articulo.Caption = "C�digo"
        Me.colCod_Articulo.FieldName = "Cod_Articulo"
        Me.colCod_Articulo.Name = "colCod_Articulo"
        Me.colCod_Articulo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCod_Articulo.VisibleIndex = 1
        Me.colCod_Articulo.Width = 58
        '
        'colCantidad
        '
        Me.colCantidad.Caption = "Cantidad"
        Me.colCantidad.FieldName = "Cantidad"
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCantidad.VisibleIndex = 0
        Me.colCantidad.Width = 58
        '
        'colDesc_Articulo
        '
        Me.colDesc_Articulo.Caption = "Descripci�n"
        Me.colDesc_Articulo.FieldName = "Desc_Articulo"
        Me.colDesc_Articulo.Name = "colDesc_Articulo"
        Me.colDesc_Articulo.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDesc_Articulo.VisibleIndex = 3
        Me.colDesc_Articulo.Width = 200
        '
        'colEntrada
        '
        Me.colEntrada.Caption = "Entrada"
        Me.colEntrada.FieldName = "Entrada"
        Me.colEntrada.Name = "colEntrada"
        Me.colEntrada.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colEntrada.VisibleIndex = 4
        '
        'colSalida
        '
        Me.colSalida.Caption = "Salida"
        Me.colSalida.FieldName = "Salida"
        Me.colSalida.Name = "colSalida"
        Me.colSalida.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colSalida.VisibleIndex = 5
        '
        'colobservacion
        '
        Me.colobservacion.Caption = "Observaciones"
        Me.colobservacion.FieldName = "observacion"
        Me.colobservacion.Name = "colobservacion"
        Me.colobservacion.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colobservacion.VisibleIndex = 6
        Me.colobservacion.Width = 150
        '
        'colTotalEntrada
        '
        Me.colTotalEntrada.Caption = "T. Entrada"
        Me.colTotalEntrada.DisplayFormat.FormatString = "#,#0.00"
        Me.colTotalEntrada.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colTotalEntrada.FieldName = "TotalEntrada"
        Me.colTotalEntrada.Name = "colTotalEntrada"
        Me.colTotalEntrada.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTotalEntrada.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colTotalEntrada.VisibleIndex = 7
        '
        'colTotalSalida
        '
        Me.colTotalSalida.Caption = "T. Salida"
        Me.colTotalSalida.DisplayFormat.FormatString = "#,#0.00"
        Me.colTotalSalida.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colTotalSalida.FieldName = "TotalSalida"
        Me.colTotalSalida.Name = "colTotalSalida"
        Me.colTotalSalida.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTotalSalida.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colTotalSalida.VisibleIndex = 8
        '
        'colCuentaContable
        '
        Me.colCuentaContable.Caption = "Cuenta Contable"
        Me.colCuentaContable.FieldName = "Cuenta_Contable"
        Me.colCuentaContable.Name = "colCuentaContable"
        Me.colCuentaContable.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCuentaContable.VisibleIndex = 9
        Me.colCuentaContable.Width = 125
        '
        'colNombreCuenta
        '
        Me.colNombreCuenta.Caption = "Nombre Cuenta"
        Me.colNombreCuenta.FieldName = "Nombre_Cuenta"
        Me.colNombreCuenta.Name = "colNombreCuenta"
        Me.colNombreCuenta.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNombreCuenta.VisibleIndex = 10
        Me.colNombreCuenta.Width = 200
        '
        'colBarras
        '
        Me.colBarras.Caption = "Barras"
        Me.colBarras.FieldName = "Barras"
        Me.colBarras.Name = "colBarras"
        Me.colBarras.VisibleIndex = 2
        Me.colBarras.Width = 88
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.txtNombreUsuario)
        Me.Panel1.Controls.Add(Me.txtUsuario)
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Location = New System.Drawing.Point(352, 480)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(288, 16)
        Me.Panel1.TabIndex = 0
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(128, 0)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(163, 13)
        Me.txtNombreUsuario.TabIndex = 2
        Me.txtNombreUsuario.Text = ""
        '
        'txtUsuario
        '
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(72, 0)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(56, 13)
        Me.txtUsuario.TabIndex = 1
        Me.txtUsuario.Text = ""
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(0, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label2.Location = New System.Drawing.Point(8, 416)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 16)
        Me.Label2.TabIndex = 112
        Me.Label2.Text = "Total Entrada"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label8.Location = New System.Drawing.Point(136, 416)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 16)
        Me.Label8.TabIndex = 114
        Me.Label8.Text = "Total Salida"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label9.Location = New System.Drawing.Point(264, 416)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 16)
        Me.Label9.TabIndex = 116
        Me.Label9.Text = "Saldo Ajuste"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtTotalEntrada
        '
        Me.txtTotalEntrada.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalEntrada.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsAjusteInv2, "AjusteInventario.TotalEntrada"))
        Me.txtTotalEntrada.EditValue = ""
        Me.txtTotalEntrada.Location = New System.Drawing.Point(8, 432)
        Me.txtTotalEntrada.Name = "txtTotalEntrada"
        '
        'txtTotalEntrada.Properties
        '
        Me.txtTotalEntrada.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtTotalEntrada.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalEntrada.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtTotalEntrada.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalEntrada.Properties.ReadOnly = True
        Me.txtTotalEntrada.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtTotalEntrada.Size = New System.Drawing.Size(120, 28)
        Me.txtTotalEntrada.TabIndex = 117
        '
        'txtTotalSalida
        '
        Me.txtTotalSalida.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalSalida.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsAjusteInv2, "AjusteInventario.TotalSalida"))
        Me.txtTotalSalida.EditValue = ""
        Me.txtTotalSalida.Location = New System.Drawing.Point(136, 432)
        Me.txtTotalSalida.Name = "txtTotalSalida"
        '
        'txtTotalSalida.Properties
        '
        Me.txtTotalSalida.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtTotalSalida.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalSalida.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtTotalSalida.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalSalida.Properties.ReadOnly = True
        Me.txtTotalSalida.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtTotalSalida.Size = New System.Drawing.Size(120, 28)
        Me.txtTotalSalida.TabIndex = 118
        '
        'txtSaldoAjuste
        '
        Me.txtSaldoAjuste.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSaldoAjuste.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsAjusteInv2, "AjusteInventario.SaldoAjuste"))
        Me.txtSaldoAjuste.EditValue = ""
        Me.txtSaldoAjuste.Location = New System.Drawing.Point(264, 432)
        Me.txtSaldoAjuste.Name = "txtSaldoAjuste"
        '
        'txtSaldoAjuste.Properties
        '
        Me.txtSaldoAjuste.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtSaldoAjuste.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldoAjuste.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtSaldoAjuste.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldoAjuste.Properties.ReadOnly = True
        Me.txtSaldoAjuste.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtSaldoAjuste.Size = New System.Drawing.Size(120, 28)
        Me.txtSaldoAjuste.TabIndex = 119
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'adAjusteInv
        '
        Me.adAjusteInv.DeleteCommand = Me.SqlDeleteCommand1
        Me.adAjusteInv.InsertCommand = Me.SqlInsertCommand1
        Me.adAjusteInv.SelectCommand = Me.SqlSelectCommand1
        Me.adAjusteInv.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AjusteInventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Consecutivo", "Consecutivo"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Anula", "Anula"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("TotalEntrada", "TotalEntrada"), New System.Data.Common.DataColumnMapping("TotalSalida", "TotalSalida"), New System.Data.Common.DataColumnMapping("SaldoAjuste", "SaldoAjuste")})})
        Me.adAjusteInv.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM AjusteInventario WHERE (Consecutivo = @Original_Consecutivo) AND (Anu" & _
        "la = @Original_Anula) AND (Cedula = @Original_Cedula) AND (Fecha = @Original_Fec" & _
        "ha) AND (SaldoAjuste = @Original_SaldoAjuste) AND (TotalEntrada = @Original_Tota" & _
        "lEntrada) AND (TotalSalida = @Original_TotalSalida)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SaldoAjuste", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SaldoAjuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO AjusteInventario(Fecha, Anula, Cedula, TotalEntrada, TotalSalida, Sal" & _
        "doAjuste) VALUES (@Fecha, @Anula, @Cedula, @TotalEntrada, @TotalSalida, @SaldoAj" & _
        "uste); SELECT Consecutivo, Fecha, Anula, Cedula, TotalEntrada, TotalSalida, Sald" & _
        "oAjuste FROM AjusteInventario WHERE (Consecutivo = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 1, "Anula"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 150, "Cedula"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SaldoAjuste", System.Data.SqlDbType.Float, 8, "SaldoAjuste"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Consecutivo, Fecha, Anula, Cedula, TotalEntrada, TotalSalida, SaldoAjuste " & _
        "FROM AjusteInventario"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE AjusteInventario SET Fecha = @Fecha, Anula = @Anula, Cedula = @Cedula, Tot" & _
        "alEntrada = @TotalEntrada, TotalSalida = @TotalSalida, SaldoAjuste = @SaldoAjust" & _
        "e WHERE (Consecutivo = @Original_Consecutivo) AND (Anula = @Original_Anula) AND " & _
        "(Cedula = @Original_Cedula) AND (Fecha = @Original_Fecha) AND (SaldoAjuste = @Or" & _
        "iginal_SaldoAjuste) AND (TotalEntrada = @Original_TotalEntrada) AND (TotalSalida" & _
        " = @Original_TotalSalida); SELECT Consecutivo, Fecha, Anula, Cedula, TotalEntrad" & _
        "a, TotalSalida, SaldoAjuste FROM AjusteInventario WHERE (Consecutivo = @Consecut" & _
        "ivo)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 1, "Anula"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 150, "Cedula"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SaldoAjuste", System.Data.SqlDbType.Float, 8, "SaldoAjuste"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SaldoAjuste", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SaldoAjuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consecutivo", System.Data.SqlDbType.BigInt, 8, "Consecutivo"))
        '
        'adAjusteInvDetalle
        '
        Me.adAjusteInvDetalle.DeleteCommand = Me.SqlDeleteCommand2
        Me.adAjusteInvDetalle.InsertCommand = Me.SqlInsertCommand2
        Me.adAjusteInvDetalle.SelectCommand = Me.SqlSelectCommand2
        Me.adAjusteInvDetalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AjusteInventario_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Consecutivo", "Consecutivo"), New System.Data.Common.DataColumnMapping("Cons_Ajuste", "Cons_Ajuste"), New System.Data.Common.DataColumnMapping("Cod_Articulo", "Cod_Articulo"), New System.Data.Common.DataColumnMapping("IdBodega", "IdBodega"), New System.Data.Common.DataColumnMapping("Desc_Articulo", "Desc_Articulo"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Entrada", "Entrada"), New System.Data.Common.DataColumnMapping("Salida", "Salida"), New System.Data.Common.DataColumnMapping("observacion", "observacion"), New System.Data.Common.DataColumnMapping("Cuenta_Contable", "Cuenta_Contable"), New System.Data.Common.DataColumnMapping("Nombre_Cuenta", "Nombre_Cuenta"), New System.Data.Common.DataColumnMapping("TotalEntrada", "TotalEntrada"), New System.Data.Common.DataColumnMapping("TotalSalida", "TotalSalida"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("CostoUnit", "CostoUnit"), New System.Data.Common.DataColumnMapping("Barras", "Barras")})})
        Me.adAjusteInvDetalle.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM AjusteInventario_Detalle WHERE (Consecutivo = @Original_Consecutivo) " & _
        "AND (Barras = @Original_Barras) AND (Cantidad = @Original_Cantidad) AND (Cod_Art" & _
        "iculo = @Original_Cod_Articulo) AND (Cons_Ajuste = @Original_Cons_Ajuste) AND (C" & _
        "ostoUnit = @Original_CostoUnit) AND (Cuenta_Contable = @Original_Cuenta_Contable" & _
        ") AND (Desc_Articulo = @Original_Desc_Articulo) AND (Entrada = @Original_Entrada" & _
        ") AND (Existencia = @Original_Existencia) AND (IdBodega = @Original_IdBodega) AN" & _
        "D (Nombre_Cuenta = @Original_Nombre_Cuenta) AND (Salida = @Original_Salida) AND " & _
        "(TotalEntrada = @Original_TotalEntrada) AND (TotalSalida = @Original_TotalSalida" & _
        ") AND (observacion = @Original_observacion) AND (Id_RazonAjuste = @Original_Id_R" & _
        "azonAjuste) OR (Consecutivo = @Original_Consecutivo) AND (Barras IS NULL) AND (C" & _
        "antidad = @Original_Cantidad) AND (Cod_Articulo = @Original_Cod_Articulo) AND (C" & _
        "ons_Ajuste = @Original_Cons_Ajuste) AND (CostoUnit = @Original_CostoUnit) AND (C" & _
        "uenta_Contable = @Original_Cuenta_Contable) AND (Desc_Articulo = @Original_Desc_" & _
        "Articulo) AND (Entrada = @Original_Entrada) AND (Existencia = @Original_Existenc" & _
        "ia) AND (IdBodega = @Original_IdBodega) AND (Nombre_Cuenta = @Original_Nombre_Cu" & _
        "enta) AND (Salida = @Original_Salida) AND (TotalEntrada = @Original_TotalEntrada" & _
        ") AND (TotalSalida = @Original_TotalSalida) AND (observacion = @Original_observa" & _
        "cion) AND (@Original_Barras IS NULL) AND (Id_RazonAjuste = @Original_Id_RazonAju" & _
        "ste)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Articulo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cons_Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoUnit", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoUnit", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Contable", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Contable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Desc_Articulo", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Desc_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Entrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdBodega", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Salida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Salida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_observacion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_RazonAjuste", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_RazonAjuste", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO AjusteInventario_Detalle (Cons_Ajuste, Cod_Articulo, IdBodega, Desc_A" & _
        "rticulo, Cantidad, Entrada, Salida, observacion, Cuenta_Contable, Nombre_Cuenta," & _
        " TotalEntrada, TotalSalida, Existencia, CostoUnit, Barras, Id_RazonAjuste) VALUE" & _
        "S (@Cons_Ajuste, @Cod_Articulo, @IdBodega, @Desc_Articulo, @Cantidad, @Entrada, " & _
        "@Salida, @observacion, @Cuenta_Contable, @Nombre_Cuenta, @TotalEntrada, @TotalSa" & _
        "lida, @Existencia, @CostoUnit, @Barras, @Id_RazonAjuste); SELECT Consecutivo, Co" & _
        "ns_Ajuste, Cod_Articulo, IdBodega, Desc_Articulo, Cantidad, Entrada, Salida, obs" & _
        "ervacion, Cuenta_Contable, Nombre_Cuenta, TotalEntrada, TotalSalida, Existencia," & _
        " CostoUnit, Barras, Id_RazonAjuste FROM AjusteInventario_Detalle WHERE (Consecut" & _
        "ivo = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, "Cons_Ajuste"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdBodega", System.Data.SqlDbType.BigInt, 8, "IdBodega"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Desc_Articulo", System.Data.SqlDbType.VarChar, 250, "Desc_Articulo"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Entrada", System.Data.SqlDbType.Bit, 1, "Entrada"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Salida", System.Data.SqlDbType.Bit, 1, "Salida"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@observacion", System.Data.SqlDbType.VarChar, 250, "observacion"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta_Contable", System.Data.SqlDbType.VarChar, 100, "Cuenta_Contable"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Cuenta", System.Data.SqlDbType.VarChar, 250, "Nombre_Cuenta"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoUnit", System.Data.SqlDbType.Float, 8, "CostoUnit"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 50, "Barras"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_RazonAjuste", System.Data.SqlDbType.Int, 4, "Id_RazonAjuste"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Consecutivo, Cons_Ajuste, Cod_Articulo, IdBodega, Desc_Articulo, Cantidad," & _
        " Entrada, Salida, observacion, Cuenta_Contable, Nombre_Cuenta, TotalEntrada, Tot" & _
        "alSalida, Existencia, CostoUnit, Barras, Id_RazonAjuste FROM AjusteInventario_De" & _
        "talle"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE AjusteInventario_Detalle SET Cons_Ajuste = @Cons_Ajuste, Cod_Articulo = @C" & _
        "od_Articulo, IdBodega = @IdBodega, Desc_Articulo = @Desc_Articulo, Cantidad = @C" & _
        "antidad, Entrada = @Entrada, Salida = @Salida, observacion = @observacion, Cuent" & _
        "a_Contable = @Cuenta_Contable, Nombre_Cuenta = @Nombre_Cuenta, TotalEntrada = @T" & _
        "otalEntrada, TotalSalida = @TotalSalida, Existencia = @Existencia, CostoUnit = @" & _
        "CostoUnit, Barras = @Barras, Id_RazonAjuste = @Id_RazonAjuste WHERE (Consecutivo" & _
        " = @Original_Consecutivo) AND (Barras = @Original_Barras OR @Original_Barras IS " & _
        "NULL AND Barras IS NULL) AND (Cantidad = @Original_Cantidad) AND (Cod_Articulo =" & _
        " @Original_Cod_Articulo) AND (Cons_Ajuste = @Original_Cons_Ajuste) AND (CostoUni" & _
        "t = @Original_CostoUnit) AND (Cuenta_Contable = @Original_Cuenta_Contable) AND (" & _
        "Desc_Articulo = @Original_Desc_Articulo) AND (Entrada = @Original_Entrada) AND (" & _
        "Existencia = @Original_Existencia) AND (IdBodega = @Original_IdBodega) AND (Nomb" & _
        "re_Cuenta = @Original_Nombre_Cuenta) AND (Salida = @Original_Salida) AND (TotalE" & _
        "ntrada = @Original_TotalEntrada) AND (TotalSalida = @Original_TotalSalida) AND (" & _
        "observacion = @Original_observacion) AND (Id_RazonAjuste = @Original_Id_RazonAju" & _
        "ste); SELECT Consecutivo, Cons_Ajuste, Cod_Articulo, IdBodega, Desc_Articulo, Ca" & _
        "ntidad, Entrada, Salida, observacion, Cuenta_Contable, Nombre_Cuenta, TotalEntra" & _
        "da, TotalSalida, Existencia, CostoUnit, Barras, Id_RazonAjuste FROM AjusteInvent" & _
        "ario_Detalle WHERE (Consecutivo = @Consecutivo)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, "Cons_Ajuste"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdBodega", System.Data.SqlDbType.BigInt, 8, "IdBodega"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Desc_Articulo", System.Data.SqlDbType.VarChar, 250, "Desc_Articulo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Entrada", System.Data.SqlDbType.Bit, 1, "Entrada"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Salida", System.Data.SqlDbType.Bit, 1, "Salida"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@observacion", System.Data.SqlDbType.VarChar, 250, "observacion"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta_Contable", System.Data.SqlDbType.VarChar, 100, "Cuenta_Contable"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre_Cuenta", System.Data.SqlDbType.VarChar, 250, "Nombre_Cuenta"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalEntrada", System.Data.SqlDbType.Float, 8, "TotalEntrada"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSalida", System.Data.SqlDbType.Float, 8, "TotalSalida"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoUnit", System.Data.SqlDbType.Float, 8, "CostoUnit"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 50, "Barras"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_RazonAjuste", System.Data.SqlDbType.Int, 4, "Id_RazonAjuste"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Articulo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cons_Ajuste", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cons_Ajuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoUnit", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoUnit", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta_Contable", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta_Contable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Desc_Articulo", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Desc_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Entrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdBodega", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Salida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Salida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalEntrada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSalida", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSalida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_observacion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_RazonAjuste", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_RazonAjuste", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consecutivo", System.Data.SqlDbType.BigInt, 8, "Consecutivo"))
        '
        'txtCedulaUsuario
        '
        Me.txtCedulaUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsAjusteInv2, "AjusteInventario.Cedula"))
        Me.txtCedulaUsuario.Location = New System.Drawing.Point(568, 2)
        Me.txtCedulaUsuario.Name = "txtCedulaUsuario"
        Me.txtCedulaUsuario.Size = New System.Drawing.Size(104, 20)
        Me.txtCedulaUsuario.TabIndex = 120
        Me.txtCedulaUsuario.Text = ""
        Me.txtCedulaUsuario.Visible = False
        '
        'adBodegas
        '
        Me.adBodegas.DeleteCommand = Me.SqlDeleteCommand3
        Me.adBodegas.InsertCommand = Me.SqlInsertCommand3
        Me.adBodegas.SelectCommand = Me.SqlSelectCommand3
        Me.adBodegas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Bodega", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("IdBodega", "IdBodega"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("CuentaContable", "CuentaContable"), New System.Data.Common.DataColumnMapping("DescripcionCuentaContable", "DescripcionCuentaContable")})})
        Me.adBodegas.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Bodega WHERE (IdBodega = @Original_IdBodega) AND (CuentaContable = @O" & _
        "riginal_CuentaContable OR @Original_CuentaContable IS NULL AND CuentaContable IS" & _
        " NULL) AND (Descripcion = @Original_Descripcion OR @Original_Descripcion IS NULL" & _
        " AND Descripcion IS NULL) AND (DescripcionCuentaContable = @Original_Descripcion" & _
        "CuentaContable OR @Original_DescripcionCuentaContable IS NULL AND DescripcionCue" & _
        "ntaContable IS NULL) AND (Nombre = @Original_Nombre)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdBodega", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CuentaContable", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionCuentaContable", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCuentaContable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Bodega(Nombre, Descripcion, CuentaContable, DescripcionCuentaContable" & _
        ") VALUES (@Nombre, @Descripcion, @CuentaContable, @DescripcionCuentaContable); S" & _
        "ELECT IdBodega, Nombre, Descripcion, CuentaContable, DescripcionCuentaContable F" & _
        "ROM Bodega WHERE (IdBodega = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 100, "Descripcion"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CuentaContable", System.Data.SqlDbType.VarChar, 255, "CuentaContable"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionCuentaContable", System.Data.SqlDbType.VarChar, 250, "DescripcionCuentaContable"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT IdBodega, Nombre, Descripcion, CuentaContable, DescripcionCuentaContable F" & _
        "ROM Bodega"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Bodega SET Nombre = @Nombre, Descripcion = @Descripcion, CuentaContable = " & _
        "@CuentaContable, DescripcionCuentaContable = @DescripcionCuentaContable WHERE (I" & _
        "dBodega = @Original_IdBodega) AND (CuentaContable = @Original_CuentaContable OR " & _
        "@Original_CuentaContable IS NULL AND CuentaContable IS NULL) AND (Descripcion = " & _
        "@Original_Descripcion OR @Original_Descripcion IS NULL AND Descripcion IS NULL) " & _
        "AND (DescripcionCuentaContable = @Original_DescripcionCuentaContable OR @Origina" & _
        "l_DescripcionCuentaContable IS NULL AND DescripcionCuentaContable IS NULL) AND (" & _
        "Nombre = @Original_Nombre); SELECT IdBodega, Nombre, Descripcion, CuentaContable" & _
        ", DescripcionCuentaContable FROM Bodega WHERE (IdBodega = @IdBodega)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 100, "Descripcion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CuentaContable", System.Data.SqlDbType.VarChar, 255, "CuentaContable"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionCuentaContable", System.Data.SqlDbType.VarChar, 250, "DescripcionCuentaContable"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdBodega", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CuentaContable", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaContable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionCuentaContable", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCuentaContable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdBodega", System.Data.SqlDbType.BigInt, 8, "IdBodega"))
        '
        'btnTerminar
        '
        Me.btnTerminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnTerminar.Location = New System.Drawing.Point(8, 384)
        Me.btnTerminar.Name = "btnTerminar"
        Me.btnTerminar.TabIndex = 121
        Me.btnTerminar.Text = "Terminar "
        '
        'chbTerminar
        '
        Me.chbTerminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chbTerminar.Enabled = False
        Me.chbTerminar.Location = New System.Drawing.Point(88, 384)
        Me.chbTerminar.Name = "chbTerminar"
        Me.chbTerminar.Size = New System.Drawing.Size(80, 24)
        Me.chbTerminar.TabIndex = 122
        Me.chbTerminar.Text = "Terminado"
        '
        'chbSaldoCXP
        '
        Me.chbSaldoCXP.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chbSaldoCXP.Location = New System.Drawing.Point(176, 384)
        Me.chbSaldoCXP.Name = "chbSaldoCXP"
        Me.chbSaldoCXP.TabIndex = 123
        Me.chbSaldoCXP.Text = "Saldo para CXP"
        '
        'txtImp
        '
        Me.txtImp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtImp.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsAjusteInv2, "AjusteInventario.Imp"))
        Me.txtImp.EditValue = ""
        Me.txtImp.Location = New System.Drawing.Point(392, 432)
        Me.txtImp.Name = "txtImp"
        '
        'txtImp.Properties
        '
        Me.txtImp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtImp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtImp.Properties.ReadOnly = True
        Me.txtImp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtImp.Size = New System.Drawing.Size(120, 28)
        Me.txtImp.TabIndex = 125
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label13.Location = New System.Drawing.Point(392, 416)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(120, 16)
        Me.Label13.TabIndex = 124
        Me.Label13.Text = "Imp"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtTotalCXP
        '
        Me.txtTotalCXP.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalCXP.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsAjusteInv2, "AjusteInventario.TotalCXP"))
        Me.txtTotalCXP.EditValue = ""
        Me.txtTotalCXP.Location = New System.Drawing.Point(520, 432)
        Me.txtTotalCXP.Name = "txtTotalCXP"
        '
        'txtTotalCXP.Properties
        '
        Me.txtTotalCXP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalCXP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalCXP.Properties.ReadOnly = True
        Me.txtTotalCXP.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText)
        Me.txtTotalCXP.Size = New System.Drawing.Size(120, 28)
        Me.txtTotalCXP.TabIndex = 127
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label14.Location = New System.Drawing.Point(520, 416)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(120, 16)
        Me.Label14.TabIndex = 126
        Me.Label14.Text = "Total para CXP"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(456, 384)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(224, 24)
        Me.Button1.TabIndex = 128
        Me.Button1.Text = "Cambiar Cuentas Contables"
        '
        'adRazonAjuste
        '
        Me.adRazonAjuste.SelectCommand = Me.SqlSelectCommand4
        Me.adRazonAjuste.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "tb_S_RazonAjuste", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Id, Nombre FROM tb_S_RazonAjuste"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection2
        '
        'SqlConnection2
        '
        Me.SqlConnection2.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'AjusteInventario
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(704, 525)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtTotalCXP)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtImp)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.chbTerminar)
        Me.Controls.Add(Me.btnTerminar)
        Me.Controls.Add(Me.txtNumero)
        Me.Controls.Add(Me.txtCedulaUsuario)
        Me.Controls.Add(Me.txtSaldoAjuste)
        Me.Controls.Add(Me.txtTotalSalida)
        Me.Controls.Add(Me.txtTotalEntrada)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.grpBox_Inventario)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.chbSaldoCXP)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(712, 552)
        Me.Name = "AjusteInventario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ajuste de Inventario"
        CType(Me.DsAjusteInv2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBox_Inventario.ResumeLayout(False)
        CType(Me.txtCostoUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExistencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtTotalEntrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalSalida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldoAjuste.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtImp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalCXP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Load"
    Private Sub AjusteInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        strModulo = IIf(NuevaConexion = "", "SeePos", "SeePos")
        Dim cx As New cFunciones
        cx.Llenar_Tabla_Generico("SELECT ID_Bodega as IdBodega, Nombre_Bodega as Nombre FROM Bodegas", Me.DsAjusteInv2.Bodega, Me.SqlConnection1.ConnectionString)

        'Establecer Valores por Defecto
        Me.DsAjusteInv2.AjusteInventario.FechaColumn.DefaultValue = Now
        Me.DsAjusteInv2.AjusteInventario.AnulaColumn.DefaultValue = False
        Me.DsAjusteInv2.AjusteInventario.CedulaColumn.DefaultValue = ""
        Me.DsAjusteInv2.AjusteInventario.TotalEntradaColumn.DefaultValue = 0
        Me.DsAjusteInv2.AjusteInventario.TotalSalidaColumn.DefaultValue = 0
        Me.DsAjusteInv2.AjusteInventario.SaldoAjusteColumn.DefaultValue = 0

        Me.DsAjusteInv2.AjusteInventario_Detalle.Cons_AjusteColumn.DefaultValue = 0
        Me.DsAjusteInv2.AjusteInventario_Detalle.Cod_ArticuloColumn.DefaultValue = 0
        Me.DsAjusteInv2.AjusteInventario_Detalle.CantidadColumn.DefaultValue = 0
        Me.DsAjusteInv2.AjusteInventario_Detalle.EntradaColumn.DefaultValue = 1
        Me.DsAjusteInv2.AjusteInventario_Detalle.SalidaColumn.DefaultValue = 0
        Me.DsAjusteInv2.AjusteInventario_Detalle.observacionColumn.DefaultValue = ""
        Me.DsAjusteInv2.AjusteInventario_Detalle.Cuenta_ContableColumn.DefaultValue = ""
        Me.DsAjusteInv2.AjusteInventario_Detalle.TotalEntradaColumn.DefaultValue = 0
        Me.DsAjusteInv2.AjusteInventario_Detalle.TotalSalidaColumn.DefaultValue = 0
        Me.DsAjusteInv2.AjusteInventario_Detalle.Desc_ArticuloColumn.DefaultValue = ""

        txtUsuario.Focus()
    End Sub
#End Region

#Region "Validacion Usuario"
    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim cConexion As New Conexion
            Dim Conx As New SqlConnection
            Conx.ConnectionString = Me.SqlConnection1.ConnectionString
            Conx.Open()
            Dim rs As SqlDataReader
            If e.KeyCode = Keys.Enter Then
                If txtUsuario.Text <> "" Then
                    rs = cConexion.GetRecorset(Conx, "SELECT Cedula, Nombre from Usuarios where Clave_Interna ='" & txtUsuario.Text & "'")

                    If rs.HasRows = False Then
                        MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
                        txtUsuario.Focus()
                    End If
                    While rs.Read
                        Try
                            Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").EndCurrentEdit()
                            Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").AddNew()
                            txtNombreUsuario.Text = rs("Nombre")
                            Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").Current("Cedula") = rs("Cedula")
                            Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").EndCurrentEdit()
                            txtUsuario.Enabled = False ' se inabilita el campo de la contrase�a
                            Me.ToolBar1.Buttons(0).Enabled = True
                            Me.ToolBar1.Buttons(1).Enabled = True
                            Me.ToolBar1.Buttons(2).Enabled = True
                            Me.grpBox_Inventario.Enabled = True
                            Me.txtCodigo.Enabled = True
                            Me.op_Entrada.Enabled = True
                            Me.op_Salida.Enabled = True
                            Me.txtObservacion.Enabled = True
                            Me.txtCantidad.Enabled = True
                            Me.NuevoAjuste()
                            ComboBox1.Focus()

                        Catch ex As SystemException
                            MsgBox(ex.Message)
                        End Try
                    End While
                    rs.Close()
                    cConexion.DesConectar(cConexion.Conectar(strModulo))
                Else
                    MsgBox("Debe de digitar la clave de usuario", MsgBoxStyle.Exclamation)
                    txtUsuario.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub txtUsuario_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUsuario.GotFocus
        txtUsuario.SelectAll()
    End Sub
#End Region

    Private Sub spCargarComboAjuste()
        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable
        Dim Fx As New cFunciones

        Dim strConexion As String = SqlConnection1.ConnectionString 'JCGA 19032007

        Fx.Cargar_Tabla_Generico(Me.adRazonAjuste, "SELECT [Id],[Nombre] FROM [Seepos].[dbo].[tb_S_RazonAjuste] ", strConexion)  'JCGA 19032007
        Me.adRazonAjuste.Fill(Me.DsAjusteInv2, "tb_S_RazonAjuste")
    End Sub

#Region "Nuevo"
    Private Sub NuevoAjuste()
        Me.btnTerminar.Visible = False
        Me.chbTerminar.Visible = False
        chbTerminar.Checked = False
        b = False
        Me.chbSaldoCXP.Checked = False
        If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then 'no hay un registro pendiente
            Me.ToolBar1.Buttons(0).Text = "Cancelar"
            Me.ToolBar1.Buttons(0).ImageIndex = 8
            Me.ToolBar1.Buttons(3).Enabled = False
            If Me.txtUsuario.Text = "" Then
                txtUsuario.Enabled = True
                txtUsuario.Focus()
            End If
            txtCodigo.Text = "" : txtDescripcion.Text = ""
            Me.spCargarComboAjuste()
        Else
            Try
                If MessageBox.Show("Desea Guardar los Cambios del Ajuste de Inventario", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                    'Me.Registrar()
                Else
                    Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").CancelCurrentEdit()
                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    Me.ToolBar1.Buttons(3).Enabled = True
                    Me.txtCodigo.Enabled = False
                    Me.txtDescripcion.Enabled = False
                    Me.txtExistencia.Enabled = False
                    Me.txtCostoUnit.Enabled = False
                    Me.op_Entrada.Enabled = False
                    Me.op_Salida.Enabled = False
                    Me.txtObservacion.Enabled = False
                    Me.txtCantidad.Enabled = False
                    Me.txtUsuario.Text = ""
                    Me.txtNombreUsuario.Text = ""
                    Me.txtCedulaUsuario.Text = ""
                End If
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try

        End If

    End Sub
#End Region

#Region "ToolBar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Try
            Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
            PMU = VSM(usua.Cedula, Me.Name) 'Carga los privilegios del usuario con el modu

            Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
                Case 1
                    If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then
                        NuevoAjuste()
                    Else
                        If MessageBox.Show("Desea Guardar este Ajuste de Inventario", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                            If PMU.Update Then
                                Me.Registrar() ' se guarda en la base de datos
                            Else
                                MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                            End If
                        Else

                            Me.DsAjusteInv2.AjusteInventario_Detalle.Clear()
                            Me.DsAjusteInv2.AjusteInventario.Clear()

                            Me.ToolBar1.Buttons(0).Text = "Nuevo"
                            Me.ToolBar1.Buttons(0).ImageIndex = 0

                            Me.ToolBar1.Buttons(0).Enabled = True
                            Me.ToolBar1.Buttons(1).Enabled = False
                            Me.ToolBar1.Buttons(2).Enabled = False
                            Me.ToolBar1.Buttons(3).Enabled = False
                            Me.ToolBar1.Buttons(4).Enabled = False
                            grpBox_Inventario.Enabled = False
                            txtUsuario.Enabled = True
                            txtUsuario.Text = ""
                            txtNombreUsuario.Text = ""
                            txtCedulaUsuario.Text = ""
                            txtUsuario.Focus()
                        End If
                    End If

                Case 2 : If PMU.Find Then Me.BuscarAjuste() Else MsgBox("No tiene permiso para Buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 3 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 4 : If PMU.Delete Then Anular() Else MsgBox("No tiene permiso para eliminar o anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 5 : If PMU.Print Then Me.imprimir() Else MsgBox("No tiene permiso para imprimir los datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

                Case 6

                Case 7 : Me.Close()
            End Select

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Anular"
    Sub Anular()
        Dim cls As New Valida_Asientos.Puede
        If cls.Anular(Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").Current("Consecutivo"), Valida_Asientos.Modulos.AjustesInventario) = False Then
            Exit Sub
        End If
        Try
            If Me.chbTerminar.Checked Then
                MsgBox("No se puede anular el ajuste porque ya fue terminado", MsgBoxStyle.Information)
                Exit Sub
            End If
            Dim resp As Integer
            If Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").Count > 0 Then

                'Dim Cx As New Conexion
                'Dim valida As String
                'Dim num_ajuste As String = Me.txtNumero.Text
                'valida = Cx.SlqExecuteScalar(Cx.Conectar("Proveeduria"), "SELECT Contabilizado FROM compras WHERE Factura= '" & num_ajuste & "'")
                'Cx.DesConectar(Cx.sQlconexion)
                'If valida = True Then ' se verifica si la factura seleccionada puede ser eliminada
                '    MessageBox.Show("La factura seleccionada no puede ser eliminada..", "Informaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                '    Exit Function
                'End If

                resp = MessageBox.Show("�Desea Anular este Ajuste?", strModulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    Check_Anulado.Checked = True
                    Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").EndCurrentEdit()

                    If Me.insertar_bitacora() And Registrar_Anulacion_Ajuste() Then

                        Me.DsAjusteInv2.AcceptChanges()
                        MsgBox("El ajuste ha sido anulado correctamente", MsgBoxStyle.Information)
                        Me.DsAjusteInv2.AjusteInventario_Detalle.Clear()
                        Me.DsAjusteInv2.AjusteInventario.Clear()
                        Me.ToolBar1.Buttons(4).Enabled = True
                        Me.ToolBar1.Buttons(1).Enabled = True
                        Me.ToolBar1.Buttons(0).Text = "Nuevo"
                        Me.ToolBar1.Buttons(0).ImageIndex = 0
                        Me.ToolBar1.Buttons(0).Enabled = True
                        Me.ToolBar1.Buttons(1).Enabled = False
                        Me.ToolBar1.Buttons(2).Enabled = False
                        grpBox_Inventario.Enabled = False
                        txtUsuario.Enabled = True
                        txtUsuario.Text = ""
                        txtNombreUsuario.Text = ""
                        txtCedulaUsuario.Text = ""
                        txtUsuario.Focus()
                        Me.ToolBar1.Buttons(3).Enabled = False
                        Me.ToolBar1.Buttons(4).Enabled = False
                    End If

                Else : Exit Sub
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function insertar_bitacora() As Boolean
        Dim funciones As New Conexion
        Dim datos As String
        datos = "'AJUSTE INVENTARIO','" & Me.txtNumero.Text & "','AJUSTE INVENTARIO','AJUSTE INVENATARIO ANULADO','" & usua.Nombre & "'," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0
        If funciones.AddNewRecord("Bitacora", "Tabla,Campo_Clave,DescripcionCampo,Accion,Usuario,Costo,VentaA,VentaB,VentaC,VentaD", datos) <> "" Then
            MsgBox("Problemas al Anular el ajuste", MsgBoxStyle.Critical)
            Return False
        Else
            Return True
        End If
    End Function

    Function Registrar_Anulacion_Ajuste() As Boolean
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction

        Try
            Me.adAjusteInv.UpdateCommand.Transaction = Trans
            Me.adAjusteInv.Update(Me.DsAjusteInv2, "AjusteInventario")
            Trans.Commit()
            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(3).Enabled = True
            Return False
        End Try
    End Function
#End Region

#Region "Imprimir"
    Function imprimir()
        Try
            Me.ToolBar1.Buttons(4).Enabled = False
            Dim Ajuste_Reporte As New Reporte_Ajuste_Inventario

            Ajuste_Reporte.SetParameterValue(0, CDbl(txtNumero.Text))
            CrystalReportsConexion.LoadShow(Ajuste_Reporte, MdiParent, SqlConnection1.ConnectionString)

            Me.ToolBar1.Buttons(4).Enabled = True
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function
#End Region

#Region "Funciones Controles"
    Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim BuscarArt As New FrmBuscarArticulo(Me.BindingContext(Me.DsAjusteInv2, "Bodega").Current("IdBodega"), Me.SqlConnection1.ConnectionString)
            Try
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").AddNew()

                BuscarArt.StartPosition = FormStartPosition.CenterScreen
                BuscarArt.ShowDialog()

                If BuscarArt.Cancelado Then
                    Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
                    Exit Sub
                End If

                txtCodigo.Text = BuscarArt.Codigo
                BuscarArt.Dispose()
                CargarInformacionArticulo(txtCodigo.Text)

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If

        If e.KeyCode = Keys.Enter Then
            Try
                Dim cod_art As String
                cod_art = Me.txtCodigo.Text
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").AddNew()
                CargarInformacionArticulo(cod_art)

            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        End If

        If e.KeyCode = Keys.F2 Then
            'Me.Registrar()
        End If
    End Sub

    Private Sub op_Entrada_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles op_Entrada.CheckedChanged
        txtObservacion.Visible = True
        Me.cbRazonAjuste.Visible = False
        Label6.Text = "Observaci�n"
        txtObservacion.Focus()
    End Sub

    Private Sub op_Salida_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles op_Salida.CheckedChanged
        Try
            If GetSetting("Seesoft", "SeePos", "MostrarRazonAjuste").Equals("1") Then
                txtObservacion.Visible = False
                Me.cbRazonAjuste.Visible = True
                Me.cbRazonAjuste.Focus()
                Label6.Text = "Raz�n de Ajuste Salida"
            Else
                txtObservacion.Visible = True
                Me.cbRazonAjuste.Visible = False
                Me.cbRazonAjuste.SelectedValue = 0
                txtObservacion.Focus()
                SaveSetting("Seesoft", "SeePos", "MostrarRazonAjuste", "0")
            End If
        Catch ex As Exception

        End Try
     

    End Sub

    Private Sub txtCodigo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub txtCantidad_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCantidad.Validating
        Try
            If Me.txtCantidad.Text = "" Then 'si el campo est� vac�o
                ErrorProvider1.SetError(sender, "Debes ajustar al menos un art�culo")
                'e.Cancel = True
            Else
                ErrorProvider1.SetError(sender, "")
            End If
            If CDbl(Me.txtCantidad.Text) = 0 Then 'si el campo est� vac�o
                ErrorProvider1.SetError(sender, "Debes ajustar al menos un art�culo")
                'e.Cancel = True
            Else
                ErrorProvider1.SetError(sender, "")
            End If
        Catch ex As SystemException
            txtCantidad.Text = 1
            ErrorProvider1.SetError(sender, "")
        End Try
    End Sub

    Private Sub txtCantidad_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCantidad.GotFocus
        txtCantidad.SelectAll()
    End Sub

    Private Sub txtCantidad_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCantidad.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub txtCantidad_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.KeyDown
        If e.KeyCode = Keys.Enter Then ' se guarda la cotizaci�n en el dataset
            Me.TextBox1.Focus()
            '    Try
            '        Dim resp As Integer
            '        If Me.txtCantidad.Text = "" Or Me.txtCantidad.Text = "0" Then
            '            MsgBox("La Cantidad de art�culos no es v�lida", MsgBoxStyle.Exclamation)
            '            Me.txtCantidad.Text = "1"
            '            Exit Sub
            '        End If
            '        resp = MessageBox.Show("�Desea agregar este art�culo al ajuste?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

            '        If resp <> 6 Then

            '            Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
            '            Me.txtCodigo.Focus()
            '            Exit Sub
            '        End If

            '        If op_Entrada.Checked = True Then
            '            txtTotalEntradaD.Text = Format(txtCostoUnit.Text * txtCantidad.Text, "#,#0.00")
            '            txtTotalSalidaD.Text = 0
            '        Else
            '            txtTotalSalidaD.Text = Format(txtCostoUnit.Text * txtCantidad.Text, "#,#0.00")
            '            txtTotalEntradaD.Text = 0
            '        End If

            '        Me.Calcular()

            '        Me.txtCodigo.Focus()

            '    Catch ex As System.Exception
            '        MsgBox(ex.Message)
            '    End Try

        End If

        If e.KeyCode = Keys.F2 Then
            'Me.Registrar()
        End If
    End Sub

    Private Sub txtObservacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservacion.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtCantidad.Focus()
        End If
    End Sub

    Private Sub GridControl1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl1.KeyDown
        If e.KeyCode = Keys.Delete Then
            Eliminar_Ariculo_Detalle()
        End If
    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtCodigo.Focus()
        End If
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then ' se guarda la cotizaci�n en el dataset
            If Me.chbTerminar.Checked Then
                MsgBox("No se puede anular el ajuste porque ya fue terminado", MsgBoxStyle.Information)
                Exit Sub
            End If

            Try

                If Me.TextBox1.Text = "" And Me.Label11.Text = "" Then
                    MsgBox("Debe seleccionar una cuenta contable....", MsgBoxStyle.Information)
                Else
                    '''''''
                    Dim Cx As New Conexion
                    Dim valida As String
                    Dim num_cuenta As String = Me.TextBox1.Text
                    valida = Cx.SlqExecuteScalar(Cx.Conectar("Contabilidad"), "SELECT CuentaContable FROM CuentaContable WHERE CuentaContable= '" & num_cuenta & "' AND Movimiento=1")
                    Cx.DesConectar(Cx.sQlconexion)
                    If valida = "" Then ' en caso de que la cuenta se haya digitado y no buscado, se valida su existencia
                        MessageBox.Show("La cuenta digitada no esta registrada o no permite movimiento..", "Informaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                        Me.TextBox1.Focus()
                    Else
                        '
                        Dim nombre As String
                        nombre = Cx.SlqExecuteScalar(Cx.Conectar("Contabilidad"), "SELECT Descripcion FROM CuentaContable WHERE CuentaContable= '" & num_cuenta & "' AND Movimiento=1")
                        Cx.DesConectar(Cx.sQlconexion)
                        Me.Label11.Text = nombre
                        '
                        Dim resp As Integer
                        If Me.txtCantidad.Text = "" Or Me.txtCantidad.Text = "0" Then
                            MsgBox("La Cantidad de art�culos no es v�lida", MsgBoxStyle.Exclamation)
                            Me.txtCantidad.Text = "1"
                            Exit Sub
                        End If
                        resp = MessageBox.Show("�Desea agregar este art�culo al ajuste?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

                        If resp <> 6 Then

                            Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
                            Me.txtCodigo.Focus()
                            Exit Sub
                        End If

                        If op_Entrada.Checked = True Then
                            txtTotalEntradaD.Text = Format(txtCostoUnit.Text * txtCantidad.Text, "#,#0.00")
                            txtTotalSalidaD.Text = 0
                         Else
                            txtTotalSalidaD.Text = Format(txtCostoUnit.Text * txtCantidad.Text, "#,#0.00")
                            txtTotalEntradaD.Text = 0
                        End If

                        Me.Calcular()

                        Me.txtCodigo.Focus()
                    End If 'termina la vaidacion
                End If

            Catch ex As System.Exception
                MsgBox(ex.Message)
            End Try

        End If

        If e.KeyCode = Keys.F1 Then
            Dim Fx As New cFunciones
            Dim Id As String
            Id = Fx.BuscarDatos("SELECT CuentaContable, Descripcion FROM cuentacontable where (Movimiento = 1) ", "Descripcion", "Buscar Cuenta Contable .....", GetSetting("Seesoft", "Contabilidad", "Conexion"))

            If Id = "" Then
                Exit Sub
            End If

            TextBox1.Text = Id
            If CargarInformacionCuentaContable(Me.TextBox1.Text) Then
            Else
                Me.TextBox1.Focus()
            End If
        End If
    End Sub
#End Region

#Region "Funciones"
    Private Sub CargarInformacionArticulo(ByVal codigo As String)
        Dim Conexion2 As New Conexion
        Dim Conexion3 As New Conexion
        Dim rs As SqlDataReader
        Dim tipocambio As Double
        Dim Encontrado As Boolean

        Dim conex As SqlConnection = Me.SqlConnection1

        If codigo <> Nothing Then
            If conex.State <> ConnectionState.Open Then conex.Open()
            'esta instruccion debera de cambiarse y hacer la seleccion del articulo x bodega   ----------------- and Inventario.Barras = '" & codigo & "'
            Dim solobarras As Integer = 0
            Try
                solobarras = GetSetting("SeeSoft", "SeePos", "SoloBarras")

            Catch ex As Exception
                SaveSetting("SeeSoft", "SeePos", "SoloBarras", "0")
                solobarras = 0
            End Try
            If solobarras = 1 Then
                rs = Conexion2.GetRecorset(conex, "SELECT Inventario.Codigo AS Codigo, Inventario.Descripcion as Descripcion, ArticulosXBodega.Existencia as Existencia , Inventario.Costo  as Costo , Inventario.MonedaCosto as MonedaCosto, Inventario.Barras from Inventario  INNER JOIN ArticulosXBodega ON ArticulosXBodega.Codigo = Inventario.Codigo where (Inhabilitado = 0) and Inventario.codigo ='" & codigo & "' and ArticulosXBodega.IdBodega = '" & Me.BindingContext(Me.DsAjusteInv2, "Bodega").Current("IdBodega") & "'")

            Else
                rs = Conexion2.GetRecorset(conex, "SELECT Inventario.Codigo AS Codigo, Inventario.Descripcion as Descripcion, ArticulosXBodega.Existencia as Existencia , Inventario.Costo  as Costo , Inventario.MonedaCosto as MonedaCosto, Inventario.Barras from Inventario  INNER JOIN ArticulosXBodega ON ArticulosXBodega.Codigo = Inventario.Codigo where (Inhabilitado = 0) and  Inventario.Codigo ='" & codigo & "' and ArticulosXBodega.IdBodega = '" & Me.BindingContext(Me.DsAjusteInv2, "Bodega").Current("IdBodega") & "' or (Inhabilitado = 0) and  Inventario.Barras ='" & codigo & "' and ArticulosXBodega.IdBodega = '" & ComboBox1.SelectedValue & "'")

            End If


            Encontrado = False
            While rs.Read
                Try
                    Encontrado = True
                    txtCodigo.Text = rs("Codigo")
                    txtDescripcion.Text = rs("Descripcion")
                    tipocambio = Conexion3.SlqExecuteScalar(Conexion3.Conectar(strModulo), "Select ValorCompra from Moneda where CodMoneda =" & rs("MonedaCosto"))
                    Me.txtCostoUnit.Text = Format(rs("Costo") * tipocambio, "#,#0.00")
                    Conexion3.DesConectar(Conexion3.Conectar(strModulo))
                    Me.txtExistencia.Text = rs("Existencia")
                    txtBarras.Text = rs("Barras")
                    Me.op_Entrada.Checked = True
                    txtObservacion.Focus()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Conexion2.DesConectar(Conexion2.Conectar(strModulo))
                End Try
            End While
            rs.Close()
            conex.Close()
            If Not Encontrado Then
                If Existe_en_inventario(codigo) Then
                    If MsgBox("No existe un art�culo con este c�digo en la bodega especificada." & vbCrLf & "DESEA AGREGAR EL ARTICULO A LA BODEGA SELECCIONADA...!!!", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        Agregar_a_Bodega(codigo, Me.BindingContext(Me.DsAjusteInv2, "Bodega").Current("IdBodega"))
                        CargarInformacionArticulo(codigo)
                        Exit Sub
                    End If
                Else
                    MsgBox("El art�culo especificado [" & codigo & "] no se encuentra registrado en el sistema..", MsgBoxStyle.Information, "Atenci�n..")
                End If

                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
                Me.txtCodigo.Focus()
                Conexion2.DesConectar(Conexion2.Conectar(strModulo))
                Exit Sub
            End If
        End If
    End Sub

    Private Function Existe_en_inventario(ByVal Codigo As String) As Boolean
        Dim cx As New Conexion
        Dim Resultado As String
        Resultado = cx.SlqExecuteScalar(cx.Conectar("SeePos"), "SELECT Codigo FROM Inventario WHERE (Codigo = " & Codigo & ")")
        cx.DesConectar(cx.sQlconexion)
        If Resultado = Codigo Then Return True Else Return False
    End Function

    Private Sub Agregar_a_Bodega(ByVal Codigo As String, ByVal Bodega As String)
        Dim Fx As New Conexion
        Dim Resultado As String
        Resultado = Fx.AddNewRecord("ArticuloSxBodega", "Codigo,idbodega", Codigo & "," & Bodega, "SeePos")
        If Resultado <> "" Then
            MsgBox(Resultado, MsgBoxStyle.Information)
        End If
    End Sub
    Sub sp_CacularTOTALCXP()
        Dim imp As Double = 0
        Dim dt As New DataTable
        For i As Integer = 0 To Me.DsAjusteInv2.AjusteInventario_Detalle.Count - 1
            If Not Me.DsAjusteInv2.AjusteInventario_Detalle(i).RowState = DataRowState.Deleted Then

                cFunciones.Llenar_Tabla_Generico("Select * From Inventario Where Codigo = " & Me.DsAjusteInv2.AjusteInventario_Detalle(i).Cod_Articulo, dt)
                If dt.Rows.Count > 0 Then

                    imp += (Me.DsAjusteInv2.AjusteInventario_Detalle(i).TotalSalida * (dt.Rows(0).Item("IVenta") / 100))

                End If

            End If
        Next
        Me.txtImp.EditValue = imp
        Me.txtTotalCXP.EditValue = Math.Abs(CDbl(Me.txtSaldoAjuste.EditValue)) + imp

    End Sub
    Sub Calcular()
        Try
            If Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Count > 0 Then
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("IdBodega") = Me.BindingContext(Me.DsAjusteInv2, "Bodega").Current("IdBodega")
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Id_RazonAjuste") = IIf(Me.cbRazonAjuste.SelectedValue = Nothing, "0", Me.cbRazonAjuste.SelectedValue)
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Entrada") = Me.op_Entrada.Checked
                Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Salida") = Me.op_Salida.Checked
                Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()
                txtTotalEntrada.Text = Format(Me.colTotalEntrada.SummaryItem.SummaryValue, "#,#0.00")
                txtTotalSalida.Text = Format(Me.colTotalSalida.SummaryItem.SummaryValue, "#,#0.00")
                txtSaldoAjuste.Text = Format(CDbl(txtTotalEntrada.Text) - CDbl(txtTotalSalida.Text), "#,#0.00")
            Else
                txtTotalEntrada.Text = "0.00"
                txtTotalSalida.Text = "0.00"
                txtSaldoAjuste.Text = "0.00"
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function CargarInformacionCuentaContable(ByVal codigo As String) As Boolean
        Dim rs As SqlDataReader
        Dim Encontrado As Boolean
        If codigo <> Nothing Then
            sqlConexion = cConexion.Conectar("Contabilidad")
            rs = cConexion.GetRecorset(sqlConexion, "SELECT CuentaContable, Descripcion FROM  CuentaContable where CuentaContable = '" & codigo & "'")
            Encontrado = False
            While rs.Read
                Try
                    Encontrado = True
                    TextBox1.Text = rs("CuentaContable")
                    Label11.Text = rs("Descripcion")
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    cConexion.DesConectar(cConexion.Conectar("Contabilidad"))
                End Try
            End While
            rs.Close()

            If Not Encontrado Then
                MsgBox("No existe Esta Cuenta Contable", MsgBoxStyle.Exclamation)
                cConexion.DesConectar(cConexion.Conectar("Contabilidad"))
                Return False
            End If
            cConexion.DesConectar(cConexion.Conectar("Contabilidad"))
            Return True
        End If
    End Function
#End Region

#Region "Buscar"
    Dim b As Boolean = False
    Private Sub BuscarAjuste()
        Dim Fx As New cFunciones
        Dim identificador As Double
        Dim buscando As Boolean

        Dim strConexion As String = SqlConnection1.ConnectionString 'JCGA 19032007

        Try
            If Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").Count > 0 Then
                If (MsgBox("Actualmente se est� realizando un Ajuste de Inventario, si contin�a se perderan los datos del mismo, �desea continuar?", MsgBoxStyle.YesNo)) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Me.DsAjusteInv2.AjusteInventario_Detalle.Clear()
            Me.DsAjusteInv2.AjusteInventario.Clear()

            identificador = CDbl(Fx.Buscar_X_Descripcion_Fecha("SELECT AjusteInventario.Consecutivo,CASE Terminar WHEN 1 THEN '(Terminado) ' ELSE '' END + '' + CASE CXP WHEN 1 THEN '(CXP) ' ELSE '' END + '' + Inventario.Descripcion AS Descripcion,AjusteInventario.Fecha FROM AjusteInventario INNER JOIN AjusteInventario_Detalle ON AjusteInventario.Consecutivo = AjusteInventario_Detalle.Cons_Ajuste INNER JOIN Inventario ON AjusteInventario_Detalle.Cod_Articulo = Inventario.Codigo Order by AjusteInventario.Fecha DESC", "Descripcion", "Fecha", "Buscar Ajuste de Inventario", strConexion))

            buscando = True
            If identificador = 0.0 Then ' si se dio en el boton de cancelar
                buscando = False
                Exit Sub
            End If
            Fx.Cargar_Tabla_Generico(Me.adAjusteInv, "SELECT * FROM AjusteInventario WHERE (Consecutivo =" & identificador & " )", strConexion) 'JCGA 19032007
            Me.adAjusteInv.Fill(Me.DsAjusteInv2, "AjusteInventario")

            Fx.Cargar_Tabla_Generico(Me.adAjusteInvDetalle, "SELECT * FROM AjusteInventario_Detalle WHERE (Cons_Ajuste =" & identificador & " )", strConexion) 'JCGA 19032007
            Me.adAjusteInvDetalle.Fill(Me.DsAjusteInv2, "AjusteInventario_Detalle")


            Me.GridControl1.Enabled = False
            grpBox_Inventario.Enabled = False
            Me.ToolBar1.Buttons(2).Enabled = False
            Me.ToolBar1.Buttons(3).Enabled = True
            Me.ToolBar1.Buttons(4).Enabled = True
            Me.ToolBar1.Buttons(0).Enabled = True
            Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
            PMU = VSM(usua.Cedula, Me.Name)
            Me.btnTerminar.Visible = True
            Me.chbTerminar.Visible = True
            If PMU.Others Then

                If Not Me.DsAjusteInv2.AjusteInventario(0).Terminar Then


                Else
                    Me.btnTerminar.Enabled = False
                End If
                Me.chbTerminar.Checked = Me.DsAjusteInv2.AjusteInventario(0).Terminar
                Me.btnTerminar.Enabled = True
            Else
                Me.btnTerminar.Enabled = False
            End If
            If Me.DsAjusteInv2.AjusteInventario(0).CXP Then

                Me.chbSaldoCXP.Checked = True
            Else
                Me.chbSaldoCXP.Checked = False
            End If
            Me.txtImp.EditValue = Me.DsAjusteInv2.AjusteInventario(0).Imp
            Me.txtTotalCXP.EditValue = Me.DsAjusteInv2.AjusteInventario(0).TotalCXP
            b = True
            'si esta venta aun no ha sido anulada
            If Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario").Current("Anula") = True Then Me.ToolBar1.Buttons(3).Enabled = False

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Registrar"
    Function Registrar()
        Dim Funciones As New Conexion
        Try
            If MessageBox.Show("�Desea guardar el ajuste de inventario?", strModulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                'Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario_Detalle").Current("IdBodega") = Me.BindingContext(Me.DsAjusteInv2, "Bodega").Current("IdBodega")
                Me.BindingContext(DsAjusteInv2, "AjusteInventario").EndCurrentEdit()
                Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()

                If Me.Registrar_Ajuste() Then
                    Me.ToolBar1.Buttons(4).Enabled = True
                    Me.ToolBar1.Buttons(1).Enabled = True

                    Me.ToolBar1.Buttons(0).Text = "Nuevo"
                    Me.ToolBar1.Buttons(0).ImageIndex = 0
                    Me.ToolBar1.Buttons(3).Enabled = True
                    guardarComoSaldoCXP()

                    MsgBox("El Ajuste de Inventario se guardo Satisfactoriamente", MsgBoxStyle.Information)
                    Me.imprimir()
                    Me.DsAjusteInv2.AjusteInventario_Detalle.Clear()
                    Me.DsAjusteInv2.AjusteInventario.Clear()
                    txtUsuario.Enabled = True
                    txtUsuario.Text = ""
                    txtNombreUsuario.Text = ""
                    txtCedulaUsuario.Text = ""
                    txtUsuario.Focus()
                Else
                    MsgBox("Error al Guardar el ajuste de inventario", MsgBoxStyle.Critical)
                End If
            Else
                Exit Function
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Sub guardarComoSaldoCXP()
        If Me.chbSaldoCXP.Checked Then
            If CDbl(Me.txtSaldoAjuste.Text) < 0 Then
                If MsgBox("Desea usar este ajuste como saldo a favor para aplicarlo a CXP", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    Dim valor As String = ""
                    Dim fx As New cFunciones
                    ' sp_CacularTOTALCXP()

                    valor = fx.BuscarDatos("Select CodigoProv,Nombre from Proveedores", "Nombre", "Buscar Proveedor...", Me.SqlConnection1.ConnectionString)
                    If valor = "" Then
                        Exit Sub
                    Else

                        Dim cnc As New Conexion
                        cnc.Conectar()
                        cnc.SlqExecute(cnc.sQlconexion, "UPDATE [Seepos].[dbo].[AjusteInventario]  SET [CXP] = 1 , CodigoProv = " & valor & ", Imp = " & Me.txtImp.EditValue & ", TotalCXP = " & Me.txtTotalCXP.EditValue & " WHERE Consecutivo = " & txtNumero.Text & " ")
                        cnc.DesConectar(cnc.sQlconexion)
                    End If
                End If
            Else
                MsgBox("No se puede usar ajustes de entrada para notas", MsgBoxStyle.OKOnly)

            End If
        Else
            Dim cnc As New Conexion
            cnc.Conectar()
            cnc.SlqExecute(cnc.sQlconexion, "UPDATE [Seepos].[dbo].[AjusteInventario]  SET [CXP] = 0, Imp = 0, TotalCXP = 0 WHERE Consecutivo = " & txtNumero.Text & " ")
            cnc.DesConectar(cnc.sQlconexion)
        End If
    End Sub
    Function Registrar_Ajuste() As Boolean
        If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Try
            Me.adAjusteInv.InsertCommand.Transaction = Trans
            Me.adAjusteInv.DeleteCommand.Transaction = Trans
            Me.adAjusteInv.UpdateCommand.Transaction = Trans
            Me.adAjusteInv.SelectCommand.Transaction = Trans

            Me.adAjusteInvDetalle.InsertCommand.Transaction = Trans
            Me.adAjusteInvDetalle.DeleteCommand.Transaction = Trans
            Me.adAjusteInvDetalle.UpdateCommand.Transaction = Trans
            Me.adAjusteInvDetalle.SelectCommand.Transaction = Trans

            Me.adAjusteInv.Update(Me.DsAjusteInv2, "AjusteInventario")
            Me.adAjusteInvDetalle.Update(Me.DsAjusteInv2, "AjusteInventario_Detalle")

            Trans.Commit()
            Return True


        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            Me.ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try
    End Function
#End Region

#Region "Eliminar Detalle"
    Private Sub Eliminar_Ariculo_Detalle()
        If Me.chbTerminar.Checked Then
            MsgBox("No se puede anular el ajuste porque ya fue terminado", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim resp As Integer
        Try 'se intenta hacer
            If Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Count > 0 Then   ' si hay ubicaciones

                resp = MessageBox.Show("�Desea eliminar este art�culo del Ajuste?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

                If resp = 6 Then
                    Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").RemoveAt(Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Position)
                    Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()

                    Me.Calcular()
                    Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").EndCurrentEdit()
                    Me.txtCodigo.Focus()
                Else
                    Me.BindingContext(DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").CancelCurrentEdit()
                End If
            Else
                MsgBox("No Existen Art�culos para eliminar de este ajuste", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region


    Private Sub btnTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTerminar.Click
        sp_Terminar()
    End Sub
    Sub sp_Terminar()
        If Not Me.chbTerminar.Checked Then
            If MsgBox("�Desea terminar el ajuste e impedir mas cambios?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
            Dim cnc As New Conexion
            cnc.Conectar()
            cnc.SlqExecute(cnc.sQlconexion, "UPDATE [Seepos].[dbo].[AjusteInventario]  SET [Terminar] = 1  WHERE Consecutivo = " & txtNumero.Text & " ")
            cnc.DesConectar(cnc.sQlconexion)
            Me.btnTerminar.Enabled = False
        Else

            Dim cnc As New Conexion
            cnc.Conectar()
            cnc.SlqExecute(cnc.sQlconexion, "UPDATE [Seepos].[dbo].[AjusteInventario]  SET [Terminar] = 0  WHERE Consecutivo = " & txtNumero.Text & " ")
            cnc.DesConectar(cnc.sQlconexion)

        End If
        chbTerminar.Checked = Not chbTerminar.Checked

    End Sub

    Private Sub GridControl1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridControl1.Click
        Me.op_Entrada.Checked = Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Entrada")
        Me.op_Salida.Checked = Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Salida")
        If Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Salida") Then
            Dim IdRazon = Me.BindingContext(Me.DsAjusteInv2, "AjusteInventario.AjusteInventarioAjusteInventario_Detalle").Current("Id_RazonAjuste")
            If GetSetting("Seesoft", "SeePos", "MostrarRazonAjuste").Equals("1") Then
                txtObservacion.Visible = False
                Me.cbRazonAjuste.Visible = True
                Me.cbRazonAjuste.Focus()
                Label6.Text = "Raz�n de Ajuste Salida"
                Me.cbRazonAjuste.SelectedValue = IdRazon
            Else
                txtObservacion.Visible = True
                Me.cbRazonAjuste.Visible = False
                txtObservacion.Focus()
                SaveSetting("Seesoft", "SeePos", "MostrarRazonAjuste", "0")
            End If


        End If
    End Sub

    Private Sub cbRazonAjuste1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbRazonAjuste.KeyPress
        If Keys.Enter = Asc(e.KeyChar) Then
            Me.txtCantidad.Focus()
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub chbSaldoCXP_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chbSaldoCXP.CheckedChanged
        If chbSaldoCXP.Checked Then
            sp_CacularTOTALCXP()
        Else
            Me.txtImp.EditValue = 0
            Me.txtTotalCXP.EditValue = 0
        End If
        If b Then
            Me.guardarComoSaldoCXP()
            If chbSaldoCXP.Checked Then
                MsgBox("Guardado como saldo CXP", MsgBoxStyle.OKOnly)
            Else
                MsgBox("No es saldo como saldo CXP", MsgBoxStyle.OKOnly)
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            If VerificandoAcceso_a_Modulos("CambiaCueta", "CambiaCueta", usua.Cedula, "Administraci�n") = False Then
                Dim frm As New frmCambiarCuentas
                frm.Id = txtNumero.Text
                frm.ShowDialog()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub AjusteInventario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp

    End Sub
End Class
