Imports System.Data
Imports System.Windows.Forms
Imports System.data.SqlClient

Public Class FrmAjusteBodega
    Inherits FrmPlantilla
    Dim Usua As Object
    Dim PMU As PerfilModulo_Class
    Dim info As Boolean = True
    Dim Lote As New DataSet_Movimiento_Bodega

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        Usua = Usuario
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.Label
    Friend WithEvents txtCedulaUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmAjusteBodega))
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapterBodegas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSet_Movimiento_Bodega As DataSet_Movimiento_Bodega
    Friend WithEvents SqlDataAdapterMovimiento_Bodega As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDataAdapter_MovimientDetalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ComboBoxBodegas As System.Windows.Forms.ComboBox
    Friend WithEvents colCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTipo_Entrada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTipo_Salida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents DateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBoxReferencia As System.Windows.Forms.TextBox
    Friend WithEvents RadioButtonSalida As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonEntrada As System.Windows.Forms.RadioButton
    Friend WithEvents TextBoxCodigo As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxCantidad As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxId As System.Windows.Forms.Label
    Friend WithEvents TextBoxDescripcion As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Check_Anulado As System.Windows.Forms.CheckBox
    Friend WithEvents Dt_FechaEntrada As System.Windows.Forms.DateTimePicker
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtExistencia As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DTPVencimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNuevoLote As System.Windows.Forms.TextBox
    Friend WithEvents CBNuevo As System.Windows.Forms.CheckBox
    Friend WithEvents CbNumero As System.Windows.Forms.ComboBox
    Friend WithEvents LNumero As System.Windows.Forms.Label
    Friend WithEvents LVencimiento As System.Windows.Forms.Label
    Friend WithEvents LNuevoLote As System.Windows.Forms.Label
    Friend WithEvents AdapterLotes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnectionLote As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmAjusteBodega))
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBoxReferencia = New System.Windows.Forms.TextBox
        Me.DataSet_Movimiento_Bodega = New LcPymes_5._2.DataSet_Movimiento_Bodega
        Me.TextBoxId = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.txtNombreUsuario = New System.Windows.Forms.Label
        Me.txtCedulaUsuario = New System.Windows.Forms.TextBox
        Me.ComboBoxBodegas = New System.Windows.Forms.ComboBox
        Me.DateTimePicker = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.RadioButtonSalida = New System.Windows.Forms.RadioButton
        Me.RadioButtonEntrada = New System.Windows.Forms.RadioButton
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.TextBoxCodigo = New System.Windows.Forms.TextBox
        Me.TextBoxDescripcion = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCodigo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colTipo_Entrada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTipo_Salida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapterBodegas = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.TextBoxCantidad = New System.Windows.Forms.TextBox
        Me.SqlDataAdapterMovimiento_Bodega = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDataAdapter_MovimientDetalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Check_Anulado = New System.Windows.Forms.CheckBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.LVencimiento = New System.Windows.Forms.Label
        Me.LNumero = New System.Windows.Forms.Label
        Me.LNuevoLote = New System.Windows.Forms.Label
        Me.DTPVencimiento = New System.Windows.Forms.DateTimePicker
        Me.txtNuevoLote = New System.Windows.Forms.TextBox
        Me.CBNuevo = New System.Windows.Forms.CheckBox
        Me.CbNumero = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtExistencia = New System.Windows.Forms.TextBox
        Me.Dt_FechaEntrada = New System.Windows.Forms.DateTimePicker
        Me.AdapterLotes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnectionLote = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.DataSet_Movimiento_Bodega, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(594, 32)
        Me.TituloModulo.Text = "Ajuste Bodega Inventario"
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.Text = "Anular"
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(449, 446)
        Me.DataNavigator.Name = "DataNavigator"
        Me.DataNavigator.Visible = False
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 416)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(594, 52)
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(5, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(480, 13)
        Me.Label1.TabIndex = 92
        Me.Label1.Text = "Nombre de la Casa consignante o Bodega"
        '
        'TextBoxReferencia
        '
        Me.TextBoxReferencia.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxReferencia.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.Referencia"))
        Me.TextBoxReferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxReferencia.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxReferencia.Location = New System.Drawing.Point(69, 50)
        Me.TextBoxReferencia.Name = "TextBoxReferencia"
        Me.TextBoxReferencia.Size = New System.Drawing.Size(512, 13)
        Me.TextBoxReferencia.TabIndex = 3
        Me.TextBoxReferencia.Text = ""
        '
        'DataSet_Movimiento_Bodega
        '
        Me.DataSet_Movimiento_Bodega.DataSetName = "DataSet_Movimiento_Bodega"
        Me.DataSet_Movimiento_Bodega.Locale = New System.Globalization.CultureInfo("es")
        '
        'TextBoxId
        '
        Me.TextBoxId.BackColor = System.Drawing.Color.White
        Me.TextBoxId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.Boleta_Movimiento"))
        Me.TextBoxId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxId.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxId.Location = New System.Drawing.Point(1, 16)
        Me.TextBoxId.Name = "TextBoxId"
        Me.TextBoxId.Size = New System.Drawing.Size(79, 13)
        Me.TextBoxId.TabIndex = 93
        Me.TextBoxId.Text = "00000"
        Me.TextBoxId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Controls.Add(Me.txtUsuario)
        Me.Panel1.Controls.Add(Me.txtNombreUsuario)
        Me.Panel1.Controls.Add(Me.txtCedulaUsuario)
        Me.Panel1.Location = New System.Drawing.Point(306, 424)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(291, 14)
        Me.Panel1.TabIndex = 0
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(-8, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 1
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtUsuario
        '
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(64, 0)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(56, 13)
        Me.txtUsuario.TabIndex = 0
        Me.txtUsuario.Text = ""
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.Color.Transparent
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(121, 0)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.Size = New System.Drawing.Size(163, 13)
        Me.txtNombreUsuario.TabIndex = 2
        '
        'txtCedulaUsuario
        '
        Me.txtCedulaUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtCedulaUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCedulaUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCedulaUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.Usuario"))
        Me.txtCedulaUsuario.Enabled = False
        Me.txtCedulaUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtCedulaUsuario.Location = New System.Drawing.Point(211, 16)
        Me.txtCedulaUsuario.Name = "txtCedulaUsuario"
        Me.txtCedulaUsuario.Size = New System.Drawing.Size(72, 13)
        Me.txtCedulaUsuario.TabIndex = 170
        Me.txtCedulaUsuario.Text = ""
        '
        'ComboBoxBodegas
        '
        Me.ComboBoxBodegas.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.Bodega"))
        Me.ComboBoxBodegas.DataSource = Me.DataSet_Movimiento_Bodega
        Me.ComboBoxBodegas.DisplayMember = "Bodegas.Nombre_Bodega"
        Me.ComboBoxBodegas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBodegas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxBodegas.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxBodegas.Location = New System.Drawing.Point(5, 26)
        Me.ComboBoxBodegas.Name = "ComboBoxBodegas"
        Me.ComboBoxBodegas.Size = New System.Drawing.Size(480, 21)
        Me.ComboBoxBodegas.TabIndex = 1
        Me.ComboBoxBodegas.ValueMember = "Bodegas.ID_Bodega"
        '
        'DateTimePicker
        '
        Me.DateTimePicker.CalendarForeColor = System.Drawing.Color.Blue
        Me.DateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.Fecha"))
        Me.DateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DateTimePicker.Location = New System.Drawing.Point(485, 26)
        Me.DateTimePicker.Name = "DateTimePicker"
        Me.DateTimePicker.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker.TabIndex = 2
        Me.DateTimePicker.Value = New Date(2006, 7, 29, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Blue
        Me.Label3.Location = New System.Drawing.Point(493, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 13)
        Me.Label3.TabIndex = 178
        Me.Label3.Text = "Fecha"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RadioButtonSalida
        '
        Me.RadioButtonSalida.BackColor = System.Drawing.Color.Transparent
        Me.RadioButtonSalida.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle.Tipo_Salida"))
        Me.RadioButtonSalida.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButtonSalida.ForeColor = System.Drawing.Color.Blue
        Me.RadioButtonSalida.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.RadioButtonSalida.Location = New System.Drawing.Point(336, 21)
        Me.RadioButtonSalida.Name = "RadioButtonSalida"
        Me.RadioButtonSalida.Size = New System.Drawing.Size(58, 16)
        Me.RadioButtonSalida.TabIndex = 1
        Me.RadioButtonSalida.Text = "&Salida"
        '
        'RadioButtonEntrada
        '
        Me.RadioButtonEntrada.BackColor = System.Drawing.Color.Transparent
        Me.RadioButtonEntrada.Checked = True
        Me.RadioButtonEntrada.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle.Tipo_Entrada"))
        Me.RadioButtonEntrada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButtonEntrada.ForeColor = System.Drawing.Color.Blue
        Me.RadioButtonEntrada.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.RadioButtonEntrada.Location = New System.Drawing.Point(256, 21)
        Me.RadioButtonEntrada.Name = "RadioButtonEntrada"
        Me.RadioButtonEntrada.Size = New System.Drawing.Size(69, 16)
        Me.RadioButtonEntrada.TabIndex = 0
        Me.RadioButtonEntrada.TabStop = True
        Me.RadioButtonEntrada.Text = "&Entrada"
        Me.RadioButtonEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Blue
        Me.Label4.Location = New System.Drawing.Point(176, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 13)
        Me.Label4.TabIndex = 180
        Me.Label4.Text = "Tipo Ajuste"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Blue
        Me.Label5.Location = New System.Drawing.Point(5, 50)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 13)
        Me.Label5.TabIndex = 181
        Me.Label5.Text = "Referencia"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(8, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(584, 13)
        Me.Label6.TabIndex = 182
        Me.Label6.Text = "Detalle de Ajuste"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Blue
        Me.Label7.Location = New System.Drawing.Point(7, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 13)
        Me.Label7.TabIndex = 184
        Me.Label7.Text = "C�digo"
        '
        'TextBoxCodigo
        '
        Me.TextBoxCodigo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle.Codigo"))
        Me.TextBoxCodigo.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxCodigo.Location = New System.Drawing.Point(7, 22)
        Me.TextBoxCodigo.Name = "TextBoxCodigo"
        Me.TextBoxCodigo.Size = New System.Drawing.Size(57, 13)
        Me.TextBoxCodigo.TabIndex = 4
        Me.TextBoxCodigo.Text = ""
        '
        'TextBoxDescripcion
        '
        Me.TextBoxDescripcion.BackColor = System.Drawing.Color.Transparent
        Me.TextBoxDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle.Descripcion"))
        Me.TextBoxDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxDescripcion.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxDescripcion.Location = New System.Drawing.Point(71, 6)
        Me.TextBoxDescripcion.Name = "TextBoxDescripcion"
        Me.TextBoxDescripcion.Size = New System.Drawing.Size(505, 13)
        Me.TextBoxDescripcion.TabIndex = 185
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Blue
        Me.Label10.Location = New System.Drawing.Point(399, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(64, 13)
        Me.Label10.TabIndex = 188
        Me.Label10.Text = "Cantidad"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle"
        Me.GridControl1.DataSource = Me.DataSet_Movimiento_Bodega
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.ForeColor = System.Drawing.Color.Blue
        Me.GridControl1.Location = New System.Drawing.Point(8, 208)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(584, 200)
        Me.GridControl1.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.SystemColors.HotTrack, System.Drawing.Color.White, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 7
        Me.GridControl1.Text = "GridControl"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCodigo, Me.colDescripcion, Me.colCantidad, Me.colTipo_Entrada, Me.colTipo_Salida})
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowDetailButtons = False
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowVertLines = False
        '
        'colCodigo
        '
        Me.colCodigo.Caption = "C�digo"
        Me.colCodigo.FieldName = "Codigo"
        Me.colCodigo.Name = "colCodigo"
        Me.colCodigo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCodigo.VisibleIndex = 0
        Me.colCodigo.Width = 62
        '
        'colDescripcion
        '
        Me.colDescripcion.Caption = "Descripci�n"
        Me.colDescripcion.FieldName = "Descripcion"
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colDescripcion.VisibleIndex = 1
        Me.colDescripcion.Width = 331
        '
        'colCantidad
        '
        Me.colCantidad.Caption = "Cantidad"
        Me.colCantidad.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colCantidad.DisplayFormat.FormatString = "#,#0.00"
        Me.colCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colCantidad.FieldName = "Cantidad"
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCantidad.VisibleIndex = 2
        Me.colCantidad.Width = 63
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'colTipo_Entrada
        '
        Me.colTipo_Entrada.Caption = "Entrada"
        Me.colTipo_Entrada.FieldName = "Tipo_Entrada"
        Me.colTipo_Entrada.Name = "colTipo_Entrada"
        Me.colTipo_Entrada.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTipo_Entrada.VisibleIndex = 3
        Me.colTipo_Entrada.Width = 56
        '
        'colTipo_Salida
        '
        Me.colTipo_Salida.Caption = "Salida"
        Me.colTipo_Salida.FieldName = "Tipo_Salida"
        Me.colTipo_Salida.Name = "colTipo_Salida"
        Me.colTipo_Salida.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTipo_Salida.VisibleIndex = 4
        Me.colTipo_Salida.Width = 58
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT ID_Bodega, Nombre_Bodega, Observaciones FROM Bodegas"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'SqlDataAdapterBodegas
        '
        Me.SqlDataAdapterBodegas.SelectCommand = Me.SqlSelectCommand2
        Me.SqlDataAdapterBodegas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Bodegas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ID_Bodega", "ID_Bodega"), New System.Data.Common.DataColumnMapping("Nombre_Bodega", "Nombre_Bodega"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT ID_Bodega, Nombre_Bodega, Observaciones FROM Bodegas ORDER BY Nombre_Bodeg" & _
        "a"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'TextBoxCantidad
        '
        Me.TextBoxCantidad.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxCantidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxCantidad.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle.Cantidad"))
        Me.TextBoxCantidad.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxCantidad.Location = New System.Drawing.Point(463, 22)
        Me.TextBoxCantidad.Name = "TextBoxCantidad"
        Me.TextBoxCantidad.Size = New System.Drawing.Size(113, 13)
        Me.TextBoxCantidad.TabIndex = 6
        Me.TextBoxCantidad.Text = ""
        '
        'SqlDataAdapterMovimiento_Bodega
        '
        Me.SqlDataAdapterMovimiento_Bodega.DeleteCommand = Me.SqlDeleteCommand2
        Me.SqlDataAdapterMovimiento_Bodega.InsertCommand = Me.SqlInsertCommand2
        Me.SqlDataAdapterMovimiento_Bodega.SelectCommand = Me.SqlSelectCommand3
        Me.SqlDataAdapterMovimiento_Bodega.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MovimientosBodega", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Boleta_Movimiento", "Boleta_Movimiento"), New System.Data.Common.DataColumnMapping("Bodega", "Bodega"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Referencia", "Referencia"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("FechaEntrada", "FechaEntrada")})})
        Me.SqlDataAdapterMovimiento_Bodega.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM MovimientosBodega WHERE (Boleta_Movimiento = @Original_Boleta_Movimie" & _
        "nto) AND (Anulado = @Original_Anulado) AND (Bodega = @Original_Bodega) AND (Fech" & _
        "a = @Original_Fecha) AND (FechaEntrada = @Original_FechaEntrada) AND (Referencia" & _
        " = @Original_Referencia) AND (Usuario = @Original_Usuario)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Boleta_Movimiento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Boleta_Movimiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Bodega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Bodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Referencia", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Referencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO MovimientosBodega(Bodega, Fecha, Referencia, Usuario, Anulado, FechaE" & _
        "ntrada) VALUES (@Bodega, @Fecha, @Referencia, @Usuario, @Anulado, @FechaEntrada)" & _
        "; SELECT Boleta_Movimiento, Bodega, Fecha, Referencia, Usuario, Anulado, FechaEn" & _
        "trada FROM MovimientosBodega WHERE (Boleta_Movimiento = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Bodega", System.Data.SqlDbType.Int, 4, "Bodega"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Referencia", System.Data.SqlDbType.VarChar, 255, "Referencia"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 8, "FechaEntrada"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT Boleta_Movimiento, Bodega, Fecha, Referencia, Usuario, Anulado, FechaEntra" & _
        "da FROM MovimientosBodega"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE MovimientosBodega SET Bodega = @Bodega, Fecha = @Fecha, Referencia = @Refe" & _
        "rencia, Usuario = @Usuario, Anulado = @Anulado, FechaEntrada = @FechaEntrada WHE" & _
        "RE (Boleta_Movimiento = @Original_Boleta_Movimiento) AND (Anulado = @Original_An" & _
        "ulado) AND (Bodega = @Original_Bodega) AND (Fecha = @Original_Fecha) AND (FechaE" & _
        "ntrada = @Original_FechaEntrada) AND (Referencia = @Original_Referencia) AND (Us" & _
        "uario = @Original_Usuario); SELECT Boleta_Movimiento, Bodega, Fecha, Referencia," & _
        " Usuario, Anulado, FechaEntrada FROM MovimientosBodega WHERE (Boleta_Movimiento " & _
        "= @Boleta_Movimiento)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Bodega", System.Data.SqlDbType.Int, 4, "Bodega"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Referencia", System.Data.SqlDbType.VarChar, 255, "Referencia"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 8, "FechaEntrada"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Boleta_Movimiento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Boleta_Movimiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Bodega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Bodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Referencia", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Referencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Boleta_Movimiento", System.Data.SqlDbType.BigInt, 8, "Boleta_Movimiento"))
        '
        'SqlDataAdapter_MovimientDetalle
        '
        Me.SqlDataAdapter_MovimientDetalle.DeleteCommand = Me.SqlDeleteCommand3
        Me.SqlDataAdapter_MovimientDetalle.InsertCommand = Me.SqlInsertCommand3
        Me.SqlDataAdapter_MovimientDetalle.SelectCommand = Me.SqlSelectCommand4
        Me.SqlDataAdapter_MovimientDetalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MovimientosBodega_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Detalle", "Id_Detalle"), New System.Data.Common.DataColumnMapping("Boleta_Movimiento", "Boleta_Movimiento"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Tipo_Entrada", "Tipo_Entrada"), New System.Data.Common.DataColumnMapping("Tipo_Salida", "Tipo_Salida"), New System.Data.Common.DataColumnMapping("Numero", "Numero")})})
        Me.SqlDataAdapter_MovimientDetalle.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM MovimientosBodega_Detalle WHERE (Id_Detalle = @Original_Id_Detalle) A" & _
        "ND (Boleta_Movimiento = @Original_Boleta_Movimiento) AND (Cantidad = @Original_C" & _
        "antidad) AND (Codigo = @Original_Codigo) AND (Descripcion = @Original_Descripcio" & _
        "n) AND (Numero = @Original_Numero) AND (Tipo_Entrada = @Original_Tipo_Entrada) A" & _
        "ND (Tipo_Salida = @Original_Tipo_Salida)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Detalle", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Detalle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Boleta_Movimiento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Boleta_Movimiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Numero", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numero", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo_Entrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo_Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo_Salida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo_Salida", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO MovimientosBodega_Detalle(Boleta_Movimiento, Codigo, Descripcion, Can" & _
        "tidad, Tipo_Entrada, Tipo_Salida, Numero) VALUES (@Boleta_Movimiento, @Codigo, @" & _
        "Descripcion, @Cantidad, @Tipo_Entrada, @Tipo_Salida, @Numero); SELECT Id_Detalle" & _
        ", Boleta_Movimiento, Codigo, Descripcion, Cantidad, Tipo_Entrada, Tipo_Salida, N" & _
        "umero FROM MovimientosBodega_Detalle WHERE (Id_Detalle = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Boleta_Movimiento", System.Data.SqlDbType.BigInt, 8, "Boleta_Movimiento"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 255, "Codigo"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo_Entrada", System.Data.SqlDbType.Bit, 1, "Tipo_Entrada"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo_Salida", System.Data.SqlDbType.Bit, 1, "Tipo_Salida"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Numero", System.Data.SqlDbType.VarChar, 250, "Numero"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Id_Detalle, Boleta_Movimiento, Codigo, Descripcion, Cantidad, Tipo_Entrada" & _
        ", Tipo_Salida, Numero FROM MovimientosBodega_Detalle"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE MovimientosBodega_Detalle SET Boleta_Movimiento = @Boleta_Movimiento, Codi" & _
        "go = @Codigo, Descripcion = @Descripcion, Cantidad = @Cantidad, Tipo_Entrada = @" & _
        "Tipo_Entrada, Tipo_Salida = @Tipo_Salida, Numero = @Numero WHERE (Id_Detalle = @" & _
        "Original_Id_Detalle) AND (Boleta_Movimiento = @Original_Boleta_Movimiento) AND (" & _
        "Cantidad = @Original_Cantidad) AND (Codigo = @Original_Codigo) AND (Descripcion " & _
        "= @Original_Descripcion) AND (Numero = @Original_Numero) AND (Tipo_Entrada = @Or" & _
        "iginal_Tipo_Entrada) AND (Tipo_Salida = @Original_Tipo_Salida); SELECT Id_Detall" & _
        "e, Boleta_Movimiento, Codigo, Descripcion, Cantidad, Tipo_Entrada, Tipo_Salida, " & _
        "Numero FROM MovimientosBodega_Detalle WHERE (Id_Detalle = @Id_Detalle)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Boleta_Movimiento", System.Data.SqlDbType.BigInt, 8, "Boleta_Movimiento"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.VarChar, 255, "Codigo"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo_Entrada", System.Data.SqlDbType.Bit, 1, "Tipo_Entrada"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo_Salida", System.Data.SqlDbType.Bit, 1, "Tipo_Salida"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Numero", System.Data.SqlDbType.VarChar, 250, "Numero"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Detalle", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Detalle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Boleta_Movimiento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Boleta_Movimiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Numero", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numero", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo_Entrada", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo_Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo_Salida", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo_Salida", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Detalle", System.Data.SqlDbType.BigInt, 8, "Id_Detalle"))
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.Check_Anulado)
        Me.Panel2.Controls.Add(Me.DateTimePicker)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.TextBoxReferencia)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.ComboBoxBodegas)
        Me.Panel2.Enabled = False
        Me.Panel2.Location = New System.Drawing.Point(8, 40)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(584, 72)
        Me.Panel2.TabIndex = 189
        '
        'Check_Anulado
        '
        Me.Check_Anulado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.Anulado"))
        Me.Check_Anulado.Enabled = False
        Me.Check_Anulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Anulado.ForeColor = System.Drawing.Color.Red
        Me.Check_Anulado.Location = New System.Drawing.Point(400, 8)
        Me.Check_Anulado.Name = "Check_Anulado"
        Me.Check_Anulado.Size = New System.Drawing.Size(81, 16)
        Me.Check_Anulado.TabIndex = 191
        Me.Check_Anulado.Text = "Anulado"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.LVencimiento)
        Me.Panel3.Controls.Add(Me.LNumero)
        Me.Panel3.Controls.Add(Me.LNuevoLote)
        Me.Panel3.Controls.Add(Me.DTPVencimiento)
        Me.Panel3.Controls.Add(Me.txtNuevoLote)
        Me.Panel3.Controls.Add(Me.CBNuevo)
        Me.Panel3.Controls.Add(Me.CbNumero)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.txtExistencia)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.TextBoxDescripcion)
        Me.Panel3.Controls.Add(Me.TextBoxCantidad)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.TextBoxCodigo)
        Me.Panel3.Controls.Add(Me.RadioButtonEntrada)
        Me.Panel3.Controls.Add(Me.RadioButtonSalida)
        Me.Panel3.Enabled = False
        Me.Panel3.Location = New System.Drawing.Point(8, 127)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(584, 73)
        Me.Panel3.TabIndex = 190
        '
        'LVencimiento
        '
        Me.LVencimiento.BackColor = System.Drawing.Color.Transparent
        Me.LVencimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LVencimiento.ForeColor = System.Drawing.Color.Blue
        Me.LVencimiento.Location = New System.Drawing.Point(440, 48)
        Me.LVencimiento.Name = "LVencimiento"
        Me.LVencimiento.Size = New System.Drawing.Size(40, 13)
        Me.LVencimiento.TabIndex = 200
        Me.LVencimiento.Text = "Vence"
        Me.LVencimiento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LVencimiento.Visible = False
        '
        'LNumero
        '
        Me.LNumero.BackColor = System.Drawing.Color.Transparent
        Me.LNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LNumero.ForeColor = System.Drawing.Color.Blue
        Me.LNumero.Location = New System.Drawing.Point(8, 48)
        Me.LNumero.Name = "LNumero"
        Me.LNumero.Size = New System.Drawing.Size(40, 13)
        Me.LNumero.TabIndex = 199
        Me.LNumero.Text = "Lote :"
        Me.LNumero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LNumero.Visible = False
        '
        'LNuevoLote
        '
        Me.LNuevoLote.BackColor = System.Drawing.Color.Transparent
        Me.LNuevoLote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LNuevoLote.ForeColor = System.Drawing.Color.Blue
        Me.LNuevoLote.Location = New System.Drawing.Point(256, 48)
        Me.LNuevoLote.Name = "LNuevoLote"
        Me.LNuevoLote.Size = New System.Drawing.Size(40, 13)
        Me.LNuevoLote.TabIndex = 198
        Me.LNuevoLote.Text = "Lote"
        Me.LNuevoLote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LNuevoLote.Visible = False
        '
        'DTPVencimiento
        '
        Me.DTPVencimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DTPVencimiento.Location = New System.Drawing.Point(488, 48)
        Me.DTPVencimiento.Name = "DTPVencimiento"
        Me.DTPVencimiento.Size = New System.Drawing.Size(88, 20)
        Me.DTPVencimiento.TabIndex = 197
        Me.DTPVencimiento.Visible = False
        '
        'txtNuevoLote
        '
        Me.txtNuevoLote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNuevoLote.Location = New System.Drawing.Point(304, 48)
        Me.txtNuevoLote.Name = "txtNuevoLote"
        Me.txtNuevoLote.Size = New System.Drawing.Size(128, 20)
        Me.txtNuevoLote.TabIndex = 195
        Me.txtNuevoLote.Text = ""
        Me.txtNuevoLote.Visible = False
        '
        'CBNuevo
        '
        Me.CBNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBNuevo.ForeColor = System.Drawing.Color.Blue
        Me.CBNuevo.Location = New System.Drawing.Point(184, 48)
        Me.CBNuevo.Name = "CBNuevo"
        Me.CBNuevo.Size = New System.Drawing.Size(64, 16)
        Me.CBNuevo.TabIndex = 193
        Me.CBNuevo.Text = "Nuevo"
        Me.CBNuevo.Visible = False
        '
        'CbNumero
        '
        Me.CbNumero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbNumero.Location = New System.Drawing.Point(56, 48)
        Me.CbNumero.Name = "CbNumero"
        Me.CbNumero.Size = New System.Drawing.Size(120, 21)
        Me.CbNumero.TabIndex = 192
        Me.CbNumero.Visible = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(72, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 190
        Me.Label2.Text = "Cant."
        '
        'txtExistencia
        '
        Me.txtExistencia.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtExistencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExistencia.Enabled = False
        Me.txtExistencia.ForeColor = System.Drawing.Color.Blue
        Me.txtExistencia.Location = New System.Drawing.Point(120, 24)
        Me.txtExistencia.Name = "txtExistencia"
        Me.txtExistencia.Size = New System.Drawing.Size(48, 13)
        Me.txtExistencia.TabIndex = 189
        Me.txtExistencia.Text = ""
        '
        'Dt_FechaEntrada
        '
        Me.Dt_FechaEntrada.CalendarForeColor = System.Drawing.Color.Blue
        Me.Dt_FechaEntrada.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Movimiento_Bodega, "MovimientosBodega.FechaEntrada"))
        Me.Dt_FechaEntrada.Enabled = False
        Me.Dt_FechaEntrada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dt_FechaEntrada.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.Dt_FechaEntrada.Location = New System.Drawing.Point(496, 448)
        Me.Dt_FechaEntrada.Name = "Dt_FechaEntrada"
        Me.Dt_FechaEntrada.Size = New System.Drawing.Size(96, 20)
        Me.Dt_FechaEntrada.TabIndex = 192
        Me.Dt_FechaEntrada.Value = New Date(2006, 7, 29, 0, 0, 0, 0)
        '
        'AdapterLotes
        '
        Me.AdapterLotes.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterLotes.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterLotes.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterLotes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Lotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Numero", "Numero"), New System.Data.Common.DataColumnMapping("Vencimiento", "Vencimiento"), New System.Data.Common.DataColumnMapping("Cant_Inicial", "Cant_Inicial"), New System.Data.Common.DataColumnMapping("Cant_Actual", "Cant_Actual"), New System.Data.Common.DataColumnMapping("Fecha_Entrada", "Fecha_Entrada"), New System.Data.Common.DataColumnMapping("Cod_Articulo", "Cod_Articulo"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo")})})
        Me.AdapterLotes.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Lotes WHERE (Id = @Original_Id) AND (Cant_Actual = @Original_Cant_Act" & _
        "ual) AND (Cant_Inicial = @Original_Cant_Inicial) AND (Cod_Articulo = @Original_C" & _
        "od_Articulo) AND (Documento = @Original_Documento) AND (Fecha_Entrada = @Origina" & _
        "l_Fecha_Entrada) AND (Numero = @Original_Numero) AND (Tipo = @Original_Tipo) AND" & _
        " (Vencimiento = @Original_Vencimiento)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnectionLote
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cant_Actual", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cant_Actual", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cant_Inicial", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cant_Inicial", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Articulo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha_Entrada", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha_Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Numero", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numero", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Vencimiento", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Vencimiento", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnectionLote
        '
        Me.SqlConnectionLote.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Lotes(Numero, Vencimiento, Cant_Inicial, Cant_Actual, Fecha_Entrada, " & _
        "Cod_Articulo, Documento, Tipo) VALUES (@Numero, @Vencimiento, @Cant_Inicial, @Ca" & _
        "nt_Actual, @Fecha_Entrada, @Cod_Articulo, @Documento, @Tipo); SELECT Id, Numero," & _
        " Vencimiento, Cant_Inicial, Cant_Actual, Fecha_Entrada, Cod_Articulo, Documento," & _
        " Tipo FROM Lotes WHERE (Id = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnectionLote
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Numero", System.Data.SqlDbType.VarChar, 250, "Numero"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Vencimiento", System.Data.SqlDbType.DateTime, 8, "Vencimiento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cant_Inicial", System.Data.SqlDbType.Float, 8, "Cant_Inicial"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cant_Actual", System.Data.SqlDbType.Float, 8, "Cant_Actual"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha_Entrada", System.Data.SqlDbType.DateTime, 8, "Fecha_Entrada"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.BigInt, 8, "Documento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 3, "Tipo"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT Id, Numero, Vencimiento, Cant_Inicial, Cant_Actual, Fecha_Entrada, Cod_Art" & _
        "iculo, Documento, Tipo FROM Lotes"
        Me.SqlSelectCommand5.Connection = Me.SqlConnectionLote
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Lotes SET Numero = @Numero, Vencimiento = @Vencimiento, Cant_Inicial = @Ca" & _
        "nt_Inicial, Cant_Actual = @Cant_Actual, Fecha_Entrada = @Fecha_Entrada, Cod_Arti" & _
        "culo = @Cod_Articulo, Documento = @Documento, Tipo = @Tipo WHERE (Id = @Original" & _
        "_Id) AND (Cant_Actual = @Original_Cant_Actual) AND (Cant_Inicial = @Original_Can" & _
        "t_Inicial) AND (Cod_Articulo = @Original_Cod_Articulo) AND (Documento = @Origina" & _
        "l_Documento) AND (Fecha_Entrada = @Original_Fecha_Entrada) AND (Numero = @Origin" & _
        "al_Numero) AND (Tipo = @Original_Tipo) AND (Vencimiento = @Original_Vencimiento)" & _
        "; SELECT Id, Numero, Vencimiento, Cant_Inicial, Cant_Actual, Fecha_Entrada, Cod_" & _
        "Articulo, Documento, Tipo FROM Lotes WHERE (Id = @Id)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnectionLote
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Numero", System.Data.SqlDbType.VarChar, 250, "Numero"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Vencimiento", System.Data.SqlDbType.DateTime, 8, "Vencimiento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cant_Inicial", System.Data.SqlDbType.Float, 8, "Cant_Inicial"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cant_Actual", System.Data.SqlDbType.Float, 8, "Cant_Actual"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha_Entrada", System.Data.SqlDbType.DateTime, 8, "Fecha_Entrada"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Articulo", System.Data.SqlDbType.BigInt, 8, "Cod_Articulo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.BigInt, 8, "Documento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 3, "Tipo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cant_Actual", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cant_Actual", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cant_Inicial", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cant_Inicial", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Articulo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Articulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha_Entrada", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha_Entrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Numero", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numero", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Vencimiento", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Vencimiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id"))
        '
        'FrmAjusteBodega
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(594, 468)
        Me.Controls.Add(Me.Dt_FechaEntrada)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TextBoxId)
        Me.MaximumSize = New System.Drawing.Size(600, 500)
        Me.Name = "FrmAjusteBodega"
        Me.Text = "Ajuste Bodega Inventario"
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.TextBoxId, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.Label6, 0)
        Me.Controls.SetChildIndex(Me.GridControl1, 0)
        Me.Controls.SetChildIndex(Me.Panel2, 0)
        Me.Controls.SetChildIndex(Me.Panel3, 0)
        Me.Controls.SetChildIndex(Me.Dt_FechaEntrada, 0)
        CType(Me.DataSet_Movimiento_Bodega, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Load"
    Private Sub FrmAjusteBodega_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SqlConnection.ConnectionString = GetSetting("SeeSOFT", "SeePos", "Conexion")
        SqlConnectionLote.ConnectionString = GetSetting("SeeSOFT", "SeePos", "Conexion")
        Me.SqlDataAdapterBodegas.Fill(Me.DataSet_Movimiento_Bodega, "Bodegas")
        AdapterLotes.Fill(Lote, "Lotes")

        Me.DataSet_Movimiento_Bodega.MovimientosBodega.Boleta_MovimientoColumn.AutoIncrement = True
        Me.DataSet_Movimiento_Bodega.MovimientosBodega.Boleta_MovimientoColumn.AutoIncrementSeed = -1
        Me.DataSet_Movimiento_Bodega.MovimientosBodega.Boleta_MovimientoColumn.AutoIncrementStep = -1
        Me.DataSet_Movimiento_Bodega.MovimientosBodega.BodegaColumn.DefaultValue = 0
        Me.DataSet_Movimiento_Bodega.MovimientosBodega.FechaColumn.DefaultValue = Now.Date
        Me.DataSet_Movimiento_Bodega.MovimientosBodega.ReferenciaColumn.DefaultValue = ""
        Me.DataSet_Movimiento_Bodega.MovimientosBodega.AnuladoColumn.DefaultValue = False
        Me.DataSet_Movimiento_Bodega.MovimientosBodega.FechaEntradaColumn.DefaultValue = Now

        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Id_DetalleColumn.AutoIncrement = True
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Id_DetalleColumn.AutoIncrementSeed = -1
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Id_DetalleColumn.AutoIncrementStep = -1
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.CodigoColumn.DefaultValue = 0
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.DescripcionColumn.DefaultValue = ""
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Tipo_EntradaColumn.DefaultValue = True
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Tipo_SalidaColumn.DefaultValue = False
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.CantidadColumn.DefaultValue = 0
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.NumeroColumn.DefaultValue = 0

        'VALORES POR DEFECTO PARA LA TABLA ARTICULOS LOTES
        Me.Lote.Lotes.IdColumn.AutoIncrement = True
        Me.Lote.Lotes.IdColumn.AutoIncrementStep = 1
        Me.Lote.Lotes.NumeroColumn.DefaultValue = 0
        Me.Lote.Lotes.VencimientoColumn.DefaultValue = Now.Date
        Me.Lote.Lotes.Fecha_EntradaColumn.DefaultValue = Date.Now
        Me.Lote.Lotes.Cant_InicialColumn.DefaultValue = 0
        Me.Lote.Lotes.Cant_ActualColumn.DefaultValue = 0
        Me.Lote.Lotes.Cod_ArticuloColumn.DefaultValue = 0
        Me.Lote.Lotes.DocumentoColumn.DefaultValue = 0
        Me.Lote.Lotes.TipoColumn.DefaultValue = "AIB"

        Me.ToolBar1.Buttons(0).Enabled = False
    End Sub
#End Region

#Region "Toolbar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Try
            Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
                Case 1 : Nuevo_o_Cancelar()
                Case 2 : If PMU.Find Then Me.CargarAjuste() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 3 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 4 : If PMU.Delete Then Eliminar() Else MsgBox("No tiene permiso para eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 5 : If PMU.Print Then Imprimir(Me.TextBoxId.Text) Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                Case 7 : If MessageBox.Show("�Desea Cerrar el modulo actual..?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then Me.Close()
            End Select
        Catch ex As Exception
            If Err.Number <> 91 Then
                MsgBox(ex.Message & " numero : " & Err.Number)
            Else
                MsgBox("Debe introducir su codigo")
            End If
        End Try
    End Sub
#End Region

#Region "Nuevo"
    Private Sub Nuevo_o_Cancelar()
        NuevaEntrada()
        If ToolBar1.Buttons(0).Text = "Cancelar" Then
            Me.ToolBar1.Buttons(0).Enabled = False
            Me.ToolBar1.Buttons(2).Enabled = False
            Me.ToolBar1.Buttons(3).Enabled = False
            Me.ToolBar1.Buttons(4).Enabled = False
        Else ' cuando es Nuevo

            Me.ToolBar1.Buttons(0).Enabled = False
            Me.ToolBar1.Buttons(2).Enabled = False
            Me.Panel2.Enabled = False
            Me.Panel3.Enabled = False
            GridControl1.Enabled = False
            Me.txtUsuario.Text = ""
            Me.txtNombreUsuario.Text = ""
            Me.txtCedulaUsuario.Text = ""
            Me.ComboBoxBodegas.SelectedIndex = -1
            ComboBoxBodegas.Focus()
        End If
    End Sub

    Private Sub NuevaEntrada()
        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Clear()
        Me.DataSet_Movimiento_Bodega.MovimientosBodega.Clear()
        Me.NuevosDatos(Me.DataSet_Movimiento_Bodega, Me.DataSet_Movimiento_Bodega.MovimientosBodega.ToString)
        If ToolBar1.Buttons(0).Text = "Cancelar" Then ComboBoxBodegas.Focus()
    End Sub
#End Region

#Region "Buscar"
    Private Sub CargarAjuste()
        Try
            If ToolBar1.Buttons(0).Text = "Cancelar" Then
                If MsgBox("Esta realizando un ajuste nuevo, si continua perdera los datos.., �Desea continuar con el movimiento?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If

            Dim Cx As New Conexion
            Dim Fx As New cFunciones
            Dim Buscar As New FrmBuscador
            Buscar.SQLString = "SELECT MovimientosBodega.Boleta_Movimiento AS Movimiento, MovimientosBodega.Referencia AS Boleta, Bodegas.Nombre_Bodega AS Bodega, MovimientosBodega.Fecha FROM MovimientosBodega INNER JOIN  Bodegas ON MovimientosBodega.Bodega = Bodegas.ID_Bodega ORDER BY Boleta_Movimiento DESC"
            Buscar.Text = "Buscar Ajuste de Bodega"
            Buscar.CampoFiltro = "Bodega"
            Buscar.CampoFecha = "Fecha"
            Buscar.ShowDialog()

            If Buscar.Cancelado Then
                Exit Sub
            Else
                If ToolBar1.Buttons(0).Text = "Cancelar" Then
                    Nuevo_o_Cancelar()
                End If

                Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega").CancelCurrentEdit()
                Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Clear()
                Me.DataSet_Movimiento_Bodega.MovimientosBodega.Clear()
                Me.ToolBar1.Buttons(0).Enabled = False
                Me.ToolBar1.Buttons(2).Enabled = False
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.ToolBar1.Buttons(4).Enabled = False

                Fx.Cargar_Tabla_Generico(Me.SqlDataAdapterMovimiento_Bodega, "SELECT * FROM MovimientosBodega WHERE (Boleta_Movimiento = " & Buscar.Codigo & ")")
                Me.SqlDataAdapterMovimiento_Bodega.Fill(Me.DataSet_Movimiento_Bodega, "MovimientosBodega")

                Fx.Cargar_Tabla_Generico(Me.SqlDataAdapter_MovimientDetalle, "SELECT * FROM MovimientosBodega_Detalle WHERE (Boleta_Movimiento = " & Buscar.Codigo & ")")
                Me.SqlDataAdapter_MovimientDetalle.Fill(Me.DataSet_Movimiento_Bodega, "MovimientosBodega_Detalle")

                If Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega").Current("Anulado") = False Then
                    Me.ToolBar1.Buttons(3).Enabled = True
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
        Me.ToolBar1.Buttons(4).Enabled = True
    End Sub
#End Region

#Region "Registrar"
    Private Sub Registrar()
        Try
            Dim funciones As New Conexion
            If Not (RegistraLote()) Then    'REGISTRA LOTES NUEVOS
                Exit Sub
            End If
            Me.RegistrarDatos(Me.SqlDataAdapterMovimiento_Bodega, Me.DataSet_Movimiento_Bodega, Me.DataSet_Movimiento_Bodega.MovimientosBodega.ToString, True, False)
            Me.RegistrarDatos(Me.SqlDataAdapter_MovimientDetalle, Me.DataSet_Movimiento_Bodega, Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.ToString, True)
            funciones.UpdateRecords("Lotes", "Documento = " & BindingContext(DataSet_Movimiento_Bodega, "MovimientosBodega").Current("Boleta_Movimiento"), "Tipo = 'AIB' AND Documento = 0")
            If MsgBox("Desea agregar un nuevo ajuste de Bodega...", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then NuevaEntrada() Else Limpiar()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Eliminar"
    Private Sub Eliminar()
        Try
            Dim resp As Integer
            If Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega").Count > 0 Then
                resp = MessageBox.Show("�Desea Anular este Ajuste de Bodega?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    Check_Anulado.Checked = True
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega").EndCurrentEdit()

                    If Me.insertar_bitacora() And Registrar_Anulacion_Ajuste() Then

                        Me.DataSet_Movimiento_Bodega.AcceptChanges()
                        MsgBox("El ajuste ha sido anulado correctamente", MsgBoxStyle.Information)
                        Me.DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Clear()
                        Me.DataSet_Movimiento_Bodega.MovimientosBodega.Clear()
                        Me.ToolBar1.Buttons(4).Enabled = True
                        Me.ToolBar1.Buttons(1).Enabled = True
                        Me.ToolBar1.Buttons(0).Text = "Nuevo"
                        Me.ToolBar1.Buttons(0).ImageIndex = 0

                        Me.ToolBar1.Buttons(0).Enabled = False
                        Me.ToolBar1.Buttons(1).Enabled = True
                        Me.ToolBar1.Buttons(2).Enabled = False
                        Me.ToolBar1.Buttons(3).Enabled = False
                        Me.ToolBar1.Buttons(4).Enabled = False
                        txtUsuario.Enabled = True
                        txtUsuario.Text = ""
                        txtNombreUsuario.Text = ""
                        txtCedulaUsuario.Text = ""
                        txtUsuario.Focus()
                        Me.ToolBar1.Buttons(3).Enabled = False
                        Me.ToolBar1.Buttons(4).Enabled = False
                    End If
                Else : Exit Sub
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Function insertar_bitacora() As Boolean
        Dim funciones As New Conexion
        Dim datos As String
        datos = "'AJUSTE BODEGA','" & Me.TextBoxId.Text & "','AJUSTE BODEGA','AJUSTE BODEGA ANULADO','" & Now & "','" & Usua.Nombre & "'," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0
        If funciones.AddNewRecord("Bitacora", "Tabla,Campo_Clave,DescripcionCampo,Accion, Fecha,Usuario,Costo,VentaA,VentaB,VentaC,VentaD", datos) <> "" Then
            MsgBox("Problemas al Anular el ajuste", MsgBoxStyle.Critical)
            Return False
        Else
            Return True
        End If
    End Function

    Function Registrar_Anulacion_Ajuste() As Boolean
        If Me.SqlConnection.State <> Me.SqlConnection.State.Open Then Me.SqlConnection.Open()
        Dim Trans As SqlTransaction = Me.SqlConnection.BeginTransaction
        Try

            Me.SqlDataAdapterMovimiento_Bodega.UpdateCommand.Transaction = Trans
            Me.SqlDataAdapterMovimiento_Bodega.Update(Me.DataSet_Movimiento_Bodega, "MovimientosBodega")

            Trans.Commit()
            Return True


        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            Me.ToolBar1.Buttons(3).Enabled = True
            Return False
        End Try
    End Function
#End Region

#Region "Imprimir"
    Private Sub Imprimir(ByVal Boleta As String)
        Try
            Dim Reporte As New ReporteMovimientoBodega
            Reporte.SetParameterValue(0, Boleta)
            CrystalReportsConexion.LoadShow(Reporte, MdiParent)
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Funciones Controles"
    Private Sub TextBox_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxBodegas.KeyDown, DateTimePicker.KeyDown, TextBoxReferencia.KeyDown, txtUsuario.KeyDown, TextBoxCantidad.KeyDown, TextBoxCodigo.KeyDown, RadioButtonEntrada.KeyDown, RadioButtonSalida.KeyDown
        If e.KeyCode = Keys.Enter Then
            Select Case sender.NAME
                Case Me.txtUsuario.Name
                    If Not ValidarUsuario() Then Exit Sub
                    Me.ToolBar1.Buttons(0).Enabled = True
                    Me.ToolBar1.Buttons(2).Enabled = True
                    Me.ToolBar1.Buttons(3).Enabled = False
                    Me.ToolBar1.Buttons(4).Enabled = False
                    Me.Panel2.Enabled = True
                    ComboBoxBodegas.Focus()

                Case Me.ComboBoxBodegas.Name
                    DateTimePicker.Focus()

                Case Me.DateTimePicker.Name
                    TextBoxReferencia.Focus()

                Case Me.TextBoxReferencia.Name
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega").EndCurrentEdit()
                    Me.Panel3.Enabled = True
                    GridControl1.Enabled = True
                    TextBoxCodigo.Focus()
                    Me.TextBoxCodigo.SelectAll()

                Case Me.TextBoxCantidad.Name
                    Dim func As New Conexion
                    If CDbl(Me.TextBoxCantidad.Text) = 0 Then
                        MsgBox("La cantidad no puede ser 0!!", MsgBoxStyle.Critical)
                        Exit Sub
                    End If
                    If Me.RadioButtonSalida.Checked = True And func.SlqExecuteScalar(func.Conectar, "SELECT ExistenciaBodega FROM Inventario WHERE (Codigo = " & CInt(Me.TextBoxCodigo.Text) & ")") < CDbl(Me.TextBoxCantidad.Text) Then
                        MsgBox("La cantidad en consignaci�n es menor a la que usted desea sacar de Existencia!!" & vbCrLf & "No se puede Continuar!!", MsgBoxStyle.Critical)
                        'If MsgBox("La cantidad en consignaci�n es menor a la que usted desea sacar de Existencia.., �Desea continuar con el movimiento?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        Exit Sub
                        'End If
                    End If

                    If CbNumero.Visible = True Then
                        BindingContext(DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").Current("Numero") = CbNumero.Text
                    ElseIf CBNuevo.Checked = True Then
                        BindingContext(DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").Current("Numero") = txtNuevoLote.Text
                        AgregaLote()
                        CBNuevo.Checked = False
                    Else
                        BindingContext(DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").Current("Numero") = "0"
                    End If

                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").EndCurrentEdit()
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").AddNew()
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").CancelCurrentEdit()
                    CBNuevo.Checked = False
                    CBNuevo.Visible = False
                    CbNumero.Items.Clear()
                    CbNumero.Visible = False
                    LNumero.Visible = False
                    Me.TextBoxCodigo.Focus()
                    Me.TextBoxCodigo.SelectAll()
                    Exit Sub

                Case Me.TextBoxCodigo.Name
                    If TextBoxCodigo.Text <> "" And TextBoxCodigo.Text <> "0" Then
                        Dim strCodigo As String = Me.TextBoxCodigo.Text
                        Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").CancelCurrentEdit()
                        Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").EndCurrentEdit()
                        Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").AddNew()
                        CargarInformacionArticulo(strCodigo)
                        If info = False Then
                            Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").CancelCurrentEdit()
                            TextBoxCodigo.Focus()
                            Me.TextBoxCodigo.SelectAll()
                            Exit Sub
                        End If
                    End If
                    Exit Sub
            End Select
        End If

        If e.KeyCode = Keys.F1 Then
            '=================================
            ' JCGA 25 DE JUNIO 2007
            Select Case sender.NAME
                Case Me.TextBoxCodigo.Name

                    Dim BuscarArt As New FrmBuscarArticulo
                    Try
                        Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").CancelCurrentEdit()
                        Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").EndCurrentEdit()
                        Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").AddNew()
                        BuscarArt.StartPosition = FormStartPosition.CenterParent
                        BuscarArt.ShowDialog()
                        If BuscarArt.Cancelado Then
                            Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").CancelCurrentEdit()
                            Exit Sub
                        End If
                        Me.TextBoxCodigo.Text = BuscarArt.Codigo
                        BuscarArt.Dispose()

                        CargarInformacionArticulo(Me.TextBoxCodigo.Text)
                        If info = False Then
                            Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").CancelCurrentEdit()
                            TextBoxCodigo.Focus()
                            Me.TextBoxCodigo.SelectAll()
                            Exit Sub
                        End If

                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Critical, "Atenci�n...")
                    End Try

            End Select
            '=================================
        End If

        If e.KeyCode = Keys.F2 Then
            '=================================
            'Oscar
            Dim resp As Integer
            If Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").Count > 0 Then  ' si hay detalles
                resp = MessageBox.Show("�Desea guardar este Ajuste?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").CancelCurrentEdit()
                    Registrar()
                End If
            Else
                MsgBox("No Existen Art�culos para guardar del ajuste!!", MsgBoxStyle.Information)
            End If
            '=================================
        End If
    End Sub

    Private Sub RadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonEntrada.CheckedChanged, RadioButtonSalida.CheckedChanged
        Me.TextBoxCantidad.Focus()
        Me.TextBoxCantidad.SelectAll()
    End Sub

    Private Sub GridControl1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl1.KeyDown
        If e.KeyCode = Keys.Delete Then
            Eliminar_Articulo_Detalle()
        End If
    End Sub
#End Region

#Region "Funciones y Procedimientos"
    Private Sub CargarInformacionArticulo(ByVal Codigo As String)
        Dim Registros As SqlClient.SqlDataReader
        Dim Conexion As New Conexion
        Dim Func As New Conexion

        Registros = Conexion.GetRecorset(Conexion.Conectar, "SELECT Inventario.Codigo, Inventario.Descripcion + ' (' + Cast(dbo.Inventario.PresentaCant AS VARCHAR) + ' ' + ISNULL(Presentaciones.Presentaciones, '') + ')' AS DescripcionArticulo, Inventario.ExistenciaBodega, Inventario.Lote FROM Inventario INNER JOIN  Presentaciones ON Inventario.CodPresentacion = Presentaciones.CodPres WHERE (Inventario.inhabilitado = 0) AND (Inventario.Consignacion = 1) AND (Inventario.Id_Bodega = " & ComboBoxBodegas.SelectedValue & ") AND (Inventario.Codigo = '" & Codigo & "') OR  (Inventario.Barras = '" & Codigo & "')")
        Try

            '==================================================
            'JCGA 25 JUN 2007
            info = False
            '==================================================

            While Registros.Read
                Me.TextBoxCodigo.Text = Registros!Codigo
                Me.TextBoxDescripcion.Text = Registros!DescripcionArticulo
                txtExistencia.Text = Registros!ExistenciaBodega
                If txtExistencia.Text = "" Then
                    txtExistencia.Text = 0
                End If

                If Registros!Lote Then
                    ActivaLote()
                    CbNumero.Focus()
                Else
                    ActivaNinguno()
                    TextBoxCantidad.Focus()
                End If

                info = True
            End While

            '==================================================
            If info <> True Then
                info = False
                MsgBox("El art�culo no existe, esta deshabilitado o no se encuentra en la Bodega seleccionada!!", MsgBoxStyle.Exclamation)
            End If
            '==================================================

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Atenci�n..")
        Finally
            Conexion.DesConectar(Conexion.sQlconexion)
            Func.DesConectar(Func.sQlconexion)
        End Try
    End Sub

    Private Sub Eliminar_Articulo_Detalle()
        Dim resp As Integer
        Try 'se intenta hacer
            If Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").Count > 0 Then  ' si hay detalles

                resp = MessageBox.Show("�Desea eliminar este art�culo del Ajuste?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

                If resp = 6 Then
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").RemoveAt(Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").Position)
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").EndCurrentEdit()
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").EndCurrentEdit()
                Else
                    Me.BindingContext(Me.DataSet_Movimiento_Bodega, "MovimientosBodega.MovimientosBodegaMovimientosBodega_Detalle").CancelCurrentEdit()
                    TextBoxCodigo.Focus()
                    Me.TextBoxCodigo.SelectAll()
                End If
            Else
                MsgBox("No Existen Art�culos para eliminar del ajuste!!", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub Limpiar()
        DataSet_Movimiento_Bodega.MovimientosBodega_Detalle.Clear()
        DataSet_Movimiento_Bodega.MovimientosBodega.Clear()
        Panel2.Enabled = False
        Panel3.Enabled = False
        GridControl1.Enabled = False
        Me.ToolBar1.Buttons(0).Enabled = False
        Me.ToolBar1.Buttons(2).Enabled = False
        Me.ToolBar1.Buttons(3).Enabled = False
        Me.ToolBar1.Buttons(4).Enabled = False
    End Sub
#End Region

#Region "Validacion de Usuarios"
    Private Function ValidarUsuario()
        Dim Registros As SqlClient.SqlDataReader
        Dim Conexiones As New Conexion
        Registros = Conexiones.GetRecorset(Conexiones.Conectar, "SELECT Cedula, Nombre  from Usuarios where Clave_Interna ='" & txtUsuario.Text & "'")
        If Registros.HasRows = False Then
            MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
            txtUsuario.Text = ""
            txtUsuario.Focus()
            Return False
        End If
        Try
            While Registros.Read
                NuevaEntrada()
                Me.DataSet_Movimiento_Bodega.MovimientosBodega.UsuarioColumn.DefaultValue = Registros!cedula
                Me.txtCedulaUsuario.Text = Registros!cedula
                Me.txtNombreUsuario.Text = Registros!Nombre
                PMU = VSM(Usua.Cedula, Me.Name) 'Carga los privilegios del usuario con el modulo 
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Atenci�n...")
        End Try

        Return True
    End Function
#End Region

#Region "Lotes - Serie"

#Region "Controles Lotes"
    Private Sub ActivaLote()
        Me.LNumero.Visible = True
        Me.LNumero.Text = "Lote"
        Me.CbNumero.Visible = True
        CargarCbNumero()
        CBNuevo.Visible = RadioButtonEntrada.Checked
    End Sub

    Private Sub ActivaNinguno()
        Me.LNumero.Visible = False
        Me.CbNumero.Visible = False
        Me.CbNumero.Items.Clear()
        CBNuevo.Visible = False
    End Sub

    Private Sub NuevoLote(ByVal Estado As Boolean)
        LNuevoLote.Visible = Estado
        txtNuevoLote.Visible = Estado
        LVencimiento.Visible = Estado
        DTPVencimiento.Visible = Estado
        txtNuevoLote.Text = ""
        CbNumero.Visible = Not (Estado)
        If Estado = True Then
            txtNuevoLote.Focus()
        Else
            CbNumero.Focus()
        End If
    End Sub
#End Region

#Region "Funciones"
    Private Sub CargarCbNumero()
        Dim rss() As System.Data.DataRow
        Dim rs As System.Data.DataRow
        Dim i As Integer
        CbNumero.Items.Clear() ' limpia el combo

        Try
            If TextBoxCodigo.Text <> Nothing Then
                rss = Me.Lote.Lotes.Select("Cod_Articulo = " & CInt(Me.TextBoxCodigo.Text) & " AND Vencimiento >= '" & Now.Date & "'")

                If rss.Length <> 0 Then ' si existe lote con cantidad disponible
                    For i = 0 To rss.Length - 1
                        rs = rss(i)
                        CbNumero.Items.Add(rs("Numero"))
                    Next i

                    If Me.CbNumero.SelectedIndex = -1 Then
                        Me.CbNumero.SelectedIndex = 0
                    End If
                    CbNumero.Enabled = True

                Else
                    MsgBox("No hay lotes disponibles!" & vbCrLf & "Solo Puede Hacer entrada al Inventario", MsgBoxStyle.Information)
                    If RadioButtonEntrada.Checked = False Then
                        TextBoxCodigo.Focus()
                    Else
                        LNumero.Visible = False
                        CbNumero.Visible = False
                        CBNuevo.Visible = True
                        CBNuevo.Checked = True
                    End If
                End If
            Else
                MsgBox("Debe escribir el C�digo del Art�culo", MsgBoxStyle.Critical)
                TextBoxCodigo.Focus()
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub


    Private Sub CargarExistenciaLote()
        Dim rss() As System.Data.DataRow
        Dim rs As System.Data.DataRow

        Try
            If CbNumero.Text <> Nothing Then
                rss = Me.Lote.Lotes.Select("Cod_Articulo = " & CInt(Me.TextBoxCodigo.Text) & " And Numero = '" & CbNumero.Text & "'")

                If rss.Length <> 0 Then ' si existe lote con cantidad disponible
                    For i As Integer = 0 To rss.Length - 1
                        rs = rss(i)
                        txtExistencia.Text = rs("Cant_Actual")
                    Next
                Else
                    MsgBox("No hay " & LNumero.Text & "s disponibles para este art�culo", MsgBoxStyle.Critical)
                End If
            Else
                MsgBox("Debe Seleccionar un N�mero de " & LNumero.Text & " V�lido", MsgBoxStyle.Critical)
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub


    Private Sub AgregaLote()
        BindingContext(Lote, "Lotes").EndCurrentEdit()
        BindingContext(Lote, "Lotes").AddNew()
        BindingContext(Lote, "Lotes").Current("Numero") = txtNuevoLote.Text
        BindingContext(Lote, "Lotes").Current("Vencimiento") = DTPVencimiento.Value.Date
        BindingContext(Lote, "Lotes").Current("Cant_Inicial") = TextBoxCantidad.Text
        BindingContext(Lote, "Lotes").Current("Cant_Actual") = 0
        BindingContext(Lote, "Lotes").Current("Fecha_Entrada") = Now
        BindingContext(Lote, "Lotes").Current("Cod_Articulo") = TextBoxCodigo.Text
        BindingContext(Lote, "Lotes").Current("Documento") = 0
        BindingContext(Lote, "Lotes").Current("Tipo") = "AIB"
        BindingContext(Lote, "Lotes").EndCurrentEdit()
    End Sub


    Public Function ValidaLote() As Boolean
        Dim DrNum() As System.Data.DataRow
        Dim DrNumero As System.Data.DataRow
        ValidaLote = False

        Try
            If Me.Lote.Lotes.Count > 0 Then
                DrNum = Lote.Lotes.Select("Cod_Articulo = " & CInt(Me.TextBoxCodigo.Text) & "AND Numero = '" & txtNuevoLote.Text & "'")

                If DrNum.Length <> 0 Then 'Si existe
                    ValidaLote = True
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Function

    Function RegistraLote() As Boolean
        Dim TransLote As SqlTransaction
        If sqlConnectionLote.State <> sqlConnectionLote.State.Open Then sqlConnectionLote.Open()
        TransLote = sqlConnectionLote.BeginTransaction

        Try
            AdapterLotes.InsertCommand.Transaction = TransLote
            AdapterLotes.DeleteCommand.Transaction = TransLote
            AdapterLotes.UpdateCommand.Transaction = TransLote

            AdapterLotes.Update(Lote, "Lotes")

            Lote.AcceptChanges()
            TransLote.Commit()
            sqlConnectionLote.Close()
            Return True

        Catch ex As SqlException
            TransLote.Rollback()
            MsgBox(ex.Message)
            ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try
    End Function
#End Region

#Region "Objetos"
    Private Sub CBNumero_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CbNumero.KeyDown
        If e.KeyCode = Keys.Enter Then
            TextBoxCantidad.Focus()
        End If
    End Sub


    Private Sub CBNumero_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbNumero.SelectedIndexChanged
        If CbNumero.Items.Count > 0 Then
            CargarExistenciaLote()
        End If
    End Sub


    Private Sub CBNuevo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CBNuevo.CheckedChanged
        NuevoLote(CBNuevo.Checked)
    End Sub


    Private Sub txtNuevoLote_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNuevoLote.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Not (ValidaLote()) Then
                DTPVencimiento.Focus()
            Else
                MsgBox("El N�mero de " & LNumero.Text & " ya existe", MsgBoxStyle.Critical)
                txtNuevoLote.Focus()
            End If
        End If
    End Sub


    Private Sub DTPVencimiento_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DTPVencimiento.KeyDown
        If e.KeyCode = Keys.Enter Then
            TextBoxCantidad.Focus()
        End If
    End Sub
#End Region

#End Region

End Class
