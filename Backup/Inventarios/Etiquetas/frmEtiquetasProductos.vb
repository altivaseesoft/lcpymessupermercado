Imports System.data.SqlClient
Imports System.Data
Imports System.Windows.Forms
Public Class frmEtiquetasProductos
    Inherits System.Windows.Forms.Form
    Public tabla As New DataTable
    Public Automatico As Boolean
    Public Codigos(50) As Integer
    Public Cantidades(50) As Integer
    Public CodPro(50) As Integer
    Dim Etiquetas1 As New Etiquetas
    Dim Etiquetas2 As New EtiquetaSATOCX400_2C
    Dim usua As Usuario_Logeado


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal f As Boolean, ByVal s() As Integer, ByVal w() As Integer, ByVal z() As Integer)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        Automatico = f
        Codigos = s
        Cantidades = w
        CodPro = z
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtCodigoBarras As System.Windows.Forms.TextBox
    Friend WithEvents txtCodigoBarrrasCorrecto As System.Windows.Forms.TextBox
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarExcel As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents DataNavigator1 As DevExpress.XtraEditors.DataNavigator
    Friend WithEvents dgEtiquetas As System.Windows.Forms.DataGrid
    Friend WithEvents TextCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmdEtiquetar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdCorregir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Textcod_Pro As System.Windows.Forms.TextBox
    Friend WithEvents Combo_Proveedores As System.Windows.Forms.ComboBox
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents daArticulos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Adapter_ArticulosXProveedor As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsEtiquetasArticulos1 As DsEtiquetasArticulos
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Text_cod_Pro As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Public WithEvents rptViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents RadioButtonFX As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonCX As System.Windows.Forms.RadioButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEtiquetasProductos))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.RadioButtonCX = New System.Windows.Forms.RadioButton
        Me.RadioButtonFX = New System.Windows.Forms.RadioButton
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Combo_Proveedores = New System.Windows.Forms.ComboBox
        Me.DsEtiquetasArticulos1 = New DsEtiquetasArticulos
        Me.cmdCorregir = New DevExpress.XtraEditors.SimpleButton
        Me.cmdEtiquetar = New DevExpress.XtraEditors.SimpleButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.TextCantidad = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtCodigoBarras = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtCodigoBarrrasCorrecto = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDescripcion = New System.Windows.Forms.TextBox
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Text_cod_Pro = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Textcod_Pro = New System.Windows.Forms.TextBox
        Me.DataNavigator1 = New DevExpress.XtraEditors.DataNavigator
        Me.Label4 = New System.Windows.Forms.Label
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarExcel = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.dgEtiquetas = New System.Windows.Forms.DataGrid
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.daArticulos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.Adapter_ArticulosXProveedor = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.rptViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DsEtiquetasArticulos1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Combo_Proveedores)
        Me.GroupBox1.Controls.Add(Me.cmdCorregir)
        Me.GroupBox1.Controls.Add(Me.cmdEtiquetar)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.TextCantidad)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtCodigoBarras)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtCodigoBarrrasCorrecto)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtDescripcion)
        Me.GroupBox1.Controls.Add(Me.txtCodigo)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Text_cod_Pro)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox1.Location = New System.Drawing.Point(7, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 176)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Art�culos"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RadioButtonCX)
        Me.Panel1.Controls.Add(Me.RadioButtonFX)
        Me.Panel1.Location = New System.Drawing.Point(328, 56)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(120, 48)
        Me.Panel1.TabIndex = 138
        '
        'RadioButtonCX
        '
        Me.RadioButtonCX.Checked = True
        Me.RadioButtonCX.Location = New System.Drawing.Point(8, 28)
        Me.RadioButtonCX.Name = "RadioButtonCX"
        Me.RadioButtonCX.Size = New System.Drawing.Size(96, 16)
        Me.RadioButtonCX.TabIndex = 1
        Me.RadioButtonCX.TabStop = True
        Me.RadioButtonCX.Text = "Estilo CX400"
        '
        'RadioButtonFX
        '
        Me.RadioButtonFX.Location = New System.Drawing.Point(8, 8)
        Me.RadioButtonFX.Name = "RadioButtonFX"
        Me.RadioButtonFX.Size = New System.Drawing.Size(104, 16)
        Me.RadioButtonFX.TabIndex = 0
        Me.RadioButtonFX.Text = "Estilo FX Serie"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.Location = New System.Drawing.Point(384, 136)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 16)
        Me.Label8.TabIndex = 72
        Me.Label8.Text = "Cod"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Location = New System.Drawing.Point(8, 136)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(360, 16)
        Me.Label7.TabIndex = 71
        Me.Label7.Text = "Proveedores"
        '
        'Combo_Proveedores
        '
        Me.Combo_Proveedores.DataSource = Me.DsEtiquetasArticulos1
        Me.Combo_Proveedores.DisplayMember = "Inventario.InventarioArticulos_x0020_x_x0020_Proveedor.Nombre"
        Me.Combo_Proveedores.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_Proveedores.Location = New System.Drawing.Point(8, 152)
        Me.Combo_Proveedores.Name = "Combo_Proveedores"
        Me.Combo_Proveedores.Size = New System.Drawing.Size(360, 21)
        Me.Combo_Proveedores.TabIndex = 70
        '
        'DsEtiquetasArticulos1
        '
        Me.DsEtiquetasArticulos1.DataSetName = "DsEtiquetasArticulos"
        Me.DsEtiquetasArticulos1.Locale = New System.Globalization.CultureInfo("es-MX")
        '
        'cmdCorregir
        '
        Me.cmdCorregir.Location = New System.Drawing.Point(249, 106)
        Me.cmdCorregir.Name = "cmdCorregir"
        Me.cmdCorregir.Size = New System.Drawing.Size(64, 24)
        Me.cmdCorregir.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        Me.cmdCorregir.TabIndex = 69
        Me.cmdCorregir.Text = "Corregir"
        '
        'cmdEtiquetar
        '
        Me.cmdEtiquetar.Location = New System.Drawing.Point(248, 72)
        Me.cmdEtiquetar.Name = "cmdEtiquetar"
        Me.cmdEtiquetar.Size = New System.Drawing.Size(64, 24)
        Me.cmdEtiquetar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
        Me.cmdEtiquetar.TabIndex = 68
        Me.cmdEtiquetar.Text = "Etiquetar"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(195, 103)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 16)
        Me.Label5.TabIndex = 67
        Me.Label5.Text = "Cant."
        '
        'TextCantidad
        '
        Me.TextCantidad.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextCantidad.Location = New System.Drawing.Point(195, 119)
        Me.TextCantidad.Name = "TextCantidad"
        Me.TextCantidad.Size = New System.Drawing.Size(48, 13)
        Me.TextCantidad.TabIndex = 66
        Me.TextCantidad.Text = ""
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(8, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(176, 16)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "C�digo de barras"
        '
        'txtCodigoBarras
        '
        Me.txtCodigoBarras.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCodigoBarras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsEtiquetasArticulos1, "Inventario.Barras"))
        Me.txtCodigoBarras.Enabled = False
        Me.txtCodigoBarras.Location = New System.Drawing.Point(8, 80)
        Me.txtCodigoBarras.Name = "txtCodigoBarras"
        Me.txtCodigoBarras.Size = New System.Drawing.Size(176, 13)
        Me.txtCodigoBarras.TabIndex = 61
        Me.txtCodigoBarras.Text = ""
        Me.txtCodigoBarras.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(8, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(176, 16)
        Me.Label3.TabIndex = 60
        Me.Label3.Text = "C�digo de barras correcto"
        '
        'txtCodigoBarrrasCorrecto
        '
        Me.txtCodigoBarrrasCorrecto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCodigoBarrrasCorrecto.Location = New System.Drawing.Point(8, 120)
        Me.txtCodigoBarrrasCorrecto.Name = "txtCodigoBarrrasCorrecto"
        Me.txtCodigoBarrrasCorrecto.Size = New System.Drawing.Size(176, 13)
        Me.txtCodigoBarrrasCorrecto.TabIndex = 59
        Me.txtCodigoBarrrasCorrecto.Text = ""
        Me.txtCodigoBarrrasCorrecto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(105, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(336, 16)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Descripci�n"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsEtiquetasArticulos1, "Inventario.Descripcion"))
        Me.txtDescripcion.Enabled = False
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.ForeColor = System.Drawing.Color.Blue
        Me.txtDescripcion.Location = New System.Drawing.Point(105, 40)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(336, 15)
        Me.txtDescripcion.TabIndex = 55
        Me.txtDescripcion.Text = ""
        '
        'txtCodigo
        '
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsEtiquetasArticulos1, "Inventario.Codigo"))
        Me.txtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigo.ForeColor = System.Drawing.Color.Blue
        Me.txtCodigo.Location = New System.Drawing.Point(8, 40)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(88, 15)
        Me.txtCodigo.TabIndex = 53
        Me.txtCodigo.Text = ""
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.Location = New System.Drawing.Point(8, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 16)
        Me.Label13.TabIndex = 54
        Me.Label13.Text = "C�digo"
        '
        'Text_cod_Pro
        '
        Me.Text_cod_Pro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Text_cod_Pro.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsEtiquetasArticulos1, "Inventario.InventarioArticulos_x0020_x_x0020_Proveedor.CodigoProveedor"))
        Me.Text_cod_Pro.Location = New System.Drawing.Point(384, 152)
        Me.Text_cod_Pro.Name = "Text_cod_Pro"
        Me.Text_cod_Pro.ReadOnly = True
        Me.Text_cod_Pro.Size = New System.Drawing.Size(48, 20)
        Me.Text_cod_Pro.TabIndex = 137
        Me.Text_cod_Pro.Text = ""
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(422, 176)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 16)
        Me.Label6.TabIndex = 71
        Me.Label6.Text = "Pro"
        '
        'Textcod_Pro
        '
        Me.Textcod_Pro.Location = New System.Drawing.Point(422, 192)
        Me.Textcod_Pro.Name = "Textcod_Pro"
        Me.Textcod_Pro.Size = New System.Drawing.Size(48, 20)
        Me.Textcod_Pro.TabIndex = 70
        Me.Textcod_Pro.Text = ""
        '
        'DataNavigator1
        '
        Me.DataNavigator1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.DataNavigator1.Buttons.Append.Visible = False
        Me.DataNavigator1.Buttons.CancelEdit.Visible = False
        Me.DataNavigator1.Buttons.EndEdit.Visible = False
        Me.DataNavigator1.Buttons.Remove.Visible = False
        Me.DataNavigator1.DataMember = "Inventario"
        Me.DataNavigator1.DataSource = Me.DsEtiquetasArticulos1
        Me.DataNavigator1.Location = New System.Drawing.Point(307, 429)
        Me.DataNavigator1.Name = "DataNavigator1"
        Me.DataNavigator1.TabIndex = 65
        Me.DataNavigator1.Text = "DataNavigator1"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Image = CType(resources.GetObject("Label4.Image"), System.Drawing.Image)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(0, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(768, 32)
        Me.Label4.TabIndex = 104
        Me.Label4.Text = "Etiquetas de Art�culos"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.AutoSize = False
        Me.ToolBar1.BackColor = System.Drawing.Color.LightGray
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarExcel, Me.ToolBarCerrar})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 402)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(768, 52)
        Me.ToolBar1.TabIndex = 135
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Text = "Nuevo"
        Me.ToolBarNuevo.Visible = False
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.ImageIndex = 1
        Me.ToolBarBuscar.Text = "Buscar"
        Me.ToolBarBuscar.Visible = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Text = "Registrar"
        Me.ToolBarRegistrar.Visible = False
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.ImageIndex = 3
        Me.ToolBarEliminar.Text = "Eliminar"
        Me.ToolBarEliminar.Visible = False
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.ImageIndex = 7
        Me.ToolBarImprimir.Text = "Imprimir"
        '
        'ToolBarExcel
        '
        Me.ToolBarExcel.Enabled = False
        Me.ToolBarExcel.ImageIndex = 5
        Me.ToolBarExcel.Text = "Exportar"
        Me.ToolBarExcel.Visible = False
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'dgEtiquetas
        '
        Me.dgEtiquetas.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgEtiquetas.DataMember = ""
        Me.dgEtiquetas.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgEtiquetas.Location = New System.Drawing.Point(8, 224)
        Me.dgEtiquetas.Name = "dgEtiquetas"
        Me.dgEtiquetas.Size = New System.Drawing.Size(464, 168)
        Me.dgEtiquetas.TabIndex = 136
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=HAZEL;packet size=4096;integrated security=SSPI;data source=SEESER" & _
        "VER;persist security info=False;initial catalog=Seepos"
        '
        'daArticulos
        '
        Me.daArticulos.SelectCommand = Me.SqlSelectCommand1
        Me.daArticulos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Inventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Barras", "Barras"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Inhabilitado", "Inhabilitado"), New System.Data.Common.DataColumnMapping("Servicio", "Servicio")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Codigo, Barras, Descripcion, Inhabilitado, Servicio FROM Inventario WHERE " & _
        "(Inhabilitado = 0) AND (Servicio = 0)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'Adapter_ArticulosXProveedor
        '
        Me.Adapter_ArticulosXProveedor.SelectCommand = Me.SqlSelectCommand2
        Me.Adapter_ArticulosXProveedor.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Articulos x Proveedor", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoArticulo", "CodigoArticulo"), New System.Data.Common.DataColumnMapping("CodigoProveedor", "CodigoProveedor"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT [Articulos x Proveedor].CodigoArticulo, [Articulos x Proveedor].CodigoProv" & _
        "eedor, Proveedores.Nombre FROM [Articulos x Proveedor] INNER JOIN Proveedores ON" & _
        " [Articulos x Proveedor].CodigoProveedor = Proveedores.CodigoProv INNER JOIN Inv" & _
        "entario ON [Articulos x Proveedor].CodigoArticulo = Inventario.Codigo WHERE (Inv" & _
        "entario.Servicio = 0) AND (Inventario.Inhabilitado = 0)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'rptViewer
        '
        Me.rptViewer.ActiveViewIndex = -1
        Me.rptViewer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rptViewer.DisplayBackgroundEdge = False
        Me.rptViewer.DisplayGroupTree = False
        Me.rptViewer.DisplayToolbar = False
        Me.rptViewer.Location = New System.Drawing.Point(480, 40)
        Me.rptViewer.Name = "rptViewer"
        Me.rptViewer.ReportSource = Nothing
        Me.rptViewer.ShowCloseButton = False
        Me.rptViewer.ShowExportButton = False
        Me.rptViewer.ShowGotoPageButton = False
        Me.rptViewer.ShowGroupTreeButton = False
        Me.rptViewer.ShowPageNavigateButtons = False
        Me.rptViewer.ShowPrintButton = False
        Me.rptViewer.ShowRefreshButton = False
        Me.rptViewer.ShowTextSearchButton = False
        Me.rptViewer.ShowZoomButton = False
        Me.rptViewer.Size = New System.Drawing.Size(288, 408)
        Me.rptViewer.TabIndex = 137
        '
        'frmEtiquetasProductos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(768, 454)
        Me.Controls.Add(Me.rptViewer)
        Me.Controls.Add(Me.DataNavigator1)
        Me.Controls.Add(Me.dgEtiquetas)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Textcod_Pro)
        Me.Name = "frmEtiquetasProductos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Etiquetas de Art�culos"
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.DsEtiquetasArticulos1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "variables"
    Private cConexion As New Conexion
    Private sqlConexion As SqlConnection
#End Region

    
    Private Sub cargarinformacion(ByVal codigo As String)
        Try
            Dim articulos() As System.Data.DataRow 'almacena temporalmente los datos de un art�culo
            Dim articulo As System.Data.DataRow 'almacena temporalmente los datos de un art�culo
            Dim pos As Integer
            Dim vista As DataView

            articulos = Me.DsEtiquetasArticulos1.Inventario.Select("codigo = " & codigo & "And Inhabilitado = 0")

            If articulos.Length <> 0 Then

                vista = Me.DsEtiquetasArticulos1.Inventario.DefaultView
                vista.Sort = "Codigo"
                pos = vista.Find(CDbl(codigo))
                Me.BindingContext(Me.DsEtiquetasArticulos1, "Inventario").CancelCurrentEdit()
                Me.BindingContext(Me.DsEtiquetasArticulos1, "Inventario").Position = pos


            Else
                MsgBox("No existe un art�culo con ese c�digo en el inventario", MsgBoxStyle.Information)
                Me.BindingContext(Me.DsEtiquetasArticulos1, "Inventario").CancelCurrentEdit()


            End If

        Catch ex As SystemException
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub frmEtiquetasProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            CrystalReportsConexion.LoadReportViewer(Nothing, Etiquetas1, True)
            CrystalReportsConexion.LoadReportViewer(Nothing, Etiquetas2, True)
            cConexion = New Conexion
            sqlConexion = cConexion.Conectar
            Me.daArticulos.Fill(Me.DsEtiquetasArticulos1.Inventario)
            Me.Adapter_ArticulosXProveedor.Fill(Me.DsEtiquetasArticulos1.Articulos_x_Proveedor)
            crear_tabla()

            If Me.Automatico Then
                Etiquetar_Factura()
            End If

        Catch ex As SyntaxErrorException  'cacha los errores
            MsgBox(ex.Message)
        End Try
    End Sub

    Function Etiquetar_Factura()
        Dim i As Integer

        For i = 0 To 50
            If Codigos(i) = 0 Then Exit For
            Me.txtCodigo.Text = Codigos(i)
            Me.cargarinformacion(Me.txtCodigo.Text)
            Me.Textcod_Pro.Text = Me.CodPro(i)
            Me.TextCantidad.Text = Cantidades(i)
            Me.insertar()
        Next i

    End Function




    Private Sub txtCodProveedor_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown

        Select Case e.KeyCode
            Case Keys.F1 : Me.Buscar_articulo() 'consultar()

            Case Keys.Enter
                Me.cargarinformacion(Me.txtCodigo.Text)
        End Select

    End Sub

    Sub Buscar_articulo()
        Try
            Dim Fx As New cFunciones
            Dim valor As String
            Dim pos As Integer
            Dim vista As DataView

            Dim BuscarArticulo As New FrmBuscarArticulo
            BuscarArticulo.ShowDialog()
            If BuscarArticulo.Cancelado Then
                Me.txtCodigo.Focus()
                Exit Sub
            Else
                valor = BuscarArticulo.Codigo
            End If

            vista = Me.DsEtiquetasArticulos1.Inventario.DefaultView
            vista.Sort = "Codigo"
            pos = vista.Find(CDbl(valor))
            Me.BindingContext(Me.DsEtiquetasArticulos1, "Inventario").Position = pos


        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub crear_tabla()
        Try
            Dim dtCodigo, dtDescripcion, dtCodProveedor, dtCantidad As System.Data.DataColumn

            dtCodigo = New System.Data.DataColumn("Codigo")
            dtDescripcion = New System.Data.DataColumn("Descripcion")
            dtCodProveedor = New System.Data.DataColumn("Proveedor")
            dtCantidad = New System.Data.DataColumn("Cantidad")
            tabla.Columns.Add(dtCodigo)
            tabla.Columns.Add(dtDescripcion)
            tabla.Columns.Add(dtCodProveedor)
            tabla.Columns.Add(dtCantidad)

            tabla.Columns(0).ReadOnly = True
            tabla.Columns(1).ReadOnly = True
            tabla.Columns(2).ReadOnly = True
            tabla.Columns(3).ReadOnly = False
            'tabla.Columns(2).DataType = System.Int32

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub insertar()
        Dim FilaEtiquetas As DataRow
        Dim cod_prov As Integer

        Try

            If Me.TextCantidad.Text = "" Then
                MsgBox("Debe digitar una cantidad", MsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(Me.TextCantidad.Text) < 1 Then
                MsgBox("Cantidad a etiquetar incorrecta", MsgBoxStyle.Information)
                Exit Sub
            End If

            FilaEtiquetas = tabla.NewRow



            If Not Me.Automatico Then Me.Textcod_Pro.Text = Text_cod_Pro.Text

            If Me.Textcod_Pro.Text = "" Then
                Try
                    cod_prov = CInt(InputBox("C�digo del Proveedor", ""))
                    Me.Textcod_Pro.Text = cod_prov

                Catch ex As SystemException
                    MsgBox("Digite un C�digo de Proveedor V�lido", MsgBoxStyle.Exclamation)
                    Me.Textcod_Pro.Text = ""
                    Exit Sub
                End Try

            End If

            FilaEtiquetas!Codigo = Me.txtCodigo.Text
            FilaEtiquetas!Descripcion = Me.txtDescripcion.Text
            FilaEtiquetas!Proveedor = Me.Textcod_Pro.Text
            FilaEtiquetas!Cantidad = CInt(Me.TextCantidad.Text)

            tabla.Rows.Add(FilaEtiquetas)

            Me.dgEtiquetas.DataSource = Nothing
            Me.dgEtiquetas.DataSource = tabla

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Imprimir(ByVal Etiquetas As CrystalDecisions.CrystalReports.Engine.ReportDocument)

        Dim i As Integer
        Dim codigo As Integer
        Dim fila As DataRow
        Dim cant As Integer
        Dim Cod_Pro As Integer
        Try
            For i = 0 To (tabla.Rows.Count - 1)
                fila = tabla.NewRow
                fila!codigo = tabla.Rows.Item(i)(0)
                codigo = CInt(fila!codigo)
                fila!Cantidad = tabla.Rows.Item(i)(3)
                cant = CInt(fila!Cantidad)
                fila!Proveedor = tabla.Rows.Item(i)(2)
                Cod_Pro = CInt(fila!Proveedor)

                Etiquetas.SetParameterValue(0, codigo)
                Etiquetas.SetParameterValue(1, Cod_Pro)

                Me.rptViewer.ReportSource = Etiquetas
                Me.rptViewer.PrintReport()

                'Etiquetas.PrintToPrinter(cant, True, 1, 1)
            Next i
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub


    'Private Sub eliminar_etiqueta()
    'Dim resp As String
    'Dim posicion As Integer

    'Try 'se intenta hacer
    '        If Me.BindingContext(Me.DsEtiquetasArticulos1, "inventario").Count > 0 Then ' si hay articulos

    'resp = MessageBox.Show("�Desea eliminar excluir este art�culo para impresi�n de etiquetas?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

    'If resp = 6 Then
    '    Try
    '        posicion = (Me.BindingContext(Me.DsEtiquetasArticulos1, "invetario").Position) + 1

    '        Me.BindingContext(Me.DsEtiquetasArticulos1, "inventario").RemoveAt(Me.BindingContext(Me.DsEtiquetasArticulos1, "inventario").Position)

    '        Me.BindingContext(Me.DsEtiquetasArticulos1.Inventario).RemoveAt(Me.BindingContext(Me.DsEtiquetasArticulos1.Inventario).Position = posicion)

    '        Me.daArticulos.Update(Me.DsEtiquetasArticulos1.Inventario)

    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '                End Try
    '            End If
    '        End If

    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 5

                If RadioButtonFX.Checked = True Then
                    Imprimir(Me.Etiquetas1)
                Else
                    Imprimir(Me.Etiquetas2)
                End If
        

            Case 7
                Me.Close()
        End Select
    End Sub

    'Private Sub dgEtiquetas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgEtiquetas.KeyDown
    'If e.KeyCode = Keys.Delete Then
    'If dgEtiquetas.CurrentRowIndex <> -1 Then
    'eliminar_etiqueta()
    'End If
    'End If
    'End Sub

    'Private Sub dgEtiquetas_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgEtiquetas.KeyUp
    'If e.KeyCode = Keys.Delete Then
    'If dgEtiquetas.CurrentRowIndex <> -1 Then
    'eliminar_etiqueta()
    'End If
    'End If
    'End Sub

    'Private Sub dgEtiquetas_Navigate(ByVal sender As System.Object, ByVal ne As System.Windows.Forms.NavigateEventArgs) Handles dgEtiquetas.Navigate
    'If dgEtiquetas.CurrentRowIndex = -1 Then Exit Sub
    'dgEtiquetas.Select(dgEtiquetas.CurrentRowIndex)
    'End Sub

    'Private Sub dgEtiquetas_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEtiquetas.CurrentCellChanged
    'If dgEtiquetas.CurrentRowIndex = -1 Then Exit Sub
    'dgEtiquetas.Select(dgEtiquetas.CurrentRowIndex)
    'End Sub

    'Private Sub dgEtiquetas_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgEtiquetas.KeyPress
    'eliminar_etiqueta()
    'End Sub

    Private Sub txtCodigoBarras_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoBarras.TextChanged
        If Me.txtCodigoBarras.Text <> "" Then
            Me.cmdEtiquetar.Enabled = True
        Else
            Me.cmdEtiquetar.Enabled = False
        End If
    End Sub


    Private Sub TextCantidad_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextCantidad.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub txtCodigo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub cmdEtiquetar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEtiquetar.Click
        Dim resp As String

        resp = MessageBox.Show("�Desea etiquetar este art�culo?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

        If resp = 6 Then
            insertar()
        End If
        Me.TextCantidad.Text = ""
    End Sub

    Private Sub cmdCorregir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCorregir.Click
        Dim resp As String
        Dim rs As SqlDataReader
        Dim cod_barras As String

        resp = MessageBox.Show("�Desea corregir el c�digo de barras de este art�culo?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

        If resp = 6 Then
            cod_barras = Me.txtCodigoBarrrasCorrecto.Text
            rs = cConexion.GetRecorset(cConexion.sQlconexion, "SELECT * FROM Inventario WHERE Barras = '" & cod_barras & "'")
            If rs.Read Then
                If rs!codigo = Me.txtCodigo.Text Then

                    MessageBox.Show("Este art�culo ya posee el c�digo de barras asignado", "", MessageBoxButtons.OK)

                    resp = MessageBox.Show("�Desea etiquetar este art�culo?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

                    If resp = 6 Then
                        insertar()
                    End If
                Else
                    MsgBox("El C�digo de barras ya existe", MsgBoxStyle.Exclamation)
                End If
            Else
                Try
                    Me.BindingContext(Me.DsEtiquetasArticulos1, "Inventario").EndCurrentEdit()
                    Me.daArticulos.Update(Me.DsEtiquetasArticulos1, "Inventario")

                    MsgBox("Actualizaci�n de art�culo exitosa", MsgBoxStyle.Information)

                    resp = MessageBox.Show("�Desea etiquetar este art�culo?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

                    If resp = 6 Then
                        insertar()
                    End If

                Catch eEndEdit As System.Data.NoNullAllowedException
                    System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                End Try
                rs.Close()
            End If
        End If
    End Sub

    Private Sub txtCodigo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.TextChanged

    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub daArticulos_RowUpdated(ByVal sender As System.Object, ByVal e As System.Data.SqlClient.SqlRowUpdatedEventArgs)

    End Sub
End Class
