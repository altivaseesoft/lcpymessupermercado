Imports DevExpress.Utils
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Data
Imports System.Windows.Forms

Public Class FrmInventario
    Inherits System.Windows.Forms.Form
    Dim usua As Usuario_Logeado
    Dim PMU As New PerfilModulo_Class
    Dim strNuevo As String = ""

#Region " Windows Form Designer generated code "
    Dim dlg As WaitDialogForm
    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        dlg = New WaitDialogForm("Cargando Componentes ...")
        dlg.Text = ""
        dlg.Caption = "Inicializando M�dulo...."
        InitializeComponent()
        dlg.Caption = "M�dulo Cargado...."
        'Add any initialization after the InitializeComponent() call
        PictureEdit1.DataBindings.Add(New Binding("EditValue", Me.DataSetInventario, "Inventario.Imagen"))
        dlg.Close()
        'AddHandler Me.BindingContext(Me.DataSetInventario, "Inventario2").PositionChanged, AddressOf Me.Position_Changed
    End Sub

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        AddHandler Me.BindingContext(Me.DataSetInventario, "Inventario2").PositionChanged, AddressOf Me.Position_Changed
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
        'Asigna la imagen al imagenbox
        PictureEdit1.DataBindings.Add(New Binding("EditValue", Me.DataSetInventario, "Inventario.Imagen"))

    End Sub


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents AdapterAUbicacion As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterCasaConsignante As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterFamilia As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterMarcas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterPresentacion As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ComboBox_Ubicacion_SubUbicacion As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxBodegas As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxFamilia As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxMarca As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxPresentacion As System.Windows.Forms.ComboBox
    Friend WithEvents DataNavigator1 As DevExpress.XtraEditors.DataNavigator

    Friend WithEvents ErrorProvider As System.Windows.Forms.ErrorProvider
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TxtBarras As System.Windows.Forms.TextBox

    Friend WithEvents TxtCodigo As System.Windows.Forms.Label

    Friend WithEvents TxtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents TxtExistencia As System.Windows.Forms.TextBox
    Friend WithEvents TxtMax As System.Windows.Forms.TextBox
    Friend WithEvents TxtMed As System.Windows.Forms.TextBox
    Friend WithEvents TxtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents TxtPrecioVenta_A As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_B As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_C As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_D As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_A As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_B As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_C As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_D As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUtilidad_A As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUtilidad_B As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUtilidad_C As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUtilidad_D As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSetInventario As DataSetInventario
    Friend WithEvents AdapterInventario As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents TxtCanPresentacion As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxMonedaVenta As System.Windows.Forms.ComboBox
    Friend WithEvents AdapterArticulosXproveedor As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents colFechaUltimaCompra As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colCodigoProveedor As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colUltimoCosto As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents TextBoxValorMonedaEnVenta As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TxtUtilidad_P As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_IV_P As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPrecioVenta_P As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarEliminar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents Adapter_cod_Inventario As System.Data.SqlClient.SqlDataAdapter

    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Adaptador_Inventraio_AUX As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Text_Cod_AUX As System.Windows.Forms.TextBox
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents TxtMin As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterBitacora As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ToolBarEliminador As System.Windows.Forms.ToolBarButton
    Friend WithEvents lblCodigoProveedor As System.Windows.Forms.Label
    Friend WithEvents daProveedores As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents cmboxProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents ckVerCodBarraInv As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtPorSugerido As System.Windows.Forms.TextBox
    Friend WithEvents txtVentaSugerido As System.Windows.Forms.TextBox
    Friend WithEvents txtSugeridoIV As System.Windows.Forms.TextBox
    Friend WithEvents Ck_Consignacion As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents AdapterLote As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtExistenciaBodega As ValidText.ValidText
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TxtCostoEquivalente As System.Windows.Forms.TextBox
    Friend WithEvents TxtOtrosCargosEquivalente As System.Windows.Forms.TextBox
    Friend WithEvents TxtFleteEquivalente As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxValorMonedaEnCosto As System.Windows.Forms.TextBox
    Friend WithEvents TxtBaseEquivalente As System.Windows.Forms.TextBox
    Friend WithEvents TxtImpuesto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TxtFlete As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtOtros As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtCosto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboBoxMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TxtBase As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Ck_Lote As System.Windows.Forms.CheckBox
    Friend WithEvents Ck_PreguntaPrecio As System.Windows.Forms.CheckBox
    Friend WithEvents Check_Inhabilitado As System.Windows.Forms.CheckBox
    Friend WithEvents Check_Servicio As System.Windows.Forms.CheckBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Hasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Check_Promo As System.Windows.Forms.CheckBox
    Friend WithEvents Desde As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents TxtMaxDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TabLotes As System.Windows.Forms.TabPage
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCodigoProveedor1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLote As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCant_Actual As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ButtonAgreBodega As System.Windows.Forms.Button
    Friend WithEvents ComboBoxBodega As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonAgregar As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminar As System.Windows.Forms.Button
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents chpeso As System.Windows.Forms.CheckBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmInventario))
        Me.TxtCodigo = New System.Windows.Forms.Label
        Me.DataSetInventario = New LcPymes_5._2.DataSetInventario
        Me.TxtBarras = New System.Windows.Forms.TextBox
        Me.TxtDescripcion = New System.Windows.Forms.TextBox
        Me.TxtMed = New System.Windows.Forms.TextBox
        Me.TxtMax = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.ComboBoxPresentacion = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.TxtObservaciones = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.TxtPrecioVenta_A = New DevExpress.XtraEditors.TextEdit
        Me.TxtPrecioVenta_B = New DevExpress.XtraEditors.TextEdit
        Me.TxtPrecioVenta_C = New DevExpress.XtraEditors.TextEdit
        Me.Label22 = New System.Windows.Forms.Label
        Me.TxtPrecioVenta_D = New DevExpress.XtraEditors.TextEdit
        Me.Label23 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.TextBoxValorMonedaEnVenta = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.TxtUtilidad_D = New DevExpress.XtraEditors.TextEdit
        Me.TxtUtilidad_C = New DevExpress.XtraEditors.TextEdit
        Me.TxtUtilidad_B = New DevExpress.XtraEditors.TextEdit
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.TxtPrecioVenta_IV_D = New DevExpress.XtraEditors.TextEdit
        Me.TxtPrecioVenta_IV_A = New DevExpress.XtraEditors.TextEdit
        Me.TxtPrecioVenta_IV_C = New DevExpress.XtraEditors.TextEdit
        Me.TxtPrecioVenta_IV_B = New DevExpress.XtraEditors.TextEdit
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.TxtUtilidad_A = New DevExpress.XtraEditors.TextEdit
        Me.Label32 = New System.Windows.Forms.Label
        Me.ComboBoxMonedaVenta = New System.Windows.Forms.ComboBox
        Me.TxtUtilidad_P = New DevExpress.XtraEditors.TextEdit
        Me.TxtPrecioVenta_IV_P = New DevExpress.XtraEditors.TextEdit
        Me.Label35 = New System.Windows.Forms.Label
        Me.TxtPrecioVenta_P = New DevExpress.XtraEditors.TextEdit
        Me.Label36 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.TxtExistencia = New System.Windows.Forms.TextBox
        Me.TxtMin = New System.Windows.Forms.TextBox
        Me.ComboBoxBodegas = New System.Windows.Forms.ComboBox
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarEliminador = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.ComboBoxMarca = New System.Windows.Forms.ComboBox
        Me.AdapterMarcas = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.ComboBoxFamilia = New System.Windows.Forms.ComboBox
        Me.ComboBox_Ubicacion_SubUbicacion = New System.Windows.Forms.ComboBox
        Me.AdapterFamilia = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.AdapterPresentacion = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.AdapterCasaConsignante = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.DataNavigator1 = New DevExpress.XtraEditors.DataNavigator
        Me.AdapterAUbicacion = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.AdapterInventario = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.TxtCanPresentacion = New System.Windows.Forms.TextBox
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.AdapterArticulosXproveedor = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.colFechaUltimaCompra = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colCodigoProveedor = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colUltimoCosto = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.Text_Cod_AUX = New System.Windows.Forms.TextBox
        Me.Adaptador_Inventraio_AUX = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.Ck_Consignacion = New System.Windows.Forms.CheckBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.cmboxProveedor = New System.Windows.Forms.ComboBox
        Me.Label41 = New System.Windows.Forms.Label
        Me.lblCodigoProveedor = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.AdapterBitacora = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.daProveedores = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand12 = New System.Data.SqlClient.SqlCommand
        Me.ckVerCodBarraInv = New System.Windows.Forms.CheckBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.txtPorSugerido = New System.Windows.Forms.TextBox
        Me.Label43 = New System.Windows.Forms.Label
        Me.txtVentaSugerido = New System.Windows.Forms.TextBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.txtSugeridoIV = New System.Windows.Forms.TextBox
        Me.Label45 = New System.Windows.Forms.Label
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.AdapterLote = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand13 = New System.Data.SqlClient.SqlCommand
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TxtCostoEquivalente = New System.Windows.Forms.TextBox
        Me.TxtOtrosCargosEquivalente = New System.Windows.Forms.TextBox
        Me.TxtFleteEquivalente = New System.Windows.Forms.TextBox
        Me.TextBoxValorMonedaEnCosto = New System.Windows.Forms.TextBox
        Me.TxtBaseEquivalente = New System.Windows.Forms.TextBox
        Me.TxtImpuesto = New DevExpress.XtraEditors.TextEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.TxtFlete = New DevExpress.XtraEditors.TextEdit
        Me.TxtOtros = New DevExpress.XtraEditors.TextEdit
        Me.TxtCosto = New DevExpress.XtraEditors.TextEdit
        Me.ComboBoxMoneda = New System.Windows.Forms.ComboBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.TxtBase = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.Ck_Lote = New System.Windows.Forms.CheckBox
        Me.Ck_PreguntaPrecio = New System.Windows.Forms.CheckBox
        Me.Check_Inhabilitado = New System.Windows.Forms.CheckBox
        Me.Check_Servicio = New System.Windows.Forms.CheckBox
        Me.Label34 = New System.Windows.Forms.Label
        Me.Hasta = New System.Windows.Forms.DateTimePicker
        Me.Label33 = New System.Windows.Forms.Label
        Me.Check_Promo = New System.Windows.Forms.CheckBox
        Me.Desde = New System.Windows.Forms.DateTimePicker
        Me.Label37 = New System.Windows.Forms.Label
        Me.TxtMaxDesc = New System.Windows.Forms.TextBox
        Me.Label38 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Label40 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TabLotes = New System.Windows.Forms.TabPage
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.ButtonAgreBodega = New System.Windows.Forms.Button
        Me.ComboBoxBodega = New System.Windows.Forms.ComboBox
        Me.ButtonAgregar = New System.Windows.Forms.Button
        Me.ButtonEliminar = New System.Windows.Forms.Button
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCodigoProveedor1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLote = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCant_Actual = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chpeso = New System.Windows.Forms.CheckBox
        CType(Me.DataSetInventario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_A.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_B.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_C.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_D.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.TxtUtilidad_D.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUtilidad_C.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUtilidad_B.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_D.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_A.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_C.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_B.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUtilidad_A.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUtilidad_P.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_IV_P.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPrecioVenta_P.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.TxtImpuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFlete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtOtros.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtBase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabLotes.SuspendLayout()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TxtCodigo
        '
        Me.TxtCodigo.BackColor = System.Drawing.Color.White
        Me.TxtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Codigo"))
        Me.TxtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCodigo.ForeColor = System.Drawing.Color.Red
        Me.TxtCodigo.Location = New System.Drawing.Point(73, 16)
        Me.TxtCodigo.Name = "TxtCodigo"
        Me.TxtCodigo.Size = New System.Drawing.Size(89, 13)
        Me.TxtCodigo.TabIndex = 2
        Me.TxtCodigo.Text = "0"
        Me.TxtCodigo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DataSetInventario
        '
        Me.DataSetInventario.DataSetName = "DataSetInventario"
        Me.DataSetInventario.Locale = New System.Globalization.CultureInfo("es")
        '
        'TxtBarras
        '
        Me.TxtBarras.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtBarras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtBarras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Barras"))
        Me.TxtBarras.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBarras.ForeColor = System.Drawing.Color.Blue
        Me.TxtBarras.Location = New System.Drawing.Point(5, 24)
        Me.TxtBarras.Name = "TxtBarras"
        Me.TxtBarras.Size = New System.Drawing.Size(79, 13)
        Me.TxtBarras.TabIndex = 1
        Me.TxtBarras.Text = ""
        '
        'TxtDescripcion
        '
        Me.TxtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtDescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Descripcion"))
        Me.TxtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDescripcion.ForeColor = System.Drawing.Color.Blue
        Me.TxtDescripcion.Location = New System.Drawing.Point(92, 24)
        Me.TxtDescripcion.Name = "TxtDescripcion"
        Me.TxtDescripcion.Size = New System.Drawing.Size(408, 13)
        Me.TxtDescripcion.TabIndex = 3
        Me.TxtDescripcion.Text = ""
        '
        'TxtMed
        '
        Me.TxtMed.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMed.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.PuntoMedio"))
        Me.TxtMed.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtMed.ForeColor = System.Drawing.Color.Blue
        Me.TxtMed.Location = New System.Drawing.Point(88, 32)
        Me.TxtMed.Name = "TxtMed"
        Me.TxtMed.Size = New System.Drawing.Size(52, 13)
        Me.TxtMed.TabIndex = 3
        Me.TxtMed.Text = "0.00"
        Me.TxtMed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtMax
        '
        Me.TxtMax.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMax.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Maxima"))
        Me.TxtMax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtMax.ForeColor = System.Drawing.Color.Blue
        Me.TxtMax.Location = New System.Drawing.Point(160, 32)
        Me.TxtMax.Name = "TxtMax"
        Me.TxtMax.Size = New System.Drawing.Size(52, 13)
        Me.TxtMax.TabIndex = 5
        Me.TxtMax.Text = "0.00"
        Me.TxtMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(6, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 14)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Cod Barras"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(92, 4)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(408, 14)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Descripci�n"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(504, 4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(116, 14)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Presentaci�n"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(4, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Familia"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(4, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Marca"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(12, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 12)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "M�nima"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.ErrorProvider.SetIconAlignment(Me.Label9, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.Label9.Image = CType(resources.GetObject("Label9.Image"), System.Drawing.Image)
        Me.Label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(0, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(630, 32)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Cat�logo de Inventario"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ComboBoxPresentacion
        '
        Me.ComboBoxPresentacion.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.CodPresentacion"))
        Me.ComboBoxPresentacion.DataSource = Me.DataSetInventario
        Me.ComboBoxPresentacion.DisplayMember = "Presentaciones.Presentaciones"
        Me.ComboBoxPresentacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPresentacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxPresentacion.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxPresentacion.ItemHeight = 13
        Me.ComboBoxPresentacion.Location = New System.Drawing.Point(536, 20)
        Me.ComboBoxPresentacion.Name = "ComboBoxPresentacion"
        Me.ComboBoxPresentacion.Size = New System.Drawing.Size(84, 21)
        Me.ComboBoxPresentacion.TabIndex = 6
        Me.ComboBoxPresentacion.ValueMember = "Presentaciones.CodPres"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(88, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 12)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Media"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(160, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 12)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "M�xima"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(4, 428)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(333, 11)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Observaciones"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(4, 65)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 16)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "Ubicaci�n"
        '
        'TxtObservaciones
        '
        Me.TxtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtObservaciones.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Observaciones"))
        Me.TxtObservaciones.ForeColor = System.Drawing.Color.Blue
        Me.TxtObservaciones.Location = New System.Drawing.Point(4, 428)
        Me.TxtObservaciones.Multiline = True
        Me.TxtObservaciones.Name = "TxtObservaciones"
        Me.TxtObservaciones.Size = New System.Drawing.Size(333, 16)
        Me.TxtObservaciones.TabIndex = 9
        Me.TxtObservaciones.Text = ""
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.Control
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label19.ForeColor = System.Drawing.Color.Blue
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(66, 106)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(11, 12)
        Me.Label19.TabIndex = 17
        Me.Label19.Text = "C"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.Control
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label20.ForeColor = System.Drawing.Color.Blue
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(66, 83)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(11, 12)
        Me.Label20.TabIndex = 12
        Me.Label20.Text = "B"
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.Control
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label21.ForeColor = System.Drawing.Color.Blue
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(66, 60)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(11, 16)
        Me.Label21.TabIndex = 7
        Me.Label21.Text = "A"
        '
        'TxtPrecioVenta_A
        '
        Me.TxtPrecioVenta_A.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_A"))
        Me.TxtPrecioVenta_A.EditValue = "0.00"
        Me.TxtPrecioVenta_A.Location = New System.Drawing.Point(84, 60)
        Me.TxtPrecioVenta_A.Name = "TxtPrecioVenta_A"
        '
        'TxtPrecioVenta_A.Properties
        '
        Me.TxtPrecioVenta_A.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_A.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_A.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_A.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_A.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_A.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_A.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_A.TabIndex = 8
        '
        'TxtPrecioVenta_B
        '
        Me.TxtPrecioVenta_B.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_B"))
        Me.TxtPrecioVenta_B.EditValue = "0.00"
        Me.TxtPrecioVenta_B.Location = New System.Drawing.Point(84, 83)
        Me.TxtPrecioVenta_B.Name = "TxtPrecioVenta_B"
        '
        'TxtPrecioVenta_B.Properties
        '
        Me.TxtPrecioVenta_B.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_B.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_B.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_B.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_B.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_B.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_B.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_B.TabIndex = 13
        '
        'TxtPrecioVenta_C
        '
        Me.TxtPrecioVenta_C.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_C"))
        Me.TxtPrecioVenta_C.EditValue = "0.00"
        Me.TxtPrecioVenta_C.Location = New System.Drawing.Point(84, 106)
        Me.TxtPrecioVenta_C.Name = "TxtPrecioVenta_C"
        '
        'TxtPrecioVenta_C.Properties
        '
        Me.TxtPrecioVenta_C.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_C.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_C.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_C.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_C.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_C.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_C.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_C.TabIndex = 18
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.Control
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label22.ForeColor = System.Drawing.Color.Blue
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(66, 128)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(11, 12)
        Me.Label22.TabIndex = 22
        Me.Label22.Text = "D"
        '
        'TxtPrecioVenta_D
        '
        Me.TxtPrecioVenta_D.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_D"))
        Me.TxtPrecioVenta_D.EditValue = "0.00"
        Me.TxtPrecioVenta_D.Location = New System.Drawing.Point(84, 128)
        Me.TxtPrecioVenta_D.Name = "TxtPrecioVenta_D"
        '
        'TxtPrecioVenta_D.Properties
        '
        Me.TxtPrecioVenta_D.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_D.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_D.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_D.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_D.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_D.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_D.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_D.TabIndex = 23
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.Control
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label23.ForeColor = System.Drawing.Color.Blue
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(164, 60)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(12, 12)
        Me.Label23.TabIndex = 9
        Me.Label23.Text = "+"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.TextBoxValorMonedaEnVenta)
        Me.GroupBox2.Controls.Add(Me.Label29)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_D)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_C)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_B)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_D)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_A)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_C)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_B)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_D)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_A)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_C)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_B)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_A)
        Me.GroupBox2.Controls.Add(Me.Label32)
        Me.GroupBox2.Controls.Add(Me.ComboBoxMonedaVenta)
        Me.GroupBox2.Controls.Add(Me.TxtUtilidad_P)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_IV_P)
        Me.GroupBox2.Controls.Add(Me.Label35)
        Me.GroupBox2.Controls.Add(Me.TxtPrecioVenta_P)
        Me.GroupBox2.Controls.Add(Me.Label36)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(352, 180)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(272, 176)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Precio de Venta"
        '
        'TextBoxValorMonedaEnVenta
        '
        Me.TextBoxValorMonedaEnVenta.AcceptsTab = True
        Me.TextBoxValorMonedaEnVenta.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxValorMonedaEnVenta.Enabled = False
        Me.TextBoxValorMonedaEnVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TextBoxValorMonedaEnVenta.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxValorMonedaEnVenta.Location = New System.Drawing.Point(188, 20)
        Me.TextBoxValorMonedaEnVenta.Name = "TextBoxValorMonedaEnVenta"
        Me.TextBoxValorMonedaEnVenta.ReadOnly = True
        Me.TextBoxValorMonedaEnVenta.Size = New System.Drawing.Size(72, 13)
        Me.TextBoxValorMonedaEnVenta.TabIndex = 2
        Me.TextBoxValorMonedaEnVenta.Text = "0.00"
        Me.TextBoxValorMonedaEnVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.Label29.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label29.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label29.Location = New System.Drawing.Point(8, 40)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(48, 16)
        Me.Label29.TabIndex = 3
        Me.Label29.Text = "Utilidad"
        '
        'TxtUtilidad_D
        '
        Me.TxtUtilidad_D.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.TxtUtilidad_D.Location = New System.Drawing.Point(8, 128)
        Me.TxtUtilidad_D.Name = "TxtUtilidad_D"
        '
        'TxtUtilidad_D.Properties
        '
        Me.TxtUtilidad_D.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_D.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_D.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_D.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_D.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_D.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_D.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_D.TabIndex = 21
        '
        'TxtUtilidad_C
        '
        Me.TxtUtilidad_C.EditValue = 0
        Me.TxtUtilidad_C.Location = New System.Drawing.Point(8, 106)
        Me.TxtUtilidad_C.Name = "TxtUtilidad_C"
        '
        'TxtUtilidad_C.Properties
        '
        Me.TxtUtilidad_C.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_C.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_C.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_C.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_C.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_C.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_C.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_C.TabIndex = 16
        '
        'TxtUtilidad_B
        '
        Me.TxtUtilidad_B.EditValue = 0
        Me.TxtUtilidad_B.Location = New System.Drawing.Point(8, 83)
        Me.TxtUtilidad_B.Name = "TxtUtilidad_B"
        '
        'TxtUtilidad_B.Properties
        '
        Me.TxtUtilidad_B.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_B.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_B.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_B.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_B.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_B.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_B.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_B.TabIndex = 11
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.Label28.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label28.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label28.Location = New System.Drawing.Point(180, 40)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(80, 16)
        Me.Label28.TabIndex = 5
        Me.Label28.Text = "Precio + I.V."
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.Label27.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label27.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label27.Location = New System.Drawing.Point(84, 40)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(76, 16)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Precio Venta"
        '
        'TxtPrecioVenta_IV_D
        '
        Me.TxtPrecioVenta_IV_D.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_D.Location = New System.Drawing.Point(180, 128)
        Me.TxtPrecioVenta_IV_D.Name = "TxtPrecioVenta_IV_D"
        '
        'TxtPrecioVenta_IV_D.Properties
        '
        Me.TxtPrecioVenta_IV_D.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_D.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_D.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_D.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_D.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_D.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_D.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_D.TabIndex = 25
        '
        'TxtPrecioVenta_IV_A
        '
        Me.TxtPrecioVenta_IV_A.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_A.Location = New System.Drawing.Point(180, 60)
        Me.TxtPrecioVenta_IV_A.Name = "TxtPrecioVenta_IV_A"
        '
        'TxtPrecioVenta_IV_A.Properties
        '
        Me.TxtPrecioVenta_IV_A.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_A.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_A.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_A.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_A.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_A.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_A.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_A.TabIndex = 10
        '
        'TxtPrecioVenta_IV_C
        '
        Me.TxtPrecioVenta_IV_C.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_C.Location = New System.Drawing.Point(180, 106)
        Me.TxtPrecioVenta_IV_C.Name = "TxtPrecioVenta_IV_C"
        '
        'TxtPrecioVenta_IV_C.Properties
        '
        Me.TxtPrecioVenta_IV_C.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_C.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_C.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_C.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_C.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_C.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_C.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_C.TabIndex = 20
        '
        'TxtPrecioVenta_IV_B
        '
        Me.TxtPrecioVenta_IV_B.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_B.Location = New System.Drawing.Point(180, 83)
        Me.TxtPrecioVenta_IV_B.Name = "TxtPrecioVenta_IV_B"
        '
        'TxtPrecioVenta_IV_B.Properties
        '
        Me.TxtPrecioVenta_IV_B.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_B.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_B.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_B.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_B.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_B.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_B.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_B.TabIndex = 15
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.SystemColors.Control
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label26.ForeColor = System.Drawing.Color.Blue
        Me.Label26.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label26.Location = New System.Drawing.Point(164, 128)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(12, 12)
        Me.Label26.TabIndex = 24
        Me.Label26.Text = "+"
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.Control
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label25.ForeColor = System.Drawing.Color.Blue
        Me.Label25.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label25.Location = New System.Drawing.Point(164, 106)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(12, 12)
        Me.Label25.TabIndex = 19
        Me.Label25.Text = "+"
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.SystemColors.Control
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label24.ForeColor = System.Drawing.Color.Blue
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(164, 83)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(12, 12)
        Me.Label24.TabIndex = 14
        Me.Label24.Text = "+"
        '
        'TxtUtilidad_A
        '
        Me.TxtUtilidad_A.EditValue = "0.00"
        Me.TxtUtilidad_A.Location = New System.Drawing.Point(8, 60)
        Me.TxtUtilidad_A.Name = "TxtUtilidad_A"
        '
        'TxtUtilidad_A.Properties
        '
        Me.TxtUtilidad_A.Properties.DisplayFormat.FormatString = "#,#0.0000"
        Me.TxtUtilidad_A.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_A.Properties.EditFormat.FormatString = "#,#0.0000"
        Me.TxtUtilidad_A.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_A.Properties.MaskData.EditMask = "#,#0.0000"
        Me.TxtUtilidad_A.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_A.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_A.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_A.TabIndex = 6
        '
        'Label32
        '
        Me.Label32.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label32.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label32.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label32.Location = New System.Drawing.Point(8, 20)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(48, 16)
        Me.Label32.TabIndex = 0
        Me.Label32.Text = "Moneda"
        '
        'ComboBoxMonedaVenta
        '
        Me.ComboBoxMonedaVenta.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.MonedaVenta"))
        Me.ComboBoxMonedaVenta.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Moneda.ValorCompra"))
        Me.ComboBoxMonedaVenta.DataSource = Me.DataSetInventario.Moneda
        Me.ComboBoxMonedaVenta.DisplayMember = "MonedaNombre"
        Me.ComboBoxMonedaVenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMonedaVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxMonedaVenta.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxMonedaVenta.ItemHeight = 13
        Me.ComboBoxMonedaVenta.Location = New System.Drawing.Point(82, 16)
        Me.ComboBoxMonedaVenta.Name = "ComboBoxMonedaVenta"
        Me.ComboBoxMonedaVenta.Size = New System.Drawing.Size(92, 21)
        Me.ComboBoxMonedaVenta.TabIndex = 1
        Me.ComboBoxMonedaVenta.ValueMember = "Moneda.CodMoneda"
        '
        'TxtUtilidad_P
        '
        Me.TxtUtilidad_P.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.TxtUtilidad_P.Location = New System.Drawing.Point(8, 150)
        Me.TxtUtilidad_P.Name = "TxtUtilidad_P"
        '
        'TxtUtilidad_P.Properties
        '
        Me.TxtUtilidad_P.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_P.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_P.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtUtilidad_P.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtUtilidad_P.Properties.Enabled = False
        Me.TxtUtilidad_P.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtUtilidad_P.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtUtilidad_P.Size = New System.Drawing.Size(56, 19)
        Me.TxtUtilidad_P.TabIndex = 26
        '
        'TxtPrecioVenta_IV_P
        '
        Me.TxtPrecioVenta_IV_P.EditValue = "0.00"
        Me.TxtPrecioVenta_IV_P.Location = New System.Drawing.Point(180, 150)
        Me.TxtPrecioVenta_IV_P.Name = "TxtPrecioVenta_IV_P"
        '
        'TxtPrecioVenta_IV_P.Properties
        '
        Me.TxtPrecioVenta_IV_P.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_P.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_P.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_IV_P.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_IV_P.Properties.Enabled = False
        Me.TxtPrecioVenta_IV_P.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_IV_P.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_IV_P.Size = New System.Drawing.Size(80, 19)
        Me.TxtPrecioVenta_IV_P.TabIndex = 30
        '
        'Label35
        '
        Me.Label35.BackColor = System.Drawing.SystemColors.Control
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label35.ForeColor = System.Drawing.Color.Blue
        Me.Label35.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label35.Location = New System.Drawing.Point(164, 150)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(12, 12)
        Me.Label35.TabIndex = 29
        Me.Label35.Text = "+"
        '
        'TxtPrecioVenta_P
        '
        Me.TxtPrecioVenta_P.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Precio_Promo"))
        Me.TxtPrecioVenta_P.EditValue = "0.00"
        Me.TxtPrecioVenta_P.Location = New System.Drawing.Point(84, 150)
        Me.TxtPrecioVenta_P.Name = "TxtPrecioVenta_P"
        '
        'TxtPrecioVenta_P.Properties
        '
        Me.TxtPrecioVenta_P.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_P.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_P.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtPrecioVenta_P.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtPrecioVenta_P.Properties.Enabled = False
        Me.TxtPrecioVenta_P.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtPrecioVenta_P.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtPrecioVenta_P.Size = New System.Drawing.Size(76, 19)
        Me.TxtPrecioVenta_P.TabIndex = 28
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.SystemColors.Control
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label36.ForeColor = System.Drawing.Color.Blue
        Me.Label36.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label36.Location = New System.Drawing.Point(66, 151)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(11, 12)
        Me.Label36.TabIndex = 27
        Me.Label36.Text = "P"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.TxtExistencia)
        Me.GroupBox3.Controls.Add(Me.TxtMin)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.TxtMed)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.TxtMax)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox3.Location = New System.Drawing.Point(8, 360)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(328, 56)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Existencias"
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label30.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(236, 16)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(72, 12)
        Me.Label30.TabIndex = 6
        Me.Label30.Text = "Actual"
        '
        'TxtExistencia
        '
        Me.TxtExistencia.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtExistencia.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Existencia"))
        Me.TxtExistencia.Enabled = False
        Me.TxtExistencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtExistencia.ForeColor = System.Drawing.Color.Blue
        Me.TxtExistencia.Location = New System.Drawing.Point(236, 32)
        Me.TxtExistencia.Name = "TxtExistencia"
        Me.TxtExistencia.Size = New System.Drawing.Size(72, 13)
        Me.TxtExistencia.TabIndex = 7
        Me.TxtExistencia.Text = "0.00"
        Me.TxtExistencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtMin
        '
        Me.TxtMin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMin.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Minima"))
        Me.TxtMin.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtMin.ForeColor = System.Drawing.Color.Blue
        Me.TxtMin.Location = New System.Drawing.Point(12, 32)
        Me.TxtMin.Name = "TxtMin"
        Me.TxtMin.Size = New System.Drawing.Size(52, 13)
        Me.TxtMin.TabIndex = 1
        Me.TxtMin.Text = "0.00"
        Me.TxtMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ComboBoxBodegas
        '
        Me.ComboBoxBodegas.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.Id_Bodega"))
        Me.ComboBoxBodegas.DataSource = Me.DataSetInventario
        Me.ComboBoxBodegas.DisplayMember = "Bodegas.Nombre_Bodega"
        Me.ComboBoxBodegas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBodegas.Enabled = False
        Me.ComboBoxBodegas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxBodegas.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ComboBoxBodegas.ItemHeight = 13
        Me.ComboBoxBodegas.Location = New System.Drawing.Point(8, 40)
        Me.ComboBoxBodegas.Name = "ComboBoxBodegas"
        Me.ComboBoxBodegas.Size = New System.Drawing.Size(256, 21)
        Me.ComboBoxBodegas.TabIndex = 1
        Me.ComboBoxBodegas.ValueMember = "Bodegas.ID_Bodega"
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.AutoSize = False
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarEliminar, Me.ToolBarImprimir, Me.ToolBarEliminador, Me.ToolBarCerrar})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 424)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(630, 52)
        Me.ToolBar1.TabIndex = 11
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Text = "Nuevo"
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.ImageIndex = 1
        Me.ToolBarBuscar.Text = "Buscar"
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Text = "Actualizar"
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.ImageIndex = 3
        Me.ToolBarEliminar.Text = "Inhabilitar"
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.ImageIndex = 7
        Me.ToolBarImprimir.Text = "Imprimir"
        '
        'ToolBarEliminador
        '
        Me.ToolBarEliminador.ImageIndex = 9
        Me.ToolBarEliminador.Text = "Eliminar"
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'ComboBoxMarca
        '
        Me.ComboBoxMarca.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.CodMarca"))
        Me.ComboBoxMarca.DataSource = Me.DataSetInventario
        Me.ComboBoxMarca.DisplayMember = "Marcas.Marca"
        Me.ComboBoxMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMarca.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxMarca.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxMarca.ItemHeight = 13
        Me.ComboBoxMarca.Location = New System.Drawing.Point(88, 112)
        Me.ComboBoxMarca.Name = "ComboBoxMarca"
        Me.ComboBoxMarca.Size = New System.Drawing.Size(532, 21)
        Me.ComboBoxMarca.TabIndex = 14
        Me.ComboBoxMarca.ValueMember = "Marcas.CodMarca"
        '
        'AdapterMarcas
        '
        Me.AdapterMarcas.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterMarcas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Marcas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMarca", "CodMarca"), New System.Data.Common.DataColumnMapping("Marca", "Marca")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT CodMarca, Marca FROM Marcas"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'ComboBoxFamilia
        '
        Me.ComboBoxFamilia.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.SubFamilia"))
        Me.ComboBoxFamilia.DataSource = Me.DataSetInventario
        Me.ComboBoxFamilia.DisplayMember = "SubFamilias.Familiares"
        Me.ComboBoxFamilia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxFamilia.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxFamilia.ItemHeight = 13
        Me.ComboBoxFamilia.Location = New System.Drawing.Point(88, 40)
        Me.ComboBoxFamilia.Name = "ComboBoxFamilia"
        Me.ComboBoxFamilia.Size = New System.Drawing.Size(532, 21)
        Me.ComboBoxFamilia.TabIndex = 8
        Me.ComboBoxFamilia.ValueMember = "SubFamilias.Codigo"
        '
        'ComboBox_Ubicacion_SubUbicacion
        '
        Me.ComboBox_Ubicacion_SubUbicacion.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.SubUbicacion"))
        Me.ComboBox_Ubicacion_SubUbicacion.DataSource = Me.DataSetInventario
        Me.ComboBox_Ubicacion_SubUbicacion.DisplayMember = "SubUbicacion.Ubicaciones"
        Me.ComboBox_Ubicacion_SubUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox_Ubicacion_SubUbicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBox_Ubicacion_SubUbicacion.ForeColor = System.Drawing.Color.Blue
        Me.ComboBox_Ubicacion_SubUbicacion.ItemHeight = 13
        Me.ComboBox_Ubicacion_SubUbicacion.Location = New System.Drawing.Point(88, 64)
        Me.ComboBox_Ubicacion_SubUbicacion.Name = "ComboBox_Ubicacion_SubUbicacion"
        Me.ComboBox_Ubicacion_SubUbicacion.Size = New System.Drawing.Size(532, 21)
        Me.ComboBox_Ubicacion_SubUbicacion.TabIndex = 10
        Me.ComboBox_Ubicacion_SubUbicacion.ValueMember = "SubUbicacion.Codigo"
        '
        'AdapterFamilia
        '
        Me.AdapterFamilia.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterFamilia.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "SubFamilias", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Familiares", "Familiares")})})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT SubFamilias.Codigo, Familia.Descripcion + '/' + SubFamilias.Descripcion AS" & _
        " Familiares FROM SubFamilias INNER JOIN Familia ON SubFamilias.CodigoFamilia = F" & _
        "amilia.Codigo"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'AdapterPresentacion
        '
        Me.AdapterPresentacion.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterPresentacion.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Presentaciones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Presentaciones", "Presentaciones"), New System.Data.Common.DataColumnMapping("CodPres", "CodPres")})})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Presentaciones, CodPres FROM Presentaciones"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'AdapterCasaConsignante
        '
        Me.AdapterCasaConsignante.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterCasaConsignante.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Bodegas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Nombre_Bodega", "Nombre_Bodega"), New System.Data.Common.DataColumnMapping("ID_Bodega", "ID_Bodega")})})
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT Nombre_Bodega, ID_Bodega FROM Bodegas"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'DataNavigator1
        '
        Me.DataNavigator1.Buttons.Append.Visible = False
        Me.DataNavigator1.Buttons.CancelEdit.Visible = False
        Me.DataNavigator1.Buttons.EndEdit.Visible = False
        Me.DataNavigator1.Buttons.Remove.Visible = False
        Me.DataNavigator1.DataMember = "Inventario2"
        Me.DataNavigator1.DataSource = Me.DataSetInventario
        Me.DataNavigator1.Location = New System.Drawing.Point(488, 456)
        Me.DataNavigator1.Name = "DataNavigator1"
        Me.DataNavigator1.Size = New System.Drawing.Size(134, 21)
        Me.DataNavigator1.TabIndex = 68
        Me.DataNavigator1.Text = "DataNavigator1"
        '
        'AdapterAUbicacion
        '
        Me.AdapterAUbicacion.SelectCommand = Me.SqlSelectCommand6
        Me.AdapterAUbicacion.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "SubUbicacion", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Ubicaciones", "Ubicaciones")})})
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT SubUbicacion.Codigo, Ubicaciones.Descripcion + '/' + SubUbicacion.Descripc" & _
        "ionD AS Ubicaciones FROM SubUbicacion INNER JOIN Ubicaciones ON SubUbicacion.Cod" & _
        "_Ubicacion = Ubicaciones.Codigo"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand7
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra")})})
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra FROM Moneda"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection1
        '
        'AdapterInventario
        '
        Me.AdapterInventario.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterInventario.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterInventario.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterInventario.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Inventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Barras", "Barras"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("PresentaCant", "PresentaCant"), New System.Data.Common.DataColumnMapping("CodPresentacion", "CodPresentacion"), New System.Data.Common.DataColumnMapping("CodMarca", "CodMarca"), New System.Data.Common.DataColumnMapping("SubFamilia", "SubFamilia"), New System.Data.Common.DataColumnMapping("Minima", "Minima"), New System.Data.Common.DataColumnMapping("PuntoMedio", "PuntoMedio"), New System.Data.Common.DataColumnMapping("Maxima", "Maxima"), New System.Data.Common.DataColumnMapping("SubUbicacion", "SubUbicacion"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("MonedaCosto", "MonedaCosto"), New System.Data.Common.DataColumnMapping("PrecioBase", "PrecioBase"), New System.Data.Common.DataColumnMapping("Fletes", "Fletes"), New System.Data.Common.DataColumnMapping("OtrosCargos", "OtrosCargos"), New System.Data.Common.DataColumnMapping("Costo", "Costo"), New System.Data.Common.DataColumnMapping("MonedaVenta", "MonedaVenta"), New System.Data.Common.DataColumnMapping("IVenta", "IVenta"), New System.Data.Common.DataColumnMapping("Precio_A", "Precio_A"), New System.Data.Common.DataColumnMapping("Precio_B", "Precio_B"), New System.Data.Common.DataColumnMapping("Precio_C", "Precio_C"), New System.Data.Common.DataColumnMapping("Precio_D", "Precio_D"), New System.Data.Common.DataColumnMapping("Precio_Promo", "Precio_Promo"), New System.Data.Common.DataColumnMapping("Promo_Activa", "Promo_Activa"), New System.Data.Common.DataColumnMapping("Promo_Inicio", "Promo_Inicio"), New System.Data.Common.DataColumnMapping("Promo_Finaliza", "Promo_Finaliza"), New System.Data.Common.DataColumnMapping("Max_Comision", "Max_Comision"), New System.Data.Common.DataColumnMapping("Max_Descuento", "Max_Descuento"), New System.Data.Common.DataColumnMapping("FechaIngreso", "FechaIngreso"), New System.Data.Common.DataColumnMapping("Servicio", "Servicio"), New System.Data.Common.DataColumnMapping("Inhabilitado", "Inhabilitado"), New System.Data.Common.DataColumnMapping("Proveedor", "Proveedor"), New System.Data.Common.DataColumnMapping("Precio_Sugerido", "Precio_Sugerido"), New System.Data.Common.DataColumnMapping("SugeridoIV", "SugeridoIV"), New System.Data.Common.DataColumnMapping("PreguntaPrecio", "PreguntaPrecio"), New System.Data.Common.DataColumnMapping("Existencia", "Existencia"), New System.Data.Common.DataColumnMapping("Lote", "Lote"), New System.Data.Common.DataColumnMapping("Consignacion", "Consignacion"), New System.Data.Common.DataColumnMapping("Id_Bodega", "Id_Bodega"), New System.Data.Common.DataColumnMapping("ExistenciaBodega", "ExistenciaBodega")})})
        Me.AdapterInventario.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Inventario WHERE (Codigo = @Original_Codigo) AND (Barras = @Original_" & _
        "Barras) AND (CodMarca = @Original_CodMarca) AND (CodPresentacion = @Original_Cod" & _
        "Presentacion) AND (Consignacion = @Original_Consignacion) AND (Costo = @Original" & _
        "_Costo) AND (Descripcion = @Original_Descripcion) AND (Existencia = @Original_Ex" & _
        "istencia) AND (ExistenciaBodega = @Original_ExistenciaBodega) AND (FechaIngreso " & _
        "= @Original_FechaIngreso) AND (Fletes = @Original_Fletes) AND (IVenta = @Origina" & _
        "l_IVenta) AND (Id_Bodega = @Original_Id_Bodega) AND (Inhabilitado = @Original_In" & _
        "habilitado) AND (Lote = @Original_Lote) AND (Max_Comision = @Original_Max_Comisi" & _
        "on) AND (Max_Descuento = @Original_Max_Descuento) AND (Maxima = @Original_Maxima" & _
        ") AND (Minima = @Original_Minima) AND (MonedaCosto = @Original_MonedaCosto) AND " & _
        "(MonedaVenta = @Original_MonedaVenta) AND (Observaciones = @Original_Observacion" & _
        "es) AND (OtrosCargos = @Original_OtrosCargos) AND (PrecioBase = @Original_Precio" & _
        "Base) AND (Precio_A = @Original_Precio_A) AND (Precio_B = @Original_Precio_B) AN" & _
        "D (Precio_C = @Original_Precio_C) AND (Precio_D = @Original_Precio_D) AND (Preci" & _
        "o_Promo = @Original_Precio_Promo) AND (Precio_Sugerido = @Original_Precio_Sugeri" & _
        "do) AND (PreguntaPrecio = @Original_PreguntaPrecio) AND (PresentaCant = @Origina" & _
        "l_PresentaCant) AND (Promo_Activa = @Original_Promo_Activa) AND (Promo_Finaliza " & _
        "= @Original_Promo_Finaliza) AND (Promo_Inicio = @Original_Promo_Inicio) AND (Pro" & _
        "veedor = @Original_Proveedor) AND (PuntoMedio = @Original_PuntoMedio) AND (Servi" & _
        "cio = @Original_Servicio) AND (SubFamilia = @Original_SubFamilia) AND (SubUbicac" & _
        "ion = @Original_SubUbicacion) AND (SugeridoIV = @Original_SugeridoIV) AND (EsPes" & _
        "ado = @Original_EsPesado)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMarca", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMarca", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodPresentacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodPresentacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consignacion", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consignacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Costo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Costo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExistenciaBodega", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExistenciaBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaIngreso", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaIngreso", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fletes", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fletes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Bodega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Bodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Inhabilitado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inhabilitado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Lote", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Lote", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Max_Comision", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Comision", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Max_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Descuento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Maxima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maxima", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Minima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Minima", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaCosto", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaCosto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaVenta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OtrosCargos", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OtrosCargos", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PrecioBase", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioBase", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_A", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_A", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_B", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_B", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_C", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_C", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_D", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_D", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_Promo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Promo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_Sugerido", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Sugerido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PreguntaPrecio", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PreguntaPrecio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PresentaCant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PresentaCant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Promo_Activa", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Activa", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Promo_Finaliza", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Finaliza", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Promo_Inicio", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Inicio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Proveedor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PuntoMedio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PuntoMedio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Servicio", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Servicio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubFamilia", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubFamilia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubUbicacion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubUbicacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SugeridoIV", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SugeridoIV", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EsPesado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EsPesado", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Inventario(Codigo, Barras, Descripcion, PresentaCant, CodPresentacion" & _
        ", CodMarca, SubFamilia, Minima, PuntoMedio, Maxima, SubUbicacion, Observaciones," & _
        " MonedaCosto, PrecioBase, Fletes, OtrosCargos, Costo, MonedaVenta, IVenta, Preci" & _
        "o_A, Precio_B, Precio_C, Precio_D, Precio_Promo, Promo_Activa, Promo_Inicio, Pro" & _
        "mo_Finaliza, Max_Comision, Max_Descuento, FechaIngreso, Servicio, Inhabilitado, " & _
        "Proveedor, Precio_Sugerido, SugeridoIV, PreguntaPrecio, Existencia, Lote, Consig" & _
        "nacion, Id_Bodega, ExistenciaBodega, EsPesado) VALUES (@Codigo, @Barras, @Descri" & _
        "pcion, @PresentaCant, @CodPresentacion, @CodMarca, @SubFamilia, @Minima, @PuntoM" & _
        "edio, @Maxima, @SubUbicacion, @Observaciones, @MonedaCosto, @PrecioBase, @Fletes" & _
        ", @OtrosCargos, @Costo, @MonedaVenta, @IVenta, @Precio_A, @Precio_B, @Precio_C, " & _
        "@Precio_D, @Precio_Promo, @Promo_Activa, @Promo_Inicio, @Promo_Finaliza, @Max_Co" & _
        "mision, @Max_Descuento, @FechaIngreso, @Servicio, @Inhabilitado, @Proveedor, @Pr" & _
        "ecio_Sugerido, @SugeridoIV, @PreguntaPrecio, @Existencia, @Lote, @Consignacion, " & _
        "@Id_Bodega, @ExistenciaBodega, @EsPesado); SELECT Codigo, Barras, Descripcion, P" & _
        "resentaCant, CodPresentacion, CodMarca, SubFamilia, Minima, PuntoMedio, Maxima, " & _
        "SubUbicacion, Observaciones, MonedaCosto, PrecioBase, Fletes, OtrosCargos, Costo" & _
        ", MonedaVenta, IVenta, Precio_A, Precio_B, Precio_C, Precio_D, Precio_Promo, Pro" & _
        "mo_Activa, Promo_Inicio, Promo_Finaliza, Max_Comision, Max_Descuento, FechaIngre" & _
        "so, Servicio, Inhabilitado, Proveedor, Precio_Sugerido, SugeridoIV, PreguntaPrec" & _
        "io, Existencia, Lote, Consignacion, Id_Bodega, ExistenciaBodega, EsPesado FROM I" & _
        "nventario WHERE (Codigo = @Codigo)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PresentaCant", System.Data.SqlDbType.Float, 8, "PresentaCant"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodPresentacion", System.Data.SqlDbType.Int, 4, "CodPresentacion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMarca", System.Data.SqlDbType.Int, 4, "CodMarca"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubFamilia", System.Data.SqlDbType.VarChar, 10, "SubFamilia"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Minima", System.Data.SqlDbType.Float, 8, "Minima"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PuntoMedio", System.Data.SqlDbType.Float, 8, "PuntoMedio"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Maxima", System.Data.SqlDbType.Float, 8, "Maxima"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubUbicacion", System.Data.SqlDbType.VarChar, 10, "SubUbicacion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaCosto", System.Data.SqlDbType.Int, 4, "MonedaCosto"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PrecioBase", System.Data.SqlDbType.Float, 8, "PrecioBase"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fletes", System.Data.SqlDbType.Float, 8, "Fletes"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OtrosCargos", System.Data.SqlDbType.Float, 8, "OtrosCargos"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 8, "Costo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaVenta", System.Data.SqlDbType.Int, 4, "MonedaVenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IVenta", System.Data.SqlDbType.Float, 8, "IVenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_A", System.Data.SqlDbType.Float, 8, "Precio_A"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_B", System.Data.SqlDbType.Float, 8, "Precio_B"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_C", System.Data.SqlDbType.Float, 8, "Precio_C"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_D", System.Data.SqlDbType.Float, 8, "Precio_D"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_Promo", System.Data.SqlDbType.Float, 8, "Precio_Promo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Promo_Activa", System.Data.SqlDbType.Bit, 1, "Promo_Activa"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Promo_Inicio", System.Data.SqlDbType.DateTime, 4, "Promo_Inicio"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Promo_Finaliza", System.Data.SqlDbType.DateTime, 4, "Promo_Finaliza"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Max_Comision", System.Data.SqlDbType.Float, 8, "Max_Comision"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Max_Descuento", System.Data.SqlDbType.Float, 8, "Max_Descuento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaIngreso", System.Data.SqlDbType.DateTime, 8, "FechaIngreso"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Servicio", System.Data.SqlDbType.Bit, 1, "Servicio"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Inhabilitado", System.Data.SqlDbType.Bit, 1, "Inhabilitado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 4, "Proveedor"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_Sugerido", System.Data.SqlDbType.Float, 8, "Precio_Sugerido"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SugeridoIV", System.Data.SqlDbType.Float, 8, "SugeridoIV"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PreguntaPrecio", System.Data.SqlDbType.Bit, 1, "PreguntaPrecio"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Lote", System.Data.SqlDbType.Bit, 1, "Lote"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consignacion", System.Data.SqlDbType.Bit, 1, "Consignacion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Bodega", System.Data.SqlDbType.Int, 4, "Id_Bodega"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExistenciaBodega", System.Data.SqlDbType.Float, 8, "ExistenciaBodega"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EsPesado", System.Data.SqlDbType.Bit, 1, "EsPesado"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Codigo, Barras, Descripcion, PresentaCant, CodPresentacion, CodMarca, SubF" & _
        "amilia, Minima, PuntoMedio, Maxima, SubUbicacion, Observaciones, MonedaCosto, Pr" & _
        "ecioBase, Fletes, OtrosCargos, Costo, MonedaVenta, IVenta, Precio_A, Precio_B, P" & _
        "recio_C, Precio_D, Precio_Promo, Promo_Activa, Promo_Inicio, Promo_Finaliza, Max" & _
        "_Comision, Max_Descuento, FechaIngreso, Servicio, Inhabilitado, Proveedor, Preci" & _
        "o_Sugerido, SugeridoIV, PreguntaPrecio, Existencia, Lote, Consignacion, Id_Bodeg" & _
        "a, ExistenciaBodega, EsPesado FROM Inventario"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Inventario SET Codigo = @Codigo, Barras = @Barras, Descripcion = @Descripc" & _
        "ion, PresentaCant = @PresentaCant, CodPresentacion = @CodPresentacion, CodMarca " & _
        "= @CodMarca, SubFamilia = @SubFamilia, Minima = @Minima, PuntoMedio = @PuntoMedi" & _
        "o, Maxima = @Maxima, SubUbicacion = @SubUbicacion, Observaciones = @Observacione" & _
        "s, MonedaCosto = @MonedaCosto, PrecioBase = @PrecioBase, Fletes = @Fletes, Otros" & _
        "Cargos = @OtrosCargos, Costo = @Costo, MonedaVenta = @MonedaVenta, IVenta = @IVe" & _
        "nta, Precio_A = @Precio_A, Precio_B = @Precio_B, Precio_C = @Precio_C, Precio_D " & _
        "= @Precio_D, Precio_Promo = @Precio_Promo, Promo_Activa = @Promo_Activa, Promo_I" & _
        "nicio = @Promo_Inicio, Promo_Finaliza = @Promo_Finaliza, Max_Comision = @Max_Com" & _
        "ision, Max_Descuento = @Max_Descuento, FechaIngreso = @FechaIngreso, Servicio = " & _
        "@Servicio, Inhabilitado = @Inhabilitado, Proveedor = @Proveedor, Precio_Sugerido" & _
        " = @Precio_Sugerido, SugeridoIV = @SugeridoIV, PreguntaPrecio = @PreguntaPrecio," & _
        " Existencia = @Existencia, Lote = @Lote, Consignacion = @Consignacion, Id_Bodega" & _
        " = @Id_Bodega, ExistenciaBodega = @ExistenciaBodega, EsPesado = @EsPesado WHERE " & _
        "(Codigo = @Original_Codigo) AND (Barras = @Original_Barras) AND (CodMarca = @Ori" & _
        "ginal_CodMarca) AND (CodPresentacion = @Original_CodPresentacion) AND (Consignac" & _
        "ion = @Original_Consignacion) AND (Costo = @Original_Costo) AND (Descripcion = @" & _
        "Original_Descripcion) AND (Existencia = @Original_Existencia) AND (ExistenciaBod" & _
        "ega = @Original_ExistenciaBodega) AND (FechaIngreso = @Original_FechaIngreso) AN" & _
        "D (Fletes = @Original_Fletes) AND (IVenta = @Original_IVenta) AND (Id_Bodega = @" & _
        "Original_Id_Bodega) AND (Inhabilitado = @Original_Inhabilitado) AND (Lote = @Ori" & _
        "ginal_Lote) AND (Max_Comision = @Original_Max_Comision) AND (Max_Descuento = @Or" & _
        "iginal_Max_Descuento) AND (Maxima = @Original_Maxima) AND (Minima = @Original_Mi" & _
        "nima) AND (MonedaCosto = @Original_MonedaCosto) AND (MonedaVenta = @Original_Mon" & _
        "edaVenta) AND (Observaciones = @Original_Observaciones) AND (OtrosCargos = @Orig" & _
        "inal_OtrosCargos) AND (PrecioBase = @Original_PrecioBase) AND (Precio_A = @Origi" & _
        "nal_Precio_A) AND (Precio_B = @Original_Precio_B) AND (Precio_C = @Original_Prec" & _
        "io_C) AND (Precio_D = @Original_Precio_D) AND (Precio_Promo = @Original_Precio_P" & _
        "romo) AND (Precio_Sugerido = @Original_Precio_Sugerido) AND (PreguntaPrecio = @O" & _
        "riginal_PreguntaPrecio) AND (PresentaCant = @Original_PresentaCant) AND (Promo_A" & _
        "ctiva = @Original_Promo_Activa) AND (Promo_Finaliza = @Original_Promo_Finaliza) " & _
        "AND (Promo_Inicio = @Original_Promo_Inicio) AND (Proveedor = @Original_Proveedor" & _
        ") AND (PuntoMedio = @Original_PuntoMedio) AND (Servicio = @Original_Servicio) AN" & _
        "D (SubFamilia = @Original_SubFamilia) AND (SubUbicacion = @Original_SubUbicacion" & _
        ") AND (SugeridoIV = @Original_SugeridoIV) AND (EsPesado = @Original_EsPesado); S" & _
        "ELECT Codigo, Barras, Descripcion, PresentaCant, CodPresentacion, CodMarca, SubF" & _
        "amilia, Minima, PuntoMedio, Maxima, SubUbicacion, Observaciones, MonedaCosto, Pr" & _
        "ecioBase, Fletes, OtrosCargos, Costo, MonedaVenta, IVenta, Precio_A, Precio_B, P" & _
        "recio_C, Precio_D, Precio_Promo, Promo_Activa, Promo_Inicio, Promo_Finaliza, Max" & _
        "_Comision, Max_Descuento, FechaIngreso, Servicio, Inhabilitado, Proveedor, Preci" & _
        "o_Sugerido, SugeridoIV, PreguntaPrecio, Existencia, Lote, Consignacion, Id_Bodeg" & _
        "a, ExistenciaBodega, EsPesado FROM Inventario WHERE (Codigo = @Codigo)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 255, "Descripcion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PresentaCant", System.Data.SqlDbType.Float, 8, "PresentaCant"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodPresentacion", System.Data.SqlDbType.Int, 4, "CodPresentacion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMarca", System.Data.SqlDbType.Int, 4, "CodMarca"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubFamilia", System.Data.SqlDbType.VarChar, 10, "SubFamilia"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Minima", System.Data.SqlDbType.Float, 8, "Minima"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PuntoMedio", System.Data.SqlDbType.Float, 8, "PuntoMedio"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Maxima", System.Data.SqlDbType.Float, 8, "Maxima"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubUbicacion", System.Data.SqlDbType.VarChar, 10, "SubUbicacion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaCosto", System.Data.SqlDbType.Int, 4, "MonedaCosto"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PrecioBase", System.Data.SqlDbType.Float, 8, "PrecioBase"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fletes", System.Data.SqlDbType.Float, 8, "Fletes"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OtrosCargos", System.Data.SqlDbType.Float, 8, "OtrosCargos"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 8, "Costo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaVenta", System.Data.SqlDbType.Int, 4, "MonedaVenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IVenta", System.Data.SqlDbType.Float, 8, "IVenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_A", System.Data.SqlDbType.Float, 8, "Precio_A"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_B", System.Data.SqlDbType.Float, 8, "Precio_B"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_C", System.Data.SqlDbType.Float, 8, "Precio_C"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_D", System.Data.SqlDbType.Float, 8, "Precio_D"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_Promo", System.Data.SqlDbType.Float, 8, "Precio_Promo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Promo_Activa", System.Data.SqlDbType.Bit, 1, "Promo_Activa"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Promo_Inicio", System.Data.SqlDbType.DateTime, 4, "Promo_Inicio"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Promo_Finaliza", System.Data.SqlDbType.DateTime, 4, "Promo_Finaliza"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Max_Comision", System.Data.SqlDbType.Float, 8, "Max_Comision"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Max_Descuento", System.Data.SqlDbType.Float, 8, "Max_Descuento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaIngreso", System.Data.SqlDbType.DateTime, 8, "FechaIngreso"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Servicio", System.Data.SqlDbType.Bit, 1, "Servicio"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Inhabilitado", System.Data.SqlDbType.Bit, 1, "Inhabilitado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Proveedor", System.Data.SqlDbType.Int, 4, "Proveedor"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Precio_Sugerido", System.Data.SqlDbType.Float, 8, "Precio_Sugerido"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SugeridoIV", System.Data.SqlDbType.Float, 8, "SugeridoIV"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PreguntaPrecio", System.Data.SqlDbType.Bit, 1, "PreguntaPrecio"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Existencia", System.Data.SqlDbType.Float, 8, "Existencia"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Lote", System.Data.SqlDbType.Bit, 1, "Lote"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Consignacion", System.Data.SqlDbType.Bit, 1, "Consignacion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Bodega", System.Data.SqlDbType.Int, 4, "Id_Bodega"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExistenciaBodega", System.Data.SqlDbType.Float, 8, "ExistenciaBodega"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EsPesado", System.Data.SqlDbType.Bit, 1, "EsPesado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Barras", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Barras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMarca", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMarca", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodPresentacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodPresentacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Consignacion", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consignacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Costo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Costo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Existencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Existencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExistenciaBodega", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExistenciaBodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaIngreso", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaIngreso", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fletes", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fletes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Bodega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Bodega", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Inhabilitado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inhabilitado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Lote", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Lote", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Max_Comision", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Comision", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Max_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Descuento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Maxima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maxima", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Minima", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Minima", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaCosto", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaCosto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaVenta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaVenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OtrosCargos", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OtrosCargos", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PrecioBase", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioBase", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_A", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_A", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_B", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_B", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_C", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_C", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_D", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_D", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_Promo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Promo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Precio_Sugerido", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Sugerido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PreguntaPrecio", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PreguntaPrecio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PresentaCant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PresentaCant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Promo_Activa", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Activa", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Promo_Finaliza", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Finaliza", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Promo_Inicio", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Promo_Inicio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Proveedor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PuntoMedio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PuntoMedio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Servicio", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Servicio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubFamilia", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubFamilia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubUbicacion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubUbicacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SugeridoIV", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SugeridoIV", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EsPesado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EsPesado", System.Data.DataRowVersion.Original, Nothing))
        '
        'TxtCanPresentacion
        '
        Me.TxtCanPresentacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtCanPresentacion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.PresentaCant"))
        Me.TxtCanPresentacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCanPresentacion.ForeColor = System.Drawing.Color.Blue
        Me.TxtCanPresentacion.Location = New System.Drawing.Point(504, 20)
        Me.TxtCanPresentacion.Name = "TxtCanPresentacion"
        Me.TxtCanPresentacion.Size = New System.Drawing.Size(32, 20)
        Me.TxtCanPresentacion.TabIndex = 5
        Me.TxtCanPresentacion.Text = ""
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Agregar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Eliminar"
        '
        'AdapterArticulosXproveedor
        '
        Me.AdapterArticulosXproveedor.SelectCommand = Me.SqlSelectCommand8
        Me.AdapterArticulosXproveedor.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Articulos x Proveedor", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoArticulo", "CodigoArticulo"), New System.Data.Common.DataColumnMapping("CodigoProveedor", "CodigoProveedor"), New System.Data.Common.DataColumnMapping("FechaUltimaCompra", "FechaUltimaCompra"), New System.Data.Common.DataColumnMapping("UltimoCosto", "UltimoCosto"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT [Articulos x Proveedor].CodigoArticulo, [Articulos x Proveedor].CodigoProv" & _
        "eedor, [Articulos x Proveedor].FechaUltimaCompra, [Articulos x Proveedor].Ultimo" & _
        "Costo, Moneda.Simbolo, Proveedores.Nombre FROM [Articulos x Proveedor] INNER JOI" & _
        "N Moneda ON [Articulos x Proveedor].Moneda = Moneda.CodMoneda INNER JOIN Proveed" & _
        "ores ON [Articulos x Proveedor].CodigoProveedor = Proveedores.CodigoProv"
        Me.SqlSelectCommand8.Connection = Me.SqlConnection1
        '
        'colFechaUltimaCompra
        '
        Me.colFechaUltimaCompra.Caption = "Fecha Compra"
        Me.colFechaUltimaCompra.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.colFechaUltimaCompra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colFechaUltimaCompra.FieldName = "FechaUltimaCompra"
        Me.colFechaUltimaCompra.Name = "colFechaUltimaCompra"
        Me.colFechaUltimaCompra.Visible = True
        Me.colFechaUltimaCompra.Width = 48
        '
        'colCodigoProveedor
        '
        Me.colCodigoProveedor.Caption = "Proveedor"
        Me.colCodigoProveedor.FieldName = "CodigoProveedor"
        Me.colCodigoProveedor.Name = "colCodigoProveedor"
        Me.colCodigoProveedor.Visible = True
        Me.colCodigoProveedor.Width = 205
        '
        'colUltimoCosto
        '
        Me.colUltimoCosto.Caption = "Costo"
        Me.colUltimoCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colUltimoCosto.FieldName = "CodigoArticulo"
        Me.colUltimoCosto.Name = "colUltimoCosto"
        Me.colUltimoCosto.Visible = True
        Me.colUltimoCosto.Width = 63
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Columns.Add(Me.colCodigoProveedor)
        Me.GridBand1.Columns.Add(Me.colFechaUltimaCompra)
        Me.GridBand1.Columns.Add(Me.colUltimoCosto)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.Visible = False
        Me.GridBand1.Width = 10
        '
        'Text_Cod_AUX
        '
        Me.Text_Cod_AUX.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario2.Codigo"))
        Me.Text_Cod_AUX.Location = New System.Drawing.Point(380, 500)
        Me.Text_Cod_AUX.Name = "Text_Cod_AUX"
        Me.Text_Cod_AUX.Size = New System.Drawing.Size(64, 20)
        Me.Text_Cod_AUX.TabIndex = 61
        Me.Text_Cod_AUX.Text = ""
        '
        'Adaptador_Inventraio_AUX
        '
        Me.Adaptador_Inventraio_AUX.DeleteCommand = Me.SqlDeleteCommand3
        Me.Adaptador_Inventraio_AUX.InsertCommand = Me.SqlInsertCommand3
        Me.Adaptador_Inventraio_AUX.SelectCommand = Me.SqlSelectCommand9
        Me.Adaptador_Inventraio_AUX.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Inventario", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo")})})
        Me.Adaptador_Inventraio_AUX.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Inventario WHERE (Codigo = @Original_Codigo)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Inventario(Codigo) VALUES (@Codigo); SELECT Codigo FROM Inventario WH" & _
        "ERE (Codigo = @Codigo) ORDER BY Codigo"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT Codigo FROM Inventario ORDER BY Codigo"
        Me.SqlSelectCommand9.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Inventario SET Codigo = @Codigo WHERE (Codigo = @Original_Codigo); SELECT " & _
        "Codigo FROM Inventario WHERE (Codigo = @Codigo) ORDER BY Codigo"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing))
        '
        'Ck_Consignacion
        '
        Me.Ck_Consignacion.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Consignacion"))
        Me.Ck_Consignacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ck_Consignacion.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Ck_Consignacion.Location = New System.Drawing.Point(8, 16)
        Me.Ck_Consignacion.Name = "Ck_Consignacion"
        Me.Ck_Consignacion.Size = New System.Drawing.Size(152, 16)
        Me.Ck_Consignacion.TabIndex = 2
        Me.Ck_Consignacion.Text = "Consignaci�n"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel2.Controls.Add(Me.cmboxProveedor)
        Me.Panel2.Controls.Add(Me.Label41)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.ComboBoxFamilia)
        Me.Panel2.Controls.Add(Me.ComboBoxMarca)
        Me.Panel2.Controls.Add(Me.TxtDescripcion)
        Me.Panel2.Controls.Add(Me.TxtCanPresentacion)
        Me.Panel2.Controls.Add(Me.TxtBarras)
        Me.Panel2.Controls.Add(Me.ComboBoxPresentacion)
        Me.Panel2.Controls.Add(Me.ComboBox_Ubicacion_SubUbicacion)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.lblCodigoProveedor)
        Me.Panel2.Location = New System.Drawing.Point(0, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(624, 136)
        Me.Panel2.TabIndex = 3
        '
        'cmboxProveedor
        '
        Me.cmboxProveedor.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.Proveedor"))
        Me.cmboxProveedor.DataSource = Me.DataSetInventario
        Me.cmboxProveedor.DisplayMember = "Proveedores.Nombre"
        Me.cmboxProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmboxProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.cmboxProveedor.ForeColor = System.Drawing.Color.Blue
        Me.cmboxProveedor.ItemHeight = 13
        Me.cmboxProveedor.Location = New System.Drawing.Point(88, 88)
        Me.cmboxProveedor.Name = "cmboxProveedor"
        Me.cmboxProveedor.Size = New System.Drawing.Size(532, 21)
        Me.cmboxProveedor.TabIndex = 12
        Me.cmboxProveedor.ValueMember = "Proveedores.CodigoProv"
        '
        'Label41
        '
        Me.Label41.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label41.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label41.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label41.Location = New System.Drawing.Point(4, 88)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(80, 16)
        Me.Label41.TabIndex = 11
        Me.Label41.Text = "Proveedor "
        '
        'lblCodigoProveedor
        '
        Me.lblCodigoProveedor.Location = New System.Drawing.Point(444, 116)
        Me.lblCodigoProveedor.Name = "lblCodigoProveedor"
        Me.lblCodigoProveedor.Size = New System.Drawing.Size(100, 16)
        Me.lblCodigoProveedor.TabIndex = 29
        Me.lblCodigoProveedor.Text = "Label42"
        Me.lblCodigoProveedor.Visible = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "C�digo"
        '
        'AdapterBitacora
        '
        Me.AdapterBitacora.DeleteCommand = Me.SqlDeleteCommand5
        Me.AdapterBitacora.InsertCommand = Me.SqlInsertCommand5
        Me.AdapterBitacora.SelectCommand = Me.SqlSelectCommand11
        Me.AdapterBitacora.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Bitacora", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Secuencia", "Secuencia"), New System.Data.Common.DataColumnMapping("Tabla", "Tabla"), New System.Data.Common.DataColumnMapping("Campo_Clave", "Campo_Clave"), New System.Data.Common.DataColumnMapping("DescripcionCampo", "DescripcionCampo"), New System.Data.Common.DataColumnMapping("Accion", "Accion"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Costo", "Costo"), New System.Data.Common.DataColumnMapping("VentaA", "VentaA"), New System.Data.Common.DataColumnMapping("VentaB", "VentaB"), New System.Data.Common.DataColumnMapping("VentaC", "VentaC"), New System.Data.Common.DataColumnMapping("VentaD", "VentaD")})})
        Me.AdapterBitacora.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Bitacora WHERE (Secuencia = @Original_Secuencia) AND (Accion = @Origi" & _
        "nal_Accion) AND (Campo_Clave = @Original_Campo_Clave) AND (Costo = @Original_Cos" & _
        "to) AND (DescripcionCampo = @Original_DescripcionCampo) AND (Fecha = @Original_F" & _
        "echa) AND (Tabla = @Original_Tabla) AND (Usuario = @Original_Usuario) AND (Venta" & _
        "A = @Original_VentaA) AND (VentaB = @Original_VentaB) AND (VentaC = @Original_Ve" & _
        "ntaC) AND (VentaD = @Original_VentaD)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Secuencia", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Secuencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Accion", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Accion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Campo_Clave", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Campo_Clave", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Costo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Costo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionCampo", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCampo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tabla", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tabla", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VentaA", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VentaA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VentaB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VentaB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VentaC", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VentaC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VentaD", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VentaD", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO Bitacora(Tabla, Campo_Clave, DescripcionCampo, Accion, Fecha, Usuario" & _
        ", Costo, VentaA, VentaB, VentaC, VentaD) VALUES (@Tabla, @Campo_Clave, @Descripc" & _
        "ionCampo, @Accion, @Fecha, @Usuario, @Costo, @VentaA, @VentaB, @VentaC, @VentaD)" & _
        "; SELECT Secuencia, Tabla, Campo_Clave, DescripcionCampo, Accion, Fecha, Usuario" & _
        ", Costo, VentaA, VentaB, VentaC, VentaD FROM Bitacora WHERE (Secuencia = @@IDENT" & _
        "ITY)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tabla", System.Data.SqlDbType.VarChar, 75, "Tabla"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Campo_Clave", System.Data.SqlDbType.VarChar, 75, "Campo_Clave"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionCampo", System.Data.SqlDbType.VarChar, 250, "DescripcionCampo"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 75, "Accion"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 150, "Usuario"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 8, "Costo"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VentaA", System.Data.SqlDbType.Float, 8, "VentaA"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VentaB", System.Data.SqlDbType.Float, 8, "VentaB"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VentaC", System.Data.SqlDbType.Float, 8, "VentaC"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VentaD", System.Data.SqlDbType.Float, 8, "VentaD"))
        '
        'SqlSelectCommand11
        '
        Me.SqlSelectCommand11.CommandText = "SELECT Secuencia, Tabla, Campo_Clave, DescripcionCampo, Accion, Fecha, Usuario, C" & _
        "osto, VentaA, VentaB, VentaC, VentaD FROM Bitacora"
        Me.SqlSelectCommand11.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE Bitacora SET Tabla = @Tabla, Campo_Clave = @Campo_Clave, DescripcionCampo " & _
        "= @DescripcionCampo, Accion = @Accion, Fecha = @Fecha, Usuario = @Usuario, Costo" & _
        " = @Costo, VentaA = @VentaA, VentaB = @VentaB, VentaC = @VentaC, VentaD = @Venta" & _
        "D WHERE (Secuencia = @Original_Secuencia) AND (Accion = @Original_Accion) AND (C" & _
        "ampo_Clave = @Original_Campo_Clave) AND (Costo = @Original_Costo) AND (Descripci" & _
        "onCampo = @Original_DescripcionCampo) AND (Fecha = @Original_Fecha) AND (Tabla =" & _
        " @Original_Tabla) AND (Usuario = @Original_Usuario) AND (VentaA = @Original_Vent" & _
        "aA) AND (VentaB = @Original_VentaB) AND (VentaC = @Original_VentaC) AND (VentaD " & _
        "= @Original_VentaD); SELECT Secuencia, Tabla, Campo_Clave, DescripcionCampo, Acc" & _
        "ion, Fecha, Usuario, Costo, VentaA, VentaB, VentaC, VentaD FROM Bitacora WHERE (" & _
        "Secuencia = @Secuencia)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tabla", System.Data.SqlDbType.VarChar, 75, "Tabla"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Campo_Clave", System.Data.SqlDbType.VarChar, 75, "Campo_Clave"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionCampo", System.Data.SqlDbType.VarChar, 250, "DescripcionCampo"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 75, "Accion"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 150, "Usuario"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Costo", System.Data.SqlDbType.Float, 8, "Costo"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VentaA", System.Data.SqlDbType.Float, 8, "VentaA"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VentaB", System.Data.SqlDbType.Float, 8, "VentaB"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VentaC", System.Data.SqlDbType.Float, 8, "VentaC"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VentaD", System.Data.SqlDbType.Float, 8, "VentaD"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Secuencia", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Secuencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Accion", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Accion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Campo_Clave", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Campo_Clave", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Costo", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Costo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionCampo", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionCampo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tabla", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tabla", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 150, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VentaA", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VentaA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VentaB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VentaB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VentaC", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VentaC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VentaD", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VentaD", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Secuencia", System.Data.SqlDbType.BigInt, 8, "Secuencia"))
        '
        'daProveedores
        '
        Me.daProveedores.SelectCommand = Me.SqlSelectCommand12
        Me.daProveedores.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Proveedores", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoProv", "CodigoProv"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlSelectCommand12
        '
        Me.SqlSelectCommand12.CommandText = "SELECT CodigoProv, Nombre FROM Proveedores"
        Me.SqlSelectCommand12.Connection = Me.SqlConnection1
        '
        'ckVerCodBarraInv
        '
        Me.ckVerCodBarraInv.Location = New System.Drawing.Point(500, 428)
        Me.ckVerCodBarraInv.Name = "ckVerCodBarraInv"
        Me.ckVerCodBarraInv.Size = New System.Drawing.Size(124, 16)
        Me.ckVerCodBarraInv.TabIndex = 69
        Me.ckVerCodBarraInv.Text = "Verificar C�d. Barra"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.txtPorSugerido)
        Me.GroupBox5.Controls.Add(Me.Label43)
        Me.GroupBox5.Controls.Add(Me.txtVentaSugerido)
        Me.GroupBox5.Controls.Add(Me.Label44)
        Me.GroupBox5.Controls.Add(Me.txtSugeridoIV)
        Me.GroupBox5.Controls.Add(Me.Label45)
        Me.GroupBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox5.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox5.Location = New System.Drawing.Point(352, 360)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(272, 52)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Precio Venta Sugerido:"
        '
        'txtPorSugerido
        '
        Me.txtPorSugerido.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPorSugerido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorSugerido.ForeColor = System.Drawing.Color.Blue
        Me.txtPorSugerido.Location = New System.Drawing.Point(16, 32)
        Me.txtPorSugerido.Name = "txtPorSugerido"
        Me.txtPorSugerido.Size = New System.Drawing.Size(48, 13)
        Me.txtPorSugerido.TabIndex = 1
        Me.txtPorSugerido.Text = "0.00"
        Me.txtPorSugerido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label43.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label43.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label43.Location = New System.Drawing.Point(96, 16)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(64, 12)
        Me.Label43.TabIndex = 2
        Me.Label43.Text = "Venta Sug."
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtVentaSugerido
        '
        Me.txtVentaSugerido.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVentaSugerido.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Precio_Sugerido"))
        Me.txtVentaSugerido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVentaSugerido.ForeColor = System.Drawing.Color.Blue
        Me.txtVentaSugerido.Location = New System.Drawing.Point(88, 32)
        Me.txtVentaSugerido.Name = "txtVentaSugerido"
        Me.txtVentaSugerido.Size = New System.Drawing.Size(72, 13)
        Me.txtVentaSugerido.TabIndex = 3
        Me.txtVentaSugerido.Text = "0.00"
        Me.txtVentaSugerido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label44
        '
        Me.Label44.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label44.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label44.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label44.Location = New System.Drawing.Point(184, 16)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(72, 12)
        Me.Label44.TabIndex = 4
        Me.Label44.Text = "Sug. + I.V."
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtSugeridoIV
        '
        Me.txtSugeridoIV.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSugeridoIV.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.SugeridoIV"))
        Me.txtSugeridoIV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSugeridoIV.ForeColor = System.Drawing.Color.Blue
        Me.txtSugeridoIV.Location = New System.Drawing.Point(184, 32)
        Me.txtSugeridoIV.Name = "txtSugeridoIV"
        Me.txtSugeridoIV.Size = New System.Drawing.Size(72, 13)
        Me.txtSugeridoIV.TabIndex = 5
        Me.txtSugeridoIV.Text = "0.00"
        Me.txtSugeridoIV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label45
        '
        Me.Label45.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label45.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label45.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label45.Location = New System.Drawing.Point(16, 16)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(52, 12)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Porc."
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.Label7)
        Me.GroupBox6.Controls.Add(Me.Ck_Consignacion)
        Me.GroupBox6.Controls.Add(Me.ComboBoxBodegas)
        Me.GroupBox6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox6.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox6.Location = New System.Drawing.Point(4, 456)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(336, 68)
        Me.GroupBox6.TabIndex = 8
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Bodega"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(264, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 14)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Existencia"
        '
        'AdapterLote
        '
        Me.AdapterLote.SelectCommand = Me.SqlSelectCommand13
        Me.AdapterLote.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Lotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Numero", "Numero"), New System.Data.Common.DataColumnMapping("Vencimiento", "Vencimiento"), New System.Data.Common.DataColumnMapping("Cant_Actual", "Cant_Actual"), New System.Data.Common.DataColumnMapping("Cod_Articulo", "Cod_Articulo"), New System.Data.Common.DataColumnMapping("Id", "Id")})})
        '
        'SqlSelectCommand13
        '
        Me.SqlSelectCommand13.CommandText = "SELECT Numero, Vencimiento, Cant_Actual, Cod_Articulo, Id FROM Lotes"
        Me.SqlSelectCommand13.Connection = Me.SqlConnection1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.TxtCostoEquivalente)
        Me.TabPage1.Controls.Add(Me.TxtOtrosCargosEquivalente)
        Me.TabPage1.Controls.Add(Me.TxtFleteEquivalente)
        Me.TabPage1.Controls.Add(Me.TextBoxValorMonedaEnCosto)
        Me.TabPage1.Controls.Add(Me.TxtBaseEquivalente)
        Me.TabPage1.Controls.Add(Me.TxtImpuesto)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.TxtFlete)
        Me.TabPage1.Controls.Add(Me.TxtOtros)
        Me.TabPage1.Controls.Add(Me.TxtCosto)
        Me.TabPage1.Controls.Add(Me.ComboBoxMoneda)
        Me.TabPage1.Controls.Add(Me.CheckBox1)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.TxtBase)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(331, 146)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "�ltimo Costo"
        '
        'TxtCostoEquivalente
        '
        Me.TxtCostoEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtCostoEquivalente.Enabled = False
        Me.TxtCostoEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtCostoEquivalente.ForeColor = System.Drawing.Color.Blue
        Me.TxtCostoEquivalente.Location = New System.Drawing.Point(184, 114)
        Me.TxtCostoEquivalente.Name = "TxtCostoEquivalente"
        Me.TxtCostoEquivalente.ReadOnly = True
        Me.TxtCostoEquivalente.Size = New System.Drawing.Size(84, 13)
        Me.TxtCostoEquivalente.TabIndex = 0
        Me.TxtCostoEquivalente.Text = "0.00"
        Me.TxtCostoEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtOtrosCargosEquivalente
        '
        Me.TxtOtrosCargosEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtOtrosCargosEquivalente.Enabled = False
        Me.TxtOtrosCargosEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtOtrosCargosEquivalente.ForeColor = System.Drawing.Color.Blue
        Me.TxtOtrosCargosEquivalente.Location = New System.Drawing.Point(184, 92)
        Me.TxtOtrosCargosEquivalente.Name = "TxtOtrosCargosEquivalente"
        Me.TxtOtrosCargosEquivalente.ReadOnly = True
        Me.TxtOtrosCargosEquivalente.Size = New System.Drawing.Size(84, 13)
        Me.TxtOtrosCargosEquivalente.TabIndex = 14
        Me.TxtOtrosCargosEquivalente.Text = "0.00"
        Me.TxtOtrosCargosEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtFleteEquivalente
        '
        Me.TxtFleteEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtFleteEquivalente.Enabled = False
        Me.TxtFleteEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtFleteEquivalente.ForeColor = System.Drawing.Color.Blue
        Me.TxtFleteEquivalente.Location = New System.Drawing.Point(184, 71)
        Me.TxtFleteEquivalente.Name = "TxtFleteEquivalente"
        Me.TxtFleteEquivalente.ReadOnly = True
        Me.TxtFleteEquivalente.Size = New System.Drawing.Size(84, 13)
        Me.TxtFleteEquivalente.TabIndex = 11
        Me.TxtFleteEquivalente.Text = "0.00"
        Me.TxtFleteEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBoxValorMonedaEnCosto
        '
        Me.TextBoxValorMonedaEnCosto.AcceptsTab = True
        Me.TextBoxValorMonedaEnCosto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxValorMonedaEnCosto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Moneda.ValorCompra"))
        Me.TextBoxValorMonedaEnCosto.Enabled = False
        Me.TextBoxValorMonedaEnCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TextBoxValorMonedaEnCosto.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxValorMonedaEnCosto.Location = New System.Drawing.Point(187, 11)
        Me.TextBoxValorMonedaEnCosto.Name = "TextBoxValorMonedaEnCosto"
        Me.TextBoxValorMonedaEnCosto.ReadOnly = True
        Me.TextBoxValorMonedaEnCosto.Size = New System.Drawing.Size(80, 13)
        Me.TextBoxValorMonedaEnCosto.TabIndex = 3
        Me.TextBoxValorMonedaEnCosto.Text = "0.00"
        Me.TextBoxValorMonedaEnCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtBaseEquivalente
        '
        Me.TxtBaseEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtBaseEquivalente.Enabled = False
        Me.TxtBaseEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TxtBaseEquivalente.ForeColor = System.Drawing.Color.Blue
        Me.TxtBaseEquivalente.Location = New System.Drawing.Point(184, 51)
        Me.TxtBaseEquivalente.Name = "TxtBaseEquivalente"
        Me.TxtBaseEquivalente.ReadOnly = True
        Me.TxtBaseEquivalente.Size = New System.Drawing.Size(84, 13)
        Me.TxtBaseEquivalente.TabIndex = 8
        Me.TxtBaseEquivalente.Text = "0.00"
        Me.TxtBaseEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtImpuesto
        '
        Me.TxtImpuesto.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.IVenta"))
        Me.TxtImpuesto.EditValue = "0.00"
        Me.TxtImpuesto.Location = New System.Drawing.Point(88, 31)
        Me.TxtImpuesto.Name = "TxtImpuesto"
        '
        'TxtImpuesto.Properties
        '
        Me.TxtImpuesto.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtImpuesto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtImpuesto.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtImpuesto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtImpuesto.Properties.ReadOnly = True
        Me.TxtImpuesto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtImpuesto.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtImpuesto.Size = New System.Drawing.Size(80, 19)
        Me.TxtImpuesto.TabIndex = 5
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(14, 7)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 16)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Moneda"
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(8, 110)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(72, 12)
        Me.Label18.TabIndex = 15
        Me.Label18.Text = "Costo"
        '
        'TxtFlete
        '
        Me.TxtFlete.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Fletes"))
        Me.TxtFlete.EditValue = "0.00"
        Me.TxtFlete.Location = New System.Drawing.Point(88, 71)
        Me.TxtFlete.Name = "TxtFlete"
        '
        'TxtFlete.Properties
        '
        Me.TxtFlete.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtFlete.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtFlete.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtFlete.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtFlete.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtFlete.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtFlete.Size = New System.Drawing.Size(80, 19)
        Me.TxtFlete.TabIndex = 10
        '
        'TxtOtros
        '
        Me.TxtOtros.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.OtrosCargos"))
        Me.TxtOtros.EditValue = "0.00"
        Me.TxtOtros.Location = New System.Drawing.Point(88, 90)
        Me.TxtOtros.Name = "TxtOtros"
        '
        'TxtOtros.Properties
        '
        Me.TxtOtros.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtOtros.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtOtros.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtOtros.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtOtros.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtOtros.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtOtros.Size = New System.Drawing.Size(80, 19)
        Me.TxtOtros.TabIndex = 13
        '
        'TxtCosto
        '
        Me.TxtCosto.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.Costo"))
        Me.TxtCosto.EditValue = "0.00"
        Me.TxtCosto.Location = New System.Drawing.Point(88, 110)
        Me.TxtCosto.Name = "TxtCosto"
        '
        'TxtCosto.Properties
        '
        Me.TxtCosto.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtCosto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtCosto.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtCosto.Properties.Enabled = False
        Me.TxtCosto.Properties.ReadOnly = True
        Me.TxtCosto.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtCosto.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtCosto.Size = New System.Drawing.Size(80, 19)
        Me.TxtCosto.TabIndex = 16
        '
        'ComboBoxMoneda
        '
        Me.ComboBoxMoneda.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetInventario, "Inventario.MonedaCosto"))
        Me.ComboBoxMoneda.DataSource = Me.DataSetInventario
        Me.ComboBoxMoneda.DisplayMember = "Moneda.MonedaNombre"
        Me.ComboBoxMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxMoneda.ForeColor = System.Drawing.Color.Blue
        Me.ComboBoxMoneda.ItemHeight = 13
        Me.ComboBoxMoneda.Location = New System.Drawing.Point(88, 7)
        Me.ComboBoxMoneda.Name = "ComboBoxMoneda"
        Me.ComboBoxMoneda.Size = New System.Drawing.Size(84, 21)
        Me.ComboBoxMoneda.TabIndex = 1
        Me.ComboBoxMoneda.ValueMember = "Moneda.CodMoneda"
        '
        'CheckBox1
        '
        Me.CheckBox1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CheckBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.CheckBox1.Location = New System.Drawing.Point(8, 31)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox1.TabIndex = 4
        Me.CheckBox1.Text = "Exento"
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(8, 52)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 12)
        Me.Label17.TabIndex = 6
        Me.Label17.Text = "Base"
        '
        'TxtBase
        '
        Me.TxtBase.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetInventario, "Inventario.PrecioBase"))
        Me.TxtBase.EditValue = "0.00"
        Me.TxtBase.Location = New System.Drawing.Point(88, 51)
        Me.TxtBase.Name = "TxtBase"
        '
        'TxtBase.Properties
        '
        Me.TxtBase.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtBase.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtBase.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtBase.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtBase.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtBase.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtBase.Size = New System.Drawing.Size(80, 19)
        Me.TxtBase.TabIndex = 7
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(8, 71)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(72, 12)
        Me.Label16.TabIndex = 9
        Me.Label16.Text = "Fletes"
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label15.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 90)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(72, 12)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Otro Cargo"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Ck_Lote)
        Me.TabPage4.Controls.Add(Me.Ck_PreguntaPrecio)
        Me.TabPage4.Controls.Add(Me.Check_Inhabilitado)
        Me.TabPage4.Controls.Add(Me.Check_Servicio)
        Me.TabPage4.Controls.Add(Me.Label34)
        Me.TabPage4.Controls.Add(Me.Hasta)
        Me.TabPage4.Controls.Add(Me.Label33)
        Me.TabPage4.Controls.Add(Me.Check_Promo)
        Me.TabPage4.Controls.Add(Me.Desde)
        Me.TabPage4.Controls.Add(Me.Label37)
        Me.TabPage4.Controls.Add(Me.TxtMaxDesc)
        Me.TabPage4.Controls.Add(Me.Label38)
        Me.TabPage4.Controls.Add(Me.Label39)
        Me.TabPage4.Controls.Add(Me.TextBox4)
        Me.TabPage4.Controls.Add(Me.Label40)
        Me.TabPage4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(331, 146)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Varios"
        '
        'Ck_Lote
        '
        Me.Ck_Lote.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Lote"))
        Me.Ck_Lote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ck_Lote.Location = New System.Drawing.Point(216, 72)
        Me.Ck_Lote.Name = "Ck_Lote"
        Me.Ck_Lote.Size = New System.Drawing.Size(112, 16)
        Me.Ck_Lote.TabIndex = 86
        Me.Ck_Lote.Text = "Lote"
        '
        'Ck_PreguntaPrecio
        '
        Me.Ck_PreguntaPrecio.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.PreguntaPrecio"))
        Me.Ck_PreguntaPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ck_PreguntaPrecio.Location = New System.Drawing.Point(216, 88)
        Me.Ck_PreguntaPrecio.Name = "Ck_PreguntaPrecio"
        Me.Ck_PreguntaPrecio.Size = New System.Drawing.Size(112, 16)
        Me.Ck_PreguntaPrecio.TabIndex = 85
        Me.Ck_PreguntaPrecio.Text = "Requiere Precio"
        '
        'Check_Inhabilitado
        '
        Me.Check_Inhabilitado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Inhabilitado"))
        Me.Check_Inhabilitado.Enabled = False
        Me.Check_Inhabilitado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Inhabilitado.Location = New System.Drawing.Point(216, 120)
        Me.Check_Inhabilitado.Name = "Check_Inhabilitado"
        Me.Check_Inhabilitado.Size = New System.Drawing.Size(88, 16)
        Me.Check_Inhabilitado.TabIndex = 84
        Me.Check_Inhabilitado.Text = "Inhabilitado"
        '
        'Check_Servicio
        '
        Me.Check_Servicio.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Servicio"))
        Me.Check_Servicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Servicio.Location = New System.Drawing.Point(216, 104)
        Me.Check_Servicio.Name = "Check_Servicio"
        Me.Check_Servicio.Size = New System.Drawing.Size(88, 16)
        Me.Check_Servicio.TabIndex = 83
        Me.Check_Servicio.Text = "Servicio"
        '
        'Label34
        '
        Me.Label34.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label34.Enabled = False
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label34.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label34.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label34.Location = New System.Drawing.Point(169, 32)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(41, 12)
        Me.Label34.TabIndex = 76
        Me.Label34.Text = "Hasta"
        '
        'Hasta
        '
        Me.Hasta.CalendarForeColor = System.Drawing.Color.RoyalBlue
        Me.Hasta.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Promo_Finaliza"))
        Me.Hasta.Enabled = False
        Me.Hasta.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.Hasta.Location = New System.Drawing.Point(216, 30)
        Me.Hasta.Name = "Hasta"
        Me.Hasta.Size = New System.Drawing.Size(88, 20)
        Me.Hasta.TabIndex = 75
        Me.Hasta.Value = New Date(2010, 1, 18, 11, 52, 19, 600)
        '
        'Label33
        '
        Me.Label33.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label33.Enabled = False
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label33.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label33.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label33.Location = New System.Drawing.Point(168, 8)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(41, 12)
        Me.Label33.TabIndex = 74
        Me.Label33.Text = "Desde"
        '
        'Check_Promo
        '
        Me.Check_Promo.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.Promo_Activa"))
        Me.Check_Promo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Promo.Location = New System.Drawing.Point(48, 16)
        Me.Check_Promo.Name = "Check_Promo"
        Me.Check_Promo.Size = New System.Drawing.Size(108, 20)
        Me.Check_Promo.TabIndex = 72
        Me.Check_Promo.Text = "Promo Activa"
        '
        'Desde
        '
        Me.Desde.CalendarForeColor = System.Drawing.Color.RoyalBlue
        Me.Desde.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Promo_Inicio"))
        Me.Desde.Enabled = False
        Me.Desde.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.Desde.Location = New System.Drawing.Point(216, 8)
        Me.Desde.Name = "Desde"
        Me.Desde.Size = New System.Drawing.Size(88, 20)
        Me.Desde.TabIndex = 73
        Me.Desde.Value = New Date(2010, 1, 18, 11, 52, 19, 640)
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label37.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label37.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label37.Location = New System.Drawing.Point(16, 64)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(104, 12)
        Me.Label37.TabIndex = 77
        Me.Label37.Text = "M�ximo Descuento"
        '
        'TxtMaxDesc
        '
        Me.TxtMaxDesc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtMaxDesc.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Max_Descuento"))
        Me.TxtMaxDesc.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TxtMaxDesc.Location = New System.Drawing.Point(136, 64)
        Me.TxtMaxDesc.Name = "TxtMaxDesc"
        Me.TxtMaxDesc.Size = New System.Drawing.Size(28, 13)
        Me.TxtMaxDesc.TabIndex = 78
        Me.TxtMaxDesc.Text = ""
        '
        'Label38
        '
        Me.Label38.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label38.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label38.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label38.Location = New System.Drawing.Point(168, 64)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(16, 12)
        Me.Label38.TabIndex = 79
        Me.Label38.Text = "%"
        '
        'Label39
        '
        Me.Label39.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label39.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label39.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label39.Location = New System.Drawing.Point(168, 80)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(16, 12)
        Me.Label39.TabIndex = 82
        Me.Label39.Text = "%"
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetInventario, "Inventario.Max_Comision"))
        Me.TextBox4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TextBox4.Location = New System.Drawing.Point(136, 80)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(28, 13)
        Me.TextBox4.TabIndex = 81
        Me.TextBox4.Text = ""
        '
        'Label40
        '
        Me.Label40.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label40.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label40.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label40.Location = New System.Drawing.Point(16, 80)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(104, 12)
        Me.Label40.TabIndex = 80
        Me.Label40.Text = "M�ximo Comisi�n"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GridControl1)
        Me.TabPage2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(331, 146)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Detalle Art�culos por Proveedor"
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "Inventario.InventarioArticulos_x0020_x_x0020_Proveedor"
        Me.GridControl1.DataSource = Me.DataSetInventario
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(328, 144)
        Me.GridControl1.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.Text = "GridControl1"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.GridView1.GroupPanelText = ""
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Fecha"
        Me.GridColumn6.DisplayFormat.FormatString = "d"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn6.FieldName = "FechaUltimaCompra"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.Width = 62
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Id P."
        Me.GridColumn7.FieldName = "CodigoProveedor"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn7.Width = 34
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Proveedor"
        Me.GridColumn8.FieldName = "Nombre"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn8.VisibleIndex = 1
        Me.GridColumn8.Width = 111
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "M"
        Me.GridColumn9.FieldName = "Simbolo"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn9.VisibleIndex = 2
        Me.GridColumn9.Width = 20
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Costo"
        Me.GridColumn10.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn10.FieldName = "UltimoCosto"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn10.VisibleIndex = 0
        Me.GridColumn10.Width = 87
        '
        'TabLotes
        '
        Me.TabLotes.Controls.Add(Me.GridControl3)
        Me.TabLotes.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabLotes.Location = New System.Drawing.Point(4, 22)
        Me.TabLotes.Name = "TabLotes"
        Me.TabLotes.Size = New System.Drawing.Size(331, 146)
        Me.TabLotes.TabIndex = 5
        Me.TabLotes.Text = "Lotes"
        '
        'GridControl3
        '
        Me.GridControl3.DataMember = "Inventario.InventarioLotes"
        Me.GridControl3.DataSource = Me.DataSetInventario
        '
        'GridControl3.EmbeddedNavigator
        '
        Me.GridControl3.EmbeddedNavigator.Name = ""
        Me.GridControl3.Location = New System.Drawing.Point(8, 8)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(320, 136)
        Me.GridControl3.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl3.TabIndex = 0
        Me.GridControl3.Text = "GridControl3"
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Lote"
        Me.GridColumn1.FieldName = "Numero"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Vencimiento"
        Me.GridColumn2.DisplayFormat.FormatString = "d"
        Me.GridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn2.FieldName = "Vencimiento"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.VisibleIndex = 0
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Existencia"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn3.FieldName = "Cant_Actual"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.PictureEdit1)
        Me.TabPage3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(331, 146)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Imagen"
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Location = New System.Drawing.Point(56, 8)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Size = New System.Drawing.Size(216, 128)
        Me.PictureEdit1.TabIndex = 72
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabLotes)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TabControl1.ItemSize = New System.Drawing.Size(71, 18)
        Me.TabControl1.Location = New System.Drawing.Point(0, 176)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(339, 172)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.ButtonAgreBodega)
        Me.TabPage5.Controls.Add(Me.ComboBoxBodega)
        Me.TabPage5.Controls.Add(Me.ButtonAgregar)
        Me.TabPage5.Controls.Add(Me.ButtonEliminar)
        Me.TabPage5.Controls.Add(Me.GridControl2)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(331, 146)
        Me.TabPage5.TabIndex = 6
        Me.TabPage5.Text = "Articulos_Bodega"
        '
        'ButtonAgreBodega
        '
        Me.ButtonAgreBodega.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgreBodega.Location = New System.Drawing.Point(272, 4)
        Me.ButtonAgreBodega.Name = "ButtonAgreBodega"
        Me.ButtonAgreBodega.Size = New System.Drawing.Size(24, 16)
        Me.ButtonAgreBodega.TabIndex = 9
        Me.ButtonAgreBodega.Text = "+"
        Me.ButtonAgreBodega.Visible = False
        '
        'ComboBoxBodega
        '
        Me.ComboBoxBodega.DataSource = Me.DataSetInventario.Bodega
        Me.ComboBoxBodega.DisplayMember = "Nombre"
        Me.ComboBoxBodega.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBodega.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxBodega.Location = New System.Drawing.Point(88, 4)
        Me.ComboBoxBodega.Name = "ComboBoxBodega"
        Me.ComboBoxBodega.Size = New System.Drawing.Size(184, 21)
        Me.ComboBoxBodega.TabIndex = 8
        Me.ComboBoxBodega.ValueMember = "Id"
        Me.ComboBoxBodega.Visible = False
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgregar.Location = New System.Drawing.Point(24, 4)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(24, 16)
        Me.ButtonAgregar.TabIndex = 7
        Me.ButtonAgregar.Text = "+"
        '
        'ButtonEliminar
        '
        Me.ButtonEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminar.ForeColor = System.Drawing.Color.Crimson
        Me.ButtonEliminar.Location = New System.Drawing.Point(56, 4)
        Me.ButtonEliminar.Name = "ButtonEliminar"
        Me.ButtonEliminar.Size = New System.Drawing.Size(24, 16)
        Me.ButtonEliminar.TabIndex = 6
        Me.ButtonEliminar.Text = "X"
        Me.ButtonEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "Inventario.InventarioArticulosXBodega"
        Me.GridControl2.DataSource = Me.DataSetInventario
        '
        'GridControl2.EmbeddedNavigator
        '
        Me.GridControl2.EmbeddedNavigator.Name = ""
        Me.GridControl2.Location = New System.Drawing.Point(0, 28)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(332, 124)
        Me.GridControl2.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.TabIndex = 1
        Me.GridControl2.Text = "GridControl2"
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Nombre"
        Me.GridColumn4.FieldName = "Nombre"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.VisibleIndex = 0
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Existencia"
        Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn5.FieldName = "Existencia"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.VisibleIndex = 1
        '
        'colCodigoProveedor1
        '
        Me.colCodigoProveedor1.Caption = "Id P."
        Me.colCodigoProveedor1.FieldName = "CodigoProveedor"
        Me.colCodigoProveedor1.Name = "colCodigoProveedor1"
        Me.colCodigoProveedor1.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCodigoProveedor1.VisibleIndex = 1
        Me.colCodigoProveedor1.Width = 34
        '
        'colLote
        '
        Me.colLote.Caption = "Lote"
        Me.colLote.FieldName = "Numero"
        Me.colLote.Name = "colLote"
        Me.colLote.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colLote.VisibleIndex = 0
        '
        'colCant_Actual
        '
        Me.colCant_Actual.Caption = "Existencia"
        Me.colCant_Actual.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colCant_Actual.FieldName = "Cant_Actual"
        Me.colCant_Actual.Name = "colCant_Actual"
        Me.colCant_Actual.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCant_Actual.VisibleIndex = 2
        '
        'chpeso
        '
        Me.chpeso.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetInventario, "Inventario.EsPesado"))
        Me.chpeso.Location = New System.Drawing.Point(436, 428)
        Me.chpeso.Name = "chpeso"
        Me.chpeso.Size = New System.Drawing.Size(52, 16)
        Me.chpeso.TabIndex = 70
        Me.chpeso.Text = "Peso"
        Me.chpeso.Visible = False
        '
        'FrmInventario
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(630, 476)
        Me.Controls.Add(Me.chpeso)
        Me.Controls.Add(Me.ckVerCodBarraInv)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TxtCodigo)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.DataNavigator1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Text_Cod_AUX)
        Me.Controls.Add(Me.TxtObservaciones)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.ForeColor = System.Drawing.Color.RoyalBlue
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmInventario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inventarios"
        CType(Me.DataSetInventario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_A.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_B.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_C.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_D.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.TxtUtilidad_D.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUtilidad_C.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUtilidad_B.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_D.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_A.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_C.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_B.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUtilidad_A.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUtilidad_P.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_IV_P.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPrecioVenta_P.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.TxtImpuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFlete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtOtros.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtBase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabLotes.ResumeLayout(False)
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Position_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If Me.Text_Cod_AUX.Text = "" Then Exit Sub
            cargando = True
            If Me.LlenarInventario(CDbl(Me.Text_Cod_AUX.Text)) Then
                cargando = False
                EjecutarCalculos()
            End If
            Application.DoEvents()
            Me.TxtBarras.Focus()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

#Region " Variable "
    '  Dim Salto As Boolean
    Dim cargando As Boolean

#End Region

    Private Sub CargarAdaptadores()
        Try
            Me.Adaptador_Inventraio_AUX.Fill(Me.DataSetInventario, "Inventario2")
            Me.AdapterMarcas.Fill(Me.DataSetInventario, "Marcas")
            Me.AdapterCasaConsignante.Fill(Me.DataSetInventario, "Bodegas")
            Me.AdapterAUbicacion.Fill(Me.DataSetInventario, "Sububicacion")
            Me.AdapterFamilia.Fill(Me.DataSetInventario, "SubFamilias")
            Me.AdapterMoneda.Fill(Me.DataSetInventario, "Moneda")
            Me.AdapterPresentacion.Fill(Me.DataSetInventario, "Presentaciones")
            Me.daProveedores.Fill(Me.DataSetInventario, "Proveedores")
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            PMU = VSM(usua.Cedula, Me.Name)
            Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            CargarAdaptadores()
            Me.DataSetInventario.Inventario.BarrasColumn.DefaultValue = ""
            Me.DataSetInventario.Inventario.SubFamiliaColumn.DefaultValue = DataSetInventario.SubFamilias.Rows(0).Item(0)
            Me.DataSetInventario.Inventario.SubUbicacionColumn.DefaultValue = Me.DataSetInventario.SubUbicacion.Rows(0).Item(0)
            Me.DataSetInventario.Inventario.CodMarcaColumn.DefaultValue = Me.DataSetInventario.Marcas.Rows(0).Item(0)
            Me.DataSetInventario.Inventario.MinimaColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.MaximaColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.PuntoMedioColumn.DefaultValue = 0
            'Me.DataSetInventario.Inventario.ConsignacionColumn.DefaultValue = 0
            'Me.DataSetInventario.Inventario.BodegaConsignacionColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.PrecioBaseColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.OtrosCargosColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.FletesColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.CostoColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.Precio_AColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.Precio_BColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.Precio_CColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.Precio_DColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.IVentaColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.ObservacionesColumn.DefaultValue = ""
            Me.DataSetInventario.Inventario.Promo_ActivaColumn.DefaultValue = False
            Me.DataSetInventario.Inventario.Precio_PromoColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.Promo_InicioColumn.DefaultValue = Now
            Me.DataSetInventario.Inventario.Promo_FinalizaColumn.DefaultValue = Now
            Me.DataSetInventario.Inventario.Max_DescuentoColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.Max_ComisionColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.ServicioColumn.DefaultValue = False
            Me.DataSetInventario.Inventario.InhabilitadoColumn.DefaultValue = False
            Me.DataSetInventario.Inventario.ProveedorColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.Precio_SugeridoColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.SugeridoIVColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.PreguntaPrecioColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.LoteColumn.DefaultValue = False
            Me.DataSetInventario.Inventario.ConsignacionColumn.DefaultValue = False
            Me.DataSetInventario.Inventario.Id_BodegaColumn.DefaultValue = 0
            Me.DataSetInventario.Inventario.ExistenciaBodegaColumn.DefaultValue = 0
            'Me.CenterToScreen()

            Me.TxtImpuesto.Properties.ReadOnly = True

            If PMU.Others Then
                TxtUtilidad_A.Visible = True
                TxtUtilidad_B.Visible = True
                TxtUtilidad_C.Visible = True
                TxtUtilidad_D.Visible = True
                Label29.Visible = True
            Else
                TxtUtilidad_A.Visible = False
                TxtUtilidad_B.Visible = False
                TxtUtilidad_C.Visible = False
                TxtUtilidad_D.Visible = False
                Label29.Visible = False
            End If

            If Me.Text_Cod_AUX.Text = "" Then Exit Sub
            cargando = True
            If Me.LlenarInventario(CDbl(Me.Text_Cod_AUX.Text)) Then
                cargando = False
                EjecutarCalculos()
            End If

            'SAJ 041207 VERIFICA SI SE DEBE DE CHECHEAR EL CODIGO DE BARRAS 
            If GetSetting("SeeSOFT", "SeePOS", "VerCodBarraInv") = "" Then
                SaveSetting("SeeSOFT", "SeePOS", "VerCodBarraInv", "0")
            End If
            Me.ckVerCodBarraInv.Checked = GetSetting("SeeSOFT", "SeePOS", "VerCodBarraInv")
            If GetSetting("SeeSOFT", "SeePOS", "PesaKG").Equals("2") Then
                Me.chpeso.Visible = True
            End If


        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCodigo.KeyPress, TxtCanPresentacion.KeyPress, TxtMed.KeyPress, TxtMax.KeyPress, TxtPrecioVenta_A.KeyPress, TxtPrecioVenta_B.KeyPress, TxtPrecioVenta_C.KeyPress, TxtPrecioVenta_D.KeyPress, TxtUtilidad_D.KeyPress, TxtUtilidad_A.KeyPress, TxtUtilidad_C.KeyPress, TxtUtilidad_B.KeyPress, TxtPrecioVenta_IV_D.KeyPress, TxtPrecioVenta_IV_A.KeyPress, TxtPrecioVenta_IV_C.KeyPress, TxtPrecioVenta_IV_B.KeyPress, TxtExistencia.KeyPress, ComboBoxMonedaVenta.KeyPress, TxtUtilidad_P.KeyPress, TxtPrecioVenta_P.KeyPress, TxtPrecioVenta_IV_P.KeyPress, TxtMin.KeyPress, TxtImpuesto.KeyPress, TxtFlete.KeyPress, TxtOtros.KeyPress, TxtCosto.KeyPress, ComboBoxMoneda.KeyPress, CheckBox1.KeyPress, TxtBase.KeyPress
        'REALIZA LOS CALCULOS PARA LAS DIFERENTES CAJAS DE TEXTO DEPENDIENDO DEL OBJETO ENTRANTE. SAJ 
        Try
            ' If cargando = True Then Exit Sub

            If Keys.Enter = Asc(e.KeyChar) Then
                Select Case sender.Name
                    Case TxtBase.Name, TxtFlete.Name, TxtOtros.Name
                        If TxtBase.Text = "" Then TxtBase.Text = 0
                        If TxtFlete.Text = "" Then TxtFlete.Text = 0
                        If TxtOtros.Text = "" Then TxtOtros.Text = 0
                        TxtCosto.Text = (CDbl(TxtBase.Text) + CDbl(TxtFlete.Text) + CDbl(TxtOtros.Text))
                        CalcularEquivalenciaMoneda()
                        'calculo de precios por la utilidad
                    Case TxtUtilidad_A.Name
                        If TxtUtilidad_A.Text = "" Then TxtUtilidad_A.Text = 0
                        TxtPrecioVenta_A.Text = TxtBaseEquivalente.Text * (TxtUtilidad_A.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                        TxtPrecioVenta_IV_A.Text = TxtPrecioVenta_A.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_A.Text

                    Case TxtUtilidad_B.Name
                        If TxtUtilidad_B.Text = "" Then TxtUtilidad_B.Text = 0
                        TxtPrecioVenta_B.Text = TxtBaseEquivalente.Text * (TxtUtilidad_B.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                        TxtPrecioVenta_IV_B.Text = TxtPrecioVenta_B.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_B.Text

                    Case TxtUtilidad_C.Name
                        If TxtUtilidad_C.Text = "" Then TxtUtilidad_C.Text = 0
                        TxtPrecioVenta_C.Text = TxtBaseEquivalente.Text * (TxtUtilidad_C.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                        TxtPrecioVenta_IV_C.Text = TxtPrecioVenta_C.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_C.Text

                    Case TxtUtilidad_D.Name
                        If TxtUtilidad_D.Text = "" Then TxtUtilidad_D.Text = 0
                        TxtPrecioVenta_D.Text = TxtBaseEquivalente.Text * (TxtUtilidad_D.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                        TxtPrecioVenta_IV_D.Text = TxtPrecioVenta_D.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_D.Text

                    Case TxtUtilidad_P.Name
                        If TxtUtilidad_P.Text = "" Then TxtUtilidad_P.Text = 0
                        TxtPrecioVenta_P.Text = TxtBaseEquivalente.Text * (TxtUtilidad_P.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                        TxtPrecioVenta_IV_P.Text = TxtPrecioVenta_P.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_P.Text


                        'calculo de de utilidad por el precio A y calculo del precio com I.V.
                    Case TxtPrecioVenta_A.Name
                        If TxtPrecioVenta_A.Text = "" Then TxtPrecioVenta_A.Text = 0
                        TxtUtilidad_A.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_A.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                        TxtPrecioVenta_IV_A.Text = TxtPrecioVenta_A.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_A.Text

                    Case TxtPrecioVenta_B.Name
                        If TxtPrecioVenta_B.Text = "" Then TxtPrecioVenta_B.Text = 0
                        TxtUtilidad_B.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_B.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                        TxtPrecioVenta_IV_B.Text = TxtPrecioVenta_B.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_B.Text

                    Case TxtPrecioVenta_C.Name
                        If TxtPrecioVenta_C.Text = "" Then TxtPrecioVenta_C.Text = 0
                        TxtUtilidad_C.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_C.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                        TxtPrecioVenta_IV_C.Text = TxtPrecioVenta_C.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_C.Text

                    Case TxtPrecioVenta_D.Name
                        If TxtPrecioVenta_D.Text = "" Then TxtPrecioVenta_D.Text = 0
                        TxtUtilidad_D.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_D.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                        TxtPrecioVenta_IV_D.Text = TxtPrecioVenta_D.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_D.Text

                    Case TxtPrecioVenta_P.Name
                        If TxtPrecioVenta_P.Text = "" Then TxtPrecioVenta_P.Text = 0
                        TxtUtilidad_P.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_P.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                        TxtPrecioVenta_IV_P.Text = TxtPrecioVenta_P.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_P.Text

                        'CALCULO DE PRECIO DE VENTA Y RECALCULAR LA  UTILIDAD CON BASE A LOS NUEVOS PRECIOS 
                    Case TxtPrecioVenta_IV_A.Name
                        If TxtPrecioVenta_IV_A.Text = "" Then TxtPrecioVenta_IV_A.Text = 0
                        TxtPrecioVenta_A.Text = TxtPrecioVenta_IV_A.Text / (1 + (TxtImpuesto.Text / 100))
                        TxtUtilidad_A.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_A.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

                    Case TxtPrecioVenta_IV_B.Name
                        If TxtPrecioVenta_IV_B.Text = "" Then TxtPrecioVenta_IV_B.Text = 0
                        TxtPrecioVenta_B.Text = TxtPrecioVenta_IV_B.Text / (1 + (TxtImpuesto.Text / 100))
                        TxtUtilidad_B.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_B.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

                    Case TxtPrecioVenta_IV_C.Name
                        If TxtPrecioVenta_IV_C.Text = "" Then TxtPrecioVenta_IV_C.Text = 0
                        TxtPrecioVenta_C.Text = TxtPrecioVenta_IV_C.Text / (1 + (TxtImpuesto.Text / 100))
                        TxtUtilidad_C.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_C.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

                    Case TxtPrecioVenta_IV_D.Name
                        If TxtPrecioVenta_IV_D.Text = "" Then TxtPrecioVenta_IV_D.Text = 0
                        TxtPrecioVenta_D.Text = TxtPrecioVenta_IV_D.Text / (1 + (TxtImpuesto.Text / 100))
                        TxtUtilidad_D.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_D.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

                    Case TxtPrecioVenta_IV_P.Name
                        If TxtPrecioVenta_IV_P.Text = "" Then TxtPrecioVenta_IV_P.Text = 0
                        TxtPrecioVenta_P.Text = TxtPrecioVenta_IV_P.Text / (1 + (TxtImpuesto.Text / 100))
                        TxtUtilidad_P.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_P.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                End Select

                Select Case sender.Name 'SAJ 04122006 Eventos enter para el avance de los campos despues de los calculos  
                    Case Me.TxtBase.Name, Me.TxtOtros.Name, Me.TxtPrecioVenta_A.Name, Me.TxtPrecioVenta_B.Name, Me.TxtPrecioVenta_C.Name, Me.TxtPrecioVenta_D.Name, _
                    Me.TxtPrecioVenta_P.Name, Me.TxtPrecioVenta_IV_A.Name, Me.TxtPrecioVenta_IV_B.Name, Me.TxtPrecioVenta_IV_C.Name, Me.TxtPrecioVenta_IV_D.Name, Me.TxtPrecioVenta_IV_P.Name, _
                    Me.TxtMin.Name, TxtMax.Name, TxtMed.Name, Me.TxtExistencia.Name, Me.TxtUtilidad_P.Name, txtVentaSugerido.Name
                        If Asc(e.KeyChar) = Keys.Enter Then SendKeys.Send("{TAB}")
                End Select
            End If
            ' esto invalida la tecla pulsada CUANDO NO ES NUMERICO SAJ:190906
            If Not IsNumeric(sender.text & e.KeyChar) Then If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then e.Handled = True

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function Utilidad(ByVal Costo As Double, ByVal Precio As Double) As Double
        Utilidad = IIf(Costo = 0, 0, Math.Round(((Precio / Costo) - 1) * 100, 5))
        Return Utilidad
    End Function

    Private Sub BuscarRegistros()
        Try
            If Me.BindingContext(Me.DataSetInventario, "Inventario").Count > 0 Then
                Me.BindingContext(Me.DataSetInventario, "Inventario").CancelCurrentEdit()
                BindingContext(Me.DataSetInventario, "Inventario2").CancelCurrentEdit()
                ToolBar1.Buttons(0).Text = "Nuevo"
                ToolBar1.Buttons(0).ImageIndex = 0
                Me.DataNavigator1.Enabled = True
            End If


            Dim BuscarArt As New FrmBuscarArticulo
            BuscarArt.StartPosition = FormStartPosition.CenterParent
            BuscarArt.CheckBoxInHabilitados.Enabled = True
            BuscarArt.ShowDialog()

            cargando = True
            If BuscarArt.Cancelado Then
                cargando = False
                Exit Sub
            End If

            Dim valor As String
            Dim pos As Integer
            Dim vista As DataView
            Me.ToolBarRegistrar.Text = "Actualizar"
            vista = Me.DataSetInventario.Inventario2.DefaultView
            vista.Sort = "Codigo"
            pos = vista.Find(CDbl(BuscarArt.Codigo))
            Me.BindingContext(Me.DataSetInventario, "Inventario2").Position = pos

            If Me.DataSetInventario.Inventario.Rows(0).Item("Inhabilitado") = False Then
                Me.ToolBar1.Buttons(3).Text = "Inhabilitar"
            Else
                Me.ToolBar1.Buttons(3).Text = "Habilitar"
            End If
            Me.ToolBarEliminar.Enabled = True
            Me.ToolBarImprimir.Enabled = True

            cargando = False
        Catch EX As SystemException
            MsgBox(EX.Message)
        End Try
        'Me.CargarAdaptadores()
    End Sub

    Function LlenarInventario(ByVal Id As Double) As Boolean

        Dim cnnv As SqlConnection = Nothing
        Dim dt As New DataTable
        '
        Try
            DataSetInventario.Lotes.Clear()
            DataSetInventario.Articulos_x_Proveedor.Clear()
            DataSetInventario.ArticulosXBodega.Clear()
            DataSetInventario.Inventario.Clear()
            cFunciones.Cargar_Tabla_Generico(AdapterInventario, "Select * from Inventario where codigo =" & Id)
            AdapterInventario.Fill(DataSetInventario.Inventario)

            If DataSetInventario.Inventario.Rows.Count() = 0 Then
                MsgBox("No existe un art�culo con ese c�digo en el inventario", MsgBoxStyle.Information)
                Return False
            End If
            '''''''''LLENAR ARTICULOS X PROVEEDOR'''''''''''''''''''''''''''''''''''''''
            cFunciones.Cargar_Tabla_Generico(AdapterArticulosXproveedor, "SELECT CodigoArticulo,CodigoProveedor,FechaUltimaCompra,UltimoCosto, Moneda.Simbolo,Proveedores.Nombre  FROM [Articulos x Proveedor] INNER JOIN Moneda ON  [Articulos x Proveedor].Moneda = Moneda.CodMoneda INNER JOIN Proveedores ON [Articulos x Proveedor].CodigoProveedor = Proveedores.CodigoProv and CodigoArticulo =" & Id)
            AdapterArticulosXproveedor.Fill(DataSetInventario.Articulos_x_Proveedor)

            '''''''''LLENAR ARTICULOS X LOTE'''''''''''''''''''''''''''''''''''''''
            cFunciones.Cargar_Tabla_Generico(AdapterLote, "SELECT Id, Numero, Vencimiento, Cant_Actual, Cod_Articulo FROM Lotes WHERE Cod_Articulo =" & Id)
            AdapterLote.Fill(DataSetInventario.Lotes)
            '--------------------------------------------------------------------------

            '''''''''LLENAR ARTICULOS X BODEGA'''''''''''''''''''''''''''''''''''''''
            cFunciones.Llenar_Tabla_Generico("SELECT  ArticulosXBodega.Existencia as Existencia, Bodegas.Nombre_Bodega as Nombre, ArticulosXBodega.Codigo as Codigo,  ArticulosXBodega.Id as Id, Bodegas.ID_Bodega as IdBodega FROM ArticulosXBodega INNER JOIN  Bodegas ON ArticulosXBodega.IdBodega = Bodegas.ID_Bodega WHERE ArticulosXBodega.Codigo = " & Id, Me.DataSetInventario.ArticulosXBodega, Me.SqlConnection1.ConnectionString)

            If Me.Check_Inhabilitado.Checked = False Then
                Me.ToolBar1.Buttons(3).Text = "Inhabilitar"
            Else
                Me.ToolBar1.Buttons(3).Text = "Habilitar"
            End If
            If PMU.Others Then
                CheckBox1.Enabled = True
                TxtImpuesto.Properties.ReadOnly = False
            Else
                CheckBox1.Enabled = False
                TxtImpuesto.Properties.ReadOnly = True
            End If
        
            Eliminar_Articulo_Validacion()
            If Me.BindingContext(DataSetInventario, "Inventario").Current("IVenta") > 0 Then
                CheckBox1.Checked = False
            Else
                CheckBox1.Checked = True
            End If
            Return True

        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atenci�n...")
        Finally
            ' Por si se produce un error,
            ' comprobamos si en realidad el objeto Connection est� iniciado,
            ' de ser as�, lo cerramos.
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Function

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        PMU = VSM(usua.Cedula, Me.Name)  'Carga los privilegios del usuario con el modulo  

        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : NuevosDatos()
            Case 2 : If PMU.Find Then Me.BuscarRegistros() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Update Then RegistrarDatos() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Delete Then Habilitar_Inhabilitar() Else MsgBox("No tiene permiso para desactivar items...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 6 : If PMU.Delete Then Eliminar_Articulo() Else MsgBox("No tiene permiso para desactivar items...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : If MessageBox.Show("�Desea Cerrar el modulo actual..?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then Me.Close()
        End Select
    End Sub

    Private Sub Eliminar_Articulo_Validacion()
        Dim Items(1) As Integer
        Dim Cx As New Conexion
        Items(0) = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT COUNT(Ventas_Detalle.Cantidad) FROM Ventas_Detalle INNER JOIN  Ventas ON Ventas_Detalle.Id_Factura = Ventas.Id WHERE (Ventas.Anulado = 0) and codigo = " & Me.TxtCodigo.Text & "GROUP BY Ventas_Detalle.Codigo")
        Items(1) = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT COUNT(Cantidad) FROM articulos_comprados GROUP BY Codigo HAVING Codigo = " & Me.TxtCodigo.Text)

        Me.ToolBarEliminador.Enabled = IIf((Items(0) + Items(1)) > 0, False, True)
        Cx.DesConectar(Cx.sQlconexion)
    End Sub
    Private Sub Eliminar_Articulo()
        Dim Items(1) As Integer
        Dim Cx As New Conexion
        Items(0) = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT COUNT(Ventas_Detalle.Cantidad) FROM Ventas_Detalle INNER JOIN  Ventas ON Ventas_Detalle.Id_Factura = Ventas.Id WHERE (Ventas.Anulado = 0) and codigo = " & Me.TxtCodigo.Text & "GROUP BY Ventas_Detalle.Codigo")
        Items(1) = Cx.SlqExecuteScalar(Cx.Conectar, "SELECT COUNT(Cantidad) FROM articulos_comprados GROUP BY Codigo HAVING Codigo = " & Me.TxtCodigo.Text)

        If Items(0) = 0 And Items(1) = 0 Then
            If MsgBox("Esta seguro que desea proceder con la ELIMINACION del art�culo seleccionado...", MsgBoxStyle.YesNo, "Atenci�n...") = MsgBoxResult.Yes Then
                Me.BindingContext(Me.DataSetInventario, "Inventario").RemoveAt(Me.BindingContext(Me.DataSetInventario, "Inventario").Position)
                Me.BindingContext(Me.DataSetInventario, "Inventario").EndCurrentEdit()
                Me.AdapterInventario.Update(Me.DataSetInventario, "Inventario")

                Me.DataSetInventario.Inventario2.Clear()
                Me.Adaptador_Inventraio_AUX.Fill(Me.DataSetInventario, "Inventario2")

                MsgBox("Se ha eliminado el registro del inventario.", MsgBoxStyle.Information, "Atenci�n...")
                Me.NuevoRegistros()
            End If
        Else
            MsgBox("Existen movimientos registrados para para el art�culo " & Me.TxtDescripcion.Text & vbCrLf & "Cantidad de items Vendidos.. " & Items(0) & vbCrLf & "Cantidad de Items Comprados.. " & Items(1), MsgBoxStyle.Critical, "Atenci�n...")
        End If
        Cx.DesConectar(Cx.sQlconexion)
    End Sub
    Function Imprimir()
        Try
            Me.ToolBar1.Buttons(4).Enabled = False
            Dim Articulo_reporte As New Reporte_Ficha_Articulo
            Articulo_reporte.SetParameterValue(0, CDbl(Me.TxtCodigo.Text))
            CrystalReportsConexion.LoadShow(Articulo_reporte, MdiParent)
            'Dim Visor As New frmVisorReportes
            'CrystalReportsConexion.LoadReportViewer(Visor.rptViewer, Articulo_reporte, False, Me.SqlConnection1.ConnectionString)
            'Visor.rptViewer.ReportSource = Articulo_reporte
            'Visor.MdiParent = MdiParent
            'Visor.rptViewer.Visible = True
            'Visor.Show()


            Me.ToolBar1.Buttons(4).Enabled = True
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Function
    Dim Nuevo As Boolean = False
    Private Sub NuevosDatos()

        If ToolBar1.Buttons(0).Text = "Nuevo" Then
            Nuevo = True
            ToolBar1.Buttons(0).Text = "Cancelar"
            'SAJ 07122006 CAMBIO DE NOMBRE DE REGISTAR A ACTUALIZA SEGUN 
            Me.ToolBarRegistrar.Text = "Registrar"
            ToolBar1.Buttons(0).ImageIndex = 8
            Me.ToolBar1.Buttons(2).Enabled = True
            Me.ToolBarEliminar.Enabled = True
            strNuevo = "1"
            Try
                Me.DataSetInventario.Inventario.FechaIngresoColumn.DefaultValue = Now
                Me.DataSetInventario.Inventario.CodigoColumn.DefaultValue = 0
                Me.BindingContext(DataSetInventario, "Inventario").CancelCurrentEdit()
                Me.BindingContext(DataSetInventario, "Inventario").EndCurrentEdit()
                Me.BindingContext(DataSetInventario, "Inventario").AddNew()
                NuevoRegistros()
                'LFCHG 07122006
                'CONTROLAR LA NAVEGACION ENTRE REGISTROS
                Me.DataNavigator1.Enabled = False
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        Else
            Nuevo = False
            Me.BindingContext(Me.DataSetInventario, "Inventario").CancelCurrentEdit()
            'LFCHG 07122006
            'CONTROLAR LA NAVEGACION ENTRE REGISTROS
            Me.DataNavigator1.Enabled = False
            ToolBar1.Buttons(0).Text = "Nuevo"
            ToolBar1.Buttons(0).ImageIndex = 0
            Me.ToolBar1.Buttons(2).Enabled = False
            Me.DataNavigator1.Enabled = True
            Me.ToolBarRegistrar.Text = "Actualizar"
            Me.ToolBarRegistrar.Enabled = True
            Me.ToolBarImprimir.Enabled = True
            strNuevo = ""
        End If
    End Sub

    'Private Sub ActualizarDatos()
    '    Try
    '        If Me.BindingContext(Me.DataSetInventario, "inventario").Count > 0 Then
    '            Me.BindingContext(Me.DataSetInventario, "inventario").EndCurrentEdit()
    '            MsgBox("Los datos se actualiazaron satisfactoriamente...", MsgBoxStyle.Information, "Atenci�n...")
    '            ToolBar1.Buttons(0).Text = "Nuevo"
    '            ToolBar1.Buttons(0).ImageIndex = 0
    '        Else
    '            MsgBox("No existen datos a ser actualizados...", MsgBoxStyle.Information, "Atenci�n...")
    '        End If
    '    Catch ex As SystemException
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub Habilitar_Inhabilitar()
        Dim resp As Integer
        Dim funciones As New Conexion
        Dim mensaje As String
        Dim Descripcion_Accion As String
        Dim mensaje2 As String

        If Me.Check_Inhabilitado.Checked = False Then
            If CDbl(TxtExistencia.Text) > 0.01 Then
                MsgBox("No es posible inhabilitar el articulo, tiene existencias", MsgBoxStyle.Critical)
                Exit Sub
            End If
            mensaje = "Desea Inhabilitar este art�culo en el inventario"
            mensaje2 = "INHABILITADO"
            Descripcion_Accion = "ARTICULO INHABILITADO"


        Else
            mensaje = "Desea Habilitar este art�culo en el inventario"
            mensaje2 = "HABILITADO"
            Descripcion_Accion = "ARTICULO HABILITADO"
        End If

        If MsgBox(mensaje, MsgBoxStyle.YesNo, "Atenci�n...") = MsgBoxResult.Yes Then
            Try 'se intenta hacer
                If Me.BindingContext(Me.DataSetInventario, "Inventario").Count > 0 Then

                    Dim datos As String
                    Dim Nombre_Tabla As String


                    Nombre_Tabla = "INVENTARIO"
                    'datos = "" & Nombre_Tabla & "','" & Me.TxtCodigo.Text & "','" & Me.TxtDescripcion.Text & "', 'ARTICULO  " & mensaje2 & "','" & usua.Nombre & "'," & CDbl(Me.TxtCosto.Text) & "," & CDbl(Me.TxtPrecioVenta_A.Text) & "," & CDbl(Me.TxtPrecioVenta_B.Text) & "," & CDbl(Me.TxtPrecioVenta_C.Text) & "," & CDbl(Me.TxtPrecioVenta_D.Text)
                    'datos = "" & Nombre_Tabla & "','" & Me.TxtCodigo.Text & "','" & Me.TxtDescripcion.Text & "','" & Descripcion_Accion & "','" & Date.Now & "','" & usua.Nombre & "','" & CDbl(Me.TxtCosto.Text) & "','" & CDbl(Me.TxtPrecioVenta_A.Text) & "','" & CDbl(Me.TxtPrecioVenta_B.Text) & "','" & CDbl(Me.TxtPrecioVenta_C.Text) & "','" & CDbl(Me.TxtPrecioVenta_D.Text)
                    'If funciones.AddNewRecord("Bitacora", "Tabla,Campo_Clave,DescripcionCampo,Accion,Fecha,Usuario,Costo,VentaA,VentaB,VentaC,VentaD", datos) <> "" Then
                    '    MsgBox("Problemas al Inhabilitar el art�culo", MsgBoxStyle.Critical)
                    '    'MsgBox(s)
                    '    Exit Sub
                    'End If

                    '+++++++++++++++++++++++++++++++++++++++++Esto es una prueba++++++++++++++++++++++++++++++++++++++++++'

                    Me.BindingContext(Me.DataSetInventario, "Bitacora").AddNew()
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Tabla") = "INVENTARIO"
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Campo_Clave") = Me.TxtCodigo.Text
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("DescripcionCampo") = Me.TxtDescripcion.Text
                    'Se especifica que tipo de accion es si es un nuevo articulo o una actualizacion'
                    'If Me.ToolBarNuevo.Text = "Cancelar" Then
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Accion") = Descripcion_Accion
                    'Else
                    '   Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Accion") = "ARTICULO ACTUALIZADO"
                    'End If
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Usuario") = usua.Nombre
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Fecha") = Now
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Costo") = CDbl(Me.TxtCosto.Text)
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("VentaA") = CDbl(Me.TxtPrecioVenta_A.Text)
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("VentaB") = CDbl(Me.TxtPrecioVenta_B.Text)
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("VentaC") = CDbl(Me.TxtPrecioVenta_C.Text)
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("VentaD") = CDbl(Me.TxtPrecioVenta_D.Text)
                    Me.BindingContext(Me.DataSetInventario, "Bitacora").EndCurrentEdit()
                    '+++++++++++++++++++++++++++++++++++++++++Esto es una prueba++++++++++++++++++++++++++++++++++++++++++'

                    If Me.Check_Inhabilitado.Checked = True Then
                        Me.Check_Inhabilitado.Checked = False
                        Me.ToolBar1.Buttons(3).Text = "Inhabilitar"
                        Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Inhabilitado") = False
                    Else
                        Me.Check_Inhabilitado.Checked = True
                        Me.ToolBar1.Buttons(3).Text = "Habilitar"
                        Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Inhabilitado") = True
                    End If

                    'Me.BindingContext(Me.DataSetInventario, "Inventario").RemoveAt(Me.BindingContext(Me.DataSetInventario, "Inventario").Position)
                    Me.BindingContext(Me.DataSetInventario, "Inventario").EndCurrentEdit()
                    Me.AdapterInventario.Update(Me.DataSetInventario, "Inventario")


                    MsgBox("El art�culo fue " & mensaje2 & " satisfactoriamente", MsgBoxStyle.Information)

                Else
                    MsgBox("No existen datos a ser Habilitados/Habilitados", MsgBoxStyle.Information)
                End If
            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        End If
    End Sub
    Private Function buscarCodigoBarra2() As String  'JCGA 12042007

        Try

            Dim strCodigoBarras As String = TxtBarras.Text
            Dim strCodigoArticulo As String = TxtCodigo.Text
            Dim strQuery As String = "SELECT Descripcion FROM inventario WHERE barras = '" & strCodigoBarras & "' and Codigo= '" & strCodigoArticulo & "'"
            Dim strArticulo As String = ""
            Dim strMensaje As String = ""

            'Evaluo si el ariculo conserva el mismo codigo de barras 
            strArticulo = cFunciones.BuscaString(strQuery, Me.SqlConnection1.ConnectionString)

            'Si articulo no trae informacion indica que el articulo cambio el codigo de barras
            If strArticulo = "" Then

                'Evaluo si algun articulo tiene asignado el codigo de barras que se le acaba de asignar al articulo
                strQuery = "SELECT Descripcion FROM inventario WHERE barras = '" & strCodigoBarras & "'"

                strArticulo = cFunciones.BuscaString(strQuery, Me.SqlConnection1.ConnectionString)
                'Si no trae nada indica que no hay ariculo con el codigo de barras
                If strArticulo = "" Then
                    Return strMensaje
                Else
                    'Si se encontro algun articulo con ese codigo de barras se enviara el la decripcion de este
                    strMensaje = strArticulo
                    Return strMensaje
                End If

            Else
                'Si el articulo conserva el mismo codigo de barra se envia el mensaje de igual
                strMensaje = "Igual"
                Return strMensaje

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function


    Private Function buscarCodigoBarra() As String  'JCGA 12042007

        Try
            Dim strCodigoBarras As String = TxtBarras.Text
            Dim strQuery As String = "SELECT Descripcion FROM inventario WHERE barras = '" & strCodigoBarras & "'"
            Dim strArticulo As String = ""
            Dim strMensaje As String = ""
            strArticulo = cFunciones.BuscaString(strQuery, Me.SqlConnection1.ConnectionString)
            If strArticulo = "" Then
                Return strMensaje
            Else
                strMensaje = strArticulo
                Return strMensaje
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Private Sub RegistrarDatos()
        Try
            If Me.ToolBarRegistrar.Text = "Registrar" Then
                Dim Link As New Conexion
                TxtCodigo.Text = CDbl(Link.SQLExeScalar("SELECT isnull(MAX(Codigo),0) FROM Inventario") + 1)
                Me.Text_Cod_AUX.Text = TxtCodigo.Text
            End If


            Dim funciones As New Conexion
            Dim datos As String
            Dim strActualiza As String = ""
            If CDbl(Me.TxtCosto.Text) = 0 Or CDbl(Me.TxtCosto.Text) < 0 Then
                MsgBox("El costo del art�culo no puede ser 0, corrigalo", MsgBoxStyle.Information)
                Exit Sub
            End If
            If ckVerCodBarraInv.Checked = True Then
                '-------------------------------------------------------------------------------------------------
                'Variable que indica si se ha presionado el boton de nuevo
                If strNuevo <> "" Then
                    Dim strExistencia As String = buscarCodigoBarra()
                    If strExistencia <> "" Then
                        MsgBox("El Codigo de Barras asignado pertenece a :[" & strExistencia & "] introduzca otro codigo!!")
                        Exit Sub
                    End If
                End If

                '-Si se realiza una actualizacion la aplicacion realiza una busqueda del Codigo de Barras--------------------

                strActualiza = buscarCodigoBarra2()

                If strActualiza <> "" Then
                    If strActualiza <> "Igual" Then
                        MsgBox("El Codigo de Barras asignado pertenece a :[" & strActualiza & "] introduzca otro codigo!!")
                        Exit Sub
                    End If
                End If

                '------------------------------------------------------------------------------------------------------------
            End If
            '********************************************************************************************
            'LFCHG 07122006 
            'DESCRIPCION DEL CAMBIO:
            'ELIMINACION DE LA VARIABLE NUEVO
            'VALIDACION EN CASO DE HABER UN CAMBIO EN LA TABLA INVENTARIO
            'INCLUIR EN UNA TRASSACCION EL PROCEDIMIENTO DE REGISTAR

            'If Me.ToolBarNuevo.Text = "Cancelar" Then
            Me.BindingContext(Me.DataSetInventario, "Bitacora").AddNew()
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Tabla") = "INVENTARIO"
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Campo_Clave") = Me.TxtCodigo.Text
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("DescripcionCampo") = Me.TxtDescripcion.Text
            'Se especifica que tipo de accion es si es un nuevo articulo o una actualizacion'
            If Me.ToolBarNuevo.Text = "Cancelar" Then
                Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Accion") = "ARTICULO AGREGADO"
            Else
                Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Accion") = "ARTICULO ACTUALIZADO"
            End If
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Usuario") = usua.Nombre
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Fecha") = Now
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("Costo") = CDbl(Me.TxtCosto.Text)
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("VentaA") = CDbl(Me.TxtPrecioVenta_A.Text)
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("VentaB") = CDbl(Me.TxtPrecioVenta_B.Text)
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("VentaC") = CDbl(Me.TxtPrecioVenta_C.Text)
            Me.BindingContext(Me.DataSetInventario, "Bitacora").Current("VentaD") = CDbl(Me.TxtPrecioVenta_D.Text)
            Me.BindingContext(Me.DataSetInventario, "Bitacora").EndCurrentEdit()
            'Else

            If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
            Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
            Try
                Me.AdapterInventario.InsertCommand.Transaction = Trans
                Me.AdapterInventario.SelectCommand.Transaction = Trans
                Me.AdapterInventario.DeleteCommand.Transaction = Trans
                Me.AdapterInventario.UpdateCommand.Transaction = Trans
                Me.AdapterBitacora.SelectCommand.Transaction = Trans
                Me.AdapterBitacora.InsertCommand.Transaction = Trans
                Me.AdapterBitacora.DeleteCommand.Transaction = Trans
                Me.AdapterBitacora.UpdateCommand.Transaction = Trans

                '**************************************************************************
                If Me.chpeso.Checked Then
                    Me.BindingContext(Me.DataSetInventario, "Inventario").Current("EsPesado") = 1
                Else
                    Me.BindingContext(Me.DataSetInventario, "Inventario").Current("EsPesado") = 0
                End If
                Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Subfamilia") = ComboBoxFamilia.SelectedValue.ToString
                Me.BindingContext(Me.DataSetInventario, "Inventario").Current("CodMarca") = ComboBoxMarca.SelectedValue.ToString
                Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Proveedor") = CInt(cmboxProveedor.SelectedValue)
                Me.BindingContext(Me.DataSetInventario, "Inventario").Current("MonedaVenta") = CInt(ComboBoxMonedaVenta.SelectedValue)

                Me.BindingContext(Me.DataSetInventario, "Inventario").Position -= 1
                Me.BindingContext(Me.DataSetInventario, "Inventario").Position += 1
                If Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Precio_Sugerido") < 0 Then
                    Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Precio_Sugerido") = 0
                    Me.BindingContext(Me.DataSetInventario, "Inventario").Current("SugeridoIV") = 0
                End If
                Me.BindingContext(Me.DataSetInventario, "Inventario").EndCurrentEdit()
                '**************************************************************************

                If Nuevo Then
                    Dim sql As New SqlClient.SqlCommand
                    Dim Fx As cFunciones
                    sql.CommandText = "INSERT INTO [Inventario] ([Codigo] ,[Barras],[Descripcion],[PresentaCant] ,[CodPresentacion] " & _
                                    " ,[CodMarca],[SubFamilia],[Minima],[PuntoMedio],[Maxima] ,[Existencia],[SubUbicacion] ,[Observaciones] " & _
                                    " ,[MonedaCosto] ,[PrecioBase],[Fletes] ,[OtrosCargos],[Costo],[MonedaVenta],[IVenta] ,[Precio_A] ,[Precio_B] " & _
                                    " ,[Precio_C] ,[Precio_D],[Precio_Promo],[Promo_Activa],[Promo_Inicio] ,[Promo_Finaliza],[Max_Comision] ,[Max_Descuento] " & _
                                    " ,[FechaIngreso] ,[Servicio] ,[Inhabilitado] ,[Proveedor] ,[Precio_Sugerido] ,[SugeridoIV] ,[PreguntaPrecio] " & _
                                    " ,[Lote],[Consignacion] ,[Id_Bodega],[ExistenciaBodega] ,[Apartado] ,[EsPesado]) " & _
                                    "  VALUES " & _
                                    "  (@Codigo ,@Barras,@Descripcion,@PresentaCant,@CodPresentacion,@CodMarca,@SubFamilia,@Minima,@PuntoMedio " & _
                                    " ,@Maxima,@Existencia ,@SubUbicacion,@Observaciones ,@MonedaCosto ,@PrecioBase,@Fletes,@OtrosCargos,@Costo " & _
                                    " ,@MonedaVenta ,@IVenta,@Precio_A ,@Precio_B ,@Precio_C ,@Precio_D ,@Precio_Promo ,@Promo_Activa ,@Promo_Inicio " & _
                                    " ,@Promo_Finaliza ,@Max_Comision ,@Max_Descuento ,@FechaIngreso,@Servicio ,@Inhabilitado  ,@Proveedor " & _
                                    " ,@Precio_Sugerido,@SugeridoIV ,@PreguntaPrecio ,@Lote ,@Consignacion ,@Id_Bodega ,@ExistenciaBodega ,@Apartado  , @EsPesado)"

                    sql.Parameters.Add("@Codigo", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Codigo"))
                    sql.Parameters.Add("@Barras", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Barras"))
                    sql.Parameters.Add("@Descripcion", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Descripcion"))
                    sql.Parameters.Add("@PresentaCant", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("PresentaCant"))
                    sql.Parameters.Add("@CodPresentacion", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("CodPresentacion"))
                    sql.Parameters.Add("@CodMarca", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("CodMarca"))
                    sql.Parameters.Add("@SubFamilia", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("SubFamilia"))
                    sql.Parameters.Add("@Minima", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Minima"))
                    sql.Parameters.Add("@PuntoMedio", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("PuntoMedio"))
                    sql.Parameters.Add("@Maxima", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Maxima"))
                    sql.Parameters.Add("@Existencia", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Existencia"))
                    sql.Parameters.Add("@SubUbicacion", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("SubUbicacion"))
                    sql.Parameters.Add("@Observaciones", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Observaciones"))
                    sql.Parameters.Add("@MonedaCosto", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("MonedaCosto"))
                    sql.Parameters.Add("@PrecioBase", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("PrecioBase"))
                    sql.Parameters.Add("@Fletes", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Fletes"))
                    sql.Parameters.Add("@OtrosCargos", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("OtrosCargos"))
                    sql.Parameters.Add("@Costo", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Costo"))
                    sql.Parameters.Add("@MonedaVenta", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("MonedaVenta"))
                    sql.Parameters.Add("@IVenta", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("IVenta"))
                    sql.Parameters.Add("@Precio_A", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Precio_A"))
                    sql.Parameters.Add("@Precio_B", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Precio_B"))
                    sql.Parameters.Add("@Precio_C", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Precio_C"))
                    sql.Parameters.Add("@Precio_D", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Precio_D"))
                    sql.Parameters.Add("@Precio_Promo", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Precio_Promo"))
                    sql.Parameters.Add("@Promo_Activa", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Promo_Activa"))
                    sql.Parameters.Add("@Promo_Inicio", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Promo_Inicio"))
                    sql.Parameters.Add("@Promo_Finaliza", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Promo_Finaliza"))
                    sql.Parameters.Add("@Max_Comision", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Max_Comision"))
                    sql.Parameters.Add("@Max_Descuento", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Max_Descuento"))
                   sql.Parameters.Add("@FechaIngreso", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("FechaIngreso"))
                    sql.Parameters.Add("@Servicio", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Servicio"))
                    sql.Parameters.Add("@Inhabilitado", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Inhabilitado"))
                    sql.Parameters.Add("@Proveedor", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Proveedor"))
                    sql.Parameters.Add("@Precio_Sugerido", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Precio_Sugerido"))
                    sql.Parameters.Add("@SugeridoIV", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("SugeridoIV"))
                    sql.Parameters.Add("@PreguntaPrecio", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("PreguntaPrecio"))
                    sql.Parameters.Add("@Lote", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Lote"))
                    sql.Parameters.Add("@Consignacion", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Consignacion"))
                    sql.Parameters.Add("@Id_Bodega", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Id_Bodega"))
                    sql.Parameters.Add("@ExistenciaBodega", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("ExistenciaBodega"))
                    sql.Parameters.Add("@Apartado", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Apartado"))
                    sql.Parameters.Add("@EsPesado", Me.BindingContext(Me.DataSetInventario, "Inventario").Current("EsPesado"))
                   
                    Fx.fnEjecutar(sql, GetSetting("SeeSOFT", "SeePos", "Conexion"))
                Else
                    Me.AdapterInventario.Update(DataSetInventario, "Inventario")
                End If
                Nuevo = False

                Me.AdapterBitacora.Update(DataSetInventario.Bitacora)
                Trans.Commit()
                Me.DataSetInventario.AcceptChanges()
            Catch ex As Exception
                Trans.Rollback()
                If Me.ToolBarNuevo.Text = "Cancelar" Then
                    MsgBox("Error al intentar Guardar el articulo en inventario" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                Else
                    MsgBox("Error al intentar Actualizar el articulo en inventario" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                End If

            End Try

            'UNA VEZ QUE SE REALIZARON LOS CAMBIOS EN EL INVENTARIO
            'LLAMAMOS A LA FUNCION PARA CARGAR EL INVENTARIO

            'NUEVO REGISTRO
            If Me.ToolBarNuevo.ImageIndex = 8 Then
                Me.DataSetInventario.Inventario2.Clear()
                Me.Adaptador_Inventraio_AUX.Fill(Me.DataSetInventario, "Inventario2")
                Me.BindingContext(Me.DataSetInventario, "Inventario2").Position = Me.BindingContext(Me.DataSetInventario, "Inventario2").Count - 1
            Else
                'EDICION


            End If


            'FIN DE CAMBIOS
            '************************************************************************************

            MsgBox("Los datos se actualizaron satisfactoriamente...", MsgBoxStyle.Information, "Atenci�n...")
            ToolBar1.Buttons(0).Text = "Nuevo"
            ToolBar1.Buttons(0).ImageIndex = 0
            Me.DataNavigator1.Enabled = True
            Me.ToolBarRegistrar.Text = "Actualizar"
            strNuevo = ""

        Catch ex As SystemException

            MsgBox("No se han detectado cambios en este item del Inventario para ser actualizados...", MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub NuevoRegistros()
        Try

            Dim Link As New Conexion
            TxtCodigo.Text = CDbl(Link.SQLExeScalar("SELECT isnull(MAX(Codigo),0) FROM Inventario") + 1)
            Me.Text_Cod_AUX.Text = TxtCodigo.Text
            ToolBar1.Buttons(3).Enabled = False
            ToolBar1.Buttons(4).Enabled = False
            TxtBarras.Text = ""
            TxtDescripcion.Text = ""
            TxtCanPresentacion.Text = "1"
            ComboBoxPresentacion.SelectedIndex = -1

            'ComboBoxMarca.SelectedIndex = -1
            'ComboBox_Ubicacion_SubUbicacion.SelectedIndex = -1
            'ComboBoxFamilia.SelectedIndex = -1

            ComboBoxMarca.SelectedValue = Me.DataSetInventario.Marcas.Rows(0).Item(0)
            ComboBox_Ubicacion_SubUbicacion.SelectedValue = Me.DataSetInventario.SubUbicacion.Rows(0).Item(0)
            ComboBoxFamilia.SelectedValue = DataSetInventario.SubFamilias.Rows(0).Item(0)
            cmboxProveedor.SelectedValue = DataSetInventario.Proveedores.Rows(0).Item(0)


            CheckBox1.Checked = False
            TxtImpuesto.Enabled = IIf(CheckBox1.Checked, False, True)
            Me.BindingContext(DataSetInventario, "Inventario").Current("IVenta") = CDbl(Link.SQLExeScalar("SELECT Imp_Venta FROM  configuraciones"))
            Me.TxtImpuesto.Text = Me.BindingContext(DataSetInventario, "Inventario").Current("IVenta")
            TxtMin.Text = "0.00"
            TxtMax.Text = "0.00"
            TxtMed.Text = "0.00"
            TxtBase.Text = "0.00"
            TxtFlete.Text = "0.00"
            TxtCosto.Text = "0.00"
            TxtOtros.Text = "0.00"
            TxtPrecioVenta_A.Text = "0.00"
            TxtPrecioVenta_B.Text = "0.00"
            TxtPrecioVenta_C.Text = "0.00"
            TxtPrecioVenta_D.Text = "0.00"
            TxtPrecioVenta_IV_A.Text = "0.00"
            TxtPrecioVenta_IV_B.Text = "0.00"
            TxtPrecioVenta_IV_C.Text = "0.00"
            TxtPrecioVenta_IV_D.Text = "0.00"
            TxtObservaciones.Text = ""
            TxtExistencia.Text = "0.00"


            Me.TxtBarras.Focus()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub TxtCanPresentacion_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Select Case sender.NAME
            Case TxtCanPresentacion.Name
        End Select

        If TxtCanPresentacion.Text = "" Then
            ErrorProvider.SetError(sender, "Debe de digitar una cantidad...")
            e.Cancel = True
        Else
            ErrorProvider.SetError(sender, "")
        End If

    End Sub

    Private Sub TxtCodigo_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtBarras.GotFocus, TxtDescripcion.GotFocus, TxtCanPresentacion.GotFocus, TxtMed.GotFocus, TxtMax.GotFocus, TxtPrecioVenta_A.GotFocus, TxtPrecioVenta_B.GotFocus, TxtPrecioVenta_C.GotFocus, TxtPrecioVenta_D.GotFocus, TxtUtilidad_D.GotFocus, TxtUtilidad_A.GotFocus, TxtUtilidad_C.GotFocus, TxtUtilidad_B.GotFocus, TxtPrecioVenta_IV_D.GotFocus, TxtPrecioVenta_IV_A.GotFocus, TxtPrecioVenta_IV_C.GotFocus, TxtPrecioVenta_IV_B.GotFocus, TxtExistencia.GotFocus, TxtObservaciones.GotFocus, ComboBoxMonedaVenta.GotFocus, TxtMin.GotFocus, TxtImpuesto.GotFocus, TxtFlete.GotFocus, TxtOtros.GotFocus, TxtCosto.GotFocus, TxtBase.GotFocus
        If sender.name = ComboBoxMonedaVenta.Name Then
            DolarVenta()
        Else
            sender.SelectionStart = 0
            sender.SelectionLength = Len(sender.Text)
        End If
    End Sub

    Private Sub ComboBoxMonedaVenta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxMonedaVenta.SelectedIndexChanged
        DolarVenta()
    End Sub

    Private Sub DolarVenta()
        Dim Link As New Conexion
        If Me.ComboBoxMonedaVenta.Text <> "" Then
            TextBoxValorMonedaEnVenta.Clear()
            TextBoxValorMonedaEnVenta.Text = CDbl(Link.SQLExeScalar("Select isnull(max(ValorCompra),0) from moneda where codmoneda = " & ComboBoxMonedaVenta.SelectedValue))
            Link.DesConectar(Link.Conectar)
            CalcularEquivalenciaMoneda()
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Dim Link As New Conexion
        TxtImpuesto.Enabled = IIf(CheckBox1.Checked, False, True)
        'TxtImpuesto.Text = IIf(CheckBox1.Checked, "0.00", CDbl(Link.SQLExeScalar("SELECT Imp_Venta FROM  configuraciones")))
        Me.BindingContext(DataSetInventario.Inventario).Current("IVenta") = IIf(CheckBox1.Checked, "0.00", CDbl(Link.SQLExeScalar("SELECT Imp_Venta FROM  configuraciones")))
        Me.TxtImpuesto.Text = Me.BindingContext(DataSetInventario.Inventario).Current("IVenta")
        Link.DesConectar(Link.Conectar)
        EjecutarCalculos()
        If TxtImpuesto.Enabled = True Then TxtImpuesto.Focus() Else TxtBase.Focus()
    End Sub

    Private Sub CalcularEquivalenciaMoneda()
        If cargando = True Then Exit Sub
        Try
            TxtBaseEquivalente.Text = TxtBase.Text * (TextBoxValorMonedaEnCosto.Text / TextBoxValorMonedaEnVenta.Text)
            TxtFleteEquivalente.Text = TxtFlete.Text * (TextBoxValorMonedaEnCosto.Text / TextBoxValorMonedaEnVenta.Text)
            TxtOtrosCargosEquivalente.Text = TxtOtros.Text * (TextBoxValorMonedaEnCosto.Text / TextBoxValorMonedaEnVenta.Text)
            TxtCostoEquivalente.Text = TxtCosto.Text * (TextBoxValorMonedaEnCosto.Text / TextBoxValorMonedaEnVenta.Text)
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub EjecutarCalculos()
        If cargando = True Then Exit Sub
        Try
            Dim Evento As New System.Windows.Forms.KeyPressEventArgs(Chr(Keys.Enter))
            ' Salto = False
            DolarVenta()
            TextBox1_KeyPress(TxtPrecioVenta_A, Evento)
            TextBox1_KeyPress(TxtPrecioVenta_B, Evento)
            TextBox1_KeyPress(TxtPrecioVenta_C, Evento)
            TextBox1_KeyPress(TxtPrecioVenta_D, Evento)
            TextBox1_KeyPress(TxtPrecioVenta_P, Evento)
            TextBox1_KeyPress(txtVentaSugerido, Evento)
            ' Salto = True
            TxtImpuesto.Enabled = IIf(CheckBox1.Checked, False, True)

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TxtUtilidad_A_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtCodigo.LostFocus, TxtBarras.LostFocus, TxtCanPresentacion.LostFocus, TxtMed.LostFocus, TxtMax.LostFocus, TxtPrecioVenta_A.LostFocus, TxtPrecioVenta_B.LostFocus, TxtPrecioVenta_C.LostFocus, TxtPrecioVenta_D.LostFocus, TxtUtilidad_D.LostFocus, TxtUtilidad_A.LostFocus, TxtUtilidad_C.LostFocus, TxtUtilidad_B.LostFocus, TxtPrecioVenta_IV_D.LostFocus, TxtPrecioVenta_IV_A.LostFocus, TxtPrecioVenta_IV_C.LostFocus, TxtPrecioVenta_IV_B.LostFocus, TxtExistencia.LostFocus, TxtObservaciones.LostFocus, ComboBoxBodegas.LostFocus, ComboBoxPresentacion.LostFocus, ComboBox_Ubicacion_SubUbicacion.LostFocus, ComboBoxFamilia.LostFocus, ComboBoxMonedaVenta.LostFocus, ComboBoxMarca.LostFocus, TxtUtilidad_P.LostFocus, TxtPrecioVenta_P.LostFocus, TxtPrecioVenta_IV_P.LostFocus, TxtMin.LostFocus, TxtImpuesto.LostFocus, TxtFlete.LostFocus, TxtOtros.LostFocus, TxtCosto.LostFocus, ComboBoxMoneda.LostFocus, CheckBox1.LostFocus, TxtBase.LostFocus
        Try

            If cargando = True Then Exit Sub
            Select Case sender.Name
                Case TxtBase.Name, TxtFlete.Name, TxtOtros.Name
                    If TxtBase.Text = "" Then TxtBase.Text = 0
                    If TxtFlete.Text = "" Then TxtFlete.Text = 0
                    If TxtOtros.Text = "" Then TxtOtros.Text = 0
                    TxtCosto.Text = (CDbl(TxtBase.Text) + CDbl(TxtFlete.Text) + CDbl(TxtOtros.Text))
                    CalcularEquivalenciaMoneda()
                    'calculo de precios por la utilidad
                Case TxtUtilidad_A.Name
                    If TxtUtilidad_A.Text = "" Then TxtUtilidad_A.Text = 0
                    TxtPrecioVenta_A.Text = TxtBaseEquivalente.Text * (TxtUtilidad_A.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                    TxtPrecioVenta_IV_A.Text = TxtPrecioVenta_A.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_A.Text

                Case TxtUtilidad_B.Name
                    If TxtUtilidad_B.Text = "" Then TxtUtilidad_B.Text = 0
                    TxtPrecioVenta_B.Text = TxtBaseEquivalente.Text * (TxtUtilidad_B.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                    TxtPrecioVenta_IV_B.Text = TxtPrecioVenta_B.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_B.Text

                Case TxtUtilidad_C.Name
                    If TxtUtilidad_C.Text = "" Then TxtUtilidad_C.Text = 0
                    TxtPrecioVenta_C.Text = TxtBaseEquivalente.Text * (TxtUtilidad_C.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                    TxtPrecioVenta_IV_C.Text = TxtPrecioVenta_C.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_C.Text

                Case TxtUtilidad_D.Name
                    If TxtUtilidad_D.Text = "" Then TxtUtilidad_D.Text = 0
                    TxtPrecioVenta_D.Text = TxtBaseEquivalente.Text * (TxtUtilidad_D.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                    TxtPrecioVenta_IV_D.Text = TxtPrecioVenta_D.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_D.Text

                Case TxtUtilidad_P.Name
                    If TxtUtilidad_P.Text = "" Then TxtUtilidad_P.Text = 0
                    TxtPrecioVenta_P.Text = TxtBaseEquivalente.Text * (TxtUtilidad_P.Text / 100) + (TxtFleteEquivalente.Text) + (TxtOtrosCargosEquivalente.Text) + TxtBaseEquivalente.Text
                    TxtPrecioVenta_IV_P.Text = TxtPrecioVenta_P.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_P.Text


                    'calculo de de utilidad por el precio A y calculo del precio com I.V.
                Case TxtPrecioVenta_A.Name
                    If TxtPrecioVenta_A.Text = "" Then TxtPrecioVenta_A.Text = 0
                    TxtUtilidad_A.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_A.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                    TxtPrecioVenta_IV_A.Text = TxtPrecioVenta_A.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_A.Text

                Case TxtPrecioVenta_B.Name
                    If TxtPrecioVenta_B.Text = "" Then TxtPrecioVenta_B.Text = 0
                    TxtUtilidad_B.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_B.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                    TxtPrecioVenta_IV_B.Text = TxtPrecioVenta_B.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_B.Text

                Case TxtPrecioVenta_C.Name
                    If TxtPrecioVenta_C.Text = "" Then TxtPrecioVenta_C.Text = 0
                    TxtUtilidad_C.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_C.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                    TxtPrecioVenta_IV_C.Text = TxtPrecioVenta_C.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_C.Text

                Case TxtPrecioVenta_D.Name
                    If TxtPrecioVenta_D.Text = "" Then TxtPrecioVenta_D.Text = 0
                    TxtUtilidad_D.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_D.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                    TxtPrecioVenta_IV_D.Text = TxtPrecioVenta_D.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_D.Text

                Case TxtPrecioVenta_P.Name
                    If TxtPrecioVenta_P.Text = "" Then TxtPrecioVenta_P.Text = 0
                    TxtUtilidad_P.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_P.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
                    TxtPrecioVenta_IV_P.Text = TxtPrecioVenta_P.Text * (TxtImpuesto.Text / 100) + TxtPrecioVenta_P.Text

                    'CALCULO DE PRECIO DE VENTA Y RECALCULAR LA  UTILIDAD CON BASE A LOS NUEVOS PRECIOS 
                Case TxtPrecioVenta_IV_A.Name
                    If TxtPrecioVenta_IV_A.Text = "" Then TxtPrecioVenta_IV_A.Text = 0
                    TxtPrecioVenta_A.Text = TxtPrecioVenta_IV_A.Text / (1 + (TxtImpuesto.Text / 100))
                    TxtUtilidad_A.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_A.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

                Case TxtPrecioVenta_IV_B.Name
                    If TxtPrecioVenta_IV_B.Text = "" Then TxtPrecioVenta_IV_B.Text = 0
                    TxtPrecioVenta_B.Text = TxtPrecioVenta_IV_B.Text / (1 + (TxtImpuesto.Text / 100))
                    TxtUtilidad_B.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_B.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

                Case TxtPrecioVenta_IV_C.Name
                    If TxtPrecioVenta_IV_C.Text = "" Then TxtPrecioVenta_IV_C.Text = 0
                    TxtPrecioVenta_C.Text = TxtPrecioVenta_IV_C.Text / (1 + (TxtImpuesto.Text / 100))
                    TxtUtilidad_C.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_C.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

                Case TxtPrecioVenta_IV_D.Name
                    If TxtPrecioVenta_IV_D.Text = "" Then TxtPrecioVenta_IV_D.Text = 0
                    TxtPrecioVenta_D.Text = TxtPrecioVenta_IV_D.Text / (1 + (TxtImpuesto.Text / 100))
                    TxtUtilidad_D.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_D.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))

                Case TxtPrecioVenta_IV_P.Name
                    If TxtPrecioVenta_IV_P.Text = "" Then TxtPrecioVenta_IV_P.Text = 0
                    TxtPrecioVenta_P.Text = TxtPrecioVenta_IV_P.Text / (1 + (TxtImpuesto.Text / 100))
                    TxtUtilidad_P.Text = Utilidad(TxtBaseEquivalente.Text, (TxtPrecioVenta_P.Text - TxtFleteEquivalente.Text - TxtOtrosCargosEquivalente.Text))
            End Select
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Check_Promo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Promo.CheckedChanged
        If Me.Check_Promo.Checked = True Then
            Me.Desde.Enabled = True
            Me.Hasta.Enabled = True
            Me.Label33.Enabled = True
            Me.Label34.Enabled = True
            Me.TxtPrecioVenta_P.Enabled = True
            Me.TxtUtilidad_P.Enabled = True
            TxtPrecioVenta_IV_P.Enabled = True
        Else
            Me.Desde.Enabled = False
            Me.Hasta.Enabled = False
            Me.Label33.Enabled = False
            Me.Label34.Enabled = False
            Me.TxtPrecioVenta_P.Enabled = False
            Me.TxtUtilidad_P.Enabled = False
            TxtPrecioVenta_IV_P.Enabled = False
        End If
    End Sub

    Private Sub Desde_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Desde.ValueChanged
        Me.Validate()
    End Sub

    Private Sub Desde_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Desde.Validating
        If CType(Desde.Value, Date) > CType(Hasta.Value, Date) Then
            ErrorProvider.SetError(sender, "La fecha Inicial no puede ser mayor que la fecha Final...")
            e.Cancel = True
        Else
            ErrorProvider.SetError(sender, "")
            e.Cancel = False
        End If
    End Sub

    Private Sub Hasta_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Hasta.ValueChanged
        Me.Validate()
    End Sub

    Private Sub Hasta_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Hasta.Validating
        If CType(Desde.Value, Date) > CType(Hasta.Value, Date) Then
            ErrorProvider.SetError(sender, "La fecha Inicial no puede ser mayor que la fecha Final...")
            e.Cancel = True
        Else
            ErrorProvider.SetError(sender, "")
            e.Cancel = False
        End If
    End Sub

    Private Sub TxtBarras_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtBarras.TextChanged
        If Len(Me.TxtBarras.Text) = 5 Then
            If Me.TxtBarras.Text.Chars(4) = "%" Then Buscar_Codigo()
        End If
    End Sub

    Private Sub TxtBarras_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtBarras.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter : TxtDescripcion.Focus()
            Case Keys.F1 : BuscarRegistros()
        End Select
    End Sub
    Private Sub TxtDescripcion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtDescripcion.KeyDown
        If e.KeyCode = Keys.Enter Then
            TxtCanPresentacion.Focus()
        End If

        If e.KeyCode = Keys.F1 Then
            BuscarRegistros()
        End If
    End Sub

    Private Sub TxtCanPresentacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtCanPresentacion.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBoxPresentacion.Focus()
        End If

        If e.KeyCode = Keys.F1 Then
            BuscarRegistros()
        End If
    End Sub

    Private Sub ComboBoxPresentacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxPresentacion.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboBoxFamilia.Focus()
        End If
    End Sub

    Private Sub ComboBoxFamilia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxFamilia.KeyDown
        Try
            If e.KeyCode = Keys.F1 Then
                Dim Fx As New cFunciones
                Dim valor As String
                valor = Fx.BuscarDatos("SELECT SubFamilias.Codigo, Familia.Descripcion + '/' + SubFamilias.Descripcion AS Familiares FROM SubFamilias INNER JOIN Familia ON SubFamilias.CodigoFamilia = Familia.Codigo", "Familia.Descripcion + '/' + SubFamilias.Descripcion", "Buscar Familia...")

                If valor = "" Then
                    Me.ComboBoxFamilia.SelectedIndex = -1
                Else

                    Me.ComboBoxFamilia.SelectedValue = valor
                    Me.BindingContext(Me.DataSetInventario, "Inventario").Current("SubFamilia") = valor
                    Dim p As Integer = Me.BindingContext(Me.DataSetInventario, "SubFamilias").Position
                    Me.BindingContext(Me.DataSetInventario, "SubFamilias").Position += 1
                    Me.BindingContext(Me.DataSetInventario, "SubFamilias").Position = p

                End If

            End If


            If e.KeyCode = Keys.Enter Then

                Dim valor As String
                valor = Me.ComboBoxFamilia.SelectedValue
                Me.BindingContext(Me.DataSetInventario, "Inventario").Current("SubFamilia") = valor
                Dim p As Integer = Me.BindingContext(Me.DataSetInventario, "SubFamilias").Position
                Me.BindingContext(Me.DataSetInventario, "SubFamilias").Position += 1
                Me.BindingContext(Me.DataSetInventario, "SubFamilias").Position = p

                Me.ComboBox_Ubicacion_SubUbicacion.Focus()


            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox_Ubicacion_SubUbicacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox_Ubicacion_SubUbicacion.KeyDown
        Try
            If e.KeyCode = Keys.F1 Then
                Dim Fx As New cFunciones
                Dim valor As String
                valor = Fx.BuscarDatos("SELECT SubUbicacion.Codigo, Ubicaciones.Descripcion + '/' + SubUbicacion.DescripcionD AS Ubicaciones FROM SubUbicacion INNER JOIN Ubicaciones ON SubUbicacion.Cod_Ubicacion = Ubicaciones.Codigo", "Ubicaciones.Descripcion + '/' + SubUbicacion.DescripcionD", "Buscar Ubicaci�n...")
                If valor = "" Then
                    Me.ComboBox_Ubicacion_SubUbicacion.SelectedIndex = -1
                Else
                    Me.ComboBox_Ubicacion_SubUbicacion.SelectedValue = valor
                    Me.BindingContext(Me.DataSetInventario, "Inventario").Current("SubUbicacion") = valor
                End If

            End If

            If e.KeyCode = Keys.Enter Then
                Dim valor As String

                valor = Me.ComboBox_Ubicacion_SubUbicacion.SelectedValue
                Me.BindingContext(Me.DataSetInventario, "Inventario").Current("SubUbicacion") = valor
                cmboxProveedor.Focus()

            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ComboBoxMarca_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxMarca.KeyDown
        Try
            If e.KeyCode = Keys.F1 Then
                Dim Fx As New cFunciones
                Dim valor As String
                valor = Fx.BuscarDatos("Select CodMarca,Marca from Marcas", "Marca", "Buscar Marca...")

                If valor = "" Then
                    Me.ComboBoxMarca.SelectedIndex = -1
                Else
                    Me.ComboBoxMarca.SelectedValue = CInt(valor)
                    Me.BindingContext(Me.DataSetInventario, "Inventario").Current("CodMarca") = valor
                End If

            End If

            If e.KeyCode = Keys.Enter Then

                Dim valor As String
                valor = Me.ComboBoxMarca.SelectedValue
                Me.BindingContext(Me.DataSetInventario, "Inventario").Current("CodMarca") = valor
                ComboBoxMoneda.Focus()

            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBoxMoneda_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxMoneda.KeyDown
        If e.KeyCode = Keys.Enter Then
            'TxtImpuesto.Focus()
            CheckBox1.Focus()
        End If
    End Sub

    Private Sub TxtImpuesto_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtImpuesto.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Me.TxtBase.Focus()

                If Me.TxtImpuesto.Text <> "" Then TxtBase.Focus() Else Me.TxtImpuesto.Text = "0"

            End If

        Catch ex As SyntaxErrorException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TxtCosto_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtCosto.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then

                If Me.TxtCosto.Text <> "" And Me.TxtCosto.Text <> "0" Then
                    ComboBoxMonedaVenta.Focus()
                Else
                    Me.TxtCosto.Text = "0"
                End If
            End If

        Catch ex As SyntaxErrorException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBoxMonedaVenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxMonedaVenta.KeyDown
        If e.KeyCode = Keys.Enter Then
            TxtUtilidad_A.Focus()
        End If
    End Sub

    Private Sub TxtUtilidad_A_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtUtilidad_A.KeyDown, TxtUtilidad_B.KeyDown, TxtUtilidad_C.KeyDown, TxtUtilidad_D.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub TxtMaxDesc_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtMaxDesc.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        If Not IsNumeric(sender.text & e.KeyChar) Then
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub TxtBarras_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtBarras.KeyPress
        Try

            If (Not e.KeyChar.IsDigit(e.KeyChar)) And (Not e.KeyChar.IsLetter(e.KeyChar)) Then   ' valida que en este campo solo se digiten numeros y/o "-"
                If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "%"c) Then
                    e.Handled = True  ' esto invalida la tecla pulsada
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Sub Buscar_Codigo()
        Dim func As New Conexion
        Dim func2 As New Conexion
        Dim retorna As String
        Dim Nuevo_Codigo As String
        Dim cod As Integer
        Dim rs As SqlDataReader
        Dim Codigo_Subfamilia As String
        Dim existe As Boolean

        rs = func.GetRecorset(func.Conectar, "SELECT RIGHT('00' + CAST(CodigoFamilia AS varchar), 2) + RIGHT('00' + CAST(SubCodigo AS varchar), 2) AS Codigo_Ceros, Codigo FROM SubFamilias WHERE (RIGHT('00' + CAST(CodigoFamilia AS varchar), 2) + RIGHT('00' + CAST(SubCodigo AS varchar), 2) = '" & Mid(Me.TxtBarras.Text, 1, 4) & "')")
        While rs.Read
            Codigo_Subfamilia = rs("Codigo")

            cod = func.SlqExecuteScalar(func2.Conectar, "SELECT max(ISNULL(CodConsFam, 0)) AS Maximo FROM Vista_Generador_Barras WHERE (CodBarrasArticulo LIKE '" & Me.TxtBarras.Text & "') GROUP BY CodigoSubFamilia")
            cod = cod + 1
            func2.DesConectar(func2.Conectar)

            Nuevo_Codigo = Mid(Me.TxtBarras.Text, 1, 4) & Format(cod, "0000")

            'mientras ya exista un articulo en el inventario con ese c�digo de barras
            Do While func.SlqExecuteScalar(func2.Conectar, "select Barras from Inventario where Barras ='" & Nuevo_Codigo & "'") <> Nothing
                cod = cod + 1
                Nuevo_Codigo = Mid(Me.TxtBarras.Text, 1, 4) & Format(cod, "0000")
            Loop
            Me.TxtBarras.Text = Nuevo_Codigo
            ComboBoxFamilia.SelectedValue = Codigo_Subfamilia
            existe = True
            Me.TxtDescripcion.Focus()

        End While

        func.DesConectar(func.Conectar)
        func2.DesConectar(func2.Conectar)


        If existe = False Then
            MsgBox("No existe una subfamilia con ese c�digo", MsgBoxStyle.Information)
            Me.TxtBarras.Text = ""
        End If
    End Sub
    Private Sub CheckBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CheckBox1.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name
                Case CheckBox1.Name
                    TxtImpuesto.Focus()
            End Select
        End If
    End Sub
    Private Sub TxtFlete_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtFlete.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub
    Private Sub ComboBoxBodegas_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxBodegas.GotFocus, ComboBoxMonedaVenta.GotFocus, ComboBoxMoneda.GotFocus
        SendKeys.Send("{F4}")
    End Sub
    Private Sub ComboBoxBodegas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxBodegas.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub txtNombreProveedor_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmboxProveedor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmboxProveedor.KeyDown
        Try

            If e.KeyCode = Keys.F1 Then
                Dim Fx As New cFunciones
                Dim valor As String
                valor = Fx.BuscarDatos("SELECT CodigoProv, Nombre FROM Proveedores", "Nombre", "Buscar Proveedor...")
                If valor = "" Then
                    Me.cmboxProveedor.SelectedIndex = -1
                Else
                    Me.cmboxProveedor.SelectedValue = valor
                    Me.BindingContext(Me.DataSetInventario, "Proveedores").Current("CodigoProv") = valor
                    Dim p As Integer = Me.BindingContext(Me.DataSetInventario, "Proveedores").Position

                    Me.BindingContext(Me.DataSetInventario, "Proveedores").Position += 1
                    Me.BindingContext(Me.DataSetInventario, "Proveedores").Position = p

                End If

            End If
            If e.KeyCode = Keys.Enter Then

                Dim valor As String
                valor = Me.cmboxProveedor.SelectedValue
                Me.BindingContext(Me.DataSetInventario, "Proveedores").Current("CodigoProv") = valor
                Dim p As Integer = Me.BindingContext(Me.DataSetInventario, "Proveedores").Position

                BindingContext(DataSetInventario, "Proveedores").Position += 1
                BindingContext(DataSetInventario, "Proveedores").Position = p
                ComboBoxMarca.Focus()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Mensaje")
        End Try
    End Sub

    Private Sub ckVerCodBarraInv_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckVerCodBarraInv.CheckedChanged
        SaveSetting("SeeSOFT", "SeePOS", "VerCodBarraInv", Me.ckVerCodBarraInv.Checked)
    End Sub

    Private Sub txtPorSugerido_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPorSugerido.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtVentaSugerido.Text = Format(Math.Round((TxtPrecioVenta_C.Text * (txtPorSugerido.Text / 100)) + TxtPrecioVenta_C.Text, 2), "#,#0.00")
            txtSugeridoIV.Text = Format(Math.Round(txtVentaSugerido.Text * (1 + (TxtImpuesto.Text / 100)), 2), "#,#0.00")
            If Me.txtPorSugerido.Text = "NeuN" Then
                Me.txtPorSugerido.Text = 0
            End If
            If Me.txtVentaSugerido.Text = "NeuN" Then
                Me.txtVentaSugerido.Text = 0
            End If
            If Me.txtSugeridoIV.Text = "NeuN" Then
                Me.txtSugeridoIV.Text = 0
            End If

            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub txtVentaSugerido_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtVentaSugerido.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtPorSugerido.Text = Format(Math.Round(((txtVentaSugerido.Text / TxtPrecioVenta_C.Text) * 100) - 100, 2), "#,#0.00")
            txtSugeridoIV.Text = Format(Math.Round(txtVentaSugerido.Text * (1 + (TxtImpuesto.Text / 100)), 2), "#,#0.00")
            If Me.txtPorSugerido.Text = "NeuN" Then
                Me.txtPorSugerido.Text = 0
            End If
            If Me.txtVentaSugerido.Text = "NeuN" Then
                Me.txtVentaSugerido.Text = 0
            End If
            If Me.txtSugeridoIV.Text = "NeuN" Then
                Me.txtSugeridoIV.Text = 0
            End If

            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub txtSugeridoIV_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSugeridoIV.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtVentaSugerido.Text = Format(Math.Round(txtSugeridoIV.Text / (1 + (TxtImpuesto.Text / 100)), 2), "#,#0.00")
            txtPorSugerido.Text = Format(Math.Round(((txtVentaSugerido.Text / TxtPrecioVenta_C.Text) * 100) - 100, 2), "#,#0.00")
            If Me.txtPorSugerido.Text = "NeuN" Then
                Me.txtPorSugerido.Text = 0
            End If
            If Me.txtVentaSugerido.Text = "NeuN" Then
                Me.txtVentaSugerido.Text = 0
            End If
            If Me.txtSugeridoIV.Text = "NeuN" Then
                Me.txtSugeridoIV.Text = 0
            End If

            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub Ck_Consignacion_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ck_Consignacion.CheckedChanged
        ComboBoxBodegas.Enabled = Ck_Consignacion.Checked
    End Sub


    Private Sub ButtonAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgregar.Click
        incluirEnBodega()
    End Sub
    Sub incluirEnBodega()
        If MsgBox("Desea incluir una bodega", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim cf As New cFunciones
            Dim cond As String = ""
            Dim i As Integer = 0
            For i = 0 To Me.DataSetInventario.ArticulosXBodega.Count - 1
                If Me.DataSetInventario.ArticulosXBodega(i).IdBodega <> 0 Then
                    cond = cond & " AND  ID_Bodega <> " & Me.DataSetInventario.ArticulosXBodega(i).IdBodega
                End If
            Next
            cf.Llenar_Tabla_Generico("SELECT [ID_Bodega] as Id, [Nombre_Bodega]as Nombre FROM [Bodegas] WHERE ID_Bodega > 0 " & cond, Me.DataSetInventario.Bodega, GetSetting("SeeSoft", "SeePos", "Conexion"))
            Me.ComboBoxBodega.Visible = True
            Me.ButtonAgreBodega.Visible = True
            Me.ButtonAgregar.Visible = False
            Me.ButtonEliminar.Visible = False
        End If
    End Sub

    Private Sub ButtonAgreBodega_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgreBodega.Click
        agregarBodega()
    End Sub
    Sub agregarBodega()
        Dim cx As New Conexion
        Dim costo As Double
        Dim i As Integer = 0

        cx.SlqExecute(cx.Conectar("SeePos"), "INSERT INTO [SeePos].[dbo].[ArticulosXBodega]( [Codigo], [IdBodega], [Existencia] )" & _
        "VALUES(" & Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Codigo") & ", " & Me.ComboBoxBodega.SelectedValue & ",0)")
        cx.DesConectar(cx.sQlconexion)
        '''''''''LLENAR ARTICULOS X BODEGAS'''''''''''''''''''''''''''''''''''''''
        cFunciones.Llenar_Tabla_Generico("SELECT  ArticulosXBodega.Existencia as Existencia, Bodegas.Nombre_Bodega as Nombre, ArticulosXBodega.Codigo as Codigo,  ArticulosXBodega.Id as Id, Bodegas.ID_Bodega as IdBodega FROM ArticulosXBodega INNER JOIN  Bodegas ON ArticulosXBodega.IdBodega = Bodegas.ID_Bodega WHERE ArticulosXBodega.Codigo = " & Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Codigo"), Me.DataSetInventario.ArticulosXBodega, Me.SqlConnection1.ConnectionString)

        Me.ComboBoxBodega.Visible = False
        Me.ButtonAgreBodega.Visible = False
        Me.DataSetInventario.Bodega.Clear()
        Me.ButtonAgregar.Visible = True
        Me.ButtonEliminar.Visible = True
    End Sub

    Private Sub ButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminar.Click
        eliminarArticuloBodega()
    End Sub

    Sub eliminarArticuloBodega()
        If Me.BindingContext(Me.DataSetInventario, "Inventario.InventarioArticulosXBodega").Current("Existencia") = 0 And Me.BindingContext(Me.DataSetInventario, "Inventario.InventarioArticulosXBodega").Current("IdBodega") <> 0 Then
            If MsgBox("�Desea quitarlo de esta bodega?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim cx As New Conexion

                cx.SlqExecute(cx.Conectar("SeePos"), "DELETE FROM ArticulosXBodega Where Id =  " & Me.BindingContext(Me.DataSetInventario, "Inventario.InventarioArticulosXBodega").Current("Id"))
                cx.DesConectar(cx.sQlconexion)
                '''''''''LLENAR ARTICULOS X BODEGAS'''''''''''''''''''''''''''''''''''''''
                cFunciones.Llenar_Tabla_Generico("SELECT  ArticulosXBodega.Existencia as Existencia, Bodegas.Nombre_Bodega as Nombre, ArticulosXBodega.Codigo as Codigo,  ArticulosXBodega.Id as Id, Bodegas.ID_Bodega as IdBodega FROM ArticulosXBodega INNER JOIN  Bodegas ON ArticulosXBodega.IdBodega = Bodegas.ID_Bodega WHERE ArticulosXBodega.Codigo = " & Me.BindingContext(Me.DataSetInventario, "Inventario").Current("Codigo"), Me.DataSetInventario.ArticulosXBodega, Me.SqlConnection1.ConnectionString)
            End If
        Else
            MsgBox("No puede eliminar la bodega porque hay existencias del articulo!", MsgBoxStyle.Exclamation, "Inventario")
        End If
    End Sub




End Class
