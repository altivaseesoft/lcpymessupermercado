Imports CrystalDecisions.Shared
Imports System.Windows.Forms

Public Class frmReporteComisiones
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.

    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonMostrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents daMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents cboMonedas As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoCambio As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label

    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents TreeList1 As DevExpress.XtraTreeList.TreeList
    Friend WithEvents TreeList As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents AdapterComisionista As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents lblComisionista As System.Windows.Forms.Label
    Friend WithEvents cbxEmpleado As System.Windows.Forms.ComboBox
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsReporteComisiones1 As LcPymes_5._2.DsReporteComisiones
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents FechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaInicio As System.Windows.Forms.DateTimePicker

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReporteComisiones))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.FechaFinal = New System.Windows.Forms.DateTimePicker
        Me.FechaInicio = New System.Windows.Forms.DateTimePicker
        Me.ButtonMostrar = New DevExpress.XtraEditors.SimpleButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.cbxEmpleado = New System.Windows.Forms.ComboBox
        Me.DsReporteComisiones1 = New LcPymes_5._2.DsReporteComisiones
        Me.Label13 = New System.Windows.Forms.Label
        Me.cboMonedas = New System.Windows.Forms.ComboBox
        Me.lblTipoCambio = New System.Windows.Forms.Label
        Me.lblComisionista = New System.Windows.Forms.Label
        Me.TreeList1 = New DevExpress.XtraTreeList.TreeList
        Me.TreeList = New DevExpress.XtraTreeList.Columns.TreeListColumn
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.AdapterComisionista = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.GroupBox1.SuspendLayout()
        CType(Me.DsReporteComisiones1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TreeList1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.FechaFinal)
        Me.GroupBox1.Controls.Add(Me.FechaInicio)
        Me.GroupBox1.Controls.Add(Me.ButtonMostrar)
        Me.GroupBox1.Controls.Add(Me.cbxEmpleado)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cboMonedas)
        Me.GroupBox1.Controls.Add(Me.lblTipoCambio)
        Me.GroupBox1.Controls.Add(Me.lblComisionista)
        Me.GroupBox1.Controls.Add(Me.TreeList1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 466)
        Me.GroupBox1.TabIndex = 77
        Me.GroupBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.Label4.Location = New System.Drawing.Point(8, 336)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(184, 20)
        Me.Label4.TabIndex = 69
        Me.Label4.Text = "Hasta"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.Label3.Location = New System.Drawing.Point(8, 288)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(184, 20)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "Desde"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FechaFinal
        '
        Me.FechaFinal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FechaFinal.CustomFormat = "dd/MM/yyyy hh:mm:ss tt"
        Me.FechaFinal.Enabled = False
        Me.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaFinal.Location = New System.Drawing.Point(8, 360)
        Me.FechaFinal.Name = "FechaFinal"
        Me.FechaFinal.Size = New System.Drawing.Size(184, 20)
        Me.FechaFinal.TabIndex = 67
        Me.FechaFinal.Value = New Date(2006, 4, 19, 0, 0, 0, 0)
        '
        'FechaInicio
        '
        Me.FechaInicio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FechaInicio.CustomFormat = "dd/MM/yyyy hh:mm:ss tt"
        Me.FechaInicio.Enabled = False
        Me.FechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaInicio.Location = New System.Drawing.Point(8, 312)
        Me.FechaInicio.Name = "FechaInicio"
        Me.FechaInicio.Size = New System.Drawing.Size(184, 20)
        Me.FechaInicio.TabIndex = 66
        Me.FechaInicio.Value = New Date(2006, 4, 10, 0, 0, 0, 0)
        '
        'ButtonMostrar
        '
        Me.ButtonMostrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonMostrar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat
        Me.ButtonMostrar.ImageIndex = 0
        Me.ButtonMostrar.ImageList = Me.ImageList1
        Me.ButtonMostrar.Location = New System.Drawing.Point(8, 416)
        Me.ButtonMostrar.Name = "ButtonMostrar"
        Me.ButtonMostrar.Size = New System.Drawing.Size(184, 48)
        Me.ButtonMostrar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.RoyalBlue)
        Me.ButtonMostrar.TabIndex = 20
        Me.ButtonMostrar.Text = "Mostrar Reporte"
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(48, 48)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'cbxEmpleado
        '
        Me.cbxEmpleado.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbxEmpleado.DataSource = Me.DsReporteComisiones1.Comisionista
        Me.cbxEmpleado.DisplayMember = "Nombre"
        Me.cbxEmpleado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxEmpleado.Enabled = False
        Me.cbxEmpleado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxEmpleado.ForeColor = System.Drawing.Color.Blue
        Me.cbxEmpleado.ItemHeight = 13
        Me.cbxEmpleado.Location = New System.Drawing.Point(8, 262)
        Me.cbxEmpleado.Name = "cbxEmpleado"
        Me.cbxEmpleado.Size = New System.Drawing.Size(186, 21)
        Me.cbxEmpleado.TabIndex = 61
        Me.cbxEmpleado.ValueMember = "Codigo"
        '
        'DsReporteComisiones1
        '
        Me.DsReporteComisiones1.DataSetName = "DsReporteComisiones"
        Me.DsReporteComisiones1.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.Location = New System.Drawing.Point(8, 384)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 20)
        Me.Label13.TabIndex = 53
        Me.Label13.Text = "Moneda"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboMonedas
        '
        Me.cboMonedas.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboMonedas.DataSource = Me.DsReporteComisiones1
        Me.cboMonedas.DisplayMember = "Moneda.MonedaNombre"
        Me.cboMonedas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMonedas.Location = New System.Drawing.Point(96, 384)
        Me.cboMonedas.Name = "cboMonedas"
        Me.cboMonedas.Size = New System.Drawing.Size(96, 21)
        Me.cboMonedas.TabIndex = 41
        Me.cboMonedas.ValueMember = "Moneda.ValorCompra"
        '
        'lblTipoCambio
        '
        Me.lblTipoCambio.Location = New System.Drawing.Point(624, 88)
        Me.lblTipoCambio.Name = "lblTipoCambio"
        Me.lblTipoCambio.Size = New System.Drawing.Size(100, 8)
        Me.lblTipoCambio.TabIndex = 42
        '
        'lblComisionista
        '
        Me.lblComisionista.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblComisionista.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lblComisionista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblComisionista.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComisionista.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblComisionista.Location = New System.Drawing.Point(8, 246)
        Me.lblComisionista.Name = "lblComisionista"
        Me.lblComisionista.Size = New System.Drawing.Size(188, 16)
        Me.lblComisionista.TabIndex = 64
        Me.lblComisionista.Text = "Comisionista"
        '
        'TreeList1
        '
        Me.TreeList1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TreeList1.Columns.AddRange(New DevExpress.XtraTreeList.Columns.TreeListColumn() {Me.TreeList})
        Me.TreeList1.Location = New System.Drawing.Point(2, 8)
        Me.TreeList1.Name = "TreeList1"
        Me.TreeList1.BeginUnboundLoad()
        Me.TreeList1.AppendNode(New Object() {"Estados de Cuenta"}, -1, 2, 3, -1)
        Me.TreeList1.AppendNode(New Object() {"Todos"}, 0, 1, 0, 0)
        Me.TreeList1.AppendNode(New Object() {"Por Comisionista"}, 0, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Comisiones Pagadas"}, -1, 2, 3, -1)
        Me.TreeList1.AppendNode(New Object() {"Todos"}, 3, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Por Comisionsita"}, 3, 1, 0, -1)
        Me.TreeList1.AppendNode(New Object() {"Comisiones"}, -1, 2, 3, -1)
        Me.TreeList1.AppendNode(New Object() {"Por Familia"}, 6, 1, 0, -1)
        Me.TreeList1.EndUnboundLoad()
        Me.TreeList1.PrintOptions = CType((((((DevExpress.XtraTreeList.PrintOptionsFlags.PrintPageHeader Or DevExpress.XtraTreeList.PrintOptionsFlags.PrintTree) _
                    Or DevExpress.XtraTreeList.PrintOptionsFlags.PrintTreeButtons) _
                    Or DevExpress.XtraTreeList.PrintOptionsFlags.PrintImages) _
                    Or DevExpress.XtraTreeList.PrintOptionsFlags.AutoWidth) _
                    Or DevExpress.XtraTreeList.PrintOptionsFlags.AutoRowHeight), DevExpress.XtraTreeList.PrintOptionsFlags)
        Me.TreeList1.RootValue = "0"
        Me.TreeList1.SelectImageList = Me.ImageList2
        Me.TreeList1.Size = New System.Drawing.Size(195, 230)
        Me.TreeList1.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyle("SelectedRow", "TreeList", New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((((DevExpress.Utils.StyleOptions.UseBackColor Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText))
        Me.TreeList1.TabIndex = 65
        Me.TreeList1.TreeLineStyle = DevExpress.XtraTreeList.LineStyle.None
        '
        'TreeList
        '
        Me.TreeList.Caption = "Tipos Reporte"
        Me.TreeList.FieldName = "TreeListColumn1"
        Me.TreeList.Name = "TreeList"
        Me.TreeList.Options = CType(((((((DevExpress.XtraTreeList.Columns.ColumnOptions.CanMoved Or DevExpress.XtraTreeList.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraTreeList.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraTreeList.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraTreeList.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraTreeList.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraTreeList.Columns.ColumnOptions.CanMovedToCustomizationForm), DevExpress.XtraTreeList.Columns.ColumnOptions)
        Me.TreeList.visibleIndex = 0
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth16Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'AdapterComisionista
        '
        Me.AdapterComisionista.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterComisionista.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Comisionista", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Codigo, Nombre FROM Comisionista"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'daMoneda
        '
        Me.daMoneda.DeleteCommand = Me.SqlDeleteCommand2
        Me.daMoneda.InsertCommand = Me.SqlInsertCommand2
        Me.daMoneda.SelectCommand = Me.SqlSelectCommand2
        Me.daMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        Me.daMoneda.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Moneda WHERE (CodMoneda = @Original_CodMoneda) AND (MonedaNombre = @O" & _
        "riginal_MonedaNombre) AND (Simbolo = @Original_Simbolo) AND (ValorCompra = @Orig" & _
        "inal_ValorCompra) AND (ValorVenta = @Original_ValorVenta)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Moneda(CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo) VAL" & _
        "UES (@CodMoneda, @MonedaNombre, @ValorCompra, @ValorVenta, @Simbolo); SELECT Cod" & _
        "Moneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda WHERE (CodMon" & _
        "eda = @CodMoneda)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Moneda SET CodMoneda = @CodMoneda, MonedaNombre = @MonedaNombre, ValorComp" & _
        "ra = @ValorCompra, ValorVenta = @ValorVenta, Simbolo = @Simbolo WHERE (CodMoneda" & _
        " = @Original_CodMoneda) AND (MonedaNombre = @Original_MonedaNombre) AND (Simbolo" & _
        " = @Original_Simbolo) AND (ValorCompra = @Original_ValorCompra) AND (ValorVenta " & _
        "= @Original_ValorVenta); SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta" & _
        ", Simbolo FROM Moneda WHERE (CodMoneda = @CodMoneda)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(200, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(488, 466)
        Me.CrystalReportViewer1.TabIndex = 78
        '
        'frmReporteComisiones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 466)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "frmReporteComisiones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reportes de Comisiones"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DsReporteComisiones1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TreeList1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "variables"
    Dim Reporte_ID As Byte
#End Region

#Region "Load"
    Private Sub frmReporteComisiones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            AdapterComisionista.Fill(DsReporteComisiones1, "Comisionista")
            daMoneda.Fill(DsReporteComisiones1, "moneda")
            FechaInicio.Value = Now
            FechaFinal.Value = Now
            TreeList1.FullExpand()

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Controles Funciones"
    Private Sub ButtonMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMostrar.Click
        Mostrar()
    End Sub

    Private Sub TreeList1_FocusedNodeChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraTreeList.FocusedNodeChangedEventArgs) Handles TreeList1.FocusedNodeChanged
        Reporte_ID = e.Node.Id
        Reporte_Seleccionado()
    End Sub

    Private Sub cbxEmpleado_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxEmpleado.KeyDown
        If e.KeyCode = Keys.Enter Then
            If FechaInicio.Enabled Then
                FechaInicio.Focus()
            Else
                Mostrar()
            End If
        End If
    End Sub

    Private Sub FechaInicio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles FechaInicio.KeyDown
        If e.KeyCode = Keys.Enter Then
            FechaFinal.Focus()
        End If
    End Sub

    Private Sub FechaFinal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles FechaFinal.KeyDown
        If e.KeyCode = Keys.Enter Then
            Mostrar()
        End If
    End Sub
#End Region

#Region "Funciones"
    Private Sub Mostrar()
        '0	-	1	Estado de Cuenta Todos
        '1	-	2	Estado de Cuenta por Comisionista
        '2	-	4	Comisiones Pagadas Todos
        '3	-	5	Comisiones Pagadas por Comisionista
        '5	-	7	Comisiones Por Familia

        Try
            Select Case Reporte_ID
                Case 1 '0 'Estado de Cuenta Todos
                    Dim Reporte As New EstadoCuenta_Comisiones_General
                    Reporte.SetParameterValue(0, cboMonedas.SelectedValue)
                    Reporte.SetParameterValue(1, cboMonedas.Text)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, Reporte)

                Case 2 '1 'Estado de Cuenta por Comisionista
                    Dim Reporte As New EstadoCuenta_Comisiones
                    Reporte.SetParameterValue(0, cboMonedas.SelectedValue)
                    Reporte.SetParameterValue(1, cboMonedas.Text)
                    Reporte.SetParameterValue(2, cbxEmpleado.SelectedValue)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, Reporte)

                Case 4 '2 'Comisiones Pagadas Todos
                    Dim Reporte As New Reporte_ComisionesPagadas_General
                    Reporte.SetParameterValue(0, cboMonedas.SelectedValue)
                    Reporte.SetParameterValue(1, cboMonedas.Text)
                    Reporte.SetParameterValue(2, FechaInicio.Value)
                    Reporte.SetParameterValue(3, FechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, Reporte)

                Case 5 '3 'Comisiones Pagadas por Comisionista
                    Dim Reporte As New Reporte_ComisionesPagadas
                    Reporte.SetParameterValue(0, cboMonedas.SelectedValue)
                    Reporte.SetParameterValue(1, cboMonedas.Text)
                    Reporte.SetParameterValue(2, FechaInicio.Value)
                    Reporte.SetParameterValue(3, FechaFinal.Value)
                    Reporte.SetParameterValue(4, cbxEmpleado.SelectedValue)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, Reporte)

                Case 7 '4 'Comisiones por Familia
                    Dim Reporte As New Reporte_ComisionesFamilias
                    Reporte.SetParameterValue(0, cboMonedas.SelectedValue)
                    Reporte.SetParameterValue(1, cboMonedas.Text)
                    Reporte.SetParameterValue(2, FechaInicio.Value)
                    Reporte.SetParameterValue(3, FechaFinal.Value)
                    CrystalReportsConexion.LoadReportViewer(CrystalReportViewer1, Reporte)

            End Select

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Reporte_Seleccionado()
        '0	-	1	Estado de Cuenta Todos
        '1	-	2	Estado de Cuenta por Comisionista
        '2	-	4	Comisiones Pagadas Todos
        '3	-	5	Comisiones Pagadas por Comisionista
        '4	-	7	Comisiones por Familia

        cbxEmpleado.Enabled = False
        FechaInicio.Enabled = False
        FechaFinal.Enabled = False

        Select Case Reporte_ID 
            Case 1 '0 'Estado de Cuenta Todos
                cbxEmpleado.Enabled = False
                ButtonMostrar.Focus()

            Case 2 '1 'Estado de Cuenta por Comisionista
                cbxEmpleado.Enabled = True
                cbxEmpleado.Focus()

            Case 4 '2 'Comisiones Pagadas Todos
                FechaInicio.Enabled = True
                FechaFinal.Enabled = True
                cbxEmpleado.Enabled = False
                ButtonMostrar.Focus()

            Case 5 '3 'Comisiones Pagadas por Comisionista
                FechaInicio.Enabled = True
                FechaFinal.Enabled = True
                cbxEmpleado.Enabled = True
                cbxEmpleado.Focus()

            Case 7 '4 'Comisiones por Familia
                FechaInicio.Enabled = True
                FechaFinal.Enabled = True
                cbxEmpleado.Enabled = False
                ButtonMostrar.Focus()
        End Select
    End Sub
#End Region

End Class
