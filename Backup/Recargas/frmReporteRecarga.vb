Imports System.data.SqlClient
Imports System.Windows.Forms
Imports System.Data
Public Class frmReporteRecarga
    Inherits System.Windows.Forms.Form

    Dim usua As Usuario_Logeado
    Dim PMU As New PerfilModulo_Class
    Dim NuevaConexion As String

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents VisorReporte As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents ButtonMostrar As System.Windows.Forms.Button
    Friend WithEvents rbDetallado As System.Windows.Forms.RadioButton
    Friend WithEvents rbUsuario As System.Windows.Forms.RadioButton
    Friend WithEvents rbOperador As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents FechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents lblFechaInicio As System.Windows.Forms.Label
    Friend WithEvents cbOperador As System.Windows.Forms.ComboBox
    Friend WithEvents cbUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents DtsRecargas1 As LcPymes_5._2.dtsRecargas
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents adUsuarios As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDataAdapter1 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents adOperador As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.rbDetallado = New System.Windows.Forms.RadioButton
        Me.cbUsuario = New System.Windows.Forms.ComboBox
        Me.DtsRecargas1 = New LcPymes_5._2.dtsRecargas
        Me.rbUsuario = New System.Windows.Forms.RadioButton
        Me.cbOperador = New System.Windows.Forms.ComboBox
        Me.rbOperador = New System.Windows.Forms.RadioButton
        Me.VisorReporte = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.ButtonMostrar = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.FechaFinal = New System.Windows.Forms.DateTimePicker
        Me.FechaInicio = New System.Windows.Forms.DateTimePicker
        Me.lblHasta = New System.Windows.Forms.Label
        Me.lblFechaInicio = New System.Windows.Forms.Label
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adUsuarios = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDataAdapter1 = New System.Data.SqlClient.SqlDataAdapter
        Me.adOperador = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        CType(Me.DtsRecargas1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'rbDetallado
        '
        Me.rbDetallado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rbDetallado.Location = New System.Drawing.Point(8, 320)
        Me.rbDetallado.Name = "rbDetallado"
        Me.rbDetallado.TabIndex = 88
        Me.rbDetallado.Text = "Detallado"
        '
        'cbUsuario
        '
        Me.cbUsuario.DataSource = Me.DtsRecargas1
        Me.cbUsuario.DisplayMember = "Usuarios.Nombre"
        Me.cbUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbUsuario.Location = New System.Drawing.Point(24, 272)
        Me.cbUsuario.Name = "cbUsuario"
        Me.cbUsuario.Size = New System.Drawing.Size(160, 21)
        Me.cbUsuario.TabIndex = 87
        Me.cbUsuario.ValueMember = "Usuarios.Cedula"
        Me.cbUsuario.Visible = False
        '
        'DtsRecargas1
        '
        Me.DtsRecargas1.DataSetName = "dtsRecargas"
        Me.DtsRecargas1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'rbUsuario
        '
        Me.rbUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rbUsuario.Location = New System.Drawing.Point(8, 248)
        Me.rbUsuario.Name = "rbUsuario"
        Me.rbUsuario.Size = New System.Drawing.Size(144, 24)
        Me.rbUsuario.TabIndex = 86
        Me.rbUsuario.Text = "Por Usuario"
        '
        'cbOperador
        '
        Me.cbOperador.DataSource = Me.DtsRecargas1
        Me.cbOperador.DisplayMember = "Operador.Nombre"
        Me.cbOperador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbOperador.Location = New System.Drawing.Point(24, 208)
        Me.cbOperador.Name = "cbOperador"
        Me.cbOperador.Size = New System.Drawing.Size(160, 21)
        Me.cbOperador.TabIndex = 85
        Me.cbOperador.ValueMember = "Operador.Codigo"
        Me.cbOperador.Visible = False
        '
        'rbOperador
        '
        Me.rbOperador.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rbOperador.Location = New System.Drawing.Point(8, 176)
        Me.rbOperador.Name = "rbOperador"
        Me.rbOperador.TabIndex = 84
        Me.rbOperador.Text = "Por Operador"
        '
        'VisorReporte
        '
        Me.VisorReporte.ActiveViewIndex = -1
        Me.VisorReporte.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.VisorReporte.AutoScroll = True
        Me.VisorReporte.DisplayGroupTree = False
        Me.VisorReporte.ForeColor = System.Drawing.Color.Blue
        Me.VisorReporte.Location = New System.Drawing.Point(192, 0)
        Me.VisorReporte.Name = "VisorReporte"
        Me.VisorReporte.ReportSource = Nothing
        Me.VisorReporte.ShowCloseButton = False
        Me.VisorReporte.Size = New System.Drawing.Size(608, 560)
        Me.VisorReporte.TabIndex = 89
        '
        'ButtonMostrar
        '
        Me.ButtonMostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ButtonMostrar.Location = New System.Drawing.Point(48, 424)
        Me.ButtonMostrar.Name = "ButtonMostrar"
        Me.ButtonMostrar.Size = New System.Drawing.Size(88, 40)
        Me.ButtonMostrar.TabIndex = 90
        Me.ButtonMostrar.Text = "Mostrar"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.FechaFinal)
        Me.GroupBox1.Controls.Add(Me.FechaInicio)
        Me.GroupBox1.Controls.Add(Me.lblHasta)
        Me.GroupBox1.Controls.Add(Me.lblFechaInicio)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox1.Location = New System.Drawing.Point(0, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(184, 104)
        Me.GroupBox1.TabIndex = 91
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtro por Fecha"
        '
        'FechaFinal
        '
        Me.FechaFinal.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.FechaFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.FechaFinal.Location = New System.Drawing.Point(76, 64)
        Me.FechaFinal.Name = "FechaFinal"
        Me.FechaFinal.Size = New System.Drawing.Size(96, 20)
        Me.FechaFinal.TabIndex = 41
        Me.FechaFinal.Value = New Date(2016, 2, 18, 10, 49, 46, 183)
        '
        'FechaInicio
        '
        Me.FechaInicio.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.FechaInicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.FechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.FechaInicio.Location = New System.Drawing.Point(76, 24)
        Me.FechaInicio.Name = "FechaInicio"
        Me.FechaInicio.Size = New System.Drawing.Size(96, 20)
        Me.FechaInicio.TabIndex = 40
        Me.FechaInicio.Value = New Date(2006, 4, 10, 0, 0, 0, 0)
        '
        'lblHasta
        '
        Me.lblHasta.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lblHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblHasta.ForeColor = System.Drawing.Color.Blue
        Me.lblHasta.Location = New System.Drawing.Point(12, 64)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(56, 16)
        Me.lblHasta.TabIndex = 39
        Me.lblHasta.Text = "Hasta : "
        '
        'lblFechaInicio
        '
        Me.lblFechaInicio.BackColor = System.Drawing.SystemColors.ControlLight
        Me.lblFechaInicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblFechaInicio.ForeColor = System.Drawing.Color.Blue
        Me.lblFechaInicio.Location = New System.Drawing.Point(12, 24)
        Me.lblFechaInicio.Name = "lblFechaInicio"
        Me.lblFechaInicio.Size = New System.Drawing.Size(56, 16)
        Me.lblFechaInicio.TabIndex = 38
        Me.lblFechaInicio.Text = "Desde : "
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'adUsuarios
        '
        Me.adUsuarios.InsertCommand = Me.SqlInsertCommand1
        Me.adUsuarios.SelectCommand = Me.SqlSelectCommand1
        Me.adUsuarios.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Usuarios(Cedula, Nombre) VALUES (@Cedula, @Nombre); SELECT Cedula, No" & _
        "mbre FROM Usuarios"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 50, "Cedula"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Cedula, Nombre FROM Usuarios"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlDataAdapter1
        '
        Me.SqlDataAdapter1.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter1.InsertCommand = Me.SqlInsertCommand2
        Me.SqlDataAdapter1.SelectCommand = Me.SqlSelectCommand2
        Me.SqlDataAdapter1.UpdateCommand = Me.SqlUpdateCommand1
        '
        'adOperador
        '
        Me.adOperador.InsertCommand = Me.SqlInsertCommand3
        Me.adOperador.SelectCommand = Me.SqlSelectCommand3
        Me.adOperador.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "tb_S_OperadorRecarga", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO tb_S_OperadorRecarga(Codigo, Nombre) VALUES (@Codigo, @Nombre); SELEC" & _
        "T Codigo, Nombre FROM tb_S_OperadorRecarga"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.Int, 4, "Codigo"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT Codigo, Nombre FROM tb_S_OperadorRecarga"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'frmReporteRecarga
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(790, 564)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ButtonMostrar)
        Me.Controls.Add(Me.VisorReporte)
        Me.Controls.Add(Me.rbDetallado)
        Me.Controls.Add(Me.cbUsuario)
        Me.Controls.Add(Me.rbUsuario)
        Me.Controls.Add(Me.cbOperador)
        Me.Controls.Add(Me.rbOperador)
        Me.ForeColor = System.Drawing.Color.Blue
        Me.Name = "frmReporteRecarga"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte de Recargas"
        CType(Me.DtsRecargas1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub frmReporteRecarga_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        Me.spCargarComboBox()
        Me.FechaInicio.Value = Now.AddDays(-15)
        Me.FechaFinal.Value = Now
    End Sub

    Private Sub ButtonMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMostrar.Click
        PMU = VSM(usua.Cedula, Me.Name)  'Carga los privilegios del usuario con el modulo  

        If PMU.Print Then Me.spMostrarReporte() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

    End Sub
    Private Function fnValidarFechas()
        If Me.FechaInicio.Value > Me.FechaFinal.Value Then
            Return False
        End If
        Return True
    End Function
    Private Function fnValidarRadioButton()
        If Not Me.rbOperador.Checked And Not Me.rbUsuario.Checked And Not Me.rbDetallado.Checked Then
            Return False
        End If
        Return True
    End Function
    Private Sub spMostrarPorOperador()
        Dim Reporte As New rptReporteOperador
        Dim Codigo As Integer = 0
        If Me.cbOperador.Text.Equals("TODOS") Then Codigo = 0
        If Me.cbOperador.Text.Equals("Kolbi") Then Codigo = 1
        If Me.cbOperador.Text.Equals("Tuyo Movil") Then Codigo = 2
        If Me.cbOperador.Text.Equals("FullMovil") Then Codigo = 3

        Reporte.SetParameterValue("CodigoOperador", CDbl(Me.cbOperador.SelectedValue))
        If Me.cbOperador.Text.Equals("TODOS") Then
            Reporte.SetParameterValue("Filtro", False)
        Else
            Reporte.SetParameterValue("Filtro", True)
        End If
        Reporte.SetParameterValue("FechaDesde", CDate(Me.FechaInicio.Value))
        Reporte.SetParameterValue("FechaHasta", CDate(Me.FechaFinal.Value))
        CrystalReportsConexion.LoadReportViewer(VisorReporte, Reporte)
        VisorReporte.Show()
    End Sub
    Private Sub spMostrarPorUsuario()
        Dim Reporte As New rptReporteRecargaUsuario
        Reporte.SetParameterValue("Cedula", Me.cbUsuario.SelectedValue)
        If Me.cbUsuario.Text.Equals("TODOS") Then
            Reporte.SetParameterValue("Filtro", False)
        Else
            Reporte.SetParameterValue("Filtro", True)
        End If
        Reporte.SetParameterValue("FechaDesde", CDate(Me.FechaInicio.Value))
        Reporte.SetParameterValue("FechaHasta", CDate(Me.FechaFinal.Value))
        CrystalReportsConexion.LoadReportViewer(VisorReporte, Reporte)
        VisorReporte.Show()
    End Sub
    Private Sub spMostrarDetallado()
        Dim Reporte As New rptReporteRecargaDetallado
        Reporte.SetParameterValue("FechaDesde", CDate(Me.FechaInicio.Value))
        Reporte.SetParameterValue("FechaHasta", CDate(Me.FechaFinal.Value))
        CrystalReportsConexion.LoadReportViewer(VisorReporte, Reporte)
        VisorReporte.Show()
    End Sub
    Private Sub spMostrarReporte()
        Try

            If Me.fnValidarFechas Then

                If Me.fnValidarRadioButton Then
                    Try
                        Me.ButtonMostrar.Enabled = False

                        If Me.rbOperador.Checked Then
                            Me.spMostrarPorOperador()
                        End If

                        If Me.rbUsuario.Checked Then
                            Me.spMostrarPorUsuario()
                        End If

                        If Me.rbDetallado.Checked Then
                            Me.spMostrarDetallado()
                        End If

                        Me.WindowState = Windows.Forms.FormWindowState.Maximized
                        Me.ButtonMostrar.Enabled = True
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        Me.ButtonMostrar.Enabled = True
                    End Try
                Else
                    MsgBox("Seleccione una de las opciones del filtro.", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("La FechaDesde no puede ser mayor a FechaHasta.", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub
    Private Sub cbOperador1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbOperador.KeyUp
        If e.KeyCode = Keys.Enter Then
            ButtonMostrar.Focus()
        End If
    End Sub
    Private Sub cbUsuario1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbUsuario.KeyUp
        If e.KeyCode = Keys.Enter Then
            ButtonMostrar.Focus()
        End If
    End Sub


    Private Sub spCargarComboBox()
        Dim Fx As New cFunciones

        Dim strConexion As String = SqlConnection1.ConnectionString

        Fx.Cargar_Tabla_Generico(Me.adUsuarios, "SELECT [Cedula] ,[Nombre] FROM [Seepos].[dbo].[Usuarios] ", strConexion)  'JCGA 19032007
        Me.adUsuarios.Fill(Me.DtsRecargas1, "Usuarios")

        Dim Fila As DataRow
        Fila = Me.DtsRecargas1.Usuarios.NewRow()
        Fila("Cedula") = 0
        Fila("Nombre") = "TODOS"
        Me.DtsRecargas1.Usuarios.Rows.Add(Fila)
        Me.cbUsuario.SelectedValue = 0


        Fx.Cargar_Tabla_Generico(Me.adUsuarios, "SELECT [Codigo],[Nombre] FROM [Seepos].[dbo].[tb_S_OperadorRecarga]", strConexion)  'JCGA 19032007
        Me.adOperador.Fill(Me.DtsRecargas1, "Operador")

        Dim Fila2 As DataRow
        Fila2 = Me.DtsRecargas1.Operador.NewRow()
        Fila2("Codigo") = 0
        Fila2("Nombre") = "TODOS"
        Me.DtsRecargas1.Operador.Rows.Add(Fila2)
        Me.cbOperador.SelectedValue = 0

    End Sub
    Private Sub rbOperador_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOperador.CheckedChanged
        If Me.rbOperador.Checked Then
            Me.cbOperador.Visible = True
            Me.cbUsuario.Visible = False
            Me.cbOperador.Focus()
        End If
    End Sub
    Private Sub rrbUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbUsuario.CheckedChanged
        If Me.rbUsuario.Checked Then
            Me.cbOperador.Visible = False
            Me.cbUsuario.Visible = True
            Me.cbUsuario.Focus()
        End If
    End Sub
    Private Sub rbDetallado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbDetallado.CheckedChanged
        If Me.rbDetallado.Checked Then
            Me.cbOperador.Visible = False
            Me.cbUsuario.Visible = False
            ButtonMostrar.Focus()
        End If
    End Sub
    Private Sub cbOperador_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbOperador.KeyUp
        If e.KeyCode = Keys.Enter Then
            ButtonMostrar.Focus()
        End If
    End Sub
    Private Sub cbUsuario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbUsuario.KeyUp
        If e.KeyCode = Keys.Enter Then
            ButtonMostrar.Focus()
        End If
    End Sub




End Class
