Imports System.Data
Public Class rptReporteHistoricoCierreCaja
    Inherits System.Windows.Forms.Form
    Dim usua As Usuario_Logeado
    Dim NuevaConexion As String
    Dim PMU As New PerfilModulo_Class

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents btnMostrar As System.Windows.Forms.Button
    Friend WithEvents dtpIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblhasta As System.Windows.Forms.Label
    Friend WithEvents dtpFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbldesde As System.Windows.Forms.Label
    Friend WithEvents rptViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents adUsuarios As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DtsReporteHistoricoCierres1 As LcPymes_5._2.dtsReporteHistoricoCierres
    Friend WithEvents cbCajero As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnMostrar = New System.Windows.Forms.Button
        Me.dtpIni = New System.Windows.Forms.DateTimePicker
        Me.lblhasta = New System.Windows.Forms.Label
        Me.dtpFin = New System.Windows.Forms.DateTimePicker
        Me.lbldesde = New System.Windows.Forms.Label
        Me.rptViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbCajero = New System.Windows.Forms.ComboBox
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adUsuarios = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DtsReporteHistoricoCierres1 = New LcPymes_5._2.dtsReporteHistoricoCierres
        Me.GroupBox1.SuspendLayout()
        CType(Me.DtsReporteHistoricoCierres1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnMostrar
        '
        Me.btnMostrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMostrar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMostrar.Location = New System.Drawing.Point(24, 320)
        Me.btnMostrar.Name = "btnMostrar"
        Me.btnMostrar.Size = New System.Drawing.Size(168, 40)
        Me.btnMostrar.TabIndex = 86
        Me.btnMostrar.Text = "Mostrar Reporte"
        '
        'dtpIni
        '
        Me.dtpIni.CustomFormat = "dd/MM/yyyy"
        Me.dtpIni.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpIni.Location = New System.Drawing.Point(96, 40)
        Me.dtpIni.Name = "dtpIni"
        Me.dtpIni.Size = New System.Drawing.Size(112, 23)
        Me.dtpIni.TabIndex = 85
        Me.dtpIni.Value = New Date(2006, 4, 19, 0, 0, 0, 0)
        '
        'lblhasta
        '
        Me.lblhasta.BackColor = System.Drawing.Color.Transparent
        Me.lblhasta.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblhasta.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblhasta.Location = New System.Drawing.Point(0, 96)
        Me.lblhasta.Name = "lblhasta"
        Me.lblhasta.Size = New System.Drawing.Size(88, 16)
        Me.lblhasta.TabIndex = 84
        Me.lblhasta.Text = "Fecha Hasta:"
        Me.lblhasta.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dtpFin
        '
        Me.dtpFin.CustomFormat = "dd/MM/yyyy"
        Me.dtpFin.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFin.Location = New System.Drawing.Point(96, 96)
        Me.dtpFin.Name = "dtpFin"
        Me.dtpFin.Size = New System.Drawing.Size(112, 23)
        Me.dtpFin.TabIndex = 82
        Me.dtpFin.Value = New Date(2006, 4, 19, 0, 0, 0, 0)
        '
        'lbldesde
        '
        Me.lbldesde.BackColor = System.Drawing.Color.Transparent
        Me.lbldesde.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldesde.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbldesde.Location = New System.Drawing.Point(-8, 40)
        Me.lbldesde.Name = "lbldesde"
        Me.lbldesde.Size = New System.Drawing.Size(104, 16)
        Me.lbldesde.TabIndex = 83
        Me.lbldesde.Text = "Fecha Desde:"
        Me.lbldesde.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rptViewer
        '
        Me.rptViewer.ActiveViewIndex = -1
        Me.rptViewer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rptViewer.AutoScroll = True
        Me.rptViewer.BackColor = System.Drawing.SystemColors.Control
        Me.rptViewer.DisplayGroupTree = False
        Me.rptViewer.Location = New System.Drawing.Point(232, 32)
        Me.rptViewer.Name = "rptViewer"
        Me.rptViewer.ReportSource = Nothing
        Me.rptViewer.ShowCloseButton = False
        Me.rptViewer.Size = New System.Drawing.Size(560, 456)
        Me.rptViewer.TabIndex = 81
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(-8, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 16)
        Me.Label1.TabIndex = 87
        Me.Label1.Text = "Responsable:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbCajero)
        Me.GroupBox1.Controls.Add(Me.dtpIni)
        Me.GroupBox1.Controls.Add(Me.lbldesde)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.lblhasta)
        Me.GroupBox1.Controls.Add(Me.dtpFin)
        Me.GroupBox1.Controls.Add(Me.btnMostrar)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(216, 456)
        Me.GroupBox1.TabIndex = 88
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtro"
        '
        'cbCajero
        '
        Me.cbCajero.DataSource = Me.DtsReporteHistoricoCierres1.Usuario
        Me.cbCajero.DisplayMember = "Nombre"
        Me.cbCajero.Location = New System.Drawing.Point(8, 184)
        Me.cbCajero.Name = "cbCajero"
        Me.cbCajero.Size = New System.Drawing.Size(200, 21)
        Me.cbCajero.TabIndex = 88
        Me.cbCajero.ValueMember = "Cedula"
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'adUsuarios
        '
        Me.adUsuarios.InsertCommand = Me.SqlInsertCommand1
        Me.adUsuarios.SelectCommand = Me.SqlSelectCommand1
        Me.adUsuarios.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Cedula, Nombre FROM Usuarios"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Usuarios(Cedula, Nombre) VALUES (@Cedula, @Nombre); SELECT Cedula, No" & _
        "mbre FROM Usuarios"
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 50, "Cedula"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        '
        'DtsReporteHistoricoCierres1
        '
        Me.DtsReporteHistoricoCierres1.DataSetName = "dtsReporteHistoricoCierres"
        Me.DtsReporteHistoricoCierres1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'rptReporteHistoricoCierreCaja
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(792, 485)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.rptViewer)
        Me.Name = "rptReporteHistoricoCierreCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Historico de Cierres de Caja"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DtsReporteHistoricoCierres1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub mostrar()
        Dim rC As New rptReporteDifCajas
        rC.SetParameterValue(0, dtpIni.Value)
        rC.SetParameterValue(1, dtpFin.Value)
        rC.SetParameterValue(2, Me.cbCajero.SelectedValue)
        CrystalReportsConexion.LoadReportViewer(rptViewer, rC)
        rptViewer.Show()
    End Sub

    Private Sub rptReporteHistoricoCierreCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        Me.dtpIni.Value = Now.AddDays(-30)
        Me.dtpFin.Value = Date.Now
        spCargarComboBox()
    End Sub
    Private Sub spCargarComboBox()
        Dim Fx As New cFunciones

        Dim strConexion As String = SqlConnection1.ConnectionString

        Fx.Cargar_Tabla_Generico(Me.adUsuarios, "SELECT [Cedula],[Nombre] FROM [Seepos].[dbo].[Usuarios] ", strConexion)   'JCGA 19032007
        Me.adUsuarios.Fill(Me.DtsReporteHistoricoCierres1, "Usuario")

        Dim Fila As DataRow
        Fila = Me.DtsReporteHistoricoCierres1.Usuario.NewRow()
        Fila("Cedula") = 0
        Fila("Nombre") = "TODOS"
        Me.DtsReporteHistoricoCierres1.Usuario.Rows.Add(Fila)
        Me.cbCajero.SelectedValue = 0
    End Sub

    Private Sub btnMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostrar.Click

        PMU = VSM(usua.Cedula, Me.Name)  'Carga los privilegios del usuario con el modulo  

        If PMU.Print Then Me.spMostrarReporte() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub



    End Sub
    Private Function fnValidarFechas()
        If Me.dtpIni.Value > Me.dtpFin.Value Then
            Return False
        End If
        Return True
    End Function
    Private Sub spMostrarReporte()

        If Me.fnValidarFechas Then
            Dim EnEspera As New DevExpress.Utils.WaitDialogForm

            EnEspera.Caption = "Reporte Comparativo"
            EnEspera.Text = "Reporte Comparativo"
            EnEspera.Show()
            Try
                mostrar()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, Text)
            End Try
            EnEspera.Close()
        Else
            MsgBox("La FechaDesde no puede ser mayor a FechaHasta.", MsgBoxStyle.Information)
        End If

    End Sub

End Class
