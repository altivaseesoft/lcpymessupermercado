Imports System.data.SqlClient
Imports System.Data
Imports System.Windows.Forms

Public Class CierreCaja
    Inherits FrmPlantilla

#Region "Variables"
    Public Usuario As String
    Public Cedula_Usuario As String
    Dim PMU As PerfilModulo_Class
    Public tabla As New DataTable
    Public tablaresumen As New DataTable
    Public tablacierre As New DataTable
    Public SubTotal As Double
    Public Total As Double
    Public Devolucion As Double
    Public TipoCambioDolar As Double
    Public TipoCambioD As Double
    Dim Apertura As Double = "0"
    Dim AperturaNumero As String
    Dim FondoCajaDolar As Double = 0
    Dim FondoCajaColon As Double = 0

#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterUsuarios As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSetCierreCaja1 As DataSetCierreCaja
    Friend WithEvents AdapterCierre As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterApertura As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterOpciones As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TimeEdit2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents TimeEdit1 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents DTPFechafinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents DTPFechainicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btndone As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtfechaapertura As System.Windows.Forms.Label
    Friend WithEvents txtcodaperturacajero As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgResumen As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterTotalTope As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterArqueo As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterTarjeta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AdapterTipoTarjeta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdapterDetallePago As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterVentasContado As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents TextEdit18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents AdapterCierreMonto As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterCierreTarjeta As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents lblanulado As System.Windows.Forms.Label
    Friend WithEvents AdapterVentasCredito As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents TxtFondoCaja As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonBuscarCajero As System.Windows.Forms.Button
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtnomcajero As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtcodcajero As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TimeEdit3 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents TimeEdit4 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtEntradas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtSalidas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtChequesCajero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtChequesSistema As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtDepositar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtChequesDolSistema As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtChequesDolCajero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TxtDepoColSistena As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDepoColCajero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtDepDolSistema As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDepDolCajero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtDevoluciones As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents CK_PVE As System.Windows.Forms.CheckBox
    Friend WithEvents txtTotalCompras As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txtDiferenciaCaja As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTotalCajero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTotalSistema As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(CierreCaja))
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.AdapterUsuarios = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DataSetCierreCaja1 = New LcPymes_5._2.DataSetCierreCaja
        Me.AdapterCierre = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.AdapterApertura = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.AdapterOpciones = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.TimeEdit2 = New DevExpress.XtraEditors.TimeEdit
        Me.TimeEdit1 = New DevExpress.XtraEditors.TimeEdit
        Me.DTPFechafinal = New System.Windows.Forms.DateTimePicker
        Me.DTPFechainicial = New System.Windows.Forms.DateTimePicker
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.btndone = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtfechaapertura = New System.Windows.Forms.Label
        Me.txtcodaperturacajero = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.dgResumen = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.AdapterTotalTope = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.AdapterArqueo = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.AdapterTarjeta = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.txtTotalCompras = New DevExpress.XtraEditors.TextEdit
        Me.Label39 = New System.Windows.Forms.Label
        Me.txtDevoluciones = New DevExpress.XtraEditors.TextEdit
        Me.Label38 = New System.Windows.Forms.Label
        Me.txtDepositar = New DevExpress.XtraEditors.TextEdit
        Me.Label33 = New System.Windows.Forms.Label
        Me.txtSalidas = New DevExpress.XtraEditors.TextEdit
        Me.Label31 = New System.Windows.Forms.Label
        Me.txtEntradas = New DevExpress.XtraEditors.TextEdit
        Me.Label30 = New System.Windows.Forms.Label
        Me.TextEdit20 = New DevExpress.XtraEditors.TextEdit
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtTotalCajero = New DevExpress.XtraEditors.TextEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtTotalSistema = New DevExpress.XtraEditors.TextEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit14 = New DevExpress.XtraEditors.TextEdit
        Me.TxtFondoCaja = New DevExpress.XtraEditors.TextEdit
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtDiferenciaCaja = New DevExpress.XtraEditors.TextEdit
        Me.Label21 = New System.Windows.Forms.Label
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.AdapterTipoTarjeta = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.txtDepDolSistema = New DevExpress.XtraEditors.TextEdit
        Me.txtDepDolCajero = New DevExpress.XtraEditors.TextEdit
        Me.Label37 = New System.Windows.Forms.Label
        Me.TxtDepoColSistena = New DevExpress.XtraEditors.TextEdit
        Me.txtDepoColCajero = New DevExpress.XtraEditors.TextEdit
        Me.Label35 = New System.Windows.Forms.Label
        Me.txtChequesDolSistema = New DevExpress.XtraEditors.TextEdit
        Me.txtChequesDolCajero = New DevExpress.XtraEditors.TextEdit
        Me.Label34 = New System.Windows.Forms.Label
        Me.txtChequesSistema = New DevExpress.XtraEditors.TextEdit
        Me.txtChequesCajero = New DevExpress.XtraEditors.TextEdit
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.TextEdit18 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit19 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.AdapterDetallePago = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand
        Me.AdapterVentasContado = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand11 = New System.Data.SqlClient.SqlCommand
        Me.AdapterCierreMonto = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand14 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.AdapterCierreTarjeta = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand15 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.lblanulado = New System.Windows.Forms.Label
        Me.AdapterVentasCredito = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand12 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ButtonBuscarCajero = New System.Windows.Forms.Button
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit
        Me.txtnomcajero = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtcodcajero = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.TimeEdit3 = New DevExpress.XtraEditors.TimeEdit
        Me.TimeEdit4 = New DevExpress.XtraEditors.TimeEdit
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.CK_PVE = New System.Windows.Forms.CheckBox
        CType(Me.DataSetCierreCaja1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgResumen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtTotalCompras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDevoluciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepositar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSalidas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEntradas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalCajero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalSistema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFondoCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDiferenciaCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.txtDepDolSistema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepDolCajero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtDepoColSistena.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepoColCajero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChequesDolSistema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChequesDolCajero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChequesSistema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChequesCajero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.Text = "Anular"
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 592)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(778, 56)
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Text = "Cierre de Caja"
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(856, 448)
        Me.DataNavigator.Name = "DataNavigator"
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(568, 616)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 60
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(568, 632)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(208, 13)
        Me.txtNombreUsuario.TabIndex = 61
        Me.txtNombreUsuario.Text = ""
        '
        'TextBox6
        '
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox6.ForeColor = System.Drawing.Color.Blue
        Me.TextBox6.Location = New System.Drawing.Point(648, 616)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.TextBox6.Size = New System.Drawing.Size(128, 13)
        Me.TextBox6.TabIndex = 59
        Me.TextBox6.Text = ""
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'AdapterUsuarios
        '
        Me.AdapterUsuarios.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterUsuarios.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Id_Usuario", "Id_Usuario"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Clave_Entrada", "Clave_Entrada"), New System.Data.Common.DataColumnMapping("Clave_Interna", "Clave_Interna"), New System.Data.Common.DataColumnMapping("Perfil", "Perfil")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Cedula, Id_Usuario, Nombre, Clave_Entrada, Clave_Interna, Perfil FROM Usua" & _
        "rios"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'DataSetCierreCaja1
        '
        Me.DataSetCierreCaja1.DataSetName = "DataSetCierreCaja"
        Me.DataSetCierreCaja1.Locale = New System.Globalization.CultureInfo("es-MX")
        '
        'AdapterCierre
        '
        Me.AdapterCierre.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterCierre.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterCierre.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterCierre.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "cierrecaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumeroCierre", "NumeroCierre"), New System.Data.Common.DataColumnMapping("Cajera", "Cajera"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Apertura", "Apertura"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Devoluciones", "Devoluciones"), New System.Data.Common.DataColumnMapping("Subtotal", "Subtotal"), New System.Data.Common.DataColumnMapping("TotalSistema", "TotalSistema"), New System.Data.Common.DataColumnMapping("Equivalencia", "Equivalencia"), New System.Data.Common.DataColumnMapping("EfectivoColones", "EfectivoColones"), New System.Data.Common.DataColumnMapping("EfectivoDolares", "EfectivoDolares"), New System.Data.Common.DataColumnMapping("TarjetaColones", "TarjetaColones"), New System.Data.Common.DataColumnMapping("TarjetaDolares", "TarjetaDolares"), New System.Data.Common.DataColumnMapping("Cheques", "Cheques"), New System.Data.Common.DataColumnMapping("ChequeDol", "ChequeDol"), New System.Data.Common.DataColumnMapping("DepositosCol", "DepositosCol"), New System.Data.Common.DataColumnMapping("DepositosDol", "DepositosDol")})})
        Me.AdapterCierre.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM cierrecaja WHERE (NumeroCierre = @Original_NumeroCierre) AND (Anulado" & _
        " = @Original_Anulado) AND (Apertura = @Original_Apertura) AND (Cajera = @Origina" & _
        "l_Cajera) AND (ChequeDol = @Original_ChequeDol) AND (Cheques = @Original_Cheques" & _
        ") AND (DepositosCol = @Original_DepositosCol) AND (DepositosDol = @Original_Depo" & _
        "sitosDol) AND (Devoluciones = @Original_Devoluciones) AND (EfectivoColones = @Or" & _
        "iginal_EfectivoColones) AND (EfectivoDolares = @Original_EfectivoDolares) AND (E" & _
        "quivalencia = @Original_Equivalencia) AND (Fecha = @Original_Fecha) AND (Nombre " & _
        "= @Original_Nombre) AND (NombreUsuario = @Original_NombreUsuario) AND (Subtotal " & _
        "= @Original_Subtotal) AND (TarjetaColones = @Original_TarjetaColones) AND (Tarje" & _
        "taDolares = @Original_TarjetaDolares) AND (TotalSistema = @Original_TotalSistema" & _
        ") AND (Usuario = @Original_Usuario)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroCierre", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroCierre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Apertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Apertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cajera", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajera", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChequeDol", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChequeDol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cheques", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cheques", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DepositosCol", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DepositosCol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DepositosDol", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DepositosDol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EfectivoColones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoColones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EfectivoDolares", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoDolares", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Equivalencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Subtotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Subtotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TarjetaColones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaColones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TarjetaDolares", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaDolares", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSistema", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO cierrecaja(Cajera, Nombre, Apertura, Usuario, Fecha, NombreUsuario, A" & _
        "nulado, Devoluciones, Subtotal, TotalSistema, Equivalencia, EfectivoColones, Efe" & _
        "ctivoDolares, TarjetaColones, TarjetaDolares, Cheques, ChequeDol, DepositosCol, " & _
        "DepositosDol) VALUES (@Cajera, @Nombre, @Apertura, @Usuario, @Fecha, @NombreUsua" & _
        "rio, @Anulado, @Devoluciones, @Subtotal, @TotalSistema, @Equivalencia, @Efectivo" & _
        "Colones, @EfectivoDolares, @TarjetaColones, @TarjetaDolares, @Cheques, @ChequeDo" & _
        "l, @DepositosCol, @DepositosDol); SELECT NumeroCierre, Cajera, Nombre, Apertura," & _
        " Usuario, Fecha, NombreUsuario, Anulado, Devoluciones, Subtotal, TotalSistema, E" & _
        "quivalencia, EfectivoColones, EfectivoDolares, TarjetaColones, TarjetaDolares, C" & _
        "heques, ChequeDol, DepositosCol, DepositosDol FROM cierrecaja WHERE (NumeroCierr" & _
        "e = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cajera", System.Data.SqlDbType.VarChar, 255, "Cajera"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Apertura", System.Data.SqlDbType.Int, 4, "Apertura"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 8, "Devoluciones"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Subtotal", System.Data.SqlDbType.Float, 8, "Subtotal"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSistema", System.Data.SqlDbType.Float, 8, "TotalSistema"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.Float, 8, "Equivalencia"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EfectivoColones", System.Data.SqlDbType.Float, 8, "EfectivoColones"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EfectivoDolares", System.Data.SqlDbType.Float, 8, "EfectivoDolares"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TarjetaColones", System.Data.SqlDbType.Float, 8, "TarjetaColones"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TarjetaDolares", System.Data.SqlDbType.Float, 8, "TarjetaDolares"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cheques", System.Data.SqlDbType.Float, 8, "Cheques"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChequeDol", System.Data.SqlDbType.Float, 8, "ChequeDol"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DepositosCol", System.Data.SqlDbType.Float, 8, "DepositosCol"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DepositosDol", System.Data.SqlDbType.Float, 8, "DepositosDol"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT NumeroCierre, Cajera, Nombre, Apertura, Usuario, Fecha, NombreUsuario, Anu" & _
        "lado, Devoluciones, Subtotal, TotalSistema, Equivalencia, EfectivoColones, Efect" & _
        "ivoDolares, TarjetaColones, TarjetaDolares, Cheques, ChequeDol, DepositosCol, De" & _
        "positosDol FROM cierrecaja"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE cierrecaja SET Cajera = @Cajera, Nombre = @Nombre, Apertura = @Apertura, U" & _
        "suario = @Usuario, Fecha = @Fecha, NombreUsuario = @NombreUsuario, Anulado = @An" & _
        "ulado, Devoluciones = @Devoluciones, Subtotal = @Subtotal, TotalSistema = @Total" & _
        "Sistema, Equivalencia = @Equivalencia, EfectivoColones = @EfectivoColones, Efect" & _
        "ivoDolares = @EfectivoDolares, TarjetaColones = @TarjetaColones, TarjetaDolares " & _
        "= @TarjetaDolares, Cheques = @Cheques, ChequeDol = @ChequeDol, DepositosCol = @D" & _
        "epositosCol, DepositosDol = @DepositosDol WHERE (NumeroCierre = @Original_Numero" & _
        "Cierre) AND (Anulado = @Original_Anulado) AND (Apertura = @Original_Apertura) AN" & _
        "D (Cajera = @Original_Cajera) AND (ChequeDol = @Original_ChequeDol) AND (Cheques" & _
        " = @Original_Cheques) AND (DepositosCol = @Original_DepositosCol) AND (Depositos" & _
        "Dol = @Original_DepositosDol) AND (Devoluciones = @Original_Devoluciones) AND (E" & _
        "fectivoColones = @Original_EfectivoColones) AND (EfectivoDolares = @Original_Efe" & _
        "ctivoDolares) AND (Equivalencia = @Original_Equivalencia) AND (Fecha = @Original" & _
        "_Fecha) AND (Nombre = @Original_Nombre) AND (NombreUsuario = @Original_NombreUsu" & _
        "ario) AND (Subtotal = @Original_Subtotal) AND (TarjetaColones = @Original_Tarjet" & _
        "aColones) AND (TarjetaDolares = @Original_TarjetaDolares) AND (TotalSistema = @O" & _
        "riginal_TotalSistema) AND (Usuario = @Original_Usuario); SELECT NumeroCierre, Ca" & _
        "jera, Nombre, Apertura, Usuario, Fecha, NombreUsuario, Anulado, Devoluciones, Su" & _
        "btotal, TotalSistema, Equivalencia, EfectivoColones, EfectivoDolares, TarjetaCol" & _
        "ones, TarjetaDolares, Cheques, ChequeDol, DepositosCol, DepositosDol FROM cierre" & _
        "caja WHERE (NumeroCierre = @NumeroCierre)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cajera", System.Data.SqlDbType.VarChar, 255, "Cajera"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Apertura", System.Data.SqlDbType.Int, 4, "Apertura"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Usuario", System.Data.SqlDbType.VarChar, 255, "Usuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 8, "Devoluciones"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Subtotal", System.Data.SqlDbType.Float, 8, "Subtotal"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalSistema", System.Data.SqlDbType.Float, 8, "TotalSistema"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Equivalencia", System.Data.SqlDbType.Float, 8, "Equivalencia"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EfectivoColones", System.Data.SqlDbType.Float, 8, "EfectivoColones"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EfectivoDolares", System.Data.SqlDbType.Float, 8, "EfectivoDolares"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TarjetaColones", System.Data.SqlDbType.Float, 8, "TarjetaColones"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TarjetaDolares", System.Data.SqlDbType.Float, 8, "TarjetaDolares"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cheques", System.Data.SqlDbType.Float, 8, "Cheques"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChequeDol", System.Data.SqlDbType.Float, 8, "ChequeDol"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DepositosCol", System.Data.SqlDbType.Float, 8, "DepositosCol"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DepositosDol", System.Data.SqlDbType.Float, 8, "DepositosDol"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumeroCierre", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumeroCierre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Apertura", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Apertura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cajera", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cajera", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChequeDol", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChequeDol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cheques", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cheques", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DepositosCol", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DepositosCol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DepositosDol", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DepositosDol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EfectivoColones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoColones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EfectivoDolares", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EfectivoDolares", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Equivalencia", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equivalencia", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Subtotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Subtotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TarjetaColones", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaColones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TarjetaDolares", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TarjetaDolares", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalSistema", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Usuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumeroCierre", System.Data.SqlDbType.Int, 4, "NumeroCierre"))
        '
        'AdapterApertura
        '
        Me.AdapterApertura.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterApertura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "aperturacaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Estado", "Estado"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula")})})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT NApertura, Fecha, Nombre, Estado, Observaciones, Anulado, Cedula FROM aper" & _
        "turacaja"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'AdapterOpciones
        '
        Me.AdapterOpciones.SelectCommand = Me.SqlSelectCommand4
        Me.AdapterOpciones.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "OpcionesDePago", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id", "id"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("TipoDocumento", "TipoDocumento"), New System.Data.Common.DataColumnMapping("MontoPago", "MontoPago"), New System.Data.Common.DataColumnMapping("FormaPago", "FormaPago"), New System.Data.Common.DataColumnMapping("Denominacion", "Denominacion"), New System.Data.Common.DataColumnMapping("Usuario", "Usuario"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Nombremoneda", "Nombremoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Numapertura", "Numapertura")})})
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT id, Documento, TipoDocumento, MontoPago, FormaPago, Denominacion, Usuario," & _
        " Nombre, CodMoneda, Nombremoneda, TipoCambio, Fecha, Numapertura FROM OpcionesDe" & _
        "Pago"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label25.Location = New System.Drawing.Point(155, 14)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(88, 16)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Fecha Cierre"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label9.Location = New System.Drawing.Point(8, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(112, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "C�digo Cajero"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label8.Location = New System.Drawing.Point(8, 51)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(240, 16)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Nombre"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimeEdit2
        '
        Me.TimeEdit2.EditValue = New Date(2006, 3, 15, 11, 33, 32, 375)
        Me.TimeEdit2.Location = New System.Drawing.Point(496, 56)
        Me.TimeEdit2.Name = "TimeEdit2"
        '
        'TimeEdit2.Properties
        '
        Me.TimeEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.TimeEdit2.Properties.UseCtrlIncrement = False
        Me.TimeEdit2.Size = New System.Drawing.Size(96, 19)
        Me.TimeEdit2.TabIndex = 12
        '
        'TimeEdit1
        '
        Me.TimeEdit1.EditValue = New Date(2006, 3, 15, 11, 33, 32, 375)
        Me.TimeEdit1.Location = New System.Drawing.Point(496, 32)
        Me.TimeEdit1.Name = "TimeEdit1"
        '
        'TimeEdit1.Properties
        '
        Me.TimeEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.TimeEdit1.Properties.UseCtrlIncrement = False
        Me.TimeEdit1.Size = New System.Drawing.Size(96, 19)
        Me.TimeEdit1.TabIndex = 8
        '
        'DTPFechafinal
        '
        Me.DTPFechafinal.Enabled = False
        Me.DTPFechafinal.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DTPFechafinal.Location = New System.Drawing.Point(360, 56)
        Me.DTPFechafinal.MaxDate = New Date(2017, 1, 1, 0, 0, 0, 0)
        Me.DTPFechafinal.MinDate = New Date(2006, 1, 1, 0, 0, 0, 0)
        Me.DTPFechafinal.Name = "DTPFechafinal"
        Me.DTPFechafinal.Size = New System.Drawing.Size(104, 20)
        Me.DTPFechafinal.TabIndex = 11
        Me.DTPFechafinal.Value = New Date(2006, 1, 1, 0, 0, 0, 0)
        '
        'DTPFechainicial
        '
        Me.DTPFechainicial.Enabled = False
        Me.DTPFechainicial.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DTPFechainicial.Location = New System.Drawing.Point(360, 32)
        Me.DTPFechainicial.MaxDate = New Date(2017, 1, 1, 0, 0, 0, 0)
        Me.DTPFechainicial.MinDate = New Date(2006, 1, 1, 0, 0, 0, 0)
        Me.DTPFechainicial.Name = "DTPFechainicial"
        Me.DTPFechainicial.Size = New System.Drawing.Size(104, 20)
        Me.DTPFechainicial.TabIndex = 7
        Me.DTPFechainicial.Value = New Date(2006, 1, 1, 0, 0, 0, 0)
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.Location = New System.Drawing.Point(304, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 16)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Final"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.Location = New System.Drawing.Point(304, 32)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Inicial"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.Location = New System.Drawing.Point(496, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Hora"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(368, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Fecha"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btndone
        '
        Me.btndone.Enabled = False
        Me.btndone.Location = New System.Drawing.Point(624, 24)
        Me.btndone.Name = "btndone"
        Me.btndone.Size = New System.Drawing.Size(32, 56)
        Me.btndone.TabIndex = 13
        Me.btndone.Text = "OK"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.txtfechaapertura)
        Me.GroupBox2.Controls.Add(Me.txtcodaperturacajero)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox2.Location = New System.Drawing.Point(8, 132)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(256, 53)
        Me.GroupBox2.TabIndex = 63
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Apertura"
        '
        'txtfechaapertura
        '
        Me.txtfechaapertura.BackColor = System.Drawing.SystemColors.Control
        Me.txtfechaapertura.Location = New System.Drawing.Point(80, 28)
        Me.txtfechaapertura.Name = "txtfechaapertura"
        Me.txtfechaapertura.Size = New System.Drawing.Size(168, 16)
        Me.txtfechaapertura.TabIndex = 3
        Me.txtfechaapertura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtcodaperturacajero
        '
        Me.txtcodaperturacajero.BackColor = System.Drawing.SystemColors.Control
        Me.txtcodaperturacajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetCierreCaja1, "cierrecaja.Apertura"))
        Me.txtcodaperturacajero.Location = New System.Drawing.Point(8, 28)
        Me.txtcodaperturacajero.Name = "txtcodaperturacajero"
        Me.txtcodaperturacajero.Size = New System.Drawing.Size(64, 16)
        Me.txtcodaperturacajero.TabIndex = 2
        Me.txtcodaperturacajero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label1.Location = New System.Drawing.Point(80, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Fecha / Hora"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 8)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "N�mero"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.dgResumen)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox4.Location = New System.Drawing.Point(280, 392)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(488, 192)
        Me.GroupBox4.TabIndex = 64
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Detalle Operaciones"
        '
        'dgResumen
        '
        Me.dgResumen.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        'dgResumen.EmbeddedNavigator
        '
        Me.dgResumen.EmbeddedNavigator.Name = ""
        Me.dgResumen.Location = New System.Drawing.Point(8, 16)
        Me.dgResumen.MainView = Me.GridView2
        Me.dgResumen.Name = "dgResumen"
        Me.dgResumen.Size = New System.Drawing.Size(472, 168)
        Me.dgResumen.Styles.AddReplace("Preview", New DevExpress.Utils.ViewStyleEx("Preview", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, True, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.Color.FromArgb(CType(191, Byte), CType(213, Byte), CType(237, Byte)), System.Drawing.Color.FromArgb(CType(79, Byte), CType(101, Byte), CType(125, Byte)), System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("FooterPanel", New DevExpress.Utils.ViewStyleEx("FooterPanel", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(131, Byte), CType(153, Byte), CType(177, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("GroupButton", New DevExpress.Utils.ViewStyleEx("GroupButton", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(131, Byte), CType(153, Byte), CType(177, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("FilterCloseButton", New DevExpress.Utils.ViewStyleEx("FilterCloseButton", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(212, Byte), CType(208, Byte), CType(200, Byte)), System.Drawing.Color.Black, System.Drawing.Color.FromArgb(CType(90, Byte), CType(90, Byte), CType(90, Byte)), System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.dgResumen.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(131, Byte), CType(153, Byte), CType(177, Byte)), System.Drawing.Color.Black, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.dgResumen.Styles.AddReplace("HideSelectionRow", New DevExpress.Utils.ViewStyleEx("HideSelectionRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.Gray, System.Drawing.Color.FromArgb(CType(212, Byte), CType(208, Byte), CType(200, Byte)), System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("FixedLine", New DevExpress.Utils.ViewStyleEx("FixedLine", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(9, Byte), CType(31, Byte), CType(55, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(131, Byte), CType(153, Byte), CType(177, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Black, System.Drawing.Color.White, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("Empty", New DevExpress.Utils.ViewStyleEx("Empty", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(151, Byte), CType(173, Byte), CType(197, Byte)), System.Drawing.Color.White, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("GroupFooter", New DevExpress.Utils.ViewStyleEx("GroupFooter", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(141, Byte), CType(163, Byte), CType(187, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("GroupRow", New DevExpress.Utils.ViewStyleEx("GroupRow", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(79, Byte), CType(101, Byte), CType(125, Byte)), System.Drawing.Color.FromArgb(CType(193, Byte), CType(204, Byte), CType(217, Byte)), System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("HorzLine", New DevExpress.Utils.ViewStyleEx("HorzLine", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(131, Byte), CType(153, Byte), CType(177, Byte)), System.Drawing.Color.FromArgb(CType(79, Byte), CType(101, Byte), CType(125, Byte)), System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("ColumnFilterButton", New DevExpress.Utils.ViewStyleEx("ColumnFilterButton", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(131, Byte), CType(153, Byte), CType(177, Byte)), System.Drawing.Color.Gray, System.Drawing.Color.FromArgb(CType(151, Byte), CType(173, Byte), CType(197, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(79, Byte), CType(101, Byte), CType(125, Byte)), System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(129, Byte), CType(151, Byte), CType(175, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("VertLine", New DevExpress.Utils.ViewStyleEx("VertLine", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(131, Byte), CType(153, Byte), CType(177, Byte)), System.Drawing.Color.FromArgb(CType(79, Byte), CType(101, Byte), CType(125, Byte)), System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.White, System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("OddRow", New DevExpress.Utils.ViewStyleEx("OddRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(193, Byte), CType(204, Byte), CType(217, Byte)), System.Drawing.Color.Black, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal))
        Me.dgResumen.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(89, Byte), CType(111, Byte), CType(135, Byte)), System.Drawing.Color.White, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("Row", New DevExpress.Utils.ViewStyleEx("Row", "Grid", New System.Drawing.Font("Arial", 8.0!), DevExpress.Utils.StyleOptions.StyleEnabled, System.Drawing.Color.White, System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("FilterPanel", New DevExpress.Utils.ViewStyleEx("FilterPanel", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Black, System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(212, Byte), CType(208, Byte), CType(200, Byte)), System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.dgResumen.Styles.AddReplace("RowSeparator", New DevExpress.Utils.ViewStyleEx("RowSeparator", "Grid", New System.Drawing.Font("Arial", 8.0!), DevExpress.Utils.StyleOptions.StyleEnabled, System.Drawing.Color.White, System.Drawing.Color.Gray, System.Drawing.Color.FromArgb(CType(151, Byte), CType(173, Byte), CType(197, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.LightGray, System.Drawing.Color.Blue, System.Drawing.Color.WhiteSmoke, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.Styles.AddReplace("DetailTip", New DevExpress.Utils.ViewStyleEx("DetailTip", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(225, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgResumen.TabIndex = 1
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn5, Me.GridColumn4, Me.GridColumn3, Me.GridColumn2, Me.GridColumn1})
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowFilterPanel = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Factura"
        Me.GridColumn6.FieldName = "Factura"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn6.VisibleIndex = 0
        Me.GridColumn6.Width = 55
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Tipo"
        Me.GridColumn5.FieldName = "Tipo"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 46
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Moneda"
        Me.GridColumn4.FieldName = "Moneda"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 69
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Pago"
        Me.GridColumn3.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn3.FieldName = "Pago"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn3.StyleName = "Style1"
        Me.GridColumn3.VisibleIndex = 4
        Me.GridColumn3.Width = 69
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Forma Pago"
        Me.GridColumn2.FieldName = "Forma Pago"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn2.VisibleIndex = 3
        Me.GridColumn2.Width = 76
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Equivalencia"
        Me.GridColumn1.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn1.FieldName = "Equivalencia"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn1.StyleName = "Style1"
        Me.GridColumn1.VisibleIndex = 5
        Me.GridColumn1.Width = 79
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand5
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'AdapterTotalTope
        '
        Me.AdapterTotalTope.SelectCommand = Me.SqlSelectCommand6
        Me.AdapterTotalTope.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Apertura_Total_Tope", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id_total_tope", "id_total_tope"), New System.Data.Common.DataColumnMapping("NApertura", "NApertura"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Monto_Tope", "Monto_Tope"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre")})})
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT id_total_tope, NApertura, CodMoneda, Monto_Tope, MonedaNombre FROM Apertur" & _
        "a_Total_Tope"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'AdapterArqueo
        '
        Me.AdapterArqueo.SelectCommand = Me.SqlSelectCommand7
        Me.AdapterArqueo.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ArqueoCajas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("EfectivoColones", "EfectivoColones"), New System.Data.Common.DataColumnMapping("EfectivoDolares", "EfectivoDolares"), New System.Data.Common.DataColumnMapping("TarjetaColones", "TarjetaColones"), New System.Data.Common.DataColumnMapping("TarjetaDolares", "TarjetaDolares"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("IdApertura", "IdApertura"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Cajero", "Cajero"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("TipoCambioD", "TipoCambioD"), New System.Data.Common.DataColumnMapping("Cheques", "Cheques"), New System.Data.Common.DataColumnMapping("ChequesDol", "ChequesDol"), New System.Data.Common.DataColumnMapping("DepositoCol", "DepositoCol"), New System.Data.Common.DataColumnMapping("DepositoDol", "DepositoDol")})})
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT Id, EfectivoColones, EfectivoDolares, TarjetaColones, TarjetaDolares, Tota" & _
        "l, IdApertura, Fecha, Cajero, Anulado, TipoCambioD, Cheques, ChequesDol, Deposit" & _
        "oCol, DepositoDol FROM ArqueoCajas"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection1
        '
        'AdapterTarjeta
        '
        Me.AdapterTarjeta.SelectCommand = Me.SqlSelectCommand8
        Me.AdapterTarjeta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ArqueoTarjeta", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Id_Arqueo", "Id_Arqueo"), New System.Data.Common.DataColumnMapping("Id_Tarjeta", "Id_Tarjeta"), New System.Data.Common.DataColumnMapping("Monto", "Monto")})})
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT Id, Id_Arqueo, Id_Tarjeta, Monto FROM ArqueoTarjeta"
        Me.SqlSelectCommand8.Connection = Me.SqlConnection1
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label3.Location = New System.Drawing.Point(24, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(200, 16)
        Me.Label3.TabIndex = 65
        Me.Label3.Text = "Fondo Caja"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.txtTotalCompras)
        Me.GroupBox3.Controls.Add(Me.Label39)
        Me.GroupBox3.Controls.Add(Me.txtDevoluciones)
        Me.GroupBox3.Controls.Add(Me.Label38)
        Me.GroupBox3.Controls.Add(Me.txtDepositar)
        Me.GroupBox3.Controls.Add(Me.Label33)
        Me.GroupBox3.Controls.Add(Me.txtSalidas)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.txtEntradas)
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.TextEdit20)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.txtTotalCajero)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.txtTotalSistema)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.TextEdit4)
        Me.GroupBox3.Controls.Add(Me.TextEdit14)
        Me.GroupBox3.Controls.Add(Me.TxtFondoCaja)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.txtDiferenciaCaja)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox3.Location = New System.Drawing.Point(280, 48)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(488, 224)
        Me.GroupBox3.TabIndex = 66
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Cierre Caja"
        '
        'txtTotalCompras
        '
        Me.txtTotalCompras.EditValue = "0.00"
        Me.txtTotalCompras.Location = New System.Drawing.Point(8, 72)
        Me.txtTotalCompras.Name = "txtTotalCompras"
        '
        'txtTotalCompras.Properties
        '
        Me.txtTotalCompras.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtTotalCompras.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtTotalCompras.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalCompras.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalCompras.Properties.ReadOnly = True
        Me.txtTotalCompras.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtTotalCompras.Size = New System.Drawing.Size(112, 19)
        Me.txtTotalCompras.TabIndex = 95
        '
        'Label39
        '
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label39.Location = New System.Drawing.Point(16, 56)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(104, 16)
        Me.Label39.TabIndex = 94
        Me.Label39.Text = "Compras"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDevoluciones
        '
        Me.txtDevoluciones.EditValue = "0.00"
        Me.txtDevoluciones.Location = New System.Drawing.Point(8, 112)
        Me.txtDevoluciones.Name = "txtDevoluciones"
        '
        'txtDevoluciones.Properties
        '
        Me.txtDevoluciones.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtDevoluciones.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtDevoluciones.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDevoluciones.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDevoluciones.Properties.ReadOnly = True
        Me.txtDevoluciones.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtDevoluciones.Size = New System.Drawing.Size(232, 19)
        Me.txtDevoluciones.TabIndex = 93
        '
        'Label38
        '
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label38.Location = New System.Drawing.Point(24, 96)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(200, 16)
        Me.Label38.TabIndex = 92
        Me.Label38.Text = "Devoluciones"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDepositar
        '
        Me.txtDepositar.EditValue = "0.00"
        Me.txtDepositar.Location = New System.Drawing.Point(264, 192)
        Me.txtDepositar.Name = "txtDepositar"
        '
        'txtDepositar.Properties
        '
        Me.txtDepositar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtDepositar.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtDepositar.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDepositar.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDepositar.Properties.ReadOnly = True
        Me.txtDepositar.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtDepositar.Size = New System.Drawing.Size(200, 19)
        Me.txtDepositar.TabIndex = 91
        '
        'Label33
        '
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label33.Location = New System.Drawing.Point(264, 176)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(208, 16)
        Me.Label33.TabIndex = 90
        Me.Label33.Text = "Monto a Depositar"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSalidas
        '
        Me.txtSalidas.EditValue = "0.00"
        Me.txtSalidas.Location = New System.Drawing.Point(368, 112)
        Me.txtSalidas.Name = "txtSalidas"
        '
        'txtSalidas.Properties
        '
        Me.txtSalidas.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtSalidas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSalidas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSalidas.Properties.ReadOnly = True
        Me.txtSalidas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtSalidas.Size = New System.Drawing.Size(96, 19)
        Me.txtSalidas.TabIndex = 89
        '
        'Label31
        '
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label31.Location = New System.Drawing.Point(368, 96)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(96, 16)
        Me.Label31.TabIndex = 88
        Me.Label31.Text = "Salidas"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtEntradas
        '
        Me.txtEntradas.EditValue = "0.00"
        Me.txtEntradas.Location = New System.Drawing.Point(264, 112)
        Me.txtEntradas.Name = "txtEntradas"
        '
        'txtEntradas.Properties
        '
        Me.txtEntradas.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtEntradas.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtEntradas.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtEntradas.Properties.ReadOnly = True
        Me.txtEntradas.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtEntradas.Size = New System.Drawing.Size(96, 19)
        Me.txtEntradas.TabIndex = 87
        '
        'Label30
        '
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label30.Location = New System.Drawing.Point(264, 96)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 16)
        Me.Label30.TabIndex = 86
        Me.Label30.Text = "Entradas"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit20
        '
        Me.TextEdit20.EditValue = "0.00"
        Me.TextEdit20.Location = New System.Drawing.Point(264, 72)
        Me.TextEdit20.Name = "TextEdit20"
        '
        'TextEdit20.Properties
        '
        Me.TextEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit20.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit20.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit20.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit20.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit20.Properties.ReadOnly = True
        Me.TextEdit20.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit20.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit20.TabIndex = 85
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label23.Location = New System.Drawing.Point(264, 56)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(208, 16)
        Me.Label23.TabIndex = 84
        Me.Label23.Text = "Ventas Cr�dito"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTotalCajero
        '
        Me.txtTotalCajero.EditValue = "0.00"
        Me.txtTotalCajero.Location = New System.Drawing.Point(264, 152)
        Me.txtTotalCajero.Name = "txtTotalCajero"
        '
        'txtTotalCajero.Properties
        '
        Me.txtTotalCajero.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtTotalCajero.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtTotalCajero.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalCajero.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtTotalCajero.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalCajero.Properties.ReadOnly = True
        Me.txtTotalCajero.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtTotalCajero.Size = New System.Drawing.Size(200, 19)
        Me.txtTotalCajero.TabIndex = 83
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label20.Location = New System.Drawing.Point(264, 136)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(200, 16)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Total Cajero"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTotalSistema
        '
        Me.txtTotalSistema.EditValue = "0.00"
        Me.txtTotalSistema.Location = New System.Drawing.Point(8, 152)
        Me.txtTotalSistema.Name = "txtTotalSistema"
        '
        'txtTotalSistema.Properties
        '
        Me.txtTotalSistema.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtTotalSistema.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtTotalSistema.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalSistema.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtTotalSistema.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtTotalSistema.Properties.ReadOnly = True
        Me.txtTotalSistema.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtTotalSistema.Size = New System.Drawing.Size(232, 19)
        Me.txtTotalSistema.TabIndex = 81
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label13.Location = New System.Drawing.Point(24, 136)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(200, 16)
        Me.Label13.TabIndex = 80
        Me.Label13.Text = "Total Sistema"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = "0.00"
        Me.TextEdit4.Location = New System.Drawing.Point(128, 72)
        Me.TextEdit4.Name = "TextEdit4"
        '
        'TextEdit4.Properties
        '
        Me.TextEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit4.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit4.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit4.Properties.ReadOnly = True
        Me.TextEdit4.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit4.Size = New System.Drawing.Size(112, 19)
        Me.TextEdit4.TabIndex = 77
        '
        'TextEdit14
        '
        Me.TextEdit14.EditValue = "0.00"
        Me.TextEdit14.Location = New System.Drawing.Point(264, 32)
        Me.TextEdit14.Name = "TextEdit14"
        '
        'TextEdit14.Properties
        '
        Me.TextEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit14.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit14.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit14.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit14.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit14.Properties.ReadOnly = True
        Me.TextEdit14.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit14.Size = New System.Drawing.Size(200, 19)
        Me.TextEdit14.TabIndex = 76
        '
        'TxtFondoCaja
        '
        Me.TxtFondoCaja.EditValue = "0.00"
        Me.TxtFondoCaja.Location = New System.Drawing.Point(8, 32)
        Me.TxtFondoCaja.Name = "TxtFondoCaja"
        '
        'TxtFondoCaja.Properties
        '
        Me.TxtFondoCaja.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TxtFondoCaja.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtFondoCaja.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtFondoCaja.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TxtFondoCaja.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtFondoCaja.Properties.ReadOnly = True
        Me.TxtFondoCaja.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TxtFondoCaja.Size = New System.Drawing.Size(232, 19)
        Me.TxtFondoCaja.TabIndex = 72
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label11.Location = New System.Drawing.Point(120, 56)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(104, 16)
        Me.Label11.TabIndex = 67
        Me.Label11.Text = "Abonos"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label10.Location = New System.Drawing.Point(272, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(192, 16)
        Me.Label10.TabIndex = 66
        Me.Label10.Text = "Ventas Contado"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDiferenciaCaja
        '
        Me.txtDiferenciaCaja.EditValue = "0.00"
        Me.txtDiferenciaCaja.Location = New System.Drawing.Point(8, 192)
        Me.txtDiferenciaCaja.Name = "txtDiferenciaCaja"
        '
        'txtDiferenciaCaja.Properties
        '
        Me.txtDiferenciaCaja.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtDiferenciaCaja.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtDiferenciaCaja.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDiferenciaCaja.Properties.EditFormat.FormatString = "#,#0.00"
        Me.txtDiferenciaCaja.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDiferenciaCaja.Properties.ReadOnly = True
        Me.txtDiferenciaCaja.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtDiferenciaCaja.Size = New System.Drawing.Size(240, 19)
        Me.txtDiferenciaCaja.TabIndex = 83
        '
        'Label21
        '
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label21.Location = New System.Drawing.Point(24, 176)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(208, 16)
        Me.Label21.TabIndex = 82
        Me.Label21.Text = "Diferencial Caja"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit2
        '
        Me.TextEdit2.EditValue = "0.00"
        Me.TextEdit2.Location = New System.Drawing.Point(8, 88)
        Me.TextEdit2.Name = "TextEdit2"
        '
        'TextEdit2.Properties
        '
        Me.TextEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit2.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit2.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit2.Properties.ReadOnly = True
        Me.TextEdit2.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit2.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit2.TabIndex = 71
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "0.00"
        Me.TextEdit1.Location = New System.Drawing.Point(8, 48)
        Me.TextEdit1.Name = "TextEdit1"
        '
        'TextEdit1.Properties
        '
        Me.TextEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit1.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit1.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit1.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit1.TabIndex = 70
        '
        'AdapterTipoTarjeta
        '
        Me.AdapterTipoTarjeta.SelectCommand = Me.SqlSelectCommand9
        Me.AdapterTipoTarjeta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TipoTarjeta", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Moneda", "Moneda")})})
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT Id, Nombre, Moneda FROM TipoTarjeta"
        Me.SqlSelectCommand9.Connection = Me.SqlConnection1
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.GridControl2)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox5.Location = New System.Drawing.Point(280, 280)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(488, 104)
        Me.GroupBox5.TabIndex = 67
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Detalles Trajetas de Cr�dito"
        '
        'GridControl2
        '
        Me.GridControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl2.DataMember = "TipoTarjeta"
        Me.GridControl2.DataSource = Me.DataSetCierreCaja1
        '
        'GridControl2.EmbeddedNavigator
        '
        Me.GridControl2.EmbeddedNavigator.Name = ""
        Me.GridControl2.Location = New System.Drawing.Point(8, 16)
        Me.GridControl2.MainView = Me.GridView1
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(472, 80)
        Me.GridControl2.Styles.AddReplace("Preview", New DevExpress.Utils.ViewStyleEx("Preview", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, True, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.Color.FromArgb(CType(217, Byte), CType(245, Byte), CType(255, Byte)), System.Drawing.Color.FromArgb(CType(85, Byte), CType(128, Byte), CType(151, Byte)), System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("FooterPanel", New DevExpress.Utils.ViewStyleEx("FooterPanel", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(157, Byte), CType(185, Byte), CType(200, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("GroupButton", New DevExpress.Utils.ViewStyleEx("GroupButton", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(157, Byte), CType(185, Byte), CType(200, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("FilterCloseButton", New DevExpress.Utils.ViewStyleEx("FilterCloseButton", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(212, Byte), CType(208, Byte), CType(200, Byte)), System.Drawing.Color.Black, System.Drawing.Color.FromArgb(CType(125, Byte), CType(125, Byte), CType(125, Byte)), System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.GridControl2.Styles.AddReplace("EvenRow", New DevExpress.Utils.ViewStyleEx("EvenRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(157, Byte), CType(185, Byte), CType(200, Byte)), System.Drawing.Color.Black, System.Drawing.Color.GhostWhite, System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.GridControl2.Styles.AddReplace("HideSelectionRow", New DevExpress.Utils.ViewStyleEx("HideSelectionRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.Gray, System.Drawing.Color.FromArgb(CType(212, Byte), CType(208, Byte), CType(200, Byte)), System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("Style2", New DevExpress.Utils.ViewStyleEx("Style2", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("FixedLine", New DevExpress.Utils.ViewStyleEx("FixedLine", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(15, Byte), CType(58, Byte), CType(81, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("HeaderPanel", New DevExpress.Utils.ViewStyleEx("HeaderPanel", "Grid", New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(157, Byte), CType(185, Byte), CType(200, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("GroupPanel", New DevExpress.Utils.ViewStyleEx("GroupPanel", "Grid", New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.SteelBlue, System.Drawing.Color.White, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("Empty", New DevExpress.Utils.ViewStyleEx("Empty", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(177, Byte), CType(205, Byte), CType(220, Byte)), System.Drawing.Color.White, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("GroupFooter", New DevExpress.Utils.ViewStyleEx("GroupFooter", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(167, Byte), CType(195, Byte), CType(210, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("GroupRow", New DevExpress.Utils.ViewStyleEx("GroupRow", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(85, Byte), CType(128, Byte), CType(151, Byte)), System.Drawing.Color.Silver, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("HorzLine", New DevExpress.Utils.ViewStyleEx("HorzLine", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(157, Byte), CType(185, Byte), CType(200, Byte)), System.Drawing.Color.FromArgb(CType(85, Byte), CType(128, Byte), CType(151, Byte)), System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("ColumnFilterButton", New DevExpress.Utils.ViewStyleEx("ColumnFilterButton", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(157, Byte), CType(185, Byte), CType(200, Byte)), System.Drawing.Color.Gray, System.Drawing.Color.FromArgb(CType(177, Byte), CType(205, Byte), CType(220, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("FocusedRow", New DevExpress.Utils.ViewStyleEx("FocusedRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(85, Byte), CType(128, Byte), CType(151, Byte)), System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(135, Byte), CType(178, Byte), CType(201, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("VertLine", New DevExpress.Utils.ViewStyleEx("VertLine", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(157, Byte), CType(185, Byte), CType(200, Byte)), System.Drawing.Color.FromArgb(CType(85, Byte), CType(128, Byte), CType(151, Byte)), System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("FocusedCell", New DevExpress.Utils.ViewStyleEx("FocusedCell", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.White, System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("OddRow", New DevExpress.Utils.ViewStyleEx("OddRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(206, Byte), CType(220, Byte), CType(227, Byte)), System.Drawing.Color.Black, System.Drawing.Color.White, System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal))
        Me.GridControl2.Styles.AddReplace("SelectedRow", New DevExpress.Utils.ViewStyleEx("SelectedRow", "Grid", New System.Drawing.Font("Arial", 8.0!), CType((((((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                            Or DevExpress.Utils.StyleOptions.UseDrawFocusRect) _
                            Or DevExpress.Utils.StyleOptions.UseFont) _
                            Or DevExpress.Utils.StyleOptions.UseForeColor) _
                            Or DevExpress.Utils.StyleOptions.UseImage), DevExpress.Utils.StyleOptions), System.Drawing.Color.FromArgb(CType(95, Byte), CType(138, Byte), CType(161, Byte)), System.Drawing.Color.White, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("Row", New DevExpress.Utils.ViewStyleEx("Row", "Grid", New System.Drawing.Font("Arial", 8.0!), DevExpress.Utils.StyleOptions.StyleEnabled, System.Drawing.Color.White, System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("FilterPanel", New DevExpress.Utils.ViewStyleEx("FilterPanel", "Grid", New System.Drawing.Font("Arial", 8.0!), "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.FromArgb(CType(35, Byte), CType(35, Byte), CType(35, Byte)), System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(212, Byte), CType(208, Byte), CType(200, Byte)), System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal))
        Me.GridControl2.Styles.AddReplace("RowSeparator", New DevExpress.Utils.ViewStyleEx("RowSeparator", "Grid", New System.Drawing.Font("Arial", 8.0!), DevExpress.Utils.StyleOptions.StyleEnabled, System.Drawing.Color.White, System.Drawing.Color.Gray, System.Drawing.Color.FromArgb(CType(177, Byte), CType(205, Byte), CType(220, Byte)), System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.LightGray, System.Drawing.Color.Blue, System.Drawing.Color.WhiteSmoke, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("DetailTip", New DevExpress.Utils.ViewStyleEx("DetailTip", "Grid", New System.Drawing.Font("Arial", 8.0!), System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(225, Byte)), System.Drawing.Color.Black, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.TabIndex = 12
        Me.GridControl2.Text = "GridControl2"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.GridView1.GroupPanelText = "Agrupe de acuerdo a una columna si lo desea"
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Tarjeta"
        Me.GridColumn7.FieldName = "Nombre"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn7.VisibleIndex = 0
        Me.GridColumn7.Width = 125
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Total Cajero"
        Me.GridColumn8.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "Total"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn8.VisibleIndex = 2
        Me.GridColumn8.Width = 118
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Moneda"
        Me.GridColumn9.FieldName = "Monedas"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn9.VisibleIndex = 1
        Me.GridColumn9.Width = 102
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Total Sistema"
        Me.GridColumn10.DisplayFormat.FormatString = "#,#0.00"
        Me.GridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn10.FieldName = "TotalS"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.GridColumn10.VisibleIndex = 3
        Me.GridColumn10.Width = 113
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.txtDepDolSistema)
        Me.GroupBox6.Controls.Add(Me.txtDepDolCajero)
        Me.GroupBox6.Controls.Add(Me.Label37)
        Me.GroupBox6.Controls.Add(Me.TxtDepoColSistena)
        Me.GroupBox6.Controls.Add(Me.txtDepoColCajero)
        Me.GroupBox6.Controls.Add(Me.Label35)
        Me.GroupBox6.Controls.Add(Me.txtChequesDolSistema)
        Me.GroupBox6.Controls.Add(Me.txtChequesDolCajero)
        Me.GroupBox6.Controls.Add(Me.Label34)
        Me.GroupBox6.Controls.Add(Me.txtChequesSistema)
        Me.GroupBox6.Controls.Add(Me.txtChequesCajero)
        Me.GroupBox6.Controls.Add(Me.Label32)
        Me.GroupBox6.Controls.Add(Me.Label22)
        Me.GroupBox6.Controls.Add(Me.TextEdit18)
        Me.GroupBox6.Controls.Add(Me.TextEdit19)
        Me.GroupBox6.Controls.Add(Me.TextEdit10)
        Me.GroupBox6.Controls.Add(Me.TextEdit11)
        Me.GroupBox6.Controls.Add(Me.TextEdit8)
        Me.GroupBox6.Controls.Add(Me.TextEdit9)
        Me.GroupBox6.Controls.Add(Me.TextEdit7)
        Me.GroupBox6.Controls.Add(Me.Label18)
        Me.GroupBox6.Controls.Add(Me.Label17)
        Me.GroupBox6.Controls.Add(Me.TextEdit5)
        Me.GroupBox6.Controls.Add(Me.Label16)
        Me.GroupBox6.Controls.Add(Me.Label15)
        Me.GroupBox6.Controls.Add(Me.Label14)
        Me.GroupBox6.Controls.Add(Me.TextEdit1)
        Me.GroupBox6.Controls.Add(Me.TextEdit2)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox6.Location = New System.Drawing.Point(8, 184)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(256, 400)
        Me.GroupBox6.TabIndex = 68
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "General"
        '
        'txtDepDolSistema
        '
        Me.txtDepDolSistema.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.DepositosDol"))
        Me.txtDepDolSistema.EditValue = "0.00"
        Me.txtDepDolSistema.Location = New System.Drawing.Point(128, 328)
        Me.txtDepDolSistema.Name = "txtDepDolSistema"
        '
        'txtDepDolSistema.Properties
        '
        Me.txtDepDolSistema.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtDepDolSistema.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtDepDolSistema.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDepDolSistema.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDepDolSistema.Properties.ReadOnly = True
        Me.txtDepDolSistema.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtDepDolSistema.Size = New System.Drawing.Size(120, 19)
        Me.txtDepDolSistema.TabIndex = 105
        '
        'txtDepDolCajero
        '
        Me.txtDepDolCajero.EditValue = "0.00"
        Me.txtDepDolCajero.Location = New System.Drawing.Point(8, 328)
        Me.txtDepDolCajero.Name = "txtDepDolCajero"
        '
        'txtDepDolCajero.Properties
        '
        Me.txtDepDolCajero.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtDepDolCajero.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtDepDolCajero.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDepDolCajero.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDepDolCajero.Properties.ReadOnly = True
        Me.txtDepDolCajero.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtDepDolCajero.Size = New System.Drawing.Size(120, 19)
        Me.txtDepDolCajero.TabIndex = 104
        '
        'Label37
        '
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label37.Location = New System.Drawing.Point(8, 312)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(240, 16)
        Me.Label37.TabIndex = 103
        Me.Label37.Text = "Depositos Dolares"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtDepoColSistena
        '
        Me.TxtDepoColSistena.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.DepositosCol"))
        Me.TxtDepoColSistena.EditValue = "0.00"
        Me.TxtDepoColSistena.Location = New System.Drawing.Point(128, 288)
        Me.TxtDepoColSistena.Name = "TxtDepoColSistena"
        '
        'TxtDepoColSistena.Properties
        '
        Me.TxtDepoColSistena.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TxtDepoColSistena.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TxtDepoColSistena.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtDepoColSistena.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TxtDepoColSistena.Properties.ReadOnly = True
        Me.TxtDepoColSistena.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TxtDepoColSistena.Size = New System.Drawing.Size(120, 19)
        Me.TxtDepoColSistena.TabIndex = 102
        '
        'txtDepoColCajero
        '
        Me.txtDepoColCajero.EditValue = "0.00"
        Me.txtDepoColCajero.Location = New System.Drawing.Point(8, 288)
        Me.txtDepoColCajero.Name = "txtDepoColCajero"
        '
        'txtDepoColCajero.Properties
        '
        Me.txtDepoColCajero.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtDepoColCajero.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtDepoColCajero.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDepoColCajero.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDepoColCajero.Properties.ReadOnly = True
        Me.txtDepoColCajero.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtDepoColCajero.Size = New System.Drawing.Size(120, 19)
        Me.txtDepoColCajero.TabIndex = 101
        '
        'Label35
        '
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label35.Location = New System.Drawing.Point(8, 272)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(240, 16)
        Me.Label35.TabIndex = 100
        Me.Label35.Text = "Depositos Colones"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtChequesDolSistema
        '
        Me.txtChequesDolSistema.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.ChequeDol"))
        Me.txtChequesDolSistema.EditValue = "0.00"
        Me.txtChequesDolSistema.Location = New System.Drawing.Point(128, 248)
        Me.txtChequesDolSistema.Name = "txtChequesDolSistema"
        '
        'txtChequesDolSistema.Properties
        '
        Me.txtChequesDolSistema.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtChequesDolSistema.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtChequesDolSistema.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtChequesDolSistema.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtChequesDolSistema.Properties.ReadOnly = True
        Me.txtChequesDolSistema.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtChequesDolSistema.Size = New System.Drawing.Size(120, 19)
        Me.txtChequesDolSistema.TabIndex = 99
        '
        'txtChequesDolCajero
        '
        Me.txtChequesDolCajero.EditValue = "0.00"
        Me.txtChequesDolCajero.Location = New System.Drawing.Point(8, 248)
        Me.txtChequesDolCajero.Name = "txtChequesDolCajero"
        '
        'txtChequesDolCajero.Properties
        '
        Me.txtChequesDolCajero.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtChequesDolCajero.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtChequesDolCajero.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtChequesDolCajero.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtChequesDolCajero.Properties.ReadOnly = True
        Me.txtChequesDolCajero.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtChequesDolCajero.Size = New System.Drawing.Size(120, 19)
        Me.txtChequesDolCajero.TabIndex = 98
        '
        'Label34
        '
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label34.Location = New System.Drawing.Point(8, 232)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(240, 16)
        Me.Label34.TabIndex = 97
        Me.Label34.Text = "Cheques Dolares"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtChequesSistema
        '
        Me.txtChequesSistema.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.Cheques"))
        Me.txtChequesSistema.EditValue = "0.00"
        Me.txtChequesSistema.Location = New System.Drawing.Point(128, 208)
        Me.txtChequesSistema.Name = "txtChequesSistema"
        '
        'txtChequesSistema.Properties
        '
        Me.txtChequesSistema.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtChequesSistema.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtChequesSistema.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtChequesSistema.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtChequesSistema.Properties.ReadOnly = True
        Me.txtChequesSistema.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtChequesSistema.Size = New System.Drawing.Size(120, 19)
        Me.txtChequesSistema.TabIndex = 96
        '
        'txtChequesCajero
        '
        Me.txtChequesCajero.EditValue = "0.00"
        Me.txtChequesCajero.Location = New System.Drawing.Point(8, 208)
        Me.txtChequesCajero.Name = "txtChequesCajero"
        '
        'txtChequesCajero.Properties
        '
        Me.txtChequesCajero.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtChequesCajero.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.txtChequesCajero.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtChequesCajero.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtChequesCajero.Properties.ReadOnly = True
        Me.txtChequesCajero.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.txtChequesCajero.Size = New System.Drawing.Size(120, 19)
        Me.txtChequesCajero.TabIndex = 95
        '
        'Label32
        '
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label32.Location = New System.Drawing.Point(8, 192)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(240, 16)
        Me.Label32.TabIndex = 94
        Me.Label32.Text = "Cheques Colones"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(8, 352)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(248, 16)
        Me.Label22.TabIndex = 93
        Me.Label22.Text = "      Total Cajero      /      Total Sistema"
        '
        'TextEdit18
        '
        Me.TextEdit18.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.TotalSistema"))
        Me.TextEdit18.EditValue = "0.00"
        Me.TextEdit18.Location = New System.Drawing.Point(128, 376)
        Me.TextEdit18.Name = "TextEdit18"
        '
        'TextEdit18.Properties
        '
        Me.TextEdit18.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit18.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit18.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit18.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit18.Properties.ReadOnly = True
        Me.TextEdit18.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit18.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit18.TabIndex = 92
        '
        'TextEdit19
        '
        Me.TextEdit19.EditValue = "0.00"
        Me.TextEdit19.Location = New System.Drawing.Point(8, 376)
        Me.TextEdit19.Name = "TextEdit19"
        '
        'TextEdit19.Properties
        '
        Me.TextEdit19.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit19.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit19.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit19.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit19.Properties.ReadOnly = True
        Me.TextEdit19.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit19.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit19.TabIndex = 91
        '
        'TextEdit10
        '
        Me.TextEdit10.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.TarjetaDolares"))
        Me.TextEdit10.EditValue = "0.00"
        Me.TextEdit10.Location = New System.Drawing.Point(128, 168)
        Me.TextEdit10.Name = "TextEdit10"
        '
        'TextEdit10.Properties
        '
        Me.TextEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit10.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit10.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit10.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit10.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit10.Properties.ReadOnly = True
        Me.TextEdit10.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit10.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit10.TabIndex = 87
        '
        'TextEdit11
        '
        Me.TextEdit11.EditValue = "0.00"
        Me.TextEdit11.Location = New System.Drawing.Point(8, 168)
        Me.TextEdit11.Name = "TextEdit11"
        '
        'TextEdit11.Properties
        '
        Me.TextEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit11.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit11.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit11.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit11.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit11.Properties.ReadOnly = True
        Me.TextEdit11.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit11.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit11.TabIndex = 86
        '
        'TextEdit8
        '
        Me.TextEdit8.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.TarjetaColones"))
        Me.TextEdit8.EditValue = "0.00"
        Me.TextEdit8.Location = New System.Drawing.Point(128, 128)
        Me.TextEdit8.Name = "TextEdit8"
        '
        'TextEdit8.Properties
        '
        Me.TextEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit8.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit8.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit8.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit8.Properties.ReadOnly = True
        Me.TextEdit8.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit8.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit8.TabIndex = 85
        '
        'TextEdit9
        '
        Me.TextEdit9.EditValue = "0.00"
        Me.TextEdit9.Location = New System.Drawing.Point(8, 128)
        Me.TextEdit9.Name = "TextEdit9"
        '
        'TextEdit9.Properties
        '
        Me.TextEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit9.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit9.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit9.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit9.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit9.Properties.ReadOnly = True
        Me.TextEdit9.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit9.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit9.TabIndex = 84
        '
        'TextEdit7
        '
        Me.TextEdit7.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.EfectivoDolares"))
        Me.TextEdit7.EditValue = "0.00"
        Me.TextEdit7.Location = New System.Drawing.Point(128, 88)
        Me.TextEdit7.Name = "TextEdit7"
        '
        'TextEdit7.Properties
        '
        Me.TextEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit7.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit7.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit7.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit7.Properties.ReadOnly = True
        Me.TextEdit7.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit7.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit7.TabIndex = 83
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label18.Location = New System.Drawing.Point(8, 152)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(240, 16)
        Me.Label18.TabIndex = 81
        Me.Label18.Text = "Tarjeta Dolares"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label17.Location = New System.Drawing.Point(8, 112)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(240, 16)
        Me.Label17.TabIndex = 80
        Me.Label17.Text = "Tarjeta Colones"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextEdit5
        '
        Me.TextEdit5.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.EfectivoColones"))
        Me.TextEdit5.EditValue = "0.00"
        Me.TextEdit5.Location = New System.Drawing.Point(128, 48)
        Me.TextEdit5.Name = "TextEdit5"
        '
        'TextEdit5.Properties
        '
        Me.TextEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.TextEdit5.Properties.DisplayFormat.FormatString = "#,#0.00"
        Me.TextEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit5.Properties.EditFormat.FormatString = "#,#0.00"
        Me.TextEdit5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit5.Properties.ReadOnly = True
        Me.TextEdit5.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
        Me.TextEdit5.Size = New System.Drawing.Size(120, 19)
        Me.TextEdit5.TabIndex = 79
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label16.Location = New System.Drawing.Point(8, 72)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(240, 16)
        Me.Label16.TabIndex = 78
        Me.Label16.Text = "Efectivo Dolares"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(3, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(248, 18)
        Me.Label15.TabIndex = 77
        Me.Label15.Text = "           Cajero          /          Sistema"
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label14.Location = New System.Drawing.Point(8, 32)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(240, 16)
        Me.Label14.TabIndex = 76
        Me.Label14.Text = "Efectivo Colones"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AdapterDetallePago
        '
        Me.AdapterDetallePago.SelectCommand = Me.SqlSelectCommand10
        Me.AdapterDetallePago.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Detalle_pago_caja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumeroFactura", "NumeroFactura"), New System.Data.Common.DataColumnMapping("TipoFactura", "TipoFactura"), New System.Data.Common.DataColumnMapping("FormaPago", "FormaPago"), New System.Data.Common.DataColumnMapping("Referencia", "Referencia"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("ReferenciaTipo", "ReferenciaTipo"), New System.Data.Common.DataColumnMapping("ReferenciaDoc", "ReferenciaDoc"), New System.Data.Common.DataColumnMapping("Moneda", "Moneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Id_ODP", "Id_ODP")})})
        '
        'SqlSelectCommand10
        '
        Me.SqlSelectCommand10.CommandText = "SELECT NumeroFactura, TipoFactura, FormaPago, Referencia, Documento, ReferenciaTi" & _
        "po, ReferenciaDoc, Moneda, TipoCambio, Id, Id_ODP FROM Detalle_pago_caja"
        Me.SqlSelectCommand10.Connection = Me.SqlConnection1
        '
        'AdapterVentasContado
        '
        Me.AdapterVentasContado.SelectCommand = Me.SqlSelectCommand11
        Me.AdapterVentasContado.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VentasContado", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Num_Apertura", "Num_Apertura"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda")})})
        '
        'SqlSelectCommand11
        '
        Me.SqlSelectCommand11.CommandText = "SELECT Num_Factura, Total, Num_Apertura, Anulado, Tipo, Cod_Moneda FROM VentasCon" & _
        "tado"
        Me.SqlSelectCommand11.Connection = Me.SqlConnection1
        '
        'AdapterCierreMonto
        '
        Me.AdapterCierreMonto.DeleteCommand = Me.SqlDeleteCommand3
        Me.AdapterCierreMonto.InsertCommand = Me.SqlInsertCommand3
        Me.AdapterCierreMonto.SelectCommand = Me.SqlSelectCommand14
        Me.AdapterCierreMonto.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "CierreCaja_DetMon", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_DetaMoneda", "Id_DetaMoneda"), New System.Data.Common.DataColumnMapping("Id_CierreCaja", "Id_CierreCaja"), New System.Data.Common.DataColumnMapping("Id_Moneda", "Id_Moneda"), New System.Data.Common.DataColumnMapping("MontoSistema", "MontoSistema"), New System.Data.Common.DataColumnMapping("MontoCajero", "MontoCajero")})})
        Me.AdapterCierreMonto.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM CierreCaja_DetMon WHERE (Id_DetaMoneda = @Original_Id_DetaMoneda) AND" & _
        " (Id_CierreCaja = @Original_Id_CierreCaja) AND (Id_Moneda = @Original_Id_Moneda)" & _
        " AND (MontoCajero = @Original_MontoCajero) AND (MontoSistema = @Original_MontoSi" & _
        "stema)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_DetaMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_DetaMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_CierreCaja", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CierreCaja", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoCajero", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCajero", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoSistema", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO CierreCaja_DetMon(Id_CierreCaja, Id_Moneda, MontoSistema, MontoCajero" & _
        ") VALUES (@Id_CierreCaja, @Id_Moneda, @MontoSistema, @MontoCajero); SELECT Id_De" & _
        "taMoneda, Id_CierreCaja, Id_Moneda, MontoSistema, MontoCajero FROM CierreCaja_De" & _
        "tMon WHERE (Id_DetaMoneda = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_CierreCaja", System.Data.SqlDbType.Int, 4, "Id_CierreCaja"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Moneda", System.Data.SqlDbType.Int, 4, "Id_Moneda"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoSistema", System.Data.SqlDbType.Float, 8, "MontoSistema"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoCajero", System.Data.SqlDbType.Float, 8, "MontoCajero"))
        '
        'SqlSelectCommand14
        '
        Me.SqlSelectCommand14.CommandText = "SELECT Id_DetaMoneda, Id_CierreCaja, Id_Moneda, MontoSistema, MontoCajero FROM Ci" & _
        "erreCaja_DetMon"
        Me.SqlSelectCommand14.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE CierreCaja_DetMon SET Id_CierreCaja = @Id_CierreCaja, Id_Moneda = @Id_Mone" & _
        "da, MontoSistema = @MontoSistema, MontoCajero = @MontoCajero WHERE (Id_DetaMoned" & _
        "a = @Original_Id_DetaMoneda) AND (Id_CierreCaja = @Original_Id_CierreCaja) AND (" & _
        "Id_Moneda = @Original_Id_Moneda) AND (MontoCajero = @Original_MontoCajero) AND (" & _
        "MontoSistema = @Original_MontoSistema); SELECT Id_DetaMoneda, Id_CierreCaja, Id_" & _
        "Moneda, MontoSistema, MontoCajero FROM CierreCaja_DetMon WHERE (Id_DetaMoneda = " & _
        "@Id_DetaMoneda)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_CierreCaja", System.Data.SqlDbType.Int, 4, "Id_CierreCaja"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Moneda", System.Data.SqlDbType.Int, 4, "Id_Moneda"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoSistema", System.Data.SqlDbType.Float, 8, "MontoSistema"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoCajero", System.Data.SqlDbType.Float, 8, "MontoCajero"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_DetaMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_DetaMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_CierreCaja", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CierreCaja", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoCajero", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCajero", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoSistema", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_DetaMoneda", System.Data.SqlDbType.Int, 4, "Id_DetaMoneda"))
        '
        'AdapterCierreTarjeta
        '
        Me.AdapterCierreTarjeta.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterCierreTarjeta.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterCierreTarjeta.SelectCommand = Me.SqlSelectCommand15
        Me.AdapterCierreTarjeta.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "CierreCaja_DetTarj", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_DetalleTarjeta", "Id_DetalleTarjeta"), New System.Data.Common.DataColumnMapping("Id_CierreCaja", "Id_CierreCaja"), New System.Data.Common.DataColumnMapping("Id_TipoTarjeta", "Id_TipoTarjeta"), New System.Data.Common.DataColumnMapping("MontoSistema", "MontoSistema"), New System.Data.Common.DataColumnMapping("MontoCajero", "MontoCajero")})})
        Me.AdapterCierreTarjeta.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM CierreCaja_DetTarj WHERE (Id_DetalleTarjeta = @Original_Id_DetalleTar" & _
        "jeta) AND (Id_CierreCaja = @Original_Id_CierreCaja) AND (Id_TipoTarjeta = @Origi" & _
        "nal_Id_TipoTarjeta) AND (MontoCajero = @Original_MontoCajero) AND (MontoSistema " & _
        "= @Original_MontoSistema)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_DetalleTarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_DetalleTarjeta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_CierreCaja", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CierreCaja", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_TipoTarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_TipoTarjeta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoCajero", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCajero", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoSistema", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO CierreCaja_DetTarj(Id_CierreCaja, Id_TipoTarjeta, MontoSistema, Monto" & _
        "Cajero) VALUES (@Id_CierreCaja, @Id_TipoTarjeta, @MontoSistema, @MontoCajero); S" & _
        "ELECT Id_DetalleTarjeta, Id_CierreCaja, Id_TipoTarjeta, MontoSistema, MontoCajer" & _
        "o FROM CierreCaja_DetTarj WHERE (Id_DetalleTarjeta = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_CierreCaja", System.Data.SqlDbType.Int, 4, "Id_CierreCaja"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_TipoTarjeta", System.Data.SqlDbType.Int, 4, "Id_TipoTarjeta"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoSistema", System.Data.SqlDbType.Float, 8, "MontoSistema"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoCajero", System.Data.SqlDbType.Float, 8, "MontoCajero"))
        '
        'SqlSelectCommand15
        '
        Me.SqlSelectCommand15.CommandText = "SELECT Id_DetalleTarjeta, Id_CierreCaja, Id_TipoTarjeta, MontoSistema, MontoCajer" & _
        "o FROM CierreCaja_DetTarj"
        Me.SqlSelectCommand15.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE CierreCaja_DetTarj SET Id_CierreCaja = @Id_CierreCaja, Id_TipoTarjeta = @I" & _
        "d_TipoTarjeta, MontoSistema = @MontoSistema, MontoCajero = @MontoCajero WHERE (I" & _
        "d_DetalleTarjeta = @Original_Id_DetalleTarjeta) AND (Id_CierreCaja = @Original_I" & _
        "d_CierreCaja) AND (Id_TipoTarjeta = @Original_Id_TipoTarjeta) AND (MontoCajero =" & _
        " @Original_MontoCajero) AND (MontoSistema = @Original_MontoSistema); SELECT Id_D" & _
        "etalleTarjeta, Id_CierreCaja, Id_TipoTarjeta, MontoSistema, MontoCajero FROM Cie" & _
        "rreCaja_DetTarj WHERE (Id_DetalleTarjeta = @Id_DetalleTarjeta)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_CierreCaja", System.Data.SqlDbType.Int, 4, "Id_CierreCaja"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_TipoTarjeta", System.Data.SqlDbType.Int, 4, "Id_TipoTarjeta"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoSistema", System.Data.SqlDbType.Float, 8, "MontoSistema"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoCajero", System.Data.SqlDbType.Float, 8, "MontoCajero"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_DetalleTarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_DetalleTarjeta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_CierreCaja", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CierreCaja", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_TipoTarjeta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_TipoTarjeta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoCajero", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCajero", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoSistema", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoSistema", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_DetalleTarjeta", System.Data.SqlDbType.Int, 4, "Id_DetalleTarjeta"))
        '
        'lblanulado
        '
        Me.lblanulado.BackColor = System.Drawing.Color.Transparent
        Me.lblanulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 34.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblanulado.ForeColor = System.Drawing.Color.Red
        Me.lblanulado.Location = New System.Drawing.Point(272, 288)
        Me.lblanulado.Name = "lblanulado"
        Me.lblanulado.Size = New System.Drawing.Size(320, 96)
        Me.lblanulado.TabIndex = 69
        Me.lblanulado.Text = "Anulado"
        Me.lblanulado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblanulado.Visible = False
        '
        'AdapterVentasCredito
        '
        Me.AdapterVentasCredito.SelectCommand = Me.SqlSelectCommand12
        Me.AdapterVentasCredito.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VentasCredito", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Expr1", "Expr1"), New System.Data.Common.DataColumnMapping("SubTotal", "SubTotal"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Num_Apertura", "Num_Apertura"), New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Cod_Cliente", "Cod_Cliente")})})
        '
        'SqlSelectCommand12
        '
        Me.SqlSelectCommand12.CommandText = "SELECT Expr1, SubTotal, Total, Num_Factura, Num_Apertura, Id, Cod_Moneda, Cod_Cli" & _
        "ente FROM VentasCredito"
        Me.SqlSelectCommand12.Connection = Me.SqlConnection1
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.ButtonBuscarCajero)
        Me.GroupBox1.Controls.Add(Me.DateEdit1)
        Me.GroupBox1.Controls.Add(Me.txtnomcajero)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.txtcodcajero)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.TimeEdit3)
        Me.GroupBox1.Controls.Add(Me.TimeEdit4)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.Label29)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox1.Location = New System.Drawing.Point(8, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(256, 88)
        Me.GroupBox1.TabIndex = 70
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Per�odo Cierre"
        '
        'ButtonBuscarCajero
        '
        Me.ButtonBuscarCajero.Location = New System.Drawing.Point(119, 30)
        Me.ButtonBuscarCajero.Name = "ButtonBuscarCajero"
        Me.ButtonBuscarCajero.Size = New System.Drawing.Size(29, 19)
        Me.ButtonBuscarCajero.TabIndex = 15
        Me.ButtonBuscarCajero.Text = "F1"
        '
        'DateEdit1
        '
        Me.DateEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetCierreCaja1, "cierrecaja.Fecha"))
        Me.DateEdit1.EditValue = New Date(2006, 5, 10, 0, 0, 0, 0)
        Me.DateEdit1.Location = New System.Drawing.Point(155, 30)
        Me.DateEdit1.Name = "DateEdit1"
        '
        'DateEdit1.Properties
        '
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.Enabled = False
        Me.DateEdit1.Size = New System.Drawing.Size(88, 21)
        Me.DateEdit1.TabIndex = 5
        '
        'txtnomcajero
        '
        Me.txtnomcajero.BackColor = System.Drawing.SystemColors.Control
        Me.txtnomcajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetCierreCaja1, "cierrecaja.Nombre"))
        Me.txtnomcajero.Location = New System.Drawing.Point(8, 67)
        Me.txtnomcajero.Name = "txtnomcajero"
        Me.txtnomcajero.Size = New System.Drawing.Size(240, 16)
        Me.txtnomcajero.TabIndex = 14
        Me.txtnomcajero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label12.Location = New System.Drawing.Point(155, 14)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 16)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Fecha Cierre"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label19.Location = New System.Drawing.Point(8, 14)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(112, 16)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "C�digo Cajero"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtcodcajero
        '
        Me.txtcodcajero.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.txtcodcajero.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtcodcajero.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetCierreCaja1, "cierrecaja.Cajera"))
        Me.txtcodcajero.ForeColor = System.Drawing.Color.Blue
        Me.txtcodcajero.Location = New System.Drawing.Point(8, 30)
        Me.txtcodcajero.Multiline = True
        Me.txtcodcajero.Name = "txtcodcajero"
        Me.txtcodcajero.ReadOnly = True
        Me.txtcodcajero.Size = New System.Drawing.Size(104, 16)
        Me.txtcodcajero.TabIndex = 4
        Me.txtcodcajero.Text = ""
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label24.Location = New System.Drawing.Point(8, 51)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(240, 16)
        Me.Label24.TabIndex = 9
        Me.Label24.Text = "Nombre"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimeEdit3
        '
        Me.TimeEdit3.EditValue = New Date(2006, 3, 15, 11, 33, 32, 375)
        Me.TimeEdit3.Location = New System.Drawing.Point(496, 56)
        Me.TimeEdit3.Name = "TimeEdit3"
        '
        'TimeEdit3.Properties
        '
        Me.TimeEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.TimeEdit3.Properties.UseCtrlIncrement = False
        Me.TimeEdit3.Size = New System.Drawing.Size(96, 21)
        Me.TimeEdit3.TabIndex = 12
        '
        'TimeEdit4
        '
        Me.TimeEdit4.EditValue = New Date(2006, 3, 15, 11, 33, 32, 375)
        Me.TimeEdit4.Location = New System.Drawing.Point(496, 32)
        Me.TimeEdit4.Name = "TimeEdit4"
        '
        'TimeEdit4.Properties
        '
        Me.TimeEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.TimeEdit4.Properties.UseCtrlIncrement = False
        Me.TimeEdit4.Size = New System.Drawing.Size(96, 21)
        Me.TimeEdit4.TabIndex = 8
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Enabled = False
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DateTimePicker1.Location = New System.Drawing.Point(360, 56)
        Me.DateTimePicker1.MaxDate = New Date(2017, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker1.MinDate = New Date(2006, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePicker1.TabIndex = 11
        Me.DateTimePicker1.Value = New Date(2006, 1, 1, 0, 0, 0, 0)
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Enabled = False
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DateTimePicker2.Location = New System.Drawing.Point(360, 32)
        Me.DateTimePicker2.MaxDate = New Date(2017, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker2.MinDate = New Date(2006, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePicker2.TabIndex = 7
        Me.DateTimePicker2.Value = New Date(2006, 1, 1, 0, 0, 0, 0)
        '
        'Label26
        '
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label26.Location = New System.Drawing.Point(304, 56)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(40, 16)
        Me.Label26.TabIndex = 10
        Me.Label26.Text = "Final"
        '
        'Label27
        '
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label27.Location = New System.Drawing.Point(304, 32)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(48, 16)
        Me.Label27.TabIndex = 6
        Me.Label27.Text = "Inicial"
        '
        'Label28
        '
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label28.Location = New System.Drawing.Point(496, 16)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(96, 16)
        Me.Label28.TabIndex = 3
        Me.Label28.Text = "Hora"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label29
        '
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label29.Location = New System.Drawing.Point(368, 16)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(96, 16)
        Me.Label29.TabIndex = 2
        Me.Label29.Text = "Fecha"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(624, 24)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(32, 56)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "OK"
        '
        'CK_PVE
        '
        Me.CK_PVE.BackColor = System.Drawing.Color.Transparent
        Me.CK_PVE.Location = New System.Drawing.Point(721, 32)
        Me.CK_PVE.Name = "CK_PVE"
        Me.CK_PVE.Size = New System.Drawing.Size(48, 24)
        Me.CK_PVE.TabIndex = 193
        Me.CK_PVE.Text = "PVE"
        '
        'CierreCaja
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(778, 648)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblanulado)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.CK_PVE)
        Me.Name = "CierreCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cierre Caja"
        Me.Controls.SetChildIndex(Me.CK_PVE, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.TextBox6, 0)
        Me.Controls.SetChildIndex(Me.txtNombreUsuario, 0)
        Me.Controls.SetChildIndex(Me.Label36, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox4, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.GroupBox5, 0)
        Me.Controls.SetChildIndex(Me.GroupBox6, 0)
        Me.Controls.SetChildIndex(Me.lblanulado, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.DataSetCierreCaja1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgResumen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.txtTotalCompras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDevoluciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepositar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSalidas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEntradas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalCajero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalSistema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFondoCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDiferenciaCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.txtDepDolSistema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepDolCajero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtDepoColSistena.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepoColCajero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChequesDolSistema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChequesDolCajero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChequesSistema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChequesCajero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Load"
    Private Sub CierreCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = GetSetting("SeeSoft", "SeePos", "Conexion")
        AdapterMoneda.Fill(Me.DataSetCierreCaja1.Moneda)
        AdapterUsuarios.Fill(Me.DataSetCierreCaja1.Usuarios)
        AdapterTipoTarjeta.Fill(Me.DataSetCierreCaja1.TipoTarjeta)
        AdapterDetallePago.Fill(Me.DataSetCierreCaja1.Detalle_pago_caja)
        FormatoTablas()
        '-----------------------------------------------------------------------
        'VERIFICA FORMATO DE IMPRESION -
        Dim PVE As Boolean
        Try
            PVE = CBool(GetSetting("SeeSOFT", "SeePos", "FormatoPVECC"))
        Catch ex As Exception
            SaveSetting("SeeSOFT", "SeePos", "FormatoPVECC", "True")
            PVE = True
        Finally
            CK_PVE.Checked = PVE
        End Try
        '-----------------------------------------------------------------------

        defaultvalue()

    End Sub

    Private Sub defaultvalue()
        Dim i, j As Integer
        DataSetCierreCaja1.cierrecaja.AperturaColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.CajeraColumn.DefaultValue = ""
        DataSetCierreCaja1.cierrecaja.NombreColumn.DefaultValue = ""
        DataSetCierreCaja1.cierrecaja.AnuladoColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.FechaColumn.DefaultValue = Now
        DataSetCierreCaja1.cierrecaja.SubtotalColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.DevolucionesColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.TotalSistemaColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.EquivalenciaColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.EfectivoColonesColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.EfectivoDolaresColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.TarjetaColonesColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.TarjetaDolaresColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.ChequesColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.ChequeDolColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.DepositosColColumn.DefaultValue = 0
        DataSetCierreCaja1.cierrecaja.DepositosDolColumn.DefaultValue = 0

        DataSetCierreCaja1.CierreCaja_DetMon.Id_DetaMonedaColumn.AutoIncrement = True
        DataSetCierreCaja1.CierreCaja_DetMon.Id_DetaMonedaColumn.AutoIncrementSeed = -1
        DataSetCierreCaja1.CierreCaja_DetMon.Id_DetaMonedaColumn.AutoIncrementStep = -1
        DataSetCierreCaja1.CierreCaja_DetMon.Id_MonedaColumn.DefaultValue = 1
        DataSetCierreCaja1.CierreCaja_DetMon.MontoCajeroColumn.DefaultValue = 0
        DataSetCierreCaja1.CierreCaja_DetMon.MontoSistemaColumn.DefaultValue = 0

        DataSetCierreCaja1.CierreCaja_DetTarj.Id_DetalleTarjetaColumn.AutoIncrement = True
        DataSetCierreCaja1.CierreCaja_DetTarj.Id_DetalleTarjetaColumn.AutoIncrementSeed = -1
        DataSetCierreCaja1.CierreCaja_DetTarj.Id_DetalleTarjetaColumn.AutoIncrementStep = -1
        DataSetCierreCaja1.CierreCaja_DetTarj.Id_TipoTarjetaColumn.DefaultValue = 0
        DataSetCierreCaja1.CierreCaja_DetTarj.MontoCajeroColumn.DefaultValue = 0
        DataSetCierreCaja1.CierreCaja_DetTarj.MontoSistemaColumn.DefaultValue = 0

        For i = 0 To DataSetCierreCaja1.TipoTarjeta.Rows.Count - 1
            DataSetCierreCaja1.TipoTarjeta.Rows(i).Item("Total") = 0
            DataSetCierreCaja1.TipoTarjeta.Rows(i).Item("TotalS") = 0
            For j = 0 To DataSetCierreCaja1.Moneda.Rows.Count - 1
                If DataSetCierreCaja1.TipoTarjeta.Rows(i).Item("Moneda") = DataSetCierreCaja1.Moneda.Rows(j).Item("CodMoneda") Then
                    DataSetCierreCaja1.TipoTarjeta.Rows(i).Item("Monedas") = DataSetCierreCaja1.Moneda.Rows(j).Item("MonedaNombre")
                End If
            Next
        Next
    End Sub

    Public Function FormatoTablas()
        'Declara de columnas
        Dim dtfact, dttipo, dtcoin, dtpago, dtformapago, dtmonto As System.Data.DataColumn
        Dim dtmoneda, dtefectivo, dttarjeta, dtcheque, dttransferencia, dtdevolucion, dttotal As System.Data.DataColumn
        Dim dtMonto_cierre, dtmoneda_cierre, dtequivalencia_cierre As System.Data.DataColumn
        'Creacion de las columnas  
        '*******************************************Tabla
        dtmoneda = New System.Data.DataColumn("Moneda")
        dtefectivo = New System.Data.DataColumn("Efectivo")
        dttarjeta = New System.Data.DataColumn("Tarjeta")
        dtcheque = New System.Data.DataColumn("Cheque")
        dttransferencia = New System.Data.DataColumn("Transferencia")
        dtdevolucion = New System.Data.DataColumn("Devoluciones")
        dttotal = New System.Data.DataColumn("Total")
        '********************************************************Tabla Resumen
        dtfact = New System.Data.DataColumn("Factura")
        dttipo = New System.Data.DataColumn("Tipo")
        dtcoin = New System.Data.DataColumn("Moneda")
        dtpago = New System.Data.DataColumn("Pago")
        dtformapago = New System.Data.DataColumn("Forma Pago")
        dtmonto = New System.Data.DataColumn("Equivalencia")
        '**********************************************************Tabla Cierre
        dtMonto_cierre = New System.Data.DataColumn("Monto")
        dtmoneda_cierre = New System.Data.DataColumn("Moneda")
        dtequivalencia_cierre = New System.Data.DataColumn("Equivalencia")
        'Me.chkAnulado.Checked = False

        'Agregar columnas a la data tabla
        '*********************************************************Tabla
        tabla.Columns.Add(dtmoneda)
        tabla.Columns.Add(dtefectivo)
        tabla.Columns.Add(dttarjeta)
        tabla.Columns.Add(dtcheque)
        tabla.Columns.Add(dttransferencia)
        tabla.Columns.Add(dtdevolucion)
        tabla.Columns.Add(dttotal)
        '*********************************************************Tabla Resumen
        tablaresumen.Columns.Add(dtfact)
        tablaresumen.Columns.Add(dttipo)
        tablaresumen.Columns.Add(dtcoin)
        tablaresumen.Columns.Add(dtpago)
        tablaresumen.Columns.Add(dtformapago)
        tablaresumen.Columns.Add(dtmonto)
        '*********************************************************Tabla Cierre
        tablacierre.Columns.Add(dtMonto_cierre)
        tablacierre.Columns.Add(dtmoneda_cierre)
        tablacierre.Columns.Add(dtequivalencia_cierre)
    End Function
#End Region

#Region "ToolBar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1 : Nuevos()
            Case 2 : If PMU.Find Then Consultar_Cierre() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Update Then Guardar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Delete Then Anular() Else MsgBox("No tiene permiso para eliminar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 5 : If PMU.Print Then Imprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : Cerrar()
        End Select
    End Sub
#End Region

#Region "Nuevo"
    Private Sub Nuevos()
        Try
            If ToolBar1.Buttons(0).Text = "Nuevo" Then
                'Restricci�n botones
                lblanulado.Visible = False
                ToolBar1.Buttons(0).Text = "Cancelar"
                ToolBar1.Buttons(0).ImageIndex = 8
                ToolBar1.Buttons(1).Enabled = False
                ToolBar1.Buttons(2).Enabled = True
                ToolBar1.Buttons(3).Enabled = False
                ToolBar1.Buttons(4).Enabled = False
                txtcodcajero.Enabled = True
                txtcodcajero.Focus()
                'Limpia los campos
                ButtonBuscarCajero.Enabled = True
                dgResumen.DataSource = Nothing

                tabla.Clear()
                tablacierre.Clear()
                tablaresumen.Clear()
                DataSetCierreCaja1.aperturacaja.Clear()
                DataSetCierreCaja1.CierreCaja_DetMon.Clear()
                DataSetCierreCaja1.CierreCaja_DetTarj.Clear()
                DataSetCierreCaja1.cierrecaja.Clear()
                DataSetCierreCaja1.ArqueoCajas.Clear()
                DataSetCierreCaja1.Apertura_Total_Tope.Clear()

                BindingContext(Me.DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()
                BindingContext(Me.DataSetCierreCaja1, "cierrecaja").AddNew()

                txtDiferenciaCaja.EditValue = 0
                TextEdit5.EditValue = 0
                TextEdit18.EditValue = 0
                txtTotalSistema.EditValue = 0
                TextEdit1.EditValue = 0
                TextEdit2.EditValue = 0
                TextEdit9.EditValue = 0
                TextEdit11.EditValue = 0
                Me.txtTotalCajero.EditValue = 0
                TxtFondoCaja.EditValue = 0
                TextEdit19.EditValue = 0
                txtChequesCajero.EditValue = 0
                txtChequesSistema.EditValue = 0
                txtChequesDolCajero.EditValue = 0
                txtChequesDolSistema.EditValue = 0
                txtDepoColCajero.EditValue = 0
                TxtDepoColSistena.EditValue = 0
                txtDepDolCajero.EditValue = 0
                txtDepDolSistema.EditValue = 0
                TipoCambioDolar = 0

            Else
                ToolBar1.Buttons(0).Text = "Nuevo"
                ToolBar1.Buttons(0).ImageIndex = 0
                ToolBar1.Buttons(0).Enabled = True
                ToolBar1.Buttons(1).Enabled = True
                ToolBar1.Buttons(2).Enabled = False
                ToolBar1.Buttons(3).Enabled = False
                ToolBar1.Buttons(4).Enabled = False
                ToolBar1.Buttons(5).Enabled = True
                BindingContext(Me.DataSetCierreCaja1, "cierrecaja").CancelCurrentEdit()
                tabla.Clear()
                tablacierre.Clear()
                tablaresumen.Clear()
                DataSetCierreCaja1.aperturacaja.Clear()
                DataSetCierreCaja1.CierreCaja_DetMon.Clear()
                DataSetCierreCaja1.CierreCaja_DetTarj.Clear()
                DataSetCierreCaja1.cierrecaja.Clear()
                txtcodcajero.Enabled = False
                ButtonBuscarCajero.Enabled = False
                txtfechaapertura.Text = ""

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Anular"
    Private Sub Anular()
        Dim resp As Integer
        If SqlConnection1.State <> SqlConnection1.State.Open Then SqlConnection1.Open()
        Dim Trans As SqlTransaction = SqlConnection1.BeginTransaction
        Dim myCommand1 As SqlCommand = SqlConnection1.CreateCommand()
        Dim myCommand2 As SqlCommand = SqlConnection1.CreateCommand()

        resp = MessageBox.Show("�Deseas Anular este Cierre?", "SeeSoft", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
        If resp = 6 Then
            myCommand1.CommandText = "UPDATE CierreCaja SET Anulado =  1 WHERE NumeroCierre = " & BindingContext(Me.DataSetCierreCaja1, "CierreCaja").Current("NumeroCierre")
            myCommand2.CommandText = "UPDATE aperturacaja SET Estado = '" & "M" & "' WHERE NApertura = " & BindingContext(Me.DataSetCierreCaja1, "CierreCaja").Current("Apertura")
            myCommand1.Transaction = Trans
            myCommand2.Transaction = Trans
            myCommand1.ExecuteNonQuery()
            myCommand2.ExecuteNonQuery()
            Trans.Commit()
            MsgBox("Datos Anulados Correctamente....", MsgBoxStyle.Information, "Atenci�n...")
            lblanulado.Visible = True
        End If
    End Sub
#End Region

#Region "Guardar"
    Private Sub Guardar()
        CargarDetalleCierreCaja()
        If Registar() Then
            CambiarEstadoApertura()
            DataSetCierreCaja1.AcceptChanges()
            If MsgBox("Desea imprimir el cierre de Caja", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Imprimir()
            End If

            tabla.Clear()
            tablacierre.Clear()
            tablaresumen.Clear()
            DataSetCierreCaja1.aperturacaja.Clear()
            DataSetCierreCaja1.CierreCaja_DetMon.Clear()
            DataSetCierreCaja1.CierreCaja_DetTarj.Clear()
            DataSetCierreCaja1.ArqueoTarjeta.Clear()
            DataSetCierreCaja1.cierrecaja.Clear()
            txtcodcajero.Enabled = False
            MessageBox.Show("El cierre de caja ha sido registrado", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ToolBar1.Buttons(0).Text = "Nuevo"
            ToolBar1.Buttons(0).ImageIndex = 0
            ToolBar1.Buttons(0).Enabled = True
            ToolBar1.Buttons(1).Enabled = True
            ToolBar1.Buttons(2).Enabled = False
            ToolBar1.Buttons(3).Enabled = False
            ToolBar1.Buttons(4).Enabled = False
            ToolBar1.Buttons(5).Enabled = True
            txtfechaapertura.Text = ""
        Else
            MessageBox.Show("Problemas al tratar de registrar el  Cierre, Intente de nuevo, si el problema persiste pongase en contacto con el adminstrador del sistema", "Atenci�n...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub CargarDetalleCierreCaja()
        Dim I As Integer
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Usuario") = TextBox6.Text
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("NombreUsuario") = txtNombreUsuario.Text
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Anulado") = 0
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Devoluciones") = txtDevoluciones.EditValue
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Cheques") = txtChequesSistema.EditValue
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("ChequeDol") = txtChequesDolSistema.EditValue
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("DepositosCol") = TxtDepoColSistena.EditValue
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("DepositosDol") = txtDepDolSistema.EditValue
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Subtotal") = Me.txtTotalSistema.EditValue
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("TotalSistema") = txtTotalSistema.EditValue
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Equivalencia") = 0
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()

        '-----------------------------------------------------------------------------------------------
        'TARJETAS - ORA
        For I = 0 To DataSetCierreCaja1.TipoTarjeta.Rows.Count - 1
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").EndCurrentEdit()
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").AddNew()
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").Current("Id_TipoTarjeta") = DataSetCierreCaja1.TipoTarjeta.Rows(I).Item("Id")
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").Current("MontoSistema") = DataSetCierreCaja1.TipoTarjeta.Rows(I).Item("TotalS")
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").Current("MontoCajero") = DataSetCierreCaja1.TipoTarjeta.Rows(I).Item("Total")
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetTarj").EndCurrentEdit()
        Next
        '-----------------------------------------------------------------------------------------------

        '-----------------------------------------------------------------------------------------------
        'DENOMINACI�N - ORA
        For I = 0 To DataSetCierreCaja1.Moneda.Rows.Count - 1
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").EndCurrentEdit()
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").AddNew()
            If DataSetCierreCaja1.Moneda.Rows(I).Item("Simbolo") = "$" Then
                BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("Id_Moneda") = DataSetCierreCaja1.Moneda.Rows(I).Item("CodMoneda")
                BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("MontoSistema") = TextEdit7.EditValue
                BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("MontoCajero") = TextEdit2.EditValue
            Else
                BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("Id_Moneda") = DataSetCierreCaja1.Moneda.Rows(I).Item("CodMoneda")
                BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("MontoSistema") = TextEdit5.EditValue
                BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").Current("MontoCajero") = TextEdit1.EditValue
            End If
            BindingContext(Me.DataSetCierreCaja1, "cierrecaja.cierrecajaCierreCaja_DetMon").EndCurrentEdit()
        Next
        '-----------------------------------------------------------------------------------------------
    End Sub

    Private Function CambiarEstadoApertura() As Boolean
        If SqlConnection1.State <> SqlConnection1.State.Open Then SqlConnection1.Open()
        Dim Trans As SqlTransaction = SqlConnection1.BeginTransaction

        Try
            Dim myCommand2 As SqlCommand = SqlConnection1.CreateCommand()
            myCommand2.CommandText = "UPDATE aperturacaja SET Estado = '" & "C" & "' WHERE NApertura = " & BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Apertura")
            myCommand2.Transaction = Trans
            myCommand2.ExecuteNonQuery()
            Trans.Commit()
            Return True

        Catch eEndEdit As System.Exception
            Trans.Rollback()
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            Return False
        End Try
    End Function

    Private Function Registar() As Boolean
        If SqlConnection1.State <> SqlConnection1.State.Open Then SqlConnection1.Open()
        Dim Trans As SqlTransaction = SqlConnection1.BeginTransaction
        Try
            SqlInsertCommand1.Transaction = Trans
            SqlUpdateCommand1.Transaction = Trans
            SqlInsertCommand2.Transaction = Trans
            SqlUpdateCommand2.Transaction = Trans
            SqlInsertCommand3.Transaction = Trans
            SqlUpdateCommand3.Transaction = Trans
            AdapterCierre.Update(Me.DataSetCierreCaja1.cierrecaja)
            Apertura = Me.DataSetCierreCaja1.cierrecaja.Rows(0).Item("Apertura")
            AdapterCierreTarjeta.Update(Me.DataSetCierreCaja1.CierreCaja_DetTarj)
            AdapterCierreMonto.Update(Me.DataSetCierreCaja1.CierreCaja_DetMon)
            Trans.Commit()
            Return True

        Catch eEndEdit As System.Exception
            Trans.Rollback()
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
            Return False
        End Try
    End Function
#End Region

#Region "Imprimir"
    Private Sub Imprimir()
        If CK_PVE.Checked Then
            Try
                Dim rptCierreCaja As New ReporteCierreControlPVE
                Dim visor As New frmVisorReportes
                visor.MdiParent = Me.ParentForm
                CrystalReportsConexion.LoadReportViewer(visor.rptViewer, rptCierreCaja)
                rptCierreCaja.SetParameterValue(0, Me.BindingContext(Me.DataSetCierreCaja1, "CierreCaja").Current("NumeroCierre"))
                rptCierreCaja.SetParameterValue(1, TextEdit14.EditValue)
                rptCierreCaja.SetParameterValue(2, TextEdit20.EditValue)
                rptCierreCaja.SetParameterValue(3, TextEdit4.EditValue)
                rptCierreCaja.SetParameterValue(4, txtEntradas.EditValue)
                rptCierreCaja.SetParameterValue(5, txtSalidas.EditValue)
                visor.Show()

            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        Else
            Try
                Dim rptCierreCaja As New ReporteCierreControl
                Dim visor As New frmVisorReportes
                visor.MdiParent = Me.ParentForm
                CrystalReportsConexion.LoadReportViewer(visor.rptViewer, rptCierreCaja)
                rptCierreCaja.SetParameterValue(0, Me.BindingContext(Me.DataSetCierreCaja1, "CierreCaja").Current("NumeroCierre"))
                rptCierreCaja.SetParameterValue(1, TextEdit14.EditValue)
                rptCierreCaja.SetParameterValue(2, TextEdit20.EditValue)
                rptCierreCaja.SetParameterValue(3, TextEdit4.EditValue)
                visor.Show()

            Catch ex As SystemException
                MsgBox(ex.Message)
            End Try
        End If

    End Sub

    ''Sub cargarDetalleOperaciones()
    ''    Dim i As Integer = 0
    ''    For i = 0 To tablaresumen.Rows.Count - 1
    ''        DataSetCierreCaja1.Imprimir.ImportRow(Me.tablaresumen.Rows(i))
    ''        DataSetCierreCaja1.Imprimir(i).FormaPago = tablaresumen.Rows(i).Item(4)
    ''    Next

    ''End Sub
#End Region

#Region "Buscar"
    Private Sub Consultar_Cierre()
        Dim cFunciones As New cFunciones
        Apertura = "0"
        Dim CierreNumero As String = cFunciones.Buscar_X_Descripcion_Fecha("SELECT CAST(NumeroCierre AS varchar) AS Cierre, Nombre, Fecha FROM cierrecaja Order by NumeroCierre Desc", "Nombre", "Fecha", "Buscando Cierre de Caja...")

        If CierreNumero = Nothing Then
            Exit Sub
        End If

        dgResumen.DataSource = Nothing
        tabla.Clear()
        tablacierre.Clear()
        tablaresumen.Clear()
        DataSetCierreCaja1.aperturacaja.Clear()
        DataSetCierreCaja1.cierrecaja.Clear()

        If CierreNumero <> Nothing Then
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM  cierrecaja where NumeroCierre = '" & CierreNumero & "'", DataSetCierreCaja1.cierrecaja)
            Apertura = DataSetCierreCaja1.cierrecaja.Rows(0).Item("Apertura")
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM aperturacaja where  NApertura = " & Apertura, DataSetCierreCaja1.aperturacaja)
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM OpcionesDePago WHERE Numapertura = " & Apertura & " AND FormaPago <> 'ANU'", DataSetCierreCaja1.OpcionesDePago)
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM Apertura_Total_Tope where NApertura  = " & Apertura, DataSetCierreCaja1.Apertura_Total_Tope)
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM ArqueoCajas where IdApertura  = " & Apertura & " AND Anulado = 0", DataSetCierreCaja1.ArqueoCajas)
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM VentasContado where Num_Apertura  = " & Apertura, DataSetCierreCaja1.VentasContado)
            If DataSetCierreCaja1.ArqueoCajas.Rows.Count <> 0 Then CargarArqueoTarjeta(Me.DataSetCierreCaja1.ArqueoCajas.Rows(0).Item("Id"))
            AperturaNumero = Apertura
            Cargando()
            CargarCierre()
            txtcodaperturacajero.Text = DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString
            txtfechaapertura.Text = CDate(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
            ToolBar1.Buttons(3).Enabled = True
            ToolBar1.Buttons(4).Enabled = True
            If DataSetCierreCaja1.cierrecaja.Rows(0).Item("Anulado") = True Then
                lblanulado.Visible = True
            Else
                lblanulado.Visible = False
            End If
        Else
            MsgBox("No se obtuvieron resultados", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub CargarArqueoTarjeta(ByVal Numapertura As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSoft", "SeePos", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM ArqueoTarjeta where Id_Arqueo  = @Numapertura "
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmd.Parameters("@Numapertura").Value = Numapertura
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(Me.DataSetCierreCaja1.ArqueoTarjeta)
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Sub
#End Region

#Region "Buscar Arqueo"
    Private Sub consultarcajero()
        Dim cFunciones As New cFunciones
        AperturaNumero = cFunciones.Buscar_X_Descripcion_Fecha("SELECT CAST(NApertura AS Varchar) as Apertura,Nombre,Fecha FROM aperturacaja where estado =  '" & "M" & "'", "Nombre", "Fecha", "Aperturas de caja sin cerrar...")

        If AperturaNumero = Nothing Then
            MsgBox("Debes selecionar un usuario para realizar el cierre de Caja", MsgBoxStyle.Exclamation)
        Else
            DataSetCierreCaja1.aperturacaja.Clear()
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM aperturacaja where Napertura = '" & AperturaNumero & "' AND Estado = 'M'", DataSetCierreCaja1.aperturacaja)
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM Apertura_Total_Tope where NApertura  = " & AperturaNumero, DataSetCierreCaja1.Apertura_Total_Tope)
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM ArqueoCajas where IdApertura  = " & AperturaNumero & " AND Anulado = 0", DataSetCierreCaja1.ArqueoCajas)

            If DataSetCierreCaja1.ArqueoCajas.Count > 0 Then
            Else
                MsgBox("Es Necesario Ingresar Un Arqueo De Caja")
                Exit Sub
            End If

            Dim Fecha As DateTime = BindingContext(Me.DataSetCierreCaja1, "aperturacaja").Current("Fecha")



            cFunciones.Llenar_Tabla_Generico("SELECT * FROM VentasContado where Num_Apertura  = " & AperturaNumero, DataSetCierreCaja1.VentasContado)
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM VentasCredito where Num_Apertura = " & AperturaNumero, DataSetCierreCaja1.VentasCredito)
            cFunciones.Llenar_Tabla_Generico("SELECT * FROM ArqueoTarjeta where Id_Arqueo  = " & BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("Id"), DataSetCierreCaja1.ArqueoTarjeta)
            Cargando()



            If DataSetCierreCaja1.aperturacaja.Count > 0 Then
                tabla.Clear()
                tablacierre.Clear()
                tablaresumen.Clear()
                DataSetCierreCaja1.OpcionesDePago.Clear()
                CargarOpcionesPago(AperturaNumero)
                txtcodaperturacajero.Text = AperturaNumero
                txtfechaapertura.Text = CDate(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
                CargarCierre()
            Else
                MsgBox("Este Usuario no tiene una caja abierta")
            End If
        End If
    End Sub

    Private Sub CargarOpcionesPago(ByVal Numapertura As String)
        Dim cnnv As SqlConnection = Nothing
        Try
            Dim sConn As String = GetSetting("SeeSoft", "SeePos", "CONEXION")
            cnnv = New SqlConnection(sConn)
            cnnv.Open()
            ' Creamos el comando para la consulta
            Dim cmdv As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM OpcionesDePago WHERE Numapertura = @Numapertura AND FormaPago <> 'ANU'"
            cmdv.CommandText = sel
            cmdv.Connection = cnnv
            cmdv.CommandType = CommandType.Text
            cmdv.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmdv.Parameters.Add(New SqlParameter("@Numapertura", SqlDbType.Int))
            cmdv.Parameters("@Numapertura").Value = Numapertura
            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim dv As New SqlDataAdapter
            dv.SelectCommand = cmdv
            ' Llenamos la tabla
            dv.Fill(Me.DataSetCierreCaja1, "OpcionesDePago")
        Catch ex As System.Exception
            ' Si hay error, devolvemos un valor nulo.
            MsgBox(ex.ToString)
        Finally
            If Not cnnv Is Nothing Then
                cnnv.Close()
            End If
        End Try
    End Sub
#End Region

#Region "Cargar Cierre"
    Private Sub CargarCierre()
        Dim I, J, K As Integer
        Dim Dr As System.Data.DataRow
        Dim Dr1 As System.Data.DataRow
        Dim Dr2 As System.Data.DataRow

        'Valores Vacios a los grid
        dgResumen.DataSource = Nothing
        txtcodcajero.Text = DataSetCierreCaja1.aperturacaja.Rows(0).Item("Cedula")
        txtnomcajero.Text = DataSetCierreCaja1.aperturacaja.Rows(0).Item("Nombre")
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Cajera") = DataSetCierreCaja1.aperturacaja.Rows(0).Item("Cedula")
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Nombre") = DataSetCierreCaja1.aperturacaja.Rows(0).Item("Nombre")
        txtcodaperturacajero.Text = DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString
        txtfechaapertura.Text = CDate(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Fecha")).ToString
        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").Current("Apertura") = DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura").ToString
        SubTotal = 0
        Devolucion = 0
        TextEdit5.EditValue = 0 : TextEdit8.EditValue = 0
        TextEdit7.EditValue = 0 : TextEdit10.EditValue = 0
        TextEdit4.EditValue = 0 : txtDevoluciones.EditValue = 0
        txtEntradas.EditValue = 0 : txtSalidas.EditValue = 0
        TextEdit14.EditValue = 0
        txtChequesDolSistema.EditValue = 0 : txtChequesSistema.EditValue = 0
        TxtDepoColSistena.EditValue = 0 : txtDepDolSistema.EditValue = 0

        'LLenar Los Totales Por monedas
        For I = 0 To (Me.DataSetCierreCaja1.Moneda.Count - 1)
            Dr2 = tablacierre.NewRow
            Dr = tabla.NewRow
            Dr(0) = DataSetCierreCaja1.Moneda.Rows(I).Item("MonedaNombre")
            TotalesPorMonedas(Dr, Dr2, DataSetCierreCaja1.Moneda.Rows(I).Item("CodMoneda"), DataSetCierreCaja1.Moneda.Rows(I).Item("ValorCompra"), DataSetCierreCaja1.Moneda.Rows(I).Item("MonedaNombre"))
            Select Case DataSetCierreCaja1.Moneda.Rows(I).Item("Simbolo")
                Case "�"
                    TextEdit5.EditValue = Dr(1)
                    TextEdit8.EditValue = Dr(2)
                Case "$"
                    TextEdit7.EditValue = Dr(1)
                    TextEdit10.EditValue = Dr(2)
            End Select
            tabla.Rows.Add(Dr)
            tablacierre.Rows.Add(Dr2)
        Next
        TextEdit7.EditValue = CDbl(TextEdit7.EditValue) + FondoCajaDolar
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        For I = 0 To (Me.DataSetCierreCaja1.VentasContado.Count - 1)
            If DataSetCierreCaja1.VentasContado.Rows(I).Item("Cod_Moneda") = 2 Then
                TextEdit14.EditValue += (Me.DataSetCierreCaja1.VentasContado.Rows(I).Item("Total") * TipoCambioDolar)
            Else
                TextEdit14.EditValue += DataSetCierreCaja1.VentasContado.Rows(I).Item("Total")
            End If
        Next

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Llenar las Operaciones
        For I = 0 To (Me.DataSetCierreCaja1.OpcionesDePago.Count - 1)
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "TAR" Then
                For K = 0 To (Me.DataSetCierreCaja1.Detalle_pago_caja.Count - 1)
                    If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Id") = DataSetCierreCaja1.Detalle_pago_caja.Rows(K).Item("Id_ODP") Then
                        'For J = 0 To (Me.DataSetCierreCaja1.TipoTarjeta.Count - 1)
                        'If DataSetCierreCaja1.TipoTarjeta.Rows(J).Item("Id") = DataSetCierreCaja1.Detalle_pago_caja.Rows(K).Item("ReferenciaTipo") Then
                        DataSetCierreCaja1.TipoTarjeta.Rows(0).Item("TotalS") += DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                        'End If
                        'Next
                    End If
                Next
            End If

            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE CHEQUES - ORA
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "CHE" Then
                If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("CodMoneda") = 1 Then
                    txtChequesSistema.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                Else
                    txtChequesDolSistema.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                End If
            End If
            '------------------------------------------------------------------------------------------

            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE TRANSFERENCIAS - ORA
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "TRA" Then
                If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("CodMoneda") = 1 Then
                    TxtDepoColSistena.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                Else
                    txtDepDolSistema.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                End If
            End If
            '------------------------------------------------------------------------------------------

            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE ABONOS - ORA
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "ABO" Or DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "ABA" Then
                If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("CodMoneda") = 1 Then
                    TextEdit4.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
                Else
                    TextEdit4.EditValue += (Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago") * TipoCambioDolar)
                End If
            End If
            '------------------------------------------------------------------------------------------

            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE ENTRADAS DE LA CAJA - ORA
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "MCE" Then
                txtEntradas.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
            End If
            '------------------------------------------------------------------------------------------

            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE SALIDAS DE LA CAJA - ORA
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "MCS" Then
                txtSalidas.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
            End If
            '------------------------------------------------------------------------------------------

            '------------------------------------------------------------------------------------------
            'MONTO TOTAL DE DEVOLUCIONES - ORA
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "DVE" Then
                txtDevoluciones.EditValue += Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
            End If
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento") = "DVA" Then
                txtDevoluciones.EditValue -= Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago")
            End If
            '------------------------------------------------------------------------------------------

            Dr1 = tablaresumen.NewRow
            Dr1(0) = DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Documento")
            Dr1(1) = DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoDocumento")
            Dr1(2) = DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("Nombremoneda")
            Dr1(3) = Format(Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago"), "#,#0.00")
            'Si es una devoluci�n
            If DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago") = "DVE" Then
                Dr1(4) = "DVE"
            Else 'Si no es una devoluci�n
                Select Case DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("FormaPago")
                    Case "EFE"
                        Dr1(4) = "EFE"
                    Case "MCS"
                        Dr1(4) = "MCS"
                    Case "MCE"
                        Dr1(4) = "MCE"
                    Case "TAR"
                        Dr1(4) = "TAR"
                    Case "CHE"
                        Dr1(4) = "CHE"
                    Case "TRA"
                        Dr1(4) = "TRA"
                End Select
            End If
            Dr1(5) = Format((Me.DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("MontoPago") * DataSetCierreCaja1.OpcionesDePago.Rows(I).Item("TipoCambio")), "#,#0.00")
            tablaresumen.Rows.Add(Dr1)
        Next

        DescontarVuelto()
        dgResumen.DataSource = tablaresumen

        TextEdit5.EditValue = TextEdit5.EditValue + FondoCajaColon - txtDevoluciones.EditValue
        TextEdit18.EditValue = (TextEdit5.EditValue + (TextEdit7.EditValue * TipoCambioDolar) + TextEdit8.EditValue + (TextEdit10.EditValue * TipoCambioDolar) + txtChequesSistema.EditValue + (txtChequesDolSistema.EditValue * TipoCambioDolar) + TxtDepoColSistena.EditValue + (txtDepDolSistema.EditValue * TipoCambioDolar))

        txtTotalSistema.EditValue = TextEdit18.Text - txtTotalCompras.EditValue
        txtDiferenciaCaja.EditValue = txtTotalCajero.EditValue - (txtTotalSistema.EditValue)

        TextEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        TextEdit5.Properties.DisplayFormat.FormatString = "#,#0.00"
        txtDepositar.EditValue = TextEdit5.EditValue + (TextEdit7.EditValue * TipoCambioDolar) + txtChequesSistema.EditValue + (txtChequesDolSistema.EditValue * TipoCambioDolar) - TxtFondoCaja.EditValue

        BindingContext(DataSetCierreCaja1, "cierrecaja").Current("Subtotal") = (SubTotal + txtDevoluciones.EditValue)
        BindingContext(DataSetCierreCaja1, "cierrecaja").Current("Devoluciones") = txtDevoluciones.EditValue

        BindingContext(DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()
    End Sub

    Private Function TotalesPorMonedas(ByRef Dr As DataRow, ByRef Dr1 As DataRow, ByVal CodigoMoneda As String, ByVal ValorCompra As Double, ByVal NombreMoneda As String) As DataRow
        Try
            Dim OperacionesMoneda() As DataRow
            Dim DenominacionesApertura() As DataRow
            Dim Operacion As DataRow
            Dim Denominacion As DataRow
            Dim J As Integer = 0
            Dim Vueltos As Double
            Dim Efectivo, Targeta, Cheque, Transferencia, devoluciones As Double
            Dim Denominaciones As Double
            OperacionesMoneda = DataSetCierreCaja1.OpcionesDePago.Select("CodMoneda = '" & CodigoMoneda & "'")

            'Cargar opciones de pago
            While J < OperacionesMoneda.Length
                Operacion = OperacionesMoneda(J)
                If Operacion("FormaPago") = "DVE" Then
                    devoluciones += Operacion("MontoPago")
                    Devolucion += (Operacion("MontoPago") * ValorCompra)
                Else
                    Select Case Operacion("FormaPago")
                        Case "EFE", "MCE"
                            Efectivo += Operacion("MontoPago")
                        Case "MCS"
                            Efectivo -= Operacion("MontoPago")
                        Case "TAR"
                            Targeta += Operacion("MontoPago")
                        Case "CHE"
                            Cheque += Operacion("MontoPago")
                        Case "TRA"
                            Transferencia += Operacion("MontoPago")
                    End Select
                End If
                J += 1
            End While
            J = 0
            Dr(1) = (Efectivo)
            Dr(2) = Targeta
            Dr(3) = Cheque
            Dr(4) = Transferencia
            Dr(5) = devoluciones
            Dr(6) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones) * ValorCompra
            Dr1(0) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones)
            Dr1(1) = NombreMoneda
            Dr1(2) = ((Efectivo) + Targeta + Cheque + Transferencia - devoluciones) * ValorCompra

        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try

    End Function

    Private Sub DescontarVuelto()
        Dim i As Integer
        'Efectivo Colones se le resta Los Vueltos
        ' tabla.Rows(0).Item(1) = (tabla.Rows(0).Item(1) - Vuelto)
        'Total Colones se le resta Los vueltos
        'tabla.Rows(0).Item(6) = (tabla.Rows(0).Item(6) - Vuelto)
        'tablacierre.Rows(0).Item(0) = tabla.Rows(0).Item(6)
        'tablacierre.Rows(0).Item(2) = tabla.Rows(0).Item(6)
        For i = 0 To (tabla.Rows.Count - 1)
            SubTotal += tabla.Rows(i).Item(6)
        Next
    End Sub

    Private Sub Cargando()
        Dim I As Integer

        Try
            txtChequesCajero.EditValue = 0 : txtChequesDolCajero.EditValue = 0 : txtDepoColCajero.EditValue = 0 : txtDepDolCajero.EditValue = 0
            CagarFacturasCredito(Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("Fecha"), DataSetCierreCaja1.aperturacaja.Rows(0).Item("Cedula"), Me.DataSetCierreCaja1.aperturacaja.Rows(0).Item("NApertura"))
            TextEdit1.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("EfectivoColones")
            TextEdit2.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("EfectivoDolares")
            TextEdit9.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TarjetaColones")
            TextEdit11.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TarjetaDolares")
            txtChequesCajero.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("Cheques")
            txtChequesDolCajero.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("ChequesDol")
            txtDepoColCajero.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("DepositoCol")
            txtDepDolCajero.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("DepositoDol")
            Me.txtTotalCajero.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("Total")
            Me.txtTotalCompras.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TotalCompras")
            'TxtFondoCaja.EditValue = BindingContext(Me.DataSetCierreCaja1, "Apertura_Total_Tope").Current("Monto_Tope")
            TxtFondoCaja.EditValue = _obtenerMontoTope()
            TextEdit19.EditValue = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("Total")
            TipoCambioDolar = BindingContext(Me.DataSetCierreCaja1, "ArqueoCajas").Current("TipoCambioD")

            For I = 0 To DataSetCierreCaja1.TipoTarjeta.Count - 1
                Try
                    DataSetCierreCaja1.TipoTarjeta.Rows(I).Item("Total") = DataSetCierreCaja1.ArqueoTarjeta.Rows(I).Item("Monto")
                Catch ex As Exception

                End Try
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Function _obtenerMontoTope() As Double
        Dim index As Integer
        Dim sumafinal As Double = 0
        Dim tipocambio As Double = 1
        FondoCajaDolar = 0
        FondoCajaColon = 0
        For index = 0 To DataSetCierreCaja1.Apertura_Total_Tope.Count - 1
            Try
                Dim consulta As String = "SELECT ValorCompra FROM Moneda  WHERE (CodMoneda = " & DataSetCierreCaja1.Apertura_Total_Tope.Rows(index).Item("CodMoneda") & ")"
                Dim conex As New Conexion
                tipocambio = conex.SlqExecuteScalar(conex.Conectar, consulta)
                sumafinal += DataSetCierreCaja1.Apertura_Total_Tope.Rows(index).Item("Monto_Tope") * tipocambio
                If DataSetCierreCaja1.Apertura_Total_Tope.Rows(index).Item("CodMoneda") = 1 Then
                    FondoCajaColon += DataSetCierreCaja1.Apertura_Total_Tope.Rows(index).Item("Monto_Tope")
                End If
                If DataSetCierreCaja1.Apertura_Total_Tope.Rows(index).Item("CodMoneda") = 2 Then
                    FondoCajaDolar += DataSetCierreCaja1.Apertura_Total_Tope.Rows(index).Item("Monto_Tope")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next
        Return sumafinal
    End Function
    Private Function CagarFacturasCredito(ByVal Fecha As Date, ByVal Cedula As String, ByVal Num_Apertura As Integer)
        Dim cnn As SqlConnection = Nothing
        Dim i As Integer
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("SeeSoft", "SeePos", "CONEXION")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            ' Creamos el comando para la consulta
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM dbo.VentasCredito WHERE Num_Apertura  = @Num_Apertura"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            cmd.Parameters.Add(New SqlParameter("@Num_Apertura", Num_Apertura))
            cmd.Parameters("@Num_Apertura").Value = CInt(Num_Apertura)
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            DataSetCierreCaja1.VentasCredito.Clear()
            da.Fill(Me.DataSetCierreCaja1, "VENTASCREDITO")
            Dim numero As Integer
            TextEdit20.EditValue = 0
            For i = 0 To DataSetCierreCaja1.VentasCredito.Count - 1
                If DataSetCierreCaja1.VentasCredito.Rows(i).Item("Cod_Moneda") = 2 Then
                    TextEdit20.EditValue += (Me.DataSetCierreCaja1.VentasCredito.Rows(i).Item("Total") * TipoCambioDolar)
                Else
                    TextEdit20.EditValue += DataSetCierreCaja1.VentasCredito.Rows(i).Item("Total")
                End If
            Next
        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Function
#End Region

#Region "Controles"
    Private Sub Limpiar()
        TextEdit1.EditValue = 0 : TextEdit2.EditValue = 0 : TextEdit9.EditValue = 0
        TextEdit11.EditValue = 0 : TextEdit19.EditValue = 0 : txtDiferenciaCaja.EditValue = 0
        TxtFondoCaja.EditValue = 0 : TextEdit4.EditValue = 0 : txtTotalSistema.EditValue = 0
        txtTotalCajero.EditValue = 0 : TextEdit20.EditValue = 0 : TextEdit18.EditValue = 0
        TextEdit14.EditValue = 0 : txtEntradas.EditValue = 0 : txtSalidas.EditValue = 0
        txtChequesCajero.EditValue = 0 : txtChequesSistema.EditValue = 0
        txtChequesDolCajero.EditValue = 0 : txtChequesDolSistema.EditValue = 0
        txtDepoColCajero.EditValue = 0 : TxtDepoColSistena.EditValue = 0
        txtDepDolCajero.EditValue = 0 : txtDepDolSistema.EditValue = 0

    End Sub

    Private Sub IniciarEdicion()
        ToolBar1.Buttons(0).Text = "Cancelar"
        ToolBar1.Buttons(0).ImageIndex = 8
        ToolBar1.Buttons(0).Enabled = True
        ToolBar1.Buttons(1).Enabled = False
        ToolBar1.Buttons(2).Enabled = True
        ToolBar1.Buttons(3).Enabled = False
        ToolBar1.Buttons(4).Enabled = False
    End Sub
#End Region

#Region "Controles Funciones"
    Private Sub txtcodcajero_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtcodcajero.KeyDown
        If e.KeyCode = Keys.F1 Then
            Limpiar()
            consultarcajero()
        End If
    End Sub

    Private Sub ButtonBuscarCajero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscarCajero.Click
        Limpiar()
        consultarcajero()
    End Sub
#End Region

#Region "Validar Usuario"
    Private Sub TextBox6_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox6.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                Dim clave As String
                Dim usuarios() As System.Data.DataRow 'almacena temporalmente los datos de un art�culo
                Dim usuario As System.Data.DataRow 'almacena temporalmente los datos de un art�culo

                clave = TextBox6.Text
                usuarios = DataSetCierreCaja1.Usuarios.Select("clave_interna= '" & CStr(clave) & "'")

                If usuarios.Length <> 0 Then
                    usuario = usuarios(0) 'se almacena la info del usuario
                    If usuario!nombre <> "" Then
                        DataSetCierreCaja1.cierrecaja.UsuarioColumn.DefaultValue = usuario!cedula
                        DataSetCierreCaja1.cierrecaja.NombreUsuarioColumn.DefaultValue = usuario!nombre
                        txtNombreUsuario.Text = usuario!nombre
                        Cedula_Usuario = usuario!Cedula
                        PMU = VSM(Me.Cedula_Usuario, Name) 'Carga los privilegios del usuario con el modulo 
                        If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Text, MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                        txtcodcajero.Enabled = True
                        ToolBar1.Buttons(1).Enabled = True
                        IniciarEdicion()
                        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").EndCurrentEdit()
                        BindingContext(Me.DataSetCierreCaja1, "cierrecaja").AddNew()
                        txtcodcajero.Focus()
                    Else
                        MsgBox("El Usuario no Existe", MsgBoxStyle.Information)
                        TextBox6.Text = ""
                        TextBox6.Focus()
                    End If
                Else
                    MsgBox("El Usuario no Existe", MsgBoxStyle.Information)
                    TextBox6.Text = ""
                    TextBox6.Focus()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        End If
    End Sub
#End Region

End Class
