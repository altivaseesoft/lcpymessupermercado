Imports System.data.SqlClient
Imports System.Data
Imports System.Windows.Forms
'-------------------------------------------------------------------------------------
'EL REGISTRO DISPARA EL TRIGGER QUE GENERA UN CHEQUE.                                !
'-------------------------------------------------------------------------------------

Public Class Abonos_Proveedor
    Inherits System.Windows.Forms.Form
    Private sqlConexion As SqlConnection
    Dim CConexion As New Conexion
    Dim Anular As Boolean = False
    Dim VariarInteres As Boolean = False
    Dim Tabla As DataTable
    Dim buscando As Boolean
    Dim usua As Usuario_Logeado
    Dim idabono1 As Integer
    Dim NuevaConexion As String
    Dim strModulo As String = ""

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Usuario_Parametro As Object, Optional ByVal Conexion As String = "")
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        '   NuevaConexion = Conexion
        usua = Usuario_Parametro
        NuevaConexion = Conexion

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarAnular As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents adAbonos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daDetalle_Abono As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents ComboMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents txtSaldoAct As System.Windows.Forms.TextBox
    Friend WithEvents txtFactura As System.Windows.Forms.TextBox
    Friend WithEvents txtCedulaUsuario As System.Windows.Forms.TextBox
    Friend WithEvents gridFacturas As DevExpress.XtraGrid.GridControl
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents Factura As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents Fecha As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtSaldo As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtAbonoSuMoneda As System.Windows.Forms.TextBox
    Friend WithEvents txtSaldoActGen As System.Windows.Forms.TextBox
    Friend WithEvents txtAbonoGen As System.Windows.Forms.TextBox
    Friend WithEvents txtSaldoAntGen As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents txtNum_Recibo As System.Windows.Forms.TextBox
    Friend WithEvents Check_Dig_Recibo As System.Windows.Forms.CheckBox
    Friend WithEvents DataSetAbonosProveedor As DataSetAbonosProveedor
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Adapter_Cuenta_Proveedor As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Adapter_Proveedor As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GroupBox_Datos_Cliente As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents AdapterBancos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents AdapterCuentasBancarias As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GroupBox_Datos_Abonos As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBoxBanco As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxCuentaBancaria As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCuentaBancariaDestino As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxTipoPago As System.Windows.Forms.ComboBox
    Friend WithEvents LabelFecha As System.Windows.Forms.Label
    Friend WithEvents txtTipoCambio As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents TextBoxTipoCambioFactura As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxProveedorDetalle As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxIdFactura As System.Windows.Forms.TextBox
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents DataGrid2 As System.Windows.Forms.DataGrid
    Friend WithEvents TxtAbono As DevExpress.XtraEditors.CalcEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMonto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSaldo_Ant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextBoxMontoLetras As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label_CodBanco As System.Windows.Forms.Label
    Friend WithEvents SqlConnection2 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents btnDevolucion As System.Windows.Forms.Button
    Friend WithEvents txtDevoluciones As System.Windows.Forms.TextBox
    Friend WithEvents cmdAsientos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents conContabilidad As System.Data.SqlClient.SqlConnection
    Friend WithEvents cmdDetalleAsientos As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Abonos_Proveedor))
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox_Datos_Cliente = New System.Windows.Forms.GroupBox
        Me.dtFecha = New DevExpress.XtraEditors.DateEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.LabelFecha = New System.Windows.Forms.Label
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNombre = New System.Windows.Forms.TextBox
        Me.ComboMoneda = New System.Windows.Forms.ComboBox
        Me.DataSetAbonosProveedor = New LcPymes_5._2.DataSetAbonosProveedor
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtObservaciones = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox
        Me.txtCedulaUsuario = New System.Windows.Forms.TextBox
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton
        Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton
        Me.ToolBarAnular = New System.Windows.Forms.ToolBarButton
        Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton
        Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.gridFacturas = New DevExpress.XtraGrid.GridControl
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.Factura = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.Fecha = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtDevoluciones = New System.Windows.Forms.TextBox
        Me.btnDevolucion = New System.Windows.Forms.Button
        Me.TxtAbono = New DevExpress.XtraEditors.CalcEdit
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtFactura = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtMonto = New System.Windows.Forms.TextBox
        Me.txtSaldo = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtSaldoAct = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtAbonoSuMoneda = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.TextBoxTipoCambioFactura = New System.Windows.Forms.TextBox
        Me.TextBoxProveedorDetalle = New System.Windows.Forms.TextBox
        Me.TextBoxIdFactura = New System.Windows.Forms.TextBox
        Me.ComboBoxTipoPago = New System.Windows.Forms.ComboBox
        Me.TextBox1 = New DevExpress.XtraEditors.TextEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMonto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colSaldo_Ant = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colAbono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adAbonos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daDetalle_Abono = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtSaldoActGen = New System.Windows.Forms.TextBox
        Me.txtAbonoGen = New System.Windows.Forms.TextBox
        Me.txtSaldoAntGen = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.txtNum_Recibo = New System.Windows.Forms.TextBox
        Me.Check_Dig_Recibo = New System.Windows.Forms.CheckBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.GroupBox_Datos_Abonos = New System.Windows.Forms.GroupBox
        Me.Label_CodBanco = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.ComboBoxBanco = New System.Windows.Forms.ComboBox
        Me.ComboBoxCuentaBancariaDestino = New System.Windows.Forms.ComboBox
        Me.ComboBoxCuentaBancaria = New System.Windows.Forms.ComboBox
        Me.Adapter_Cuenta_Proveedor = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.Adapter_Proveedor = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.AdapterBancos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection2 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.AdapterCuentasBancarias = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.txtTipoCambio = New System.Windows.Forms.ComboBox
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.DataGrid2 = New System.Windows.Forms.DataGrid
        Me.TextBoxMontoLetras = New System.Windows.Forms.Label
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.cmdAsientos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand
        Me.conContabilidad = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand
        Me.cmdDetalleAsientos = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox_Datos_Cliente.SuspendLayout()
        CType(Me.dtFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetAbonosProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.gridFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.TxtAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_Datos_Abonos.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Image = CType(resources.GetObject("Label9.Image"), System.Drawing.Image)
        Me.Label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(-16, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(754, 32)
        Me.Label9.TabIndex = 67
        Me.Label9.Text = "Abonos a Proveedores"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(1, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 12)
        Me.Label1.TabIndex = 157
        Me.Label1.Text = "Recibo N�"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox_Datos_Cliente
        '
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.dtFecha)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.Label3)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.LabelFecha)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.txtCodigo)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.Label37)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.Label5)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.Label2)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.txtNombre)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.ComboMoneda)
        Me.GroupBox_Datos_Cliente.Controls.Add(Me.Label30)
        Me.GroupBox_Datos_Cliente.Enabled = False
        Me.GroupBox_Datos_Cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox_Datos_Cliente.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox_Datos_Cliente.Location = New System.Drawing.Point(10, 33)
        Me.GroupBox_Datos_Cliente.Name = "GroupBox_Datos_Cliente"
        Me.GroupBox_Datos_Cliente.Size = New System.Drawing.Size(702, 55)
        Me.GroupBox_Datos_Cliente.TabIndex = 1
        Me.GroupBox_Datos_Cliente.TabStop = False
        '
        'dtFecha
        '
        Me.dtFecha.EditValue = New Date(2009, 11, 19, 0, 0, 0, 0)
        Me.dtFecha.Location = New System.Drawing.Point(496, 32)
        Me.dtFecha.Name = "dtFecha"
        '
        'dtFecha.Properties
        '
        Me.dtFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtFecha.Properties.Enabled = False
        Me.dtFecha.Size = New System.Drawing.Size(88, 21)
        Me.dtFecha.TabIndex = 187
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(496, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 15)
        Me.Label3.TabIndex = 186
        Me.Label3.Text = "Fecha"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelFecha
        '
        Me.LabelFecha.BackColor = System.Drawing.Color.RoyalBlue
        Me.LabelFecha.Enabled = False
        Me.LabelFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFecha.ForeColor = System.Drawing.Color.White
        Me.LabelFecha.Location = New System.Drawing.Point(540, -2)
        Me.LabelFecha.Name = "LabelFecha"
        Me.LabelFecha.Size = New System.Drawing.Size(152, 16)
        Me.LabelFecha.TabIndex = 185
        Me.LabelFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCodigo
        '
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.ForeColor = System.Drawing.Color.Blue
        Me.txtCodigo.Location = New System.Drawing.Point(5, 33)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(59, 20)
        Me.txtCodigo.TabIndex = 0
        Me.txtCodigo.Text = ""
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(8, -1)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(688, 16)
        Me.Label37.TabIndex = 157
        Me.Label37.Text = "Datos del Proveedor"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(72, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(416, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Nombre"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "C�digo"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNombre
        '
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Enabled = False
        Me.txtNombre.ForeColor = System.Drawing.Color.Blue
        Me.txtNombre.Location = New System.Drawing.Point(72, 33)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(416, 20)
        Me.txtNombre.TabIndex = 1
        Me.txtNombre.Text = ""
        '
        'ComboMoneda
        '
        Me.ComboMoneda.DataSource = Me.DataSetAbonosProveedor
        Me.ComboMoneda.DisplayMember = "Moneda.MonedaNombre"
        Me.ComboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboMoneda.Enabled = False
        Me.ComboMoneda.ForeColor = System.Drawing.Color.Blue
        Me.ComboMoneda.Location = New System.Drawing.Point(592, 31)
        Me.ComboMoneda.Name = "ComboMoneda"
        Me.ComboMoneda.Size = New System.Drawing.Size(104, 21)
        Me.ComboMoneda.TabIndex = 2
        Me.ComboMoneda.ValueMember = "Moneda.CodMoneda"
        '
        'DataSetAbonosProveedor
        '
        Me.DataSetAbonosProveedor.DataSetName = "DataSetAbonosProveedor"
        Me.DataSetAbonosProveedor.Locale = New System.Globalization.CultureInfo("es")
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label30.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(592, 16)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(104, 15)
        Me.Label30.TabIndex = 164
        Me.Label30.Text = "Moneda"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.SystemColors.Control
        Me.Label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label29.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label29.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label29.Location = New System.Drawing.Point(8, 368)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(352, 16)
        Me.Label29.TabIndex = 160
        Me.Label29.Text = "Observaciones:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtObservaciones
        '
        Me.txtObservaciones.AutoSize = False
        Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservaciones.Enabled = False
        Me.txtObservaciones.ForeColor = System.Drawing.Color.Blue
        Me.txtObservaciones.Location = New System.Drawing.Point(8, 385)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(344, 27)
        Me.txtObservaciones.TabIndex = 23
        Me.txtObservaciones.Text = ""
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Controls.Add(Me.txtUsuario)
        Me.Panel1.Controls.Add(Me.txtNombreUsuario)
        Me.Panel1.Controls.Add(Me.txtCedulaUsuario)
        Me.Panel1.Location = New System.Drawing.Point(416, 474)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(291, 16)
        Me.Panel1.TabIndex = 0
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(-8, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtUsuario
        '
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(64, 0)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(56, 13)
        Me.txtUsuario.TabIndex = 1
        Me.txtUsuario.Text = ""
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.ControlDark
        Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(125, 0)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.ReadOnly = True
        Me.txtNombreUsuario.Size = New System.Drawing.Size(163, 13)
        Me.txtNombreUsuario.TabIndex = 2
        Me.txtNombreUsuario.Text = ""
        '
        'txtCedulaUsuario
        '
        Me.txtCedulaUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.txtCedulaUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCedulaUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCedulaUsuario.Enabled = False
        Me.txtCedulaUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtCedulaUsuario.Location = New System.Drawing.Point(216, 16)
        Me.txtCedulaUsuario.Name = "txtCedulaUsuario"
        Me.txtCedulaUsuario.Size = New System.Drawing.Size(72, 13)
        Me.txtCedulaUsuario.TabIndex = 170
        Me.txtCedulaUsuario.Text = ""
        '
        'ToolBar1
        '
        Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarAnular, Me.ToolBarImprimir, Me.ToolBarCerrar})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
        Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ToolBar1.Location = New System.Drawing.Point(0, 432)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(712, 58)
        Me.ToolBar1.TabIndex = 7
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Enabled = False
        Me.ToolBarNuevo.ImageIndex = 0
        Me.ToolBarNuevo.Text = "Nuevo"
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Enabled = False
        Me.ToolBarBuscar.ImageIndex = 1
        Me.ToolBarBuscar.Text = "Buscar"
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        Me.ToolBarRegistrar.ImageIndex = 2
        Me.ToolBarRegistrar.Text = "Registrar"
        '
        'ToolBarAnular
        '
        Me.ToolBarAnular.ImageIndex = 3
        Me.ToolBarAnular.Text = "Anular"
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        Me.ToolBarImprimir.ImageIndex = 7
        Me.ToolBarImprimir.Text = "Imprimir"
        '
        'ToolBarCerrar
        '
        Me.ToolBarCerrar.ImageIndex = 6
        Me.ToolBarCerrar.Text = "Cerrar"
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'gridFacturas
        '
        '
        'gridFacturas.EmbeddedNavigator
        '
        Me.gridFacturas.EmbeddedNavigator.Name = ""
        Me.gridFacturas.Location = New System.Drawing.Point(8, 99)
        Me.gridFacturas.MainView = Me.AdvBandedGridView1
        Me.gridFacturas.Name = "gridFacturas"
        Me.gridFacturas.Size = New System.Drawing.Size(176, 149)
        Me.gridFacturas.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.gridFacturas.TabIndex = 168
        Me.gridFacturas.Text = "GridControl1"
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.AdvBandedGridView1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.Factura, Me.Fecha})
        Me.AdvBandedGridView1.GroupPanelText = "Facturas Pendientes"
        Me.AdvBandedGridView1.IndicatorWidth = 8
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        Me.AdvBandedGridView1.OptionsView.ShowGroupedColumns = False
        Me.AdvBandedGridView1.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "Facturas Pendiente de Pago"
        Me.GridBand1.Columns.Add(Me.Factura)
        Me.GridBand1.Columns.Add(Me.Fecha)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.Width = 165
        '
        'Factura
        '
        Me.Factura.Caption = "Factura No."
        Me.Factura.FieldName = "Factura"
        Me.Factura.Name = "Factura"
        Me.Factura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Factura.SortIndex = 0
        Me.Factura.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Me.Factura.Visible = True
        Me.Factura.Width = 70
        '
        'Fecha
        '
        Me.Fecha.Caption = "Fecha"
        Me.Fecha.FieldName = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.Fecha.Visible = True
        Me.Fecha.Width = 95
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.GroupBox1.Controls.Add(Me.txtDevoluciones)
        Me.GroupBox1.Controls.Add(Me.btnDevolucion)
        Me.GroupBox1.Controls.Add(Me.TxtAbono)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtFactura)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtMonto)
        Me.GroupBox1.Controls.Add(Me.txtSaldo)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtSaldoAct)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtAbonoSuMoneda)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TextBoxTipoCambioFactura)
        Me.GroupBox1.Controls.Add(Me.TextBoxProveedorDetalle)
        Me.GroupBox1.Controls.Add(Me.TextBoxIdFactura)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(512, 128)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'txtDevoluciones
        '
        Me.txtDevoluciones.Location = New System.Drawing.Point(352, 24)
        Me.txtDevoluciones.Name = "txtDevoluciones"
        Me.txtDevoluciones.ReadOnly = True
        Me.txtDevoluciones.Size = New System.Drawing.Size(128, 20)
        Me.txtDevoluciones.TabIndex = 193
        Me.txtDevoluciones.Text = "0"
        Me.txtDevoluciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnDevolucion
        '
        Me.btnDevolucion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDevolucion.Location = New System.Drawing.Point(232, 24)
        Me.btnDevolucion.Name = "btnDevolucion"
        Me.btnDevolucion.Size = New System.Drawing.Size(120, 23)
        Me.btnDevolucion.TabIndex = 192
        Me.btnDevolucion.Text = "Aplicar Devoluci�n:"
        Me.btnDevolucion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxtAbono
        '
        Me.TxtAbono.Location = New System.Drawing.Point(352, 48)
        Me.TxtAbono.Name = "TxtAbono"
        '
        'TxtAbono.Properties
        '
        Me.TxtAbono.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TxtAbono.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TxtAbono.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TxtAbono.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TxtAbono.Size = New System.Drawing.Size(136, 21)
        Me.TxtAbono.TabIndex = 191
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.Color.Silver
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label26.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label26.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label26.Location = New System.Drawing.Point(240, 88)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(104, 16)
        Me.Label26.TabIndex = 184
        Me.Label26.Text = "AB Moneda Fact"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Silver
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Factura No."
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFactura
        '
        Me.txtFactura.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFactura.Enabled = False
        Me.txtFactura.ForeColor = System.Drawing.Color.Blue
        Me.txtFactura.Location = New System.Drawing.Point(104, 18)
        Me.txtFactura.Name = "txtFactura"
        Me.txtFactura.Size = New System.Drawing.Size(112, 13)
        Me.txtFactura.TabIndex = 0
        Me.txtFactura.Text = ""
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Silver
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label14.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(8, 37)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(88, 15)
        Me.Label14.TabIndex = 175
        Me.Label14.Text = "Mont. Fact."
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Silver
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label13.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 55)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 16)
        Me.Label13.TabIndex = 173
        Me.Label13.Text = "Saldo Anterior"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(8, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(496, 16)
        Me.Label4.TabIndex = 157
        Me.Label4.Text = "Datos de la Factura"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtMonto
        '
        Me.txtMonto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMonto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.MontoFactura"))
        Me.txtMonto.Enabled = False
        Me.txtMonto.ForeColor = System.Drawing.Color.Blue
        Me.txtMonto.Location = New System.Drawing.Point(104, 37)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(112, 13)
        Me.txtMonto.TabIndex = 2
        Me.txtMonto.Text = ""
        '
        'txtSaldo
        '
        Me.txtSaldo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Saldo_Ant"))
        Me.txtSaldo.Enabled = False
        Me.txtSaldo.ForeColor = System.Drawing.Color.Blue
        Me.txtSaldo.Location = New System.Drawing.Point(104, 55)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(112, 13)
        Me.txtSaldo.TabIndex = 3
        Me.txtSaldo.Text = ""
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Silver
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(240, 48)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(104, 16)
        Me.Label11.TabIndex = 169
        Me.Label11.Text = "Abono"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSaldoAct
        '
        Me.txtSaldoAct.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSaldoAct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoAct.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Saldo_Actual"))
        Me.txtSaldoAct.Enabled = False
        Me.txtSaldoAct.ForeColor = System.Drawing.Color.Blue
        Me.txtSaldoAct.Location = New System.Drawing.Point(352, 72)
        Me.txtSaldoAct.Name = "txtSaldoAct"
        Me.txtSaldoAct.Size = New System.Drawing.Size(136, 13)
        Me.txtSaldoAct.TabIndex = 5
        Me.txtSaldoAct.Text = ""
        Me.txtSaldoAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Silver
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(240, 72)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(104, 16)
        Me.Label12.TabIndex = 171
        Me.Label12.Text = "Saldo Actual"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAbonoSuMoneda
        '
        Me.txtAbonoSuMoneda.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAbonoSuMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAbonoSuMoneda.Enabled = False
        Me.txtAbonoSuMoneda.ForeColor = System.Drawing.Color.Blue
        Me.txtAbonoSuMoneda.Location = New System.Drawing.Point(352, 88)
        Me.txtAbonoSuMoneda.Name = "txtAbonoSuMoneda"
        Me.txtAbonoSuMoneda.Size = New System.Drawing.Size(136, 13)
        Me.txtAbonoSuMoneda.TabIndex = 183
        Me.txtAbonoSuMoneda.Text = ""
        Me.txtAbonoSuMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Silver
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 74)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(88, 16)
        Me.Label8.TabIndex = 165
        Me.Label8.Text = "T.C.F"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextBoxTipoCambioFactura
        '
        Me.TextBoxTipoCambioFactura.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxTipoCambioFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxTipoCambioFactura.Enabled = False
        Me.TextBoxTipoCambioFactura.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxTipoCambioFactura.Location = New System.Drawing.Point(104, 74)
        Me.TextBoxTipoCambioFactura.Name = "TextBoxTipoCambioFactura"
        Me.TextBoxTipoCambioFactura.Size = New System.Drawing.Size(112, 13)
        Me.TextBoxTipoCambioFactura.TabIndex = 188
        Me.TextBoxTipoCambioFactura.Text = ""
        '
        'TextBoxProveedorDetalle
        '
        Me.TextBoxProveedorDetalle.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxProveedorDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxProveedorDetalle.Enabled = False
        Me.TextBoxProveedorDetalle.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxProveedorDetalle.Location = New System.Drawing.Point(104, 92)
        Me.TextBoxProveedorDetalle.Name = "TextBoxProveedorDetalle"
        Me.TextBoxProveedorDetalle.Size = New System.Drawing.Size(112, 13)
        Me.TextBoxProveedorDetalle.TabIndex = 189
        Me.TextBoxProveedorDetalle.Text = ""
        Me.TextBoxProveedorDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBoxIdFactura
        '
        Me.TextBoxIdFactura.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxIdFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxIdFactura.Enabled = False
        Me.TextBoxIdFactura.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxIdFactura.Location = New System.Drawing.Point(8, 92)
        Me.TextBoxIdFactura.Name = "TextBoxIdFactura"
        Me.TextBoxIdFactura.Size = New System.Drawing.Size(88, 13)
        Me.TextBoxIdFactura.TabIndex = 190
        Me.TextBoxIdFactura.Text = ""
        Me.TextBoxIdFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ComboBoxTipoPago
        '
        Me.ComboBoxTipoPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxTipoPago.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ComboBoxTipoPago.Items.AddRange(New Object() {"CHEQUE", "TRANFERENCIA"})
        Me.ComboBoxTipoPago.Location = New System.Drawing.Point(8, 117)
        Me.ComboBoxTipoPago.Name = "ComboBoxTipoPago"
        Me.ComboBoxTipoPago.Size = New System.Drawing.Size(104, 21)
        Me.ComboBoxTipoPago.TabIndex = 2
        '
        'TextBox1
        '
        Me.TextBox1.EditValue = ""
        Me.TextBox1.Location = New System.Drawing.Point(120, 117)
        Me.TextBox1.Name = "TextBox1"
        '
        'TextBox1.Properties
        '
        Me.TextBox1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextBox1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextBox1.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Default, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.TextBox1.Size = New System.Drawing.Size(120, 21)
        Me.TextBox1.TabIndex = 3
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label20.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(8, 101)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(104, 16)
        Me.Label20.TabIndex = 186
        Me.Label20.Text = "Tipo Pago"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label19.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(8, 53)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(104, 21)
        Me.Label19.TabIndex = 184
        Me.Label19.Text = "Cuenta Bancaria"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "abonocpagar.abonocpagardetalle_abonocpagar"
        Me.GridControl2.DataSource = Me.DataSetAbonosProveedor
        '
        'GridControl2.EmbeddedNavigator
        '
        Me.GridControl2.EmbeddedNavigator.Name = ""
        Me.GridControl2.Location = New System.Drawing.Point(6, 256)
        Me.GridControl2.MainView = Me.GridView1
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1})
        Me.GridControl2.Size = New System.Drawing.Size(704, 104)
        Me.GridControl2.Styles.AddReplace("Style1", New DevExpress.Utils.ViewStyleEx("Style1", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl2.TabIndex = 1
        Me.GridControl2.Text = "GridControl2"
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFactura, Me.colMonto, Me.colSaldo_Ant, Me.colAbono, Me.colSaldo})
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colFactura
        '
        Me.colFactura.Caption = "Factura"
        Me.colFactura.FieldName = "Factura"
        Me.colFactura.Name = "colFactura"
        Me.colFactura.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFactura.VisibleIndex = 0
        Me.colFactura.Width = 114
        '
        'colMonto
        '
        Me.colMonto.Caption = "Monto Fact."
        Me.colMonto.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colMonto.FieldName = "MontoFactura"
        Me.colMonto.Name = "colMonto"
        Me.colMonto.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colMonto.VisibleIndex = 1
        Me.colMonto.Width = 111
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.DisplayFormat.FormatString = "#,#0.00"
        Me.RepositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'colSaldo_Ant
        '
        Me.colSaldo_Ant.Caption = "Saldo Ant."
        Me.colSaldo_Ant.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colSaldo_Ant.DisplayFormat.FormatString = "#,#0.00"
        Me.colSaldo_Ant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSaldo_Ant.FieldName = "Saldo_Ant"
        Me.colSaldo_Ant.Name = "colSaldo_Ant"
        Me.colSaldo_Ant.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colSaldo_Ant.SummaryItem.DisplayFormat = "#,#0.00"
        Me.colSaldo_Ant.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSaldo_Ant.VisibleIndex = 2
        Me.colSaldo_Ant.Width = 158
        '
        'colAbono
        '
        Me.colAbono.Caption = "Abono"
        Me.colAbono.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colAbono.DisplayFormat.FormatString = "#,#0.00"
        Me.colAbono.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colAbono.FieldName = "Abono"
        Me.colAbono.Name = "colAbono"
        Me.colAbono.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colAbono.SummaryItem.DisplayFormat = "#,#0.00"
        Me.colAbono.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colAbono.VisibleIndex = 3
        Me.colAbono.Width = 141
        '
        'colSaldo
        '
        Me.colSaldo.Caption = "Saldo Actual"
        Me.colSaldo.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colSaldo.DisplayFormat.FormatString = "#,#0.00"
        Me.colSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSaldo.FieldName = "Saldo_Actual"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.Options = CType(((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colSaldo.StyleName = "Style1"
        Me.colSaldo.SummaryItem.DisplayFormat = "#,#0.00"
        Me.colSaldo.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSaldo.VisibleIndex = 4
        Me.colSaldo.Width = 163
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'adAbonos
        '
        Me.adAbonos.DeleteCommand = Me.SqlDeleteCommand1
        Me.adAbonos.InsertCommand = Me.SqlInsertCommand1
        Me.adAbonos.SelectCommand = Me.SqlSelectCommand1
        Me.adAbonos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "abonocpagar", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Abonocpagar", "Id_Abonocpagar"), New System.Data.Common.DataColumnMapping("Documento", "Documento"), New System.Data.Common.DataColumnMapping("TipoDocumento", "TipoDocumento"), New System.Data.Common.DataColumnMapping("CuentaBancaria", "CuentaBancaria"), New System.Data.Common.DataColumnMapping("Codigo_banco", "Codigo_banco"), New System.Data.Common.DataColumnMapping("Saldo_Cuenta", "Saldo_Cuenta"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Saldo_Actual", "Saldo_Actual"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Contabilizado", "Contabilizado"), New System.Data.Common.DataColumnMapping("Recibo", "Recibo"), New System.Data.Common.DataColumnMapping("Cedula_Usuario", "Cedula_Usuario"), New System.Data.Common.DataColumnMapping("Cod_Proveedor", "Cod_Proveedor"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("cod_Moneda", "cod_Moneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio"), New System.Data.Common.DataColumnMapping("MontoLetras", "MontoLetras"), New System.Data.Common.DataColumnMapping("CuentaDestino", "CuentaDestino"), New System.Data.Common.DataColumnMapping("FechaEntrada", "FechaEntrada")})})
        Me.adAbonos.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM abonocpagar WHERE (Id_Abonocpagar = @Original_Id_Abonocpagar) AND (An" & _
        "ulado = @Original_Anulado) AND (Cedula_Usuario = @Original_Cedula_Usuario) AND (" & _
        "Cod_Proveedor = @Original_Cod_Proveedor) AND (Codigo_banco = @Original_Codigo_ba" & _
        "nco) AND (Contabilizado = @Original_Contabilizado) AND (CuentaBancaria = @Origin" & _
        "al_CuentaBancaria) AND (CuentaDestino = @Original_CuentaDestino) AND (Documento " & _
        "= @Original_Documento) AND (Fecha = @Original_Fecha) AND (FechaEntrada = @Origin" & _
        "al_FechaEntrada) AND (Monto = @Original_Monto) AND (MontoLetras = @Original_Mont" & _
        "oLetras) AND (Recibo = @Original_Recibo) AND (Saldo_Actual = @Original_Saldo_Act" & _
        "ual) AND (Saldo_Cuenta = @Original_Saldo_Cuenta) AND (TipoCambio = @Original_Tip" & _
        "oCambio) AND (TipoDocumento = @Original_TipoDocumento) AND (cod_Moneda = @Origin" & _
        "al_cod_Moneda)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Abonocpagar", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Abonocpagar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula_Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Proveedor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo_banco", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo_banco", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CuentaBancaria", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaBancaria", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CuentaDestino", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaDestino", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoLetras", System.Data.SqlDbType.VarChar, 350, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoLetras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Recibo", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Recibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Actual", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Actual", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Cuenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_cod_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cod_Moneda", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO abonocpagar(Documento, TipoDocumento, CuentaBancaria, Codigo_banco, S" & _
        "aldo_Cuenta, Monto, Saldo_Actual, Fecha, Contabilizado, Recibo, Cedula_Usuario, " & _
        "Cod_Proveedor, Anulado, cod_Moneda, TipoCambio, MontoLetras, CuentaDestino, Fech" & _
        "aEntrada) VALUES (@Documento, @TipoDocumento, @CuentaBancaria, @Codigo_banco, @S" & _
        "aldo_Cuenta, @Monto, @Saldo_Actual, @Fecha, @Contabilizado, @Recibo, @Cedula_Usu" & _
        "ario, @Cod_Proveedor, @Anulado, @cod_Moneda, @TipoCambio, @MontoLetras, @CuentaD" & _
        "estino, @FechaEntrada); SELECT Id_Abonocpagar, Documento, TipoDocumento, CuentaB" & _
        "ancaria, Codigo_banco, Saldo_Cuenta, Monto, Saldo_Actual, Fecha, Contabilizado, " & _
        "Recibo, Cedula_Usuario, Cod_Proveedor, Anulado, cod_Moneda, TipoCambio, MontoLet" & _
        "ras, CuentaDestino, FechaEntrada FROM abonocpagar WHERE (Id_Abonocpagar = @@IDEN" & _
        "TITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.BigInt, 8, "Documento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 20, "TipoDocumento"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CuentaBancaria", System.Data.SqlDbType.VarChar, 255, "CuentaBancaria"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo_banco", System.Data.SqlDbType.BigInt, 8, "Codigo_banco"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Cuenta", System.Data.SqlDbType.Float, 8, "Saldo_Cuenta"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Actual", System.Data.SqlDbType.Float, 8, "Saldo_Actual"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 1, "Contabilizado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Recibo", System.Data.SqlDbType.VarChar, 255, "Recibo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula_Usuario", System.Data.SqlDbType.VarChar, 75, "Cedula_Usuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Proveedor", System.Data.SqlDbType.Int, 4, "Cod_Proveedor"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@cod_Moneda", System.Data.SqlDbType.Int, 4, "cod_Moneda"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoLetras", System.Data.SqlDbType.VarChar, 350, "MontoLetras"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CuentaDestino", System.Data.SqlDbType.BigInt, 8, "CuentaDestino"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 8, "FechaEntrada"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id_Abonocpagar, Documento, TipoDocumento, CuentaBancaria, Codigo_banco, Sa" & _
        "ldo_Cuenta, Monto, Saldo_Actual, Fecha, Contabilizado, Recibo, Cedula_Usuario, C" & _
        "od_Proveedor, Anulado, cod_Moneda, TipoCambio, MontoLetras, CuentaDestino, Fecha" & _
        "Entrada FROM abonocpagar"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE abonocpagar SET Documento = @Documento, TipoDocumento = @TipoDocumento, Cu" & _
        "entaBancaria = @CuentaBancaria, Codigo_banco = @Codigo_banco, Saldo_Cuenta = @Sa" & _
        "ldo_Cuenta, Monto = @Monto, Saldo_Actual = @Saldo_Actual, Fecha = @Fecha, Contab" & _
        "ilizado = @Contabilizado, Recibo = @Recibo, Cedula_Usuario = @Cedula_Usuario, Co" & _
        "d_Proveedor = @Cod_Proveedor, Anulado = @Anulado, cod_Moneda = @cod_Moneda, Tipo" & _
        "Cambio = @TipoCambio, MontoLetras = @MontoLetras, CuentaDestino = @CuentaDestino" & _
        ", FechaEntrada = @FechaEntrada WHERE (Id_Abonocpagar = @Original_Id_Abonocpagar)" & _
        " AND (Anulado = @Original_Anulado) AND (Cedula_Usuario = @Original_Cedula_Usuari" & _
        "o) AND (Cod_Proveedor = @Original_Cod_Proveedor) AND (Codigo_banco = @Original_C" & _
        "odigo_banco) AND (Contabilizado = @Original_Contabilizado) AND (CuentaBancaria =" & _
        " @Original_CuentaBancaria) AND (CuentaDestino = @Original_CuentaDestino) AND (Do" & _
        "cumento = @Original_Documento) AND (Fecha = @Original_Fecha) AND (FechaEntrada =" & _
        " @Original_FechaEntrada) AND (Monto = @Original_Monto) AND (MontoLetras = @Origi" & _
        "nal_MontoLetras) AND (Recibo = @Original_Recibo) AND (Saldo_Actual = @Original_S" & _
        "aldo_Actual) AND (Saldo_Cuenta = @Original_Saldo_Cuenta) AND (TipoCambio = @Orig" & _
        "inal_TipoCambio) AND (TipoDocumento = @Original_TipoDocumento) AND (cod_Moneda =" & _
        " @Original_cod_Moneda); SELECT Id_Abonocpagar, Documento, TipoDocumento, CuentaB" & _
        "ancaria, Codigo_banco, Saldo_Cuenta, Monto, Saldo_Actual, Fecha, Contabilizado, " & _
        "Recibo, Cedula_Usuario, Cod_Proveedor, Anulado, cod_Moneda, TipoCambio, MontoLet" & _
        "ras, CuentaDestino, FechaEntrada FROM abonocpagar WHERE (Id_Abonocpagar = @Id_Ab" & _
        "onocpagar)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Documento", System.Data.SqlDbType.BigInt, 8, "Documento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDocumento", System.Data.SqlDbType.VarChar, 20, "TipoDocumento"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CuentaBancaria", System.Data.SqlDbType.VarChar, 255, "CuentaBancaria"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo_banco", System.Data.SqlDbType.BigInt, 8, "Codigo_banco"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Cuenta", System.Data.SqlDbType.Float, 8, "Saldo_Cuenta"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Actual", System.Data.SqlDbType.Float, 8, "Saldo_Actual"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 1, "Contabilizado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Recibo", System.Data.SqlDbType.VarChar, 255, "Recibo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula_Usuario", System.Data.SqlDbType.VarChar, 75, "Cedula_Usuario"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Proveedor", System.Data.SqlDbType.Int, 4, "Cod_Proveedor"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@cod_Moneda", System.Data.SqlDbType.Int, 4, "cod_Moneda"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoLetras", System.Data.SqlDbType.VarChar, 350, "MontoLetras"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CuentaDestino", System.Data.SqlDbType.BigInt, 8, "CuentaDestino"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 8, "FechaEntrada"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Abonocpagar", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Abonocpagar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula_Usuario", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula_Usuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Proveedor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo_banco", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo_banco", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CuentaBancaria", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaBancaria", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CuentaDestino", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CuentaDestino", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Documento", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Documento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoLetras", System.Data.SqlDbType.VarChar, 350, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoLetras", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Recibo", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Recibo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Actual", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Actual", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Cuenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDocumento", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDocumento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_cod_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cod_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Abonocpagar", System.Data.SqlDbType.BigInt, 8, "Id_Abonocpagar"))
        '
        'daDetalle_Abono
        '
        Me.daDetalle_Abono.DeleteCommand = Me.SqlDeleteCommand2
        Me.daDetalle_Abono.InsertCommand = Me.SqlInsertCommand2
        Me.daDetalle_Abono.SelectCommand = Me.SqlSelectCommand2
        Me.daDetalle_Abono.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "detalle_abonocpagar", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Detalle_abonocpagar", "Id_Detalle_abonocpagar"), New System.Data.Common.DataColumnMapping("Factura", "Factura"), New System.Data.Common.DataColumnMapping("Cod_Proveedor", "Cod_Proveedor"), New System.Data.Common.DataColumnMapping("MontoFactura", "MontoFactura"), New System.Data.Common.DataColumnMapping("Saldo_Ant", "Saldo_Ant"), New System.Data.Common.DataColumnMapping("Abono", "Abono"), New System.Data.Common.DataColumnMapping("Abono_SuMoneda", "Abono_SuMoneda"), New System.Data.Common.DataColumnMapping("Saldo_Actual", "Saldo_Actual"), New System.Data.Common.DataColumnMapping("Id_Abonocpagar", "Id_Abonocpagar"), New System.Data.Common.DataColumnMapping("Id_Compra", "Id_Compra")})})
        Me.daDetalle_Abono.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM detalle_abonocpagar WHERE (Id_Detalle_abonocpagar = @Original_Id_Deta" & _
        "lle_abonocpagar) AND (Abono = @Original_Abono) AND (Abono_SuMoneda = @Original_A" & _
        "bono_SuMoneda) AND (Cod_Proveedor = @Original_Cod_Proveedor) AND (Factura = @Ori" & _
        "ginal_Factura) AND (Id_Abonocpagar = @Original_Id_Abonocpagar) AND (Id_Compra = " & _
        "@Original_Id_Compra) AND (MontoFactura = @Original_MontoFactura) AND (Saldo_Actu" & _
        "al = @Original_Saldo_Actual) AND (Saldo_Ant = @Original_Saldo_Ant)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Detalle_abonocpagar", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Detalle_abonocpagar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Abono", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Abono_SuMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono_SuMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Proveedor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Factura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Abonocpagar", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Abonocpagar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Compra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Compra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoFactura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Actual", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Actual", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Ant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Ant", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO detalle_abonocpagar(Factura, Cod_Proveedor, MontoFactura, Saldo_Ant, " & _
        "Abono, Abono_SuMoneda, Saldo_Actual, Id_Abonocpagar, Id_Compra) VALUES (@Factura" & _
        ", @Cod_Proveedor, @MontoFactura, @Saldo_Ant, @Abono, @Abono_SuMoneda, @Saldo_Act" & _
        "ual, @Id_Abonocpagar, @Id_Compra); SELECT Id_Detalle_abonocpagar, Factura, Cod_P" & _
        "roveedor, MontoFactura, Saldo_Ant, Abono, Abono_SuMoneda, Saldo_Actual, Id_Abono" & _
        "cpagar, Id_Compra FROM detalle_abonocpagar WHERE (Id_Detalle_abonocpagar = @@IDE" & _
        "NTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Factura", System.Data.SqlDbType.Float, 8, "Factura"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Proveedor", System.Data.SqlDbType.Int, 4, "Cod_Proveedor"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoFactura", System.Data.SqlDbType.Float, 8, "MontoFactura"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Ant", System.Data.SqlDbType.Float, 8, "Saldo_Ant"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Abono", System.Data.SqlDbType.Float, 8, "Abono"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Abono_SuMoneda", System.Data.SqlDbType.Float, 8, "Abono_SuMoneda"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Actual", System.Data.SqlDbType.Float, 8, "Saldo_Actual"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Abonocpagar", System.Data.SqlDbType.BigInt, 8, "Id_Abonocpagar"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Compra", System.Data.SqlDbType.Float, 8, "Id_Compra"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Id_Detalle_abonocpagar, Factura, Cod_Proveedor, MontoFactura, Saldo_Ant, A" & _
        "bono, Abono_SuMoneda, Saldo_Actual, Id_Abonocpagar, Id_Compra FROM detalle_abono" & _
        "cpagar"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE detalle_abonocpagar SET Factura = @Factura, Cod_Proveedor = @Cod_Proveedor" & _
        ", MontoFactura = @MontoFactura, Saldo_Ant = @Saldo_Ant, Abono = @Abono, Abono_Su" & _
        "Moneda = @Abono_SuMoneda, Saldo_Actual = @Saldo_Actual, Id_Abonocpagar = @Id_Abo" & _
        "nocpagar, Id_Compra = @Id_Compra WHERE (Id_Detalle_abonocpagar = @Original_Id_De" & _
        "talle_abonocpagar) AND (Abono = @Original_Abono) AND (Abono_SuMoneda = @Original" & _
        "_Abono_SuMoneda) AND (Cod_Proveedor = @Original_Cod_Proveedor) AND (Factura = @O" & _
        "riginal_Factura) AND (Id_Abonocpagar = @Original_Id_Abonocpagar) AND (Id_Compra " & _
        "= @Original_Id_Compra) AND (MontoFactura = @Original_MontoFactura) AND (Saldo_Ac" & _
        "tual = @Original_Saldo_Actual) AND (Saldo_Ant = @Original_Saldo_Ant); SELECT Id_" & _
        "Detalle_abonocpagar, Factura, Cod_Proveedor, MontoFactura, Saldo_Ant, Abono, Abo" & _
        "no_SuMoneda, Saldo_Actual, Id_Abonocpagar, Id_Compra FROM detalle_abonocpagar WH" & _
        "ERE (Id_Detalle_abonocpagar = @Id_Detalle_abonocpagar)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Factura", System.Data.SqlDbType.Float, 8, "Factura"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Proveedor", System.Data.SqlDbType.Int, 4, "Cod_Proveedor"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoFactura", System.Data.SqlDbType.Float, 8, "MontoFactura"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Ant", System.Data.SqlDbType.Float, 8, "Saldo_Ant"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Abono", System.Data.SqlDbType.Float, 8, "Abono"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Abono_SuMoneda", System.Data.SqlDbType.Float, 8, "Abono_SuMoneda"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Saldo_Actual", System.Data.SqlDbType.Float, 8, "Saldo_Actual"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Abonocpagar", System.Data.SqlDbType.BigInt, 8, "Id_Abonocpagar"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Compra", System.Data.SqlDbType.Float, 8, "Id_Compra"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Detalle_abonocpagar", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Detalle_abonocpagar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Abono", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Abono_SuMoneda", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono_SuMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Proveedor", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Proveedor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Factura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Factura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Abonocpagar", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Abonocpagar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Compra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Compra", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoFactura", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoFactura", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Actual", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Actual", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Saldo_Ant", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Ant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Detalle_abonocpagar", System.Data.SqlDbType.Int, 4, "Id_Detalle_abonocpagar"))
        '
        'daMoneda
        '
        Me.daMoneda.SelectCommand = Me.SqlSelectCommand3
        Me.daMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Monedas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Monedas"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(608, 384)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(104, 16)
        Me.Label16.TabIndex = 159
        Me.Label16.Text = "Monto Recibos"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(496, 384)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(104, 16)
        Me.Label18.TabIndex = 161
        Me.Label18.Text = "Saldo Act."
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(368, 368)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(344, 16)
        Me.Label15.TabIndex = 157
        Me.Label15.Text = "Saldos de la Cuenta"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSaldoActGen
        '
        Me.txtSaldoActGen.BackColor = System.Drawing.SystemColors.Control
        Me.txtSaldoActGen.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSaldoActGen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoActGen.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.Saldo_Actual"))
        Me.txtSaldoActGen.ForeColor = System.Drawing.Color.Blue
        Me.txtSaldoActGen.Location = New System.Drawing.Point(496, 400)
        Me.txtSaldoActGen.Name = "txtSaldoActGen"
        Me.txtSaldoActGen.ReadOnly = True
        Me.txtSaldoActGen.Size = New System.Drawing.Size(104, 13)
        Me.txtSaldoActGen.TabIndex = 162
        Me.txtSaldoActGen.Text = ""
        Me.txtSaldoActGen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAbonoGen
        '
        Me.txtAbonoGen.BackColor = System.Drawing.SystemColors.Control
        Me.txtAbonoGen.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAbonoGen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAbonoGen.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.Monto"))
        Me.txtAbonoGen.ForeColor = System.Drawing.Color.Blue
        Me.txtAbonoGen.Location = New System.Drawing.Point(608, 400)
        Me.txtAbonoGen.Name = "txtAbonoGen"
        Me.txtAbonoGen.ReadOnly = True
        Me.txtAbonoGen.Size = New System.Drawing.Size(104, 13)
        Me.txtAbonoGen.TabIndex = 160
        Me.txtAbonoGen.Text = ""
        Me.txtAbonoGen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSaldoAntGen
        '
        Me.txtSaldoAntGen.BackColor = System.Drawing.SystemColors.Control
        Me.txtSaldoAntGen.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSaldoAntGen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoAntGen.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.Saldo_Cuenta"))
        Me.txtSaldoAntGen.ForeColor = System.Drawing.Color.Blue
        Me.txtSaldoAntGen.Location = New System.Drawing.Point(368, 400)
        Me.txtSaldoAntGen.Name = "txtSaldoAntGen"
        Me.txtSaldoAntGen.ReadOnly = True
        Me.txtSaldoAntGen.Size = New System.Drawing.Size(120, 13)
        Me.txtSaldoAntGen.TabIndex = 158
        Me.txtSaldoAntGen.Text = ""
        Me.txtSaldoAntGen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(368, 384)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(120, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Saldo Ant."
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'CheckBox1
        '
        Me.CheckBox1.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox1.Enabled = False
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.Red
        Me.CheckBox1.Location = New System.Drawing.Point(720, 32)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(105, 16)
        Me.CheckBox1.TabIndex = 172
        Me.CheckBox1.Text = "Anulada"
        '
        'txtNum_Recibo
        '
        Me.txtNum_Recibo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNum_Recibo.Enabled = False
        Me.txtNum_Recibo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNum_Recibo.Location = New System.Drawing.Point(74, 17)
        Me.txtNum_Recibo.Name = "txtNum_Recibo"
        Me.txtNum_Recibo.Size = New System.Drawing.Size(72, 13)
        Me.txtNum_Recibo.TabIndex = 159
        Me.txtNum_Recibo.Text = ""
        '
        'Check_Dig_Recibo
        '
        Me.Check_Dig_Recibo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Check_Dig_Recibo.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Check_Dig_Recibo.Location = New System.Drawing.Point(864, 32)
        Me.Check_Dig_Recibo.Name = "Check_Dig_Recibo"
        Me.Check_Dig_Recibo.Size = New System.Drawing.Size(104, 16)
        Me.Check_Dig_Recibo.TabIndex = 173
        Me.Check_Dig_Recibo.Text = "Digitar Recibo"
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label21.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(120, 101)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(120, 16)
        Me.Label21.TabIndex = 189
        Me.Label21.Text = "Documento"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(8, 13)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(496, 16)
        Me.Label22.TabIndex = 184
        Me.Label22.Text = "Origen de la Forma de Pago"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox_Datos_Abonos
        '
        Me.GroupBox_Datos_Abonos.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label_CodBanco)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label6)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label25)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label24)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label23)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.ComboBoxBanco)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.ComboBoxCuentaBancariaDestino)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label21)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.ComboBoxTipoPago)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.TextBox1)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label19)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label20)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.ComboBoxCuentaBancaria)
        Me.GroupBox_Datos_Abonos.Controls.Add(Me.Label22)
        Me.GroupBox_Datos_Abonos.Enabled = False
        Me.GroupBox_Datos_Abonos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox_Datos_Abonos.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox_Datos_Abonos.Location = New System.Drawing.Point(0, -8)
        Me.GroupBox_Datos_Abonos.Name = "GroupBox_Datos_Abonos"
        Me.GroupBox_Datos_Abonos.Size = New System.Drawing.Size(528, 151)
        Me.GroupBox_Datos_Abonos.TabIndex = 0
        Me.GroupBox_Datos_Abonos.TabStop = False
        '
        'Label_CodBanco
        '
        Me.Label_CodBanco.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label_CodBanco.Font = New System.Drawing.Font("OCR A Extended", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_CodBanco.ForeColor = System.Drawing.Color.Lime
        Me.Label_CodBanco.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label_CodBanco.Location = New System.Drawing.Point(416, 13)
        Me.Label_CodBanco.Name = "Label_CodBanco"
        Me.Label_CodBanco.Size = New System.Drawing.Size(88, 16)
        Me.Label_CodBanco.TabIndex = 196
        Me.Label_CodBanco.Text = "000000"
        Me.Label_CodBanco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(248, 103)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(256, 16)
        Me.Label6.TabIndex = 197
        Me.Label6.Text = "Cuenta Destino"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label25.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label25.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label25.Location = New System.Drawing.Point(272, 56)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(232, 16)
        Me.Label25.TabIndex = 195
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(8, 85)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(496, 16)
        Me.Label24.TabIndex = 194
        Me.Label24.Text = "Forma de Pago a Proveedor"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label23.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(8, 30)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(104, 18)
        Me.Label23.TabIndex = 193
        Me.Label23.Text = "Banco"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ComboBoxBanco
        '
        Me.ComboBoxBanco.DataSource = Me.DataSetAbonosProveedor
        Me.ComboBoxBanco.DisplayMember = "Bancos.Descripcion"
        Me.ComboBoxBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBanco.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxBanco.ItemHeight = 13
        Me.ComboBoxBanco.Location = New System.Drawing.Point(112, 30)
        Me.ComboBoxBanco.Name = "ComboBoxBanco"
        Me.ComboBoxBanco.Size = New System.Drawing.Size(392, 21)
        Me.ComboBoxBanco.TabIndex = 0
        Me.ComboBoxBanco.ValueMember = "Bancos.Codigo_banco"
        '
        'ComboBoxCuentaBancariaDestino
        '
        Me.ComboBoxCuentaBancariaDestino.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetAbonosProveedor, "abonocpagar.CuentaDestino"))
        Me.ComboBoxCuentaBancariaDestino.DataSource = Me.DataSetAbonosProveedor
        Me.ComboBoxCuentaBancariaDestino.DisplayMember = "Proveedores.ProveedoresCuentas_Bancarias_Proveedor.Num_Cuenta"
        Me.ComboBoxCuentaBancariaDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxCuentaBancariaDestino.Enabled = False
        Me.ComboBoxCuentaBancariaDestino.Location = New System.Drawing.Point(248, 117)
        Me.ComboBoxCuentaBancariaDestino.Name = "ComboBoxCuentaBancariaDestino"
        Me.ComboBoxCuentaBancariaDestino.Size = New System.Drawing.Size(256, 21)
        Me.ComboBoxCuentaBancariaDestino.TabIndex = 4
        Me.ComboBoxCuentaBancariaDestino.ValueMember = "Cuentas_Bancarias_Proveedor.Id_Cuenta"
        '
        'ComboBoxCuentaBancaria
        '
        Me.ComboBoxCuentaBancaria.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.CuentaBancaria"))
        Me.ComboBoxCuentaBancaria.DataSource = Me.DataSetAbonosProveedor
        Me.ComboBoxCuentaBancaria.DisplayMember = "Bancos.BancosCuentas_bancarias.Cuenta"
        Me.ComboBoxCuentaBancaria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxCuentaBancaria.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCuentaBancaria.ItemHeight = 13
        Me.ComboBoxCuentaBancaria.Location = New System.Drawing.Point(120, 53)
        Me.ComboBoxCuentaBancaria.Name = "ComboBoxCuentaBancaria"
        Me.ComboBoxCuentaBancaria.Size = New System.Drawing.Size(144, 21)
        Me.ComboBoxCuentaBancaria.TabIndex = 1
        Me.ComboBoxCuentaBancaria.ValueMember = "Cuentas_bancarias.Id_CuentaBancaria"
        '
        'Adapter_Cuenta_Proveedor
        '
        Me.Adapter_Cuenta_Proveedor.DeleteCommand = Me.SqlDeleteCommand3
        Me.Adapter_Cuenta_Proveedor.InsertCommand = Me.SqlInsertCommand3
        Me.Adapter_Cuenta_Proveedor.SelectCommand = Me.SqlSelectCommand4
        Me.Adapter_Cuenta_Proveedor.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Cuentas_Bancarias_Proveedor", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Cuenta", "Id_Cuenta"), New System.Data.Common.DataColumnMapping("CodigoProv", "CodigoProv"), New System.Data.Common.DataColumnMapping("TipoCuenta", "TipoCuenta"), New System.Data.Common.DataColumnMapping("Banco", "Banco"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("Num_Cuenta", "Num_Cuenta"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre")})})
        Me.Adapter_Cuenta_Proveedor.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM SeePOS.dbo.Cuentas_Bancarias_Proveedor WHERE (Id_Cuenta = @Original_I" & _
        "d_Cuenta) AND (Banco = @Original_Banco) AND (CodMoneda = @Original_CodMoneda) AN" & _
        "D (CodigoProv = @Original_CodigoProv) AND (MonedaNombre = @Original_MonedaNombre" & _
        ") AND (Num_Cuenta = @Original_Num_Cuenta) AND (TipoCuenta = @Original_TipoCuenta" & _
        ")"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Cuenta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Banco", System.Data.SqlDbType.VarChar, 70, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Banco", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodigoProv", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoProv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Cuenta", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCuenta", System.Data.SqlDbType.VarChar, 35, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCuenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO SeePOS.dbo.Cuentas_Bancarias_Proveedor(CodigoProv, TipoCuenta, Banco," & _
        " CodMoneda, Num_Cuenta, MonedaNombre) VALUES (@CodigoProv, @TipoCuenta, @Banco, " & _
        "@CodMoneda, @Num_Cuenta, @MonedaNombre); SELECT Id_Cuenta, CodigoProv, TipoCuent" & _
        "a, Banco, CodMoneda, Num_Cuenta, MonedaNombre FROM SeePOS.dbo.Cuentas_Bancarias_" & _
        "Proveedor WHERE (Id_Cuenta = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodigoProv", System.Data.SqlDbType.Int, 4, "CodigoProv"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCuenta", System.Data.SqlDbType.VarChar, 35, "TipoCuenta"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Banco", System.Data.SqlDbType.VarChar, 70, "Banco"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Cuenta", System.Data.SqlDbType.VarChar, 50, "Num_Cuenta"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT Id_Cuenta, CodigoProv, TipoCuenta, Banco, CodMoneda, Num_Cuenta, MonedaNom" & _
        "bre FROM SeePOS.dbo.Cuentas_Bancarias_Proveedor"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE SeePOS.dbo.Cuentas_Bancarias_Proveedor SET CodigoProv = @CodigoProv, TipoC" & _
        "uenta = @TipoCuenta, Banco = @Banco, CodMoneda = @CodMoneda, Num_Cuenta = @Num_C" & _
        "uenta, MonedaNombre = @MonedaNombre WHERE (Id_Cuenta = @Original_Id_Cuenta) AND " & _
        "(Banco = @Original_Banco) AND (CodMoneda = @Original_CodMoneda) AND (CodigoProv " & _
        "= @Original_CodigoProv) AND (MonedaNombre = @Original_MonedaNombre) AND (Num_Cue" & _
        "nta = @Original_Num_Cuenta) AND (TipoCuenta = @Original_TipoCuenta); SELECT Id_C" & _
        "uenta, CodigoProv, TipoCuenta, Banco, CodMoneda, Num_Cuenta, MonedaNombre FROM S" & _
        "eePOS.dbo.Cuentas_Bancarias_Proveedor WHERE (Id_Cuenta = @Id_Cuenta)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodigoProv", System.Data.SqlDbType.Int, 4, "CodigoProv"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCuenta", System.Data.SqlDbType.VarChar, 35, "TipoCuenta"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Banco", System.Data.SqlDbType.VarChar, 70, "Banco"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Num_Cuenta", System.Data.SqlDbType.VarChar, 50, "Num_Cuenta"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_Cuenta", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Banco", System.Data.SqlDbType.VarChar, 70, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Banco", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodigoProv", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoProv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Num_Cuenta", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCuenta", System.Data.SqlDbType.VarChar, 35, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_Cuenta", System.Data.SqlDbType.Int, 4, "Id_Cuenta"))
        '
        'Adapter_Proveedor
        '
        Me.Adapter_Proveedor.DeleteCommand = Me.SqlDeleteCommand6
        Me.Adapter_Proveedor.InsertCommand = Me.SqlInsertCommand6
        Me.Adapter_Proveedor.SelectCommand = Me.SqlSelectCommand5
        Me.Adapter_Proveedor.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Proveedores", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoProv", "CodigoProv"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Contacto", "Contacto"), New System.Data.Common.DataColumnMapping("Telefono_Cont", "Telefono_Cont"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Telefono1", "Telefono1"), New System.Data.Common.DataColumnMapping("Telefono2", "Telefono2"), New System.Data.Common.DataColumnMapping("Fax1", "Fax1"), New System.Data.Common.DataColumnMapping("Fax2", "Fax2"), New System.Data.Common.DataColumnMapping("Email", "Email"), New System.Data.Common.DataColumnMapping("Direccion", "Direccion"), New System.Data.Common.DataColumnMapping("MontoCredito", "MontoCredito"), New System.Data.Common.DataColumnMapping("Plazo", "Plazo"), New System.Data.Common.DataColumnMapping("CostoTotal", "CostoTotal"), New System.Data.Common.DataColumnMapping("ImpIncluido", "ImpIncluido"), New System.Data.Common.DataColumnMapping("PedidosMes", "PedidosMes"), New System.Data.Common.DataColumnMapping("Utilidad_Inventario", "Utilidad_Inventario"), New System.Data.Common.DataColumnMapping("Utilidad_Fija", "Utilidad_Fija")})})
        Me.Adapter_Proveedor.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM Proveedores WHERE (CodigoProv = @Original_CodigoProv) AND (Cedula = @" & _
        "Original_Cedula) AND (Contacto = @Original_Contacto) AND (CostoTotal = @Original" & _
        "_CostoTotal) AND (Direccion = @Original_Direccion) AND (Email = @Original_Email)" & _
        " AND (Fax1 = @Original_Fax1) AND (Fax2 = @Original_Fax2) AND (ImpIncluido = @Ori" & _
        "ginal_ImpIncluido) AND (MontoCredito = @Original_MontoCredito) AND (Nombre = @Or" & _
        "iginal_Nombre) AND (Observaciones = @Original_Observaciones) AND (PedidosMes = @" & _
        "Original_PedidosMes) AND (Plazo = @Original_Plazo) AND (Telefono1 = @Original_Te" & _
        "lefono1) AND (Telefono2 = @Original_Telefono2) AND (Telefono_Cont = @Original_Te" & _
        "lefono_Cont) AND (Utilidad_Fija = @Original_Utilidad_Fija) AND (Utilidad_Inventa" & _
        "rio = @Original_Utilidad_Inventario)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodigoProv", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoProv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contacto", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contacto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoTotal", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoTotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ImpIncluido", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ImpIncluido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoCredito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PedidosMes", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PedidosMes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Plazo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono_Cont", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_Cont", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Utilidad_Fija", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Utilidad_Fija", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Utilidad_Inventario", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Utilidad_Inventario", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO Proveedores(CodigoProv, Cedula, Nombre, Contacto, Telefono_Cont, Obse" & _
        "rvaciones, Telefono1, Telefono2, Fax1, Fax2, Email, Direccion, MontoCredito, Pla" & _
        "zo, CostoTotal, ImpIncluido, PedidosMes, Utilidad_Inventario, Utilidad_Fija) VAL" & _
        "UES (@CodigoProv, @Cedula, @Nombre, @Contacto, @Telefono_Cont, @Observaciones, @" & _
        "Telefono1, @Telefono2, @Fax1, @Fax2, @Email, @Direccion, @MontoCredito, @Plazo, " & _
        "@CostoTotal, @ImpIncluido, @PedidosMes, @Utilidad_Inventario, @Utilidad_Fija); S" & _
        "ELECT CodigoProv, Cedula, Nombre, Contacto, Telefono_Cont, Observaciones, Telefo" & _
        "no1, Telefono2, Fax1, Fax2, Email, Direccion, MontoCredito, Plazo, CostoTotal, I" & _
        "mpIncluido, PedidosMes, Utilidad_Inventario, Utilidad_Fija FROM Proveedores WHER" & _
        "E (CodigoProv = @CodigoProv)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection1
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodigoProv", System.Data.SqlDbType.Int, 4, "CodigoProv"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 20, "Cedula"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contacto", System.Data.SqlDbType.VarChar, 250, "Contacto"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono_Cont", System.Data.SqlDbType.VarChar, 15, "Telefono_Cont"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono1", System.Data.SqlDbType.VarChar, 15, "Telefono1"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono2", System.Data.SqlDbType.VarChar, 15, "Telefono2"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax1", System.Data.SqlDbType.VarChar, 15, "Fax1"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax2", System.Data.SqlDbType.VarChar, 15, "Fax2"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 100, "Email"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 250, "Direccion"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoCredito", System.Data.SqlDbType.Float, 8, "MontoCredito"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Plazo", System.Data.SqlDbType.Int, 4, "Plazo"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoTotal", System.Data.SqlDbType.Bit, 1, "CostoTotal"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ImpIncluido", System.Data.SqlDbType.Bit, 1, "ImpIncluido"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PedidosMes", System.Data.SqlDbType.Int, 4, "PedidosMes"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Utilidad_Inventario", System.Data.SqlDbType.Float, 8, "Utilidad_Inventario"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Utilidad_Fija", System.Data.SqlDbType.Bit, 1, "Utilidad_Fija"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT CodigoProv, Cedula, Nombre, Contacto, Telefono_Cont, Observaciones, Telefo" & _
        "no1, Telefono2, Fax1, Fax2, Email, Direccion, MontoCredito, Plazo, CostoTotal, I" & _
        "mpIncluido, PedidosMes, Utilidad_Inventario, Utilidad_Fija FROM Proveedores"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE Proveedores SET CodigoProv = @CodigoProv, Cedula = @Cedula, Nombre = @Nomb" & _
        "re, Contacto = @Contacto, Telefono_Cont = @Telefono_Cont, Observaciones = @Obser" & _
        "vaciones, Telefono1 = @Telefono1, Telefono2 = @Telefono2, Fax1 = @Fax1, Fax2 = @" & _
        "Fax2, Email = @Email, Direccion = @Direccion, MontoCredito = @MontoCredito, Plaz" & _
        "o = @Plazo, CostoTotal = @CostoTotal, ImpIncluido = @ImpIncluido, PedidosMes = @" & _
        "PedidosMes, Utilidad_Inventario = @Utilidad_Inventario, Utilidad_Fija = @Utilida" & _
        "d_Fija WHERE (CodigoProv = @Original_CodigoProv) AND (Cedula = @Original_Cedula)" & _
        " AND (Contacto = @Original_Contacto) AND (CostoTotal = @Original_CostoTotal) AND" & _
        " (Direccion = @Original_Direccion) AND (Email = @Original_Email) AND (Fax1 = @Or" & _
        "iginal_Fax1) AND (Fax2 = @Original_Fax2) AND (ImpIncluido = @Original_ImpIncluid" & _
        "o) AND (MontoCredito = @Original_MontoCredito) AND (Nombre = @Original_Nombre) A" & _
        "ND (Observaciones = @Original_Observaciones) AND (PedidosMes = @Original_Pedidos" & _
        "Mes) AND (Plazo = @Original_Plazo) AND (Telefono1 = @Original_Telefono1) AND (Te" & _
        "lefono2 = @Original_Telefono2) AND (Telefono_Cont = @Original_Telefono_Cont) AND" & _
        " (Utilidad_Fija = @Original_Utilidad_Fija) AND (Utilidad_Inventario = @Original_" & _
        "Utilidad_Inventario); SELECT CodigoProv, Cedula, Nombre, Contacto, Telefono_Cont" & _
        ", Observaciones, Telefono1, Telefono2, Fax1, Fax2, Email, Direccion, MontoCredit" & _
        "o, Plazo, CostoTotal, ImpIncluido, PedidosMes, Utilidad_Inventario, Utilidad_Fij" & _
        "a FROM Proveedores WHERE (CodigoProv = @CodigoProv)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodigoProv", System.Data.SqlDbType.Int, 4, "CodigoProv"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 20, "Cedula"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, "Nombre"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contacto", System.Data.SqlDbType.VarChar, 250, "Contacto"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono_Cont", System.Data.SqlDbType.VarChar, 15, "Telefono_Cont"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono1", System.Data.SqlDbType.VarChar, 15, "Telefono1"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Telefono2", System.Data.SqlDbType.VarChar, 15, "Telefono2"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax1", System.Data.SqlDbType.VarChar, 15, "Fax1"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fax2", System.Data.SqlDbType.VarChar, 15, "Fax2"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 100, "Email"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 250, "Direccion"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoCredito", System.Data.SqlDbType.Float, 8, "MontoCredito"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Plazo", System.Data.SqlDbType.Int, 4, "Plazo"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostoTotal", System.Data.SqlDbType.Bit, 1, "CostoTotal"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ImpIncluido", System.Data.SqlDbType.Bit, 1, "ImpIncluido"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PedidosMes", System.Data.SqlDbType.Int, 4, "PedidosMes"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Utilidad_Inventario", System.Data.SqlDbType.Float, 8, "Utilidad_Inventario"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Utilidad_Fija", System.Data.SqlDbType.Bit, 1, "Utilidad_Fija"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodigoProv", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodigoProv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contacto", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contacto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostoTotal", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostoTotal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Direccion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Direccion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fax2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fax2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ImpIncluido", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ImpIncluido", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MontoCredito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoCredito", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PedidosMes", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PedidosMes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Plazo", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Telefono_Cont", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_Cont", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Utilidad_Fija", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Utilidad_Fija", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Utilidad_Inventario", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Utilidad_Inventario", System.Data.DataRowVersion.Original, Nothing))
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(192, 91)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(520, 157)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox_Datos_Abonos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(512, 131)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos del Abono"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(512, 131)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Detalle  de la(s) Factura(s) a Abonar"
        '
        'AdapterBancos
        '
        Me.AdapterBancos.DeleteCommand = Me.SqlDeleteCommand5
        Me.AdapterBancos.InsertCommand = Me.SqlInsertCommand5
        Me.AdapterBancos.SelectCommand = Me.SqlSelectCommand6
        Me.AdapterBancos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Bancos", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo_banco", "Codigo_banco"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion")})})
        Me.AdapterBancos.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Bancos WHERE (Codigo_banco = @Original_Codigo_banco) AND (Descripcion" & _
        " = @Original_Descripcion)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection2
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo_banco", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo_banco", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection2
        '
        Me.SqlConnection2.ConnectionString = "workstation id=(local);packet size=4096;integrated security=SSPI;data source=""."";" & _
        "persist security info=False;initial catalog=Bancos"
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO Bancos(Descripcion) VALUES (@Descripcion); SELECT Codigo_banco, Descr" & _
        "ipcion FROM Bancos WHERE (Codigo_banco = @@IDENTITY)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection2
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 250, "Descripcion"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT Codigo_banco, Descripcion FROM Bancos"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection2
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE Bancos SET Descripcion = @Descripcion WHERE (Codigo_banco = @Original_Codi" & _
        "go_banco) AND (Descripcion = @Original_Descripcion); SELECT Codigo_banco, Descri" & _
        "pcion FROM Bancos WHERE (Codigo_banco = @Codigo_banco)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection2
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 250, "Descripcion"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo_banco", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo_banco", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo_banco", System.Data.SqlDbType.BigInt, 8, "Codigo_banco"))
        '
        'AdapterCuentasBancarias
        '
        Me.AdapterCuentasBancarias.DeleteCommand = Me.SqlDeleteCommand4
        Me.AdapterCuentasBancarias.InsertCommand = Me.SqlInsertCommand4
        Me.AdapterCuentasBancarias.SelectCommand = Me.SqlSelectCommand7
        Me.AdapterCuentasBancarias.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Cuentas_bancarias", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cuenta", "Cuenta"), New System.Data.Common.DataColumnMapping("Codigo_banco", "Codigo_banco"), New System.Data.Common.DataColumnMapping("tipoCuenta", "tipoCuenta"), New System.Data.Common.DataColumnMapping("NombreCuenta", "NombreCuenta"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Id_CuentaBancaria", "Id_CuentaBancaria")})})
        Me.AdapterCuentasBancarias.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Cuentas_bancarias WHERE (Id_CuentaBancaria = @Original_Id_CuentaBanca" & _
        "ria) AND (Cod_Moneda = @Original_Cod_Moneda) AND (Codigo_banco = @Original_Codig" & _
        "o_banco) AND (Cuenta = @Original_Cuenta) AND (NombreCuenta = @Original_NombreCue" & _
        "nta) AND (tipoCuenta = @Original_tipoCuenta)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection2
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_CuentaBancaria", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CuentaBancaria", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo_banco", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo_banco", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_tipoCuenta", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "tipoCuenta", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO Cuentas_bancarias(Cuenta, Codigo_banco, tipoCuenta, NombreCuenta, Cod" & _
        "_Moneda) VALUES (@Cuenta, @Codigo_banco, @tipoCuenta, @NombreCuenta, @Cod_Moneda" & _
        "); SELECT Cuenta, Codigo_banco, tipoCuenta, NombreCuenta, Cod_Moneda, Id_CuentaB" & _
        "ancaria FROM Cuentas_bancarias WHERE (Id_CuentaBancaria = @@IDENTITY)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection2
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta", System.Data.SqlDbType.VarChar, 75, "Cuenta"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo_banco", System.Data.SqlDbType.BigInt, 8, "Codigo_banco"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@tipoCuenta", System.Data.SqlDbType.VarChar, 20, "tipoCuenta"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"))
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT Cuenta, Codigo_banco, tipoCuenta, NombreCuenta, Cod_Moneda, Id_CuentaBanca" & _
        "ria FROM Cuentas_bancarias"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection2
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE Cuentas_bancarias SET Cuenta = @Cuenta, Codigo_banco = @Codigo_banco, tipo" & _
        "Cuenta = @tipoCuenta, NombreCuenta = @NombreCuenta, Cod_Moneda = @Cod_Moneda WHE" & _
        "RE (Id_CuentaBancaria = @Original_Id_CuentaBancaria) AND (Cod_Moneda = @Original" & _
        "_Cod_Moneda) AND (Codigo_banco = @Original_Codigo_banco) AND (Cuenta = @Original" & _
        "_Cuenta) AND (NombreCuenta = @Original_NombreCuenta) AND (tipoCuenta = @Original" & _
        "_tipoCuenta); SELECT Cuenta, Codigo_banco, tipoCuenta, NombreCuenta, Cod_Moneda," & _
        " Id_CuentaBancaria FROM Cuentas_bancarias WHERE (Id_CuentaBancaria = @Id_CuentaB" & _
        "ancaria)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection2
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta", System.Data.SqlDbType.VarChar, 75, "Cuenta"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Codigo_banco", System.Data.SqlDbType.BigInt, 8, "Codigo_banco"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@tipoCuenta", System.Data.SqlDbType.VarChar, 20, "tipoCuenta"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 4, "Cod_Moneda"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id_CuentaBancaria", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_CuentaBancaria", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cod_Moneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Moneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Codigo_banco", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo_banco", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_tipoCuenta", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "tipoCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id_CuentaBancaria", System.Data.SqlDbType.Int, 4, "Id_CuentaBancaria"))
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.DataSource = Me.DataSetAbonosProveedor
        Me.txtTipoCambio.DisplayMember = "Moneda.ValorCompra"
        Me.txtTipoCambio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtTipoCambio.Enabled = False
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.Blue
        Me.txtTipoCambio.Location = New System.Drawing.Point(720, 64)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(80, 21)
        Me.txtTipoCambio.TabIndex = 165
        Me.txtTipoCambio.ValueMember = "Moneda.ValorCompra"
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = "abonocpagar"
        Me.DataGrid1.DataSource = Me.DataSetAbonosProveedor
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(720, 96)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(424, 120)
        Me.DataGrid1.TabIndex = 192
        '
        'DataGrid2
        '
        Me.DataGrid2.DataMember = "abonocpagar.abonocpagardetalle_abonocpagar"
        Me.DataGrid2.DataSource = Me.DataSetAbonosProveedor
        Me.DataGrid2.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid2.Location = New System.Drawing.Point(720, 224)
        Me.DataGrid2.Name = "DataGrid2"
        Me.DataGrid2.Size = New System.Drawing.Size(424, 96)
        Me.DataGrid2.TabIndex = 193
        '
        'TextBoxMontoLetras
        '
        Me.TextBoxMontoLetras.Enabled = False
        Me.TextBoxMontoLetras.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxMontoLetras.ForeColor = System.Drawing.Color.Blue
        Me.TextBoxMontoLetras.Location = New System.Drawing.Point(10, 419)
        Me.TextBoxMontoLetras.Name = "TextBoxMontoLetras"
        Me.TextBoxMontoLetras.Size = New System.Drawing.Size(696, 12)
        Me.TextBoxMontoLetras.TabIndex = 194
        Me.TextBoxMontoLetras.Text = "Monto en letras"
        '
        'CheckBox2
        '
        Me.CheckBox2.Enabled = False
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.Red
        Me.CheckBox2.Location = New System.Drawing.Point(312, 472)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(104, 16)
        Me.CheckBox2.TabIndex = 195
        Me.CheckBox2.Text = "Anulado"
        '
        'cmdAsientos
        '
        Me.cmdAsientos.DeleteCommand = Me.SqlDeleteCommand7
        Me.cmdAsientos.InsertCommand = Me.SqlInsertCommand7
        Me.cmdAsientos.SelectCommand = Me.SqlSelectCommand8
        Me.cmdAsientos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "AsientosContables", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NumAsiento", "NumAsiento"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("IdNumDoc", "IdNumDoc"), New System.Data.Common.DataColumnMapping("NumDoc", "NumDoc"), New System.Data.Common.DataColumnMapping("Beneficiario", "Beneficiario"), New System.Data.Common.DataColumnMapping("TipoDoc", "TipoDoc"), New System.Data.Common.DataColumnMapping("Accion", "Accion"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("FechaEntrada", "FechaEntrada"), New System.Data.Common.DataColumnMapping("Mayorizado", "Mayorizado"), New System.Data.Common.DataColumnMapping("Periodo", "Periodo"), New System.Data.Common.DataColumnMapping("NumMayorizado", "NumMayorizado"), New System.Data.Common.DataColumnMapping("Modulo", "Modulo"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario"), New System.Data.Common.DataColumnMapping("TotalDebe", "TotalDebe"), New System.Data.Common.DataColumnMapping("TotalHaber", "TotalHaber"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("TipoCambio", "TipoCambio")})})
        Me.cmdAsientos.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = "DELETE FROM AsientosContables WHERE (NumAsiento = @Original_NumAsiento) AND (Acci" & _
        "on = @Original_Accion) AND (Anulado = @Original_Anulado) AND (Beneficiario = @Or" & _
        "iginal_Beneficiario) AND (CodMoneda = @Original_CodMoneda) AND (Fecha = @Origina" & _
        "l_Fecha) AND (FechaEntrada = @Original_FechaEntrada) AND (IdNumDoc = @Original_I" & _
        "dNumDoc) AND (Mayorizado = @Original_Mayorizado) AND (Modulo = @Original_Modulo)" & _
        " AND (NombreUsuario = @Original_NombreUsuario) AND (NumDoc = @Original_NumDoc) A" & _
        "ND (NumMayorizado = @Original_NumMayorizado) AND (Observaciones = @Original_Obse" & _
        "rvaciones) AND (Periodo = @Original_Periodo) AND (TipoCambio = @Original_TipoCam" & _
        "bio) AND (TipoDoc = @Original_TipoDoc) AND (TotalDebe = @Original_TotalDebe) AND" & _
        " (TotalHaber = @Original_TotalHaber)"
        Me.SqlDeleteCommand7.Connection = Me.conContabilidad
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Accion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Accion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Beneficiario", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Beneficiario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdNumDoc", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdNumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Mayorizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Modulo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Modulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Periodo", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periodo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDoc", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalDebe", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalDebe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalHaber", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalHaber", System.Data.DataRowVersion.Original, Nothing))
        '
        'conContabilidad
        '
        Me.conContabilidad.ConnectionString = "workstation id=""ROLANDO-PC"";packet size=4096;integrated security=SSPI;data source" & _
        "=""ROLANDO-PC\TMS"";persist security info=False;initial catalog=Contabilidad"
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO AsientosContables(NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, " & _
        "TipoDoc, Accion, Anulado, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modu" & _
        "lo, Observaciones, NombreUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio) " & _
        "VALUES (@NumAsiento, @Fecha, @IdNumDoc, @NumDoc, @Beneficiario, @TipoDoc, @Accio" & _
        "n, @Anulado, @FechaEntrada, @Mayorizado, @Periodo, @NumMayorizado, @Modulo, @Obs" & _
        "ervaciones, @NombreUsuario, @TotalDebe, @TotalHaber, @CodMoneda, @TipoCambio); S" & _
        "ELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDoc, Accion, Anulad" & _
        "o, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Observaciones, Nomb" & _
        "reUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM AsientosContables W" & _
        "HERE (NumAsiento = @NumAsiento)"
        Me.SqlInsertCommand7.Connection = Me.conContabilidad
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 4, "Fecha"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdNumDoc", System.Data.SqlDbType.BigInt, 8, "IdNumDoc"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumDoc", System.Data.SqlDbType.VarChar, 50, "NumDoc"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Beneficiario", System.Data.SqlDbType.VarChar, 250, "Beneficiario"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDoc", System.Data.SqlDbType.Int, 4, "TipoDoc"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 50, "Accion"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 4, "FechaEntrada"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Mayorizado", System.Data.SqlDbType.Bit, 1, "Mayorizado"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Periodo", System.Data.SqlDbType.VarChar, 8, "Periodo"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Modulo", System.Data.SqlDbType.VarChar, 50, "Modulo"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalDebe", System.Data.SqlDbType.Float, 8, "TotalDebe"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalHaber", System.Data.SqlDbType.Float, 8, "TotalHaber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDoc, Accion, Anulad" & _
        "o, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Observaciones, Nomb" & _
        "reUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM AsientosContables"
        Me.SqlSelectCommand8.Connection = Me.conContabilidad
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = "UPDATE AsientosContables SET NumAsiento = @NumAsiento, Fecha = @Fecha, IdNumDoc =" & _
        " @IdNumDoc, NumDoc = @NumDoc, Beneficiario = @Beneficiario, TipoDoc = @TipoDoc, " & _
        "Accion = @Accion, Anulado = @Anulado, FechaEntrada = @FechaEntrada, Mayorizado =" & _
        " @Mayorizado, Periodo = @Periodo, NumMayorizado = @NumMayorizado, Modulo = @Modu" & _
        "lo, Observaciones = @Observaciones, NombreUsuario = @NombreUsuario, TotalDebe = " & _
        "@TotalDebe, TotalHaber = @TotalHaber, CodMoneda = @CodMoneda, TipoCambio = @Tipo" & _
        "Cambio WHERE (NumAsiento = @Original_NumAsiento) AND (Accion = @Original_Accion)" & _
        " AND (Anulado = @Original_Anulado) AND (Beneficiario = @Original_Beneficiario) A" & _
        "ND (CodMoneda = @Original_CodMoneda) AND (Fecha = @Original_Fecha) AND (FechaEnt" & _
        "rada = @Original_FechaEntrada) AND (IdNumDoc = @Original_IdNumDoc) AND (Mayoriza" & _
        "do = @Original_Mayorizado) AND (Modulo = @Original_Modulo) AND (NombreUsuario = " & _
        "@Original_NombreUsuario) AND (NumDoc = @Original_NumDoc) AND (NumMayorizado = @O" & _
        "riginal_NumMayorizado) AND (Observaciones = @Original_Observaciones) AND (Period" & _
        "o = @Original_Periodo) AND (TipoCambio = @Original_TipoCambio) AND (TipoDoc = @O" & _
        "riginal_TipoDoc) AND (TotalDebe = @Original_TotalDebe) AND (TotalHaber = @Origin" & _
        "al_TotalHaber); SELECT NumAsiento, Fecha, IdNumDoc, NumDoc, Beneficiario, TipoDo" & _
        "c, Accion, Anulado, FechaEntrada, Mayorizado, Periodo, NumMayorizado, Modulo, Ob" & _
        "servaciones, NombreUsuario, TotalDebe, TotalHaber, CodMoneda, TipoCambio FROM As" & _
        "ientosContables WHERE (NumAsiento = @NumAsiento)"
        Me.SqlUpdateCommand7.Connection = Me.conContabilidad
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 4, "Fecha"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdNumDoc", System.Data.SqlDbType.BigInt, 8, "IdNumDoc"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumDoc", System.Data.SqlDbType.VarChar, 50, "NumDoc"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Beneficiario", System.Data.SqlDbType.VarChar, 250, "Beneficiario"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoDoc", System.Data.SqlDbType.Int, 4, "TipoDoc"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Accion", System.Data.SqlDbType.VarChar, 50, "Accion"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FechaEntrada", System.Data.SqlDbType.DateTime, 4, "FechaEntrada"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Mayorizado", System.Data.SqlDbType.Bit, 1, "Mayorizado"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Periodo", System.Data.SqlDbType.VarChar, 8, "Periodo"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Modulo", System.Data.SqlDbType.VarChar, 50, "Modulo"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 255, "Observaciones"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalDebe", System.Data.SqlDbType.Float, 8, "TotalDebe"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotalHaber", System.Data.SqlDbType.Float, 8, "TotalHaber"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TipoCambio", System.Data.SqlDbType.Float, 8, "TipoCambio"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Accion", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Accion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Beneficiario", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Beneficiario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FechaEntrada", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FechaEntrada", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IdNumDoc", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IdNumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Mayorizado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Modulo", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Modulo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreUsuario", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreUsuario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumDoc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumMayorizado", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(0, Byte), "NumMayorizado", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Periodo", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periodo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoCambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoCambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TipoDoc", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TipoDoc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalDebe", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalDebe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotalHaber", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotalHaber", System.Data.DataRowVersion.Original, Nothing))
        '
        'cmdDetalleAsientos
        '
        Me.cmdDetalleAsientos.DeleteCommand = Me.SqlDeleteCommand8
        Me.cmdDetalleAsientos.InsertCommand = Me.SqlInsertCommand8
        Me.cmdDetalleAsientos.SelectCommand = Me.SqlSelectCommand9
        Me.cmdDetalleAsientos.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "DetallesAsientosContable", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ID_Detalle", "ID_Detalle"), New System.Data.Common.DataColumnMapping("NumAsiento", "NumAsiento"), New System.Data.Common.DataColumnMapping("Cuenta", "Cuenta"), New System.Data.Common.DataColumnMapping("NombreCuenta", "NombreCuenta"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Debe", "Debe"), New System.Data.Common.DataColumnMapping("Haber", "Haber"), New System.Data.Common.DataColumnMapping("DescripcionAsiento", "DescripcionAsiento"), New System.Data.Common.DataColumnMapping("Tipocambio", "Tipocambio")})})
        Me.cmdDetalleAsientos.UpdateCommand = Me.SqlUpdateCommand8
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = "DELETE FROM DetallesAsientosContable WHERE (ID_Detalle = @Original_ID_Detalle) AN" & _
        "D (Cuenta = @Original_Cuenta) AND (Debe = @Original_Debe) AND (DescripcionAsient" & _
        "o = @Original_DescripcionAsiento) AND (Haber = @Original_Haber) AND (Monto = @Or" & _
        "iginal_Monto) AND (NombreCuenta = @Original_NombreCuenta) AND (NumAsiento = @Ori" & _
        "ginal_NumAsiento) AND (Tipocambio = @Original_Tipocambio OR @Original_Tipocambio" & _
        " IS NULL AND Tipocambio IS NULL)"
        Me.SqlDeleteCommand8.Connection = Me.conContabilidad
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID_Detalle", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID_Detalle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Debe", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Debe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Haber", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Haber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipocambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipocambio", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = "INSERT INTO DetallesAsientosContable(NumAsiento, Cuenta, NombreCuenta, Monto, Deb" & _
        "e, Haber, DescripcionAsiento, Tipocambio) VALUES (@NumAsiento, @Cuenta, @NombreC" & _
        "uenta, @Monto, @Debe, @Haber, @DescripcionAsiento, @Tipocambio); SELECT ID_Detal" & _
        "le, NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Ti" & _
        "pocambio FROM DetallesAsientosContable WHERE (ID_Detalle = @@IDENTITY)"
        Me.SqlInsertCommand8.Connection = Me.conContabilidad
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta", System.Data.SqlDbType.VarChar, 255, "Cuenta"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Debe", System.Data.SqlDbType.Bit, 1, "Debe"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Haber", System.Data.SqlDbType.Bit, 1, "Haber"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, "DescripcionAsiento"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipocambio", System.Data.SqlDbType.Float, 8, "Tipocambio"))
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT ID_Detalle, NumAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, Descripc" & _
        "ionAsiento, Tipocambio FROM DetallesAsientosContable"
        Me.SqlSelectCommand9.Connection = Me.conContabilidad
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = "UPDATE DetallesAsientosContable SET NumAsiento = @NumAsiento, Cuenta = @Cuenta, N" & _
        "ombreCuenta = @NombreCuenta, Monto = @Monto, Debe = @Debe, Haber = @Haber, Descr" & _
        "ipcionAsiento = @DescripcionAsiento, Tipocambio = @Tipocambio WHERE (ID_Detalle " & _
        "= @Original_ID_Detalle) AND (Cuenta = @Original_Cuenta) AND (Debe = @Original_De" & _
        "be) AND (DescripcionAsiento = @Original_DescripcionAsiento) AND (Haber = @Origin" & _
        "al_Haber) AND (Monto = @Original_Monto) AND (NombreCuenta = @Original_NombreCuen" & _
        "ta) AND (NumAsiento = @Original_NumAsiento) AND (Tipocambio = @Original_Tipocamb" & _
        "io OR @Original_Tipocambio IS NULL AND Tipocambio IS NULL); SELECT ID_Detalle, N" & _
        "umAsiento, Cuenta, NombreCuenta, Monto, Debe, Haber, DescripcionAsiento, Tipocam" & _
        "bio FROM DetallesAsientosContable WHERE (ID_Detalle = @ID_Detalle)"
        Me.SqlUpdateCommand8.Connection = Me.conContabilidad
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumAsiento", System.Data.SqlDbType.VarChar, 15, "NumAsiento"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cuenta", System.Data.SqlDbType.VarChar, 255, "Cuenta"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreCuenta", System.Data.SqlDbType.VarChar, 250, "NombreCuenta"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 8, "Monto"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Debe", System.Data.SqlDbType.Bit, 1, "Debe"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Haber", System.Data.SqlDbType.Bit, 1, "Haber"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, "DescripcionAsiento"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tipocambio", System.Data.SqlDbType.Float, 8, "Tipocambio"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID_Detalle", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID_Detalle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cuenta", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Debe", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Debe", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DescripcionAsiento", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DescripcionAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Haber", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Haber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NombreCuenta", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NombreCuenta", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumAsiento", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumAsiento", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tipocambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipocambio", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID_Detalle", System.Data.SqlDbType.BigInt, 8, "ID_Detalle"))
        '
        'Abonos_Proveedor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(712, 490)
        Me.Controls.Add(Me.CheckBox2)
        Me.Controls.Add(Me.TextBoxMontoLetras)
        Me.Controls.Add(Me.DataGrid2)
        Me.Controls.Add(Me.DataGrid1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GridControl2)
        Me.Controls.Add(Me.GroupBox_Datos_Cliente)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Check_Dig_Recibo)
        Me.Controls.Add(Me.txtNum_Recibo)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.gridFacturas)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.txtSaldoActGen)
        Me.Controls.Add(Me.txtAbonoGen)
        Me.Controls.Add(Me.txtSaldoAntGen)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label16)
        Me.MaximumSize = New System.Drawing.Size(800, 600)
        Me.Name = "Abonos_Proveedor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Abono a Proveedor"
        Me.GroupBox_Datos_Cliente.ResumeLayout(False)
        CType(Me.dtFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetAbonosProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.gridFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.TxtAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_Datos_Abonos.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmReciboDinero_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            conContabilidad.ConnectionString = GetSetting("SeeSOFT", "Contabilidad", "Conexion")
            SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
            Me.SqlConnection2.ConnectionString = GetSetting("SeeSOFT", "Bancos", "Conexion")
            strModulo = IIf(NuevaConexion = "", "SeePOS", "SeePos")

            Me.DataSetAbonosProveedor.abonocpagar.Id_AbonocpagarColumn.AutoIncrement = True
            Me.DataSetAbonosProveedor.abonocpagar.Id_AbonocpagarColumn.AutoIncrementSeed = -1
            Me.DataSetAbonosProveedor.abonocpagar.Id_AbonocpagarColumn.AutoIncrementStep = -1
            Me.DataSetAbonosProveedor.abonocpagar.CuentaDestinoColumn.DefaultValue = 0
            Me.DataSetAbonosProveedor.abonocpagar.FechaEntradaColumn.DefaultValue = Now
            Me.DataSetAbonosProveedor.abonocpagar.FechaColumn.DefaultValue = Now
            Me.DataSetAbonosProveedor.abonocpagar.cod_MonedaColumn.DefaultValue = 1

            Me.DataSetAbonosProveedor.detalle_abonocpagar.Id_Detalle_abonocpagarColumn.AutoIncrement = True
            Me.DataSetAbonosProveedor.detalle_abonocpagar.Id_Detalle_abonocpagarColumn.AutoIncrementSeed = -1
            Me.DataSetAbonosProveedor.detalle_abonocpagar.Id_Detalle_abonocpagarColumn.AutoIncrementStep = -1

            'CARGA DE NUEVO los adaptadores.
            Me.daMoneda.Fill(Me.DataSetAbonosProveedor, "Moneda")
            If (Me.AdapterBancos.Fill(Me.DataSetAbonosProveedor, "Bancos")) = 0 Then
                MsgBox("No se pueden Realizar abonos porque no hay Bancos Registrados, el m�dulo se cerrar�", MsgBoxStyle.Critical)
                Me.Close()
            Else
                Me.ComboBoxBanco.SelectedIndex = 0

            End If

            Me.AdapterCuentasBancarias.Fill(Me.DataSetAbonosProveedor, "Cuentas_bancarias")

            Me.ToolBar1.Buttons(1).Enabled = True
            Me.txtUsuario.Focus()
            Me.ToolBarImprimir.Visible = False

            Me.ComboBoxCuentaBancaria.SelectedIndex = 0

            Binding()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub


    Function IdAbono()
        Dim Cx As New Conexion

        'Dim Id_Cuenta As Integer = ComboBox1.SelectedValue
        idabono1 = Cx.SlqExecuteScalar(Cx.Conectar(strModulo), "SELECT ISNULL(MAX(Id_Abonocpagar), 0) + 1  FROM abonocpagar")
        Cx.DesConectar(Cx.sQlconexion)
        'Me.Label3.Text = idabono1
        Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Current("Id_Abonocpagar") = idabono1
        'Me.TxtNumCheque.EditValue = NumeroCheque
    End Function

    Sub defaulvalue()
        Me.DataSetAbonosProveedor.abonocpagar.AnuladoColumn.DefaultValue = False
        Me.DataSetAbonosProveedor.abonocpagar.Cedula_UsuarioColumn.DefaultValue = ""
        Me.DataSetAbonosProveedor.abonocpagar.cod_MonedaColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.abonocpagar.Cod_ProveedorColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.abonocpagar.Codigo_bancoColumn.DefaultValue = Label_CodBanco.Text
        Me.DataSetAbonosProveedor.abonocpagar.ContabilizadoColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.abonocpagar.CuentaBancariaColumn.DefaultValue = ""
        Me.DataSetAbonosProveedor.abonocpagar.DocumentoColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.abonocpagar.FechaColumn.DefaultValue = Now
        Me.DataSetAbonosProveedor.abonocpagar.MontoColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.abonocpagar.ReciboColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.abonocpagar.Saldo_ActualColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.abonocpagar.Saldo_CuentaColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.abonocpagar.TipoCambioColumn.DefaultValue = 1
        Me.DataSetAbonosProveedor.abonocpagar.TipoDocumentoColumn.DefaultValue = "CHEQUE"
        Me.DataSetAbonosProveedor.abonocpagar.MontoLetrasColumn.DefaultValue = ""


        Me.DataSetAbonosProveedor.detalle_abonocpagar.FacturaColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.detalle_abonocpagar.Cod_ProveedorColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.detalle_abonocpagar.MontoFacturaColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.detalle_abonocpagar.Saldo_AntColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.detalle_abonocpagar.AbonoColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.detalle_abonocpagar.Abono_SuMonedaColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.detalle_abonocpagar.Saldo_ActualColumn.DefaultValue = 0
        Me.DataSetAbonosProveedor.detalle_abonocpagar.Id_CompraColumn.DefaultValue = 0

        Me.LabelFecha.Text = Date.Now
        Me.txtTipoCambio.Text = 1
        Me.LabelFecha.Text = Date.Now

    End Sub


#Region "Validacion Usuario"

    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown

        Try


            Dim cConexion As New Conexion
            Dim Conx As New SqlConnection
            Conx.ConnectionString = Me.SqlConnection1.ConnectionString
            Conx.Open()
            Dim rs As SqlDataReader

            If e.KeyCode = Keys.Enter Then
                If txtUsuario.Text <> "" Then

                    rs = cConexion.GetRecorset(Conx, "SELECT Cedula, Nombre from Usuarios where Clave_Interna ='" & txtUsuario.Text & "'")

                    If rs.HasRows = False Then
                        MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
                        txtUsuario.Focus()
                    End If

                    While rs.Read
                        Try
                            Me.defaulvalue()
                            Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()
                            Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").AddNew()

                            txtNombreUsuario.Text = rs!Nombre
                            txtCedulaUsuario.Text = rs("Cedula")
                            'If rs("AnuRecibos") = 1 Then Anular = True Else Anular = False

                            'If rs("VariarIntereses") = True Then
                            '    VariarInteres = True
                            'Else
                            '    VariarInteres = False
                            'End If
                            txtUsuario.Enabled = False ' se inabilita el campo de la contrase�a
                            Me.ToolBar1.Buttons(0).Enabled = True
                            Me.ToolBar1.Buttons(1).Enabled = True
                            Me.ToolBar1.Buttons(2).Enabled = True
                            Me.NuevoRecibo()
                            Me.txtCodigo.Focus()
                            '    ComboMoneda.Focus()
                        Catch eEndEdit As System.Data.NoNullAllowedException
                            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                        End Try
                    End While
                    rs.Close()
                    cConexion.DesConectar(cConexion.sQlconexion)
                Else
                    MsgBox("Debe de digitar la clave de usuario", MsgBoxStyle.Exclamation)
                    txtUsuario.Focus()
                End If



            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
#End Region


    Private Function Numero_de_Recibo() As Double
        Try
            Dim cConexion As New Conexion
            Dim Num_Recibo As Double
            Dim Conx As New SqlConnection
            Conx.ConnectionString = Me.SqlConnection1.ConnectionString
            Conx.Open()
            Num_Recibo = CDbl(cConexion.SlqExecuteScalar(Conx, "SELECT isnull(Max(CAST(Recibo AS BIGINT)),0) FROM abonocpagar"))
            Numero_de_Recibo = Num_Recibo + 1
            cConexion.DesConectar(cConexion.sQlconexion)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Function

    Private Sub NuevoRecibo()
        Try

            If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then 'no hay un registro pendiente
                Me.ToolBar1.Buttons(0).Text = "Cancelar"
                Me.ToolBar1.Buttons(0).ImageIndex = 8
                Me.ToolBar1.Buttons(1).Enabled = False
                Me.ToolBar1.Buttons(3).Enabled = False
                Me.GroupBox_Datos_Cliente.Enabled = False

                If Me.txtUsuario.Text = "" Then
                    txtUsuario.Enabled = True
                    txtUsuario.Focus()
                End If
                txtCodigo.Text = "" : txtNombre.Text = ""
                Try
                    Me.ComboMoneda.Enabled = True
                    txtNum_Recibo.Text = Numero_de_Recibo()
                    Me.GroupBox_Datos_Cliente.Enabled = True
                Catch eEndEdit As System.Data.NoNullAllowedException
                    System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                End Try
            Else
                Try
                    If MessageBox.Show("Desea Guardar los Cambios del Recibo de Dinero", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                        Me.Registrar()
                    Else
                        Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").CancelCurrentEdit()
                        Me.ToolBar1.Buttons(0).Text = "Nuevo"
                        Me.ToolBar1.Buttons(0).ImageIndex = 0
                        Me.ToolBar1.Buttons(1).Enabled = True
                        Me.txtUsuario.Text = ""
                        Me.txtNombreUsuario.Text = ""
                        Me.txtCedulaUsuario.Text = ""
                        Me.GroupBox_Datos_Cliente.Enabled = False
                        Me.txtObservaciones.Enabled = False
                        'txtIntereses.Enabled = False
                        TxtAbono.Enabled = False
                    End If
                Catch eEndEdit As System.Data.NoNullAllowedException
                    System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                End Try

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
    Private Sub txtUsuario_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUsuario.GotFocus
        txtUsuario.SelectAll()
    End Sub
    Dim fec As Date
    Function Registrar()
        Dim Trans As SqlTransaction
        Dim cx As New Conexion
        Dim i As Integer
        Dim Funciones As New Conexion
        Dim FactTemp As Double
        Dim numero As Integer
        Dim num_cuenta As String
        Dim documento, Tipo As String
        Try
            'VALIDA EL PERIODO DE TRABAJO
            fec = Me.dtFecha.EditValue
            Dim fx As New cFunciones
            Dim periodo As String = fx.BuscaPeriodo(fec)
            If periodo Is Nothing Then
                MsgBox("El periodo de trabajo no existe o esta cerrado", MsgBoxStyle.OKOnly)
                Exit Function
            Else
                If fx.ValidarPeriodo(fec) Then
                    MsgBox("El periodo de trabajo no existe o esta cerrado", MsgBoxStyle.OKOnly)
                    Exit Function
                End If
            End If

            If MessageBox.Show("�Desea guardar el pago al proveedor?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").CancelCurrentEdit()
                documento = Me.TextBox1.Text
                num_cuenta = Me.ComboBoxCuentaBancaria.SelectedValue
                numero = cx.SlqExecuteScalar(cx.Conectar("Bancos"), "Select Num_Cheque, Id_CuentaBancaria FROM Cheques where Num_Cheque = " & documento & " AND Id_CuentaBancaria=" & num_cuenta)

                If numero Then
                    MsgBox("El N�mero de Cheque ya esta ingresado, favor revisar", MsgBoxStyle.Information, "Atenci�n ...")
                    Exit Function
                End If

                If VerificarExistenciaDocumento(Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("Documento"), Me.ComboBoxTipoPago.Text, Me.ComboBoxCuentaBancaria.Text) = True Then
                    Me.TextBox1.Text = Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("Documento")
                    MsgBox("No se puede registrar este abono por que ya ha sigo registrado.", MsgBoxStyle.Information, "Atenci�n..")
                    Exit Function
                End If

                Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("CuentaBancaria") = Me.ComboBoxCuentaBancaria.Text
                Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("Fecha") = fec
                Tipo = Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("TipoDocumento")
                Me.BindingContext(DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()

                Me.BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").EndCurrentEdit()

                'Empieza a Guardar Datos
                '*****************************************************************************************************************
                Try
                    Dim sql As String = ""
                    If Me.SqlConnection1.State = ConnectionState.Closed Then Me.SqlConnection1.Open()
                    Trans = Me.SqlConnection1.BeginTransaction
                    For i = 0 To Me.BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Count - 1
                        Me.BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Position = i
                        If Me.BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Current("Saldo_Actual") = 0 Then
                            sql &= " Update Compras set FacturaCancelado = 1 where Id_Compra = " & Me.BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Current("Id_Compra")
                        End If
                    Next i
                    Me.adAbonos.InsertCommand.Transaction = Trans
                    Me.adAbonos.UpdateCommand.Transaction = Trans
                    Me.adAbonos.Update(Me.DataSetAbonosProveedor, "abonocpagar") 'guarda abono
                    Dim idAbono As Integer = Me.DataSetAbonosProveedor.abonocpagar(0).Id_Abonocpagar
                    For j As Integer = 0 To Me.DataSetAbonosProveedor.detalle_abonocpagar.Count - 1
                        With Me.DataSetAbonosProveedor.detalle_abonocpagar(j)
                            sql &= " INSERT INTO [SeePOS].[dbo].[detalle_abonocpagar] " & _
                            " ([Factura]  ,[Cod_Proveedor]  ,[MontoFactura]  ,[Saldo_Ant]  ,[Abono]  ,[Abono_SuMoneda]  ,[Saldo_Actual]  ,[Id_Abonocpagar]  ,[Id_Compra], Aplicado)  " & _
                            "VALUES  (" & .Factura & "," & .Cod_Proveedor & "," & .MontoFactura & "," & .Saldo_Ant & "," & .Abono & "," & .Abono_SuMoneda & "," & .Saldo_Actual & "  ," & .Id_Abonocpagar & " ," & .Id_Compra & ",  " & .Aplicado & ")"
                        End With
                    Next
                    For j As Integer = 0 To frm.DataSetAbonosProveedor1.AplicarDevolucion.Count - 1
                        With frm.DataSetAbonosProveedor1.AplicarDevolucion(j)
                            sql &= " UPDATE AjusteInventario SET Aplicado =  Aplicado + " & fnDevuelveMontoAplicado(.Id_Ajuste) & " WHERE Consecutivo = " & .Id_Ajuste
                        End With
                    Next
                    For c As Integer = 0 To Me.DataSetAbonosProveedor.NotaAplicadaAbono.Count - 1
                        sql &= " INSERT INTO [SeePOS].[dbo].[NotaAplicadaAbono]  ([ID_Abonocpagar]  ,[ID_Ajustecinventario]  ,[MontoAplicado])  VALUES  (" & idAbono & "  ," & Me.DataSetAbonosProveedor.NotaAplicadaAbono(c).IdAjusteInv & "  ," & Me.DataSetAbonosProveedor.NotaAplicadaAbono(c).MontoApli & ")"
                    Next
                    Dim cmd As New SqlCommand(sql, Me.SqlConnection1, Trans)
                    cmd.ExecuteNonQuery()
                    Trans.Commit()
                Catch ex As Exception
                    Trans.Rollback()
                    MsgBox("Error :" & ex.Message & vbCrLf _
                    & "-producido durante el proceso de guardar datos. " & vbCrLf _
                    & "ck: " & Me.TextBox1.Text & ", Cta: " & Me.ComboBoxCuentaBancaria.Text, MsgBoxStyle.Exclamation, Text)
                    Exit Function
                End Try
                'Termina de Guardar Datos
                '*****************************************************************************************************************

                MsgBox("Datos Guardados Satisfactoriamente", MsgBoxStyle.Information)
                numero = cx.SlqExecuteScalar(cx.Conectar("Bancos"), "Select Id_Cheque FROM Cheques where Num_Cheque = " & documento & " AND Id_CuentaBancaria=" & num_cuenta)
                Me.GuardaAsiento(numero)
                If Tipo = "CHEQUE" Then
                    If MessageBox.Show("�Desea imprimir el cheque?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                        imprimir(numero)
                    End If
                End If

                Me.DataSetAbonosProveedor.abonocpagar.AcceptChanges()
                Me.DataSetAbonosProveedor.detalle_abonocpagar.Clear()
                Me.DataSetAbonosProveedor.abonocpagar.Clear()
                Me.ToolBar1.Buttons(1).Enabled = True
                Me.ToolBar1.Buttons(0).Text = "Nuevo"
                Me.ToolBar1.Buttons(0).ImageIndex = 0
                Dim ConexionProve As String
                ConexionProve = GetSetting("SeeSOFT", "SeePos", "Conexion")
                Dim abono As New Abonos_Proveedor(usua, ConexionProve)
                abono.MdiParent = Me.MdiParent
                abono.Show()
                Me.Close()

            Else
                Exit Function
            End If
        Catch ex As System.Exception

            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Function
    Function fnDevuelveMontoAplicado(ByVal _IdAjuste As Integer) As Double
        Dim montoApli As Double = 0
        For i As Integer = 0 To Me.DataSetAbonosProveedor.NotaAplicadaAbono.Count - 1

            If Me.DataSetAbonosProveedor.NotaAplicadaAbono(i).IdAjusteInv = _IdAjuste Then
                montoApli += Me.DataSetAbonosProveedor.NotaAplicadaAbono(i).MontoApli
            End If

        Next
        Return montoApli

    End Function
    Private Function VerificarExistenciaDocumento(ByVal Documento As String, ByVal TipoDoc As String, ByVal Cuenta As String) As Boolean
        Dim Cx As New Conexion
        Dim Status As String
        Cx.Conectar("SeePos")
        Status = Cx.SlqExecuteScalar(Cx.sQlconexion, "SELECT Id_Abonocpagar FROM abonocpagar WHERE (Documento = " & Documento & ") AND (TipoDocumento = '" & TipoDoc & "') AND (CuentaBancaria = '" & Cuenta & "')")
        Cx.DesConectar(Cx.sQlconexion)

        If Status <> "" Then Return True Else Return False
    End Function

    Private Sub BuscarRecibo()
        Dim Fx As New cFunciones
        Dim identificador As Double

        Try
            If Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Count > 0 Then
                If (MsgBox("Actualmente se est� realizando un Recibo de Dinero, si contin�a se perderan los datos del Recibo actual, �desea continuar?", MsgBoxStyle.YesNo)) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Me.ToolBar1.Buttons(0).Text = "Nuevo"
            Me.ToolBar1.Buttons(0).ImageIndex = 0
            Me.DataSetAbonosProveedor.detalle_abonocpagar.Clear()
            Me.DataSetAbonosProveedor.abonocpagar.Clear()
            identificador = CDbl(Fx.Buscar_X_Descripcion_Fecha("SELECT abonocpagar.Id_Abonocpagar, cast(abonocpagar.Documento as varchar) as Documento, ISNULL(Proveedores.Nombre, 'Proveedor Eliminado') AS Proveedor, abonocpagar.Fecha FROM abonocpagar LEFT OUTER JOIN Proveedores ON abonocpagar.Cod_Proveedor = Proveedores.CodigoProv ORDER BY abonocpagar.Fecha DESC", "Proveedor", "Fecha", "Buscar Pagos a Proveedor", SqlConnection1.ConnectionString))
            buscando = True
            If identificador = 0.0 Then ' si se dio en el boton de cancelar
                Me.buscando = False
                Exit Sub
            End If

            Me.CargarInformacionProveedor(identificador)
            ' si esta venta aun no ha sido anulada
            'If Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("Anula") = False Then Me.ToolBar1.Buttons(3).Enabled = True
            Me.GridControl2.Enabled = False
            Me.ToolBar1.Buttons(4).Enabled = True
            Me.ToolBar1.Buttons(3).Enabled = True
            Me.ToolBar1.Buttons(0).Enabled = True

        Catch ex As SystemException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
    Function CargarInformacionProveedor(ByVal Id As Double)
        'Dim cnnv As SqlConnection = Nothing
        'Dim dt As New DataTable
        'Dim cConexion As New Conexion
        'Dim IdRec As Long
        'Dentro de un Try/Catch por si se produce un error
        Try


            Dim Fx As New cFunciones
            Me.DataSetAbonosProveedor.detalle_abonocpagar.Clear()
            Me.DataSetAbonosProveedor.abonocpagar.Clear()
            Me.DataSetAbonosProveedor.Cuentas_Bancarias_Proveedor.Clear()
            Me.DataSetAbonosProveedor.Proveedores.Clear()
            'Me.DataSetAbonosProveedor.Moneda.Clear()
            'Me.DataSetAbonosProveedor.Cuentas_bancarias.Clear()

            'Me.DataSetAbonosProveedor.Bancos.Clear()




            'CARGA EL ABONO SELECCIONADO
            Fx.Cargar_Tabla_Generico(Me.adAbonos, "Select * from abonocpagar where Id_Abonocpagar =" & Id, SqlConnection1.ConnectionString)
            Me.adAbonos.Fill(Me.DataSetAbonosProveedor.abonocpagar)

            Dim ii As Integer
            For ii = 0 To Me.DataSetAbonosProveedor.abonocpagar.Count - 1
                If Me.BindingContext(Me.DataSetAbonosProveedor.abonocpagar).Current("Anulado") = True Then
                    Me.CheckBox2.Checked = True
                End If
            Next

            Fx.Cargar_Tabla_Generico(Me.daDetalle_Abono, "SELECT * FROM detalle_abonocpagar WHERE Id_Abonocpagar = " & Id, SqlConnection1.ConnectionString)
            Me.daDetalle_Abono.Fill(Me.DataSetAbonosProveedor, "detalle_abonocpagar")

            Fx.Cargar_Tabla_Generico(Me.Adapter_Proveedor, "SELECT * from proveedores where codigoProv  =" & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Cod_Proveedor"), SqlConnection1.ConnectionString)
            Me.Adapter_Proveedor.Fill(Me.DataSetAbonosProveedor, "Proveedores")
            txtNombre.Text = BindingContext(DataSetAbonosProveedor, "Proveedores").Current("Nombre")

            Fx.Cargar_Tabla_Generico(Me.Adapter_Cuenta_Proveedor, "SELECT * FROM Cuentas_Bancarias_Proveedor WHERE CodigoProv = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Cod_Proveedor"), SqlConnection1.ConnectionString)
            Me.Adapter_Cuenta_Proveedor.Fill(Me.DataSetAbonosProveedor, "Cuentas_Bancarias_Proveedor")
            ComboBoxCuentaBancariaDestino.SelectedValue = BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("CuentaDestino")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try

    End Function

    'Anulaciones de abonos a proveedor 
    Function Registrar_Anulacion_Venta() As Boolean
        Dim IdCuentaBancaria As Integer
        Dim i As Long
        Dim Facttem As Double
        Dim Funciones As New Conexion

        IdCuentaBancaria = Funciones.SlqExecuteScalar(Funciones.Conectar("Bancos"), "Select Id_CuentaBancaria from Cuentas_bancarias where cuenta = '" & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("CuentaBancaria") & "'")

        'If Me.SqlConnection1.State <> Me.SqlConnection1.State.Open Then Me.SqlConnection1.Open()
        'Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
        Dim sql As String = ""
        Try
            If MsgBox("Desea mantener habilitado el consecutivo del cheque", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                'Funciones.UpdateRecords("Cheques", "Anulado = 1, Observaciones = Observaciones  + ' <Anulado: " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Documento") & ">'", "Num_Cheque = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Documento") & " and Id_CuentaBancaria =" & IdCuentaBancaria, "Bancos")
                sql += " Update Bancos.dbo.Cheques set Anulado = 1, Observaciones = Observaciones + ' <Anulado: " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Documento") & ">' Where Num_Cheque = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Documento") & " and Id_CuentaBancaria =" & IdCuentaBancaria & " "
                'Funciones.UpdateRecords("abonocpagar", "Anulado = 1", "Id_Abonocpagar = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Id_Abonocpagar"), "SeePOS")
                sql += " Update SeePos.dbo.Abonocpagar set Anulado = 1 Where Id_Abonocpagar = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Id_Abonocpagar") & " "
            Else
                'Funciones.UpdateRecords("Cheques", "Anulado = 1, Num_Cheque = 0, Observaciones = Observaciones  + ' <Anulado: " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Documento") & ">'", "Num_Cheque = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Documento") & " and Id_CuentaBancaria =" & IdCuentaBancaria, "Bancos")
                sql += " Update Bancos.dbo.Cheques set Anulado = 1, Num_Cheque = 0, Observaciones = Observaciones + ' <Anulado: " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Documento") & ">' Where Num_Cheque = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Documento") & " and Id_CuentaBancaria =" & IdCuentaBancaria & " "
                'Funciones.UpdateRecords("abonocpagar", "Anulado = 1, Documento = Documento * -1", "Id_Abonocpagar = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Id_Abonocpagar"), "SeePOS")
                sql += " Update SeePos.dbo.Abonocpagar set Anulado = 1, Documento = Documento * -1 Where Id_Abonocpagar = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Id_Abonocpagar") & " "
            End If
            'Funciones.UpdateRecords("abonocpagar", "Anulado = 1, Documento = Documento * -1", "Id_Abonocpagar = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Id_Abonocpagar"), "SeePOS")
            'sql += " Update SeePos.dbo.Abonocpagar set Anulado = 1, Documento = Documento * -1 Where Id_Abonocpagar = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Id_Abonocpagar") & " "

            If Not (Me.asiento.Equals("") Or Me.asiento.Equals("0")) Then 'ANULA EL ASIENTO
                'Funciones.UpdateRecords("AsientosContables", "Anulado = 1", "NumAsiento = '" & asiento & "'", "Contabilidad")
                sql += " Update Contabilidad.dbo.AsientosContables set Anulado = 1, Mayorizado = 0 Where NumAsiento = '" & asiento & "' "
            End If

            For i = 0 To Me.BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Count - 1
                Me.BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Position = i
                Facttem = Me.BindingContext(DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Current("Factura")
                'Funciones.UpdateRecords("Compras", "FacturaCancelado = 0", "Factura =" & Facttem & "and TipoCompra = 'CRE'", strModulo)
                sql += " UPDATE SeePOs.dbo.compras SET FacturaCancelado = 0 WHERE Factura = " & Facttem & " AND TipoCompra = 'CRE' "
            Next i

            sql += " UPDATE AjusteInventario SET Aplicado -= NotaAplicadaAbono.MontoAplicado FROM NotaAplicadaAbono INNER JOIN  AjusteInventario ON NotaAplicadaAbono.ID_Ajustecinventario = AjusteInventario.Consecutivo WHERE (NotaAplicadaAbono.ID_Abonocpagar = " & BindingContext(DataSetAbonosProveedor, "abonocpagar").Current("Id_Abonocpagar") & ") "

            'Dim msg As String = ""
            Dim Trans As SqlTransaction
            Dim cmd As SqlCommand
            Dim cnc As New Conexion

            Try
                cnc.Conectar()
                Trans = cnc.sQlconexion.BeginTransaction
                cmd = New SqlCommand(sql, cnc.sQlconexion, Trans)
                cmd.ExecuteNonQuery()
                Trans.Commit()
                cnc.sQlconexion.Close()
            Catch ex As Exception
                Trans.Rollback()
                cnc.sQlconexion.Close()
                MsgBox(ex.Message, MsgBoxStyle.Critical, Text)
                Return False
            End Try

            'msg = cnc.SlqExecute(cnc.sQlconexion, sql)
            'If Not msg Is Nothing Then
            'MsgBox(msg, MsgBoxStyle.Critical)
            'Return False
            'End If

            'Trans.Commit()
            Return True
        Catch ex As Exception
            ' Trans.Rollback()
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            Me.ToolBar1.Buttons(3).Enabled = True
            Return False
        End Try
    End Function

    Dim asiento As String = ""
    Function AnularRecibo()
        Try
            fec = Me.dtFecha.EditValue
            Dim fx As New cFunciones
            Dim periodo As String = fx.BuscaPeriodo(fec)
            If periodo Is Nothing Then
                MsgBox("El periodo de trabajo no existe o esta cerrado", MsgBoxStyle.OKOnly)
                Exit Function
            Else
                If fx.ValidarPeriodo(fec) Then
                    MsgBox("El periodo de trabajo no existe o esta cerrado", MsgBoxStyle.OKOnly)
                    Exit Function
                End If
            End If


            Dim resp As Integer
            If Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Count > 0 Then
                If Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Count > 0 Then
                    'VALIDA ASIENTO SI TIENE
                    asiento = ""
                    Dim dtChe As New DataTable
                    cFunciones.Llenar_Tabla_Generico("Select Asiento, Contabilizado From Cheques WHERE (Num_Cheque = '" & Me.TextBox1.Text & "') AND (Id_CuentaBancaria = " & Me.ComboBoxCuentaBancaria.SelectedValue & ")", dtChe, GetSetting("SeeSoft", "Bancos", "Conexion"))
                    If dtChe.Rows.Count > 0 Then
                        If dtChe.Rows(0).Item("Contabilizado") Then
                            asiento = dtChe.Rows(0).Item("Asiento")
                        Else
                            asiento = 0
                        End If

                    End If

                    If Not asiento.Equals("0") Then ' REVISA SI ESTA MAYORIZADO
                        Dim dt As New DataTable
                        cFunciones.Llenar_Tabla_Generico("Select Mayorizado From AsientosContables WHERE NumAsiento = '" & asiento & "'", dt, GetSetting("SeeSoft", "Contabilidad", "Conexion"))
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0).Item(0) Then
                                MsgBox("El asiento # " & asiento & " que corresponde a este abono ya esta mayorizado, NO se puede anular", MsgBoxStyle.OKOnly)
                                Exit Function
                            End If
                        End If
                    End If


                    If CheckBox1.Checked = True Then
                        MsgBox("El abono ya esta anulado!", MsgBoxStyle.Exclamation)
                        Exit Function
                    End If

                    resp = MessageBox.Show("�Desea Anular este Recibo de Pago...?", strModulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                    If resp = 6 Then
                        CheckBox1.Checked = True
                        Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()

                        If Registrar_Anulacion_Venta() Then

                            Me.DataSetAbonosProveedor.AcceptChanges()
                            MsgBox("El Recibo de Dinero ha sido anulado correctamente", MsgBoxStyle.Information)
                            Me.DataSetAbonosProveedor.detalle_abonocpagar.Clear()
                            Me.DataSetAbonosProveedor.abonocpagar.Clear()
                            Me.ToolBar1.Buttons(4).Enabled = True
                            Me.ToolBar1.Buttons(1).Enabled = True

                            Me.ToolBar1.Buttons(0).Text = "Nuevo"
                            Me.ToolBar1.Buttons(0).ImageIndex = 0
                            Me.ToolBar1.Buttons(3).Enabled = False
                            Me.ToolBar1.Buttons(2).Enabled = False
                            Me.ToolBar1.Buttons(4).Enabled = False


                            Me.GroupBox_Datos_Cliente.Enabled = False

                            Me.txtUsuario.Enabled = True
                            Me.txtUsuario.Text = ""
                            Me.txtNombreUsuario.Text = ""
                            Me.txtUsuario.Focus()
                        End If

                    Else : Exit Function

                    End If
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Function
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Dim PMU As New PerfilModulo_Class   'Declara la variable Perfil Modulo Usuario
        PMU = VSM(usua.Cedula, Me.Name) 'Carga los privilegios del usuario con el modu
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 1
                If Me.ToolBar1.Buttons(0).Text = "Cancelar" Then
                    NuevoRecibo()
                Else
                    txtUsuario.Enabled = True
                    txtUsuario.Text = ""
                    txtNombreUsuario.Text = ""
                    txtCedulaUsuario.Text = ""
                    txtUsuario.Focus()
                End If

            Case 2 : If PMU.Find Then BuscarRecibo() Else MsgBox("No tiene permiso para Buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 3 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para agregar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 4 : If PMU.Delete Then AnularRecibo() Else MsgBox("No tiene permiso para eliminar o anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub

            Case 5
                Dim numero As Integer = 0
                Dim cx As New Conexion
                numero = cx.SlqExecuteScalar(cx.Conectar("Bancos"), "Select Num_Cheque, Id_CuentaBancaria FROM Cheques where Num_Cheque = " & Me.TextBox1.Text & " AND Id_CuentaBancaria=" & Me.ComboBoxCuentaBancaria.SelectedValue)
                imprimir(numero)
            Case 6 : Me.Close()

        End Select
    End Sub
    Function imprimir(ByVal Id As Integer)
        Try
            Dim visor As New frmVisorReportes
            Dim REPORT As New ReporteChequesEstructura 'CHEQUE DE FELIPE
            REPORT.SetParameterValue(0, Id)
            CrystalReportsConexion.LoadReportViewer(visor.rptViewer, REPORT, False, Me.SqlConnection2.ConnectionString)
            REPORT.PrintToPrinter(1, True, 0, 0)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Function
    Private Sub ComboMoneda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboMoneda.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try


                If ComboMoneda.Text <> "" Then
                    ComboMoneda.Enabled = False
                    GroupBox_Datos_Cliente.Enabled = True
                    GroupBox_Datos_Abonos.Enabled = True
                    ' Me.gridFacturas.Enabled = True
                    TabControl1.Visible = True
                    TabControl1.SelectedIndex = 0
                    ComboBoxBanco.Focus() : ComboBoxBanco.SelectAll()

                    Me.ComboBoxBanco.Focus()

                Else
                    MsgBox("Debe de seleccionar una moneda ...", MsgBoxStyle.Critical)
                    ComboMoneda.Focus()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
            End Try

        End If
    End Sub
#Region "Clientes"
    Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown

        Try
            Select Case e.KeyCode
                Case Keys.F1
                    Dim cFunciones As New cFunciones
                    Me.txtCodigo.Text = cFunciones.BuscarDatos("SELECT CodigoProv AS Identificaci�n, Nombre AS Proveedor FROM Proveedores", "Nombre", "Busqueda de Proveedor...", Me.SqlConnection1.ConnectionString)
                Case Keys.Enter
                    If Not IsNumeric(txtCodigo.Text) Then Exit Sub
                    Me.DataSetAbonosProveedor.Cuentas_Bancarias_Proveedor.Clear()

                    CargarInformacionProveedor(txtCodigo.Text)
                    Cargar_Cuentas_Bancarias_Proveedor(txtCodigo.Text)
                    If Me.DataSetAbonosProveedor.detalle_abonocpagar.Count = 0 Then
                        Me.DataSetAbonosProveedor.Cuentas_bancarias.Clear()
                        'Me.DataSetAbonosProveedor.Bancos.Clear()

                        Me.AdapterBancos.Fill(Me.DataSetAbonosProveedor, "Bancos")
                        Me.AdapterCuentasBancarias.Fill(Me.DataSetAbonosProveedor, "Cuentas_bancarias")
                        Me.ComboMoneda.Enabled = True
                    End If


            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
    Private Sub CargarInformacionProveedor(ByVal codigo As String)
        Dim cConexion As New Conexion
        Dim funciones As New cFunciones
        Dim rs As SqlDataReader
        Dim i As Integer
        Dim fila As DataRow
        Dim factura As Long
        Dim conex As SqlConnection = Me.SqlConnection1

        If codigo <> Nothing Then
            conex.Open()
            rs = cConexion.GetRecorset(conex, "SELECT CodigoProv, Nombre from proveedores where codigoProv  ='" & codigo & "'")
            Try
                If rs.Read Then
                    txtCodigo.Text = rs("CodigoProv")
                    txtNombre.Text = rs("Nombre")

                    funciones.Cargar_Tabla_Generico(Me.Adapter_Proveedor, "SELECT * from proveedores where codigoProv  ='" & codigo & "'", conex.ConnectionString)
                    Me.Adapter_Proveedor.Fill(Me.DataSetAbonosProveedor, "Proveedores")
                    Tabla = funciones.BuscarFacturas_Proveedor(codigo, conex.ConnectionString)
                    txtDevoluciones.Text = 0
                    frm = New frmObtenerSaldo
                    gridFacturas.DataSource = Tabla
                    If Me.DataSetAbonosProveedor.detalle_abonocpagar.Count = 0 Then
                        gridFacturas.Enabled = False
                    End If
                    'Saldo_Cuenta(Tabla)

                    conex.Close()
                    If Tabla.Rows.Count = 0 Then
                        MessageBox.Show("El Proveedor no tiene facturas pendientes...", "Atenci�n...", MessageBoxButtons.OK)
                        txtCodigo.Focus()
                        rs.Close()
                        Exit Sub
                    Else

                        If VariarInteres = True Then
                            'txtIntereses.Enabled = True
                        Else
                            'txtIntereses.Enabled = False
                        End If
                        TxtAbono.Enabled = True
                        Me.dtFecha.Focus()
                    End If
                Else
                    MsgBox("La identificaci�n del Proveedor no se encuentra", MsgBoxStyle.Information, "Atenci�n...")
                    txtCodigo.Focus()
                    rs.Close()
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                If rs.IsClosed = False Then rs.Close()
                If conex.State <> ConnectionState.Closed Then conex.Close()
                cConexion.DesConectar(cConexion.Conectar(strModulo))
            End Try
        End If
    End Sub
    Dim Disponible As Double
    Sub saldoCuenta()
        Disponible = 0
        Dim Aplicado As Double = 0
        Dim Monto As Double = 0
        Dim dt As New DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT SaldoAjuste, Aplicado FROM AjusteInventario WHERE  Anulado = 0 AND CXP = 1 AND CodigoProv = " & Me.txtCodigo.Text, dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            Monto += dt.Rows(i).Item("SaldoAjuste")
            Aplicado += dt.Rows(i).Item("Aplicado")
        Next

        Disponible = Monto - Aplicado
        Me.txtDevoluciones.Text = Format(Disponible, "#,##0.00")

    End Sub
    Private Sub Cargar_Cuentas_Bancarias_Proveedor(ByVal Cod_Proveedor As Integer)
        Dim Fx As New cFunciones
        'Me.DataSetAbonosProveedor.Cuentas_Ban        cargas_Proveedor.Clear()
        Fx.Cargar_Tabla_Generico(Me.Adapter_Cuenta_Proveedor, "SELECT Id_Cuenta, CodigoProv, TipoCuenta, Banco, CodMoneda, Num_Cuenta, MonedaNombre FROM Cuentas_Bancarias_Proveedor WHERE (CodigoProv = " & Cod_Proveedor & ")", Me.SqlConnection1.ConnectionString)
        Me.Adapter_Cuenta_Proveedor.Fill(Me.DataSetAbonosProveedor, "Cuentas_Bancarias_Proveedor")

    End Sub

    Sub Binding()
        Try
            Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSetAbonosProveedor, "abonocpagar.Anulado"))
            Me.ComboBoxBanco.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetAbonosProveedor, "abonocpagar.Codigo_banco"))
            Me.ComboBoxTipoPago.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.TipoDocumento"))
            Me.ComboMoneda.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetAbonosProveedor, "abonocpagar.cod_Moneda"))
            Me.Label25.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "Bancos.BancosCuentas_bancarias.NombreCuenta"))
            Me.Label_CodBanco.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "Bancos.Codigo_banco"))
            Me.dtFecha.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetAbonosProveedor, "abonocpagar.Fecha"))
            Me.LabelFecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.FechaEntrada"))
            Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.Documento"))
            Me.txtCedulaUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.Cedula_Usuario"))
            Me.txtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.Cod_Proveedor"))
            Me.txtNum_Recibo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.Recibo"))
            Me.TextBoxMontoLetras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.MontoLetras"))


            Me.txtTipoCambio.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSetAbonosProveedor, "abonocpagar.TipoCambio"))

            Me.TextBoxIdFactura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Id_Compra"))
            'Me.txtMonto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.MontoFactura"))
            Me.TextBoxProveedorDetalle.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Cod_Proveedor"))
            'Me.txtSaldoAct.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Saldo_Actual"))
            Me.txtAbonoSuMoneda.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Abono_SuMoneda"))
            Me.TxtAbono.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Abono"))
            Me.txtFactura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Factura"))
            'Me.Label3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar.Id_Abonocpagar")) '<<<<<<<<<<
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub
    Function Saldo_Cuenta(ByVal Tabla1 As DataTable)

        Dim i As Integer
        Dim fila As DataRow
        Dim facturatemp As Double
        Dim Totaltemp As Double
        Dim SaldoCuenta As Double
        Dim CodigoMoneda As Integer
        Dim funciones As New cFunciones
        Dim ConexionLocal As New Conexion
        Dim rs As SqlDataReader
        Dim ValorFactura As Double
        SaldoCuenta = 0
        Try
            For i = 0 To Tabla1.Rows.Count - 1
                fila = Tabla1.Rows(i)
                facturatemp = fila("Factura")
                Totaltemp = fila("TotalFactura")
                CodigoMoneda = fila("Cod_MonedaCompra")
                rs = ConexionLocal.GetRecorset(ConexionLocal.Conectar, "SELECT ValorCompra from Moneda where CodMoneda =" & CodigoMoneda)
                If rs.Read Then ValorFactura = rs("ValorCompra")
                rs.Close()
                ConexionLocal.DesConectar(ConexionLocal.Conectar)
                ' SaldoCuenta = SaldoCuenta + funciones.Saldo_de_Factura_Proveedor(facturatemp, ((Totaltemp * ValorFactura) / TipoCambio), ValorFactura, TipoCambio)
            Next

            ConexionLocal.Conectar()
            SaldoCuenta = ConexionLocal.SQLExeScalar("SELECT ROUND(SUM(dbo.SaldoFacturaCompra(GETDATE(), compras.Factura, compras.CodigoProv) * Moneda.ValorCompra / 1), 2) AS Saldo FROM compras LEFT OUTER JOIN Moneda ON compras.Cod_MonedaCompra = Moneda.CodMoneda WHERE (compras.CodigoProv = 2)")


            ConexionLocal = Nothing
            txtSaldoAntGen.Text = Format(SaldoCuenta, "#,#0.00")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Function
#End Region
    Private Sub InformacionFactura(ByVal Factura As Double, ByVal Proveedor As Integer)
        Dim cConexion As New Conexion
        Dim rs As SqlDataReader
        Try
            If Factura <> Nothing Then
                rs = cConexion.GetRecorset(cConexion.Conectar(strModulo), "SELECT compras.Factura, compras.Fecha, compras.Vence, compras.Cod_MonedaCompra, compras.TotalFactura * (Moneda.ValorCompra / " & CDbl(Me.txtTipoCambio.Text) & ") AS TotalFactura,compras.CodigoProv, compras.Id_Compra, dbo.SaldoFacturaCompra(GETDATE(), compras.Factura, compras.CodigoProv) * (Moneda.ValorCompra / " & CDbl(Me.txtTipoCambio.Text) & ") AS SaldoFactura,Moneda.ValorCompra as TipoCambioFactura FROM compras LEFT OUTER JOIN Moneda ON compras.Cod_MonedaCompra = Moneda.CodMoneda WHERE (compras.CodigoProv = " & Proveedor & ") AND (compras.TipoCompra = 'CRE') AND (compras.Factura = " & Factura & ")")

                While rs.Read
                    Try
                        Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()
                        Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").CancelCurrentEdit()
                        Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").EndCurrentEdit()
                        Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").AddNew()

                    Catch eEndEdit As System.Data.NoNullAllowedException
                        System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
                    End Try
                    Try
                        txtFactura.Text = rs!Factura
                        ' dtEmitida.Text = rs!Fecha
                        txtMonto.Text = Format(rs!TotalFactura, "#,#0.00")
                        txtSaldo.Text = Format(rs!SaldoFactura, "#,#0.00")
                        'txtSaldoAnt.Text = txtSaldo.Text
                        TxtAbono.Text = Format(CDbl(txtSaldo.Text), "#,#0.00")
                        txtSaldoAct.Text = "0.00"
                        TextBoxTipoCambioFactura.Text = rs!TipoCambioFactura
                        Me.TextBoxProveedorDetalle.Text = Proveedor
                        Me.TextBoxIdFactura.Text = rs!Id_Compra

                        Me.TabControl1.SelectedIndex = 1
                        Me.TxtAbono.Focus()
                        Me.TxtAbono.SelectAll()




                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                cConexion.DesConectar(cConexion.sQlconexion)
            End If

        Catch ex As Exception
            MsgBox(EX.Message, MsgBoxStyle.Critical, "Alerta...!")
        End Try
    End Sub

    Private Sub gridFacturas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridFacturas.Click
        Try
            Dim hi As DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo = _
                   AdvBandedGridView1.CalcHitInfo(CType(gridFacturas, Control).PointToClient(Control.MousePosition))
            Dim data As DataRow
            Dim factura As Double

            If hi.RowHandle >= 0 Then
                data = AdvBandedGridView1.GetDataRow(hi.RowHandle)
            ElseIf AdvBandedGridView1.FocusedRowHandle >= 0 Then
                data = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle)
            Else
                data = Nothing
                Exit Sub
            End If
            factura = data("Factura")

            Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()
            InformacionFactura(factura, Me.txtCodigo.Text)

        Catch ex As SyntaxErrorException
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Private Sub txtCodigo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigo.GotFocus
        txtCodigo.SelectAll()
    End Sub
    Private Sub txtCodigo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub
    Private Sub txtObservaciones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservaciones.KeyDown
        If e.KeyData = Keys.Enter Then
            txtFactura.Focus()
        End If
    End Sub
    Private Sub txtAbono_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtAbono.KeyDown
        Try
            If e.KeyData = Keys.Enter Then

                Dim ab As Double
                If TxtAbono.Text = "" Then TxtAbono.Text = 0

                If TxtAbono.Text = 0 Then
                    MessageBox.Show("Debe de digitar un monto mayor que 0", "Atenci�n...", MessageBoxButtons.OK)
                    TxtAbono.Text = 0.0 : TxtAbono.Focus() : TxtAbono.SelectAll() : Exit Sub
                End If

                If CDbl(TxtAbono.Text) > CDbl(txtSaldo.Text) Then
                    MessageBox.Show("No puede abonarle m�s de lo que adeuda, Favor revisar...", "Atenci�n...", MessageBoxButtons.OK)
                    TxtAbono.Text = 0.0 : TxtAbono.Focus() : TxtAbono.SelectAll() : Exit Sub
                Else
                    TxtAbono.Text = CDbl(TxtAbono.Text)
                    txtAbonoSuMoneda.Text = (CDbl(TxtAbono.Text) / CDbl(Me.TextBoxTipoCambioFactura.Text)) * CDbl(Me.txtTipoCambio.Text)
                    txtSaldoAct.Text = Format(CDbl(txtSaldo.Text) - CDbl(TxtAbono.Text), "#,#0.00")
                    Dim txtPapa As String = Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("Id_Abonocpagar")
                    Dim txt As String = Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Current("Id_Abonocpagar")
                    'Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Current("Id_AjusteInventario") = Me.idAjuste
                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Current("Aplicado") = Me.montoAjuste
                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").EndCurrentEdit()
                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").AddNew()
                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").CancelCurrentEdit()
                    For i As Integer = 0 To frm.DataSetAbonosProveedor1.AplicarDevolucion.Count - 1

                        Me.BindingContext(Me.DataSetAbonosProveedor, "NotaAplicadaAbono").AddNew()
                        Me.BindingContext(Me.DataSetAbonosProveedor, "NotaAplicadaAbono").Current("IdAjusteInv") = frm.DataSetAbonosProveedor1.AplicarDevolucion(i).Id_Ajuste
                        Me.BindingContext(Me.DataSetAbonosProveedor, "NotaAplicadaAbono").Current("IdAbono") = -1
                        Me.BindingContext(Me.DataSetAbonosProveedor, "NotaAplicadaAbono").Current("MontoApli") = frm.DataSetAbonosProveedor1.AplicarDevolucion(i).MontoAplicar
                        Me.BindingContext(Me.DataSetAbonosProveedor, "NotaAplicadaAbono").EndCurrentEdit()
                        frm.DataSetAbonosProveedor1.AplicarDevolucion(i).SaldoDispo = frm.DataSetAbonosProveedor1.AplicarDevolucion(i).SaldoDispo - frm.DataSetAbonosProveedor1.AplicarDevolucion(i).MontoAplicar
                        frm.DataSetAbonosProveedor1.AplicarDevolucion(i).MontoAplicar = 0
                        frm.GridView1.RefreshEditor()
                        frm.DataSetAbonosProveedor1.AplicarDevolucion(i).EndEdit()

                    Next

                    Me.txtAbonoGen.Text = Me.colAbono.SummaryItem.SummaryValue
                    Me.txtAbonoGen.Text = Format(CDec(Me.txtAbonoGen.Text), "#,#0.00")
                    ' txtSaldoActGen.Text = Format(txtSaldoAntGen.Text - txtAbonoGen.Text, "#,#0.00")
                    txtSaldoActGen.Text = Format(txtSaldoAntGen.Text - txtAbonoGen.Text, "#,#0.00")
                    'me.txtAbonoGen.Text = 

                    Dim Num2Text As New cNum2Text
                    TextBoxMontoLetras.Text = Num2Text.Numero2Letra(Me.txtAbonoGen.Text, 0, 2, "COLON", "CENTIMO", cNum2Text.eSexo.Masculino, cNum2Text.eSexo.Masculino).ToUpper.ToString
                    'Me.TextBoxMontoLetras.Text = Num2Text.Numero2Letra(Me.txtAbonoGen.Text, 0, 2, "", "CENTIMO", cNum2Text.eSexo.Masculino, cNum2Text.eSexo.Masculino).ToUpper.ToString


                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()

                    If Me.BindingContext(Me.Tabla).Current("Factura") = Me.txtFactura.Text Then
                        Me.BindingContext(Me.Tabla).RemoveAt(BindingContext(Me.Tabla).Position())
                    End If

                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message & " Error este campo se requiere un n�merico", "SeePos", MessageBoxButtons.OK, MessageBoxIcon.Error)

            TxtAbono.Text = 0
            TxtAbono.Focus()
        End Try

    End Sub
    Private Sub txtAbono_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TxtAbono.SelectAll()
    End Sub

    Private Sub txtIntereses_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "."c) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub
    Private Sub GridControl2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl2.KeyDown
        If e.KeyCode = Keys.Delete Then
            Eliminar_Factura_Detalle()
        End If
    End Sub
    Private Sub Eliminar_Factura_Detalle()
        Dim resp As Integer
        Dim FilaTabla As DataRow
        Dim Conexion2 As New Conexion
        Dim Facturatem As Long
        Dim Fechatem As Date
        Try 'se intenta hacer
            If Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Count > 0 Then  ' si hay ubicaciones
                resp = MessageBox.Show("�Desea eliminar esta factura del Recibo de Dinero?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If resp = 6 Then
                    FilaTabla = Tabla.NewRow
                    FilaTabla("Factura") = Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Current("Factura")
                    Facturatem = FilaTabla("factura")
                    Fechatem = Conexion2.SlqExecuteScalar(Conexion2.Conectar(strModulo), "Select Fecha from compras Where Factura =" & Facturatem & " and CodigoProv =" & Me.txtCodigo.Text)
                    Conexion2.DesConectar(Conexion2.sQlconexion)
                    FilaTabla("Fecha") = Fechatem
                    Tabla.Rows.Add(FilaTabla)

                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").RemoveAt(Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").Position)
                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").EndCurrentEdit()
                    Me.txtAbonoGen.Text = Format(Me.colAbono.SummaryItem.SummaryValue, "#,#0.00")
                    txtSaldoActGen.Text = Format(txtSaldoAntGen.Text - txtAbonoGen.Text, "#,#0.00")
                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()

                    Dim Num2Text As New cNum2Text
                    TextBoxMontoLetras.Text = Num2Text.Numero2Letra(Me.txtAbonoGen.Text, 0, 2, "", "CENTIMO", cNum2Text.eSexo.Masculino, cNum2Text.eSexo.Masculino)

                Else
                    Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar.abonocpagardetalle_abonocpagar").CancelCurrentEdit()
                End If
            Else
                MsgBox("No Existen Facturas para eliminar del Recibo de Dinero", MsgBoxStyle.Information)
            End If
        Catch eEndEdit As System.Data.NoNullAllowedException
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
        End Try
    End Sub

    Private Sub txtAbono_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "."c) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub
    Private Sub Check_Dig_Recibo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Dig_Recibo.CheckedChanged
        If Me.Check_Dig_Recibo.Checked = True Then
            Me.txtNum_Recibo.Enabled = True
            Me.txtNum_Recibo.Text = ""
            Me.txtNum_Recibo.Focus()
        Else
            Me.txtNum_Recibo.Enabled = False
            txtNum_Recibo.Text = Numero_de_Recibo()
        End If
    End Sub
    Private Sub txtNum_Recibo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNum_Recibo.KeyPress
        If (Not e.KeyChar.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sende5r As System.Object, ByVal e As System.EventArgs) Handles ComboBoxTipoPago.SelectedIndexChanged
        'Revisar : PORQUE SE PONE BLANCO EL BANCO
        ComboBoxCuentaBancariaDestino.Text = ""
        Select Case Me.ComboBoxTipoPago.SelectedIndex
            Case 0 : ComboBoxCuentaBancariaDestino.Enabled = False : BuscarNumeroCheque()
            Case 1 : TextBox1.Text = "" : ComboBoxCuentaBancariaDestino.Enabled = True
        End Select
    End Sub
    Private Sub ComboBoxs_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBoxBanco.KeyDown, ComboBoxCuentaBancaria.KeyDown, ComboMoneda.KeyDown, ComboBoxTipoPago.KeyDown, ComboBoxCuentaBancariaDestino.KeyDown
        Select Case sender.name
            Case Me.ComboMoneda.Name
                Try
                    Me.ComboBoxBanco.SelectedIndex = 0
                    Dim Conexion As New Conexion
                    Conexion.Conectar(strModulo)
                    txtSaldoAntGen.Text = Conexion.SQLExeScalar("SELECT ROUND(SUM(dbo.SaldoFacturaCompra(GETDATE(), compras.Factura, compras.CodigoProv) * Moneda.ValorCompra / " & Me.txtTipoCambio.Text & "), 2) AS Saldo FROM compras LEFT OUTER JOIN Moneda ON compras.Cod_MonedaCompra = Moneda.CodMoneda WHERE (compras.CodigoProv = " & Me.txtCodigo.Text & ")")
                    Conexion = Nothing
                    Me.TabPage1.Visible = True
                    Me.TabControl1.SelectedIndex = 0
                    BuscarNumeroCheque()
                Catch ex As SystemException
                    MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
                End Try
        End Select
        If e.KeyCode = Keys.Enter Then
            Me.ProcessTabKey(True)
            'Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()
        End If
    End Sub

#Region "VALIDACION DE CAMPOS"
    Private Function ValidacionDatos()
        ValidacionDatos = False

        If Me.ComboMoneda.Text = "" Then
            MsgBox("Debe de seleccionar una moneda....", MsgBoxStyle.Exclamation)
            Me.ComboMoneda.Focus()
            Exit Function
        End If
        ValidacionDatos = True
    End Function
#End Region

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        Dim sql As String
        Dim cx As New Conexion
        Dim cnnConexion As New System.Data.SqlClient.SqlConnection
        Dim rstReader As System.Data.SqlClient.SqlDataReader
        Dim Numero As Double

        If e.KeyCode = Keys.Enter Then
            cnnConexion.ConnectionString = GetSetting("SeeSoft", "Bancos", "CONEXION")
            cnnConexion.Open()
            Numero = cx.SlqExecuteScalar(cx.Conectar("Bancos"), "Select Num_Cheque, Id_CuentaBancaria FROM Cheques where Num_Cheque = " & Me.TextBox1.Text & " AND Id_CuentaBancaria=" & Me.ComboBoxCuentaBancaria.SelectedValue)
            If Numero Then
                MsgBox("El N�mero de Cheque ya esta ingresado, favor revisar", MsgBoxStyle.Information, "Atenci�n ...")
            Else
                Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("Documento") = Me.TextBox1.Text
                Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").Current("TipoDocumento") = ComboBoxTipoPago.Text
                Me.BindingContext(Me.DataSetAbonosProveedor, "abonocpagar").EndCurrentEdit()
                Me.gridFacturas.Enabled = True
                If ComboBoxCuentaBancariaDestino.Enabled = True Then
                    Me.ComboBoxCuentaBancariaDestino.Focus()
                End If
            End If
            cx.DesConectar(cnnConexion)
        End If
    End Sub

    Private Sub ComboBoxCuentaBancaria_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCuentaBancaria.SelectedIndexChanged
        BuscarNumeroCheque()
    End Sub

    Private Sub BuscarNumeroCheque()
        Dim clsConexion As New Conexion
        Dim cnnConexion As New System.Data.SqlClient.SqlConnection
        Dim rstReader As System.Data.SqlClient.SqlDataReader
        Dim n As Integer
        Dim sql, Cuenta As String
        Dim Cx As New Conexion


        cnnConexion.ConnectionString = GetSetting("SeeSoft", "Bancos", "CONEXION")
        cnnConexion.Open()


        If Me.ComboBoxCuentaBancaria.SelectedIndex = -1 Then Exit Sub


        Cuenta = Me.ComboBoxCuentaBancaria.Text

        'Cuenta = Me.DataSetAbonosProveedor.Cuentas_bancarias(BindingContext(DataSetAbonosProveedor.Cuentas_bancarias).Position).Cuenta

        sql = " select isnull(max(num_cheque + 1 ),1) as numero from cheques che, Cuentas_bancarias CB  " & _
                " where Che.id_cuentabancaria = CB.Id_CuentaBancaria " & _
                " and CB.Cuenta ='" & Cuenta & "' and Tipo = 'CHEQUE'"


        rstReader = clsConexion.GetRecorset(cnnConexion, sql)
        If rstReader.Read() = False Then Exit Sub
        Me.TextBox1.Text = rstReader("NUMERO")
        rstReader.Close()

        If CDbl(TextBox1.Text) = 1 Then
            TextBox1.Text = Cx.SlqExecuteScalar(Cx.Conectar("Bancos"), "SELECT ChequeInicial FROM Cuentas_bancarias WHERE(Cuenta = '" & Cuenta & "')")
            Cx.DesConectar(Cx.sQlconexion)
        End If
    End Sub

    Private Sub dtFecha_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtFecha.KeyDown
        If e.KeyCode = Keys.Enter Then
            ComboMoneda.Focus()
        End If
    End Sub
    Dim idAjuste As Double = 0
    Dim montoAjuste As Double = 0
    Private Sub btnDevolucion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDevolucion.Click
        sp_ApliDevolucion()
        Me.TxtAbono.Focus()
    End Sub
    Dim frm As New frmObtenerSaldo
    Sub sp_ApliDevolucion()
        If Me.ComboMoneda.SelectedValue = 2 Then
            MsgBox("Las devoluciones de mercancia solo estan disponibles para relacionar a pagos en COLONES", MsgBoxStyle.OKOnly)
            Exit Sub
        End If

        frm.CodigoProv = Me.txtCodigo.Text



        'Dim mud As Double = 0 'monto usado en devoluciones

        'For i As Integer = 0 To Me.DataSetAbonosProveedor.detalle_abonocpagar.Count - 1
        '    If Me.DataSetAbonosProveedor.detalle_abonocpagar(i).RowState = DataRowState.Deleted Then
        '        mud += Me.DataSetAbonosProveedor.detalle_abonocpagar(i).Aplicado

        '    End If
        'Next
        'frm.MontoAjuste = mud
        If frm.ShowDialog = DialogResult.OK Then
            'idAjuste = frm.BindingContext(frm.DataSetAbonosProveedor1, "AplicarDevolucion").Current("Id_Ajuste")

            montoAjuste = frm.MontoAjuste
            Me.txtDevoluciones.Text = montoAjuste
            Me.txtSaldo.Text -= montoAjuste
            Me.TxtAbono.Text -= montoAjuste

        End If
    End Sub


    Private Sub txtDevoluciones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDevoluciones.KeyDown
        If e.KeyCode = Keys.F1 Then
            sp_ApliDevolucion()
        End If
    End Sub

#Region "Asientos Contables"
    'Dim EditaAsiento As Boolean = False
    Public Sub GuardaAsiento(ByVal _Id_Cheque As String)
        Dim NumeroAsiento As String
        Dim Fx As New cFunciones
        Dim Funciones As New Conexion
        Dim Id_Cheque As Long
        Dim Num_Cheque As String
        Dim Monto As Decimal
        Dim CodMoneda As Integer

        Dim dts As New DataTable
        cFunciones.Llenar_Tabla_Generico("select Id_Cheque, Num_Cheque, Monto, CodigoMoneda from dbo.Cheques where Id_Cheque = " & _Id_Cheque, dts, GetSetting("SeeSOFT", "Bancos", "Conexion"))
        If dts.Rows.Count > 0 Then
            Id_Cheque = dts.Rows(0).Item("Id_Cheque")
            Num_Cheque = dts.Rows(0).Item("Num_Cheque")
            Monto = dts.Rows(0).Item("Monto")
            CodMoneda = dts.Rows(0).Item("CodigoMoneda")
        End If

        Try
            '------------------------------------------------------------------
            'CREA EL ASIENTO CONTABLE cambiar a false DIEGO
            'If EditaAsiento = False Then    'SI NO SE ESTA EDITANDO EL ASIENTO LO CREA NUEVO
            Me.DataSetAbonosProveedor.AsientosContables.Clear()
            Me.DataSetAbonosProveedor.DetallesAsientosContable.Clear()
            NumeroAsiento = Fx.BuscaNumeroAsiento("BCO-" & Format(fec.Month, "00") & Format(fec.Date, "yy") & "-")
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").EndCurrentEdit()
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").AddNew()
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NumAsiento") = NumeroAsiento
            'Else                            'SI SE ESTA EDITANDO EL ASIENTO BORRA LOS DETALLES PARA VOLVERLOS A CREAR
            '    If BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Count < 1 Then
            '        Me.DataSetAbonosProveedor.AsientosContables.Clear()
            '        Me.DataSetAbonosProveedor.DetallesAsientosContable.Clear()
            '        NumeroAsiento = Fx.BuscaNumeroAsiento("BCO-" & Format(fec.Month, "00") & Format(fec.Date, "yy") & "-")
            '        BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").EndCurrentEdit()
            '        BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").AddNew()
            '        BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NumAsiento") = NumeroAsiento
            '    Else
            '        Funciones.DeleteRecords("DetallesAsientosContable", "NumAsiento ='" & BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NumAsiento") & "'", "Contabilidad")
            '    End If
            'End If

            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Fecha") = fec
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("IdNumDoc") = Id_Cheque
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NumDoc") = Num_Cheque
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Beneficiario") = txtNombre.Text
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("TipoDoc") = 1
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Accion") = "AUT"
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Anulado") = 0
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("FechaEntrada") = Now.Date
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Mayorizado") = 1
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Periodo") = Fx.BuscaPeriodo(fec)
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NumMayorizado") = 1
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Modulo") = "Cheques/Transferencias"
            If ComboBoxTipoPago.Text = "CHEQUE" Then
                BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Observaciones") = "Cheque # " & Num_Cheque
            Else
                BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Observaciones") = "Transferencia # " & Num_Cheque
            End If
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NombreUsuario") = txtNombreUsuario.Text
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("TotalDebe") = Monto
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("TotalHaber") = Monto
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("CodMoneda") = CodMoneda
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("TipoCambio") = CDbl(txtTipoCambio.Text)
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").EndCurrentEdit()
            '------------------------------------------------------------------

            'CREA TODOS LOS DETALLES DEL ASIENTO
            AsientoDetalle(Monto)

            '------------------------------------------------------------------
            'ACTUALIZA CENTROS DE COSTOS
            'If Me.DataSetAbonosProveedor.CentroCosto_Movimientos.Count > 0 Then
            '    For i As Integer = 0 To Me.DataSetAbonosProveedor.CentroCosto_Movimientos.Count - 1    'LE ASIGNA EL NUMERO DE ASIENTO Y DE DOCUMENTO A LOS CENTROS DE COSTO
            '        If Not Me.DataSetAbonosProveedor.CentroCosto_Movimientos(i).RowState = DataRowState.Deleted Then
            '            Me.DataSetAbonosProveedor.CentroCosto_Movimientos.Item(i).IdAsiento = BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NumAsiento")
            '            Me.DataSetAbonosProveedor.CentroCosto_Movimientos.Item(i).Documento = Me.DataSetAbonosProveedor.Cheques(0).Num_Cheque
            '        End If

            '    Next i
            'End If
            '------------------------------------------------------------------

            'ACTUALIZA EL NUMERO DE ASIENTO AL CHEQUE
            Me.TransAsiento()
            Funciones.UpdateRecords("Cheques", "Contabilizado = 1, Asiento = '" & BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NumAsiento") & "'", "Id_Cheque = " & Id_Cheque, "Bancos")

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Sub

    Public Sub GuardaAsientoDetalle(ByVal Monto As Double, ByVal Debe As Boolean, ByVal Haber As Boolean, ByVal Cuenta As String, ByVal NombreCuenta As String, ByVal Observacion As String)
        If Monto <> 0 Then       'CREA LOS DETALLES DE ASIENTOS CONTABLES
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").AddNew()
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("NumAsiento") = BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("NumAsiento")
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("DescripcionAsiento") = Observacion
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Cuenta") = Cuenta
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("NombreCuenta") = NombreCuenta
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Monto") = Monto
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Debe") = Debe
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("Haber") = Haber
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").Current("TipoCambio") = CDbl(txtTipoCambio.Text)
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
        End If
    End Sub

    Private Function AsientoDetalle(ByVal _monto As Decimal) As Boolean
        Try
            Dim dts As New DataTable
            Dim Obs As String = BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").Current("Observaciones")

            cFunciones.Llenar_Tabla_Generico("select CuentaContable, DescripcionCuentaContable from Proveedores where CodigoProv = " & Me.txtCodigo.Text, dts, GetSetting("SeeSOFT", "SeePos", "Conexion"))
            If dts.Rows.Count > 0 Then
                GuardaAsientoDetalle(_monto, True, False, dts.Rows(0).Item("CuentaContable"), dts.Rows(0).Item("DescripcionCuentaContable"), Obs)
            Else
                Return False
            End If

            cFunciones.Llenar_Tabla_Generico("select CuentaContable, NombreCuentaContable from Cuentas_bancarias where Id_CuentaBancaria = " & Me.ComboBoxCuentaBancaria.SelectedValue, dts, GetSetting("SeeSOFT", "Bancos", "Conexion"))
            If dts.Rows.Count > 0 Then
                GuardaAsientoDetalle(_monto, False, True, dts.Rows(0).Item("CuentaContable"), dts.Rows(0).Item("NombreCuentaContable"), Obs)
            Else
                Return False
            End If

            Return True
        Catch ex As System.Exception
            Return False
            MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
        End Try
    End Function


    Function TransAsiento() As Boolean  'REALIZA LA TRANSACCI�N DE LOS ASIENTOS CONTABLES
        Dim Trans As SqlTransaction
        Try
            If Me.conContabilidad.State <> Me.conContabilidad.State.Open Then Me.conContabilidad.Open()

            Trans = Me.conContabilidad.BeginTransaction
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables.AsientosContablesDetallesAsientosContable").EndCurrentEdit()
            BindingContext(Me.DataSetAbonosProveedor, "AsientosContables").EndCurrentEdit()
            'BindingContext(Me.DataSetAbonosProveedor, "CentroCosto").EndCurrentEdit()

            Me.cmdDetalleAsientos.UpdateCommand.Transaction = Trans
            Me.cmdDetalleAsientos.DeleteCommand.Transaction = Trans
            Me.cmdDetalleAsientos.InsertCommand.Transaction = Trans

            Me.cmdAsientos.UpdateCommand.Transaction = Trans
            Me.cmdAsientos.DeleteCommand.Transaction = Trans
            Me.cmdAsientos.InsertCommand.Transaction = Trans

            'AdapterCentroCostoMovimiento.UpdateCommand.Transaction = Trans
            'AdapterCentroCostoMovimiento.DeleteCommand.Transaction = Trans
            'AdapterCentroCostoMovimiento.InsertCommand.Transaction = Trans

            '-----------------------------------------------------------------------------------
            'Inicia Transacci�n....
            Me.cmdDetalleAsientos.Update(Me.DataSetAbonosProveedor.DetallesAsientosContable)
            Me.cmdAsientos.Update(Me.DataSetAbonosProveedor.AsientosContables)
            'AdapterCentroCostoMovimiento.Update(Me.DataSetAbonosProveedor.CentroCosto_Movimientos)
            '-----------------------------------------------------------------------------------
            Trans.Commit()
            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message, MsgBoxStyle.Information)
            Return False
        End Try
    End Function


    Function CargarAsiento(ByVal Id As String)
        Dim cnn As SqlConnection = Nothing
        ' Dentro de un Try/Catch por si se produce un error
        Try
            Dim sConn As String = GetSetting("Seesoft", "Contabilidad", "Conexion")
            cnn = New SqlConnection(sConn)
            cnn.Open()
            Dim cmd As SqlCommand = New SqlCommand
            Dim sel As String = "Select * From AsientosContables WHERE NumAsiento='" & Id & "'"
            cmd.CommandText = sel
            cmd.Connection = cnn
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 90
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            Me.DataSetAbonosProveedor.DetallesAsientosContable.Clear()
            Me.DataSetAbonosProveedor.AsientosContables.Clear()
            da.Fill(Me.DataSetAbonosProveedor.AsientosContables)
            If Me.DataSetAbonosProveedor.AsientosContables.Count < 1 Then
                Me.DataSetAbonosProveedor.AsientosContables.Clear()
                Exit Try
            End If

        Catch ex As System.Exception
            MsgBox(ex.ToString)
        Finally
            If Not cnn Is Nothing Then
                cnn.Close()
            End If
        End Try
    End Function
#End Region


End Class
