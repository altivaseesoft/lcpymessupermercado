Public Class frmObtenerSaldo
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIdAjuste As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSaldoDisponible As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAplicar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DataSetAbonosProveedor1 As LcPymes_5._2.DataSetAbonosProveedor
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSaldoDisponible As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.DataSetAbonosProveedor1 = New LcPymes_5._2.DataSetAbonosProveedor
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colIdAjuste = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSaldoDisponible = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colAplicar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtSaldoDisponible = New System.Windows.Forms.TextBox
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetAbonosProveedor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(8, 0)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 56)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "Aceptar"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(88, 0)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 56)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "Cancelar"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.DataMember = "AplicarDevolucion"
        Me.GridControl1.DataSource = Me.DataSetAbonosProveedor1
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 72)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(392, 264)
        Me.GridControl1.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 6
        Me.GridControl1.Text = "GridControl1"
        '
        'DataSetAbonosProveedor1
        '
        Me.DataSetAbonosProveedor1.DataSetName = "DataSetAbonosProveedor"
        Me.DataSetAbonosProveedor1.Locale = New System.Globalization.CultureInfo("es-MX")
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIdAjuste, Me.colSaldoDisponible, Me.colAplicar})
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowFilterPanel = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colIdAjuste
        '
        Me.colIdAjuste.Caption = "Ajuste"
        Me.colIdAjuste.FieldName = "Id_Ajuste"
        Me.colIdAjuste.Name = "colIdAjuste"
        Me.colIdAjuste.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colIdAjuste.VisibleIndex = 0
        '
        'colSaldoDisponible
        '
        Me.colSaldoDisponible.Caption = "Saldo Disponible"
        Me.colSaldoDisponible.FieldName = "SaldoDispo"
        Me.colSaldoDisponible.Name = "colSaldoDisponible"
        Me.colSaldoDisponible.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colSaldoDisponible.VisibleIndex = 1
        '
        'colAplicar
        '
        Me.colAplicar.Caption = "Aplicar"
        Me.colAplicar.FieldName = "MontoAplicar"
        Me.colAplicar.Name = "colAplicar"
        Me.colAplicar.VisibleIndex = 2
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(176, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Total Disponible:"
        '
        'txtSaldoDisponible
        '
        Me.txtSaldoDisponible.Location = New System.Drawing.Point(176, 32)
        Me.txtSaldoDisponible.Name = "txtSaldoDisponible"
        Me.txtSaldoDisponible.ReadOnly = True
        Me.txtSaldoDisponible.Size = New System.Drawing.Size(176, 20)
        Me.txtSaldoDisponible.TabIndex = 8
        Me.txtSaldoDisponible.Text = "0"
        Me.txtSaldoDisponible.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frmObtenerSaldo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(400, 354)
        Me.Controls.Add(Me.txtSaldoDisponible)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(384, 304)
        Me.Name = "frmObtenerSaldo"
        Me.Text = "Obtener Saldo de Devoluci�n"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetAbonosProveedor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public CodigoProv As Integer = 0
    Public IdAjuste As Integer = 0
    Public MontoAjuste As Double = 0

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim x As Integer = 0
        DialogResult = Windows.Forms.DialogResult.OK
        MontoAjuste = 0
        For i As Integer = 0 To Me.DataSetAbonosProveedor1.AplicarDevolucion.Count - 1
            MontoAjuste += Me.DataSetAbonosProveedor1.AplicarDevolucion(i).MontoAplicar
           
        Next
        'For x = 0 To Me.DataSetAbonosProveedor1.AplicarDevolucion.Count - 1
        '    If Me.DataSetAbonosProveedor1.AplicarDevolucion(x).SaldoDispo = 0 Then
        '        Me.DataSetAbonosProveedor1.AplicarDevolucion.Clear()
        '    End If
        'Next
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub frmObtenerSaldo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If DataSetAbonosProveedor1.AplicarDevolucion.Count = 0 Then
            cFunciones.Llenar_Tabla_Generico("SELECT Consecutivo AS Id_Ajuste, TotalCXP - Aplicado AS SaldoDispo, 0 AS MontoAplicar FROM AjusteInventario WHERE ABS(SaldoAjuste) - Aplicado > 0 AND (CXP = 1) AND (CodigoProv = " & Me.CodigoProv & ")", Me.DataSetAbonosProveedor1.AplicarDevolucion)
        End If

        Dim saldoDisponible As Double = 0
        For i As Integer = 0 To Me.DataSetAbonosProveedor1.AplicarDevolucion.Count - 1

            saldoDisponible += Me.DataSetAbonosProveedor1.AplicarDevolucion(i).SaldoDispo
        Next
        Me.txtSaldoDisponible.Text = Format(saldoDisponible, "#,##0.00")


    End Sub

End Class
