Imports System.data.SqlClient
Imports System.Data

Public Class MovimientoArticulos
    Inherits FrmPlantilla

#Region "Variables"
    Private cConexion As New Conexion
    Private sqlConexion As SqlConnection
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCodArticulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridCompras As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewCompras As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridVentas As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewVentas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents AdapterVentas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents DsMovimientoArticulos1 As LcPymes_5._2.DsMovimientoArticulos
    Friend WithEvents colFactura As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DT_Inicio As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DT_Final As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BCompras As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BVentas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents AdapterCompras As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents colFacturaCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTipoCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFechaCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colProveedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCantidadCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrecioCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GBCompras As System.Windows.Forms.GroupBox
    Friend WithEvents GBVentas As System.Windows.Forms.GroupBox
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents lblComprados As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblVendidos As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MovimientoArticulos))
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.DT_Inicio = New DevExpress.XtraEditors.DateEdit
        Me.DT_Final = New DevExpress.XtraEditors.DateEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtCodigo = New DevExpress.XtraEditors.TextEdit
        Me.txtCodArticulo = New DevExpress.XtraEditors.TextEdit
        Me.GridCompras = New DevExpress.XtraGrid.GridControl
        Me.DsMovimientoArticulos1 = New LcPymes_5._2.DsMovimientoArticulos
        Me.GridViewCompras = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colFacturaCompra = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTipoCompra = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colFechaCompra = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colProveedor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCantidadCompra = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colPrecioCompra = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridVentas = New DevExpress.XtraGrid.GridControl
        Me.GridViewVentas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colFactura = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colPrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.AdapterVentas = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.BCompras = New DevExpress.XtraEditors.SimpleButton
        Me.BVentas = New DevExpress.XtraEditors.SimpleButton
        Me.AdapterCompras = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.GBCompras = New System.Windows.Forms.GroupBox
        Me.GBVentas = New System.Windows.Forms.GroupBox
        Me.lblComprados = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblVendidos = New System.Windows.Forms.Label
        CType(Me.DT_Inicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DT_Final.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodArticulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsMovimientoArticulos1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GBCompras.SuspendLayout()
        Me.GBVentas.SuspendLayout()
        Me.SuspendLayout()
        '
        'TituloModulo
        '
        Me.TituloModulo.Name = "TituloModulo"
        Me.TituloModulo.Size = New System.Drawing.Size(754, 32)
        Me.TituloModulo.Text = "Movimiento de Art�culos"
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 480)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.Size = New System.Drawing.Size(754, 8)
        Me.ToolBar1.Visible = False
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(766, 458)
        Me.DataNavigator.Name = "DataNavigator"
        Me.DataNavigator.Visible = False
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label4.Location = New System.Drawing.Point(504, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 72
        Me.Label4.Text = "Hasta"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.Location = New System.Drawing.Point(400, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 71
        Me.Label3.Text = "Desde"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DT_Inicio
        '
        Me.DT_Inicio.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DT_Inicio.EditValue = New Date(2010, 2, 25, 0, 0, 0, 0)
        Me.DT_Inicio.Location = New System.Drawing.Point(400, 56)
        Me.DT_Inicio.Name = "DT_Inicio"
        '
        'DT_Inicio.Properties
        '
        Me.DT_Inicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DT_Inicio.Size = New System.Drawing.Size(96, 19)
        Me.DT_Inicio.TabIndex = 1
        '
        'DT_Final
        '
        Me.DT_Final.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DT_Final.EditValue = New Date(2010, 2, 25, 0, 0, 0, 0)
        Me.DT_Final.Location = New System.Drawing.Point(504, 56)
        Me.DT_Final.Name = "DT_Final"
        '
        'DT_Final.Properties
        '
        Me.DT_Final.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DT_Final.Size = New System.Drawing.Size(96, 19)
        Me.DT_Final.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(16, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 16)
        Me.Label1.TabIndex = 75
        Me.Label1.Text = "C�digo"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCodigo
        '
        Me.txtCodigo.EditValue = ""
        Me.txtCodigo.Location = New System.Drawing.Point(16, 56)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(120, 19)
        Me.txtCodigo.TabIndex = 0
        '
        'txtCodArticulo
        '
        Me.txtCodArticulo.EditValue = ""
        Me.txtCodArticulo.Location = New System.Drawing.Point(-104, 40)
        Me.txtCodArticulo.Name = "txtCodArticulo"
        Me.txtCodArticulo.Size = New System.Drawing.Size(104, 19)
        Me.txtCodArticulo.TabIndex = 80
        Me.txtCodArticulo.Visible = False
        '
        'GridCompras
        '
        Me.GridCompras.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridCompras.DataMember = "MovimientoArticulosCompra"
        Me.GridCompras.DataSource = Me.DsMovimientoArticulos1
        '
        'GridCompras.EmbeddedNavigator
        '
        Me.GridCompras.EmbeddedNavigator.Name = ""
        Me.GridCompras.Location = New System.Drawing.Point(8, 16)
        Me.GridCompras.MainView = Me.GridViewCompras
        Me.GridCompras.Name = "GridCompras"
        Me.GridCompras.Size = New System.Drawing.Size(720, 136)
        Me.GridCompras.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridCompras.TabIndex = 81
        Me.GridCompras.Text = "Compras"
        '
        'DsMovimientoArticulos1
        '
        Me.DsMovimientoArticulos1.DataSetName = "DsMovimientoArticulos"
        Me.DsMovimientoArticulos1.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'GridViewCompras
        '
        Me.GridViewCompras.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFacturaCompra, Me.colTipoCompra, Me.colFechaCompra, Me.colProveedor, Me.colCantidadCompra, Me.colPrecioCompra})
        Me.GridViewCompras.Name = "GridViewCompras"
        Me.GridViewCompras.OptionsView.ShowGroupPanel = False
        '
        'colFacturaCompra
        '
        Me.colFacturaCompra.Caption = "Factura"
        Me.colFacturaCompra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colFacturaCompra.FieldName = "Factura"
        Me.colFacturaCompra.Name = "colFacturaCompra"
        Me.colFacturaCompra.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFacturaCompra.VisibleIndex = 0
        Me.colFacturaCompra.Width = 81
        '
        'colTipoCompra
        '
        Me.colTipoCompra.Caption = "Tipo"
        Me.colTipoCompra.FieldName = "TipoCompra"
        Me.colTipoCompra.Name = "colTipoCompra"
        Me.colTipoCompra.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTipoCompra.VisibleIndex = 1
        Me.colTipoCompra.Width = 56
        '
        'colFechaCompra
        '
        Me.colFechaCompra.Caption = "Fecha"
        Me.colFechaCompra.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.colFechaCompra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colFechaCompra.FieldName = "Fecha"
        Me.colFechaCompra.Name = "colFechaCompra"
        Me.colFechaCompra.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFechaCompra.VisibleIndex = 2
        Me.colFechaCompra.Width = 76
        '
        'colProveedor
        '
        Me.colProveedor.Caption = "Proveedor"
        Me.colProveedor.FieldName = "Nombre"
        Me.colProveedor.Name = "colProveedor"
        Me.colProveedor.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colProveedor.VisibleIndex = 3
        Me.colProveedor.Width = 321
        '
        'colCantidadCompra
        '
        Me.colCantidadCompra.Caption = "Cantidad"
        Me.colCantidadCompra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colCantidadCompra.FieldName = "Cantidad"
        Me.colCantidadCompra.Name = "colCantidadCompra"
        Me.colCantidadCompra.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCantidadCompra.VisibleIndex = 4
        Me.colCantidadCompra.Width = 78
        '
        'colPrecioCompra
        '
        Me.colPrecioCompra.Caption = "Precio"
        Me.colPrecioCompra.DisplayFormat.FormatString = "#,##0.00"
        Me.colPrecioCompra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colPrecioCompra.FieldName = "Total"
        Me.colPrecioCompra.Name = "colPrecioCompra"
        Me.colPrecioCompra.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colPrecioCompra.VisibleIndex = 5
        Me.colPrecioCompra.Width = 94
        '
        'GridVentas
        '
        Me.GridVentas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridVentas.DataMember = "MovimientoArticuloVentas"
        Me.GridVentas.DataSource = Me.DsMovimientoArticulos1
        '
        'GridVentas.EmbeddedNavigator
        '
        Me.GridVentas.EmbeddedNavigator.Name = ""
        Me.GridVentas.Location = New System.Drawing.Point(8, 16)
        Me.GridVentas.MainView = Me.GridViewVentas
        Me.GridVentas.Name = "GridVentas"
        Me.GridVentas.Size = New System.Drawing.Size(720, 136)
        Me.GridVentas.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridVentas.TabIndex = 82
        Me.GridVentas.Text = "Ventas"
        '
        'GridViewVentas
        '
        Me.GridViewVentas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFactura, Me.colTipo, Me.colFecha, Me.colCliente, Me.colCantidad, Me.colPrecio})
        Me.GridViewVentas.Name = "GridViewVentas"
        Me.GridViewVentas.OptionsView.ShowGroupPanel = False
        '
        'colFactura
        '
        Me.colFactura.Caption = "Factura"
        Me.colFactura.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colFactura.FieldName = "Num_Factura"
        Me.colFactura.Name = "colFactura"
        Me.colFactura.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFactura.VisibleIndex = 0
        Me.colFactura.Width = 85
        '
        'colTipo
        '
        Me.colTipo.Caption = "Tipo"
        Me.colTipo.FieldName = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colTipo.VisibleIndex = 1
        Me.colTipo.Width = 51
        '
        'colFecha
        '
        Me.colFecha.Caption = "Fecha"
        Me.colFecha.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.colFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colFecha.FieldName = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colFecha.VisibleIndex = 2
        Me.colFecha.Width = 76
        '
        'colCliente
        '
        Me.colCliente.Caption = "Cliente"
        Me.colCliente.FieldName = "Nombre_Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCliente.VisibleIndex = 3
        Me.colCliente.Width = 321
        '
        'colCantidad
        '
        Me.colCantidad.Caption = "Cantidad"
        Me.colCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colCantidad.FieldName = "Cantidad"
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colCantidad.VisibleIndex = 4
        '
        'colPrecio
        '
        Me.colPrecio.Caption = "Precio"
        Me.colPrecio.DisplayFormat.FormatString = "#,##0.00"
        Me.colPrecio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colPrecio.FieldName = "Precio_Unit"
        Me.colPrecio.Name = "colPrecio"
        Me.colPrecio.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colPrecio.VisibleIndex = 5
        Me.colPrecio.Width = 98
        '
        'AdapterVentas
        '
        Me.AdapterVentas.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterVentas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MovimientoArticuloVentas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Nombre_Cliente", "Nombre_Cliente"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Precio_Unit", "Precio_Unit")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Num_Factura, Tipo, Nombre_Cliente, Fecha, Codigo, Descripcion, Cantidad, P" & _
        "recio_Unit FROM MovimientoArticuloVentas"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=Esteban1;packet size=4096;integrated security=SSPI;data source=""Es" & _
        "teban1\SQLEXPRESS"";persist security info=False;initial catalog=SeePos"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(144, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(248, 16)
        Me.Label2.TabIndex = 83
        Me.Label2.Text = "Descripci�n"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescripcion.EditValue = ""
        Me.txtDescripcion.Location = New System.Drawing.Point(144, 56)
        Me.txtDescripcion.Name = "txtDescripcion"
        '
        'txtDescripcion.Properties
        '
        Me.txtDescripcion.Properties.ReadOnly = True
        Me.txtDescripcion.Size = New System.Drawing.Size(248, 19)
        Me.txtDescripcion.TabIndex = 84
        '
        'BCompras
        '
        Me.BCompras.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BCompras.Location = New System.Drawing.Point(640, 32)
        Me.BCompras.Name = "BCompras"
        Me.BCompras.TabIndex = 3
        Me.BCompras.Text = "Compras"
        '
        'BVentas
        '
        Me.BVentas.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BVentas.Location = New System.Drawing.Point(640, 56)
        Me.BVentas.Name = "BVentas"
        Me.BVentas.TabIndex = 4
        Me.BVentas.Text = "Ventas"
        '
        'AdapterCompras
        '
        Me.AdapterCompras.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterCompras.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MovimientoArticulosCompra", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Factura", "Factura"), New System.Data.Common.DataColumnMapping("TipoCompra", "TipoCompra"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Total", "Total")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Factura, TipoCompra, Nombre, Fecha, Codigo, Cantidad, Total FROM Movimient" & _
        "oArticulosCompra"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'GBCompras
        '
        Me.GBCompras.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GBCompras.BackColor = System.Drawing.Color.Transparent
        Me.GBCompras.Controls.Add(Me.GridCompras)
        Me.GBCompras.Controls.Add(Me.Label6)
        Me.GBCompras.Controls.Add(Me.lblComprados)
        Me.GBCompras.Location = New System.Drawing.Point(8, 80)
        Me.GBCompras.Name = "GBCompras"
        Me.GBCompras.Size = New System.Drawing.Size(736, 192)
        Me.GBCompras.TabIndex = 85
        Me.GBCompras.TabStop = False
        Me.GBCompras.Text = "Compras"
        '
        'GBVentas
        '
        Me.GBVentas.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GBVentas.BackColor = System.Drawing.Color.Transparent
        Me.GBVentas.Controls.Add(Me.GridVentas)
        Me.GBVentas.Controls.Add(Me.lblVendidos)
        Me.GBVentas.Controls.Add(Me.Label5)
        Me.GBVentas.Location = New System.Drawing.Point(8, 280)
        Me.GBVentas.Name = "GBVentas"
        Me.GBVentas.Size = New System.Drawing.Size(736, 192)
        Me.GBVentas.TabIndex = 86
        Me.GBVentas.TabStop = False
        Me.GBVentas.Text = "Ventas"
        '
        'lblComprados
        '
        Me.lblComprados.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblComprados.BackColor = System.Drawing.Color.Transparent
        Me.lblComprados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComprados.Location = New System.Drawing.Point(616, 160)
        Me.lblComprados.Name = "lblComprados"
        Me.lblComprados.TabIndex = 87
        Me.lblComprados.Text = "0.00"
        Me.lblComprados.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Location = New System.Drawing.Point(504, 160)
        Me.Label6.Name = "Label6"
        Me.Label6.TabIndex = 88
        Me.Label6.Text = "Total Articulos"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Location = New System.Drawing.Point(512, 160)
        Me.Label5.Name = "Label5"
        Me.Label5.TabIndex = 90
        Me.Label5.Text = "Total Articulos"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblVendidos
        '
        Me.lblVendidos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblVendidos.BackColor = System.Drawing.Color.Transparent
        Me.lblVendidos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVendidos.Location = New System.Drawing.Point(624, 160)
        Me.lblVendidos.Name = "lblVendidos"
        Me.lblVendidos.TabIndex = 89
        Me.lblVendidos.Text = "0.00"
        Me.lblVendidos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MovimientoArticulos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(754, 488)
        Me.Controls.Add(Me.GBVentas)
        Me.Controls.Add(Me.GBCompras)
        Me.Controls.Add(Me.BVentas)
        Me.Controls.Add(Me.BCompras)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtCodArticulo)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DT_Final)
        Me.Controls.Add(Me.DT_Inicio)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Name = "MovimientoArticulos"
        Me.Text = "Movimiento de Art�culos"
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.DT_Inicio, 0)
        Me.Controls.SetChildIndex(Me.DT_Final, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.txtCodigo, 0)
        Me.Controls.SetChildIndex(Me.txtCodArticulo, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.BCompras, 0)
        Me.Controls.SetChildIndex(Me.BVentas, 0)
        Me.Controls.SetChildIndex(Me.GBCompras, 0)
        Me.Controls.SetChildIndex(Me.GBVentas, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        CType(Me.DT_Inicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DT_Final.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodArticulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridCompras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsMovimientoArticulos1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewCompras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GBCompras.ResumeLayout(False)
        Me.GBVentas.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Load"
    Private Sub MovimientoArticulos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            DT_Inicio.DateTime = Now.Date
            DT_Final.DateTime = Now.Date

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Funciones"
    Private Sub MostrarVentas()
        Try
            If txtCodigo.Text = "" Or txtCodigo.Text = "0" Then
                MsgBox("Debe seleccionar un art�culo!!", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            DsMovimientoArticulos1.MovimientoArticuloVentas.Clear()

            Dim cnnv As SqlConnection = Nothing
            Dim dt As New DataTable
            '
            ' Dentro de un Try/Catch por si se produce un error
            '''''''''Cotizacion''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            cnnv = New SqlConnection(sConn)
            cnnv.Open()
            ' Creamos el comando para la consulta
            Dim cmdv As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM MovimientoArticuloVentas WHERE (Codigo = @Codigo) AND Fecha BETWEEN '" & DT_Inicio.DateTime & "' AND '" & DT_Final.DateTime & "'"

            cmdv.CommandText = sel
            cmdv.Connection = cnnv
            cmdv.CommandType = CommandType.Text
            cmdv.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmdv.Parameters.Add(New SqlParameter("@Codigo", SqlDbType.BigInt))

            cmdv.Parameters("@Codigo").Value = txtCodArticulo.Text

            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim dv As New SqlDataAdapter
            dv.SelectCommand = cmdv
            ' Llenamos la tabla
            dv.Fill(DsMovimientoArticulos1, "MovimientoArticuloVentas")

            Me.TotalArticulosVendidos = 0
            For Each fila As DataRow In Me.DsMovimientoArticulos1.MovimientoArticuloVentas.Rows
                Me.TotalArticulosVendidos = Me.TotalArticulosVendidos + fila.Item("Cantidad")
            Next
            Me.lblVendidos.Text = CDec(Me.TotalArticulosVendidos)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub MostrarCompras()
        Try
            If txtCodigo.Text = "" Or txtCodigo.Text = "0" Then
                MsgBox("Debe seleccionar un art�culo!!", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            DsMovimientoArticulos1.MovimientoArticulosCompra.Clear()

            Dim cnnv As SqlConnection = Nothing
            Dim dt As New DataTable
            '
            ' Dentro de un Try/Catch por si se produce un error
            '''''''''Cotizacion''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Obtenemos la cadena de conexi�n adecuada
            Dim sConn As String = GetSetting("SeeSOFT", "SeePos", "CONEXION")
            cnnv = New SqlConnection(sConn)
            cnnv.Open()
            ' Creamos el comando para la consulta
            Dim cmdv As SqlCommand = New SqlCommand
            Dim sel As String = "SELECT * FROM MovimientoArticulosCompra WHERE (Codigo = @Codigo) AND Fecha BETWEEN '" & DT_Inicio.DateTime & "' AND '" & DT_Final.DateTime & "'"

            cmdv.CommandText = sel
            cmdv.Connection = cnnv
            cmdv.CommandType = CommandType.Text
            cmdv.CommandTimeout = 90
            ' Los par�metros usados en la cadena de la consulta 
            cmdv.Parameters.Add(New SqlParameter("@Codigo", SqlDbType.BigInt))

            cmdv.Parameters("@Codigo").Value = txtCodArticulo.Text

            ' Creamos el dataAdapter y asignamos el comando de selecci�n
            Dim dv As New SqlDataAdapter
            dv.SelectCommand = cmdv
            ' Llenamos la tabla
            dv.Fill(DsMovimientoArticulos1, "MovimientoArticulosCompra")


            Me.TotalArticulosComprados = 0
            For Each fila As DataRow In Me.DsMovimientoArticulos1.MovimientoArticulosCompra.Rows
                Me.TotalArticulosComprados = Me.TotalArticulosComprados + fila.Item("Cantidad")
            Next
            lblComprados.Text = CDec(Me.TotalArticulosComprados)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private TotalArticulosComprados As Decimal = 0
    Private TotalArticulosVendidos As Decimal = 0

    Private Sub consultar()
        Try
            Dim BuscarArticulo As New FrmBuscarArticulo
            BuscarArticulo.ShowDialog()

            If BuscarArticulo.Cancelado Then
                txtCodigo.Focus()
            Else
                CargarInformacionArticulo(BuscarArticulo.Codigo)
                DsMovimientoArticulos1.MovimientoArticulosCompra.Clear()
                DsMovimientoArticulos1.MovimientoArticuloVentas.Clear()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function CargarInformacionArticulo(ByVal codigo As String) As Boolean
        Dim rs As SqlDataReader
        Dim Encontrado As Boolean
        Try
            If codigo <> Nothing Then
                sqlConexion = cConexion.Conectar
                rs = cConexion.GetRecorset(sqlConexion, "SELECT Codigo, dbo.Inventario.Descripcion + '( ' + Cast(dbo.Inventario.PresentaCant AS VARCHAR) + ' ' + dbo.Presentaciones.Presentaciones + ') ' AS Descripcion  from Inventario INNER JOIN Presentaciones ON Presentaciones.CodPres = Inventario.CodPresentacion WHERE (Inhabilitado = 0) and Codigo ='" & codigo & "' or Barras = '" & codigo & "'")
                Encontrado = False

                While rs.Read
                    Try
                        Encontrado = True
                        txtCodigo.Text = rs("Codigo")
                        txtCodArticulo.Text = rs("Codigo")
                        txtDescripcion.Text = rs("Descripcion")

                    Catch ex As Exception
                        MsgBox(ex.Message)
                        cConexion.DesConectar(cConexion.sQlconexion)
                    End Try
                End While
                If rs.IsClosed = False Then rs.Close()

                If Not Encontrado Then
                    MsgBox("No existe este Articulo! O esta deshabilitado", MsgBoxStyle.Exclamation)
                    cConexion.DesConectar(cConexion.sQlconexion)
                    Return False
                End If

                Return True
            End If

        Catch ex As Exception
            If rs.IsClosed = False Then rs.Close()
            MsgBox(ex.Message)
            Return False
        Finally
            cConexion.DesConectar(cConexion.sQlconexion)
        End Try
    End Function
#End Region

#Region "Funciones Controles"
    Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown
        If e.KeyCode = Windows.Forms.Keys.F1 Then
            consultar()
        End If

        If e.KeyCode = Windows.Forms.Keys.Enter Then
            If CargarInformacionArticulo(txtCodigo.Text) Then
                DsMovimientoArticulos1.MovimientoArticulosCompra.Clear()
                DsMovimientoArticulos1.MovimientoArticuloVentas.Clear()
                DT_Inicio.Focus()
            End If
        End If
    End Sub

    Private Sub DT_Inicio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DT_Inicio.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            DT_Final.Focus()
        End If
    End Sub

    Private Sub DT_Final_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DT_Final.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            BCompras.Focus()
        End If
    End Sub

    Private Sub BVentas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BVentas.Click
        MostrarVentas()
    End Sub

    Private Sub BVentas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles BVentas.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            MostrarVentas()
        End If
    End Sub

    Private Sub BCompras_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCompras.Click
        MostrarCompras()
    End Sub

    Private Sub BCompras_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles BCompras.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            MostrarCompras()
        End If
    End Sub
#End Region

End Class

