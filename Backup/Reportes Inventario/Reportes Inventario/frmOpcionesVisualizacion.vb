Imports System.Data.SqlClient
Imports system.Data
Imports System.Windows.Forms

Public Class frmOpcionesVisualizacion
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents cbxUbicacion As System.Windows.Forms.ComboBox
    Friend WithEvents cbxProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents cbxFamilia As System.Windows.Forms.ComboBox
    Friend WithEvents rptViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ButtonMostrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbxCondicion As System.Windows.Forms.ComboBox
    Friend WithEvents txtCantidad As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbxMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents LbTipoCambio As System.Windows.Forms.Label
    Friend WithEvents SqlDataAdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataSetReporteInventario As DataSetReporteInventario
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents lblgeneral As System.Windows.Forms.Label
    Friend WithEvents FechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblhasta As System.Windows.Forms.Label
    Friend WithEvents lbldesde As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents codigo As System.Windows.Forms.ComboBox
    Friend WithEvents AdapterProveedor As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents AdapterBodegas As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Cb_Bodegas As System.Windows.Forms.ComboBox
    Friend WithEvents lbodega As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents LProveedor As System.Windows.Forms.Label
    Friend WithEvents CB_Proveedor As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CBAgrupado As System.Windows.Forms.ComboBox
    Friend WithEvents LAgrupado As System.Windows.Forms.Label
    Friend WithEvents CBOrdenar As System.Windows.Forms.ComboBox
    Friend WithEvents LOrdenado As System.Windows.Forms.Label
    Friend WithEvents CbExistencia As System.Windows.Forms.CheckBox
    Friend WithEvents CBTipoPrecio As System.Windows.Forms.ComboBox
    Friend WithEvents LTipoPrecio As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOpcionesVisualizacion))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.CBTipoPrecio = New System.Windows.Forms.ComboBox
        Me.LTipoPrecio = New System.Windows.Forms.Label
        Me.CbExistencia = New System.Windows.Forms.CheckBox
        Me.CBAgrupado = New System.Windows.Forms.ComboBox
        Me.LAgrupado = New System.Windows.Forms.Label
        Me.CBOrdenar = New System.Windows.Forms.ComboBox
        Me.LOrdenado = New System.Windows.Forms.Label
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Cb_Bodegas = New System.Windows.Forms.ComboBox
        Me.DataSetReporteInventario = New LcPymes_5._2.DataSetReporteInventario
        Me.lbodega = New System.Windows.Forms.Label
        Me.LProveedor = New System.Windows.Forms.Label
        Me.CB_Proveedor = New System.Windows.Forms.ComboBox
        Me.codigo = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblMoneda = New System.Windows.Forms.Label
        Me.cbxMoneda = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.ButtonMostrar = New DevExpress.XtraEditors.SimpleButton
        Me.lblhasta = New System.Windows.Forms.Label
        Me.lbldesde = New System.Windows.Forms.Label
        Me.lblgeneral = New System.Windows.Forms.Label
        Me.cbxProveedor = New System.Windows.Forms.ComboBox
        Me.txtDescripcion = New System.Windows.Forms.TextBox
        Me.cbxFamilia = New System.Windows.Forms.ComboBox
        Me.cbxUbicacion = New System.Windows.Forms.ComboBox
        Me.FechaFinal = New System.Windows.Forms.DateTimePicker
        Me.FechaInicio = New System.Windows.Forms.DateTimePicker
        Me.txtCantidad = New System.Windows.Forms.NumericUpDown
        Me.cbxCondicion = New System.Windows.Forms.ComboBox
        Me.LbTipoCambio = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.rptViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapterMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.AdapterProveedor = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.AdapterBodegas = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataSetReporteInventario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.CBTipoPrecio)
        Me.GroupBox1.Controls.Add(Me.LTipoPrecio)
        Me.GroupBox1.Controls.Add(Me.CbExistencia)
        Me.GroupBox1.Controls.Add(Me.CBAgrupado)
        Me.GroupBox1.Controls.Add(Me.LAgrupado)
        Me.GroupBox1.Controls.Add(Me.CBOrdenar)
        Me.GroupBox1.Controls.Add(Me.LOrdenado)
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Controls.Add(Me.Cb_Bodegas)
        Me.GroupBox1.Controls.Add(Me.lbodega)
        Me.GroupBox1.Controls.Add(Me.LProveedor)
        Me.GroupBox1.Controls.Add(Me.CB_Proveedor)
        Me.GroupBox1.Controls.Add(Me.codigo)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblMoneda)
        Me.GroupBox1.Controls.Add(Me.cbxMoneda)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.ButtonMostrar)
        Me.GroupBox1.Controls.Add(Me.lblhasta)
        Me.GroupBox1.Controls.Add(Me.lbldesde)
        Me.GroupBox1.Controls.Add(Me.lblgeneral)
        Me.GroupBox1.Controls.Add(Me.cbxProveedor)
        Me.GroupBox1.Controls.Add(Me.txtDescripcion)
        Me.GroupBox1.Controls.Add(Me.cbxFamilia)
        Me.GroupBox1.Controls.Add(Me.cbxUbicacion)
        Me.GroupBox1.Controls.Add(Me.FechaFinal)
        Me.GroupBox1.Controls.Add(Me.FechaInicio)
        Me.GroupBox1.Controls.Add(Me.txtCantidad)
        Me.GroupBox1.Controls.Add(Me.cbxCondicion)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1232, 80)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'CBTipoPrecio
        '
        Me.CBTipoPrecio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBTipoPrecio.Items.AddRange(New Object() {"Precio A", "Precio B", "Precio C", "Precio D"})
        Me.CBTipoPrecio.Location = New System.Drawing.Point(1072, 24)
        Me.CBTipoPrecio.Name = "CBTipoPrecio"
        Me.CBTipoPrecio.Size = New System.Drawing.Size(80, 21)
        Me.CBTipoPrecio.TabIndex = 77
        Me.CBTipoPrecio.Visible = False
        '
        'LTipoPrecio
        '
        Me.LTipoPrecio.BackColor = System.Drawing.Color.Transparent
        Me.LTipoPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTipoPrecio.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LTipoPrecio.Location = New System.Drawing.Point(1064, 8)
        Me.LTipoPrecio.Name = "LTipoPrecio"
        Me.LTipoPrecio.Size = New System.Drawing.Size(88, 16)
        Me.LTipoPrecio.TabIndex = 76
        Me.LTipoPrecio.Text = "Tipo Precio"
        Me.LTipoPrecio.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.LTipoPrecio.Visible = False
        '
        'CbExistencia
        '
        Me.CbExistencia.ForeColor = System.Drawing.Color.MediumBlue
        Me.CbExistencia.Location = New System.Drawing.Point(480, 24)
        Me.CbExistencia.Name = "CbExistencia"
        Me.CbExistencia.Size = New System.Drawing.Size(80, 16)
        Me.CbExistencia.TabIndex = 75
        Me.CbExistencia.Text = "Existencia"
        Me.CbExistencia.Visible = False
        '
        'CBAgrupado
        '
        Me.CBAgrupado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBAgrupado.Items.AddRange(New Object() {"Ubicacion", "Familia", "NO"})
        Me.CBAgrupado.Location = New System.Drawing.Point(688, 24)
        Me.CBAgrupado.Name = "CBAgrupado"
        Me.CBAgrupado.Size = New System.Drawing.Size(96, 21)
        Me.CBAgrupado.TabIndex = 74
        Me.CBAgrupado.Visible = False
        '
        'LAgrupado
        '
        Me.LAgrupado.BackColor = System.Drawing.Color.Transparent
        Me.LAgrupado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LAgrupado.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LAgrupado.Location = New System.Drawing.Point(688, 8)
        Me.LAgrupado.Name = "LAgrupado"
        Me.LAgrupado.Size = New System.Drawing.Size(96, 16)
        Me.LAgrupado.TabIndex = 73
        Me.LAgrupado.Text = "Agrupado por:"
        Me.LAgrupado.Visible = False
        '
        'CBOrdenar
        '
        Me.CBOrdenar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBOrdenar.Items.AddRange(New Object() {"Descripcion", "Codigo", "Barras"})
        Me.CBOrdenar.Location = New System.Drawing.Point(576, 24)
        Me.CBOrdenar.Name = "CBOrdenar"
        Me.CBOrdenar.Size = New System.Drawing.Size(96, 21)
        Me.CBOrdenar.TabIndex = 72
        Me.CBOrdenar.Visible = False
        '
        'LOrdenado
        '
        Me.LOrdenado.BackColor = System.Drawing.Color.Transparent
        Me.LOrdenado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LOrdenado.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LOrdenado.Location = New System.Drawing.Point(576, 8)
        Me.LOrdenado.Name = "LOrdenado"
        Me.LOrdenado.Size = New System.Drawing.Size(96, 16)
        Me.LOrdenado.TabIndex = 71
        Me.LOrdenado.Text = "Ordenado por:"
        Me.LOrdenado.Visible = False
        '
        'CheckBox1
        '
        Me.CheckBox1.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(163, 8)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(80, 16)
        Me.CheckBox1.TabIndex = 69
        Me.CheckBox1.Text = "Generales"
        '
        'Cb_Bodegas
        '
        Me.Cb_Bodegas.DataSource = Me.DataSetReporteInventario.Bodegas
        Me.Cb_Bodegas.DisplayMember = "Nombre_Bodega"
        Me.Cb_Bodegas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cb_Bodegas.ForeColor = System.Drawing.Color.Blue
        Me.Cb_Bodegas.Location = New System.Drawing.Point(224, 24)
        Me.Cb_Bodegas.Name = "Cb_Bodegas"
        Me.Cb_Bodegas.Size = New System.Drawing.Size(144, 21)
        Me.Cb_Bodegas.TabIndex = 65
        Me.Cb_Bodegas.ValueMember = "ID_Bodega"
        '
        'DataSetReporteInventario
        '
        Me.DataSetReporteInventario.DataSetName = "DataSetReporteInventario"
        Me.DataSetReporteInventario.Locale = New System.Globalization.CultureInfo("")
        '
        'lbodega
        '
        Me.lbodega.BackColor = System.Drawing.Color.Transparent
        Me.lbodega.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbodega.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbodega.Location = New System.Drawing.Point(240, 8)
        Me.lbodega.Name = "lbodega"
        Me.lbodega.Size = New System.Drawing.Size(120, 16)
        Me.lbodega.TabIndex = 66
        Me.lbodega.Text = "Bodegas"
        Me.lbodega.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LProveedor
        '
        Me.LProveedor.BackColor = System.Drawing.Color.Transparent
        Me.LProveedor.ForeColor = System.Drawing.Color.RoyalBlue
        Me.LProveedor.Location = New System.Drawing.Point(16, 48)
        Me.LProveedor.Name = "LProveedor"
        Me.LProveedor.Size = New System.Drawing.Size(96, 16)
        Me.LProveedor.TabIndex = 68
        Me.LProveedor.Text = "Proveedor:"
        Me.LProveedor.Visible = False
        '
        'CB_Proveedor
        '
        Me.CB_Proveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Proveedor.DropDownWidth = 80
        Me.CB_Proveedor.ForeColor = System.Drawing.Color.Blue
        Me.CB_Proveedor.Location = New System.Drawing.Point(112, 48)
        Me.CB_Proveedor.Name = "CB_Proveedor"
        Me.CB_Proveedor.Size = New System.Drawing.Size(280, 21)
        Me.CB_Proveedor.TabIndex = 67
        Me.CB_Proveedor.Visible = False
        '
        'codigo
        '
        Me.codigo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.codigo.Items.AddRange(New Object() {"Id", "Barras"})
        Me.codigo.Location = New System.Drawing.Point(880, 24)
        Me.codigo.Name = "codigo"
        Me.codigo.Size = New System.Drawing.Size(64, 21)
        Me.codigo.TabIndex = 64
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.Location = New System.Drawing.Point(873, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 63
        Me.Label2.Text = "Codigo"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblMoneda
        '
        Me.lblMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoneda.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblMoneda.Location = New System.Drawing.Point(800, 8)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(72, 16)
        Me.lblMoneda.TabIndex = 62
        Me.lblMoneda.Text = "Moneda"
        Me.lblMoneda.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblMoneda.Visible = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.DataSource = Me.DataSetReporteInventario
        Me.cbxMoneda.DisplayMember = "Moneda.MonedaNombre"
        Me.cbxMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxMoneda.Location = New System.Drawing.Point(792, 24)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.Size = New System.Drawing.Size(80, 21)
        Me.cbxMoneda.TabIndex = 21
        Me.cbxMoneda.Visible = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Location = New System.Drawing.Point(5, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(153, 16)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Ver Reporte Seg�n"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"Articulos con Existencia en..", "Articulos Por Descripci�n", "Articulos Por Familia", "Articulos Por Ubicaci�n", "Articulos Por Exist. Max.", "Articulos Por Exist. Min.", "Articulos Por Proveedor", "Toma F�sica de Inventario", "Toma F�sca Por Ubicaci�n", "Toma F�sica Por Categor�a", "Art�culos con servicios", "Art�culos por Pedir", "Art�culos por Pedir por Proveedor", "Listado de Precios", "Articulos Vendidos", "Articulos Vendidos por Proveedor", "Articulos en Apartado", "Costo de Art�culos"})
        Me.ComboBox1.Location = New System.Drawing.Point(16, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(168, 21)
        Me.ComboBox1.TabIndex = 17
        '
        'ButtonMostrar
        '
        Me.ButtonMostrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonMostrar.Location = New System.Drawing.Point(1160, 24)
        Me.ButtonMostrar.Name = "ButtonMostrar"
        Me.ButtonMostrar.Size = New System.Drawing.Size(64, 24)
        Me.ButtonMostrar.TabIndex = 20
        Me.ButtonMostrar.Text = "Mostrar"
        '
        'lblhasta
        '
        Me.lblhasta.BackColor = System.Drawing.Color.Transparent
        Me.lblhasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblhasta.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblhasta.Location = New System.Drawing.Point(592, 8)
        Me.lblhasta.Name = "lblhasta"
        Me.lblhasta.Size = New System.Drawing.Size(96, 16)
        Me.lblhasta.TabIndex = 61
        Me.lblhasta.Text = "Hasta"
        Me.lblhasta.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblhasta.Visible = False
        '
        'lbldesde
        '
        Me.lbldesde.BackColor = System.Drawing.Color.Transparent
        Me.lbldesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldesde.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbldesde.Location = New System.Drawing.Point(416, 8)
        Me.lbldesde.Name = "lbldesde"
        Me.lbldesde.Size = New System.Drawing.Size(96, 16)
        Me.lbldesde.TabIndex = 60
        Me.lbldesde.Text = "Desde"
        Me.lbldesde.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lbldesde.Visible = False
        '
        'lblgeneral
        '
        Me.lblgeneral.BackColor = System.Drawing.Color.Transparent
        Me.lblgeneral.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblgeneral.Location = New System.Drawing.Point(368, 8)
        Me.lblgeneral.Name = "lblgeneral"
        Me.lblgeneral.Size = New System.Drawing.Size(248, 16)
        Me.lblgeneral.TabIndex = 22
        Me.lblgeneral.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblgeneral.Visible = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.DataSource = Me.DataSetReporteInventario.Proveedores
        Me.cbxProveedor.DisplayMember = "Nombre"
        Me.cbxProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.ForeColor = System.Drawing.Color.Blue
        Me.cbxProveedor.Location = New System.Drawing.Point(392, 24)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.Size = New System.Drawing.Size(152, 21)
        Me.cbxProveedor.TabIndex = 7
        Me.cbxProveedor.ValueMember = "CodigoProv"
        Me.cbxProveedor.Visible = False
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.ForeColor = System.Drawing.Color.Blue
        Me.txtDescripcion.Location = New System.Drawing.Point(408, 24)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(136, 20)
        Me.txtDescripcion.TabIndex = 14
        Me.txtDescripcion.Text = ""
        Me.txtDescripcion.Visible = False
        '
        'cbxFamilia
        '
        Me.cbxFamilia.DisplayMember = "SubFamilia"
        Me.cbxFamilia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxFamilia.Enabled = False
        Me.cbxFamilia.ForeColor = System.Drawing.Color.Blue
        Me.cbxFamilia.Location = New System.Drawing.Point(392, 24)
        Me.cbxFamilia.Name = "cbxFamilia"
        Me.cbxFamilia.Size = New System.Drawing.Size(152, 21)
        Me.cbxFamilia.TabIndex = 9
        Me.cbxFamilia.Visible = False
        '
        'cbxUbicacion
        '
        Me.cbxUbicacion.DisplayMember = "Ubicacion"
        Me.cbxUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxUbicacion.Enabled = False
        Me.cbxUbicacion.ForeColor = System.Drawing.Color.Blue
        Me.cbxUbicacion.Location = New System.Drawing.Point(392, 24)
        Me.cbxUbicacion.Name = "cbxUbicacion"
        Me.cbxUbicacion.Size = New System.Drawing.Size(152, 21)
        Me.cbxUbicacion.TabIndex = 8
        Me.cbxUbicacion.Visible = False
        '
        'FechaFinal
        '
        Me.FechaFinal.CustomFormat = "dd/MM/yyyy hh:mm:ss tt"
        Me.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaFinal.Location = New System.Drawing.Point(552, 24)
        Me.FechaFinal.Name = "FechaFinal"
        Me.FechaFinal.Size = New System.Drawing.Size(152, 20)
        Me.FechaFinal.TabIndex = 59
        Me.FechaFinal.Value = New Date(2006, 4, 19, 0, 0, 0, 0)
        Me.FechaFinal.Visible = False
        '
        'FechaInicio
        '
        Me.FechaInicio.CustomFormat = "dd/MM/yyyy hh:mm:ss tt"
        Me.FechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaInicio.Location = New System.Drawing.Point(408, 24)
        Me.FechaInicio.Name = "FechaInicio"
        Me.FechaInicio.Size = New System.Drawing.Size(160, 20)
        Me.FechaInicio.TabIndex = 58
        Me.FechaInicio.Value = New Date(2006, 4, 10, 0, 0, 0, 0)
        Me.FechaInicio.Visible = False
        '
        'txtCantidad
        '
        Me.txtCantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidad.Location = New System.Drawing.Point(472, 24)
        Me.txtCantidad.Minimum = New Decimal(New Integer() {10000, 0, 0, -2147483648})
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(72, 20)
        Me.txtCantidad.TabIndex = 16
        Me.txtCantidad.Value = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtCantidad.Visible = False
        '
        'cbxCondicion
        '
        Me.cbxCondicion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCondicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCondicion.ForeColor = System.Drawing.Color.Blue
        Me.cbxCondicion.ItemHeight = 13
        Me.cbxCondicion.Location = New System.Drawing.Point(408, 24)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.Size = New System.Drawing.Size(48, 21)
        Me.cbxCondicion.TabIndex = 10
        Me.cbxCondicion.Visible = False
        '
        'LbTipoCambio
        '
        Me.LbTipoCambio.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetReporteInventario, "Moneda.ValorCompra"))
        Me.LbTipoCambio.Location = New System.Drawing.Point(616, 104)
        Me.LbTipoCambio.Name = "LbTipoCambio"
        Me.LbTipoCambio.TabIndex = 22
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'rptViewer
        '
        Me.rptViewer.ActiveViewIndex = -1
        Me.rptViewer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rptViewer.AutoScroll = True
        Me.rptViewer.BackColor = System.Drawing.SystemColors.Control
        Me.rptViewer.DisplayGroupTree = False
        Me.rptViewer.Location = New System.Drawing.Point(0, 88)
        Me.rptViewer.Name = "rptViewer"
        Me.rptViewer.ReportSource = Nothing
        Me.rptViewer.ShowCloseButton = False
        Me.rptViewer.Size = New System.Drawing.Size(1232, 520)
        Me.rptViewer.TabIndex = 73
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'SqlDataAdapterMoneda
        '
        Me.SqlDataAdapterMoneda.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT MonedaNombre, ValorCompra, CodMoneda FROM Moneda"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        '
        'AdapterProveedor
        '
        Me.AdapterProveedor.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterProveedor.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Proveedores", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodigoProv", "CodigoProv"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT CodigoProv, Nombre FROM Proveedores"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'AdapterBodegas
        '
        Me.AdapterBodegas.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterBodegas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Bodegas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ID_Bodega", "ID_Bodega"), New System.Data.Common.DataColumnMapping("Nombre_Bodega", "Nombre_Bodega")})})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT ID_Bodega, Nombre_Bodega FROM Bodegas ORDER BY Nombre_Bodega"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        '
        'frmOpcionesVisualizacion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(1232, 610)
        Me.Controls.Add(Me.rptViewer)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.LbTipoCambio)
        Me.Name = "frmOpcionesVisualizacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reportes de Inventario"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataSetReporteInventario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "variables"
    Private cConexion As Conexion
    Private sqlConexion As SqlConnection
    Dim TomaFisica As New CrystalDecisions.CrystalReports.Engine.ReportDocument
#End Region

#Region "Load"
    Private Sub frmOpcionesVisualizacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SqlConnection.ConnectionString = GetSetting("SeeSOFT", "SeePos", "Conexion")
        cConexion = New Conexion
        sqlConexion = cConexion.Conectar
        fulldata()
        SqlDataAdapterMoneda.Fill(Me.DataSetReporteInventario, "Moneda")
        AdapterBodegas.Fill(Me.DataSetReporteInventario, "Bodegas")
        FechaInicio.Value = Now
        FechaFinal.Value = Now
        codigo.SelectedIndex = 0
        CBOrdenar.SelectedIndex = 0
        CBAgrupado.SelectedIndex = 0
        CBTipoPrecio.SelectedIndex = 0
    End Sub

    Private Sub fulldata()
        'Condiciones
        Me.cbxCondicion.Items.Add("<")
        Me.cbxCondicion.Items.Add(">")
        Me.cbxCondicion.Items.Add("<=")
        Me.cbxCondicion.Items.Add(">=")
        Me.cbxCondicion.Items.Add("<>")
        Me.cbxCondicion.Items.Add("=")
        'Llena moneda
    End Sub
#End Region

#Region "Mostrar"
    Private Sub ButtonMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonMostrar.Click
        ' Dim Reporte As New ConsultaInventario
        Dim Reporte As New ConsultaInventario2
        Dim SQLConexion As New Conexion
        Dim EnEspera As New DevExpress.Utils.WaitDialogForm

        Try
            SQLConexion.SQLStringConexion = Me.SqlConnection.ConnectionString
            SQLConexion.Conectar()
            Reporte.RecordSelectionFormula = ">"
            EnEspera.Caption = ComboBox1.Text
            EnEspera.Text = "Reporte de Inventario"
            EnEspera.Show()

            Application.DoEvents()
            Select Case Me.ComboBox1.SelectedIndex
                Case 0
                    If CheckBox1.Checked = True Then
                        'If Me.cbxCondicion.Text <> "" Then Reporte.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {Inventario.Existencia} " & Me.cbxCondicion.Text & " " & txtCantidad.Text
                        'Reporte.SetParameterValue(0, "Reporte de Art�culos por Existencia " & Me.cbxCondicion.Text & "" & Me.txtCantidad.Text & " Art�culos")
                        'Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                        'Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                        'Reporte.SetParameterValue(3, Me.codigo.Text) 'para codigos de barra o id
                        'Reporte.SetParameterValue(4, FechaFinal.Value)
                        'CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)
                        If Me.cbxCondicion.Text <> "" Then
                            Reporte.RecordSelectionFormula = "not {ConsultaInventario.Consignacion} and {ConsultaInventario.servicio} = false and {ConsultaInventario.Inhabilitado} = False and {ConsultaInventario.Existencia} " & Me.cbxCondicion.Text & " " & txtCantidad.Text
                        Else
                            Reporte.RecordSelectionFormula = "not {ConsultaInventario.Consignacion} and {ConsultaInventario.servicio} = false and {ConsultaInventario.Inhabilitado} = False "
                            'and {ConsultaInventario.Existencia} " & Me.cbxCondicion.Text & " " & txtCantidad.Text
                        End If

                        Reporte.SetParameterValue(0, "Reporte de Art�culos por Existencia " & Me.cbxCondicion.Text & "" & Me.txtCantidad.Text & " Art�culos")
                        Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                        Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                        Reporte.SetParameterValue(3, Me.codigo.Text) 'para codigos de barra o id

                        CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)

                    Else
                        Dim Reporte1 As New exsitenciaxbodega
                        If Me.cbxCondicion.Text = "" Then
                            Reporte1.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and  {ArticulosXBodega.IdBodega}=" & Cb_Bodegas.SelectedValue   '{ArticulosXBodega.Existencia} " & Me.cbxCondicion.Text & " " & txtCantidad.Text & " and
                            Reporte1.SetParameterValue(0, "Reporte de Art�culos por Existencia en la Bodega " & Cb_Bodegas.Text & " " & Me.cbxCondicion.Text & "" & Me.txtCantidad.Text & " Art�culos")
                            Reporte1.SetParameterValue(1, Me.cbxMoneda.Text)
                            Reporte1.SetParameterValue(2, Me.LbTipoCambio.Text)
                            Reporte1.SetParameterValue(3, Me.codigo.Text) 'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte1)
                        Else
                            If Me.cbxCondicion.Text <> "" Then Reporte1.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {ArticulosXBodega.Existencia} " & Me.cbxCondicion.Text & " " & txtCantidad.Text & " and {ArticulosXBodega.IdBodega}=" & Cb_Bodegas.SelectedValue
                            Reporte1.SetParameterValue(0, "Reporte de Art�culos por Existencia en la Bodega " & Cb_Bodegas.Text & " " & Me.cbxCondicion.Text & "" & Me.txtCantidad.Text & " Art�culos")
                            Reporte1.SetParameterValue(1, Me.cbxMoneda.Text)
                            Reporte1.SetParameterValue(2, Me.LbTipoCambio.Text)
                            Reporte1.SetParameterValue(3, Me.codigo.Text) 'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte1)

                        End If
                    End If
                Case 1
                        If CheckBox1.Checked = True Then
                            'If Me.txtDescripcion.Text <> "" Then Reporte.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {Inventario.Descripcion} like '*" & Me.txtDescripcion.Text & "*'"
                            'Reporte.SetParameterValue(0, "Reporte de Art�culos por Descripci�n " & Me.txtDescripcion.Text & "")
                            'Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            'Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            'Reporte.SetParameterValue(3, Me.codigo.Text) 'para codigos de barra o id
                            'Reporte.SetParameterValue(4, FechaFinal.Value)
                            'CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)
                            If Me.txtDescripcion.Text <> "" Then Reporte.RecordSelectionFormula = "{ConsultaInventario.Consignacion} = false and {ConsultaInventario.servicio} = false and {ConsultaInventario.Inhabilitado} = False and {ConsultaInventario.Descripcion} like '*" & Me.txtDescripcion.Text & "*'"
                            Reporte.SetParameterValue(0, "Reporte de Art�culos por Descripci�n " & Me.txtDescripcion.Text & "")
                            Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)


                        Else
                            Dim reportdescripcionbodegas As New artculosxdescripcionbodega
                            If Me.txtDescripcion.Text <> "" Then reportdescripcionbodegas.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {Inventario.Descripcion} like '*" & Me.txtDescripcion.Text & "*' and {ArticulosXBodega.IdBodega}=" & Cb_Bodegas.SelectedValue
                            reportdescripcionbodegas.SetParameterValue(0, "Reporte de Art�culos por Descripci�n en la Bodega " & Cb_Bodegas.Text & " " & Me.txtDescripcion.Text & "  En La Bodega ""& Cb_Bodegas.Text ")
                            reportdescripcionbodegas.SetParameterValue(1, Me.cbxMoneda.Text)
                            reportdescripcionbodegas.SetParameterValue(2, Me.LbTipoCambio.Text)
                            reportdescripcionbodegas.SetParameterValue(3, Me.codigo.Text) 'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, reportdescripcionbodegas)
                        End If

                Case 2
                        If CheckBox1.Checked = True Then
                            'If Me.cbxFamilia.Text <> "" Then Reporte.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {Familia.Descripcion}= '" & Me.cbxFamilia.Text & "'"
                            'Reporte.SetParameterValue(0, "Reporte de Art�culos por Familia " & Me.cbxFamilia.Text & "")
                            'Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            'Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            'Reporte.SetParameterValue(3, Me.codigo.Text)    'para codigos de barra o id
                            'Reporte.SetParameterValue(4, FechaFinal.Value)
                            'CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)
                            If Me.cbxFamilia.Text <> "" Then Reporte.RecordSelectionFormula = "{ConsultaInventario.Consignacion} = false and {ConsultaInventario.servicio} = false and {ConsultaInventario.Inhabilitado} = False and {ConsultaInventario.Familia}= '" & Me.cbxFamilia.Text & "'"
                            Reporte.SetParameterValue(0, "Reporte de Art�culos por Familia " & Me.cbxFamilia.Text & "")
                            Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)
                        Else
                            Dim reporarticulosxfamilia As New articulosxfamiliabodegas
                            If Me.cbxFamilia.Text <> "" Then reporarticulosxfamilia.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {Familia.Descripcion}= '" & Me.cbxFamilia.Text & "' and {ArticulosXBodega.IdBodega}=" & Cb_Bodegas.SelectedValue
                            reporarticulosxfamilia.SetParameterValue(0, "Reporte de Art�culos por Familia en la Bodega  " & Cb_Bodegas.Text & " " & Me.cbxFamilia.Text & "")
                            reporarticulosxfamilia.SetParameterValue(1, Me.cbxMoneda.Text)
                            reporarticulosxfamilia.SetParameterValue(2, Me.LbTipoCambio.Text)
                            reporarticulosxfamilia.SetParameterValue(3, Me.codigo.Text)    'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, reporarticulosxfamilia)
                        End If

                Case 3
                        If CheckBox1.Checked = True Then
                            'If Me.cbxUbicacion.Text <> "" Then Reporte.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {ubicaciones.Descripcion} LIKE '*" & Me.cbxUbicacion.Text & "*'"
                            'Reporte.SetParameterValue(0, "Reporte de Art�culos por Ubicaci�n " & Me.cbxUbicacion.Text)
                            'Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            'Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            'Reporte.SetParameterValue(3, Me.codigo.Text)    'para codigos de barra o id
                            'Reporte.SetParameterValue(4, FechaFinal.Value)
                            'CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)
                            If Me.cbxUbicacion.Text <> "" Then Reporte.RecordSelectionFormula = "{ConsultaInventario.Consignacion} = false and {ConsultaInventario.servicio} = false and {ConsultaInventario.Inhabilitado} = False and {ConsultaInventario.Ubicacion} LIKE '*" & Me.cbxUbicacion.Text & "*'"
                            Reporte.SetParameterValue(0, "Reporte de Art�culos por Ubicaci�n " & Me.cbxUbicacion.Text)
                            Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)

                        Else
                            Dim reporarticulosxubicacion As New articulosubicacionxbodega
                            If Me.cbxUbicacion.Text <> "" Then reporarticulosxubicacion.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {ubicaciones.Descripcion} LIKE '*" & Me.cbxUbicacion.Text & "*' and {ArticulosXBodega.IdBodega}=" & Cb_Bodegas.SelectedValue
                            reporarticulosxubicacion.SetParameterValue(0, "Reporte de Art�culos por Ubicaci�n " & Me.cbxUbicacion.Text & " en la Bodega " & Cb_Bodegas.Text & "")
                            reporarticulosxubicacion.SetParameterValue(1, Me.cbxMoneda.Text)
                            reporarticulosxubicacion.SetParameterValue(2, Me.LbTipoCambio.Text)
                            reporarticulosxubicacion.SetParameterValue(3, Me.codigo.Text)    'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, reporarticulosxubicacion)
                        End If

                Case 4
                        If CheckBox1.Checked = True Then
                            'Reporte.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {Inventario.maxima} > 0 and {Inventario.Existencia} => {Inventario.maxima} "
                            'Reporte.SetParameterValue(0, "Reporte de Art�culos con Existencias M�ximas")
                            'Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            'Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            'Reporte.SetParameterValue(3, Me.codigo.Text)    'para codigos de barra o id
                            'Reporte.SetParameterValue(4, FechaFinal.Value)
                            'CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)
                            Reporte.RecordSelectionFormula = "{ConsultaInventario.Consignacion} = false and {ConsultaInventario.servicio} = false and {ConsultaInventario.Inhabilitado} = False and {ConsultaInventario.maxima} > 0"
                            Reporte.SetParameterValue(0, "Reporte de Art�culos con Existencias M�ximas")
                            Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)
                        Else
                            Dim rptexistemaxima As New articulosexistenciamaximabodega
                            rptexistemaxima.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {Inventario.maxima} > 0 and {ArticulosXBodega.Existencia} => {Inventario.maxima} and {ArticulosXBodega.IdBodega}=" & Cb_Bodegas.SelectedValue
                            rptexistemaxima.SetParameterValue(0, "Reporte de Art�culos con Existencias M�ximas en la Bodega " & Cb_Bodegas.Text)
                            rptexistemaxima.SetParameterValue(1, Me.cbxMoneda.Text)
                            rptexistemaxima.SetParameterValue(2, Me.LbTipoCambio.Text)
                            rptexistemaxima.SetParameterValue(3, Me.codigo.Text)    'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, rptexistemaxima)
                        End If

                Case 5
                        If CheckBox1.Checked = True Then
                            'Reporte.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and {Inventario.minima} > 0 and {Inventario.Existencia} =< {Inventario.minima} "
                            'Reporte.SetParameterValue(0, "Reporte de Art�culos con Existencias M�nimas")
                            'Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            'Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            'Reporte.SetParameterValue(3, Me.codigo.Text)    'para codigos de barra o id
                            'Reporte.SetParameterValue(4, FechaFinal.Value)
                            'CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)
                            Reporte.RecordSelectionFormula = "{ConsultaInventario.Consignacion} = false and {ConsultaInventario.servicio} = false and {ConsultaInventario.Inhabilitado} = False and  {ConsultaInventario.minima} > 0"
                            Reporte.SetParameterValue(0, "Reporte de Art�culos con Existencias M�nimas")
                            Reporte.SetParameterValue(1, Me.cbxMoneda.Text)
                            Reporte.SetParameterValue(2, Me.LbTipoCambio.Text)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Reporte)

                        Else
                            Dim rptexisteminimas As New articulosexistenciaminimabodega
                            rptexisteminimas.RecordSelectionFormula = "{Inventario.servicio} = false and {Inventario.Inhabilitado} = False and  {Inventario.minima} > 0 and {ArticulosXBodega.Existencia} =< {Inventario.minima} and {ArticulosXBodega.IdBodega}=" & Cb_Bodegas.SelectedValue
                            rptexisteminimas.SetParameterValue(0, "Reporte de Art�culos con Existencias M�nimas")
                            rptexisteminimas.SetParameterValue(1, Me.cbxMoneda.Text)
                            rptexisteminimas.SetParameterValue(2, Me.LbTipoCambio.Text)
                            rptexisteminimas.SetParameterValue(3, Me.codigo.Text)    'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, rptexisteminimas)
                        End If

                Case 6
                        Dim ReporteXproveedor As New ConsultaInventarioXProveedor
                        ReporteXproveedor.SetParameterValue(2, "Reporte de Art�culos por Proveedor " & cbxProveedor.Text)
                        ReporteXproveedor.SetParameterValue(0, cbxMoneda.Text)
                        ReporteXproveedor.SetParameterValue(1, CDbl(Me.LbTipoCambio.Text))
                        ReporteXproveedor.SetParameterValue(3, codigo.Text)  'para codigos de barra o id
                        ReporteXproveedor.SetParameterValue(4, cbxProveedor.SelectedValue)
                        CrystalReportsConexion.LoadReportViewer(rptViewer, ReporteXproveedor)
                        'FALTA POR BODEGA

                Case 7
                        If CheckBox1.Checked = True Then
                            Dim Inventario_Toma As New Inventario_Toma_Fisica
                            Inventario_Toma.SetParameterValue(0, Me.codigo.Text) 'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Inventario_Toma)
                        Else
                            If CBAgrupado.SelectedIndex = 0 Then
                                Dim Inventario_Toma As New Inventario_Toma_Fisicaxbodega
                                TomaFisica = Inventario_Toma
                                TomaFisica.SetParameterValue(0, Me.codigo.Text) 'para codigos de barra o id
                                TomaFisica.SetParameterValue(1, Cb_Bodegas.SelectedValue)
                                TomaFisica.SetParameterValue(2, CbExistencia.Checked)


                            ElseIf CBAgrupado.SelectedIndex = 1 Then
                                Dim Inventario_Toma As New Inventario_Toma_Fisica_Familia
                                TomaFisica = Inventario_Toma
                                TomaFisica.SetParameterValue(0, CbExistencia.Checked)
                                TomaFisica.SetParameterValue(1, Cb_Bodegas.SelectedValue)
                            ElseIf CBAgrupado.SelectedIndex = 2 Then
                                If CBOrdenar.SelectedIndex = 1 Then
                                    Dim Inventario_Toma As New Inventario_Toma_Fisica_NoAgrupado_Codigo
                                    TomaFisica = Inventario_Toma
                                    TomaFisica.SetParameterValue(0, CbExistencia.Checked)
                                    TomaFisica.SetParameterValue(1, Cb_Bodegas.SelectedValue)
                                ElseIf CBOrdenar.SelectedIndex = 0 Then
                                    Dim Inventario_Toma As New Inventario_Toma_Fisica_NoAgrupado
                                    TomaFisica = Inventario_Toma
                                    TomaFisica.SetParameterValue(0, CbExistencia.Checked)
                                    TomaFisica.SetParameterValue(1, Cb_Bodegas.SelectedValue)
                                End If
                            End If

                            Dim Ordenamiento As CrystalDecisions.CrystalReports.Engine.DatabaseFieldDefinition
                            CrystalReportsConexion.LoadReportViewer(rptViewer, TomaFisica, False, SQLConexion.SQLStringConexion)

                            If (CBOrdenar.SelectedIndex = 1 Or CBOrdenar.SelectedIndex = 2) And CBAgrupado.SelectedIndex <> 2 Then
                                Ordenamiento = TomaFisica.Database.Tables.Item(0).Fields.Item(CBOrdenar.Text)
                                TomaFisica.DataDefinition.SortFields.Item(0).Field = Ordenamiento
                                TomaFisica.DataDefinition.SortFields(0).SortDirection = CrystalDecisions.[Shared].SortDirection.AscendingOrder
                            End If
                        End If

                Case 8
                        If CheckBox1.Checked = True Then
                            Dim TomaxUbicacion As New InventarioxUbicacion
                            TomaxUbicacion.SetParameterValue(0, Me.cbxUbicacion.Text)
                            TomaxUbicacion.SetParameterValue(1, Me.codigo.Text)     'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, TomaxUbicacion)
                        Else
                            Dim tomafiscaxubicacionbodega As New InventarioxUbicacionbodega
                            tomafiscaxubicacionbodega.SetParameterValue(0, Me.cbxUbicacion.Text)
                            tomafiscaxubicacionbodega.SetParameterValue(1, Me.codigo.Text)   'para codigos de barra o id
                            tomafiscaxubicacionbodega.SetParameterValue(2, Cb_Bodegas.SelectedValue)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, tomafiscaxubicacionbodega)
                        End If

                Case 9
                        If CheckBox1.Checked = True Then
                            Dim TomaxCategoria As New InventarioxCategoria
                            TomaxCategoria.SetParameterValue(0, Me.cbxFamilia.Text)
                            TomaxCategoria.SetParameterValue(1, Me.codigo.Text)     'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, TomaxCategoria)
                        Else
                            Dim TomaxCategoriaxbodega As New InventarioxCategoriaxbodega
                            TomaxCategoriaxbodega.SetParameterValue(0, Me.cbxFamilia.Text)
                            TomaxCategoriaxbodega.SetParameterValue(1, Me.codigo.Text) 'para codigos de barra o id
                            TomaxCategoriaxbodega.SetParameterValue(2, Cb_Bodegas.SelectedValue)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, TomaxCategoriaxbodega)
                        End If

                Case 10
                        If CheckBox1.Checked = True Then
                            Dim Servicios As New InventarioxServicios
                            Servicios.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                            Servicios.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                            Servicios.SetParameterValue(2, Me.codigo.Text)      'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Servicios)
                        Else
                            Dim Servicios As New InventarioxServiciosxboega
                            Servicios.SetParameterValue(0, CDate(Me.FechaInicio.Value))
                            Servicios.SetParameterValue(1, CDate(Me.FechaFinal.Value))
                            Servicios.SetParameterValue(2, Me.codigo.Text)      'para codigos de barra o id
                            Servicios.SetParameterValue(3, Cb_Bodegas.SelectedValue)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, Servicios)
                        End If

                Case 11
                        If CheckBox1.Checked = True Then
                            Dim APedirGeneral As New ArticulosxPedir
                            APedirGeneral.SetParameterValue(0, Me.cbxMoneda.Text)
                            APedirGeneral.SetParameterValue(1, Me.LbTipoCambio.Text)
                            APedirGeneral.SetParameterValue(2, Me.codigo.Text)  'para codigos de barra o id
                            CrystalReportsConexion.LoadReportViewer(rptViewer, APedirGeneral)
                        Else
                            Dim APedirxbodega As New ArticulosxPedirxbodega
                            APedirxbodega.SetParameterValue(0, Me.cbxMoneda.Text)
                            APedirxbodega.SetParameterValue(1, Me.LbTipoCambio.Text)
                            APedirxbodega.SetParameterValue(2, Me.codigo.Text)  'para codigos de barra o id
                            APedirxbodega.SetParameterValue(3, Cb_Bodegas.SelectedValue)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, APedirxbodega)
                        End If

                Case 12
                        If CheckBox1.Checked = True Then
                            Dim APedirXProveedor As New ArticulosxPedirxProveedor
                            APedirXProveedor.SetParameterValue(0, cbxMoneda.Text)
                            APedirXProveedor.SetParameterValue(1, LbTipoCambio.Text)
                            APedirXProveedor.SetParameterValue(2, cbxProveedor.Text)
                            APedirXProveedor.SetParameterValue(3, codigo.Text)   'para codigos de barra o id
                            APedirXProveedor.SetParameterValue(4, cbxProveedor.SelectedValue)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, APedirXProveedor)

                        Else
                            Dim APedirXProveedorxbodega As New ArticulosxPedirxProveedorxbodega
                            APedirXProveedorxbodega.SetParameterValue(0, Me.cbxMoneda.Text)
                            APedirXProveedorxbodega.SetParameterValue(1, Me.LbTipoCambio.Text)
                            APedirXProveedorxbodega.SetParameterValue(2, Me.cbxProveedor.Text)
                            APedirXProveedorxbodega.SetParameterValue(3, Me.codigo.Text)   'para codigos de barra o id
                            APedirXProveedorxbodega.SetParameterValue(4, cbxProveedor.SelectedValue)
                            APedirXProveedorxbodega.SetParameterValue(5, Cb_Bodegas.SelectedValue)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, APedirXProveedorxbodega)
                        End If

                Case 13
                        If CheckBox1.Checked = True Then
                            Dim ReporteListaPrecios As New ListadoPreciosVenta
                            ReporteListaPrecios.SetParameterValue(0, Me.cbxMoneda.Text)
                            ReporteListaPrecios.SetParameterValue(1, Me.LbTipoCambio.Text)
                            ReporteListaPrecios.SetParameterValue(2, Me.codigo.Text)    'para codigos de barra o id
                            ReporteListaPrecios.SetParameterValue(3, CBTipoPrecio.SelectedIndex + 1)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, ReporteListaPrecios)

                        Else
                            Dim ReporteListaPreciosxbodega As New ListadoPreciosVentaxbodega
                            ReporteListaPreciosxbodega.SetParameterValue(0, Me.cbxMoneda.Text)
                            ReporteListaPreciosxbodega.SetParameterValue(1, Me.LbTipoCambio.Text)
                            ReporteListaPreciosxbodega.SetParameterValue(2, Me.codigo.Text)    'para codigos de barra o id
                            ReporteListaPreciosxbodega.SetParameterValue(3, Cb_Bodegas.SelectedValue)
                            ReporteListaPreciosxbodega.SetParameterValue(4, CBTipoPrecio.SelectedIndex + 1)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, ReporteListaPreciosxbodega)
                        End If

                Case 14
                        'HACK AQUI FALTA EL REPORTE DE ArticulosVendidos
                        If CheckBox1.Checked = True Then
                            Dim ArticulosVentas As New ArticulosVendidos
                            ArticulosVentas.SetParameterValue(0, cbxMoneda.Text)
                            ArticulosVentas.SetParameterValue(1, LbTipoCambio.Text)
                            ArticulosVentas.SetParameterValue(2, codigo.Text)    'para codigos de barra o id
                            ArticulosVentas.SetParameterValue(3, FechaInicio.Value)
                            ArticulosVentas.SetParameterValue(4, FechaFinal.Value)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, ArticulosVentas)
                        Else
                            Dim ArticulosVentasxbodega As New Report1
                            ArticulosVentasxbodega.SetParameterValue(0, FechaInicio.Text)
                            ArticulosVentasxbodega.SetParameterValue(1, FechaFinal.Text)
                            ArticulosVentasxbodega.SetParameterValue(2, 0)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, ArticulosVentasxbodega)
                        End If

                Case 15
                        If CheckBox1.Checked = True Then
                            Dim VendidosXProveedor As New ArticulosVendidosXProveedor
                            VendidosXProveedor.SetParameterValue(0, cbxMoneda.Text)
                            VendidosXProveedor.SetParameterValue(1, LbTipoCambio.Text)
                            VendidosXProveedor.SetParameterValue(2, codigo.Text)
                            VendidosXProveedor.SetParameterValue(3, CB_Proveedor.Text)
                            VendidosXProveedor.SetParameterValue(4, FechaInicio.Value)
                            VendidosXProveedor.SetParameterValue(5, FechaFinal.Value)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, VendidosXProveedor)
                        Else
                            Dim VendidosXProveedorxbodega As New ArticulosVendidosXProveedorbodega
                            VendidosXProveedorxbodega.SetParameterValue(0, cbxMoneda.Text)
                            VendidosXProveedorxbodega.SetParameterValue(1, LbTipoCambio.Text)
                            VendidosXProveedorxbodega.SetParameterValue(2, codigo.Text)
                            VendidosXProveedorxbodega.SetParameterValue(3, CB_Proveedor.Text)
                            VendidosXProveedorxbodega.SetParameterValue(4, FechaInicio.Value)
                            VendidosXProveedorxbodega.SetParameterValue(5, FechaFinal.Value)
                            VendidosXProveedorxbodega.SetParameterValue(6, Cb_Bodegas.SelectedValue)
                            CrystalReportsConexion.LoadReportViewer(rptViewer, VendidosXProveedorxbodega)
                        End If

                Case 16
                        Dim Apartados As New Inventario_Apartados
                        Apartados.SetParameterValue(0, cbxMoneda.Text)
                        Apartados.SetParameterValue(1, LbTipoCambio.Text)
                        Apartados.SetParameterValue(2, codigo.Text)
                        CrystalReportsConexion.LoadReportViewer(rptViewer, Apartados)

                Case 17
                    Dim ReporteCostoArticulo As New CostoArticulo
                    ReporteCostoArticulo.SetParameterValue(0, Me.cbxMoneda.Text)
                    ReporteCostoArticulo.SetParameterValue(1, Me.LbTipoCambio.Text)
                    ReporteCostoArticulo.SetParameterValue(2, Me.codigo.Text)    'para codigos de barra o id
                    CrystalReportsConexion.LoadReportViewer(rptViewer, ReporteCostoArticulo)

            End Select
            rptViewer.Show()

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            EnEspera.Close()
            cConexion.DesConectar(SQLConexion.sQlconexion)
        End Try
    End Sub
#End Region

#Region "Combobox"
    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.cbxCondicion.Visible = False
        Me.txtCantidad.Visible = False
        Me.txtDescripcion.Visible = False
        Me.cbxFamilia.Visible = False
        Me.cbxUbicacion.Visible = False
        Me.cbxProveedor.Visible = False
        Me.cbxProveedor.Width = 136
        Me.lblMoneda.Visible = True
        Me.cbxMoneda.Visible = True
        Me.cbxMoneda.Enabled = True
        If CheckBox1.Checked Then
            Cb_Bodegas.Visible = False
            lbodega.Visible = False
        Else
            Cb_Bodegas.Visible = True
            lbodega.Visible = True
        End If
        CbExistencia.Visible = False
        LProveedor.Visible = False
        CB_Proveedor.Visible = False
        LOrdenado.Visible = False
        CBOrdenar.Visible = False
        LAgrupado.Visible = False
        CBAgrupado.Visible = False
        LTipoPrecio.Visible = False
        CBTipoPrecio.Visible = False

        Select Case Me.ComboBox1.SelectedIndex
            Case 0
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = True
                Me.lblgeneral.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = True
                Me.cbxCondicion.Visible = True
                Me.txtCantidad.Visible = True

            Case 1
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = True
                Me.lblgeneral.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = True
                Me.lblgeneral.Visible = True
                Me.lblgeneral.Text = "Descripci�n"
                Me.txtDescripcion.Visible = True
                Me.txtDescripcion.Enabled = True

            Case 2
                Me.cbxFamilia.DataSource = Nothing
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = True
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = True
                Me.lblgeneral.Visible = True
                Me.lblgeneral.Text = "Familia"
                Me.cbxFamilia.Visible = True
                Me.cbxFamilia.Enabled = True
                cbxFamilia.Items.Clear()

                Dim rs As SqlDataReader
                rs = cConexion.GetRecorset(sqlConexion, "SELECT distinct(descripcion) from familia")
                While rs.Read
                    Try
                        Me.cbxFamilia.Items.Add(rs!descripcion)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()

            Case 3
                Me.cbxUbicacion.DataSource = Nothing
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = True
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = True
                Me.lblgeneral.Visible = True
                Me.lblgeneral.Text = "Ubicaci�n"
                Me.cbxUbicacion.Visible = True
                Me.cbxUbicacion.Enabled = True
                cbxUbicacion.Items.Clear()

                Dim rs As SqlDataReader
                rs = cConexion.GetRecorset(sqlConexion, "SELECT descripcion from ubicaciones")
                While rs.Read
                    Try
                        Me.cbxUbicacion.Items.Add(rs!descripcion)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()

            Case 4
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = True
                Me.lblgeneral.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = True

            Case 5
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = True
                Me.lblgeneral.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = True

            Case 6
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = False
                Me.lblgeneral.Visible = False
                Me.cbxProveedor.Enabled = True
                Me.cbxProveedor.Visible = True
                Me.cbxProveedor.Width = 300
                Me.lblgeneral.Visible = True
                Me.lblgeneral.Text = "Proveedor"
                DataSetReporteInventario.Proveedores.Clear()
                AdapterProveedor.Fill(DataSetReporteInventario, "Proveedores")

            Case 7
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = False
                Me.lblgeneral.Visible = False
                Me.cbxCondicion.Visible = False
                Me.txtCantidad.Visible = False
                Me.txtDescripcion.Visible = False
                Me.cbxFamilia.Visible = False
                Me.cbxUbicacion.Visible = False
                Me.cbxProveedor.Visible = False
                Me.LOrdenado.Visible = True
                Me.CBOrdenar.Visible = True
                Me.LAgrupado.Visible = True
                Me.CBAgrupado.Visible = True
                Me.CbExistencia.Visible = True
            Case 8
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = False
                Me.cbxUbicacion.DataSource = Nothing
                Me.cbxCondicion.Visible = False
                Me.txtCantidad.Visible = False
                Me.txtDescripcion.Visible = False
                Me.cbxFamilia.Visible = False
                Me.cbxProveedor.Visible = False
                Me.cbxMoneda.Enabled = False
                Me.cbxUbicacion.Visible = True
                Me.cbxUbicacion.Enabled = True
                Me.lblgeneral.Visible = True
                Me.lblgeneral.Text = "Ubicaci�n"
                cbxUbicacion.Items.Clear()

                Dim rs As SqlDataReader
                rs = cConexion.GetRecorset(sqlConexion, "SELECT descripcion from ubicaciones")
                While rs.Read
                    Try
                        Me.cbxUbicacion.Items.Add(rs!descripcion)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()

            Case 9
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = False
                Me.cbxFamilia.DataSource = Nothing
                Me.cbxCondicion.Visible = False
                Me.txtCantidad.Visible = False
                Me.txtDescripcion.Visible = False
                Me.cbxUbicacion.Visible = False
                Me.cbxProveedor.Visible = False
                Me.cbxMoneda.Enabled = False
                Me.lblgeneral.Visible = True
                Me.lblgeneral.Text = "Categor�a"
                Me.cbxFamilia.Visible = True
                Me.cbxFamilia.Enabled = True
                cbxFamilia.Items.Clear()

                Dim rs As SqlDataReader
                rs = cConexion.GetRecorset(sqlConexion, "SELECT distinct(descripcion) from familia")
                While rs.Read
                    Try
                        Me.cbxFamilia.Items.Add(rs!descripcion)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()

            Case 10
                Me.lblgeneral.Visible = False
                Me.cbxFamilia.DataSource = Nothing
                Me.cbxCondicion.Visible = False
                Me.txtCantidad.Visible = False
                Me.txtDescripcion.Visible = False
                Me.cbxUbicacion.Visible = False
                Me.cbxProveedor.Visible = False
                Me.cbxMoneda.Enabled = False
                Me.FechaInicio.Text = Date.Today
                Me.FechaFinal.Text = Date.Today
                Me.FechaInicio.Visible = True
                Me.FechaFinal.Visible = True
                Me.lbldesde.Visible = True
                Me.lblhasta.Visible = True

            Case 11
                Me.lblMoneda.Visible = True
                Me.cbxMoneda.Visible = True
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = False
                Me.lblgeneral.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = False

            Case 12
                Me.lblMoneda.Visible = True
                Me.cbxMoneda.Visible = True
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = False
                lblgeneral.Visible = True
                lblgeneral.Text = "Proveedor"
                cbxProveedor.Enabled = True
                Me.cbxProveedor.Width = 300
                cbxProveedor.Visible = True
                DataSetReporteInventario.Proveedores.Clear()
                AdapterProveedor.Fill(DataSetReporteInventario, "Proveedores")

            Case 13
                Me.lblMoneda.Visible = True
                Me.cbxMoneda.Visible = True
                LTipoPrecio.Visible = True
                CBTipoPrecio.Visible = True
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = False
                Me.lblgeneral.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = False
                CBTipoPrecio.Focus()

            Case 14
                Me.lblgeneral.Visible = False
                Me.lbldesde.Visible = True
                Me.lblhasta.Visible = True
                Me.FechaInicio.Visible = True
                Me.FechaFinal.Visible = True

            Case 15
                Dim rs As SqlDataReader
                lbldesde.Visible = True
                lblhasta.Visible = True
                FechaInicio.Visible = True
                FechaFinal.Visible = True
                lblMoneda.Visible = True
                cbxMoneda.Visible = True
                CB_Proveedor.DataSource = Nothing
                lblgeneral.Visible = False
                LProveedor.Visible = True
                CB_Proveedor.Enabled = True
                CB_Proveedor.Visible = True
                CB_Proveedor.Items.Clear()
                rs = cConexion.GetRecorset(sqlConexion, "SELECT CodigoProv, nombre from proveedores")
                While rs.Read
                    Try
                        CB_Proveedor.Items.Add(rs!nombre)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()
                CB_Proveedor.SelectedIndex = 0

            Case 16

                Dim rs As SqlDataReader
                lbldesde.Visible = True
                lblhasta.Visible = True
                FechaInicio.Visible = True
                FechaFinal.Visible = True
                lblMoneda.Visible = True
                cbxMoneda.Visible = True
                CB_Proveedor.DataSource = Nothing
                lblgeneral.Visible = False
                LProveedor.Visible = True
                CB_Proveedor.Enabled = True
                CB_Proveedor.Visible = True
                CB_Proveedor.Items.Clear()
                rs = cConexion.GetRecorset(sqlConexion, "SELECT CodigoProv, nombre from proveedores")
                While rs.Read
                    Try
                        CB_Proveedor.Items.Add(rs!nombre)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End While
                rs.Close()
                CB_Proveedor.SelectedIndex = 0

            Case 17
                Me.lbldesde.Visible = False
                Me.lblhasta.Visible = False
                Me.lblgeneral.Visible = False
                Me.FechaInicio.Visible = False
                Me.FechaFinal.Visible = False
                Me.cbxCondicion.Visible = False
                Me.txtCantidad.Visible = False
                Me.CheckBox1.Visible = False
                Me.lbodega.Visible = False
                Me.Cb_Bodegas.Visible = False

        End Select
    End Sub
#End Region

#Region "CheckBox"
    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If (CheckBox1.Checked = False) Then
            Cb_Bodegas.Visible = True
            lbodega.Visible = True
        Else
            Cb_Bodegas.Visible = False
            lbodega.Visible = False
        End If
    End Sub
#End Region
    
    Private Sub CBOrdenar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBOrdenar.SelectedIndexChanged
        If Me.CBOrdenar.SelectedItem = "Codigo" Then
            Me.codigo.SelectedItem = "Id"

        End If

        If Me.CBOrdenar.SelectedItem = "Barras" Then
            Me.codigo.SelectedItem = "Barras"
        End If
    End Sub

End Class
