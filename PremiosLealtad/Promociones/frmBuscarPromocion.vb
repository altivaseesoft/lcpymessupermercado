Imports System.Windows.Forms
Public Class frmBuscarPromocion
    Inherits System.Windows.Forms.Form

    Dim usua As Usuario_Logeado
    Dim PMU As New PerfilModulo_Class
    Dim NuevaConexion As String


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txBusqueda As System.Windows.Forms.TextBox
    Friend WithEvents btBuscar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btCrear As System.Windows.Forms.Button
    Friend WithEvents btEditar As System.Windows.Forms.Button
    Friend WithEvents btImprimirLista As System.Windows.Forms.Button
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents DtsPromociones1 As LcPymes_5._2.dtsPromociones
    Friend WithEvents adPromociones As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DataGridTableStyle1 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents DataGridTextBoxColumn1 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn2 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn3 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn4 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn5 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn6 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridBoolColumn1 As System.Windows.Forms.DataGridBoolColumn
    Friend WithEvents dgBuscarPromociones As System.Windows.Forms.DataGrid
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBuscarPromocion))
        Me.Label1 = New System.Windows.Forms.Label
        Me.txBusqueda = New System.Windows.Forms.TextBox
        Me.btBuscar = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.dgBuscarPromociones = New System.Windows.Forms.DataGrid
        Me.DtsPromociones1 = New LcPymes_5._2.dtsPromociones
        Me.DataGridTableStyle1 = New System.Windows.Forms.DataGridTableStyle
        Me.DataGridTextBoxColumn1 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn2 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn3 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn4 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn5 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn6 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridBoolColumn1 = New System.Windows.Forms.DataGridBoolColumn
        Me.btCrear = New System.Windows.Forms.Button
        Me.btEditar = New System.Windows.Forms.Button
        Me.btImprimirLista = New System.Windows.Forms.Button
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adPromociones = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.dgBuscarPromociones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsPromociones1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 0)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Buscar : "
        '
        'txBusqueda
        '
        Me.txBusqueda.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txBusqueda.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txBusqueda.Location = New System.Drawing.Point(72, 24)
        Me.txBusqueda.Name = "txBusqueda"
        Me.txBusqueda.Size = New System.Drawing.Size(416, 23)
        Me.txBusqueda.TabIndex = 2
        Me.txBusqueda.Text = ""
        '
        'btBuscar
        '
        Me.btBuscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btBuscar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btBuscar.Location = New System.Drawing.Point(544, 24)
        Me.btBuscar.Name = "btBuscar"
        Me.btBuscar.TabIndex = 3
        Me.btBuscar.Text = "Buscar"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(16, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Buscar : "
        '
        'dgBuscarPromociones
        '
        Me.dgBuscarPromociones.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgBuscarPromociones.DataMember = ""
        Me.dgBuscarPromociones.DataSource = Me.DtsPromociones1.vs_Promociones
        Me.dgBuscarPromociones.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgBuscarPromociones.Location = New System.Drawing.Point(16, 64)
        Me.dgBuscarPromociones.Name = "dgBuscarPromociones"
        Me.dgBuscarPromociones.ReadOnly = True
        Me.dgBuscarPromociones.Size = New System.Drawing.Size(712, 288)
        Me.dgBuscarPromociones.TabIndex = 5
        Me.dgBuscarPromociones.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.DataGridTableStyle1})
        '
        'DtsPromociones1
        '
        Me.DtsPromociones1.DataSetName = "dtsPromociones"
        Me.DtsPromociones1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'DataGridTableStyle1
        '
        Me.DataGridTableStyle1.DataGrid = Me.dgBuscarPromociones
        Me.DataGridTableStyle1.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.DataGridTextBoxColumn1, Me.DataGridTextBoxColumn2, Me.DataGridTextBoxColumn3, Me.DataGridTextBoxColumn4, Me.DataGridTextBoxColumn5, Me.DataGridTextBoxColumn6, Me.DataGridBoolColumn1})
        Me.DataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGridTableStyle1.MappingName = "vs_Promociones"
        '
        'DataGridTextBoxColumn1
        '
        Me.DataGridTextBoxColumn1.Format = ""
        Me.DataGridTextBoxColumn1.FormatInfo = Nothing
        Me.DataGridTextBoxColumn1.HeaderText = "Id"
        Me.DataGridTextBoxColumn1.MappingName = "Id"
        Me.DataGridTextBoxColumn1.Width = 49
        '
        'DataGridTextBoxColumn2
        '
        Me.DataGridTextBoxColumn2.Format = ""
        Me.DataGridTextBoxColumn2.FormatInfo = Nothing
        Me.DataGridTextBoxColumn2.HeaderText = "Nombre"
        Me.DataGridTextBoxColumn2.MappingName = "Nombre"
        Me.DataGridTextBoxColumn2.Width = 145
        '
        'DataGridTextBoxColumn3
        '
        Me.DataGridTextBoxColumn3.Format = ""
        Me.DataGridTextBoxColumn3.FormatInfo = Nothing
        Me.DataGridTextBoxColumn3.HeaderText = "Paquete"
        Me.DataGridTextBoxColumn3.MappingName = "NombrePaquete"
        Me.DataGridTextBoxColumn3.Width = 145
        '
        'DataGridTextBoxColumn4
        '
        Me.DataGridTextBoxColumn4.Format = ""
        Me.DataGridTextBoxColumn4.FormatInfo = Nothing
        Me.DataGridTextBoxColumn4.HeaderText = "Acumulado"
        Me.DataGridTextBoxColumn4.MappingName = "MontoAcumulado"
        Me.DataGridTextBoxColumn4.NullText = "0"
        Me.DataGridTextBoxColumn4.Width = 90
        '
        'DataGridTextBoxColumn5
        '
        Me.DataGridTextBoxColumn5.Format = ""
        Me.DataGridTextBoxColumn5.FormatInfo = Nothing
        Me.DataGridTextBoxColumn5.HeaderText = "Creado por"
        Me.DataGridTextBoxColumn5.MappingName = "NombreUsuario"
        Me.DataGridTextBoxColumn5.Width = 90
        '
        'DataGridTextBoxColumn6
        '
        Me.DataGridTextBoxColumn6.Format = "dd/mm/yyyy"
        Me.DataGridTextBoxColumn6.FormatInfo = Nothing
        Me.DataGridTextBoxColumn6.HeaderText = "Fecha Creaci�n"
        Me.DataGridTextBoxColumn6.MappingName = "Fecha"
        Me.DataGridTextBoxColumn6.Width = 85
        '
        'DataGridBoolColumn1
        '
        Me.DataGridBoolColumn1.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.DataGridBoolColumn1.FalseValue = False
        Me.DataGridBoolColumn1.HeaderText = "Desactivado"
        Me.DataGridBoolColumn1.MappingName = "Estado"
        Me.DataGridBoolColumn1.NullValue = CType(resources.GetObject("DataGridBoolColumn1.NullValue"), Object)
        Me.DataGridBoolColumn1.TrueValue = True
        Me.DataGridBoolColumn1.Width = 69
        '
        'btCrear
        '
        Me.btCrear.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btCrear.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btCrear.Location = New System.Drawing.Point(80, 376)
        Me.btCrear.Name = "btCrear"
        Me.btCrear.Size = New System.Drawing.Size(104, 23)
        Me.btCrear.TabIndex = 6
        Me.btCrear.Text = "Crear"
        '
        'btEditar
        '
        Me.btEditar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btEditar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEditar.Location = New System.Drawing.Point(320, 376)
        Me.btEditar.Name = "btEditar"
        Me.btEditar.Size = New System.Drawing.Size(104, 23)
        Me.btEditar.TabIndex = 7
        Me.btEditar.Text = "Editar"
        '
        'btImprimirLista
        '
        Me.btImprimirLista.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btImprimirLista.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btImprimirLista.Location = New System.Drawing.Point(560, 376)
        Me.btImprimirLista.Name = "btImprimirLista"
        Me.btImprimirLista.Size = New System.Drawing.Size(104, 23)
        Me.btImprimirLista.TabIndex = 8
        Me.btImprimirLista.Text = "Imprimir Lista"
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'adPromociones
        '
        Me.adPromociones.InsertCommand = Me.SqlInsertCommand1
        Me.adPromociones.SelectCommand = Me.SqlSelectCommand1
        Me.adPromociones.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "vs_S_Promociones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("MontoAcumulado", "MontoAcumulado"), New System.Data.Common.DataColumnMapping("IdUsuario", "IdUsuario"), New System.Data.Common.DataColumnMapping("Estado", "Estado"), New System.Data.Common.DataColumnMapping("IdPaquete", "IdPaquete"), New System.Data.Common.DataColumnMapping("NombrePaquete", "NombrePaquete"), New System.Data.Common.DataColumnMapping("NombreUsuario", "NombreUsuario")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO vs_S_Promociones(Id, Nombre, Fecha, MontoAcumulado, IdUsuario, Estado" & _
        ", IdPaquete, NombrePaquete, NombreUsuario) VALUES (@Id, @Nombre, @Fecha, @MontoA" & _
        "cumulado, @IdUsuario, @Estado, @IdPaquete, @NombrePaquete, @NombreUsuario); SELE" & _
        "CT Id, Nombre, Fecha, MontoAcumulado, IdUsuario, Estado, IdPaquete, NombrePaquet" & _
        "e, NombreUsuario FROM vs_S_Promociones"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Nombre", System.Data.SqlDbType.VarChar, 50, "Nombre"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MontoAcumulado", System.Data.SqlDbType.Float, 8, "MontoAcumulado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdUsuario", System.Data.SqlDbType.VarChar, 50, "IdUsuario"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Estado", System.Data.SqlDbType.Bit, 1, "Estado"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IdPaquete", System.Data.SqlDbType.Int, 4, "IdPaquete"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombrePaquete", System.Data.SqlDbType.VarChar, 50, "NombrePaquete"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NombreUsuario", System.Data.SqlDbType.VarChar, 255, "NombreUsuario"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id, Nombre, Fecha, MontoAcumulado, IdUsuario, Estado, IdPaquete, NombrePaq" & _
        "uete, NombreUsuario FROM vs_S_Promociones"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'frmBuscarPromocion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(744, 426)
        Me.Controls.Add(Me.btImprimirLista)
        Me.Controls.Add(Me.btEditar)
        Me.Controls.Add(Me.btCrear)
        Me.Controls.Add(Me.dgBuscarPromociones)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btBuscar)
        Me.Controls.Add(Me.txBusqueda)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(760, 464)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(760, 464)
        Me.Name = "frmBuscarPromocion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promoci�n"
        CType(Me.dgBuscarPromociones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsPromociones1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmBuscarPromocion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        PMU = VSM(usua.Cedula, Me.Name)  'Carga los privilegios del usuario con el modulo  

        Me.spBusqueda()

    End Sub
    Private Sub spBusqueda()
        Dim Fx As New cFunciones
        Dim sql As String
        sql = "SELECT * FROM [Seepos].[dbo].[vs_S_Promociones] " & _
        " where [Nombre] LIKE '%" & Me.txBusqueda.Text & "%' or [NombrePaquete] LIKE '%" & Me.txBusqueda.Text & "%' " & _
        " or [MontoAcumulado] LIKE '%" & Me.txBusqueda.Text & "%' or [NombreUsuario] LIKE '%" & Me.txBusqueda.Text & "%'"
        Dim strConexion As String = SqlConnection1.ConnectionString
        DtsPromociones1.Tables("vs_Promociones").Clear()
        Fx.Cargar_Tabla_Generico(Me.adPromociones, sql, strConexion)  'JCGA 19032007
        Me.adPromociones.Fill(Me.DtsPromociones1, "vs_Promociones")
    End Sub

    Private Sub btCrear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCrear.Click
        If PMU.Update Then spCrearPromocion() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub
    Private Sub spCrearPromocion()
        Me.Visible = False
        Dim frm As New frmPromocion(usua)
        frm.Text = "Crear Promoci�n"
        frm.Nuevo = True
        frm.ShowDialog()
        Me.Visible = True
        Me.spBusqueda()
    End Sub

    Private Sub btEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btEditar.Click
        If PMU.Update Then spEditarPromocion() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub

    Private Sub spEditarPromocion()
        If Me.dgBuscarPromociones.CurrentRowIndex >= 0 Then
            Me.Visible = False
            Dim frm As New frmPromocion(usua)
            frm.Text = "Editar Promoci�n"
            frm.pubId = Trim(DtsPromociones1.Tables("vs_Promociones").Rows(Me.dgBuscarPromociones.CurrentRowIndex).Item("Id"))
            frm.pubIdPaquete = Trim(DtsPromociones1.Tables("vs_Promociones").Rows(Me.dgBuscarPromociones.CurrentRowIndex).Item("IdPaquete"))
            frm.pubDescripcion = Trim(DtsPromociones1.Tables("vs_Promociones").Rows(Me.dgBuscarPromociones.CurrentRowIndex).Item("Nombre"))
            frm.pubAcumulado = Trim(DtsPromociones1.Tables("vs_Promociones").Rows(Me.dgBuscarPromociones.CurrentRowIndex).Item("MontoAcumulado"))
            frm.pubUsuario = Trim(DtsPromociones1.Tables("vs_Promociones").Rows(Me.dgBuscarPromociones.CurrentRowIndex).Item("NombreUsuario"))
            frm.pubEstado = Trim(DtsPromociones1.Tables("vs_Promociones").Rows(Me.dgBuscarPromociones.CurrentRowIndex).Item("Estado"))
            frm.Nuevo = False
            frm.ShowDialog()
            Me.Visible = True
            Me.spBusqueda()
        End If
    End Sub


    Private Sub btBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btBuscar.Click
        If PMU.Find Then Me.spBusqueda() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub
    Private Sub txBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles txBusqueda.KeyUp
        If PMU.Find Then Me.spBusqueda() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub
    Private Sub btImprimirLista_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btImprimirLista.Click
        If PMU.Print Then spImprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub
    Private Sub spImprimir()
        Me.btImprimirLista.Enabled = False
        If pbReporteImpreso = False Then
            Me.spVerReporte()
        End If
        If Me.Visor.IsDisposed Then
            Visor = New frmContenedor
            Me.spVerReporte()
        End If
        If pbReporteImpreso Then
            Me.Visor.BringToFront()
            Me.Visor.WindowState = FormWindowState.Maximized
            Me.Visor.Show()
        End If
        Me.btImprimirLista.Enabled = True
    End Sub
    Dim Visor As New frmContenedor
    Dim pbReporteImpreso As Boolean
    Private Sub spVerReporte()

        Try
            Dim rpt As New rptPromociones

            rpt.Refresh()
            rpt.SetDataSource(Me.DtsPromociones1)
            CrystalReportsConexion.LoadReportViewer(Visor.VisorReporte, rpt)
            Visor.Text = "Reporte de Paquetes"
            Visor.Show()
            Visor.WindowState = FormWindowState.Maximized
            pbReporteImpreso = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
End Class
