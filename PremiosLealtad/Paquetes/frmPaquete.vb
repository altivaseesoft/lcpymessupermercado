Imports System.data.SqlClient
Imports System.Data
Imports System.Windows.Forms
Public Class frmPaquete
    Inherits System.Windows.Forms.Form

    Public IdPaquete As Integer
    Public Nuevo As Boolean
    Dim NuevaConexion As String
    Dim CodigoArticulo As String
    Dim strConexion As String

    Dim usua As Usuario_Logeado
    Dim priCambio As Boolean


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txNombrePaquete As System.Windows.Forms.TextBox
    Friend WithEvents cbDesactivado As System.Windows.Forms.CheckBox
    Friend WithEvents btAgregarProducto As System.Windows.Forms.Button
    Friend WithEvents btQuitarProducto As System.Windows.Forms.Button
    Friend WithEvents btGuardar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbNombreUsuario As System.Windows.Forms.Label
    Friend WithEvents lbID As System.Windows.Forms.Label
    Friend WithEvents DataGridTableStyle1 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents DataGridTextBoxColumn1 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents dgPaqueteDetalle As System.Windows.Forms.DataGrid
    Friend WithEvents DtsPaquetes1 As LcPymes_5._2.dtsPaquetes
    Friend WithEvents DataGridTextBoxColumn2 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn3 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents adPaqueteDetalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection2 As System.Data.SqlClient.SqlConnection
    Friend WithEvents Label4 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txNombrePaquete = New System.Windows.Forms.TextBox
        Me.cbDesactivado = New System.Windows.Forms.CheckBox
        Me.btAgregarProducto = New System.Windows.Forms.Button
        Me.btQuitarProducto = New System.Windows.Forms.Button
        Me.dgPaqueteDetalle = New System.Windows.Forms.DataGrid
        Me.DtsPaquetes1 = New LcPymes_5._2.dtsPaquetes
        Me.DataGridTableStyle1 = New System.Windows.Forms.DataGridTableStyle
        Me.DataGridTextBoxColumn1 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn2 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn3 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.btGuardar = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lbNombreUsuario = New System.Windows.Forms.Label
        Me.lbID = New System.Windows.Forms.Label
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adPaqueteDetalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection2 = New System.Data.SqlClient.SqlConnection
        Me.Label4 = New System.Windows.Forms.Label
        CType(Me.dgPaqueteDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsPaquetes1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txNombrePaquete
        '
        Me.txNombrePaquete.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txNombrePaquete.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txNombrePaquete.Location = New System.Drawing.Point(152, 24)
        Me.txNombrePaquete.MaxLength = 50
        Me.txNombrePaquete.Name = "txNombrePaquete"
        Me.txNombrePaquete.Size = New System.Drawing.Size(448, 23)
        Me.txNombrePaquete.TabIndex = 1
        Me.txNombrePaquete.Text = ""
        '
        'cbDesactivado
        '
        Me.cbDesactivado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbDesactivado.Location = New System.Drawing.Point(624, 24)
        Me.cbDesactivado.Name = "cbDesactivado"
        Me.cbDesactivado.TabIndex = 2
        Me.cbDesactivado.Text = "Desactivado"
        '
        'btAgregarProducto
        '
        Me.btAgregarProducto.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAgregarProducto.Location = New System.Drawing.Point(24, 88)
        Me.btAgregarProducto.Name = "btAgregarProducto"
        Me.btAgregarProducto.Size = New System.Drawing.Size(120, 32)
        Me.btAgregarProducto.TabIndex = 3
        Me.btAgregarProducto.Text = "Agregar Producto"
        '
        'btQuitarProducto
        '
        Me.btQuitarProducto.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btQuitarProducto.Location = New System.Drawing.Point(200, 88)
        Me.btQuitarProducto.Name = "btQuitarProducto"
        Me.btQuitarProducto.Size = New System.Drawing.Size(120, 32)
        Me.btQuitarProducto.TabIndex = 4
        Me.btQuitarProducto.Text = "Quitar Producto"
        '
        'dgPaqueteDetalle
        '
        Me.dgPaqueteDetalle.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgPaqueteDetalle.DataMember = ""
        Me.dgPaqueteDetalle.DataSource = Me.DtsPaquetes1.PaqueteDetalle
        Me.dgPaqueteDetalle.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgPaqueteDetalle.Location = New System.Drawing.Point(24, 128)
        Me.dgPaqueteDetalle.Name = "dgPaqueteDetalle"
        Me.dgPaqueteDetalle.PreferredColumnWidth = 219
        Me.dgPaqueteDetalle.ReadOnly = True
        Me.dgPaqueteDetalle.Size = New System.Drawing.Size(696, 216)
        Me.dgPaqueteDetalle.TabIndex = 5
        Me.dgPaqueteDetalle.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.DataGridTableStyle1})
        '
        'DtsPaquetes1
        '
        Me.DtsPaquetes1.DataSetName = "dtsPaquetes"
        Me.DtsPaquetes1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'DataGridTableStyle1
        '
        Me.DataGridTableStyle1.DataGrid = Me.dgPaqueteDetalle
        Me.DataGridTableStyle1.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.DataGridTextBoxColumn1, Me.DataGridTextBoxColumn2, Me.DataGridTextBoxColumn3})
        Me.DataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGridTableStyle1.MappingName = "PaqueteDetalle"
        '
        'DataGridTextBoxColumn1
        '
        Me.DataGridTextBoxColumn1.Format = ""
        Me.DataGridTextBoxColumn1.FormatInfo = Nothing
        Me.DataGridTextBoxColumn1.HeaderText = "C�digo Producto"
        Me.DataGridTextBoxColumn1.MappingName = "Codigo"
        Me.DataGridTextBoxColumn1.Width = 150
        '
        'DataGridTextBoxColumn2
        '
        Me.DataGridTextBoxColumn2.Format = ""
        Me.DataGridTextBoxColumn2.FormatInfo = Nothing
        Me.DataGridTextBoxColumn2.HeaderText = "Cantidad"
        Me.DataGridTextBoxColumn2.MappingName = "Cantidad"
        Me.DataGridTextBoxColumn2.Width = 140
        '
        'DataGridTextBoxColumn3
        '
        Me.DataGridTextBoxColumn3.Format = ""
        Me.DataGridTextBoxColumn3.FormatInfo = Nothing
        Me.DataGridTextBoxColumn3.HeaderText = "Descripci�n"
        Me.DataGridTextBoxColumn3.MappingName = "Descripcion"
        Me.DataGridTextBoxColumn3.Width = 367
        '
        'btGuardar
        '
        Me.btGuardar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btGuardar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btGuardar.Location = New System.Drawing.Point(312, 376)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(120, 23)
        Me.btGuardar.TabIndex = 6
        Me.btGuardar.Text = "Guardar"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(32, 376)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(24, 23)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "ID: "
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(512, 376)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 23)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Usuario Creador: "
        '
        'lbNombreUsuario
        '
        Me.lbNombreUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbNombreUsuario.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbNombreUsuario.Location = New System.Drawing.Point(512, 400)
        Me.lbNombreUsuario.Name = "lbNombreUsuario"
        Me.lbNombreUsuario.Size = New System.Drawing.Size(224, 23)
        Me.lbNombreUsuario.TabIndex = 9
        Me.lbNombreUsuario.Text = "Nombre"
        '
        'lbID
        '
        Me.lbID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbID.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbID.Location = New System.Drawing.Point(56, 376)
        Me.lbID.Name = "lbID"
        Me.lbID.Size = New System.Drawing.Size(24, 23)
        Me.lbID.TabIndex = 10
        Me.lbID.Text = "0"
        '
        'adPaqueteDetalle
        '
        Me.adPaqueteDetalle.SelectCommand = Me.SqlSelectCommand1
        Me.adPaqueteDetalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "tb_S_Paquete_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("IdPaquete", "IdPaquete")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id, Codigo, Cantidad, Descripcion, IdPaquete FROM tb_S_Paquete_Detalle"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection2
        '
        'SqlConnection2
        '
        Me.SqlConnection2.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(24, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 23)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Nombre Paquete : "
        '
        'frmPaquete
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(744, 426)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lbID)
        Me.Controls.Add(Me.lbNombreUsuario)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.dgPaqueteDetalle)
        Me.Controls.Add(Me.btQuitarProducto)
        Me.Controls.Add(Me.btAgregarProducto)
        Me.Controls.Add(Me.cbDesactivado)
        Me.Controls.Add(Me.txNombrePaquete)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(760, 464)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(760, 464)
        Me.Name = "frmPaquete"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Paquete"
        CType(Me.dgPaqueteDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsPaquetes1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmPaquete_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        spIniciarForm()
    End Sub
    Private Sub frmPaquete_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Me.priCambio = True Then
            If MsgBox("�Desea guardar los cambios efectuados?", MsgBoxStyle.YesNo, "Atenci�n") = MsgBoxResult.Yes Then
                If Not Me.fnGuardar() Then
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Private Sub spIniciarForm()
        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        strConexion = SqlConnection1.ConnectionString
        If Nuevo Then
            Me.cbDesactivado.Enabled = False
            Me.lbID.Text = ""
            Me.lbNombreUsuario.Text = ""
        Else
            spCargarDatos()
        End If
        priCambio = False
    End Sub
    Private Sub spCargarDatos()
        Dim Fx As New cFunciones
        Dim dt As New DataTable
        Dim sql As String
        Dim sql2 As String

        Try

            sql = " SELECT dbo.tb_S_Paquetes.Id, dbo.tb_S_Paquetes.Nombre, dbo.tb_S_Paquetes.Estado, dbo.Usuarios.Nombre AS NombreUsuario " & _
                  " FROM dbo.tb_S_Paquetes INNER JOIN dbo.Usuarios ON dbo.tb_S_Paquetes.IdUsuario = dbo.Usuarios.Id_Usuario WHERE dbo.tb_S_Paquetes.Id = " & Me.IdPaquete

            sql2 = "SELECT [Codigo],[Cantidad] ,[Descripcion] ,[IdPaquete]  FROM [Seepos].[dbo].[tb_S_Paquete_Detalle] where [IdPaquete] = " & Me.IdPaquete
            Fx.Cargar_Tabla_Generico(Me.adPaqueteDetalle, sql2, strConexion)
            adPaqueteDetalle.Fill(DtsPaquetes1.Tables("PaqueteDetalle"))
            Fx.Llenar_Tabla_Generico(sql, dt, strConexion)

            If dt.Rows.Count > 0 Then
                Me.txNombrePaquete.Text = dt.Rows(0).Item("Nombre")
                Me.cbDesactivado.Checked = dt.Rows(0).Item("Estado")
                Me.lbID.Text = dt.Rows(0).Item("Id")
                Me.lbNombreUsuario.Text = dt.Rows(0).Item("NombreUsuario")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub spAgregarArticulo()
        Dim BuscarArticulo As New frmBuscarArt
        BuscarArticulo.DtsPaquetes1 = DtsPaquetes1
        If BuscarArticulo.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim row As dtsPaquetes.PaqueteDetalleRow
            row = DtsPaquetes1.Tables("PaqueteDetalle").NewRow
            row("Codigo") = BuscarArticulo.Codigo
            row("Cantidad") = BuscarArticulo.Cantidad
            row("Descripcion") = BuscarArticulo.Descripcion
            Me.DtsPaquetes1.PaqueteDetalle.Rows.Add(row)
            priCambio = True
        End If

    End Sub
    Private Sub spCargarArticulo(ByVal Codigo As String)
        Dim Fx As New cFunciones
        Dim dt As New DataTable
        Dim sql As String

        Try
            sql = " SELECT dbo.tb_S_Paquetes.Id, dbo.tb_S_Paquetes.Nombre, dbo.tb_S_Paquetes.Estado, dbo.Usuarios.Nombre AS NombreUsuario " & _
                  " FROM dbo.tb_S_Paquetes INNER JOIN dbo.Usuarios ON dbo.tb_S_Paquetes.IdUsuario = dbo.Usuarios.Id_Usuario WHERE dbo.tb_S_Paquetes.Id = " & Me.IdPaquete

            Fx.Llenar_Tabla_Generico(sql, dt, strConexion)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btAgregarProducto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAgregarProducto.Click
        spAgregarArticulo()
    End Sub

    Private Sub btQuitarProducto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btQuitarProducto.Click
        If Me.dgPaqueteDetalle.CurrentRowIndex >= 0 Then
            If MsgBox("�Desea eliminar el producto?", MsgBoxStyle.YesNo, "Atenci�n") = MsgBoxResult.Yes Then
                Me.spEliminarproducto()
            End If
        End If
    End Sub

    Private Sub spEliminarproducto()
        Dim Id As Integer
        Try
            If Me.dgPaqueteDetalle.CurrentRowIndex >= 0 Then
                Id = Me.dgPaqueteDetalle.Item(dgPaqueteDetalle.CurrentCell.RowNumber, 0)
                For i As Integer = 0 To DtsPaquetes1.PaqueteDetalle.Rows.Count - 1
                    If i >= DtsPaquetes1.PaqueteDetalle.Rows.Count Then
                        Exit For
                    End If
                    If Not DtsPaquetes1.PaqueteDetalle(i).RowState = DataRowState.Deleted Then
                        If DtsPaquetes1.PaqueteDetalle(i).Codigo = Id Then
                            DtsPaquetes1.PaqueteDetalle.Rows(i).Delete()
                            i = i - 1
                            priCambio = True
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Me.fnGuardar()
    End Sub

    Private Function fnGuardar() As Boolean
        Dim sql As New SqlClient.SqlCommand
        Dim cmd As New SqlClient.SqlCommand
        Dim SQlInsert As String
        Dim Fx As cFunciones

        If Me.fnValidarCampos Then
            If Me.dgPaqueteDetalle.CurrentRowIndex >= 0 Then

                Try
                    If Not Nuevo Then
                        If fnValidarPromocion(Me.IdPaquete) Then
                            sql.CommandText = "UPDATE [Seepos].[dbo].[tb_S_Paquetes] SET [Nombre] = '" & Me.txNombrePaquete.Text & "' ,[Estado] = '" & Me.cbDesactivado.Checked & "'  WHERE Id = " & Me.IdPaquete
                            Fx.fnEjecutar(sql, strConexion)
                        Else
                            Return False
                        End If

                    Else
                        Dim sqlPaquete As String
                        Dim dt As New DataTable

                        sqlPaquete = "INSERT INTO [Seepos].[dbo].[tb_S_Paquetes] ([Nombre] ,[Fecha] ,[IdUsuario] ,[Estado]) " & _
                                          " VALUES ( '" & Me.txNombrePaquete.Text & "','" & Now & "','" & usua.Cedula & "','" & Me.cbDesactivado.Checked & "')"
                        sql.CommandText = sqlPaquete
                        Fx.fnEjecutar(sql, strConexion)

                        sqlPaquete = "select isNull(Max(Id),0) as Id from tb_S_Paquetes"
                        Fx.Llenar_Tabla_Generico(sqlPaquete, dt, strConexion)
                        If dt.Rows.Count > 0 Then
                            Me.IdPaquete = dt.Rows(0).Item("Id")
                            Me.lbID.Text = dt.Rows(0).Item("Id")
                            Me.lbNombreUsuario.Text = usua.Nombre
                        End If
                    End If

                    cmd.CommandText = "DELETE FROM [Seepos].[dbo].[tb_S_Paquete_Detalle] WHERE IdPaquete = " & Me.IdPaquete
                    Fx.fnEjecutar(cmd, strConexion)

                    For i As Integer = 0 To DtsPaquetes1.Tables("PaqueteDetalle").Rows.Count - 1
                        If Not DtsPaquetes1.Tables("PaqueteDetalle").Rows(i).RowState = DataRowState.Deleted Then
                            SQlInsert += "INSERT INTO [Seepos].[dbo].[tb_S_Paquete_Detalle] ([Codigo] ,[Cantidad] ,[Descripcion] ,[IdPaquete])" & _
                            " VALUES ( " & DtsPaquetes1.Tables("PaqueteDetalle").Rows(i).Item("Codigo") & _
                            "," & DtsPaquetes1.Tables("PaqueteDetalle").Rows(i).Item("Cantidad") & _
                            ",'" & DtsPaquetes1.Tables("PaqueteDetalle").Rows(i).Item("Descripcion") & _
                            "'," & Me.lbID.Text & ")     "
                        End If
                    Next
                    cmd.CommandText = SQlInsert
                    Fx.fnEjecutar(cmd, strConexion)
                    If Nuevo Then
                        Me.Text = "Editar Paquete"
                        Me.cbDesactivado.Enabled = True
                        MsgBox("La informaci�n se guard� exitosamente.", MsgBoxStyle.Information, "Atenci�n")
                    Else
                        MsgBox("La informaci�n se actualiz� exitosamente.", MsgBoxStyle.Information, "Atenci�n")
                    End If
                    Nuevo = False
                    Me.priCambio = False
                    Return True
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Return False
                End Try

            Else
                MsgBox("Por favor ingrese un producto.", MsgBoxStyle.Information, "Atenci�n")
                Return False
            End If
        End If
        Return False
    End Function

    Private Function fnValidarPromocion(ByVal _IdPaquete As Integer) As Boolean
        Dim sql As String
        Dim dt As New DataTable
        Dim Fx As New cFunciones
        Try
            If Me.cbDesactivado.Checked Then
                sql = "Select * From tb_S_Promociones where  Estado = 0 AND IdPaquete = '" & _IdPaquete & "'"

                Fx.Llenar_Tabla_Generico(sql, dt, strConexion)

                If dt.Rows.Count > 0 Then
                    Me.txNombrePaquete.Focus()
                    MsgBox("No puede desactivar un paquete ligado a una promoci�n activa.", MsgBoxStyle.Information, "Atenci�n")
                    Return False
                End If
            End If

        Catch ex As Exception
            Return False
            MsgBox(ex.Message)
        End Try

        Return True

    End Function
    Private Function fnValidarCampos()
        Dim sql As String
        Dim dt As New DataTable
        Dim Fx As New cFunciones
        Try


            If Replace(Me.txNombrePaquete.Text, " ", "") = "" Then
                Me.txNombrePaquete.BackColor = BackColor.Bisque
                Me.txNombrePaquete.Focus()
                MsgBox("Por favor, completar los campos obligatorios.", MsgBoxStyle.Information, "Atenci�n")
                Return False
            Else
                Me.txNombrePaquete.BackColor = BackColor.White
            End If

            If Nuevo Then
                sql = "Select * From tb_S_Paquetes where  Nombre collate SQL_Latin1_General_Cp1_CI_AI = '" & Me.txNombrePaquete.Text & "'"
            Else
                sql = "Select * From tb_S_Paquetes where Id <> " & Me.IdPaquete & " AND Nombre collate SQL_Latin1_General_Cp1_CI_AI = '" & Me.txNombrePaquete.Text & "'"
            End If

            Fx.Llenar_Tabla_Generico(sql, dt, strConexion)

            If dt.Rows.Count > 0 Then
                Me.txNombrePaquete.BackColor = BackColor.Bisque
                Me.txNombrePaquete.Focus()
                MsgBox("Los datos ya existen.", MsgBoxStyle.Information, "Atenci�n")
                Return False
            Else
                Me.txNombrePaquete.BackColor = BackColor.White
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return True
    End Function



    Private Sub txNombrePaquete_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txNombrePaquete.TextChanged
        Dim str As String = "01234567890abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ �����"
        For Each c As Char In Me.txNombrePaquete.Text.ToCharArray()
            If InStr(1, str, c) = 0 Then
                Me.txNombrePaquete.Text = Replace(Me.txNombrePaquete.Text, c, "")
            End If
        Next
        priCambio = True
    End Sub

    Private Sub cbDesactivado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDesactivado.CheckedChanged
        priCambio = True
    End Sub

    Private Sub txNombrePaquete_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txNombrePaquete.KeyPress

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            Exit Sub
        End If
        If Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
            Exit Sub
        End If
        If Not (Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub
End Class
