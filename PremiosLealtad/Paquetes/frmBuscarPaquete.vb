Imports System.Windows.Forms

Public Class frmBuscarPaquete
    Inherits System.Windows.Forms.Form

    Dim usua As Usuario_Logeado
    Dim PMU As New PerfilModulo_Class
    Dim NuevaConexion As String
#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New(ByVal Usuario As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txBusqueda As System.Windows.Forms.TextBox
    Friend WithEvents btBuscar As System.Windows.Forms.Button
    Friend WithEvents btCrear As System.Windows.Forms.Button
    Friend WithEvents btEditar As System.Windows.Forms.Button
    Friend WithEvents btImprimirLista As System.Windows.Forms.Button
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection

    Friend WithEvents adPaquetes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dbPaquetes As System.Windows.Forms.DataGrid
    Friend WithEvents adPaqueteDetalle As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DtsPaquetes1 As LcPymes_5._2.dtsPaquetes
    Friend WithEvents DataGridTableStyle1 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents DataGridTextBoxColumn2 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn3 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn4 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn5 As System.Windows.Forms.DataGridTextBoxColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.txBusqueda = New System.Windows.Forms.TextBox
        Me.btBuscar = New System.Windows.Forms.Button
        Me.dbPaquetes = New System.Windows.Forms.DataGrid
        Me.DtsPaquetes1 = New LcPymes_5._2.dtsPaquetes
        Me.DataGridTableStyle1 = New System.Windows.Forms.DataGridTableStyle
        Me.DataGridTextBoxColumn2 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn3 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn4 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.btCrear = New System.Windows.Forms.Button
        Me.btEditar = New System.Windows.Forms.Button
        Me.btImprimirLista = New System.Windows.Forms.Button
        Me.adPaquetes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.adPaqueteDetalle = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.DataGridTextBoxColumn5 = New System.Windows.Forms.DataGridTextBoxColumn
        CType(Me.dbPaquetes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsPaquetes1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Buscar : "
        '
        'txBusqueda
        '
        Me.txBusqueda.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txBusqueda.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txBusqueda.Location = New System.Drawing.Point(72, 24)
        Me.txBusqueda.Name = "txBusqueda"
        Me.txBusqueda.Size = New System.Drawing.Size(416, 23)
        Me.txBusqueda.TabIndex = 1
        Me.txBusqueda.Text = ""
        '
        'btBuscar
        '
        Me.btBuscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btBuscar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btBuscar.Location = New System.Drawing.Point(544, 24)
        Me.btBuscar.Name = "btBuscar"
        Me.btBuscar.TabIndex = 2
        Me.btBuscar.Text = "Buscar"
        '
        'dbPaquetes
        '
        Me.dbPaquetes.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.dbPaquetes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dbPaquetes.DataMember = ""
        Me.dbPaquetes.DataSource = Me.DtsPaquetes1.Paquetes
        Me.dbPaquetes.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dbPaquetes.Location = New System.Drawing.Point(16, 64)
        Me.dbPaquetes.Name = "dbPaquetes"
        Me.dbPaquetes.PreferredColumnWidth = 170
        Me.dbPaquetes.ReadOnly = True
        Me.dbPaquetes.RowHeaderWidth = 30
        Me.dbPaquetes.Size = New System.Drawing.Size(712, 288)
        Me.dbPaquetes.TabIndex = 3
        Me.dbPaquetes.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.DataGridTableStyle1})
        '
        'DtsPaquetes1
        '
        Me.DtsPaquetes1.DataSetName = "dtsPaquetes"
        Me.DtsPaquetes1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'DataGridTableStyle1
        '
        Me.DataGridTableStyle1.DataGrid = Me.dbPaquetes
        Me.DataGridTableStyle1.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.DataGridTextBoxColumn5, Me.DataGridTextBoxColumn2, Me.DataGridTextBoxColumn3, Me.DataGridTextBoxColumn4})
        Me.DataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGridTableStyle1.MappingName = "Paquetes"
        Me.DataGridTableStyle1.RowHeaderWidth = 30
        '
        'DataGridTextBoxColumn2
        '
        Me.DataGridTextBoxColumn2.Format = ""
        Me.DataGridTextBoxColumn2.FormatInfo = Nothing
        Me.DataGridTextBoxColumn2.HeaderText = "Nombre"
        Me.DataGridTextBoxColumn2.MappingName = "Nombre"
        Me.DataGridTextBoxColumn2.Width = 260
        '
        'DataGridTextBoxColumn3
        '
        Me.DataGridTextBoxColumn3.Format = ""
        Me.DataGridTextBoxColumn3.FormatInfo = Nothing
        Me.DataGridTextBoxColumn3.HeaderText = "Creado por"
        Me.DataGridTextBoxColumn3.MappingName = "Creado por"
        Me.DataGridTextBoxColumn3.Width = 200
        '
        'DataGridTextBoxColumn4
        '
        Me.DataGridTextBoxColumn4.Format = "dd/mm/yyyy"
        Me.DataGridTextBoxColumn4.FormatInfo = Nothing
        Me.DataGridTextBoxColumn4.HeaderText = "Fecha Creaci�n"
        Me.DataGridTextBoxColumn4.MappingName = "Fecha Creacion"
        Me.DataGridTextBoxColumn4.Width = 148
        '
        'btCrear
        '
        Me.btCrear.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btCrear.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btCrear.Location = New System.Drawing.Point(80, 376)
        Me.btCrear.Name = "btCrear"
        Me.btCrear.Size = New System.Drawing.Size(104, 23)
        Me.btCrear.TabIndex = 4
        Me.btCrear.Text = "Crear"
        '
        'btEditar
        '
        Me.btEditar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btEditar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEditar.Location = New System.Drawing.Point(320, 376)
        Me.btEditar.Name = "btEditar"
        Me.btEditar.Size = New System.Drawing.Size(104, 23)
        Me.btEditar.TabIndex = 5
        Me.btEditar.Text = "Editar"
        '
        'btImprimirLista
        '
        Me.btImprimirLista.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btImprimirLista.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btImprimirLista.Location = New System.Drawing.Point(560, 376)
        Me.btImprimirLista.Name = "btImprimirLista"
        Me.btImprimirLista.Size = New System.Drawing.Size(104, 23)
        Me.btImprimirLista.TabIndex = 6
        Me.btImprimirLista.Text = "Imprimir Lista"
        '
        'adPaquetes
        '
        Me.adPaquetes.SelectCommand = Me.SqlSelectCommand1
        Me.adPaquetes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "tb_S_Paquetes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("IdUsuario", "IdUsuario"), New System.Data.Common.DataColumnMapping("Estado", "Estado")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id, Nombre, Fecha, IdUsuario, Estado FROM tb_S_Paquetes"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""PURAVIDA-PC"";packet size=4096;integrated security=SSPI;data sourc" & _
        "e=""PURAVIDA-PC\RESTAURANTE"";persist security info=False;initial catalog=Seepos"
        '
        'adPaqueteDetalle
        '
        Me.adPaqueteDetalle.SelectCommand = Me.SqlSelectCommand2
        Me.adPaqueteDetalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "tb_S_Paquete_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("IdPaquete", "IdPaquete"), New System.Data.Common.DataColumnMapping("Id", "Id")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Codigo, Cantidad, Descripcion, IdPaquete, Id FROM tb_S_Paquete_Detalle"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'DataGridTextBoxColumn5
        '
        Me.DataGridTextBoxColumn5.Format = ""
        Me.DataGridTextBoxColumn5.FormatInfo = Nothing
        Me.DataGridTextBoxColumn5.HeaderText = "ID"
        Me.DataGridTextBoxColumn5.MappingName = "Id"
        Me.DataGridTextBoxColumn5.Width = 70
        '
        'frmBuscarPaquete
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(744, 426)
        Me.Controls.Add(Me.btImprimirLista)
        Me.Controls.Add(Me.btEditar)
        Me.Controls.Add(Me.btCrear)
        Me.Controls.Add(Me.dbPaquetes)
        Me.Controls.Add(Me.btBuscar)
        Me.Controls.Add(Me.txBusqueda)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(760, 464)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(760, 464)
        Me.Name = "frmBuscarPaquete"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Paquetes"
        CType(Me.dbPaquetes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsPaquetes1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub frmBuscarPaquete_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = IIf(NuevaConexion = "", GetSetting("SeeSOFT", "SeePos", "Conexion"), NuevaConexion)
        PMU = VSM(usua.Cedula, Me.Name)  'Carga los privilegios del usuario con el modulo  

        Me.spBusqueda()

    End Sub
    Private Sub spBusqueda()
        Dim Fx As New cFunciones
        Dim sql As String
        sql = "SELECT Id ,Nombre, Fecha as [Fecha Creacion] , NombreUsuario as [Creado por], Estado FROM [Seepos].[dbo].[vs_S_Paquetes] " & _
        " where Nombre LIKE '%" & Me.txBusqueda.Text & "%' or NombreUsuario LIKE '%" & Me.txBusqueda.Text & "%'"
        Dim strConexion As String = SqlConnection1.ConnectionString
        DtsPaquetes1.Tables("Paquetes").Clear()
        DtsPaquetes1.Tables("PaqueteDetalle").Clear()
        Fx.Cargar_Tabla_Generico(Me.adPaquetes, sql, strConexion)  'JCGA 19032007
        Me.adPaquetes.Fill(Me.DtsPaquetes1, "Paquetes")
        Me.adPaqueteDetalle.Fill(Me.DtsPaquetes1, "PaqueteDetalle")

    End Sub

    Private Sub btCrear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCrear.Click
        If PMU.Update Then spCrearPaquete() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub

    Private Sub spCrearPaquete()
        Dim frm As New frmPaquete(usua)
        Me.Visible = False
        frm.Text = "Crear Paquete"
        frm.Nuevo = True
        frm.ShowDialog()
        Me.Visible = True
        Me.spBusqueda()
    End Sub



    Private Sub btEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btEditar.Click
        If PMU.Update Then spEditar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub

    Private Sub spEditar()
        If Me.dbPaquetes.CurrentRowIndex >= 0 Then
            Me.Visible = False

            Dim Id As Integer
            Id = Trim(DtsPaquetes1.Tables("Paquetes").Rows(Me.dbPaquetes.CurrentRowIndex).Item("Id"))

            Dim frm As New frmPaquete(usua)
            frm.IdPaquete = Id
            frm.Nuevo = False
            frm.Text = "Editar Paquete"
            frm.ShowDialog()
            Me.Visible = True
            Me.txBusqueda.Text = ""
            Me.spBusqueda()
        End If
    End Sub

    Private Sub btBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btBuscar.Click
        If PMU.Find Then Me.spBusqueda() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub


    Private Sub dbPaquetes_(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dbPaquetes.DoubleClick
        If PMU.Update Then spEditar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub

    Private Sub txBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As KeyEventArgs) Handles txBusqueda.KeyUp
        If PMU.Find Then Me.spBusqueda() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub

    Private Sub btImprimirLista_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btImprimirLista.Click
        If PMU.Print Then spImprimir() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
    End Sub
    Private Sub spImprimir()
        Me.btImprimirLista.Enabled = False
        If pbReporteImpreso = False Then
            Me.spVerReporte()
        End If
        If Me.Visor.IsDisposed Then
            Visor = New frmContenedor
            Me.spVerReporte()
        End If
        If pbReporteImpreso Then
            Me.Visor.BringToFront()
            Me.Visor.WindowState = FormWindowState.Maximized
            Me.Visor.Show()
        End If
        Me.btImprimirLista.Enabled = True
    End Sub
    Dim Visor As New frmContenedor
    Dim pbReporteImpreso As Boolean
    Private Sub spVerReporte()

        Try
            Dim rpt As New rptPaquetes

            rpt.Refresh()
            rpt.SetDataSource(Me.DtsPaquetes1)
            CrystalReportsConexion.LoadReportViewer(Visor.VisorReporte, rpt)
            Visor.Text = "Reporte de Paquetes"
            Visor.Show()
            Visor.WindowState = FormWindowState.Maximized
            pbReporteImpreso = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

End Class
