﻿Imports System.Data
Imports System.Data.SqlClient

Public Class GenerarConsecutivoHA

	Private _suc As String
	Private _ptoVenta As String
	Private _tipoDoc As String
	Private _consecutivoHA As String
	Private _numConsecutiva As String
	Private _consecutivoHA_Generado As String
	Dim newConsecutivo As String
	Dim strQuery As String
	Dim strUpdate As String
	Dim CConexion As New Conexion
	Dim sqlConexion As New SqlConnection
	Dim random = New Random()

	Public Sub crearNumeroConsecutivo(ByVal _CodCliente As String, ByVal idFactura As Integer, ByVal _numFactura As Integer, ByVal _fecha As DateTime, Optional ByVal _esAnulacion As Boolean = False, Optional ByVal _esNotaDebito As Boolean = False)


		Dim dt As New DataTable
		Dim cmd As String
		cmd = "Select Consecutivo_HA_FE,Consecutivo_HA_TIQE,Consecutivo_HA_NCE,Consecutivo_HA_NDE,No_Sucursal,No_Caja,Prefijo_Pais,NumeroIdEmisor  From dbo.TB_CE_Cofiguracion, dbo.VS_CE_InfoEmisor"

		cFunciones.Llenar_Tabla_Generico(cmd, dt, GetSetting("SeeSOFT", "Seepos", "CONEXION"))



		Dim columna As String = "Consecutivo_HA_FE"
		For i As Integer = 0 To dt.Rows.Count - 1
			If _CodCliente.Equals("0") Then
				_tipoDoc = "04"
				_consecutivoHA = dt.Rows(i).Item("Consecutivo_HA_TIQE")
				columna = "Consecutivo_HA_TIQE"
			ElseIf _CodCliente <> "0" Then
				_tipoDoc = "01"
				_consecutivoHA = dt.Rows(i).Item("Consecutivo_HA_FE")
				columna = "Consecutivo_HA_FE"
			End If

			If _esAnulacion = True Then
				_tipoDoc = "03"
				_consecutivoHA = dt.Rows(i).Item("Consecutivo_HA_NCE")
				columna = "Consecutivo_HA_NCE"
			End If

			If _esNotaDebito = True Then
				_tipoDoc = "02"
				_consecutivoHA = dt.Rows(i).Item("Consecutivo_HA_NDE")
				columna = "Consecutivo_HA_NDE"
			End If

			_suc = dt.Rows(i).Item("No_Sucursal")
			_ptoVenta = dt.Rows(i).Item("No_Caja")
		Next

		_numConsecutiva = _suc + _ptoVenta + _tipoDoc + _consecutivoHA

		_consecutivoHA_Generado = _numConsecutiva

		Dim clave As String
		clave = crearClaveNumerica(dt.Rows(0).Item("Prefijo_Pais"), _fecha, dt.Rows(0).Item("NumeroIdEmisor"))
		strQuery = "INSERT INTO dbo.TB_CE_ConsecutivoProvisionalHA (Id_Factura,Num_Factura,Consecutivo_Hacienda,TipoDocElectronico,Clave)  VALUES ('" & idFactura & "','" & _numFactura & "','" & _numConsecutiva & "','" & _tipoDoc & "','" & clave & "')"


		_consecutivoHA = _consecutivoHA + 1

		strQuery &= " UPDATE [TB_CE_Cofiguracion]  SET " & columna & " =  '" & agregarCeros(_consecutivoHA, 10) & "'"

		Dim cmdUpdate As New SqlCommand
		cmdUpdate.CommandText = strQuery
		cFunciones.spEjecutar(cmdUpdate, GetSetting("SeeSOFT", "Seepos", "CONEXION"))
	End Sub
	Function agregarCeros(Numero As String, tama As Integer) As String
		Dim cuantos As Integer = tama - Numero.Length
		Dim resultado As String = ""
		For i As Integer = 1 To cuantos
			resultado &= "0"
		Next
		resultado &= Numero
		Return resultado
	End Function
	Public Function crearClaveNumerica(ByVal _codigoPais As String, ByVal _fechaEmision As DateTime, ByVal _cedula As String) As String
		Dim _claveNumerica As String
		Dim randomNumber As Integer = random.Next(0, 99999999)
		_claveNumerica = _codigoPais & _fechaEmision.ToString("dd") & _fechaEmision.ToString("MM") & _fechaEmision.ToString("yy") & _cedula.PadLeft(12, "0") & _consecutivoHA_Generado & "1" & randomNumber.ToString()

		If _claveNumerica.Length < 50 Then
			_claveNumerica.PadRight(50, "0")
		End If
		Return _claveNumerica
	End Function
End Class
