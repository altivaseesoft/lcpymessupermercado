Public Class frmArqueo
    Inherits System.Windows.Forms.Form

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTotalVentas As System.Windows.Forms.TextBox
    Friend WithEvents txtFondoAperturaCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents grpControles As System.Windows.Forms.GroupBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents lbUsuario As System.Windows.Forms.Label
    Friend WithEvents grpDesgloce As System.Windows.Forms.GroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSalidaColones As System.Windows.Forms.TextBox
    Friend WithEvents txtSalidaDolares As System.Windows.Forms.TextBox
    Friend WithEvents txtPagoComprasCol As System.Windows.Forms.TextBox
    Friend WithEvents txtDolaresDep As System.Windows.Forms.TextBox
    Friend WithEvents txtColonesDep As System.Windows.Forms.TextBox
    Friend WithEvents txtTarjetaDolares As System.Windows.Forms.TextBox
    Friend WithEvents txtTarjetaColones As System.Windows.Forms.TextBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtEntradaColones As System.Windows.Forms.TextBox
    Friend WithEvents txtEntradasDolares As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtIDApertura As System.Windows.Forms.TextBox
    Friend WithEvents txtIdArqueo As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtEstado As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents grbEncabezado As System.Windows.Forms.GroupBox
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents dtpFApertura As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtAbonosCxC As System.Windows.Forms.TextBox
    Friend WithEvents lbNombreCajero As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dtpFArqueo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtTotalCredito As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtTotalContado As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmArqueo))
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtTotalVentas = New System.Windows.Forms.TextBox
        Me.txtFondoAperturaCaja = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtSalidaColones = New System.Windows.Forms.TextBox
        Me.txtEntradaColones = New System.Windows.Forms.TextBox
        Me.txtSalidaDolares = New System.Windows.Forms.TextBox
        Me.txtEntradasDolares = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtPagoComprasCol = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtDolaresDep = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtColonesDep = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtClave = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.btnNuevo = New System.Windows.Forms.Button
        Me.grpControles = New System.Windows.Forms.GroupBox
        Me.btnAnular = New System.Windows.Forms.Button
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.btnGuardar = New System.Windows.Forms.Button
        Me.btnBuscar = New System.Windows.Forms.Button
        Me.lbUsuario = New System.Windows.Forms.Label
        Me.grpDesgloce = New System.Windows.Forms.GroupBox
        Me.txtTotalContado = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtTotalCredito = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtAbonosCxC = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtTarjetaDolares = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtTarjetaColones = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtIDApertura = New System.Windows.Forms.TextBox
        Me.txtIdArqueo = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtEstado = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.grbEncabezado = New System.Windows.Forms.GroupBox
        Me.dtpFArqueo = New System.Windows.Forms.DateTimePicker
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.lbNombreCajero = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtObservaciones = New System.Windows.Forms.TextBox
        Me.dtpFApertura = New System.Windows.Forms.DateTimePicker
        Me.grpControles.SuspendLayout()
        Me.grpDesgloce.SuspendLayout()
        Me.grbEncabezado.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 23)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Total Ventas:"
        '
        'txtTotalVentas
        '
        Me.txtTotalVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalVentas.Location = New System.Drawing.Point(192, 40)
        Me.txtTotalVentas.Name = "txtTotalVentas"
        Me.txtTotalVentas.ReadOnly = True
        Me.txtTotalVentas.Size = New System.Drawing.Size(208, 23)
        Me.txtTotalVentas.TabIndex = 5
        Me.txtTotalVentas.Text = "0"
        Me.txtTotalVentas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFondoAperturaCaja
        '
        Me.txtFondoAperturaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFondoAperturaCaja.Location = New System.Drawing.Point(192, 16)
        Me.txtFondoAperturaCaja.Name = "txtFondoAperturaCaja"
        Me.txtFondoAperturaCaja.ReadOnly = True
        Me.txtFondoAperturaCaja.Size = New System.Drawing.Size(208, 23)
        Me.txtFondoAperturaCaja.TabIndex = 4
        Me.txtFondoAperturaCaja.Text = "0"
        Me.txtFondoAperturaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 23)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Fondo Caja:"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(16, 160)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(168, 23)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Total Salidas Colones:"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 440)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(168, 23)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Total Entradas Colones:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label4.Visible = False
        '
        'txtSalidaColones
        '
        Me.txtSalidaColones.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalidaColones.Location = New System.Drawing.Point(192, 160)
        Me.txtSalidaColones.Name = "txtSalidaColones"
        Me.txtSalidaColones.ReadOnly = True
        Me.txtSalidaColones.Size = New System.Drawing.Size(208, 23)
        Me.txtSalidaColones.TabIndex = 6
        Me.txtSalidaColones.Text = "0"
        Me.txtSalidaColones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtEntradaColones
        '
        Me.txtEntradaColones.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntradaColones.Location = New System.Drawing.Point(184, 440)
        Me.txtEntradaColones.Name = "txtEntradaColones"
        Me.txtEntradaColones.ReadOnly = True
        Me.txtEntradaColones.Size = New System.Drawing.Size(208, 23)
        Me.txtEntradaColones.TabIndex = 7
        Me.txtEntradaColones.Text = "0"
        Me.txtEntradaColones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEntradaColones.Visible = False
        '
        'txtSalidaDolares
        '
        Me.txtSalidaDolares.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalidaDolares.Location = New System.Drawing.Point(192, 192)
        Me.txtSalidaDolares.Name = "txtSalidaDolares"
        Me.txtSalidaDolares.ReadOnly = True
        Me.txtSalidaDolares.Size = New System.Drawing.Size(208, 23)
        Me.txtSalidaDolares.TabIndex = 8
        Me.txtSalidaDolares.Text = "0"
        Me.txtSalidaDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtEntradasDolares
        '
        Me.txtEntradasDolares.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntradasDolares.Location = New System.Drawing.Point(184, 408)
        Me.txtEntradasDolares.Name = "txtEntradasDolares"
        Me.txtEntradasDolares.ReadOnly = True
        Me.txtEntradasDolares.Size = New System.Drawing.Size(208, 23)
        Me.txtEntradasDolares.TabIndex = 9
        Me.txtEntradasDolares.Text = "0"
        Me.txtEntradasDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEntradasDolares.Visible = False
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 408)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(152, 23)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Total Entradas Dolares:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label5.Visible = False
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(16, 192)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(168, 23)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Total Salidas Dolares:"
        '
        'txtPagoComprasCol
        '
        Me.txtPagoComprasCol.BackColor = System.Drawing.Color.Bisque
        Me.txtPagoComprasCol.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPagoComprasCol.Location = New System.Drawing.Point(192, 224)
        Me.txtPagoComprasCol.Name = "txtPagoComprasCol"
        Me.txtPagoComprasCol.Size = New System.Drawing.Size(208, 23)
        Me.txtPagoComprasCol.TabIndex = 0
        Me.txtPagoComprasCol.Text = "0"
        Me.txtPagoComprasCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(16, 224)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(168, 23)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Total Pagos Compras Col:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDolaresDep
        '
        Me.txtDolaresDep.BackColor = System.Drawing.Color.Bisque
        Me.txtDolaresDep.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDolaresDep.Location = New System.Drawing.Point(192, 288)
        Me.txtDolaresDep.Name = "txtDolaresDep"
        Me.txtDolaresDep.Size = New System.Drawing.Size(208, 23)
        Me.txtDolaresDep.TabIndex = 3
        Me.txtDolaresDep.Text = "0"
        Me.txtDolaresDep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(16, 288)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(168, 23)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Total Dolares Depositar:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtColonesDep
        '
        Me.txtColonesDep.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtColonesDep.Location = New System.Drawing.Point(192, 320)
        Me.txtColonesDep.Name = "txtColonesDep"
        Me.txtColonesDep.ReadOnly = True
        Me.txtColonesDep.Size = New System.Drawing.Size(208, 23)
        Me.txtColonesDep.TabIndex = 17
        Me.txtColonesDep.Text = "0"
        Me.txtColonesDep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(16, 320)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(168, 23)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Total Colones Depositar:"
        '
        'txtClave
        '
        Me.txtClave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtClave.BackColor = System.Drawing.Color.Bisque
        Me.txtClave.Location = New System.Drawing.Point(488, 373)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtClave.Size = New System.Drawing.Size(96, 20)
        Me.txtClave.TabIndex = 0
        Me.txtClave.Text = ""
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.Location = New System.Drawing.Point(432, 373)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(56, 23)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Clave:"
        '
        'btnNuevo
        '
        Me.btnNuevo.Enabled = False
        Me.btnNuevo.Location = New System.Drawing.Point(8, 16)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 56)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "Nuevo"
        '
        'grpControles
        '
        Me.grpControles.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grpControles.Controls.Add(Me.btnAnular)
        Me.grpControles.Controls.Add(Me.btnImprimir)
        Me.grpControles.Controls.Add(Me.btnGuardar)
        Me.grpControles.Controls.Add(Me.btnBuscar)
        Me.grpControles.Controls.Add(Me.btnNuevo)
        Me.grpControles.Location = New System.Drawing.Point(8, 352)
        Me.grpControles.Name = "grpControles"
        Me.grpControles.Size = New System.Drawing.Size(408, 80)
        Me.grpControles.TabIndex = 1
        Me.grpControles.TabStop = False
        Me.grpControles.Text = "Controles"
        '
        'btnAnular
        '
        Me.btnAnular.Enabled = False
        Me.btnAnular.Location = New System.Drawing.Point(328, 16)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 56)
        Me.btnAnular.TabIndex = 4
        Me.btnAnular.Text = "Anular"
        '
        'btnImprimir
        '
        Me.btnImprimir.Enabled = False
        Me.btnImprimir.Location = New System.Drawing.Point(248, 16)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 56)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "Imprimir"
        '
        'btnGuardar
        '
        Me.btnGuardar.Enabled = False
        Me.btnGuardar.Location = New System.Drawing.Point(168, 16)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 56)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "Guardar"
        '
        'btnBuscar
        '
        Me.btnBuscar.Enabled = False
        Me.btnBuscar.Location = New System.Drawing.Point(88, 16)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 56)
        Me.btnBuscar.TabIndex = 1
        Me.btnBuscar.Text = "Buscar"
        '
        'lbUsuario
        '
        Me.lbUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbUsuario.Location = New System.Drawing.Point(432, 405)
        Me.lbUsuario.Name = "lbUsuario"
        Me.lbUsuario.Size = New System.Drawing.Size(224, 16)
        Me.lbUsuario.TabIndex = 4
        Me.lbUsuario.Text = "Usuario"
        '
        'grpDesgloce
        '
        Me.grpDesgloce.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grpDesgloce.Controls.Add(Me.txtTotalContado)
        Me.grpDesgloce.Controls.Add(Me.Label21)
        Me.grpDesgloce.Controls.Add(Me.txtTotalCredito)
        Me.grpDesgloce.Controls.Add(Me.Label20)
        Me.grpDesgloce.Controls.Add(Me.Label17)
        Me.grpDesgloce.Controls.Add(Me.txtAbonosCxC)
        Me.grpDesgloce.Controls.Add(Me.Label11)
        Me.grpDesgloce.Controls.Add(Me.txtTarjetaDolares)
        Me.grpDesgloce.Controls.Add(Me.Label12)
        Me.grpDesgloce.Controls.Add(Me.txtTarjetaColones)
        Me.grpDesgloce.Controls.Add(Me.Label3)
        Me.grpDesgloce.Controls.Add(Me.txtTotalVentas)
        Me.grpDesgloce.Controls.Add(Me.Label1)
        Me.grpDesgloce.Controls.Add(Me.txtFondoAperturaCaja)
        Me.grpDesgloce.Controls.Add(Me.txtSalidaColones)
        Me.grpDesgloce.Controls.Add(Me.txtEntradaColones)
        Me.grpDesgloce.Controls.Add(Me.txtSalidaDolares)
        Me.grpDesgloce.Controls.Add(Me.Label7)
        Me.grpDesgloce.Controls.Add(Me.txtPagoComprasCol)
        Me.grpDesgloce.Controls.Add(Me.Label6)
        Me.grpDesgloce.Controls.Add(Me.txtEntradasDolares)
        Me.grpDesgloce.Controls.Add(Me.Label5)
        Me.grpDesgloce.Controls.Add(Me.Label2)
        Me.grpDesgloce.Controls.Add(Me.Label4)
        Me.grpDesgloce.Controls.Add(Me.txtDolaresDep)
        Me.grpDesgloce.Controls.Add(Me.Label8)
        Me.grpDesgloce.Controls.Add(Me.txtColonesDep)
        Me.grpDesgloce.Controls.Add(Me.Label9)
        Me.grpDesgloce.Enabled = False
        Me.grpDesgloce.Location = New System.Drawing.Point(8, 0)
        Me.grpDesgloce.Name = "grpDesgloce"
        Me.grpDesgloce.Size = New System.Drawing.Size(408, 352)
        Me.grpDesgloce.TabIndex = 2
        Me.grpDesgloce.TabStop = False
        Me.grpDesgloce.Text = "Desgloce de Caja"
        '
        'txtTotalContado
        '
        Me.txtTotalContado.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalContado.Location = New System.Drawing.Point(192, 96)
        Me.txtTotalContado.Name = "txtTotalContado"
        Me.txtTotalContado.ReadOnly = True
        Me.txtTotalContado.Size = New System.Drawing.Size(208, 23)
        Me.txtTotalContado.TabIndex = 25
        Me.txtTotalContado.Text = "0"
        Me.txtTotalContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(8, 96)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(112, 23)
        Me.Label21.TabIndex = 26
        Me.Label21.Text = "Total Contado:"
        '
        'txtTotalCredito
        '
        Me.txtTotalCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCredito.Location = New System.Drawing.Point(192, 64)
        Me.txtTotalCredito.Name = "txtTotalCredito"
        Me.txtTotalCredito.ReadOnly = True
        Me.txtTotalCredito.Size = New System.Drawing.Size(208, 23)
        Me.txtTotalCredito.TabIndex = 23
        Me.txtTotalCredito.Text = "0"
        Me.txtTotalCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(8, 64)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(112, 23)
        Me.Label20.TabIndex = 24
        Me.Label20.Text = "Total Cr�dito:"
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(8, 128)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(168, 23)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Total Abonos a CxC:"
        '
        'txtAbonosCxC
        '
        Me.txtAbonosCxC.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAbonosCxC.Location = New System.Drawing.Point(192, 128)
        Me.txtAbonosCxC.Name = "txtAbonosCxC"
        Me.txtAbonosCxC.ReadOnly = True
        Me.txtAbonosCxC.Size = New System.Drawing.Size(208, 23)
        Me.txtAbonosCxC.TabIndex = 21
        Me.txtAbonosCxC.Text = "0"
        Me.txtAbonosCxC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(8, 376)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(160, 23)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Total Tarjeta Dolares:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label11.Visible = False
        '
        'txtTarjetaDolares
        '
        Me.txtTarjetaDolares.BackColor = System.Drawing.Color.Bisque
        Me.txtTarjetaDolares.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTarjetaDolares.Location = New System.Drawing.Point(184, 376)
        Me.txtTarjetaDolares.Name = "txtTarjetaDolares"
        Me.txtTarjetaDolares.Size = New System.Drawing.Size(208, 23)
        Me.txtTarjetaDolares.TabIndex = 2
        Me.txtTarjetaDolares.Text = "0"
        Me.txtTarjetaDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTarjetaDolares.Visible = False
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(16, 256)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(160, 23)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Total Tarjeta Colones:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTarjetaColones
        '
        Me.txtTarjetaColones.BackColor = System.Drawing.Color.Bisque
        Me.txtTarjetaColones.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTarjetaColones.Location = New System.Drawing.Point(192, 256)
        Me.txtTarjetaColones.Name = "txtTarjetaColones"
        Me.txtTarjetaColones.Size = New System.Drawing.Size(208, 23)
        Me.txtTarjetaColones.TabIndex = 1
        Me.txtTarjetaColones.Text = "0"
        Me.txtTarjetaColones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(8, 48)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(64, 23)
        Me.Label13.TabIndex = 5
        Me.Label13.Text = "# Apertura:"
        '
        'txtIDApertura
        '
        Me.txtIDApertura.Location = New System.Drawing.Point(96, 48)
        Me.txtIDApertura.Name = "txtIDApertura"
        Me.txtIDApertura.ReadOnly = True
        Me.txtIDApertura.TabIndex = 6
        Me.txtIDApertura.Text = "0"
        Me.txtIDApertura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIdArqueo
        '
        Me.txtIdArqueo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdArqueo.Location = New System.Drawing.Point(96, 24)
        Me.txtIdArqueo.Name = "txtIdArqueo"
        Me.txtIdArqueo.ReadOnly = True
        Me.txtIdArqueo.TabIndex = 8
        Me.txtIdArqueo.Text = "0"
        Me.txtIdArqueo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(8, 24)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 23)
        Me.Label14.TabIndex = 7
        Me.Label14.Text = "# Arqueo:"
        '
        'txtEstado
        '
        Me.txtEstado.Location = New System.Drawing.Point(8, 72)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(200, 24)
        Me.txtEstado.TabIndex = 9
        Me.txtEstado.Text = "Estado:"
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(8, 120)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(88, 23)
        Me.Label15.TabIndex = 10
        Me.Label15.Text = "Fecha Apertura:"
        '
        'grbEncabezado
        '
        Me.grbEncabezado.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbEncabezado.Controls.Add(Me.dtpFArqueo)
        Me.grbEncabezado.Controls.Add(Me.Label19)
        Me.grbEncabezado.Controls.Add(Me.Label18)
        Me.grbEncabezado.Controls.Add(Me.lbNombreCajero)
        Me.grbEncabezado.Controls.Add(Me.Label16)
        Me.grbEncabezado.Controls.Add(Me.txtObservaciones)
        Me.grbEncabezado.Controls.Add(Me.dtpFApertura)
        Me.grbEncabezado.Controls.Add(Me.Label15)
        Me.grbEncabezado.Controls.Add(Me.txtIdArqueo)
        Me.grbEncabezado.Controls.Add(Me.txtIDApertura)
        Me.grbEncabezado.Controls.Add(Me.Label13)
        Me.grbEncabezado.Controls.Add(Me.txtEstado)
        Me.grbEncabezado.Controls.Add(Me.Label14)
        Me.grbEncabezado.Enabled = False
        Me.grbEncabezado.Location = New System.Drawing.Point(424, 0)
        Me.grbEncabezado.Name = "grbEncabezado"
        Me.grbEncabezado.Size = New System.Drawing.Size(232, 352)
        Me.grbEncabezado.TabIndex = 11
        Me.grbEncabezado.TabStop = False
        Me.grbEncabezado.Text = "Informaci�n Arqueo"
        '
        'dtpFArqueo
        '
        Me.dtpFArqueo.CustomFormat = "dd/MM/yy HH:mm"
        Me.dtpFArqueo.Enabled = False
        Me.dtpFArqueo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFArqueo.Location = New System.Drawing.Point(96, 96)
        Me.dtpFArqueo.Name = "dtpFArqueo"
        Me.dtpFArqueo.Size = New System.Drawing.Size(112, 20)
        Me.dtpFArqueo.TabIndex = 17
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(8, 96)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(88, 23)
        Me.Label19.TabIndex = 16
        Me.Label19.Text = "Fecha Arqueo:"
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(8, 152)
        Me.Label18.Name = "Label18"
        Me.Label18.TabIndex = 15
        Me.Label18.Text = "Cajero:"
        '
        'lbNombreCajero
        '
        Me.lbNombreCajero.Location = New System.Drawing.Point(8, 184)
        Me.lbNombreCajero.Name = "lbNombreCajero"
        Me.lbNombreCajero.Size = New System.Drawing.Size(216, 23)
        Me.lbNombreCajero.TabIndex = 14
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(8, 208)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(104, 23)
        Me.Label16.TabIndex = 13
        Me.Label16.Text = "Observaciones:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtObservaciones.BackColor = System.Drawing.Color.Bisque
        Me.txtObservaciones.Location = New System.Drawing.Point(8, 232)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(208, 104)
        Me.txtObservaciones.TabIndex = 12
        Me.txtObservaciones.Text = ""
        '
        'dtpFApertura
        '
        Me.dtpFApertura.CustomFormat = "dd/MM/yy HH:mm"
        Me.dtpFApertura.Enabled = False
        Me.dtpFApertura.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFApertura.Location = New System.Drawing.Point(96, 120)
        Me.dtpFApertura.Name = "dtpFApertura"
        Me.dtpFApertura.Size = New System.Drawing.Size(112, 20)
        Me.dtpFApertura.TabIndex = 11
        '
        'frmArqueo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(664, 434)
        Me.Controls.Add(Me.grbEncabezado)
        Me.Controls.Add(Me.grpDesgloce)
        Me.Controls.Add(Me.lbUsuario)
        Me.Controls.Add(Me.grpControles)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtClave)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(680, 472)
        Me.Name = "frmArqueo"
        Me.Text = "Arqueo Simplificado"
        Me.grpControles.ResumeLayout(False)
        Me.grpDesgloce.ResumeLayout(False)
        Me.grbEncabezado.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


    Dim PMU As New PerfilModulo_Class
    Dim Id_Usuario As String = ""
    Private Sub txtClave_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtClave.KeyDown
        Try

       
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            If txtClave.Text <> "" Then
                Dim dt As New Data.DataTable
                cFunciones.Llenar_Tabla_Generico("SELECT Id_Usuario, Nombre from Usuarios where Clave_Interna ='" & txtClave.Text & "'", dt, GetSetting("SeeSoft", "Seguridad", "Conexion"))
                If dt.Rows.Count = 0 Then
                    MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
                    txtClave.Focus()

                Else

                    Try
                        PMU = VSM(dt.Rows(0).Item("Id_Usuario"), "ArqueoCaja") 'Carga los privilegios del usuario con el modulo 
                        If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo arqueo de caja", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
                            Me.lbUsuario.Text = dt.Rows(0).Item("Nombre")
                            Id_Usuario = dt.Rows(0).Item("Id_Usuario")
                            txtClave.Enabled = False
                        Me.grpControles.Enabled = True
                        Me.btnNuevo.Enabled = True
                        Me.btnBuscar.Enabled = True

                    Catch ex As SystemException
                        MsgBox(ex.Message)
                    End Try

                End If

            Else

                MsgBox("Debe de digitar la clave de usuario", MsgBoxStyle.Exclamation)

                Me.txtClave.Focus()
                End If

            End If
        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        sp_Nuevo()
    End Sub
    Sub sp_Nuevo()
        Me.sp_limpiar()
        If Buscar_Apertura(Me.Id_Usuario) Then


            cargarDatosApertura(NApertura)
            cargarVentasApertura(NApertura)
            cargarMoviApertura(NApertura)
            cargarAbonosApertura(NApertura)
            'Dim dt As New Data.DataTable
            'cFunciones.Llenar_Tabla_Generico("Select * From Moneda Where CodMoneda = 2 ", dt, GetSetting("SeeSoft", "SeePOS", "Conexion"))
            'If dt.Rows.Count > 0 Then
            '    tipoCambio = dt.Rows(0).Item("ValorCompra")

            'End If

            btnGuardar.Enabled = True
            grbEncabezado.Enabled = True
            grpDesgloce.Enabled = True
            nuevo = True
            Me.sp_CalcColonesDep()
            Me.txtPagoComprasCol.SelectAll()
            Me.txtPagoComprasCol.Focus()
        Else
            MsgBox("No puede crear arqueo, no tiene 1 apertura activa", MsgBoxStyle.Exclamation)

            Exit Sub
        End If
    End Sub
    Sub sp_limpiar()
        Me.grbEncabezado.Enabled = False
        Me.txtSalidaColones.Text = 0
        Me.txtSalidaDolares.Text = 0
        Me.txtEntradasDolares.Text = 0
        Me.txtEntradaColones.Text = 0
        Me.txtTotalVentas.Text = 0
        Me.txtFondoAperturaCaja.Text = 0
        Me.txtTarjetaColones.Text = 0
        Me.txtTarjetaDolares.Text = 0
        Me.txtPagoComprasCol.Text = 0
        Me.txtDolaresDep.Text = 0
        Me.txtColonesDep.Text = 0
        Me.btnGuardar.Enabled = False
        Me.txtObservaciones.Text = ""
        Me.txtIdArqueo.Text = "0"
        Me.txtIDApertura.Text = "0"
        Me.txtEstado.Text = "Estado:"
        Me.dtpFApertura.Value = Now
        Me.txtAbonosCxC.Text = "0"
        Me.dtpFArqueo.Value = Now
        nuevo = False
        Anulado = False
        Me.btnAnular.Enabled = False
        Me.btnImprimir.Enabled = False

        Dim dt As New Data.DataTable
        cFunciones.Llenar_Tabla_Generico("Select * From Moneda Where CodMoneda = 2 ", dt, GetSetting("SeeSoft", "SeePOS", "Conexion"))
        If dt.Rows.Count > 0 Then
            tipoCambio = dt.Rows(0).Item("ValorCompra")

        End If

    End Sub
    Sub sp_CalcColonesDep()
        Try
            Dim totalColonesDep As Double = CDbl(Me.txtTotalContado.Text) + Me.EntradasColones _
               - Me.SalidasColones _
               - CDbl(Me.txtTarjetaColones.Text) - CDbl(Me.txtTarjetaDolares.Text) _
                                                           - (CDbl(Me.txtDolaresDep.Text) * tipoCambio) _
                                                           - CDbl(Me.txtPagoComprasCol.Text) + CDbl(Me.txtAbonosCxC.Text)

            If totalColonesDep < 0 Then
                Me.txtColonesDep.Text = Format(totalColonesDep, "#,##0.00")
            Else
                Me.txtColonesDep.Text = Format(totalColonesDep, "#,##0.00")
            End If

        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Calculo de Deposito")

        End Try
    End Sub
    Sub cargarDatosApertura(ByVal Apertura As Integer)
        Dim dt As New Data.DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT id_total_tope, NApertura, CodMoneda, Monto_Tope, MonedaNombre FROM Apertura_Total_Tope WHERE CodMoneda = 1 AND (NApertura = " & Apertura & ")", dt, GetSetting("SeeSoft", "SeePOs", "Conexion"))

        If dt.Rows.Count > 0 Then
            Me.txtFondoAperturaCaja.Text = Format(dt.Rows(0).Item("Monto_Tope"), "#,##0.00")

        End If

    End Sub
    Sub cargarVentasApertura(ByVal Apertura As Integer)
        Dim dt As New Data.DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT Num_Apertura, SUM(Total) AS STotal FROM Ventas WHERE (Anulado = 0) GROUP BY Num_Apertura HAVING (Num_Apertura = " & Apertura & ") ", dt, )
        If dt.Rows.Count > 0 Then
            Me.txtTotalVentas.Text = Format(dt.Rows(0).Item("STotal"), "#,##0.00")

        End If

        cFunciones.Llenar_Tabla_Generico("SELECT Num_Apertura, SUM(Total) AS STotal FROM Ventas WHERE (Anulado = 0) GROUP BY Num_Apertura, Tipo HAVING (Tipo = 'CRE' AND Num_Apertura = " & Apertura & ") ", dt, )
        If dt.Rows.Count > 0 Then
            Me.txtTotalCredito.Text = Format(dt.Rows(0).Item("STotal"), "#,##0.00")

        End If

        cFunciones.Llenar_Tabla_Generico("SELECT Num_Apertura, SUM(Total) AS STotal FROM Ventas WHERE (Anulado = 0) GROUP BY Num_Apertura, Tipo HAVING (Tipo = 'CON' AND Num_Apertura = " & Apertura & ") ", dt, )
        If dt.Rows.Count > 0 Then
            Me.txtTotalContado.Text = Format(dt.Rows(0).Item("STotal"), "#,##0.00")

        End If

    End Sub
    Sub cargarAbonosApertura(ByVal Apertura As Integer)
        Dim dt As New Data.DataTable
        cFunciones.Llenar_Tabla_Generico("SELECT SUM(MontoPago) AS Abono, Numapertura FROM OpcionesDePago WHERE (TipoDocumento = 'ABO')and (FormaPago ='EFE') OR  (FormaPago ='TAR') GROUP BY Numapertura,TipoDocumento HAVING (TipoDocumento ='ABO')AND (Numapertura = " & Apertura & ") ", dt, )
        If dt.Rows.Count > 0 Then
            Me.txtAbonosCxC.Text = Format(dt.Rows(0).Item("Abono"), "#,##0.00")

        End If

    End Sub
    Dim SalidasColones As Double = 0
    Dim EntradasColones As Double = 0
    Sub cargarMoviApertura(ByVal Apertura As Integer)
        Dim dt As New Data.DataTable
        EntradasColones = 0
        SalidasColones = 0
        'Entradas Colones
        cFunciones.Llenar_Tabla_Generico("SELECT NumApertura, SUM(Monto) AS Total FROM Movimiento_Caja WHERE (Entrada = 1) AND (Anulado = 0) GROUP BY CodMoneda, NumApertura HAVING (CodMoneda = 1) AND (NumApertura = " & Apertura & ")", dt, )
        If dt.Rows.Count > 0 Then
            Me.txtEntradaColones.Text = Format(dt.Rows(0).Item("Total"), "#,##0.00")
            EntradasColones += dt.Rows(0).Item("Total")
        End If
        'Entradas Dolares
        cFunciones.Llenar_Tabla_Generico("SELECT NumApertura, SUM(Monto) AS TColon, SUM(Monto * TipoCambio) AS Total  FROM Movimiento_Caja WHERE (Entrada = 1) AND (Anulado = 0) GROUP BY CodMoneda, NumApertura HAVING (CodMoneda = 2) AND (NumApertura = " & Apertura & ")", dt, )
        If dt.Rows.Count > 0 Then
            Me.txtEntradasDolares.Text = Format(dt.Rows(0).Item("Total"), "#,##0.00")
            EntradasColones += dt.Rows(0).Item("TColon")
        End If
        'Salidas Colones
        cFunciones.Llenar_Tabla_Generico("SELECT NumApertura, SUM(Monto) AS Total FROM Movimiento_Caja WHERE (Salida = 1) AND (Anulado = 0) GROUP BY CodMoneda, NumApertura HAVING (CodMoneda = 1) AND (NumApertura = " & Apertura & ")", dt, )
        If dt.Rows.Count > 0 Then
            Me.txtSalidaColones.Text = Format(dt.Rows(0).Item("Total"), "#,##0.00")
            SalidasColones += dt.Rows(0).Item("Total")
        End If
        'Salidas Dolares
        cFunciones.Llenar_Tabla_Generico("SELECT NumApertura, SUM(Monto) AS Total, SUM(Monto * TipoCambio) AS TColon FROM Movimiento_Caja WHERE (Salida = 1) AND (Anulado = 0) GROUP BY CodMoneda, NumApertura HAVING (CodMoneda = 2) AND (NumApertura = " & Apertura & ")", dt, )
        If dt.Rows.Count > 0 Then
            Me.txtSalidaDolares.Text = Format(dt.Rows(0).Item("Total"), "#,##0.00")
            SalidasColones += dt.Rows(0).Item("TColon")
        End If

    End Sub
    Dim NApertura As Integer
    Function Buscar_Apertura(ByVal usuario As String) As Boolean
        Try
            Dim func As New cFunciones
            Dim dt As New Data.DataTable
            func.Llenar_Tabla_Generico("SELECT * FROM AperturaCaja WHERE (Anulado = 0) AND (Estado = 'A') AND (Cedula = '" & usuario & "')", dt, GetSetting("SeeSoft", "SeePOS", "Conexion"))

            Select Case dt.Rows.Count

                Case 1
                    NApertura = dt.Rows(0).Item("NApertura")
                    Me.txtIDApertura.Text = NApertura
                    Me.dtpFApertura.Value = dt.Rows(0).Item("Fecha")
                    Me.lbNombreCajero.Text = Me.lbUsuario.Text
                    Select Case dt.Rows(0).Item("Estado")
                        Case "A"
                            Me.txtEstado.Text = "Estado: Activa"
                        Case "M"
                            Me.txtEstado.Text = "Estado: En Revision"
                        Case "C"
                            Me.txtEstado.Text = "Estado: Cerrada"

                    End Select

                    Return True

                Case 0
                    MsgBox(lbUsuario.Text & " No tiene una apertura de caja abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
                    Return False
                Case Else
                    MsgBox(lbUsuario.Text & " " & "tiene mas de una abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
                    Return False
            End Select


        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Private Sub txtPagoComprasCol_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPagoComprasCol.KeyDown, txtDolaresDep.KeyDown, txtTarjetaColones.KeyDown, txtTarjetaDolares.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            Me.sp_CalcColonesDep()
        End If
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Me.sp_CalcColonesDep()
        sp_GuardarArqueo()
    End Sub
    Dim nuevo As Boolean = False
    Dim tipoCambio As Double = 1

    Sub sp_GuardarArqueo()
        Try
            Dim cnx As New Conexion
            cnx.Conectar()
            Dim sql As String = ""
            Dim msg As String = ""
            If Me.nuevo Then
                sql = "INSERT INTO [SEEPOS].[dbo].[ArqueoCajas]  ([EfectivoColones]  ,[EfectivoDolares]  ,[TarjetaColones]  ,[TarjetaDolares]  ,[Cheques]  ,[ChequesDol]  ,[DepositoCol]  ,[DepositoDol]  ,[Total]  ,[IdApertura]  , [Cajero]  ,[Anulado]  ,[TipoCambioD]  ,[Observaciones]  ,[TotalCompras]) " & _
                "  VALUES  (" & CDbl(Me.txtColonesDep.Text) & "  ," & CDbl(Me.txtDolaresDep.Text) & "  ," & CDbl(Me.txtTarjetaColones.Text) & "  ," & CDbl(Me.txtTarjetaDolares.Text) & "  ,0  ,0  ,0  ,0  ," & CDbl(Me.txtTotalVentas.Text) & "  ," & CInt(Me.txtIDApertura.Text) & "  , '" & Me.lbNombreCajero.Text & "' ,0  , " & tipoCambio & "  ,'" & Me.txtObservaciones.Text & "' ," & CDbl(Me.txtPagoComprasCol.Text) & ") "
                msg = cnx.SlqExecute(cnx.sQlconexion, sql)
                If Not (msg Is Nothing) Then
                    MsgBox(msg, MsgBoxStyle.Critical)
                    cnx.DesConectar(cnx.sQlconexion)
                    Exit Sub
                End If
                Dim dt As New Data.DataTable
                cFunciones.Llenar_Tabla_Generico("SELECT MAX(Id) AS Id FROM ArqueoCajas ORDER BY Id DESC", dt)
                If dt.Rows.Count > 0 Then
                    txtIdArqueo.Text = dt.Rows(0).Item("Id")
                    Me.nuevo = False
                End If

            Else
                sql = "UPDATE [SEEPOS].[dbo].[ArqueoCajas]  SET [EfectivoColones] = " & CDbl(Me.txtColonesDep.Text) & "  ,[EfectivoDolares] = " & CDbl(Me.txtDolaresDep.Text) & "  ,[TarjetaColones] = " & CDbl(Me.txtTarjetaColones.Text) & " ,[TarjetaDolares] = " & CDbl(Me.txtTarjetaDolares.Text) & "   ,[Cheques] = 0  ,[ChequesDol] = 0  ,[DepositoCol] = 0  ,[DepositoDol] = 0  ,[Total] = " & CDbl(Me.txtTotalVentas.Text) & "  ,[IdApertura] = " & CInt(Me.txtIDApertura.Text) & " ,[Fecha] = GETDATE() ,[Cajero] = '" & Me.lbNombreCajero.Text & "'  ,[Observaciones] = '" & Me.txtObservaciones.Text & "'  ,[TotalCompras] = " & CDbl(Me.txtTarjetaDolares.Text) & "   WHERE Id = " & Me.txtIdArqueo.Text
                msg = cnx.SlqExecute(cnx.sQlconexion, sql)
                If Not (msg Is Nothing) Then
                    MsgBox(msg, MsgBoxStyle.Critical)
                    cnx.DesConectar(cnx.sQlconexion)
                    Exit Sub
                End If

            End If

            sql = "UPDATE aperturacaja Set Estado = 'M' Where NApertura = " & Me.NApertura
            msg = cnx.SlqExecute(cnx.sQlconexion, sql)
            If Not (msg Is Nothing) Then
                MsgBox(msg, MsgBoxStyle.Critical)
            End If
            cnx.DesConectar(cnx.sQlconexion)
            MsgBox("Guardado Satisfactorio", MsgBoxStyle.OKOnly)

            If PMU.Print Then
                Me.btnImprimir.Enabled = True
            End If


        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Guardar Arqueo")


        End Try

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        sp_Buscar()
    End Sub
    Dim Anulado As Boolean = False
    Sub sp_Buscar()
        Try
            Dim cFunciones As New cFunciones
            Dim Id_ArqueoCaja As String = cFunciones.Buscar_X_Descripcion_Fecha("select cast(Id as varchar) as Arqueo, Cajero,Fecha from ArqueoCajas Order by Id Desc", "Cajero", "Fecha", "Arqueo Caja ....")

            If Id_ArqueoCaja = Nothing Then

            Else


                Dim dt As New Data.DataTable
                cFunciones.Llenar_Tabla_Generico(" SELECT [Id],[EfectivoColones],[EfectivoDolares],[TarjetaColones],[TarjetaDolares],[Cheques],[ChequesDol]  ,[DepositoCol]  ,[DepositoDol]  ,[Total]  ,[IdApertura]  ,[Fecha]  ,[Cajero]  ,[Anulado]  ,[TipoCambioD]  ,[Observaciones]  ,[TotalCompras]  FROM [Seepos].[dbo].[ArqueoCajas] " & _
                "where [Id] = " & Id_ArqueoCaja & "", dt)

                If dt.Rows.Count > 0 Then
                    Me.sp_limpiar()
                    Me.NApertura = dt.Rows(0).Item("IdApertura")
                    Dim _dt As New Data.DataTable
                    cFunciones.Llenar_Tabla_Generico("SELECT * FROM AperturaCaja WHERE (NApertura =  " & NApertura & " )", _dt, GetSetting("SeeSoft", "SeePOS", "Conexion"))
                    If _dt.Rows.Count > 0 Then

                        Me.dtpFApertura.Value = _dt.Rows(0).Item("Fecha")
                        Select Case _dt.Rows(0).Item("Estado")
                            Case "A"
                                Me.txtEstado.Text = "Estado: Activa"
                            Case "M"
                                Me.txtEstado.Text = "Estado: En Revision"
                            Case "C"
                                Me.txtEstado.Text = "Estado: Cerrada"

                        End Select

                    End If
                    Me.dtpFArqueo.Value = dt.Rows(0).Item("Fecha")
                    Me.lbNombreCajero.Text = dt.Rows(0).Item("Cajero")
                    Me.txtTarjetaColones.Text = dt.Rows(0).Item("TarjetaColones")
                    Me.txtTarjetaDolares.Text = dt.Rows(0).Item("TarjetaDolares")
                    Me.txtPagoComprasCol.Text = dt.Rows(0).Item("TotalCompras")
                    Me.txtDolaresDep.Text = dt.Rows(0).Item("EfectivoDolares")
                    Me.txtColonesDep.Text = dt.Rows(0).Item("EfectivoColones")
                    Me.btnGuardar.Enabled = False
                    Me.txtObservaciones.Text = dt.Rows(0).Item("Observaciones")
                    Me.txtIdArqueo.Text = Id_ArqueoCaja
                    Me.txtIDApertura.Text = NApertura
                    Me.cargarDatosApertura(NApertura)
                    Me.cargarMoviApertura(NApertura)
                    Me.cargarDatosApertura(NApertura)
                    Me.cargarVentasApertura(NApertura)
                    Me.cargarAbonosApertura(NApertura)
                    Me.cargarDatosApertura(NApertura)
                    Anulado = dt.Rows(0).Item("Anulado")
                    If Anulado Then
                        Me.btnAnular.Enabled = False
                        Me.txtObservaciones.Text &= " (ARQUEO ANULADO)"
                    Else
                        If PMU.Delete Then
                            Me.btnAnular.Enabled = True
                        End If
                        btnGuardar.Enabled = True
                        grbEncabezado.Enabled = True
                        grpDesgloce.Enabled = True
                        nuevo = False
                        Me.txtPagoComprasCol.SelectAll()
                        Me.txtPagoComprasCol.Focus()
                    End If
                    If PMU.Print Then
                        Me.btnImprimir.Enabled = True
                    End If
                End If

            End If
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Buscar Arqueo")

        End Try
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        sp_AnularArqueo()
    End Sub
    Sub sp_AnularArqueo()

        If MsgBox("�Desea anular este arqueo?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim msg As String = ""
            Dim cnx As New Conexion
            cnx.Conectar()
            msg = cnx.SlqExecute(cnx.sQlconexion, _
            "UPDATE [dbo].[ArqueoCajas]  SET [Anulado] =1   WHERE Id = " & Me.txtIdArqueo.Text)
            If Not (msg Is Nothing) Then
                MsgBox(msg, MsgBoxStyle.Critical)
                Exit Sub
            End If

            msg = cnx.SlqExecute(cnx.sQlconexion, _
            "UPDATE aperturacaja Set Estado = 'A' Where NApertura = " & Me.NApertura)
            If Not (msg Is Nothing) Then
                MsgBox(msg, MsgBoxStyle.Critical)
                Exit Sub
            End If
            cnx.DesConectar(cnx.sQlconexion)
            MsgBox("Anulaci�n Satisfactoria", MsgBoxStyle.OKOnly)
            Me.txtObservaciones.Text &= " ARQUEO ANULADO"
            Me.btnAnular.Enabled = False

        End If
    End Sub

    Private Sub txtPagoComprasCol_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPagoComprasCol.LostFocus
        Me.sp_CalcColonesDep()

    End Sub

    Private Sub txtTarjetaColones_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTarjetaColones.LostFocus
        Me.sp_CalcColonesDep()

    End Sub

    Private Sub txtTarjetaDolares_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTarjetaDolares.LostFocus
        Me.sp_CalcColonesDep()
    End Sub

    Private Sub txtDolaresDep_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDolaresDep.LostFocus
        Me.sp_CalcColonesDep()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        sp_Imprimir()
    End Sub
    Sub sp_Imprimir()
        Try
            If MsgBox("�Desea imprimir?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim rpt As New rptPVEArqueo
                rpt.SetParameterValue("IDArqueo", txtIdArqueo.Text)
                rpt.SetParameterValue("IDApertura", txtIDApertura.Text)
                rpt.SetParameterValue("FechaApertura", dtpFApertura.Value)
                rpt.SetParameterValue("FechaArqueo", dtpFArqueo.Value)
                rpt.SetParameterValue("Observaciones", txtObservaciones.Text)
                rpt.SetParameterValue("FondoCaja", txtFondoAperturaCaja.Text)
                rpt.SetParameterValue("TotalVentas", txtTotalVentas.Text)
                rpt.SetParameterValue("TotalAbonos", txtAbonosCxC.Text)
                rpt.SetParameterValue("SalidasColones", txtSalidaColones.Text)
                rpt.SetParameterValue("EntradasColones", txtEntradaColones.Text)
                rpt.SetParameterValue("SalidasDolares", txtSalidaDolares.Text)
                rpt.SetParameterValue("EntradasDolares", txtEntradasDolares.Text)
                rpt.SetParameterValue("TotalPagos", txtPagoComprasCol.Text)
                rpt.SetParameterValue("TarjetasColones", txtTarjetaColones.Text)
                rpt.SetParameterValue("TarjetasDolares", txtTarjetaDolares.Text)
                rpt.SetParameterValue("DolaresDepositar", txtDolaresDep.Text)
                rpt.SetParameterValue("Cajero", lbNombreCajero.Text)
                rpt.SetParameterValue("TotalCredito", txtTotalCredito.Text)
                rpt.SetParameterValue("TotalContado", txtTotalContado.Text)
                If Anulado Then
                    rpt.SetParameterValue("Anulado", "ANULADO")
                Else
                    rpt.SetParameterValue("Anulado", "")
                End If

                rpt.SetParameterValue("ColonesDepositar", txtColonesDep.Text)
                Dim visor As New frmVisorReportes
                visor.MdiParent = Me.ParentForm
                visor.rptViewer.ReportSource = rpt
                visor.Show()

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmArqueo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub txtAbonosCxC_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAbonosCxC.TextChanged

    End Sub
End Class

